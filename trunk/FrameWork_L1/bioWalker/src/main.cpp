#include <irrlicht.h>

//#include <assert.h>
//#pragma comment(lib,"irrlicht.lib")

/*
초급응용 예제

응용요소

1. 지형과 반응하는 캐릭터 걷기
2. FSM 응용 캐릭터제어
3. 원하는 방향으로 물체이동
4. 발사체 공격 구현
5. 장면관리툴 이용하기
*/

//배이스 클래스
class CGameObject : public irr::scene::ISceneNodeAnimator
{
public:
	irr::u32 m_uLastTick;	
	irr::scene::ISceneManager *m_pSmgr;
	irr::video::IVideoDriver *m_pVideo;
	//FSM 구현용
	int m_nStatus;	
	int m_nWork;

};

//발사체 객체
class CBullet_Object : public CGameObject
{
public:

	irr::scene::ISceneManager *m_pSmgr;
	irr::core::list<irr::scene::ISceneNode *> m_Colliderlist;	
	

	CBullet_Object()
	{
	}
	virtual ~CBullet_Object()
	{
	}

	//펙토리 메쏘드
	inline static CBullet_Object* Create(
		irr::scene::ISceneManager *pSmgr,
		irr::core::list<irr::scene::ISceneNode *> &Colliderlist
		)
	{
		CBullet_Object *pObj = new CBullet_Object();

		pObj->m_pSmgr = pSmgr;
		pObj->m_Colliderlist = Colliderlist;
		//pObj->m_pHitCheckCallBack = pHitCheckCallBack;		
		
		return (pObj);
	}

	virtual inline void animateNode(irr::scene::ISceneNode* node, irr::u32 timeMs)
	{
		irr::scene::ISceneNodeList::Iterator it = m_Colliderlist.begin();

		
		for(it;it!= m_Colliderlist.end();it++)
		{
			//월드좌표끼리의 비교		

			irr::core::aabbox3df collide = node->getTransformedBoundingBox();
			irr::core::aabbox3df collider = (*it)->getTransformedBoundingBox();

			if(collide.intersectsWithBox(collider))
			{
				//m_pHitCheckCallBack->OnHit(node,timeMs,NULL);	

						printf("hit \n");			

						//바로 없애주면 컨테이너 리스트 순회시 오류가 발생하므로 저장해두엇다가 일괄적으로 제거해준다.
						m_pSmgr->addToDeletionQueue(node);

						{
							irr::scene::IParticleSystemSceneNode* pPSNode = 0;
							pPSNode = m_pSmgr->addParticleSystemSceneNode();
							pPSNode->setPosition(node->getAbsolutePosition());
							pPSNode->setParticleSize(irr::core::dimension2d<irr::f32>(2.0f, 2.0f));

							irr::scene::IParticleEmitter *pEm = pPSNode->createPointEmitter();
							pPSNode->setEmitter(pEm);
							pEm->drop();


							irr::scene::IParticleAffector *pAf = pPSNode->createFadeOutParticleAffector(
								irr::video::SColor(0,0,0,0),300);
							pPSNode->addAffector(pAf);
							pAf->drop();

							pPSNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
							pPSNode->setMaterialFlag(irr::video::EMF_ZWRITE_ENABLE,false);

							pPSNode->setMaterialTexture(0,m_pSmgr->getVideoDriver()->getTexture("../res/fire.bmp"));
							pPSNode->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);

							irr::scene::ISceneNodeAnimator *pAnim = m_pSmgr->createDeleteAnimator(500);
							pPSNode->addAnimator(pAnim);
							pAnim->drop();
						}				

				break;
			}
		}
	}


	virtual ISceneNodeAnimator* createClone(irr::scene::ISceneNode* node, irr::scene::ISceneManager* newManager=0)
	{
		return NULL;
	}	
};

//캐릭터 객체
class CWalkerAnimator : public CGameObject,
	irr::scene::IAnimationEndCallBack
{
public:
	bool m_Key[irr::KEY_KEY_CODES_COUNT];

	

	enum {
		EST_STAND,
		EST_ATTACK,
		EST_WALK
	};

	//펙토리 메쏘드
	inline static CWalkerAnimator* CreateCWalkerAnimator(irr::scene::ISceneManager *pSmgr,irr::video::IVideoDriver *pVideo)
	{
		CWalkerAnimator *pObj = new CWalkerAnimator();
		pObj->m_pSmgr = pSmgr;	
		pObj->m_pVideo = pVideo;

		return (pObj);

	}

	CWalkerAnimator()
	{
		int i;
		for(i=0;i<irr::KEY_KEY_CODES_COUNT;i++)
		{
			m_Key[i] = false;
		}
		m_nStatus = 0;
	}

	virtual ~CWalkerAnimator()
	{
	}

	virtual void OnAnimationEnd(irr::scene::IAnimatedMeshSceneNode *pNode)
	{
		switch(m_nStatus)
		{
		case EST_ATTACK:
			m_nStatus = EST_STAND;//서있는동작으로 다시
			pNode->setFrameLoop(206,250);
			pNode->setLoopMode(true);

			{
				irr::scene::ISceneNode *pBillNode = m_pSmgr->addBillboardSceneNode();
				pBillNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pBillNode->setMaterialTexture(0,m_pVideo->getTexture("portal1.bmp"));
				pBillNode->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);

				//발사체 처리
				irr::scene::ISceneNodeAnimator *pAnim;

				irr::core::vector3df vDir(0,0,1);				

				irr::core::matrix4 mat;
				mat = pNode->getAbsoluteTransformation();
				mat[12] = 0;
				mat[13] = 0;
				mat[14] = 0;

				mat.transformVect(vDir);

				//직선이동애니메이터
				pAnim = m_pSmgr->createFlyStraightAnimator(pNode->getAbsolutePosition(),
					pNode->getAbsolutePosition() + vDir*50,3000);

				pBillNode->addAnimator(pAnim);
				pAnim->drop();

				pAnim = m_pSmgr->createDeleteAnimator(3000);
				pBillNode->addAnimator(pAnim);
				pAnim->drop();

				//충돌처리 애니메이터
				irr::core::list<irr::scene::ISceneNode *> coll_list;
				
				//충돌 타겟지정
				coll_list.push_back(m_pSmgr->getSceneNodeFromName("usr/scene/cube/collider1"));
				coll_list.push_back(m_pSmgr->getSceneNodeFromName("usr/scene/cube/collider2"));

				pAnim = CBullet_Object::Create(m_pSmgr,coll_list);
				pBillNode->addAnimator(pAnim);				
				pAnim->drop();
			}

			break;
		}
	}

	//일반 애니메이션 제어와 상태변환처리
	virtual void animateNode(irr::scene::ISceneNode* node, irr::u32 timeMs)
	{
		irr::f32 fDelta = (float)(timeMs - m_uLastTick) / 1000.f;	

		irr::scene::IAnimatedMeshSceneNode *pAniNode;
		pAniNode = (irr::scene::IAnimatedMeshSceneNode *)m_pSmgr->getSceneNodeFromName("body",node);

		switch(m_nStatus)
		{
		case EST_STAND://정지상태
			if(m_Key[irr::KEY_UP])
			{
				m_nStatus = EST_WALK;
				pAniNode->setFrameLoop(0,13);
				pAniNode->setLoopMode(true);
			}
			if(m_Key[irr::KEY_SPACE])
			{
				m_nStatus = EST_ATTACK;
				pAniNode->setFrameLoop(73,83);
				pAniNode->setLoopMode(false);
				pAniNode->setAnimationEndCallback(this);			
			}
			break;
		case EST_WALK://이동상태
			if(!m_Key[irr::KEY_UP])
			{
				m_nStatus = EST_STAND;
				pAniNode->setFrameLoop(206,250);
				pAniNode->setLoopMode(true);
			}
			else
			{				
				irr::core::vector3df vDir(0,0,1);	

				//행렬에서 이동 부분만 남기기				
				//mat4(행,렬)
				/*
				일리히트는 열우선
				* * * #
				* * * #
				* * * #
				* * * *

				이동부분제거하기
				*/

				//이동요소 부분만 제거하기
				irr::core::matrix4 mat;
				mat = node->getAbsoluteTransformation();
				mat[12] = 0;
				mat[13] = 0;
				mat[14] = 0;

				mat.transformVect(vDir);

				node->setPosition(node->getPosition() + (fDelta*10*vDir));			
			}
			break;
		case EST_ATTACK:
			break;
		}		

		//돌리기
		if(m_Key[irr::KEY_LEFT])
		{
			//회전 각속도(각/초) * 시간델타값
			node->setRotation(node->getRotation() + irr::core::vector3df(0,90.f*fDelta,0));
		}
		if(m_Key[irr::KEY_RIGHT])
		{
			node->setRotation(node->getRotation() - irr::core::vector3df(0,90.f*fDelta,0));
		}		

		m_uLastTick = timeMs;
	}
	
	virtual ISceneNodeAnimator* createClone(irr::scene::ISceneNode* node, irr::scene::ISceneManager* newManager=0)
	{
		return NULL;
	}
	
	virtual bool isEventReceiverEnabled() const
	{
		return true;
	}
	
	virtual bool OnEvent(const irr::SEvent& event)
	{
		if(event.EventType == irr::EET_KEY_INPUT_EVENT)
		{
			if(event.KeyInput.PressedDown)
			{
				m_Key[event.KeyInput.Key] = true;
			}
			else
			{
				m_Key[event.KeyInput.Key] = false;
			}
		}
		return false;
	}
};

class CMyApp :  public irr::IEventReceiver
{
public:

	irr::IrrlichtDevice *m_pDevice;

	irr::video::IVideoDriver *m_pVideo;
	irr::scene::ISceneManager *m_pSmgr;
	irr::gui::IGUIEnvironment *m_pGuiEnv;

	CWalkerAnimator *m_pWalkerAnim;
	irr::scene::ISceneNodeAnimatorCollisionResponse *m_pCRAnim;	

	CMyApp ()
	{
		m_pWalkerAnim = 0;
		m_pCRAnim = 0;

		m_pDevice = irr::createDevice(     
			irr::video::EDT_OPENGL,
			irr::core::dimension2du(640,480), 32,
			false, false, false,
			this
			);
		m_pVideo = m_pDevice->getVideoDriver();
		m_pSmgr = m_pDevice->getSceneManager();
		m_pGuiEnv = m_pDevice->getGUIEnvironment(); 

		//scene inittialize here
		
		//작업디랙토리설정
		m_pDevice->getFileSystem()->changeWorkingDirectoryTo("../../FrameWork_L1/bioWalker/res");

		//씬로딩
		m_pSmgr->loadScene("scene/bw_World.irr");

		//카메라세팅
		{
			irr::scene::ICameraSceneNode *pCam = 
				(irr::scene::ICameraSceneNode *)m_pSmgr->getSceneNodeFromName("usr/scene/camera/1");				
			m_pSmgr->setActiveCamera((irr::scene::ICameraSceneNode *)pCam);
			pCam->setTarget(m_pSmgr->getSceneNodeFromName("usr/scene/camera/1/lookat")->getPosition());
		}

		{
			irr::scene::ITerrainSceneNode *pTerrain = 
				(irr::scene::ITerrainSceneNode *)m_pSmgr->getSceneNodeFromName("usr/scene/terrain/1");			
			irr::scene::ITriangleSelector* pSelector = m_pSmgr->createTerrainTriangleSelector(pTerrain);
			pTerrain->setTriangleSelector(pSelector);
			pSelector->drop();
		}

		//닌자 오브잭트만들기
		{
			irr::scene::ISceneNode *pNode = m_pSmgr->getSceneNodeFromName("usr/scene/ninja_root");			

			//생성포인트 얻어서 위치시키기
			pNode->setPosition(m_pSmgr->getSceneNodeFromName("usr/scene/point/1")->getPosition());

			//애니메이션 초기화
			irr::scene::IAnimatedMeshSceneNode *pAniNode = 
				 (irr::scene::IAnimatedMeshSceneNode *)m_pSmgr->getSceneNodeFromName("body",pNode);
			pAniNode->setFrameLoop(206,250);
			pAniNode->setLoopMode(true);
			pAniNode->setAnimationSpeed(30.f);

			//충돌관리자.애니메이터
			irr::scene::ITerrainSceneNode *pTerrain = 
				(irr::scene::ITerrainSceneNode *)m_pSmgr->getSceneNodeFromName("usr/scene/terrain/1");
			irr::scene::ITriangleSelector* pSelector = pTerrain->getTriangleSelector();

			irr::scene::ISceneNodeAnimatorCollisionResponse *pcrAnim = m_pSmgr->createCollisionResponseAnimator(
				pSelector,pNode,
				irr::core::vector3df(2,5,2),
				irr::core::vector3df(0,-10,0),
				irr::core::vector3df(0,0,0),
				0.005f
				);
			pNode->addAnimator(pcrAnim);
			m_pCRAnim = pcrAnim;
			pcrAnim->drop();


			////바이오하자드식 컨트롤 애니메이터 만들기
			CWalkerAnimator *pAnim = CWalkerAnimator::CreateCWalkerAnimator(m_pSmgr,m_pVideo);
			pNode->addAnimator(pAnim);
			m_pWalkerAnim = pAnim;
			pAnim->drop();		
			

			
		}

		//디버깅 정보출력용
		{
			irr::scene::ISceneNode *pNode = m_pSmgr->addSphereSceneNode(1.f,8);
			pNode->setScale(m_pCRAnim->getEllipsoidRadius());
			pNode->setPosition(-m_pCRAnim->getEllipsoidTranslation());
			pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
			pNode->getMaterial(0).Wireframe = true;		
			pNode->setName("usr/dbg/vol/1");

			irr::scene::ISceneNode *pRootNode = m_pSmgr->getSceneNodeFromName("usr/scene/ninja_root");
			pRootNode->addChild(pNode);
		}

		//유아이 초기화
		{
			m_pGuiEnv->addCheckBox(true,irr::core::rect<irr::s32>(0,32,64,32+16),0,100,L"View Character Volume");

			irr::gui::IGUISpinBox *pGuiSpin;
			pGuiSpin = m_pGuiEnv->addSpinBox(L"ellips_Tr_X",irr::core::rect<irr::s32>(0,48,128,64),true,0,200);
			pGuiSpin->setValue(0);			
			pGuiSpin = m_pGuiEnv->addSpinBox(L"ellips_Tr_Y",irr::core::rect<irr::s32>(0,64,128,80),true,0,201);
			pGuiSpin->setValue(0);	
			pGuiSpin = m_pGuiEnv->addSpinBox(L"ellips_Tr_Z",irr::core::rect<irr::s32>(0,80,128,96),true,0,202);
			pGuiSpin->setValue(0);

			
			pGuiSpin = m_pGuiEnv->addSpinBox(L"ellips_sc_X",irr::core::rect<irr::s32>(0,128,128,144),true,0,210);
			pGuiSpin->setValue(0);
			pGuiSpin = m_pGuiEnv->addSpinBox(L"ellips_sc_Y",irr::core::rect<irr::s32>(0,144,128,160),true,0,211);
			pGuiSpin->setValue(0);
			pGuiSpin = m_pGuiEnv->addSpinBox(L"ellips_sc_Z",irr::core::rect<irr::s32>(0,160,128,176),true,0,212);
			pGuiSpin->setValue(0);
		}

	}

	~CMyApp ()
	{
		m_pDevice->drop();
	} 

	//이벤트 핸들러
	virtual bool OnEvent(const irr::SEvent& event)
	{
		if(m_pWalkerAnim)
		{
			if(m_pWalkerAnim->isEventReceiverEnabled())
			{
				m_pWalkerAnim->OnEvent(event);

			}
		}

		switch(event.EventType)
		{
		case irr::EET_GUI_EVENT:
			{
				if(event.GUIEvent.EventType == irr::gui::EGET_CHECKBOX_CHANGED)
				{
					
					if(event.GUIEvent.Caller->getID() == 100)
					{
						//체크박스 체크여부확인
						m_pSmgr->getSceneNodeFromName("usr/dbg/vol/1")->setVisible(
							((irr::gui::IGUICheckBox *)event.GUIEvent.Caller)->isChecked()
							);
					}
					
				}
				else if(event.GUIEvent.EventType == irr::gui::EGET_SPINBOX_CHANGED)
				{
					if(event.GUIEvent.Caller->getID() == 200)
					{					
						m_pCRAnim->setEllipsoidTranslation(
							irr::core::vector3df(((irr::gui::IGUISpinBox *)event.GUIEvent.Caller)->getValue(),0,0)
						);

						irr::scene::ISceneNode *pDbgNode = m_pSmgr->getSceneNodeFromName("usr/dbg/vol/1");
						pDbgNode->setPosition(-m_pCRAnim->getEllipsoidTranslation());
					}
					if(event.GUIEvent.Caller->getID() == 201)
					{					
						m_pCRAnim->setEllipsoidTranslation(
							irr::core::vector3df(0,((irr::gui::IGUISpinBox *)event.GUIEvent.Caller)->getValue(),0)
						);

						irr::scene::ISceneNode *pDbgNode = m_pSmgr->getSceneNodeFromName("usr/dbg/vol/1");
						pDbgNode->setPosition(-m_pCRAnim->getEllipsoidTranslation());
					}
					if(event.GUIEvent.Caller->getID() == 202)
					{					
						m_pCRAnim->setEllipsoidTranslation(
							irr::core::vector3df(0,0,((irr::gui::IGUISpinBox *)event.GUIEvent.Caller)->getValue())
						);

						irr::scene::ISceneNode *pDbgNode = m_pSmgr->getSceneNodeFromName("usr/dbg/vol/1");
						pDbgNode->setPosition(-m_pCRAnim->getEllipsoidTranslation());
					}
				}

			}
			break;
		case irr::EET_KEY_INPUT_EVENT:
			{
				//if(   [event.KeyInput.Key]
			}
			break;
		case irr::EET_MOUSE_INPUT_EVENT:
			{

			}
			break;
		case irr::EET_USER_EVENT:
			break;
		}
		return false;
	}

	void Update()
	{

	}

};


int main()
{
	CMyApp App; 



	while(App.m_pDevice->run())
	{  

		App.Update();


		App.m_pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

		App.m_pSmgr->drawAll();
		App.m_pGuiEnv->drawAll();

		{			
			char szBuf[256];
			sprintf(szBuf,"fps %d",App.m_pVideo->getFPS());

			irr::gui::IGUIFont *pDefaultFont = App.m_pGuiEnv->getBuiltInFont();

			irr::core::stringw strw = szBuf;

			pDefaultFont->draw(strw.c_str(),irr::core::rect<irr::s32>(0,0,60,20),irr::video::SColor(255,0,255,0));
				
		}
		App.m_pVideo->endScene();

		
	}

	return 0;
}

