#include <irrlicht.h>
#include <iostream>

class CGameFSMObject : public irr::scene::ISceneNodeAnimator
{
	enum {
		EST_STAND,
		EST_WALK2POINT
	};

	
protected:

	irr::u32 m_uLastTick; //타이밍 계산용
	irr::scene::ISceneManager *m_pSmgr;
	irr::IEventReceiver *m_pObjMng;


	//FSM 
	int m_nStatus;
	int m_nWork;
	irr::core::vector3df m_vMoveTarget;

public:

	irr::scene::ISceneNode *m_pRootNode;

	void setStatus(irr::u32 status)
	{
		m_nStatus = status;
		m_nWork = 0;
	}

	int getStatus()
	{
		return m_nStatus;
	}
//생성자
	CGameFSMObject(irr::IrrlichtDevice *pDevice,irr::IEventReceiver *pObjMng=NULL)
	{
		m_pRootNode = 0;
		m_pSmgr = pDevice->getSceneManager();
		if(!pObjMng)
		{
			m_pObjMng = pDevice->getEventReceiver();
		}
		setStatus(EST_STAND);
	}
	//파괴자
	virtual ~CGameFSMObject()
	{
	}	

	//로직 업데이트 함수
	virtual void animateNode(irr::scene::ISceneNode *node,irr::u32 timeMS)
	{
		irr::f32 fDelta = (float)(timeMS - m_uLastTick)/1000.f;

		//FSM코드....오브잭트 제어

		switch(m_nStatus)
		{
		case EST_STAND:

			if(m_nWork == 0) //초기화 단계
			{
				irr::scene::IAnimatedMeshSceneNode *pNode = 
					(irr::scene::IAnimatedMeshSceneNode *)m_pSmgr->getSceneNodeFromName("body",node);
				pNode->setFrameLoop(136,168);
				m_nWork = 10;
			}
			else //실제 진행단계
			{

			}
			break;
		case EST_WALK2POINT:

			//지정된 위치로 이동시키기상태
			if(m_nWork == 0)
			{
				irr::scene::IAnimatedMeshSceneNode *pNode = 
					(irr::scene::IAnimatedMeshSceneNode *)m_pSmgr->getSceneNodeFromName("body",node);
				pNode->setFrameLoop(1,19);
				m_nWork = 10;
			}
			else if(m_nWork == 10)
			{
				irr::core::vector3df vPos = node->getPosition();
				//방향벡터 얻기
				irr::core::vector3df vDir = m_vMoveTarget - vPos;

				if(vDir.getLength() < 1.0f)
				{
					setStatus(EST_STAND);

				}
				else
				{
					vDir.normalize();
					irr::f32 roty = vDir.getHorizontalAngle().Y;
					irr::f32 fSpeed = 2.0f;
					node->setPosition(node->getPosition() + (vDir*fDelta*fSpeed) );
					node->setRotation(irr::core::vector3df(0,roty,0));
				}
			}

			break;
		}

		m_uLastTick = timeMS;
	}

	virtual bool isEventReceiverEnabled() const
	{
		return true;
	}

	virtual irr::scene::ISceneNodeAnimator *createClone(irr::scene::ISceneNode *node,
		irr::scene::ISceneManager *newManager=0)
	{
		return NULL;
	}

	//이벤트처리
	virtual bool OnEvent(const irr::SEvent &event)
	{
		if(event.EventType == irr::EET_USER_EVENT)
		{
			printf("signal : %s,param %d \n",event.LogEvent.Text,event.LogEvent.Level);

			//시그널 파싱하기
			irr::core::array<irr::core::stringc> tokens;
			irr::core::stringc strc = (char*)event.LogEvent.Text;
			irr::u32 count = strc.split(tokens,
				"/ ,", //분리자지정
				3 //분리자 갯수
				);
			int i;
			for(i=0;i<count;i++)
			{
				std::cout << tokens[i].c_str() << std::endl;
			}

			//해석
			if(tokens[0] == "move")
			{
				setStatus(EST_WALK2POINT);
				m_vMoveTarget = irr::core::vector3df(
					irr::core::fast_atof(tokens[1].c_str()),
					0,
					irr::core::fast_atof(tokens[2].c_str())
					);

			}
		}
		return false;
	}

};

//포대 객체
class CGameFSMObject_Turet : public CGameFSMObject
{
	enum {
		EST_STAND,
		EST_ATTACK
	};

public:

	CGameFSMObject *m_pTarget;
	irr::scene::ISceneNode *m_pTargetNode;

	//생성자
	CGameFSMObject_Turet(irr::IrrlichtDevice *pDevice,irr::IEventReceiver *pObjMng=NULL)
		:CGameFSMObject(pDevice)
	{
		m_pTarget = 0;
		m_pTargetNode = 0;
		
	}
	//파괴자
	virtual ~CGameFSMObject_Turet()
	{

	}	

	virtual void animateNode(irr::scene::ISceneNode *node,irr::u32 timeMS)
	{
		irr::f32 fDelta = (float)(timeMS - m_uLastTick)/1000.f;

		//FSM코드....오브잭트 제어

		switch(m_nStatus)
		{
		case EST_STAND:
			break;
		case EST_ATTACK:
			{
				if(m_pTargetNode)
				{
					irr::core::vector3df vDir = m_pTargetNode->getPosition() - node->getPosition();
					vDir.normalize();
					
					irr::f32 roty = vDir.getHorizontalAngle().Y;
					printf("%f \n",roty);
					node->setRotation(irr::core::vector3df(0,roty,0));
				}
			}
			break;
		default:
			break;
		}

		m_uLastTick = timeMS;
	}

	//이벤트처리
	virtual bool OnEvent(const irr::SEvent &event)
	{
		if(event.EventType == irr::EET_USER_EVENT)
		{
			printf("signal : %s,param %d \n",event.LogEvent.Text,event.LogEvent.Level);

			//시그널 파싱하기
			irr::core::array<irr::core::stringc> tokens;
			irr::core::stringc strc = (char*)event.LogEvent.Text;
			irr::u32 count = strc.split(tokens,
				"/ ,", //분리자지정
				3 //분리자 갯수
				);
			int i;
			for(i=0;i<count;i++)
			{
				std::cout << tokens[i].c_str() << std::endl;
			}

			//해석
			if(tokens[0] == "attack")
			{
				m_pTargetNode = 
					m_pSmgr->getSceneNodeFromName(tokens[1].c_str());
				setStatus(EST_ATTACK);
			}
		}
		return false;
	}


};

class CMyApp : public irr::IEventReceiver 
{
public:

	irr::IrrlichtDevice *m_pDevice;

	irr::core::map<irr::core::stringc,CGameFSMObject *> m_mapObjects;

	CGameFSMObject *m_pZombieObject;
	CGameFSMObject *m_pZombieObject2;

	CGameFSMObject_Turet *m_pTuret;

	CMyApp()
	{
	}

	irr::core::stringc m_strSignal;
	
	virtual bool OnEvent(const irr::SEvent &event)
	{
		switch(event.EventType)
		{
		case irr::EET_KEY_INPUT_EVENT:
			if(event.KeyInput.PressedDown)
			{
				if(event.KeyInput.Key == irr::KEY_F1)
				{
					//일리히트 이벤트 처리기를 이용한 시그널처리예
					static char szSignal[256];
					irr::SEvent ev;
					ev.EventType = irr::EET_USER_EVENT;
					sprintf(szSignal,"move %f,%f",10.f,10.f);

					ev.LogEvent.Text = szSignal;
					ev.LogEvent.Level = irr::ELL_INFORMATION;
					m_pZombieObject->OnEvent(ev);

				}
				else if(event.KeyInput.Key == irr::KEY_F2)
				{
					irr::scene::ISceneNode *pTargetNode = m_pDevice->getSceneManager()->getSceneNodeFromName
						("Depence_Base");

					static char szSignal[256];
					irr::SEvent ev;
					ev.EventType = irr::EET_USER_EVENT;
					sprintf(szSignal,"move %f,%f",
						pTargetNode->getPosition().X,
						pTargetNode->getPosition().Z);

					ev.LogEvent.Text = szSignal;
					ev.LogEvent.Level = irr::ELL_INFORMATION;
					m_pZombieObject->OnEvent(ev);
				}
				else if(event.KeyInput.Key == irr::KEY_F3)
				{
					irr::scene::ISceneNode *pTargetNode = m_pDevice->getSceneManager()->getSceneNodeFromName
						("Depence_Base");

					static char szSignal[256];
					irr::SEvent ev;
					ev.EventType = irr::EET_USER_EVENT;
					sprintf(szSignal,"move %f,%f",
						pTargetNode->getPosition().X,
						pTargetNode->getPosition().Z);

					ev.LogEvent.Text = szSignal;
					ev.LogEvent.Level = irr::ELL_INFORMATION;
					m_pZombieObject2->OnEvent(ev);
				}
				else if(event.KeyInput.Key == irr::KEY_F4)
				{
					static char szSignal[256];
					irr::SEvent ev;
					ev.EventType = irr::EET_USER_EVENT;
					sprintf(szSignal,"attack %s",
						m_pZombieObject->m_pRootNode->getName()	   	   		  						
						);

					ev.LogEvent.Text = szSignal;
					ev.LogEvent.Level = irr::ELL_INFORMATION;
					m_pTuret->OnEvent(ev);
				}
			}
			break;
		case irr::EET_MOUSE_INPUT_EVENT:
			break;
		}
		return false;
	}

	void Update(irr::IrrlichtDevice *pDevice,irr::f32 fDelta)
	{
		irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
		irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
		irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

		//목표지점
		irr::core::vector3df vTarget(0,0,0);

		//피킹 
		{
			//픽킹레이
			irr::core::line3df PickingRay;
			irr::core::position2di mouse_pos = pDevice->getCursorControl()->getPosition();

			PickingRay = pSmgr->getSceneCollisionManager()->getRayFromScreenCoordinates(mouse_pos);

			irr::core::vector3df v3Intersec; //픽킹 위치
			irr::core::triangle3df tri; //픽킹된 폴리건
			irr::scene::ITriangleSelector *pSelector;

			pSelector = pSmgr->getSceneNodeFromName("usr/scene/ground")->getTriangleSelector();

			const irr::scene::ISceneNode *pCollNode;
			if(pSelector)
			{
				if(pSmgr->getSceneCollisionManager()->getCollisionPoint(PickingRay,pSelector,
					v3Intersec, //광선과 충돌점
					tri,pCollNode))
				{
					vTarget = v3Intersec; //목표지점 설정
					pSmgr->getSceneNodeFromName("usr/scene/arrow_pickingpos")->setPosition(v3Intersec);
					//printf("%f,%f,%f \n",v3Intersec.X,v3Intersec.Y,v3Intersec.Z);
				}
			}
		}		

		{
			irr::scene::ISceneNode *pNode = pSmgr->getSceneNodeFromName("usr/scene/arrow_testobj");
			irr::core::vector3df vPos = pNode->getPosition();

			//방향벡터얻기
			irr::core::vector3df vDir;
			vDir = vTarget - vPos;
			vDir.normalize(); //정규화

			irr::f32 roty = vDir.getHorizontalAngle().Y;

			pNode->setPosition(pNode->getPosition() + (vDir * fDelta * 2) ); 
			pNode->setRotation(irr::core::vector3df(90,roty,0));
		}
	}

};

int main()
{
	irr::IrrlichtDevice *pDevice = irr::createDevice(irr::video::EDT_OPENGL);
	pDevice->setWindowCaption(L"iDepence");

	//pDevice->getFileSystem()->changeWorkingDirectoryTo("./res");

	CMyApp theApp;


	//이벤트 리시버등록
	pDevice->setEventReceiver(&theApp);

	//디바이스등록
	theApp.m_pDevice = pDevice;
	

	irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
	irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
	irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

	//테스트용 오브잭트 만들기(화살표)
	{
		irr::scene::IAnimatedMesh *pMesh = pSmgr->addArrowMesh("usr/mesh/arrow/red",
			irr::video::SColor(255,255,0,0),
			irr::video::SColor(255,0,255,0)
			);//메쉬등록
		irr::scene::ISceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(pMesh);
		pNode->setPosition(irr::core::vector3df(0,5,0));
		pNode->setRotation(irr::core::vector3df(90,0,0));
		pNode->setScale(irr::core::vector3df(2,2,2));
		pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
		pNode->setName("usr/scene/arrow_testobj");

		pNode = pSmgr->addAnimatedMeshSceneNode(pMesh);
		//pNode->setPosition(irr::core::vector3df(0,5,0));
		pNode->setRotation(irr::core::vector3df(180,0,0));
		pNode->setScale(irr::core::vector3df(2,2,2));
		pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
		pNode->setName("usr/scene/arrow_pickingpos");

	}


	//리소스 로딩 지형만들기
	{
		irr::scene::ISceneNode *pNode = 0;
		irr::scene::IAnimatedMesh *pMesh;

		pMesh = pSmgr->addHillPlaneMesh(
			"myhill",
			irr::core::dimension2df(8,8),
			irr::core::dimension2du(5,5),
			0,0,
			irr::core::dimension2df(0,0),
			irr::core::dimension2df(5,5)
			);
		pNode = pSmgr->addAnimatedMeshSceneNode(pMesh);
		pNode->setPosition(irr::core::vector3df(0,0,0));
		pNode->setName("usr/scene/ground");
		pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
		pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);

		//기하정보 생성

		irr::scene::ITriangleSelector *pSelector;
		pSelector = pSmgr->createTriangleSelector((irr::scene::IAnimatedMeshSceneNode *)pNode);
		pNode->setTriangleSelector(pSelector);
		pSelector->drop();
	}

	//좀비 리소스 로딩
	{
		pSmgr->addEmptySceneNode()->setName("zombie_root");
		irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode
			(pSmgr->getMesh("./res/walkers/zombie/zombie.b3d"));
		pNode->setName("body");
		pNode->setParent(
			pSmgr->getSceneNodeFromName("zombie_root"));
		pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
		pNode->setScale(irr::core::vector3df(0.3f,0.3f,0.3f));
		pNode->setRotation(irr::core::vector3df(0,180,0));
	}

	//객체 만들기1
	{
		irr::scene::ISceneNode *pNode = pSmgr->getSceneNodeFromName("zombie_root");
		pNode->setPosition(irr::core::vector3df(0,0,20));	


		CGameFSMObject *pZombieObj = new CGameFSMObject(pDevice);
		pNode->addAnimator(pZombieObj); //애니메이터 등록
		pZombieObj->m_pRootNode = pNode;

		pZombieObj->drop();		

		theApp.m_pZombieObject = pZombieObj; //참조용
	}

	//객체 만들기2
	{
		irr::scene::ISceneNode *pNode = pSmgr->getSceneNodeFromName("zombie_root")->clone();
		pNode->setName("zombie_root_1");
		pNode->setPosition(irr::core::vector3df(10,0,20));
		CGameFSMObject *pZombieObj = new CGameFSMObject(pDevice);
		pNode->addAnimator(pZombieObj); //애니메이터 등록
		pZombieObj->m_pRootNode = pNode;

		pZombieObj->drop();
		theApp.m_pZombieObject2 = pZombieObj; //참조용
	}

	//방어진지
	{
		irr::scene::ISceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(
			pSmgr->getMesh("usr/mesh/arrow/red")
			);
		pNode->setPosition(irr::core::vector3df(0,0,-10));
		pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
		pNode->setScale(irr::core::vector3df(3.f,3.f,3.f));
		pNode->setName("Depence_Base");
	}

	//포대객체
	{
		irr::scene::ISceneNode *pRoot = pSmgr->addEmptySceneNode(0);
		pRoot->setName("turet_root");
		pRoot->setPosition(irr::core::vector3df(15,0,0));

		irr::scene::ISceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(
			pSmgr->getMesh("usr/mesh/arrow/red")
			);		
		pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
		pNode->setScale(irr::core::vector3df(2.f,2.f,2.f));
		pNode->setRotation(irr::core::vector3df(irr::core::vector3df(90,0,0)));
		pNode->setParent(pRoot);

		CGameFSMObject_Turet *pTuret = new CGameFSMObject_Turet(pDevice);
		pTuret->m_pRootNode = pRoot;
		
		pRoot->addAnimator(pTuret);
		

		theApp.m_pTuret = pTuret;

	}

	//카메라 만들기
	pSmgr->addCameraSceneNode(0,irr::core::vector3df(25,5,-5),irr::core::vector3df(0,0,0));



	irr::u32 uLastTick = pDevice->getTimer()->getTime();


	while(pDevice->run())
	{
		irr::u32 uTick = pDevice->getTimer()->getTime();
		irr::f32 fDelta = ((irr::f32)(uTick - uLastTick))/1000.f; //프레임간 시간차값 얻기
		uLastTick = uTick;

		theApp.Update(pDevice,fDelta);

		pVideo->beginScene(true,true,irr::video::SColor(255,100,101,140));
		
		pSmgr->drawAll();
		
		pVideo->endScene();

	}

	pDevice->drop();
//<<<<<<< .mine
	return 0;
}
//=======

	//return 0;
//}//>>>>>>> .r44
