
#include <irrlicht.h>

class CGameObject : public irr::scene::ISceneNodeAnimator
{
protected:
	//FSM 구현용
	int m_nStatus;	
	int m_nWork;

	void setStatus(irr::s32 nStatus)
	{
		m_nStatus = nStatus;
		m_nWork = 0;

	}

public:
	irr::u32 m_uLastTick;	
	irr::scene::ISceneManager *m_pSmgr;	

	CGameObject(irr::scene::ISceneManager *pSmgr) :
	m_pSmgr(pSmgr)	
	{

	}
};

class CGameObject_Zombie : public CGameObject
{
	enum
	{
		EST_STAND =0,
		EST_WALK
	};

public:


	CGameObject_Zombie(irr::scene::ISceneManager *pSmgr)
		: CGameObject(pSmgr)
	{
		setStatus(EST_STAND);

	}
	~CGameObject_Zombie()
	{

	}
	
	virtual inline void animateNode(irr::scene::ISceneNode* node, irr::u32 timeMs)
	{
		switch(m_nStatus)
		{
		case EST_STAND:
			if(m_nWork == 0)
			{
				irr::scene::IAnimatedMeshSceneNode *pAniNode = 
					(irr::scene::IAnimatedMeshSceneNode *)m_pSmgr->getSceneNodeFromName("body",node);
				pAniNode->setFrameLoop(137-1,169-1);
				m_nWork = 10;
			}
			else if(m_nWork == 10)
			{

			}
			break;
		case EST_WALK:
			if(m_nWork == 0)
			{
				irr::scene::IAnimatedMeshSceneNode *pAniNode = 
					(irr::scene::IAnimatedMeshSceneNode *)m_pSmgr->getSceneNodeFromName("body",node);
				pAniNode->setFrameLoop(2-1,20-1);
				m_nWork = 10;
			}
			else if(m_nWork == 10)
			{

			}
			break;			
		}
	}

	virtual bool OnEvent(const irr::SEvent& event)
	{		
		if(event.EventType == irr::EET_USER_EVENT)
		{
			printf("signal : %s,param %d \n",event.LogEvent.Text,event.LogEvent.Level);

			//시그널 파싱하기
			irr::core::array<irr::core::stringc> tokens;
			irr::core::stringc strc = (char*)event.LogEvent.Text;
			irr::u32 count = strc.split(tokens,
				"/ ,", //분리자지정
				3 //분리자 갯수
				);			

			//해석
			if(tokens[0] == "stand")
			{
				setStatus(EST_STAND);
				
			}
			else if(tokens[0] == "walk")
			{
				setStatus(EST_WALK);
			}
		}
		return false;
	}

	virtual ISceneNodeAnimator* createClone(irr::scene::ISceneNode* node, irr::scene::ISceneManager* newManager=0)
	{
		return NULL;
	}

};




class CSimpleApp : public irr::IEventReceiver 
{
public:

	irr::IrrlichtDevice *m_pDevice;
	irr::scene::ISceneManager *m_pSmgr;
	irr::video::IVideoDriver *m_pVideo;
	irr::gui::IGUIEnvironment *m_pGuiEnv;

	irr::core::map<irr::core::stringc,irr::gui::IGUIElement *> m_mapName2Gui;
	irr::core::map<irr::gui::IGUIElement *,irr::core::stringc> m_mapGui2Name;

	irr::core::stringc m_strSignal;

	CGameObject *m_pZombie;

	CSimpleApp()
	{
	}

	~CSimpleApp()
	{
		
	}
	
	void Init()
	{
		m_pDevice = irr::createDevice(irr::video::EDT_OPENGL);
		m_pSmgr = m_pDevice->getSceneManager();
		m_pVideo = m_pDevice->getVideoDriver();
		m_pGuiEnv = m_pDevice->getGUIEnvironment();

		m_pDevice->setEventReceiver(this);
		m_pDevice->getFileSystem()->changeWorkingDirectoryTo("../../FrameWork_L1/iStanByMe/res");

		//씬 로딩
		m_pSmgr->loadScene("scene.xml");

		//카메라 세팅
		{
			m_pSmgr->setActiveCamera(
				(irr::scene::ICameraSceneNode *)m_pSmgr->getSceneNodeFromName(
				"cam1",
				m_pSmgr->getSceneNodeFromName("cam_root"))
				);
		}	

		//gui 세팅
		{
			irr::gui::IGUIButton *pBtn = m_pGuiEnv->addButton(irr::core::recti(50,400,150,420));
			pBtn->setText(L"Stand");

			irr::core::stringc strName = "StandButton";
			m_mapName2Gui[strName] = pBtn;
			m_mapGui2Name[pBtn] = strName;
		}

		{
			irr::gui::IGUIButton *pBtn = m_pGuiEnv->addButton(
				irr::core::recti(50,400,150,420) + irr::core::vector2di(120,0)
				);
			pBtn->setText(L"Walk");

			irr::core::stringc strName = "WalkButton";
			m_mapName2Gui[strName] = pBtn;
			m_mapGui2Name[pBtn] = strName;
		}

		//오브잭트 초기화
		{
			m_pZombie = new CGameObject_Zombie(m_pSmgr);

			irr::scene::ISceneNode *pNode = m_pSmgr->getSceneNodeFromName("zombie_root");
			pNode->addAnimator(m_pZombie);
			m_pZombie->drop();

		}

	}

	void Close()
	{
		m_pDevice->drop();
	}

	virtual bool OnEvent(const irr::SEvent &event)
	{
		switch(event.EventType)
		{
		case irr::EET_KEY_INPUT_EVENT:			
			break;
		case irr::EET_MOUSE_INPUT_EVENT:
			break;
		case irr::EET_GUI_EVENT:
			{
				irr::core::stringc strName = m_mapGui2Name[event.GUIEvent.Caller];

				if(event.GUIEvent.EventType == irr::gui::EGET_BUTTON_CLICKED)
				{
					if(strName == "StandButton")
					{						
						printf("push stand \n");

						static char szSignal[256];
						irr::SEvent ev;
						ev.EventType = irr::EET_USER_EVENT;
						sprintf(szSignal,"stand");

						ev.LogEvent.Text = szSignal;
						ev.LogEvent.Level = irr::ELL_INFORMATION;

						m_pZombie->OnEvent(ev);
					}
					else if(strName == "WalkButton")
					{
						printf("push walk \n");
						
						static char szSignal[256];
						irr::SEvent ev;
						ev.EventType = irr::EET_USER_EVENT;
						sprintf(szSignal,"walk");

						ev.LogEvent.Text = szSignal;
						ev.LogEvent.Level = irr::ELL_INFORMATION;

						m_pZombie->OnEvent(ev);
					}
				}

			}
			break;
		default:
			break;
		}
		return false;
	}

	void Run()
	{		
		while(m_pDevice->run())
		{
			m_pVideo->beginScene();

			m_pSmgr->drawAll();
			m_pGuiEnv->drawAll();

			m_pVideo->endScene();
		}
	}

};

CSimpleApp theApp;

int main()
{
	theApp.Init();

	theApp.Run();

	theApp.Close();
	
	return 0;
}