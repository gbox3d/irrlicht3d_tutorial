#include <irrlicht.h>
#include <assert.h>

#pragma comment(lib,"irrlicht.lib")

class CMyApp :  public irr::IEventReceiver
{
	
public :

	CMyApp()
	{

		{
			int ix,iy;
			irr::scene::ISceneNode *pNode;

			for(iy=0;iy< 4; iy++)
			{
				for(ix=0;ix< 4; ix++)
				{
					m_Board[ix][iy] = 0;
				}
			}	
		}

	}

	irr::core::stringc m_strSignal;


	irr::c8 m_Board[4][4];
	irr::core::position2di m_posCursor;

	virtual bool OnEvent(const irr::SEvent& event)
	{
		switch(event.EventType)
		{
		case irr::EET_GUI_EVENT:
			{			

			}
			break;
		case irr::EET_KEY_INPUT_EVENT:
			{
				if(!event.KeyInput.PressedDown) //키를 눌렀다 땟을때..
				{
					if(event.KeyInput.Key == irr::KEY_RIGHT)
					{
						m_posCursor += irr::core::position2di(1,0);
					}
					else if(event.KeyInput.Key == irr::KEY_LEFT)
					{
						m_posCursor -= irr::core::position2di(1,0);
					}
					else if(event.KeyInput.Key == irr::KEY_KEY_1)
					{
						m_strSignal = "put black";
						m_Board[m_posCursor.Y][m_posCursor.X] = 1;
					}
					else if(event.KeyInput.Key == irr::KEY_KEY_2)
					{
						m_Board[m_posCursor.Y][m_posCursor.X] = 2;
					}
				}
				

			}
			break;
		case irr::EET_MOUSE_INPUT_EVENT:
			{

			}
			break;
		case irr::EET_USER_EVENT:
			{
				
			}
			break;		
		}
		return false;
	}

	void Update(irr::IrrlichtDevice *pDevice)
	{		
		irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
		irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
		irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

		if(m_strSignal == "put black")
		{
			printf("put blackl!! \n");
			m_strSignal = "no signal";

			irr::scene::ISceneNode *pNode;
			pNode = pSmgr->addSphereSceneNode(2);
			pNode->setScale(irr::core::vector3df(1.f,.5f,1.f));
			pNode->setMaterialTexture(0, pVideo->getTexture("black_dol.jpg"));		
			pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);	
			irr::core::vector3df cursorpos = irr::core::vector3df(-(4 + 8*(1-m_posCursor.X)),2,(4 + 8*(1-m_posCursor.Y)));
			pNode->setPosition(cursorpos + irr::core::vector3df(0,5,0));
			

		}

		//커서 위치 설정
		{
			irr::scene::ISceneNode *pNode = pSmgr->getSceneNodeFromName("usr/node/arrow_cursor");
			pNode->setPosition(irr::core::vector3df(-(4 + 8*(1-m_posCursor.X)),2,(4 + 8*(1-m_posCursor.Y))));
		}

		

		//바둑돌 보이게하기
		{
			int ix,iy;
			for(iy=0;iy< 4; iy++)
			{
				for(ix=0;ix< 4; ix++)
				{
					irr::core::stringc strName = irr::core::stringc(ix) + irr::core::stringc(iy);
					irr::scene::ISceneNode *pNode = pSmgr->getSceneNodeFromName(strName.c_str(),0);
					if(m_Board[iy][ix] == 1)
					{
						pNode->setMaterialTexture(0, pVideo->getTexture("black_dol.jpg"));
						pNode->setVisible(true);
					}
					else if(m_Board[iy][ix] == 2)
					{
						pNode->setMaterialTexture(0, pVideo->getTexture("white_dol.jpg"));
						pNode->setVisible(true);
					}
					else
					{
						pNode->setVisible(false);
					}

				}
			}


		}
		
	}
};



void main()
{
	irr::IrrlichtDevice *pDevice = irr::createDevice(     
		irr::video::EDT_DIRECT3D9,
		irr::core::dimension2du(640,480), 32,
		false, false, true,
		NULL
		);

	pDevice->setWindowCaption(L"irr_omok");

	//이벤트 리시버 등록
	CMyApp theApp;
	pDevice->setEventReceiver(&theApp);

	//get engine proerty

	irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
	irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
	irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

	pDevice->getFileSystem()->changeWorkingDirectoryTo("../../FrameWork_L1/irr_oMok/res/");



	// camera 

	pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,20,-15), irr::core::vector3df(0,0,0));


	//커서용 화살표 만들기
	{

		irr::scene::IAnimatedMesh *pMesh = pSmgr->addArrowMesh("usr/mesh/arrow/red",
			irr::video::SColor(255,255,0,0),
			irr::video::SColor(255,0,255,0)
			); //메쉬등록	

		irr::scene::ISceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(pMesh);
		pNode->setPosition(irr::core::vector3df(0,5,0));
		pNode->setScale(irr::core::vector3df(2,2,2));
		pNode->setRotation(irr::core::vector3df(180,0,0));
		pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
		pNode->setName("usr/node/arrow_cursor");

	}


	// 바둑판용 플래인 메쉬 생성추가
	{
		irr::scene::ISceneNode* pNode = 0;
		irr::scene::IAnimatedMesh* pMesh;			

		pMesh = pSmgr->addHillPlaneMesh(
			"myHill",
			irr::core::dimension2d<irr::f32>(8,8),
			irr::core::dimension2d<irr::u32>(5,5), 
			0,0,
			irr::core::dimension2d<irr::f32>(0,0),
			irr::core::dimension2d<irr::f32>(5,5));

		pNode = pSmgr->addAnimatedMeshSceneNode(pMesh);
		pNode->setPosition(irr::core::vector3df(0,0,0));
		pNode->setMaterialTexture(0, pVideo->getTexture("pan_ele.jpg"));		
		pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
	}

	{
		int ix,iy;
		irr::scene::ISceneNode *pNode;

		for(iy=0;iy< 4; iy++)
		{
			for(ix=0;ix< 4; ix++)
			{
				pNode = pSmgr->addSphereSceneNode(2);
				pNode->setScale(irr::core::vector3df(1.f,.5f,1.f));
				//pNode->setMaterialTexture(0, pVideo->getTexture("omok/black_dol.jpg"));		
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);	
				pNode->setPosition(irr::core::vector3df(-(4 + 8*(1-ix)),0,(4 + 8*(1-iy))));	
				pNode->setVisible(false);

				irr::core::stringc strName = irr::core::stringc(ix) + irr::core::stringc(iy);

				pNode->setName(strName.c_str());
			}
		}
	}

	

	


	//g_Board[1][1] = 1; //흰동 
	//g_Board[1][2] = 2; //검은돌		


	//프레임 레이트 표시용 유아이
	irr::gui::IGUIStaticText *pstextFPS = 
		pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

	irr::u32 uLastTick = pDevice->getTimer()->getTime();
	while(pDevice->run())
	{ 
		irr::u32 uTick = pDevice->getTimer()->getTime();
		irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f;  

		uLastTick = pDevice->getTimer()->getTime();

		theApp.Update(pDevice);


		//프레임레이트 갱신
		{
			wchar_t wszbuf[256];
			swprintf(wszbuf,L"Frame rate : %d",pVideo->getFPS());
			pstextFPS->setText(wszbuf);
		}


		

		pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

		pSmgr->drawAll();
		pGuiEnv->drawAll();  

		pVideo->endScene(); 
	}

	pDevice->drop();
}

