/*
* sample_fortris.cpp
*
*  Created on: 2011. 6. 1.
*  Author: 도플광어(gbox3d)

일리히트엔진으로 배워보는  탄도 시뮬레이션 예제 v1.0

*/

#include <irrlicht.h>
#include "CSampleApp.h"

namespace canon1 {

	void main() {
		irr::IrrlichtDevice *pDevice = irr::createDevice(
			irr::video::EDT_OPENGL);

		CSampleApp theApp(irr::core::vector2di(64,320));

		pDevice->setEventReceiver(&theApp);

		pDevice->setWindowCaption(L"Type-A1");

		irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
		irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
		irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

		pSmgr->addCameraSceneNode(0, irr::core::vector3df(0, 5, -10),
			irr::core::vector3df(0, 0, 0));

		//프레임 레이트 표시용 유아이
		irr::gui::IGUIStaticText *pstextFPS = pGuiEnv->addStaticText(L"Frame rate",
			irr::core::rect<irr::s32>(0, 0, 100, 20), true, true, 0, 100, true);

		{
			irr::scene::ISceneNode *pRoot = pSmgr->addEmptySceneNode();
			pRoot->setName("Turet");
			pRoot->setRotation(irr::core::vector3df(-45,0,0));

			irr::scene::IAnimatedMesh *pMesh = pSmgr->addArrowMesh("usr/mesh/arrow/red",
				irr::video::SColor(255, 255, 0, 0),
				irr::video::SColor(255, 255, 0, 0)); //메쉬등록

			irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(pMesh,pRoot);
			pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
			pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);
			pNode->setRotation(irr::core::vector3df(90,0,0));
		}

		{
			irr::scene::ISceneNode *pRoot = pSmgr->addEmptySceneNode();
			pRoot->setName("arrow");
			pRoot->setRotation(irr::core::vector3df(0,0,0));

			irr::scene::IAnimatedMesh *pMesh = pSmgr->addArrowMesh("yellow_arrow",
				irr::video::SColor(255, 255, 255, 0),
				irr::video::SColor(255, 255, 255, 0)
				); //메쉬등록
		}

		{
			//힐플래인 메쉬 추가

			irr::scene::IAnimatedMesh *pMesh = pSmgr->addHillPlaneMesh(
				"plane",
				irr::core::dimension2d<irr::f32>(4, 4),
				irr::core::dimension2d<irr::u32>(16, 16), 0, 0,
				irr::core::dimension2d<irr::f32>(0, 0),
				irr::core::dimension2d<irr::f32>(8, 8));
			irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(pMesh);
			pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
			pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);
			pNode->setName("ground");

			irr::scene::ITriangleSelector *pSelector = pSmgr->createTriangleSelector(pNode);
			pNode->setTriangleSelector(pSelector);
			pSelector->drop();
		}

		//질량은 계산에 포함하지 않았다.
		// 나중에 항력이 들어가는 예제 만들때 넣을 예정임
		irr::f32  fAccel = 1.f; //가속도
		irr::f32 gforce = 0.98f; //중력
		irr::f32 _fTimeDelta = 1.0f; //시간 델타값

		while (pDevice->run()) {


			static irr::u32 uLastTick = 0;
			irr::u32 uTick = pDevice->getTimer()->getTime();
			irr::f32 fDelta = ((float) (uTick - uLastTick)) / 1000.f; //델타값 구하기
			uLastTick = uTick;

			//프레임레이트 갱신, 삼각형수 표시
			{
				wchar_t wszbuf[256];
				swprintf(wszbuf,256 ,L"Frame rate : %d\n TriAngle: %d",
					pVideo->getFPS(), pVideo->getPrimitiveCountDrawn());
				//swprintf(wszbuf,256,L"");
				pstextFPS->setText(wszbuf);
			}

			{
				irr::scene::ISceneNode *pNode = pSmgr->getSceneNodeFromName("Turet");
				if (theApp.m_bDirCmd[0]) {//up

					pNode->setRotation(
						pNode->getRotation() - irr::core::vector3df(1, 0, 0)
						* fDelta * 45.f);//초당 45
				}
				if (theApp.m_bDirCmd[4]) {//down

					pNode->setRotation(
						pNode->getRotation() + irr::core::vector3df(1, 0, 0)
						* fDelta * 45.f);//초당 45
				}
				if (theApp.m_bDirCmd[2]) { //right

					pNode->setRotation(
						pNode->getRotation() + irr::core::vector3df(0, 1, 0)
						* fDelta * 45.f);//초당 45
				}
				if (theApp.m_bDirCmd[6]) {//left

					pNode->setRotation(
						pNode->getRotation() - irr::core::vector3df(0, 1, 0)
						* fDelta * 45.f);//초당 45
				}

				if (theApp.m_bTrigerCmd[0]) {
					_fTimeDelta += fDelta * 1.0f;
					//printf("%f \n",_fDetail);

				}
				if (theApp.m_bTrigerCmd[1]) {//left

					_fTimeDelta -= fDelta * 1.0f;
					//printf("%f \n",_fDetail);
				}

				if (theApp.m_bDirCmd[7]) {
					fAccel += fDelta * 1.0f;
					//printf("%f \n",force);

				}
				if (theApp.m_bDirCmd[1]) {//left

					fAccel -= fDelta * 1.0f;
					//printf("%f \n",force);
				}

			}

			pVideo->beginScene(true, true, irr::video::SColor(255, 100, 101, 140));

			pSmgr->drawAll();
			pGuiEnv->drawAll();

			theApp.DrawControlButton(pVideo);

			{
				irr::video::SMaterial m;
				m.Lighting = false; //라이트를꺼야색이제데로나온다.
				m.Wireframe = true;
				pVideo->setMaterial(m);
			}
			//탄도계산
			{
				irr::scene::ISceneNode *pNode =
					pSmgr->getSceneNodeFromName("Turet");


				irr::core::vector3df start = irr::core::vector3df(0,0,0);
				irr::core::vector3df end = start;			

				irr::core::vector3df vAccel = pNode->getRotation().rotationToDirection() * fAccel; //탄환의 가속도 벡터

				irr::core::vector3df vPos = pNode->getAbsolutePosition() + 
					(pNode->getRotation().rotationToDirection() * 1.0f); //포신길이

				irr::core::vector3df vGravity = irr::core::vector3df(0,-1,0) * gforce;			

				irr::core::array<irr::core::vector3df> aPoint; //탄도 위치
				irr::core::array<irr::core::vector3df> aDirVector; //탄도 각도

				irr::u32 i;
				start = vPos;
				irr::core::vector3df vVelocity(0,0,0);
				i=0;
				irr::f32 fTime=0;
				while(i < 256) {

					fTime += _fTimeDelta;

					end =  vPos + vAccel * fTime + ((vGravity * fTime * fTime) / 2.0f);	

					irr::core::vector3df fVAngle = (vAccel + vGravity*fTime).normalize().getHorizontalAngle();
					aDirVector.push_back(fVAngle);

					/*
					직선과 면의 교차점을 판별하여 충돌위치를 구한다.
					*/					
					{
						irr::scene::ITriangleSelector *pSelector = pSmgr->getSceneNodeFromName("ground")->getTriangleSelector();

						irr::core::triangle3df col_tri;
						irr::core::vector3df col_pos;
						const irr::scene::ISceneNode *pcol_Node;
						irr::core::line3df Ray(start, end);					
											
						//지면과 충돌한 위치구하기
						if(pSmgr->getSceneCollisionManager()->getCollisionPoint(Ray,pSelector,
							col_pos,col_tri,
							pcol_Node
							)) //충돌점이 존재하면
						{						
							aPoint.push_back(col_pos);
							break;
						}
						else //충돌하지않았으면
						{
							aPoint.push_back(end);
							start = end;
						}
					}
					i++;
				}


				for(i=0;i<aPoint.size();i++)
				{
					irr::core::aabbox3df box(-irr::core::vector3df(0.1,0.1,0.1),irr::core::vector3df(0.1,0.1,0.1));
					irr::core::matrix4 mat;
					mat.setTranslation(aPoint[i]);
					mat.setRotationDegrees(aDirVector[i]);
				
					irr::core::matrix4 matWorld;
					irr::core::matrix4 matlocal;

					matlocal.makeIdentity();
					matlocal.setRotationDegrees(irr::core::vector3df(90,0,0));

					matWorld = mat * matlocal;

					pVideo->setTransform(irr::video::ETS_WORLD,matWorld);

					int i;
					irr::scene::IMesh *pMesh = pSmgr->getMesh("yellow_arrow")->getMesh(0);					
					for(i=0;i<pMesh->getMeshBufferCount();i++)
					{
						pVideo->drawMeshBuffer(pMesh->getMeshBuffer(i));
					}

				}

				//마지막 요소는 충돌위치
				{
					irr::core::aabbox3df box(-irr::core::vector3df(0.1,0.1,0.1),irr::core::vector3df(0.1,0.1,0.1));
					irr::core::matrix4 mat;
					mat.setTranslation(aPoint.getLast());
					pVideo->setTransform(irr::video::ETS_WORLD,mat);			


					pVideo->draw3DBox(box,irr::video::SColor(255,255,0,0));
				}
			}

			pVideo->endScene();
		}

		pDevice->drop();
	}

}
