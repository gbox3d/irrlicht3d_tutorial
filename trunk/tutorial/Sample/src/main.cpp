/*
 * main.cpp
 *
 *  Created on: 2011. 6. 1.
 *      Author: gbox3d
 */


#include <iostream>
#include <map>
#include <string>

#include <irrlicht.h>

using namespace std;

typedef void (*fPtr)();
std::map<std::string, fPtr > mapExam;

namespace canon1 {
void main();
}

int main(int argc,char *argv[]) {

	mapExam["canon1"] = canon1::main;

	fPtr funct_pt;

	std::string input;

	if(argc <= 1)
	{
		cout << "sample" << endl; // prints beginner guide sample

		cout << "input exam : ";
		
		std::cin >> input;		
	}
	else
	{
		input = argv[1];
	}

	if (mapExam.find(input) != mapExam.end()) {
		mapExam[input]();
	} else {
		std::cout << "not found fuction" << std::endl;
	}

	return 0;
}
