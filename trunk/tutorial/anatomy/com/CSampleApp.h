/*
* CSampleApp.h
*
*  Created on: 2011. 6. 1.
*      Author: gbox3d
*/

#ifndef CSAMPLEAPP_H_
#define CSAMPLEAPP_H_

class CSampleApp :  public irr::IEventReceiver
{
public:

	irr::core::map<irr::EKEY_CODE,bool> m_KeyBuffer;

	bool m_bDirCmd[8];
	bool m_bTrigerCmd[4];

	irr::core::recti m_rectDirBtn[8];
	irr::core::recti m_rectCmdBtn[4];


	CSampleApp(irr::core::vector2di pos)
	{

		m_rectDirBtn[0] = irr::core::recti(0,0,32,32) + pos; //up
		m_rectDirBtn[1] = m_rectDirBtn[0] + irr::core::vector2di(32,0);
		m_rectDirBtn[2] = m_rectDirBtn[1] + irr::core::vector2di(0,32);
		m_rectDirBtn[3] = m_rectDirBtn[2] + irr::core::vector2di(0,32);
		m_rectDirBtn[4] = m_rectDirBtn[3] + irr::core::vector2di(-32,0);
		m_rectDirBtn[5] = m_rectDirBtn[4] + irr::core::vector2di(-32,0);
		m_rectDirBtn[6] = m_rectDirBtn[5] + irr::core::vector2di(0,-32);
		m_rectDirBtn[7] = m_rectDirBtn[6] + irr::core::vector2di(0,-32);

		m_rectCmdBtn[0] = m_rectDirBtn[0] + irr::core::vector2di(32*4,0);
		m_rectCmdBtn[1] = m_rectCmdBtn[0] + irr::core::vector2di(32,0);
		m_rectCmdBtn[2] = m_rectCmdBtn[1] + irr::core::vector2di(32,0);
		m_rectCmdBtn[3] = m_rectCmdBtn[2] + irr::core::vector2di(32,0);

		int i;
		for (i = 0; i < 8; i++) {
			m_bDirCmd[i] = false;
		}
		for (i = 0; i < 4; i++) {
			m_bTrigerCmd[i] = false;
		}

	}

	inline virtual bool OnEvent(const irr::SEvent& event)
	{
		switch(event.EventType)
		{
		case irr::EET_GUI_EVENT:
			{

			}
			break;
		case irr::EET_KEY_INPUT_EVENT: {
			//printf("%d\n", event.KeyInput.Key);
			if (event.KeyInput.PressedDown) {
				m_KeyBuffer[event.KeyInput.Key] = true;
			} else {
				m_KeyBuffer[event.KeyInput.Key] = false;

			}
									   }
									   break;
		case irr::EET_MOUSE_INPUT_EVENT:
			{
				//printf("mouse event %d, %d,%d\n", event.MouseInput.ButtonStates,
				//	event.MouseInput.X, event.MouseInput.Y);
				int i;
				for (i = 0; i < 8; i++) {

					if (m_rectDirBtn[i].isPointInside(
						irr::core::vector2di(event.MouseInput.X,
						event.MouseInput.Y))
						&& event.MouseInput.isLeftPressed() == true) {
							m_bDirCmd[i] = true;
					} else {
						m_bDirCmd[i] = false;

					}
				}

				for(i=0;i<4;i++)
				{
					if (m_rectCmdBtn[i].isPointInside(
						irr::core::vector2di(event.MouseInput.X,
						event.MouseInput.Y))
						&& event.MouseInput.isLeftPressed() == true) {
							m_bTrigerCmd[i] = true;
					} else {
						m_bTrigerCmd[i] = false;

					}
				}

			}
			break;
		case irr::EET_USER_EVENT:
			break;
		default:
			break;
		}
		return false;
	}

	inline void DrawControlButton(irr::video::IVideoDriver *pVideo)
	{
		int i;
		for(i=0;i<8;i++)
		{
			if(m_bDirCmd[i] == true)
			{
				pVideo->draw2DRectangle(irr::video::SColor(255,255,255,0), m_rectDirBtn[i]);


			}
			else
			{
				pVideo->draw2DRectangleOutline(m_rectDirBtn[i]);
			}
		}

		for (i = 0; i < 4; i++) {
			if(m_bTrigerCmd[i] == true)
			{
				pVideo->draw2DRectangle(irr::video::SColor(255,255,255,0), m_rectCmdBtn[i]);
				//pVideo->draw2DRectangleOutline(m_rectCmdBtn[i]);
			}
			else
			{
				pVideo->draw2DRectangleOutline(m_rectCmdBtn[i]);
			}
		}

	}

};


#endif /* CSAMPLEAPP_H_ */