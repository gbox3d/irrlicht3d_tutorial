﻿#include <irrlicht.h>
#include <iostream>
#include <string>
#include <vector>

//기본 그리기 예제
namespace lsn02
{
	//삼각형 & 박스 그리기
	namespace _00
	{
		//기본 A2형
		//직접 그리기 프레임웍
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"Type-A2");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,0,-5), irr::core::vector3df(0,0,0));				

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();

				//직접 그리기
				{
					irr::core::matrix4 mat;//단위행렬로초기화
					mat.makeIdentity();
					pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

					irr::video::SMaterial m;
					m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.
					m.ZBuffer = false;

					pVideo->setMaterial(m);	
				}

				//직선그리기
				{
					pVideo->draw3DLine(
						irr::core::vector3df(0,0,0),
						irr::core::vector3df(-3,3,0)						
						);   
				}

				//삼각형그리기
				{
					irr::core::triangle3df tri(irr::core::vector3df(0,0,0),irr::core::vector3df(1,1,0),irr::core::vector3df(2,0,0));
					pVideo->draw3DTriangle(
						tri,
						irr::video::SColor(255,255,255,0)
						);
				}

				//박스 그리기
				{
					irr::core::aabbox3df box(irr::core::vector3df(-1,-1,-1),irr::core::vector3df(1,1,1));
					pVideo->draw3DBox(
						box,
						irr::video::SColor(255,255,0,255)
						);
				}

				pVideo->endScene();	
			}
			pDevice->drop();
		}
	}

	//triangle list
	namespace _01
	{	
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"Type-A2");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,0,-5), irr::core::vector3df(0,0,0));	

			
			irr::core::array<irr::video::S3DVertex> Vertices;
			//std::vector<irr::video::S3DVertex> Vertices;

			Vertices.push_back(irr::video::S3DVertex(-.5,-.5,0, 0,0,-1,irr::video::SColor(0,0,255,255),0,1) );
			Vertices.push_back(irr::video::S3DVertex(-.5,.5,0, 0,0,-1,irr::video::SColor(0,255,0,255),0,0) );
			Vertices.push_back(irr::video::S3DVertex(.5,.5,0, 0,0,-1,irr::video::SColor(0,255,255,0),1,0) );
			Vertices.push_back(irr::video::S3DVertex(.5,-.5,0, 0,0,-1,irr::video::SColor(0,0,255,0),1,1) );

			irr::core::array<irr::u16> Indice;
			//std::vector<irr::u16> Indice;

			 //0,1,2,3,0,2 
			Indice.push_back(0);
			Indice.push_back(1);
			Indice.push_back(2);
			Indice.push_back(3);
			Indice.push_back(0);
			Indice.push_back(2);


			while(pDevice->run())
			{	
				
				static irr::u32 uLastTick=0;
				//밀리세컨드값얻기
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();

				//직접 그리기
				{
					irr::core::matrix4 mat;//단위행렬로초기화
					mat.makeIdentity();
					pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

					irr::video::SMaterial m;
					m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.
					//m.ZBuffer = false;

					pVideo->setMaterial(m);	
				}

				pVideo->drawIndexedTriangleList((irr::video::S3DVertex *)(&Vertices[0]),
					4,
					(irr::u16 *)&Indice[0],
					2
					);				

				pVideo->endScene();	
			}
			pDevice->drop();
		}
	}

	//triangle fan
	namespace _02
	{	
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"Type-A2");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,0,-5), irr::core::vector3df(0,0,0));	

			std::vector<irr::video::S3DVertex> Vertices;

			Vertices.push_back(irr::video::S3DVertex(-.5,-.5,0, 0,0,-1,irr::video::SColor(0,0,255,255),0,1) );
			Vertices.push_back(irr::video::S3DVertex(-.5,.5,0, 0,0,-1,irr::video::SColor(0,255,0,255),0,0) );
			Vertices.push_back(irr::video::S3DVertex(.25,.25,0, 0,0,-1,irr::video::SColor(0,255,255,0),1,0) );
			Vertices.push_back(irr::video::S3DVertex(.5,-.5,0, 0,0,-1,irr::video::SColor(0,255,255,255),1,1) );
			Vertices.push_back(irr::video::S3DVertex(.25f,-1.f,0, 0,0,-1,irr::video::SColor(0,0,255,0),1,1) );

			std::vector<irr::u16> Indice;

			 //순서대로 인덱스 배열만들기
			irr::u32 i;
			for(i=0;i<Vertices.size();i++)
			{
				Indice.push_back(i);
			}			


			while(pDevice->run())
			{	
				
				static irr::u32 uLastTick=0;
				//밀리세컨드값얻기
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();

				//직접 그리기
				{
					irr::core::matrix4 mat;//단위행렬로초기화
					mat.makeIdentity();
					pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

					irr::video::SMaterial m;
					m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.
					m.ZBuffer = irr::video::ECFN_NEVER;
					//m.ZBuffer = false;

					pVideo->setMaterial(m);	
				}

				pVideo->drawIndexedTriangleFan((irr::video::S3DVertex *)(&Vertices[0]),
					5,
					(irr::u16 *)&Indice[0],
					3
					);				

				pVideo->endScene();	
			}
			pDevice->drop();
		}
	}

	
	//drawVertexPrimitiveList
	//EPT_LINE.....
	namespace _03
	{	
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"Type-A2");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,0,-5), irr::core::vector3df(0,0,0));	

			std::vector<irr::video::S3DVertex> Vertices;

			Vertices.push_back(irr::video::S3DVertex(-.5,-.5,0, 0,0,-1,irr::video::SColor(0,0,255,255),0,1) );
			Vertices.push_back(irr::video::S3DVertex(-.5,.5,0, 0,0,-1,irr::video::SColor(0,255,0,255),0,0) );
			Vertices.push_back(irr::video::S3DVertex(.5,.5,0, 0,0,-1,irr::video::SColor(0,255,255,0),1,0) );
			Vertices.push_back(irr::video::S3DVertex(.5,-.5,0, 0,0,-1,irr::video::SColor(0,0,255,0),1,1) );

			std::vector<irr::u16> Indice;

			 //인덱스 배열만들기
			Indice.push_back(0);
			Indice.push_back(1);
			Indice.push_back(2);
			Indice.push_back(3);

			/*

			1---------2
			|         |
			|         |
			|         |
			|         |
			0---------3
			
			*/
			//Indice.push_back(0);
			//Indice.push_back(2);


			while(pDevice->run())
			{	
				
				static irr::u32 uLastTick=0;
				//밀리세컨드값얻기
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();

				//직접 그리기
				{
					irr::core::matrix4 mat;//단위행렬로초기화
					mat.makeIdentity();
					mat.setTranslation(irr::core::vector3df(-2,0,0));
					pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

					irr::video::SMaterial m;
					m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.
					//m.ZBuffer = false;

					pVideo->setMaterial(m);	
				}

				pVideo->drawVertexPrimitiveList(
					(irr::video::S3DVertex *)(&Vertices[0]),
					4,
					(irr::u16 *)&Indice[0],
					3,
					irr::video::EVT_STANDARD,
					irr::scene::EPT_LINE_STRIP
					);				

				{
					irr::core::matrix4 mat;//단위행렬로초기화
					mat.makeIdentity();
					mat.setTranslation(irr::core::vector3df(0,0,0));
					pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화
				}

				pVideo->drawVertexPrimitiveList(
					(irr::video::S3DVertex *)(&Vertices[0]),
					4,
					(irr::u16 *)&Indice[0],
					3,
					irr::video::EVT_STANDARD,
					irr::scene::EPT_LINE_LOOP					
					);				

				{
					irr::core::matrix4 mat;//단위행렬로초기화
					mat.makeIdentity();
					mat.setTranslation(irr::core::vector3df(2,0,0));
					pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화
				}

				pVideo->drawVertexPrimitiveList(
					(irr::video::S3DVertex *)(&Vertices[0]),
					4,
					(irr::u16 *)&Indice[0],
					2,
					irr::video::EVT_STANDARD,
					irr::scene::EPT_LINES
					);				


				pVideo->endScene();	
			}
			pDevice->drop();
		}
	}

	
	
	/*
	drawVertexPrimitiveList 이용
	포인트 스프라이트 출력

	포인트 사이즈는 D3DRS_POINTSCALEENABLE 에 의해 결정한다. 이 값이 FALSE 로 설정되어 있는 경우는, 애플리케이션으로 지정된 사이즈가 스크린 공간 (변환 후) 사이즈로서 사용된다. 스크린 공간에서 Direct3D 에게 건네지는 정점은, 포인트 사이즈를 계산하지 않고, 지정된 포인트 사이즈가 스크린 공간의 사이즈로서 해석된다. 

D3DRS_POINTSCALEENABLE 가 TRUE 의 경우는, Direct3D 가, 다음의 식에 따라 스크린 공간의 포인트 사이즈를 계산한다. 애플리케이션으로 지정된 포인트 사이즈는, 카메라 공간 단위로 표현된다. 

S s = Vh * S i * sqrt(1/(A + B * D e + C *( D e2 )))

이 식안의 입력 포인트 사이즈 Si 는, 정점마다 값인가, 또는 D3DRS_POINTSIZE 렌더링 스테이트의 값이다. 포인트 스케일 계수 D3DRS_POINTSCALE_A ,D3DRS_POINTSCALE_B ,D3DRS_POINTSCALE_C 는, 포인트 A, B, 및 C 로 나타낸다. 뷰포트의 높이 V h 는, 뷰포트를 나타내는 D3DVIEWPORT9 구조체의 Height 멤버이다. 시점에서 위치 좌표에의 거리 (원점의 눈) De는, 포인트 (Xe, Ye, Ze)의 시점 공간 위치를 잡아, 다음의 처리를 실행하는 것에 의해 계산된다. 

D e = sqrt (Xe2 + Y e2 + Z e2)

최대 포인트 사이즈 Pmax 는,D3DCAPS9 구조체의 MaxPointSize 멤버 또는 D3DRS_POINTSIZE_MAX 렌더링 스테이트의 어느 쪽인지 작은 (분)편을 취해 결정된다. 최소 포인트 사이즈 Pmin 는,D3DRS_POINTSIZE_MIN 의 값을 조사하는 것에 의해 결정된다. 즉, 최종적인 스크린 공간 포인트 사이즈 S 는 다음의 규칙에 따라 결정된다. 

Ss > Pmax 의 경우는 S = P max
S < Pmin 의 경우는 S = P min
그 외의 경우는 S = S s
	*/
	namespace _04
	{	
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"Type-A2");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,0,-5), irr::core::vector3df(0,0,0));	

			std::vector<irr::video::S3DVertex> Vertices;

			Vertices.push_back(irr::video::S3DVertex(-.5,-.5,0, 0,0,-1,irr::video::SColor(0,0,255,255),0,1) );
			Vertices.push_back(irr::video::S3DVertex(-.5,.5,0, 0,0,-1,irr::video::SColor(0,255,0,255),0,0) );
			Vertices.push_back(irr::video::S3DVertex(.5,.5,0, 0,0,-1,irr::video::SColor(0,255,255,0),1,0) );
			Vertices.push_back(irr::video::S3DVertex(.5,-.5,0, 0,0,-1,irr::video::SColor(0,0,255,0),1,1) );

			std::vector<irr::u16> Indice;

			 //인덱스 배열만들기
			Indice.push_back(0);
			Indice.push_back(1);
			Indice.push_back(2);
			Indice.push_back(3);

			/*

			1---------2
			|         |
			|         |
			|         |
			|         |
			0---------3
			
			*/			


			while(pDevice->run())
			{	
				
				static irr::u32 uLastTick=0;
				//밀리세컨드값얻기
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();

				//직접 그리기
				{
					irr::core::matrix4 mat;//단위행렬로초기화
					mat.makeIdentity();
					mat.setTranslation(irr::core::vector3df(-2,0,0));
					pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

					irr::video::SMaterial m;
					m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.
					//m.ZBuffer = false;

					pVideo->setMaterial(m);	
				}

				pVideo->drawVertexPrimitiveList(
					(irr::video::S3DVertex *)(&Vertices[0]),
					4,
					(irr::u16 *)&Indice[0],
					3,
					irr::video::EVT_STANDARD,
					irr::scene::EPT_POINT_SPRITES
					);

				pVideo->endScene();	
			}
			pDevice->drop();
		}
	}


	//drawVertexPrimitiveList, EPT_TRIANGLE_....
	namespace _05
	{	
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"Type-A2");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,0,-5), irr::core::vector3df(0,0,0));	

			std::vector<irr::video::S3DVertex> Vertices;

			/*

			1---------2
			|         |
			|         |
			|         |
			|         |
			0---------3
			
			*/

			Vertices.push_back(irr::video::S3DVertex(-.5,-.5,0, 0,0,-1,irr::video::SColor(0,0,255,255),0,1) );
			Vertices.push_back(irr::video::S3DVertex(-.5,.5,0, 0,0,-1,irr::video::SColor(0,255,0,255),0,0) );
			Vertices.push_back(irr::video::S3DVertex(.5,.5,0, 0,0,-1,irr::video::SColor(0,255,255,0),1,0) );
			Vertices.push_back(irr::video::S3DVertex(.5,-.5,0, 0,0,-1,irr::video::SColor(0,0,255,0),1,1) );

			std::vector<irr::u16> Indice;

			 //인덱스 배열만들기
			Indice.push_back(0);
			Indice.push_back(1);
			Indice.push_back(2);
			Indice.push_back(3);


			std::vector<irr::u16> Indice4Strip;

			//인덱스 배열만들기
			//스트립의 특징은 다음에 올 정점이 이전 삼각형의 2,3번째 정점과 인점하게해야한다.
			//첫번째 정점이 새로 추가될삼각형에 포함되면 안된다.
			//2,3,번째 +  새로 추가될 삼각형 이런식이다.
			Indice4Strip.push_back(0);
			Indice4Strip.push_back(1);
			Indice4Strip.push_back(3);
			Indice4Strip.push_back(2);

			std::vector<irr::u16> Indice4TriList;

			 //인덱스 배열만들기
			Indice4TriList.push_back(0);
			Indice4TriList.push_back(1);
			Indice4TriList.push_back(2);
			Indice4TriList.push_back(2);
			Indice4TriList.push_back(3);
			Indice4TriList.push_back(0);		

			

			while(pDevice->run())
			{	
				
				static irr::u32 uLastTick=0;
				//밀리세컨드값얻기
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();

				//EPT_TRIANGLE_FAN 그리기
				{
					irr::core::matrix4 mat;//단위행렬로초기화
					mat.makeIdentity();
					mat.setTranslation(irr::core::vector3df(-2,0,0));
					pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

					irr::video::SMaterial m;
					m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.
					//m.ZBuffer = false;
					//m.setTexture(TextureLayer[0].Texture = 

					pVideo->setMaterial(m);	
				}				

				pVideo->drawVertexPrimitiveList(
					(irr::video::S3DVertex *)(&Vertices[0]),
					4,
					(irr::u16 *)&Indice[0],
					2,
					irr::video::EVT_STANDARD,
					irr::scene::EPT_TRIANGLE_FAN
					);

				//EPT_TRIANGLE_STRIP 그리기
				{
					irr::core::matrix4 mat;//단위행렬로초기화
					mat.makeIdentity();
					mat.setTranslation(irr::core::vector3df(0,0,0));
					pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

					irr::video::SMaterial m;
					m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.
					//m.ZBuffer = false;

					pVideo->setMaterial(m);	
				}

				pVideo->drawVertexPrimitiveList(
					(irr::video::S3DVertex *)(&Vertices[0]),
					4,
					(irr::u16 *)&Indice4Strip[0],
					2,
					irr::video::EVT_STANDARD,
					irr::scene::EPT_TRIANGLE_STRIP
					);

				//EPT_TRIANGLES 그리기
				{
					irr::core::matrix4 mat;//단위행렬로초기화
					mat.makeIdentity();
					mat.setTranslation(irr::core::vector3df(2,0,0));
					pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

					irr::video::SMaterial m;
					m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.
					//m.ZBuffer = false;

					pVideo->setMaterial(m);	
				}

				pVideo->drawVertexPrimitiveList(
					(irr::video::S3DVertex *)(&Vertices[0]),
					4,
					(irr::u16 *)&Indice4TriList[0],
					2,
					irr::video::EVT_STANDARD,
					irr::scene::EPT_TRIANGLES
					);

				pVideo->endScene();	
			}
			pDevice->drop();
		}
	}
}