#include <irrlicht.h>
#include <iostream>

namespace lsn03
{
    
    //2d 벡터 예제
    
	namespace _50
	{
        
		void main()
		{
            irr::core::vector2df vect1(1,0);
            
            vect1.normalize();
            
            std::cout << vect1.getAngle() << std::endl;
            
            vect1 = irr::core::vector2df(0,-1);
            vect1.normalize();
            std::cout << vect1.getAngle() << std::endl;
            
            vect1 = irr::core::vector2df(-1,0);
            vect1.normalize();
            std::cout << vect1.getAngle() << std::endl;
            
            vect1 = irr::core::vector2df(0,1);
            vect1.normalize();
            std::cout << vect1.getAngle() << std::endl;
            
            
			
		}
	}
	
}


