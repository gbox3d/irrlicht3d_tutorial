﻿#include <irrlicht.h>
#include <iostream>

//#include <windows.h>

//메쉬 관련예제
namespace lsn04 {

	//박스,구 노드 출력하기 	
	namespace _00 {

		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(irr::video::EDT_OPENGL);

			//창이름정하기
			pDevice->setWindowCaption(L"sample 4-1");
			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();			
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();			
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,30,-40), irr::core::vector3df(0,5,0));

			{
				irr::scene::ISceneNode *pNode =  pSmgr->addCubeSceneNode();				

				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setMaterialTexture(0,pVideo->getTexture("t351sml.jpg"));					
			}

			{
				irr::scene::IMeshSceneNode *pNode =  (irr::scene::IMeshSceneNode *)pSmgr->addSphereSceneNode();

				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setMaterialTexture(0,pVideo->getTexture("earth.bmp"));	

				pNode->setPosition(irr::core::vector3df(15,0,0));
			}			

			//pSmgr->addToDeletionQueue(

			while(pDevice->run())
			{		
				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));		
				pSmgr->drawAll();
				pGuiEnv->drawAll();			
				pVideo->endScene();						
			}	

			pDevice->drop();
		}
	}


	//화살표 메쉬만들어서 노드화 시켜 출력하기
	namespace _01 {		
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,5,-10), irr::core::vector3df(0,0,0));

			{				
				pSmgr->addArrowMesh("usr/mesh/arrow"); //메쉬등록			
			}

			{				
				irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(
					pSmgr->getMesh("usr/mesh/arrow"));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);				
				pNode->setPosition(irr::core::vector3df(0,0,0));
			}
			{				
				irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(
					pSmgr->getMesh("usr/mesh/arrow"));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);				
				pNode->setPosition(irr::core::vector3df(5,0,0));
				pNode->setRotation(irr::core::vector3df(0,0,-90));
			}

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);


			while(pDevice->run())
			{	
				static irr::u32 uLastTick = pDevice->getTimer()->getTime();
				irr::u32 uTick = pDevice->getTimer()->getTime();
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = pDevice->getTimer()->getTime();

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}


	//GeometryCreator로 메쉬 만들어서 씬노드로 등록시키기 샘플
	namespace _02 {		

		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"4-2. GeometryCreator sample");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,5,-10), irr::core::vector3df(0,0,0));

			//콘 메쉬
			{				
				irr::scene::IMesh *pMesh = pSmgr->getGeometryCreator()->createConeMesh(2,3,6);
				irr::scene::SAnimatedMesh *pAniMesh = new irr::scene::SAnimatedMesh(pMesh);								
				pMesh->drop();
				pAniMesh->recalculateBoundingBox();
				pSmgr->getMeshCache()->addMesh("usr/mesh/cone",pAniMesh); //씬메니져에 등록
				pAniMesh->drop();
			}

			//씬노드 등록
			{				
				irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(
					pSmgr->getMesh("usr/mesh/cone"));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);				
				pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);				
				pNode->setPosition(irr::core::vector3df(0,0,0));
			}

			//실린더 메쉬만들기
			{				
				irr::scene::IMesh *pMesh = pSmgr->getGeometryCreator()->createCylinderMesh(2,3,8,irr::video::SColor(255,255,0,0));
				irr::scene::SAnimatedMesh *pAniMesh = new irr::scene::SAnimatedMesh(pMesh);								
				pMesh->drop();
				pAniMesh->recalculateBoundingBox();
				pSmgr->getMeshCache()->addMesh("usr/mesh/cylinder",pAniMesh); //씬메니져에 등록
				pAniMesh->drop();
			}			

			//씬노드 등록
			{				
				irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(
					pSmgr->getMesh("usr/mesh/cylinder"));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);				
				pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);				
				pNode->setPosition(irr::core::vector3df(3,0,0));
			}


			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);


			while(pDevice->run())
			{	
				static irr::u32 uLastTick = pDevice->getTimer()->getTime();
				irr::u32 uTick = pDevice->getTimer()->getTime();
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = pDevice->getTimer()->getTime();

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}


	//GeometryCreator로 IMesh 만들고
	//getMeshManipulator의 createAnimatedMesh로 IAnimateMesh만들기 샘플
	namespace _03 {		

		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"4-3.");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,5,-10), irr::core::vector3df(0,0,0));


			//실린더 메쉬만들기
			{				
				irr::scene::IMesh *pMesh = pSmgr->getGeometryCreator()->createCylinderMesh(2,3,8);				
				irr::scene::IAnimatedMesh *pAniMesh = pSmgr->getMeshManipulator()->createAnimatedMesh(pMesh);
				pMesh->drop();				
				pSmgr->getMeshCache()->addMesh("usr/mesh/cylinder",pAniMesh); //씬메니져에 등록
				pAniMesh->drop();
			}			

			//씬노드 등록
			{				
				irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(
					pSmgr->getMesh("usr/mesh/cylinder"));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);				
				pNode->setPosition(irr::core::vector3df(0,0,0));
				pNode->setMaterialTexture(0,pVideo->getTexture("stones.jpg"));
				pNode->setDebugDataVisible(irr::scene::EDS_FULL);				
			}




			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);


			while(pDevice->run())
			{	
				static irr::u32 uLastTick = pDevice->getTimer()->getTime();
				irr::u32 uTick = pDevice->getTimer()->getTime();
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = pDevice->getTimer()->getTime();

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}

		//getMeshManipulator이용 해서 메쉬의 버텍스 컬러바꾸기
		namespace _01 {
			void main()
			{
				irr::IrrlichtDevice *pDevice = irr::createDevice(					
					irr::video::EDT_OPENGL
					);

				pDevice->setWindowCaption(L"4-3.");

				irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
				irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
				irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
				pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

				pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,5,-40), irr::core::vector3df(0,0,0));


				//getGeometryCreator 이용해서 큐브 메시생성
				{				
					irr::scene::IMesh *pMesh = pSmgr->getGeometryCreator()->createCubeMesh();
					//색바꾸기
					pSmgr->getMeshManipulator()->setVertexColors(pMesh,irr::video::SColor(255,255,0,0));
					
					irr::scene::IAnimatedMesh *pAniMesh = pSmgr->getMeshManipulator()->createAnimatedMesh(pMesh);
					pMesh->drop();				
					pSmgr->getMeshCache()->addMesh("usr/mesh/cube/red",pAniMesh); //씬메니져에 등록
					pAniMesh->drop();
				}			

				//getGeometryCreator 이용해서 큐브 메시생성
				{				
					irr::scene::IMesh *pMesh = pSmgr->getGeometryCreator()->createCubeMesh();
					//색바꾸기
					pSmgr->getMeshManipulator()->setVertexColors(pMesh,irr::video::SColor(255,0,255,0));
					
					irr::scene::IAnimatedMesh *pAniMesh = pSmgr->getMeshManipulator()->createAnimatedMesh(pMesh);
					pMesh->drop();				
					pSmgr->getMeshCache()->addMesh("usr/mesh/cube/blue",pAniMesh); //씬메니져에 등록
					pAniMesh->drop();
				}		

				//getGeometryCreator 이용해서 큐브 메시생성
				{				
					irr::scene::IMesh *pMesh = pSmgr->getGeometryCreator()->createCubeMesh();
					//색바꾸기
					pSmgr->getMeshManipulator()->setVertexColors(pMesh,irr::video::SColor(255,0,0,255));
					
					irr::scene::IAnimatedMesh *pAniMesh = pSmgr->getMeshManipulator()->createAnimatedMesh(pMesh);
					pMesh->drop();				
					pSmgr->getMeshCache()->addMesh("usr/mesh/cube/green",pAniMesh); //씬메니져에 등록
					pAniMesh->drop();
				}		



				//씬노드 등록
				{				
					irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(
						pSmgr->getMesh("usr/mesh/cube/red"));
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);				
					pNode->setPosition(irr::core::vector3df(0,0,0));
					//pNode->setMaterialTexture(0,pVideo->getTexture("stones.jpg"));
					//pNode->setDebugDataVisible(irr::scene::EDS_FULL);
				}

					//씬노드 등록
				{				
					irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(
						pSmgr->getMesh("usr/mesh/cube/blue"));
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);				
					pNode->setPosition(irr::core::vector3df(-15,0,0));
					//pNode->setMaterialTexture(0,pVideo->getTexture("stones.jpg"));
					//pNode->setDebugDataVisible(irr::scene::EDS_FULL);
				}

				
				//옥트리  등록(씬노드의 버텍스 컬러 바꾸기)
				{	
					irr::scene::IMeshSceneNode *pNode = pSmgr->addOctreeSceneNode(
						pSmgr->getMesh("usr/mesh/cube/green"));		

					pSmgr->getMeshManipulator()->setVertexColors(
						pNode->getMesh(),
						irr::video::SColor(255,255,0,0));
					pNode->setMesh(pNode->getMesh());//옥트리 재구성 

					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);				
					pNode->setPosition(irr::core::vector3df(15,0,0));				
					
				}

				//프레임 레이트 표시용 유아이
				irr::gui::IGUIStaticText *pstextFPS = 
					pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);


				while(pDevice->run())
				{	
					static irr::u32 uLastTick = pDevice->getTimer()->getTime();
					irr::u32 uTick = pDevice->getTimer()->getTime();
					irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
					uLastTick = pDevice->getTimer()->getTime();

					//프레임레이트 갱신, 삼각형수 표시
					{
						wchar_t wszbuf[256];
						swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
						pstextFPS->setText(wszbuf);
					}

					pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

					pSmgr->drawAll();
					pGuiEnv->drawAll();		

					pVideo->endScene();	
				}

				pDevice->drop();
			}

		}
	}


	//힐플래인 메쉬 만들기
	namespace _04 {				
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"4-4.");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,5,-10), irr::core::vector3df(0,0,0));		

			//힐플래인 메쉬 추가
			{
				irr::scene::IAnimatedMesh *pMesh =
					pSmgr->addHillPlaneMesh("usr/mesh/myhill",
					irr::core::dimension2d<irr::f32>(8,8),
					irr::core::dimension2d<irr::u32>(8,8),
					0,0,
					irr::core::dimension2d<irr::f32>(0,0),
					irr::core::dimension2d<irr::f32>(8,8)
					);
			}

			{
				irr::scene::IAnimatedMeshSceneNode *pNode =
					pSmgr->addAnimatedMeshSceneNode(pSmgr->getMesh("usr/mesh/myhill"));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setMaterialTexture(0,pVideo->getTexture("wall.jpg"));

				pNode->setName("usr/scene/hill1");
			}


			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick = pDevice->getTimer()->getTime();
				irr::u32 uTick = pDevice->getTimer()->getTime();
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = pDevice->getTimer()->getTime();

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}

	//정적모델 로딩
	//모델파일에서 읽기
	namespace _05 {				
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"4-5");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,250,-610), irr::core::vector3df(0,200,0));		

			{
				irr::scene::IAnimatedMesh *pAniMesh = pSmgr->getMesh("jga/buildings_by_km_2/building_008.x");
				irr::scene::IMesh *pMesh = pAniMesh->getMesh(0);
				irr::scene::IMeshSceneNode *pNode = pSmgr->addMeshSceneNode(pMesh);				

				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setMaterialTexture(0,pVideo->getTexture("jga/buildings_by_km_2/building_008tex.jpg"));			

			}		

			/*{
			irr::scene::IMesh *pMesh = 
			pSmgr->getMesh("jga/swatcharacter/swat_1a_mako.X")->getMesh(0);
			irr::scene::IMeshSceneNode *pNode = pSmgr->addMeshSceneNode(pMesh);
			pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);

			}*/

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick = pDevice->getTimer()->getTime();
				irr::u32 uTick = pDevice->getTimer()->getTime();
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = pDevice->getTimer()->getTime();

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}


	//메쉬 직접 만들기
	namespace _06 {				
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"4-5");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,2.5,-10), irr::core::vector3df(0,2,0));		

			//삼각형 두개로 평면 만들기
			{
				//irr::scene::IMesh *pMesh;

				{				
					irr::video::S3DVertex v;
					irr::scene::SMeshBuffer* buffer = new irr::scene::SMeshBuffer();

					v.Pos.X = -5.f;
					v.Pos.Y = 5.f;
					v.Pos.Z = 0;
					v.TCoords.X = 0;
					v.TCoords.Y = 0;
					buffer->Vertices.push_back(v);

					v.Pos.X = 5.f;
					v.Pos.Y = 5.f;
					v.Pos.Z = 0;
					v.TCoords.X = 1;
					v.TCoords.Y = 0;
					buffer->Vertices.push_back(v);

					v.Pos.X = 5.f;
					v.Pos.Y = -5.f;
					v.Pos.Z = 0;
					v.TCoords.X = 1;
					v.TCoords.Y = 1;
					buffer->Vertices.push_back(v);

					v.Pos.X = -5.f;
					v.Pos.Y = -5.f;
					v.Pos.Z = 0;
					v.TCoords.X = 0;
					v.TCoords.Y = 1;
					buffer->Vertices.push_back(v);

					//삼각형리스트
					//시계방향으로 감아주기
					buffer->Indices.push_back(0);
					buffer->Indices.push_back(1);
					buffer->Indices.push_back(2);

					buffer->Indices.push_back(3);
					buffer->Indices.push_back(0);
					buffer->Indices.push_back(2);

					buffer->recalculateBoundingBox();

					//메쉬는 다수의 메쉬버퍼를 가진다.
					irr::scene::SMesh* mesh = new irr::scene::SMesh();
					mesh->addMeshBuffer(buffer);
					mesh->recalculateBoundingBox();
					buffer->drop();

					//SMesh는 IMesh를 실제 구현한것이다.
					//pMesh = mesh; 					

					//메쉬를 씬메니져에 등록하기(리소스 관리자)
					irr::scene::IAnimatedMesh *pAniMesh = pSmgr->getMeshManipulator()->createAnimatedMesh(mesh);					
					mesh->drop();
					pSmgr->getMeshCache()->addMesh("usr/mesh/plane",pAniMesh); //씬메니져에 등록
					pAniMesh->drop();

				}				

				irr::scene::IMeshSceneNode *pNode = pSmgr->addMeshSceneNode(pSmgr->getMesh("usr/mesh/plane"));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setMaterialTexture(0,pVideo->getTexture("jga/박예진.jpg"));
			}


			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick = pDevice->getTimer()->getTime();
				irr::u32 uTick = pDevice->getTimer()->getTime();
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = pDevice->getTimer()->getTime();


				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}



	//메쉬 직접 만들어 저장하기
	namespace _07 {				
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"4-5");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,2.5,-10), irr::core::vector3df(0,2,0));		

			//irr::scene::IMesh *pMesh;
			{			

				{				
					irr::video::S3DVertex v;
					irr::scene::SMeshBuffer* buffer = new irr::scene::SMeshBuffer();

					v.Pos.X = -5.f;
					v.Pos.Y = 5.f;
					v.Pos.Z = 0;
					v.TCoords.X = 0;
					v.TCoords.Y = 0;
					buffer->Vertices.push_back(v);

					v.Pos.X = 5.f;
					v.Pos.Y = 5.f;
					v.Pos.Z = 0;
					v.TCoords.X = 1;
					v.TCoords.Y = 0;
					buffer->Vertices.push_back(v);

					v.Pos.X = 5.f;
					v.Pos.Y = -5.f;
					v.Pos.Z = 0;
					v.TCoords.X = 1;
					v.TCoords.Y = 1;
					buffer->Vertices.push_back(v);

					v.Pos.X = -5.f;
					v.Pos.Y = -5.f;
					v.Pos.Z = 0;
					v.TCoords.X = 0;
					v.TCoords.Y = 1;
					buffer->Vertices.push_back(v);

					//삼각형리스트
					//시계방향으로 감아주기
					buffer->Indices.push_back(0);
					buffer->Indices.push_back(1);
					buffer->Indices.push_back(2);

					buffer->Indices.push_back(3);
					buffer->Indices.push_back(0);
					buffer->Indices.push_back(2);

					buffer->recalculateBoundingBox();

					//메쉬는 다수의 메쉬버퍼를 가진다. SMesh는 IMesh를 실제 구현한것이다.
					irr::scene::SMesh* mesh = new irr::scene::SMesh();
					mesh->addMeshBuffer(buffer);
					mesh->recalculateBoundingBox();
					buffer->drop();


					//메쉬를 씬메니져에 등록하기(리소스 관리자)
					irr::scene::IAnimatedMesh *pAniMesh = pSmgr->getMeshManipulator()->createAnimatedMesh(mesh);					
					mesh->drop();
					pSmgr->getMeshCache()->addMesh("usr/mesh/plane",pAniMesh); //씬메니져에 등록
					pAniMesh->drop();

				}				

			}

			{
				irr::scene::IMeshSceneNode *pNode = pSmgr->addMeshSceneNode(pSmgr->getMesh("usr/mesh/plane"));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setMaterialTexture(0,pVideo->getTexture("jga/박예진.jpg"));
			}

			{
				irr::scene::IMeshWriter	* mw = pSmgr->createMeshWriter(irr::scene::EMWT_COLLADA);
				irr::io::IWriteFile* file = pDevice->getFileSystem()->createAndWriteFile("../bin/test.dea");
				mw->writeMesh(file, pSmgr->getMesh("usr/mesh/plane"));
				mw->drop();
				file->drop();

				mw = pSmgr->createMeshWriter(irr::scene::EMWT_IRR_MESH);
				file = pDevice->getFileSystem()->createAndWriteFile("../bin/test.irrmesh");
				mw->writeMesh(file, pSmgr->getMesh("usr/mesh/plane"));				
				mw->drop();
				file->drop();

				mw = pSmgr->createMeshWriter(irr::scene::EMWT_OBJ);
				file = pDevice->getFileSystem()->createAndWriteFile("../bin/test.obj");
				mw->writeMesh(file, pSmgr->getMesh("usr/mesh/plane"));				
				mw->drop();
				file->drop();

				mw = pSmgr->createMeshWriter(irr::scene::EMWT_PLY);
				file = pDevice->getFileSystem()->createAndWriteFile("../bin/test.ply");
				mw->writeMesh(file, pSmgr->getMesh("usr/mesh/plane"));				
				mw->drop();
				file->drop();

				mw = pSmgr->createMeshWriter(irr::scene::EMWT_STL);
				file = pDevice->getFileSystem()->createAndWriteFile("../bin/test.stl");
				mw->writeMesh(file, pSmgr->getMesh("usr/mesh/plane"));				
				mw->drop();
				file->drop();
			}


			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick = pDevice->getTimer()->getTime();
				irr::u32 uTick = pDevice->getTimer()->getTime();
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = pDevice->getTimer()->getTime();


				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}


	//메쉬 직접 만들어 drawMeshBuffer 함수로 직접출력하기
	namespace _08 {				
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"4-5");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,2.5,-10), irr::core::vector3df(0,2,0));		

			irr::scene::SMesh* pMesh;
			//삼각형 두개로 평면 만들기
			{				
				{				
					irr::video::S3DVertex v;
					irr::scene::SMeshBuffer* buffer = new irr::scene::SMeshBuffer();

					v.Pos.X = -5.f;
					v.Pos.Y = 5.f;
					v.Pos.Z = 0;
					v.TCoords.X = 0;
					v.TCoords.Y = 0;
					buffer->Vertices.push_back(v);

					v.Pos.X = 5.f;
					v.Pos.Y = 5.f;
					v.Pos.Z = 0;
					v.TCoords.X = 1;
					v.TCoords.Y = 0;
					buffer->Vertices.push_back(v);

					v.Pos.X = 5.f;
					v.Pos.Y = -5.f;
					v.Pos.Z = 0;
					v.TCoords.X = 1;
					v.TCoords.Y = 1;
					buffer->Vertices.push_back(v);

					v.Pos.X = -5.f;
					v.Pos.Y = -5.f;
					v.Pos.Z = 0;
					v.TCoords.X = 0;
					v.TCoords.Y = 1;
					buffer->Vertices.push_back(v);

					//삼각형리스트
					//시계방향으로 감아주기
					buffer->Indices.push_back(0);
					buffer->Indices.push_back(1);
					buffer->Indices.push_back(2);

					buffer->Indices.push_back(3);
					buffer->Indices.push_back(0);
					buffer->Indices.push_back(2);

					buffer->recalculateBoundingBox();

					//재질설정
					irr::video::SMaterial m;
					m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.
					m.Wireframe = true;
					buffer->Material = m;

					//buffer->setHardwareMappingHint(irr::scene::EHM_STATIC);

					
					//SMesh는 IMesh를 실제 구현한것이다.
					pMesh = new irr::scene::SMesh();
					//메쉬는 다수의 메쉬버퍼를 가진다.
					pMesh->addMeshBuffer(buffer);
					pMesh->recalculateBoundingBox();
					buffer->drop();
				}		
			}


			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick = pDevice->getTimer()->getTime();
				irr::u32 uTick = pDevice->getTimer()->getTime();
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = pDevice->getTimer()->getTime();


				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				//변환초기화
				{
					//월드변환
					irr::core::matrix4 mat;//단위행렬로초기화
					mat.makeIdentity();					
					pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화
				}				

				pVideo->setMaterial(pMesh->getMeshBuffer(0)->getMaterial()); //재질설정
				pVideo->drawMeshBuffer(pMesh->getMeshBuffer(0)); //메쉬그리기


				pVideo->endScene();	
			}

			pMesh->drop(); //씬매니져에 등록시키지않았으므로 직접 해제해야한다.
			pDevice->drop();
		}
	}



	//대용량 메쉬 출력위한 하드웨어 버퍼 사용하기
	//EHM_DYNAMIC
	namespace _09 {				
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"4-4.");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,5,-10), irr::core::vector3df(0,0,0));		

			//힐플래인 메쉬 추가
			{
				irr::scene::IAnimatedMesh *pMesh =
					pSmgr->addHillPlaneMesh("usr/mesh/myhill",
					irr::core::dimension2d<irr::f32>(8,8),
					irr::core::dimension2d<irr::u32>(256,128)
					
					);				
			}

			{
				irr::scene::IAnimatedMeshSceneNode *pNode =
					pSmgr->addAnimatedMeshSceneNode(pSmgr->getMesh("usr/mesh/myhill"));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setMaterialTexture(0,pVideo->getTexture("wall.jpg"));

				pNode->setName("usr/scene/hill1");
			}


			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick = pDevice->getTimer()->getTime();
				irr::u32 uTick = pDevice->getTimer()->getTime();
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = pDevice->getTimer()->getTime();

                /*
				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				if( (GetAsyncKeyState('1') & 0x8001) == 0x8001)
				{
					pVideo->removeAllHardwareBuffers();
					pSmgr->getMesh("usr/mesh/myhill")->setHardwareMappingHint(irr::scene::EHM_NEVER);
					std::cout << "EHM_NEVER" << std::endl;
				}
				if( (GetAsyncKeyState('2') & 0x8001) == 0x8001)
				{
					pVideo->removeAllHardwareBuffers();
					pSmgr->getMesh("usr/mesh/myhill")->setHardwareMappingHint(irr::scene::EHM_DYNAMIC);
					std::cout << "EHM_DYNAMIC" << std::endl;
				}
				if( (GetAsyncKeyState('3') & 0x8001) == 0x8001)
				{					
					pVideo->removeAllHardwareBuffers();					
					pSmgr->getMesh("usr/mesh/myhill")->setHardwareMappingHint(irr::scene::EHM_STATIC);
					std::cout << "EHM_STATIC" << std::endl;
				}
				if( (GetAsyncKeyState('4') & 0x8001) == 0x8001)
				{
					pVideo->removeAllHardwareBuffers();
					pSmgr->getMesh("usr/mesh/myhill")->setHardwareMappingHint(irr::scene::EHM_STREAM);
					std::cout << "EHM_STREAM" << std::endl;
				}
                 */


				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}
}




