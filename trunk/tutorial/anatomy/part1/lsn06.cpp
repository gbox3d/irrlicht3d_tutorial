﻿//#include "part1.h"
//씬그래프 계층구조 이해

#include <irrlicht.h>
#include <iostream>


//#include <SDKDDKVer.h>
//#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
//#include <windows.h>

//stl header
//#include <vector>
//#include <map>

void Lession06()
{
	irr::IrrlichtDevice *pDevice = irr::createDevice(					
			irr::video::EDT_OPENGL,
#if(IRRLICHT_VERSION_MAJOR == 1 && IRRLICHT_VERSION_MINOR >= 6 )
		irr::core::dimension2d<irr::u32>(640, 480), 
#else
		irr::core::dimension2d<irr::s32>(640, 480), 
#endif
			32,
			false, false, true,
			NULL
			);

	pDevice->setWindowCaption(L"lesson 06");

	irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
	irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
	irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

	irr::scene::ISceneNode *pCam = pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,30,-40), irr::core::vector3df(0,5,0));


	irr::scene::ISceneNode *pNode = pSmgr->addCubeSceneNode();	
	pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
	pNode->setMaterialTexture(0,pVideo->getTexture("../res/irr_exam/t351sml.jpg"));

	//pCam->setParent(pNode);

	pNode->setPosition(irr::core::vector3df(-3,0,0));
	irr::scene::ISceneNode *pNode2 = pSmgr->addCubeSceneNode();
	//assert(pNode2);
	pNode2->setMaterialFlag(irr::video::EMF_LIGHTING,false);
	pNode2->setMaterialTexture(0,pVideo->getTexture("../res/irr_exam/t351sml.jpg"));	
	pNode2->setPosition(irr::core::vector3df(15,0,0));

	irr::scene::IAnimatedMesh *pAniMesh =  pSmgr->addArrowMesh("arrow",
			irr::video::SColor(255,255,0,0),
			irr::video::SColor(255,0,0,255),
			4,8,6,4,3,3			
			);

	irr::scene::ISceneNode *pNode3 = pSmgr->addEmptySceneNode();
	irr::scene::ISceneNode *pNode4 = pSmgr->addEmptySceneNode();

	irr::scene::IAnimatedMeshSceneNode *pNode5 = pSmgr->addAnimatedMeshSceneNode(pAniMesh);
	irr::scene::IAnimatedMeshSceneNode *pNode6 = pSmgr->addAnimatedMeshSceneNode(pAniMesh);	

	pNode5->setPosition(irr::core::vector3df(0,20,0));
	pNode5->setMaterialFlag(irr::video::EMF_LIGHTING,false);
	pNode6->setPosition(irr::core::vector3df(10,0,0));
	pNode6->setMaterialFlag(irr::video::EMF_LIGHTING,false);

	//첫번째구룹
	pNode3->setParent(pNode);
	pNode2->setParent(pNode3);	

	//두번째구룹
	pNode4->setParent(pNode5);
	pNode6->setParent(pNode4);

	printf("%f,%f %f \n",
			pNode2->getAbsoluteTransformation().getTranslation().X,
			pNode2->getRelativeTransformation().getTranslation().X,
			pNode2->getAbsolutePosition().X
			);	

	pNode->OnAnimate(0); //계층적 변환 업데이트	
	
	pNode4->OnAnimate(0);

	printf("%f,%f %f \n",
			pNode2->getAbsoluteTransformation().getTranslation().X,
			pNode2->getRelativeTransformation().getTranslation().X,
			pNode2->getAbsolutePosition().X
			);	

	irr::f32 fRotX = 0.f;
	irr::f32 fRotY = 0.f;

	irr::u32 uLastTick = pDevice->getTimer()->getTime();	

	while(pDevice->run())
	{	
		
		irr::u32 uTick = pDevice->getTimer()->getTime();

		irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f;		

		fRotX += (fDelta * 45.f);				
		fRotY += (fDelta * 90.f);				

		
		pNode->setRotation(irr::core::vector3df(fRotX,fRotY,0));
		

		pNode5->setRotation(irr::core::vector3df(fRotX,0,0));
		pNode4->setRotation(irr::core::vector3df(0,fRotY,0));
		

		uLastTick = pDevice->getTimer()->getTime();

		pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));	
		
		pSmgr->drawAll();
		pGuiEnv->drawAll();			
		
		pVideo->endScene();	

		pDevice->getCursorControl()->setPosition(irr::core::position2di(100,100));
	}

	pDevice->drop();
}


//SceneManaging system 변환계층구조 예제
//엔진 변환 시스템 예제
namespace lsn06
{
	//씬노드 없이 메쉬에 변환 직접적용
	namespace _00
	{		
		//메트릭스를 각도로
		//다시 각도에서 메트릭스로
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,50,-220), irr::core::vector3df(0,0,0));

			/*{
				pSmgr->addArrowMesh("usr/mesh/arrow/red",
					irr::video::SColor(255,255,0,0),
					irr::video::SColor(255,255,0,0)
					);
			}*/

			{
				irr::core::matrix4 mat;
				mat.setRotationDegrees(irr::core::vector3df(90,0,0));
				pSmgr->getMeshManipulator()->transformMesh(pSmgr->getMesh("jga/sign.3DS"),mat);
			}

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			const int max_obj=2;
			irr::core::matrix4 matWorld[max_obj],matWorld_scl[max_obj],matWorld_trn[max_obj],matWorld_rot[max_obj];
			
			

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				{
					//월드변환
					{
						int iobj = 0;


						matWorld_trn[iobj].setTranslation(irr::core::vector3df(0,0,0));

						irr::core::matrix4 matRdiff;
						matRdiff.setRotationDegrees(irr::core::vector3df(0,45*fDelta,0));

						matWorld_rot[iobj] *= matRdiff;

						/*matWorld_rot.setRotationDegrees(irr::core::vector3df(0,45,0));
						matWorld_scl.setScale(irr::core::vector3df(150,1,150));*/

						matWorld[iobj] = matWorld_trn[iobj] * matWorld_rot[iobj] * matWorld_scl[iobj];

						pVideo->setTransform(
							irr::video::ETS_WORLD,
							matWorld[iobj]
							); 
					}					

					//메트리얼설정
					{					
						irr::video::SMaterial m;
						m.Lighting = false; //라이트를꺼야색이제데로나온다.
						//m.Wireframe = true;		
						m.setTexture(0,pVideo->getTexture("jga/sign.tga"));
						pVideo->setMaterial(m);
					}

					irr::scene::IAnimatedMesh* pMesh;
					pMesh = pSmgr->getMesh("jga/sign.3DS");


					irr::u32 i;
					for(i=0;i<pMesh->getMeshBufferCount();i++)
					{
						pVideo->drawMeshBuffer(pMesh->getMeshBuffer(i));
					}
				}

				{
					//월드변환
					{
						int iobj = 1;

						matWorld_trn[iobj].setTranslation(irr::core::vector3df(0,0,0));						

						//메트릭스를 각도로 다시 메트릭스로
						irr::core::vector3df vRot = matWorld_rot[0].getRotationDegrees();
						matWorld_rot[iobj].setRotationDegrees(vRot);

						std::cout << vRot.Y << std::endl;												

						matWorld[iobj] = matWorld_trn[iobj] * matWorld_rot[iobj] * matWorld_scl[iobj];

						pVideo->setTransform(
							irr::video::ETS_WORLD,
							matWorld[iobj]
							); 
					}

					//메트리얼설정
					{					
						irr::video::SMaterial m;
						m.Lighting = false; //라이트를꺼야색이제데로나온다.
						//m.Wireframe = true;		
						m.setTexture(0,pVideo->getTexture("jga/sign.tga"));
						pVideo->setMaterial(m);
					}

					irr::scene::IAnimatedMesh* pMesh;
					pMesh = pSmgr->getMesh("jga/sign.3DS");


					irr::u32 i;
					for(i=0;i<pMesh->getMeshBufferCount();i++)
					{
						pVideo->drawMeshBuffer(pMesh->getMeshBuffer(i));
					}
				}




				pVideo->endScene();	
			}

			pDevice->drop();
		}	
	}
	
	
	//씬노드없이 메쉬에 직접 변환적용 쿼터니온 적용예
	namespace _01
	{		
		//기본 A1형
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,50,-220), irr::core::vector3df(0,0,0));

			/*{
				pSmgr->addArrowMesh("usr/mesh/arrow/red",
					irr::video::SColor(255,255,0,0),
					irr::video::SColor(255,255,0,0)
					);
			}*/

			{
				irr::core::matrix4 mat;
				mat.setRotationDegrees(irr::core::vector3df(90,0,0));
				pSmgr->getMeshManipulator()->transformMesh(pSmgr->getMesh("jga/sign.3DS"),mat);
			}

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			const int max_obj=2;
			irr::core::matrix4 matWorld[max_obj],matWorld_scl[max_obj],matWorld_trn[max_obj],matWorld_rot[max_obj];

			irr::core::quaternion qt;
			
			

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				{
					//월드변환
					{
						int iobj = 0;


						matWorld_trn[iobj].setTranslation(irr::core::vector3df(0,0,0));

						irr::core::matrix4 matRdiff;
						//matRdiff.setRotationDegrees(irr::core::vector3df(0,45*fDelta,0));
						irr::core::quaternion qtr(0,45*fDelta*irr::core::DEGTORAD,0);

						qt *= qtr;

						//쿼터니온을 바로 회전 메트릭스로
						qt.getMatrix(matWorld_rot[iobj],irr::core::vector3df(0,0,0));

						//matWorld_rot[iobj] *= matRdiff;
						/*matWorld_rot.setRotationDegrees(irr::core::vector3df(0,45,0));
						matWorld_scl.setScale(irr::core::vector3df(150,1,150));*/

						matWorld[iobj] = matWorld_trn[iobj] * matWorld_rot[iobj] * matWorld_scl[iobj];

						pVideo->setTransform(
							irr::video::ETS_WORLD,
							matWorld[iobj]
							); 
					}					

					//메트리얼설정
					{					
						irr::video::SMaterial m;
						m.Lighting = false; //라이트를꺼야색이제데로나온다.
						//m.Wireframe = true;		
						m.setTexture(0,pVideo->getTexture("jga/sign.tga"));
						pVideo->setMaterial(m);
					}

					irr::scene::IAnimatedMesh* pMesh;
					pMesh = pSmgr->getMesh("jga/sign.3DS");


					irr::u32 i;
					for(i=0;i<pMesh->getMeshBufferCount();i++)
					{
						pVideo->drawMeshBuffer(pMesh->getMeshBuffer(i));
					}
				}

				{
					//월드변환
					{
						int iobj = 1;

						matWorld_trn[iobj].setTranslation(irr::core::vector3df(0,0,0));						

						//쿼터니온을 각도로 
						irr::core::vector3df vRot;
						qt.toEuler(vRot);
						vRot *= irr::core::RADTODEG;

						//각도를 다시 메트릭스로
						matWorld_rot[iobj].setRotationDegrees(vRot);

						std::cout << vRot.Y << std::endl;												

						matWorld[iobj] = matWorld_trn[iobj] * matWorld_rot[iobj] * matWorld_scl[iobj];

						pVideo->setTransform(
							irr::video::ETS_WORLD,
							matWorld[iobj]
							); 
					}

					//메트리얼설정
					{					
						irr::video::SMaterial m;
						m.Lighting = false; //라이트를꺼야색이제데로나온다.
						//m.Wireframe = true;		
						m.setTexture(0,pVideo->getTexture("jga/sign.tga"));
						pVideo->setMaterial(m);
					}

					irr::scene::IAnimatedMesh* pMesh;
					pMesh = pSmgr->getMesh("jga/sign.3DS");


					irr::u32 i;
					for(i=0;i<pMesh->getMeshBufferCount();i++)
					{
						pVideo->drawMeshBuffer(pMesh->getMeshBuffer(i));
					}
				}




				pVideo->endScene();	
			}

			pDevice->drop();
		}	
	}
	
	
	//씬매니져 의 addToDeletionQueue 사용 해서 노드 안전하게 제거하기
	namespace _02
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(irr::video::EDT_OPENGL);

			//창이름정하기
			pDevice->setWindowCaption(L"sample 6-2");
			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();			
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();			
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,30,-40), irr::core::vector3df(0,5,0));

			{
				irr::scene::ISceneNode *pNode =  pSmgr->addCubeSceneNode();			
				pNode->setName("usr/scene/node/cube");

				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setMaterialTexture(0,pVideo->getTexture("t351sml.jpg"));					
			}
			
			//pSmgr->addToDeletionQueue(

			while(pDevice->run())
			{	
                //다른 운영제제호환 되도록 수정요함                 
				//if( (::GetAsyncKeyState(VK_SPACE) & 0x8001) == 0x8001)
				{
					pSmgr->addToDeletionQueue(
						pSmgr->getSceneNodeFromName("usr/scene/node/cube")
						);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));		
				pSmgr->drawAll();
				pGuiEnv->drawAll();			
				pVideo->endScene();						
			}	

			pDevice->drop();
		}
	}

	
	//씬그라프 순회하기
	namespace _03
	{
		void recurDebugInfoChange(irr::scene::ISceneNode *pNode,bool wireframe)
		{
			irr::core::list<irr::scene::ISceneNode *> children = pNode->getChildren();

			irr::core::list<irr::scene::ISceneNode*>::Iterator it = children.begin();
			for (; it != children.end(); ++it)
			{
				//(*it)->setDebugDataVisible(state);
				(*it)->setMaterialFlag(irr::video::EMF_WIREFRAME,wireframe);

				recurDebugInfoChange((*it),wireframe);
			}
		}

		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_OPENGL,
				irr::core::dimension2du(640, 480), 
				32,
				false, false, true,
				NULL
				);

			pDevice->setWindowCaption(L"6-3");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			irr::scene::ISceneNode *pCam = pSmgr->addCameraSceneNode(0, 
				irr::core::vector3df(0,0,-40), 
				irr::core::vector3df(0,-5,0));


			irr::scene::ISceneNode *pNode1 = pSmgr->addCubeSceneNode();	
			pNode1->setMaterialFlag(irr::video::EMF_LIGHTING,false);
			pNode1->setMaterialTexture(0,pVideo->getTexture("t351sml.jpg"));			
			pNode1->setPosition(irr::core::vector3df(0,0,0));

			irr::scene::ISceneNode *pNode2 = pSmgr->addCubeSceneNode();			
			pNode2->setMaterialFlag(irr::video::EMF_LIGHTING,false);
			pNode2->setMaterialTexture(0,pVideo->getTexture("t351sml.jpg"));	
			pNode2->setPosition(irr::core::vector3df(15,-10,0));

			//종속관계 만들기
			pNode1->addChild(pNode2);						

			while(pDevice->run())
			{	
				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

                /*
				if( (::GetAsyncKeyState(VK_F1) & 0x8001) == 0x8001)
				{
					recurDebugInfoChange(pSmgr->getRootSceneNode(),true);
				}

				if( (::GetAsyncKeyState(VK_F2) & 0x8001) == 0x8001)
				{
					recurDebugInfoChange(pSmgr->getRootSceneNode(),false);
				}
                 */

				pSmgr->drawAll();
				pGuiEnv->drawAll();			

				pVideo->endScene();					
			}

			pDevice->drop();
		}
	}
}