﻿//#include "part1.h"

#include <irrlicht.h>
#include <iostream>


//#include <SDKDDKVer.h>
//#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
//#include <windows.h>

//stl header
//#include <vector>
//#include <map>

/////////////////////////////
//
//스키닝 예제
//
/////////////////////////////


/*
void loadModel(const c8* filename){	//load a model into the engine	
	if (Model)		Model->remove();	
	Model = 0;	
	scene::IAnimatedMesh* m = Device->getSceneManager()->getMesh(filename);	
	if (!m) 	{		// model could not be loaded		
		if (StartUpModelFile != filename)			
			Device->getGUIEnvironment()->addMessageBox(
			Caption.c_str(), L"The model could not be loaded. Maybe it is not a supported file format."
			);		
		return;	
	}	// set default material properties	
	Model = Device->getSceneManager()->addAnimatedMeshSceneNode(m);	
	Model->setMaterialType(video::EMT_TRANSPARENT_ADD_COLOR);	
	Model->setMaterialFlag(video::EMF_LIGHTING, false);	
	Model->setDebugDataVisible(true);
}
*/


void Lession07()
{
	irr::IrrlichtDevice *pDevice = irr::createDevice(					
			irr::video::EDT_OPENGL,
#if(IRRLICHT_VERSION_MAJOR == 1 && IRRLICHT_VERSION_MINOR >= 6 )
		irr::core::dimension2d<irr::u32>(640, 480), 
#else
		irr::core::dimension2d<irr::s32>(640, 480), 
#endif
			32,
			false, false, true,
			NULL
			);

	pDevice->setWindowCaption(L"Mount to Joint");

	irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
	irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
	irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

	pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,60,-120), irr::core::vector3df(0,5,0));	


	{//스키닝 애니관련예제임
		
		irr::scene::IAnimatedMesh* pAniMesh = pSmgr->getMesh("../res/dwarf/dwarf1.b3d" );

		irr::scene::IAnimatedMeshSceneNode *pNodeDwarf = pSmgr->addAnimatedMeshSceneNode(pAniMesh);
		pNodeDwarf->setMaterialFlag(irr::video::EMF_LIGHTING, false);
		pNodeDwarf->setAnimationSpeed(5);
		pNodeDwarf->setName("dwarf");
		pNodeDwarf->setFrameLoop(112,126);
		
		
		//스키닝 메쉬로 형변환(조인트를 가지는 b3d,ms3d,x 파일용)
		irr::scene::ISkinnedMesh* pSkinMesh = (irr::scene::ISkinnedMesh*)pAniMesh;

		printf("joint count %d \n",pSkinMesh->getJointCount());
		irr::u32 i;
		for(i=0;i<pSkinMesh->getJointCount();i++)
		{
			irr::core::array<irr::scene::ISkinnedMesh::SJoint *> AllJoint = pSkinMesh->getAllJoints();
			printf("%d / %s \n",i,AllJoint[i]->Name.c_str());
		}

		irr::scene::IAnimatedMesh *pArrowMesh =  pSmgr->addArrowMesh("arrow",
			irr::video::SColor(64,64,0,0),
			irr::video::SColor(64,0,0,64),
			4,8,60,50,5,10
			//4,8,16,15,10,10
			);

		printf("----------------------\n");
		{
			irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(pArrowMesh);
			pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
			pNode->setName("arrow");

			irr::scene::IBoneSceneNode *pBone = pNodeDwarf->getJointNode("hit");
			pBone->addChild(pNode); // 도끼에 화살표 붙이기
			
			const irr::core::list<irr::scene::ISceneNode*> &chiled = pBone->getChildren();

			irr::core::list<irr::scene::ISceneNode*>::ConstIterator it = chiled.begin();
			
			for(;it!=chiled.end();it++)
			{				
				printf("%s\n",(*it)->getName());
			}
			
			
			pNode->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);
			pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);

			//pNode = pSmgr->addAnimatedMeshSceneNode(pArrowMesh);
			//pNodeDwarf->getJointNode("head")->addChild(pNode);
			//pNode->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);
			
		}
	}


	irr::u32 uLastTick = pDevice->getTimer()->getTime();
	while(pDevice->run())
	{	
		irr::u32 uTick = pDevice->getTimer()->getTime();
		irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f;		

		uLastTick = pDevice->getTimer()->getTime();

		pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));	
		
		pSmgr->drawAll();
		pGuiEnv->drawAll();			
		
		pVideo->endScene();	
	}

	pDevice->drop();
}



