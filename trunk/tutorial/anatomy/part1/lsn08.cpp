﻿//#include "part1.h"

#include <irrlicht.h>
#include <iostream>


//#include <SDKDDKVer.h>
//#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
//#include <windows.h>

//stl header
//#include <vector>
//#include <map>

//애니메이션 제어 예제
namespace lsn08
{

	//애니메이터(활동자) 예제
	namespace _00
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL);

			pDevice->setWindowCaption(L"lesson 08");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,30,-140), irr::core::vector3df(0,5,0));	


			irr::scene::IAnimatedMesh *pAniArrow =  pSmgr->addArrowMesh("arrow",
				irr::video::SColor(255,255,0,0),
				irr::video::SColor(255,0,0,255),
				4,8,6,4,3,3			
				);

			irr::core::vector3df StartPoint(100,0,0);
			irr::core::vector3df EndPoint(-100,0,0);
			irr::core::vector3df Point1(-50,50,0);

			irr::scene::IAnimatedMeshSceneNode *pArrowNode1= pSmgr->addAnimatedMeshSceneNode(pAniArrow);	
			pArrowNode1->setMaterialFlag(irr::video::EMF_LIGHTING,false);
			pArrowNode1->setPosition(StartPoint);

			irr::scene::IAnimatedMeshSceneNode *pArrowNode2 = pSmgr->addAnimatedMeshSceneNode(pAniArrow);	
			pArrowNode2->setMaterialFlag(irr::video::EMF_LIGHTING,false);
			pArrowNode2->setPosition(EndPoint);

			irr::scene::IAnimatedMeshSceneNode *pArrowNode3 = pSmgr->addAnimatedMeshSceneNode(pAniArrow);	
			pArrowNode3->setMaterialFlag(irr::video::EMF_LIGHTING,false);
			pArrowNode3->setPosition(Point1);

			irr::scene::ISceneNode *pNode1 = pSmgr->addCubeSceneNode();	
			pNode1->setMaterialTexture(0, pVideo->getTexture("wall.bmp"));
			pNode1->setMaterialFlag(irr::video::EMF_LIGHTING,false);

			irr::scene::ISceneNode *pNode2 = pSmgr->addCubeSceneNode();	
			pNode2->setMaterialTexture(0, pVideo->getTexture("wall.bmp"));
			pNode2->setMaterialFlag(irr::video::EMF_LIGHTING,false);

			irr::scene::ISceneNode *pNode3 = pSmgr->addCubeSceneNode();	
			pNode3->setMaterialTexture(0, pVideo->getTexture("wall.bmp"));
			pNode3->setMaterialFlag(irr::video::EMF_LIGHTING,false);

			{//직선이동 & 시한부 삭제 애니메이터
				irr::scene::ISceneNodeAnimator* pAnim;
				pAnim = pSmgr->createFlyStraightAnimator(
					StartPoint,	//시작위치
					EndPoint, //끝위치
					10000,	//수행시간
					true	//루핑여부
					);
				pNode1->addAnimator(pAnim);			
				pAnim->drop();

				pAnim = pSmgr->createDeleteAnimator(5000);

				pNode1->addAnimator(pAnim);
				pAnim->drop();		
			}

			{//회전 애니메이터
				irr::scene::ISceneNodeAnimator* pAnim;
				pAnim = pSmgr->createFlyCircleAnimator(
					Point1,
					15
					);

				std::cout << "ref count : " <<  pAnim->getReferenceCount() << std::endl;

				pNode3->addAnimator(pAnim);			

				std::cout << "ref count : " << pAnim->getReferenceCount() << std::endl;

				pAnim->drop(); //레퍼런스 카운터 하나줄이기

				std::cout << "ref count : " << pAnim->getReferenceCount() << std::endl;
			}

			{//스플라인 곡선이동
				irr::scene::ISceneNodeAnimator* pAnim;
				irr::core::array<irr::core::vector3df> WayPoints;

				WayPoints.push_back(StartPoint);
				WayPoints.push_back(EndPoint);
				WayPoints.push_back(Point1);

				pAnim = pSmgr->createFollowSplineAnimator(
					pDevice->getTimer()->getTime(),
					WayPoints			
					);

				pNode2->addAnimator(pAnim);		
				pAnim->drop();
			}	

			

			irr::u32 uLastTick = pDevice->getTimer()->getTime();	

			while(pDevice->run())
			{	
				irr::u32 uTick = pDevice->getTimer()->getTime();
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f;

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));	

				{
					irr::core::matrix4 mat;//단위행렬로초기화
					pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

					irr::video::SMaterial m;
					m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.
					m.ZBuffer = false;

					pVideo->setMaterial(m);

					pVideo->draw3DLine(irr::core::vector3df(-100,0,0),irr::core::vector3df(100,0,0),irr::video::SColor(0,0,255,0));
					pVideo->draw3DLine(irr::core::vector3df(0,-100,0),irr::core::vector3df(0,100,0),irr::video::SColor(0,0,255,0));
					pVideo->draw3DLine(irr::core::vector3df(0,0,-100),irr::core::vector3df(0,0,100),irr::video::SColor(0,0,255,0));

				}

				pSmgr->drawAll();
				pGuiEnv->drawAll();			

				pVideo->endScene();	

				uLastTick = pDevice->getTimer()->getTime();
			}

			pDevice->drop();
		}




	}

	//
	//fps 식 게임 카메라 캐릭터 제어 예제
	//방향키 상하 : 전후 이동
	//방향키 좌후: 게걸음질이동
	//pgdn,pgup : 상하 이동
	//ctrl+ 방향키 상하 : 피치회전
	//ctrl+ 방향키 좌우 : 요회전
	//F1,F2 카메라 전환
	//
	namespace _10
	{		
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");
			{
				irr::scene::ICameraSceneNode *pCam = pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,5,-10), irr::core::vector3df(0,0,0));
				pCam->setName("usr/scne/camera/1");
			}

			//힐플래인 메쉬 추가
			{
				irr::scene::IAnimatedMesh *pMesh =
					pSmgr->addHillPlaneMesh("usr/mesh/myhill",
					irr::core::dimension2d<irr::f32>(8,8),
					irr::core::dimension2d<irr::u32>(8,8),
					0,0,
					irr::core::dimension2d<irr::f32>(0,0),
					irr::core::dimension2d<irr::f32>(8,8)
					);
			}

			//씬노드 추가
			{
				irr::scene::IAnimatedMeshSceneNode *pNode =
					pSmgr->addAnimatedMeshSceneNode(pSmgr->getMesh("usr/mesh/myhill"));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setMaterialTexture(0,pVideo->getTexture("wall.jpg"));

				pNode->setName("usr/scene/hill1");
			}

			{
				//irr::scene::ISceneNode *pRootNode = pSmgr->addEmptySceneNode();
				//pRootNode->setName("usr/scenenode/chr_root");

				irr::scene::ICameraSceneNode *pCam = pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,3,-5), irr::core::vector3df(0,0,0));
				pCam->setName("usr/scne/camera/2");


				irr::scene::ISceneNode *pNode;

				{
					pNode = pSmgr->addAnimatedMeshSceneNode(
						pSmgr->addArrowMesh("usr/mesh/arrow/red",
						irr::video::SColor(255,255,0,0),
						irr::video::SColor(255,255,0,0)
						),
						pCam,
						-1,
						irr::core::vector3df(0,0,0),
						irr::core::vector3df(0,0,0)
						);
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
					pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);

					pNode = pSmgr->addAnimatedMeshSceneNode(
						pSmgr->addArrowMesh("usr/mesh/arrow/green",
						irr::video::SColor(255,0,255,0),
						irr::video::SColor(255,0,255,0)
						),
						pCam,
						-1,
						irr::core::vector3df(0,0,0),
						irr::core::vector3df(0,0,-90)
						);
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
					pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);

					pNode = pSmgr->addAnimatedMeshSceneNode(
						pSmgr->addArrowMesh("usr/mesh/arrow/blue",
						irr::video::SColor(255,0,0,255),
						irr::video::SColor(255,0,0,255)
						),
						pCam,
						-1,
						irr::core::vector3df(0,0,0),
						irr::core::vector3df(90,0,0)
						);
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
					pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);
				}
			}

			pSmgr->setActiveCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/1"));

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				//fps 식 카메라 캐릭터 컨트롤
				{
					irr::scene::ICameraSceneNode *pCam = (irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2");

					irr::f32 fRotSpeed = 45.f;
					irr::f32 fSpeed = 5.0f;
					irr::core::vector3df vFront = (pCam->getTarget() - pCam->getAbsolutePosition());
					irr::core::vector3df vUp = pCam->getUpVector();

					//직교 재설정
					vFront.normalize();
					vUp.normalize();
					//좌향벡터구하기
					irr::core::vector3df vSide = vFront.crossProduct(vUp); //vFront X vUp
					vSide.normalize();
					//상방벡터구하기
					vUp = vSide.crossProduct(vFront);// vSide X vFront

					irr::core::vector3df vMoveFront = (vFront*fDelta * fSpeed);
					irr::core::vector3df vMoveUp = vUp*fDelta * fSpeed;
					irr::core::vector3df vMoveSide = vSide*fDelta * fSpeed;					

                    /*
					if(!(::GetAsyncKeyState(VK_LCONTROL) & 0x8001))
					{
						//전후진
						if( (::GetAsyncKeyState(VK_UP) & 0x8001))
						{
							pCam->setPosition(
								pCam->getAbsolutePosition() + vMoveFront
								);

						}
						if( (::GetAsyncKeyState(VK_DOWN) & 0x8001))
						{
							pCam->setPosition(
								pCam->getAbsolutePosition() - vMoveFront
								);

						}

						//좌우게걸음질
						if( (::GetAsyncKeyState(VK_LEFT) & 0x8001) )
						{
							pCam->setPosition(
								pCam->getAbsolutePosition() + vMoveSide
								);

						}

						if( (::GetAsyncKeyState(VK_RIGHT) & 0x8001) )
						{
							pCam->setPosition(
								pCam->getPosition() - vMoveSide
								);

						}

						//상하
						if( (::GetAsyncKeyState(VK_PRIOR) & 0x8001) )
						{
							pCam->setPosition(
								pCam->getPosition() + vMoveUp
								);

						}

						if( (::GetAsyncKeyState(VK_NEXT) & 0x8001) )
						{
							pCam->setPosition(
								pCam->getAbsolutePosition() - vMoveUp
								);

						}
					}
					else
					{//회전
						if( (::GetAsyncKeyState(VK_UP) & 0x8001) )
						{
							pCam->setRotation(
								pCam->getRotation() - irr::core::vector3df(fDelta*fRotSpeed,0,0)
								);
						}
						if( (::GetAsyncKeyState(VK_DOWN) & 0x8001))
						{
							pCam->setRotation(
								pCam->getRotation() + irr::core::vector3df(fDelta*fRotSpeed,0,0)
								);						
						}
						if( (::GetAsyncKeyState(VK_LEFT) & 0x8001) )
						{	
							pCam->setRotation(
								pCam->getRotation() - irr::core::vector3df(0,fDelta*fRotSpeed,0)
								);							
						}
						if( (::GetAsyncKeyState(VK_RIGHT) & 0x8001))
						{		
							pCam->setRotation(
								pCam->getRotation() + irr::core::vector3df(0,fDelta*fRotSpeed,0)
								);							
						}
					}
                     */

					//전역변환 업데이트해주기
					pCam->updateAbsolutePosition();

					//업벡터와 시점벡터에 변환반영
					{
						irr::core::vector3df vTarget(0,0,1);
						irr::core::vector3df vUp(0,1,0);

						irr::core::matrix4 mat = pCam->getAbsoluteTransformation();

						mat.transformVect(vTarget);
						mat.transformVect(vUp);

						pCam->setTarget(vTarget);	
						pCam->setUpVector(vUp - pCam->getPosition());						
					}

                    /*
					if((::GetAsyncKeyState(VK_F1) & 0x8001) == 0x8001)
					{
						pSmgr->setActiveCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/1"));
					}
					if((::GetAsyncKeyState(VK_F2) & 0x8001) == 0x8001)
					{
						pSmgr->setActiveCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2"));
					}
                     */
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}

	//비행시뮬레이션 이동 제어 예제
	//임의의 축에대한 회전 구현
	namespace _11
	{		
		void rotate(irr::scene::ISceneNode *node, irr::core::vector3df rot) 
		{ 
			/*
			//행렬버전
			irr::core::matrix4 m; 
			m.setRotationDegrees(node->getRotation()); 
			irr::core::matrix4 n; 
			n.setRotationDegrees(rot); 
			m *= n; 
			node->setRotation( m.getRotationDegrees() );
			*/
			//사원수버전
			irr::core::quaternion qt(node->getRotation() * irr::core::DEGTORAD);
			irr::core::quaternion qt_rot(rot * irr::core::DEGTORAD);

			qt *= qt_rot;

			irr::core::vector3df vEuler;
			qt.toEuler(vEuler);

			node->setRotation(vEuler * irr::core::RADTODEG);

			node->updateAbsolutePosition(); 
		} 

		void rotateAxies(irr::scene::ISceneNode *node, irr::core::vector3df axies,irr::f32 rad) 
		{ 
			/*
			//행렬버전
			irr::core::quaternion qt;
			irr::core::matrix4 matW,matL;
			matW = node->getAbsoluteTransformation();
			qt.fromAngleAxis(rad,axies);
			matL = qt.getMatrix();
			matW = matL * matW;
			node->setRotation(matW.getRotationDegrees());						
			*/

			//쿼터니온 버전
			irr::core::quaternion qt,qt_node;			
			qt_node.set(node->getRotation() * irr::core::DEGTORAD);
			qt.fromAngleAxis(rad,axies);			
			qt_node = qt_node * qt; //곱하는 순서에 주의를
			irr::core::vector3df vEur;
			qt_node.toEuler(vEur);
			node->setRotation(vEur*irr::core::RADTODEG);

			node->updateAbsolutePosition(); 
		}

		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			{
				irr::scene::ICameraSceneNode *pCam = pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,5,-10), irr::core::vector3df(0,0,0));
				pCam->setName("usr/scne/camera/1");
			}

			//힐플래인 메쉬 추가
			{
				irr::scene::IAnimatedMesh *pMesh =
					pSmgr->addHillPlaneMesh("usr/mesh/myhill",
					irr::core::dimension2d<irr::f32>(8,8),
					irr::core::dimension2d<irr::u32>(8,8),
					0,0,
					irr::core::dimension2d<irr::f32>(0,0),
					irr::core::dimension2d<irr::f32>(8,8)
					);
			}

			//씬노드 추가
			{
				irr::scene::IAnimatedMeshSceneNode *pNode =
					pSmgr->addAnimatedMeshSceneNode(pSmgr->getMesh("usr/mesh/myhill"));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setMaterialTexture(0,pVideo->getTexture("wall.jpg"));

				pNode->setName("usr/scene/hill1");
			}

			{
				//irr::scene::ISceneNode *pRootNode = pSmgr->addEmptySceneNode();
				//pRootNode->setName("usr/scenenode/chr_root");

				irr::scene::ICameraSceneNode *pCam = pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,3,-5), irr::core::vector3df(0,0,0));
				pCam->setName("usr/scne/camera/2");


				irr::scene::ISceneNode *pNode;

				{
					pNode = pSmgr->addAnimatedMeshSceneNode(
						pSmgr->addArrowMesh("usr/mesh/arrow/red",
						irr::video::SColor(255,255,0,0),
						irr::video::SColor(255,255,0,0)
						),
						pCam,
						-1,
						irr::core::vector3df(0,0,0),
						irr::core::vector3df(0,0,0)
						);
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
					pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);

					pNode = pSmgr->addAnimatedMeshSceneNode(
						pSmgr->addArrowMesh("usr/mesh/arrow/green",
						irr::video::SColor(255,0,255,0),
						irr::video::SColor(255,0,255,0)
						),
						pCam,
						-1,
						irr::core::vector3df(0,0,0),
						irr::core::vector3df(0,0,-90)
						);
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
					pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);

					pNode = pSmgr->addAnimatedMeshSceneNode(
						pSmgr->addArrowMesh("usr/mesh/arrow/blue",
						irr::video::SColor(255,0,0,255),
						irr::video::SColor(255,0,0,255)
						),
						pCam,
						-1,
						irr::core::vector3df(0,0,0),
						irr::core::vector3df(90,0,0)
						);
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
					pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);
				}
			}

			pSmgr->setActiveCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/1"));

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				//fps 식 카메라 캐릭터 컨트롤
				{
					irr::scene::ICameraSceneNode *pCam = (irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2");

					irr::f32 fRotSpeed = 45.f;
					irr::f32 fSpeed = 5.0f;
					irr::core::vector3df vFront = (pCam->getTarget() - pCam->getAbsolutePosition());
					irr::core::vector3df vUp = pCam->getUpVector();

					//직교 재설정
					vFront.normalize();
					vUp.normalize();
					//좌향벡터구하기
					irr::core::vector3df vSide = vFront.crossProduct(vUp); //vFront X vUp
					vSide.normalize();
					//상방벡터구하기
					vUp = vSide.crossProduct(vFront);// vSide X vFront

					irr::core::vector3df vMoveFront = (vFront*fDelta * fSpeed);
					irr::core::vector3df vMoveUp = vUp*fDelta * fSpeed;
					irr::core::vector3df vMoveSide = vSide*fDelta * fSpeed;					

                    /*
					if(!(::GetAsyncKeyState(VK_LCONTROL) & 0x8001))
					{
						//전후진
						if( (::GetAsyncKeyState(VK_UP) & 0x8001))
						{
							pCam->setPosition(
								pCam->getAbsolutePosition() + vMoveFront
								);

						}
						if( (::GetAsyncKeyState(VK_DOWN) & 0x8001))
						{
							pCam->setPosition(
								pCam->getAbsolutePosition() - vMoveFront
								);

						}						
					}
					else
					{//회전
						if( (::GetAsyncKeyState(VK_UP) & 0x8001) )
						{							
							rotate(pCam,-irr::core::vector3df(fDelta*fRotSpeed,0,0));
						}
						if( (::GetAsyncKeyState(VK_DOWN) & 0x8001))
						{							
							rotate(pCam,irr::core::vector3df(fDelta*fRotSpeed,0,0));
						}
						if( (::GetAsyncKeyState(VK_LEFT) & 0x8001) )
						{								
							rotate(pCam,irr::core::vector3df(0,fDelta*fRotSpeed,0));							
						}
						if( (::GetAsyncKeyState(VK_RIGHT) & 0x8001))
						{								
							rotate(pCam,irr::core::vector3df(0,-fDelta*fRotSpeed,0));							
						}
						if( (::GetAsyncKeyState(VK_PRIOR) & 0x8001)) //전방벡터를 축으로 회전하기
						{	
							rotate(pCam,irr::core::vector3df(0,0,fDelta*fRotSpeed));							
						}
						if( (::GetAsyncKeyState(VK_NEXT) & 0x8001)) //전방벡터를 축으로 회전하기
						{	
							rotate(pCam,irr::core::vector3df(0,0,-fDelta*fRotSpeed));							
						}
					}
                     */

					//전역변환 업데이트해주기
					pCam->updateAbsolutePosition();

					//업벡터와 시점벡터에 변환반영
					{
						irr::core::vector3df vTarget(0,0,1);
						irr::core::vector3df vUp(0,1,0);

						irr::core::matrix4 mat = pCam->getAbsoluteTransformation();

						mat.transformVect(vTarget);
						mat.transformVect(vUp);

						pCam->setTarget(vTarget);	
						pCam->setUpVector(vUp - pCam->getPosition());						
					}

                    /*
					if((::GetAsyncKeyState(VK_F1) & 0x8001) == 0x8001)
					{
						pSmgr->setActiveCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/1"));
					}
					if((::GetAsyncKeyState(VK_F2) & 0x8001) == 0x8001)
					{
						pSmgr->setActiveCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2"));
					}
                     */
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}

	//fps 식 게임 카메라 캐릭터 제어 예제 2
	//갸우뚱거림처리
	namespace _12
	{	
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			{
				irr::scene::ICameraSceneNode *pCam = pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,5,-10), irr::core::vector3df(0,0,0));
				pCam->setName("usr/scne/camera/1");
			}

			//힐플래인 메쉬 추가
			{
				irr::scene::IAnimatedMesh *pMesh =
					pSmgr->addHillPlaneMesh("usr/mesh/myhill",
					irr::core::dimension2d<irr::f32>(8,8),
					irr::core::dimension2d<irr::u32>(8,8),
					0,0,
					irr::core::dimension2d<irr::f32>(0,0),
					irr::core::dimension2d<irr::f32>(8,8)
					);
			}

			//씬노드 추가
			{
				irr::scene::IAnimatedMeshSceneNode *pNode =
					pSmgr->addAnimatedMeshSceneNode(pSmgr->getMesh("usr/mesh/myhill"));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setMaterialTexture(0,pVideo->getTexture("wall.jpg"));

				pNode->setName("usr/scene/hill1");
			}

			{
				//irr::scene::ISceneNode *pRootNode = pSmgr->addEmptySceneNode();
				//pRootNode->setName("usr/scenenode/chr_root");

				irr::scene::ICameraSceneNode *pCam = pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,3,-5), irr::core::vector3df(0,0,0));
				pCam->setName("usr/scne/camera/2");


				irr::scene::ISceneNode *pNode;

				{
					pNode = pSmgr->addAnimatedMeshSceneNode(
						pSmgr->addArrowMesh("usr/mesh/arrow/red",
						irr::video::SColor(255,255,0,0),
						irr::video::SColor(255,255,0,0)
						),
						pCam,
						-1,
						irr::core::vector3df(0,0,0),
						irr::core::vector3df(0,0,0)
						);
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
					pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);

					pNode = pSmgr->addAnimatedMeshSceneNode(
						pSmgr->addArrowMesh("usr/mesh/arrow/green",
						irr::video::SColor(255,0,255,0),
						irr::video::SColor(255,0,255,0)
						),
						pCam,
						-1,
						irr::core::vector3df(0,0,0),
						irr::core::vector3df(0,0,-90)
						);
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
					pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);

					pNode = pSmgr->addAnimatedMeshSceneNode(
						pSmgr->addArrowMesh("usr/mesh/arrow/blue",
						irr::video::SColor(255,0,0,255),
						irr::video::SColor(255,0,0,255)
						),
						pCam,
						-1,
						irr::core::vector3df(0,0,0),
						irr::core::vector3df(90,0,0)
						);
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
					pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);
				}
			}

			pSmgr->setActiveCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/1"));

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				//fps 식 카메라 캐릭터 컨트롤
				{
					irr::scene::ICameraSceneNode *pCam = (irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2");

					irr::f32 fRotSpeed = 45.f;
					irr::f32 fSpeed = 5.0f;
					irr::core::vector3df vFront = (pCam->getTarget() - pCam->getAbsolutePosition());
					irr::core::vector3df vUp = pCam->getUpVector();

					//직교 재설정
					vFront.normalize();
					vUp.normalize();
					//좌향벡터구하기
					irr::core::vector3df vSide = vFront.crossProduct(vUp); //vFront X vUp
					vSide.normalize();
					//상방벡터구하기
					vUp = vSide.crossProduct(vFront);// vSide X vFront

					irr::core::vector3df vMoveFront = (vFront*fDelta * fSpeed);
					irr::core::vector3df vMoveUp = vUp*fDelta * fSpeed;
					irr::core::vector3df vMoveSide = vSide*fDelta * fSpeed;		
/*
					if(!(::GetAsyncKeyState(VK_LCONTROL) & 0x8001))
					{
						//전후진
						if( (::GetAsyncKeyState(VK_UP) & 0x8001))
						{
							pCam->setPosition(
								pCam->getAbsolutePosition() + vMoveFront
								);

						}
						if( (::GetAsyncKeyState(VK_DOWN) & 0x8001))
						{
							pCam->setPosition(
								pCam->getAbsolutePosition() - vMoveFront
								);

						}

						//좌우게걸음질
						if( (::GetAsyncKeyState(VK_LEFT) & 0x8001) )
						{
							pCam->setPosition(
								pCam->getAbsolutePosition() + vMoveSide
								);

						}

						if( (::GetAsyncKeyState(VK_RIGHT) & 0x8001) )
						{
							pCam->setPosition(
								pCam->getPosition() - vMoveSide
								);

						}

						//상하
						if( (::GetAsyncKeyState(VK_PRIOR) & 0x8001) )
						{
							pCam->setPosition(
								pCam->getPosition() + vMoveUp
								);

						}

						if( (::GetAsyncKeyState(VK_NEXT) & 0x8001) )
						{
							pCam->setPosition(
								pCam->getAbsolutePosition() - vMoveUp
								);

						}
					}
					else
					{//회전
						if( (::GetAsyncKeyState(VK_UP) & 0x8001) )
						{
							pCam->setRotation(
								pCam->getRotation() - irr::core::vector3df(fDelta*fRotSpeed,0,0)
								);
						}
						if( (::GetAsyncKeyState(VK_DOWN) & 0x8001))
						{
							pCam->setRotation(
								pCam->getRotation() + irr::core::vector3df(fDelta*fRotSpeed,0,0)
								);						
						}
						if( (::GetAsyncKeyState(VK_LEFT) & 0x8001) )
						{	
							_11::rotateAxies(pCam,irr::core::vector3df(0,1,0),-fDelta*fRotSpeed * irr::core::DEGTORAD);
							
						}
						if( (::GetAsyncKeyState(VK_RIGHT) & 0x8001))
						{		
							_11::rotateAxies(pCam,irr::core::vector3df(0,1,0),+fDelta*fRotSpeed * irr::core::DEGTORAD);							
						}

						//갸우뚱처리
						if( (::GetAsyncKeyState(VK_PRIOR) & 0x8001))
						{		
							_11::rotateAxies(pCam,vFront,fDelta*fRotSpeed * irr::core::DEGTORAD);							
						}

						if( (::GetAsyncKeyState(VK_NEXT) & 0x8001))
						{	
							_11::rotateAxies(pCam,vFront,-(fDelta*fRotSpeed * irr::core::DEGTORAD));							
						}
					}

 */


					//전역변환 업데이트해주기
					pCam->updateAbsolutePosition();

					//업벡터와 시점벡터에 변환반영
					{
						irr::core::vector3df vTarget(0,0,1);
						irr::core::vector3df vUp(0,1,0);

						irr::core::matrix4 mat = pCam->getAbsoluteTransformation();

						mat.transformVect(vTarget);
						mat.transformVect(vUp);

						pCam->setTarget(vTarget);	
						pCam->setUpVector(vUp - pCam->getPosition());						
					}

                    /*
					if((::GetAsyncKeyState(VK_F1) & 0x8001) == 0x8001)
					{
						pSmgr->setActiveCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/1"));
					}
					if((::GetAsyncKeyState(VK_F2) & 0x8001) == 0x8001)
					{
						pSmgr->setActiveCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2"));
					}
                     */
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}

	//트랙볼 카메라 구현예제
	namespace _13
	{	
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			{
				irr::scene::ICameraSceneNode *pCam = pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,5,-10), irr::core::vector3df(0,0,0));
				pCam->setName("usr/scne/camera/1");
			}

			//힐플래인 메쉬 추가
			{
				irr::scene::IAnimatedMesh *pMesh =
					pSmgr->addHillPlaneMesh("usr/mesh/myhill",
					irr::core::dimension2d<irr::f32>(8,8),
					irr::core::dimension2d<irr::u32>(8,8),
					0,0,
					irr::core::dimension2d<irr::f32>(0,0),
					irr::core::dimension2d<irr::f32>(8,8)
					);
			}

			//씬노드 추가
			{
				irr::scene::IAnimatedMeshSceneNode *pNode =
					pSmgr->addAnimatedMeshSceneNode(pSmgr->getMesh("usr/mesh/myhill"));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setMaterialTexture(0,pVideo->getTexture("wall.jpg"));

				pNode->setName("usr/scene/hill1");
			}

			{
				irr::scene::ISceneNode *pRootNode = 
					pSmgr->addEmptySceneNode();
				pRootNode->setName("usr/scene/trackball");

				irr::scene::IMeshSceneNode *pNode = 
					pSmgr->addSphereSceneNode(1.f);
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);
				pNode->setName("usr/scene/sphere");
				pNode->setParent(pRootNode);
			}


			{
				irr::scene::ICameraSceneNode *pCam = pSmgr->addCameraSceneNode(0, 
					irr::core::vector3df(0,3,-5), 
					irr::core::vector3df(0,0,0)
				);

				pCam->setName("usr/scne/camera/2");

				irr::scene::ISceneNode *pNode;

				{
					pNode = pSmgr->addAnimatedMeshSceneNode(
						pSmgr->addArrowMesh("usr/mesh/arrow/red",
						irr::video::SColor(255,255,0,0),
						irr::video::SColor(255,255,0,0)
						),
						pCam,
						-1,
						irr::core::vector3df(0,0,0),
						irr::core::vector3df(0,0,0)
						);
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
					pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);

					pNode = pSmgr->addAnimatedMeshSceneNode(
						pSmgr->addArrowMesh("usr/mesh/arrow/green",
						irr::video::SColor(255,0,255,0),
						irr::video::SColor(255,0,255,0)
						),
						pCam,
						-1,
						irr::core::vector3df(0,0,0),
						irr::core::vector3df(0,0,-90)
						);
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
					pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);

					pNode = pSmgr->addAnimatedMeshSceneNode(
						pSmgr->addArrowMesh("usr/mesh/arrow/blue",
						irr::video::SColor(255,0,0,255),
						irr::video::SColor(255,0,0,255)
						),
						pCam,
						-1,
						irr::core::vector3df(0,0,0),
						irr::core::vector3df(90,0,0)
						);
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
					pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);
				}
			}

			pSmgr->setActiveCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/1"));

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				//트랙볼식 에디터 컨트롤
				{
					/*
					{
						irr::scene::ISceneNode *pRootNode = 
							pSmgr->addEmptySceneNode();
						pRootNode->setName("usr/scene/trackball");
					}
					*/
					static irr::core::quaternion trackball_qtX(45*irr::core::DEGTORAD,0,0);
					static irr::core::quaternion trackball_qtY;
					static irr::core::quaternion trackball_qt;
					static irr::f32 fLength = 3;					

					//중심기준 노드
					irr::scene::ISceneNode *pNodeBall = pSmgr->getSceneNodeFromName("usr/scene/trackball"); 
					//중심기준회전할 위성 노드
					irr::scene::ICameraSceneNode *pCam = (irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2");

					irr::f32 fRotSpeed = 45.f;
					irr::f32 fSpeed = 5.0f;
					irr::core::vector3df vFront = (pCam->getTarget() - pCam->getAbsolutePosition());										
					irr::core::vector3df vUp = pCam->getUpVector();

					//직교 재설정
					vFront.normalize();
					vUp.normalize();
					//좌향벡터구하기
					irr::core::vector3df vSide = vFront.crossProduct(vUp); //vFront X vUp
					vSide.normalize();
					//상방벡터구하기
					vUp = vSide.crossProduct(vFront);// vSide X vFront

					irr::core::vector3df vMoveFront = (vFront*fDelta * fSpeed);
					irr::core::vector3df vMoveUp = vUp*fDelta * fSpeed;
					irr::core::vector3df vMoveSide = vSide*fDelta * fSpeed;		

                    /*
					if(!(::GetAsyncKeyState(VK_LCONTROL) & 0x8001))
					{
						//전후진 
						if( (::GetAsyncKeyState(VK_UP) & 0x8001))
						{
							pNodeBall->setPosition(pNodeBall->getAbsolutePosition() + vFront*fDelta*fSpeed);
							
						}
						if( (::GetAsyncKeyState(VK_DOWN) & 0x8001))
						{
							pNodeBall->setPosition(pNodeBall->getAbsolutePosition() - vFront*fDelta*fSpeed);
							
						}

						//시야거리 조정
						if( (::GetAsyncKeyState(VK_PRIOR) & 0x8001))
						{
							fLength -= fDelta*fSpeed;
						}
						if( (::GetAsyncKeyState(VK_NEXT) & 0x8001))
						{
							fLength += fDelta*fSpeed;
						}						
					}
					else
					{//회전
						irr::core::quaternion qt_pitch(irr::core::vector3df(fDelta*fRotSpeed,0,0) * irr::core::DEGTORAD);
						irr::core::quaternion qt_yaw(irr::core::vector3df(0,fDelta*fRotSpeed,0) * irr::core::DEGTORAD);		
		
						if( (::GetAsyncKeyState(VK_UP) & 0x8001) )
						{
							trackball_qtX *= qt_pitch;
							
						}
						if( (::GetAsyncKeyState(VK_DOWN) & 0x8001))
						{
							qt_pitch.makeInverse();
							trackball_qtX *= qt_pitch;										
						}
						if( (::GetAsyncKeyState(VK_LEFT) & 0x8001) )
						{	
							trackball_qtY *= qt_yaw;							
							
						}
						if( (::GetAsyncKeyState(VK_RIGHT) & 0x8001))
						{		
							qt_yaw.makeInverse();
							trackball_qtY *= qt_yaw;							
						}
						
					}
                     */

					//절대변환 갱신
					pNodeBall->updateAbsolutePosition();

					//카메라 변환 적용
					{

						trackball_qt = trackball_qtX * trackball_qtY;				

						irr::core::vector3df vDir(0,0,-1); 												
						vDir = trackball_qt * vDir; //벡터회전

						vDir.normalize();
						pCam->setPosition(pNodeBall->getAbsolutePosition() + vDir*fLength);

						irr::core::vector3df vRot;
						trackball_qt.toEuler(vRot);
						vRot *= irr::core::RADTODEG;						
						pCam->setRotation(vRot);							
					}

					//절대변환 갱신
					pCam->updateAbsolutePosition();

					//업벡터와 시점벡터에 변환반영
					{
						irr::core::vector3df vUp(0,1,0);
						pCam->setTarget(pNodeBall->getAbsolutePosition());
						vUp = trackball_qt * vUp; //회전
						pCam->setUpVector(vUp);
					}
                    /*

					if((::GetAsyncKeyState(VK_F1) & 0x8001) == 0x8001)
					{
						pSmgr->setActiveCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/1"));
					}
					if((::GetAsyncKeyState(VK_F2) & 0x8001) == 0x8001)
					{
						pSmgr->setActiveCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2"));
					}
                     */
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}

}


// FSM 응용 예제
//1.6에서 추가되는  hasFinished 함수 사용예

void Lession08_01()
{
	irr::IrrlichtDevice *pDevice = irr::createDevice(					
		irr::video::EDT_OPENGL,
#if(IRRLICHT_VERSION_MAJOR == 1 && IRRLICHT_VERSION_MINOR >= 6 )
		irr::core::dimension2d<irr::u32>(640, 480), 
#else
		irr::core::dimension2d<irr::s32>(640, 480), 
#endif
		32,
		false, false, true,
		NULL
		);

	pDevice->setWindowCaption(L"lesson 08_01");

	irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
	irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
	irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

	pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,30,-140), irr::core::vector3df(0,5,0));	


	irr::scene::IAnimatedMesh *pAniArrow =  pSmgr->addArrowMesh("arrow",
		irr::video::SColor(255,255,0,0),
		irr::video::SColor(255,0,0,255),
		4,8,6,4,3,3			
		);	

	irr::scene::IAnimatedMeshSceneNode *pArrowNode1= pSmgr->addAnimatedMeshSceneNode(pAniArrow);	
	pArrowNode1->setMaterialFlag(irr::video::EMF_LIGHTING,false);

	irr::scene::ISceneNodeAnimator* pAnim;
	int nFsmStatus = 0;

	irr::u32 uLastTick = pDevice->getTimer()->getTime();	

	while(pDevice->run())
	{	
		irr::u32 uTick = pDevice->getTimer()->getTime();
		irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f;

		pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));	

		{
			irr::core::matrix4 mat;//단위행렬로초기화
			pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

			irr::video::SMaterial m;
			m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.
			m.ZBuffer = false;

			pVideo->setMaterial(m);

			pVideo->draw3DLine(irr::core::vector3df(-100,0,0),irr::core::vector3df(100,0,0),irr::video::SColor(0,0,255,0));
			pVideo->draw3DLine(irr::core::vector3df(0,-100,0),irr::core::vector3df(0,100,0),irr::video::SColor(0,0,255,0));
			pVideo->draw3DLine(irr::core::vector3df(0,0,-100),irr::core::vector3df(0,0,100),irr::video::SColor(0,0,255,0));

		}

		//FSM 상태처리
		switch(nFsmStatus)
		{
		case 0: //초기 상태
			{
				pAnim = pSmgr->createFlyStraightAnimator(
					irr::core::vector3df(100,0,0),	//시작위치
					irr::core::vector3df(-100,0,0), //끝위치
					2000,	//수행시간
					false	//루핑여부
					);
				pArrowNode1->addAnimator(pAnim);	
				nFsmStatus = 1; //상태 전이
			}
			break;
		case 1:
			if(pAnim && pAnim->hasFinished() )
			{
				std::cout << "1st animator finished" << std::endl;			
				pArrowNode1->removeAnimator(pAnim);			

				pAnim->drop();
				pAnim = NULL;		

				pAnim = pSmgr->createFlyStraightAnimator(
					irr::core::vector3df(-100,0,0),	//시작위치
					irr::core::vector3df(100,0,0), //끝위치
					2000,	//수행시간
					false	//루핑여부
					);
				pArrowNode1->addAnimator(pAnim);	
				nFsmStatus = 2;
			}						
			break;
		case 2:
			if(pAnim && pAnim->hasFinished() )
			{
				std::cout << "2nd animator finished" << std::endl;			
				pArrowNode1->removeAnimator(pAnim);			

				pAnim->drop();
				pAnim = NULL;		

				pAnim = pSmgr->createFlyStraightAnimator(
					irr::core::vector3df(100,0,0),	//시작위치
					irr::core::vector3df(-100,0,0), //끝위치
					2000,	//수행시간
					false	//루핑여부
					);
				pArrowNode1->addAnimator(pAnim);	
				nFsmStatus = 1;
			}						
			break;
		}

		pSmgr->drawAll();
		pGuiEnv->drawAll();			

		pVideo->endScene();	

		uLastTick = pDevice->getTimer()->getTime();
	}

	pAnim->drop();
	pDevice->drop();
}


