﻿#include <irrlicht.h>
#include <iostream>
#include <map>

namespace lsn09 
{
	class MyApp :  public irr::IEventReceiver
	{
	public:

		std::map<int,std::string> m_mapGuiEventType;

		MyApp()
		{
			m_mapGuiEventType[irr::gui::EGET_ELEMENT_FOCUS_LOST] = "EGET_ELEMENT_FOCUS_LOST";
			m_mapGuiEventType[irr::gui::EGET_ELEMENT_FOCUSED] = "EGET_ELEMENT_FOCUSED";
			m_mapGuiEventType[irr::gui::EGET_ELEMENT_HOVERED] = "EGET_ELEMENT_HOVERED";
			m_mapGuiEventType[irr::gui::EGET_ELEMENT_LEFT] = "EGET_ELEMENT_LEFT";
			m_mapGuiEventType[irr::gui::EGET_ELEMENT_CLOSED] = "EGET_ELEMENT_CLOSED";
			m_mapGuiEventType[irr::gui::EGET_BUTTON_CLICKED] = "EGET_BUTTON_CLICKED";
			m_mapGuiEventType[irr::gui::EGET_SCROLL_BAR_CHANGED] = "EGET_SCROLL_BAR_CHANGED";
			m_mapGuiEventType[irr::gui::EGET_LISTBOX_CHANGED] = "EGET_LISTBOX_CHANGED";
			m_mapGuiEventType[irr::gui::EGET_LISTBOX_SELECTED_AGAIN] = "EGET_LISTBOX_SELECTED_AGAIN";
			m_mapGuiEventType[irr::gui::EGET_FILE_SELECTED] = "EGET_FILE_SELECTED";
			m_mapGuiEventType[irr::gui::EGET_FILE_CHOOSE_DIALOG_CANCELLED] = "EGET_FILE_CHOOSE_DIALOG_CANCELLED";				
			m_mapGuiEventType[irr::gui::EGET_MESSAGEBOX_YES] = "EGET_MESSAGEBOX_YES";				
			m_mapGuiEventType[irr::gui::EGET_MESSAGEBOX_NO] = "EGET_MESSAGEBOX_NO";				
			m_mapGuiEventType[irr::gui::EGET_MESSAGEBOX_OK] = "EGET_MESSAGEBOX_OK";				
			m_mapGuiEventType[irr::gui::EGET_MESSAGEBOX_CANCEL] = "EGET_MESSAGEBOX_CANCEL";				
			m_mapGuiEventType[irr::gui::EGET_EDITBOX_ENTER] = "EGET_EDITBOX_ENTER";				
			m_mapGuiEventType[irr::gui::EGET_TAB_CHANGED] = "EGET_TAB_CHANGED";				
			m_mapGuiEventType[irr::gui::EGET_MENU_ITEM_SELECTED] = "EGET_MENU_ITEM_SELECTED";				
			m_mapGuiEventType[irr::gui::EGET_COMBO_BOX_CHANGED] = "EGET_COMBO_BOX_CHANGED";				
			m_mapGuiEventType[irr::gui::EGET_SPINBOX_CHANGED] = "EGET_SPINBOX_CHANGED";				
		}

		virtual bool OnEvent(const irr::SEvent& event)
		{
			switch(event.EventType)
			{
			case irr::EET_GUI_EVENT:
				{
					printf("%s %d, caller ID:%d \n",
						m_mapGuiEventType[event.GUIEvent.EventType].c_str(),
						event.GUIEvent.EventType,
						event.GUIEvent.Caller->getID()
						);
				}
				break;
			case irr::EET_KEY_INPUT_EVENT:
				{	
					printf("%d\n",event.KeyInput.Key);
				}
				break;
			case irr::EET_MOUSE_INPUT_EVENT:
				{

				}
				break;
			case irr::EET_USER_EVENT:
				break;
			}
			return false;
		}

	};

	void main()
	{
		MyApp myReceiver;

		irr::IrrlichtDevice *pDevice = irr::createDevice(
			irr::video::EDT_OPENGL			
			);

		pDevice->setEventReceiver(&myReceiver);

		pDevice->setWindowCaption(L"gui exam");

		irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
		irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
		irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

		pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,30,-140), irr::core::vector3df(0,5,0));	

		{// 폰트 바꾸기
			irr::gui::IGUISkin* skin = pGuiEnv->getSkin();
			irr::gui::IGUIFont* font = pGuiEnv->getFont("myfont.xml");
			skin->setFont(font,irr::gui::EGDF_DEFAULT);		//모든 출력에 다적용.		
		}

		pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

		//gui elements

		pGuiEnv->addButton(irr::core::rect<irr::s32>(10,210,100,240), 0, 101, L"박한별");	
		pGuiEnv->addCheckBox(false,irr::core::rect<irr::s32>(110,210,200,240),0,102,L"check test");
		pGuiEnv->addEditBox(L"Test",irr::core::rect<irr::s32>(210,210,300,240),true,0,103);	
		pGuiEnv->addImage(pVideo->getTexture("fireBall.bmp"),irr::core::position2di(0,0),true,0,104,L"Image Test");		

		irr::gui::IGUISpinBox *pSpin = pGuiEnv->addSpinBox(L"0",irr::core::rect<irr::s32>(310,210,400,240));
		pSpin->setValue(10.5f);
		pSpin->setStepSize(0.1f);



		irr::u32 uLastTick = pDevice->getTimer()->getTime();	

		while(pDevice->run())
		{			
			pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));	
			pSmgr->drawAll();
			pGuiEnv->drawAll();					
			pVideo->endScene();			
		}

		pDevice->drop();
	}

	// 탭 컨트롤
	namespace _01
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"Gui exam - tab ctrl");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			//리소스 디랙토리
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			{// 폰트 바꾸기
				irr::gui::IGUISkin* skin = pGuiEnv->getSkin();
				irr::gui::IGUIFont* font = pGuiEnv->getFont("lucida.xml");
				skin->setFont(font,irr::gui::EGDF_DEFAULT);		//모든 출력에 다적용.		
			}

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,10,20), irr::core::vector3df(0,5,0));	


			irr::gui::IGUITabControl *pTabCtrl = pGuiEnv->addTabControl(irr::core::recti(10,10,200,300));

			irr::gui::IGUITab* pTab = pTabCtrl->addTab(L"Test1");
			pTab->addChild(pGuiEnv->addStaticText(L"it is sTab1",irr::core::recti(0,0,120,20)));
			pTab = pTabCtrl->addTab(L"Test2");
			pTab->addChild(pGuiEnv->addStaticText(L"it is sTab2",irr::core::recti(0,0,120,20)));
			pTab = pTabCtrl->addTab(L"Test3");
			pTab->addChild(pGuiEnv->addStaticText(L"it is sTab3",irr::core::recti(0,0,120,20)));



			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();

				//직접 그리기
				{
					irr::core::matrix4 mat;//단위행렬로초기화
					mat.makeIdentity();
					pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

					irr::video::SMaterial m;
					m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.
					m.ZBuffer = irr::video::ECFN_NEVER; //깊이 테스트 하지않음

					pVideo->setMaterial(m);		
				}

				//그리기코드추가

				pVideo->endScene();	
			}
			pDevice->drop();
		}
	}

	// 메뉴
	namespace _02 {
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"Gui exam - menu");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			//리소스 디랙토리
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			{// 폰트 바꾸기
				irr::gui::IGUISkin* skin = pGuiEnv->getSkin();
				irr::gui::IGUIFont* font = pGuiEnv->getFont("lucida.xml");
				skin->setFont(font,irr::gui::EGDF_DEFAULT);		//모든 출력에 다적용.		
			}

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,10,20), irr::core::vector3df(0,5,0));	

			irr::gui::IGUIContextMenu* menu = pGuiEnv->addMenu();
			menu->addItem(L"File", -1, true, true);
			menu->addItem(L"View", -1, true, true);
			menu->addItem(L"Camera", -1, true, true);
			menu->addItem(L"Help", -1, true, true);		

			//서브메뉴
			irr::gui::IGUIContextMenu* submenu;
			submenu = menu->getSubMenu(0);
			submenu->addItem(L"Open Model File & Texture...");
			submenu->addItem(L"Set Model Archive...");
			submenu->addItem(L"Load as Octree");
			submenu->addSeparator(); //항목 분리자
			submenu->addItem(L"Quit");


			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();

				//직접 그리기
				{
					irr::core::matrix4 mat;//단위행렬로초기화
					mat.makeIdentity();
					pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

					irr::video::SMaterial m;
					m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.
					m.ZBuffer = irr::video::ECFN_NEVER; //깊이 테스트 하지않음

					pVideo->setMaterial(m);		
				}

				//그리기코드추가

				pVideo->endScene();	
			}
			pDevice->drop();
		}
	}

	//툴바
	namespace _03 {
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"Gui exam - tool bar");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			//리소스 디랙토리
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			{// 폰트 바꾸기
				irr::gui::IGUISkin* skin = pGuiEnv->getSkin();
				irr::gui::IGUIFont* font = pGuiEnv->getFont("lucida.xml");
				skin->setFont(font,irr::gui::EGDF_DEFAULT);		//모든 출력에 다적용.		
			}

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,10,20), irr::core::vector3df(0,5,0));	

			irr::gui::IGUIToolBar* bar = pGuiEnv->addToolBar();

			irr::video::ITexture* image = pVideo->getTexture("open.png");
			bar->addButton(1102, 0, L"Open a model",image, 0, false, true);

			image = pVideo->getTexture("tools.png");
			bar->addButton(1104, 0, L"Open Toolset",image, 0, false, true);

			image = pVideo->getTexture("zip.png");
			bar->addButton(1105, 0, L"Set Model Archive",image, 0, false, true);

			image = pVideo->getTexture("help.png");
			bar->addButton(1103, 0, L"Open Help", image, 0, false, true);


			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();

				//직접 그리기
				{
					irr::core::matrix4 mat;//단위행렬로초기화
					mat.makeIdentity();
					pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

					irr::video::SMaterial m;
					m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.
					m.ZBuffer = irr::video::ECFN_NEVER; //깊이 테스트 하지않음

					pVideo->setMaterial(m);		
				}

				//그리기코드추가

				pVideo->endScene();	
			}
			pDevice->drop();
		}
	}

	//list box
	namespace _04 {

		class CMyReceiver :  public irr::IEventReceiver
		{			
		public: 

			CMyReceiver(irr::gui::IGUIElement *pRootElement)
				:m_pRootElement(pRootElement)
			{	}
			
			irr::gui::IGUIElement *m_pRootElement;

			virtual bool OnEvent(const irr::SEvent& event)
			{
				switch(event.EventType)
				{
				case irr::EET_GUI_EVENT:
					{
						printf("%d, caller ID:%d \n",
							event.GUIEvent.EventType,
							event.GUIEvent.Caller->getID()
							);

						if(event.GUIEvent.EventType == irr::gui::EGET_LISTBOX_CHANGED)
						{
							if(event.GUIEvent.Caller->getID() == 1000)
							{
								irr::gui::IGUIListBox *pList = 
									static_cast<irr::gui::IGUIListBox *> (event.GUIEvent.Caller);

								irr::s32 nSel = pList->getSelected();

								
								
								m_pRootElement->getElementFromId(1001)->setText(
									pList->getListItem(nSel));
							}
						}
					}
					break;
				case irr::EET_KEY_INPUT_EVENT:
					{
					}
					break;
				case irr::EET_MOUSE_INPUT_EVENT:
					{
					}
					break;
				case irr::EET_USER_EVENT:
					break;
				}
				return false;
			}
		};  


		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);
			

			pDevice->setWindowCaption(L"Gui exam - tool bar");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			CMyReceiver MyRev(pGuiEnv->getRootGUIElement());
			pDevice->setEventReceiver(&MyRev);

			//리소스 디랙토리
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			{// 폰트 바꾸기
				irr::gui::IGUISkin* skin = pGuiEnv->getSkin();
				irr::gui::IGUIFont* font = pGuiEnv->getFont("lucida.xml");
				skin->setFont(font,irr::gui::EGDF_DEFAULT);		//모든 출력에 다적용.		
			}

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,10,20), irr::core::vector3df(0,5,0));	

			irr::gui::IGUIListBox *pList = pGuiEnv->addListBox(irr::core::recti(100,50,200,150),0,1000);
			pList->setDrawBackground(true);
			pList->addItem(L"Item 1");
			pList->addItem(L"Item 2");
			pList->addItem(L"Item 3");
			pList->addItem(L"Item 4");
			pList->addItem(L"Item 5");
			pList->addItem(L"Item 6");
			pList->addItem(L"Item 7");
			pList->addItem(L"Item 8");
			pList->addItem(L"Item 9");
			pList->addItem(L"Item 10");

			//선택 보기용 텍스트창
			pGuiEnv->addStaticText(L"--",irr::core::recti(250,0,350,20),true,true,0,1001,true);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();

				//직접 그리기
				{
					irr::core::matrix4 mat;//단위행렬로초기화
					mat.makeIdentity();
					pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

					irr::video::SMaterial m;
					m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.
					m.ZBuffer = irr::video::ECFN_NEVER; //깊이 테스트 하지않음

					pVideo->setMaterial(m);		
				}

				//그리기코드추가

				pVideo->endScene();	
			}
			pDevice->drop();
		}
	}

	//스크롤바
	namespace _05 {

		class CMyReceiver :  public irr::IEventReceiver
		{			
		public: 

			CMyReceiver(irr::gui::IGUIElement *pRootElement)
				:m_pRootElement(pRootElement)
			{	}
			
			irr::gui::IGUIElement *m_pRootElement;

			virtual bool OnEvent(const irr::SEvent& event)
			{
				switch(event.EventType)
				{
				case irr::EET_GUI_EVENT:
					{
						printf("%d, caller ID:%d \n",
							event.GUIEvent.EventType,
							event.GUIEvent.Caller->getID()
							);

						if(event.GUIEvent.EventType == irr::gui::EGET_SCROLL_BAR_CHANGED)
						{
							if(event.GUIEvent.Caller->getID() == 1000)
							{
								irr::gui::IGUIScrollBar *pElem = 
									static_cast<irr::gui::IGUIScrollBar *> (event.GUIEvent.Caller);							

								wchar_t szwBuf[256];
								swprintf(szwBuf,256,L"%d",pElem->getPos());
								m_pRootElement->getElementFromId(1001)->setText(	
									szwBuf
									);
							}
						}
					}
					break;
				case irr::EET_KEY_INPUT_EVENT:
					{
					}
					break;
				case irr::EET_MOUSE_INPUT_EVENT:
					{
					}
					break;
				case irr::EET_USER_EVENT:
					break;
				}
				return false;
			}
		};  


		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);
			

			pDevice->setWindowCaption(L"Gui exam - tool bar");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			CMyReceiver MyRev(pGuiEnv->getRootGUIElement());
			pDevice->setEventReceiver(&MyRev);

			//리소스 디랙토리
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			{// 폰트 바꾸기
				irr::gui::IGUISkin* skin = pGuiEnv->getSkin();
				irr::gui::IGUIFont* font = pGuiEnv->getFont("lucida.xml");
				skin->setFont(font,irr::gui::EGDF_DEFAULT);		//모든 출력에 다적용.		
			}

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,10,20), irr::core::vector3df(0,5,0));	

			irr::gui::IGUIScrollBar *pScrol = pGuiEnv->addScrollBar(true,
				irr::core::recti(100,50,200,70),0,1000);

			pScrol->setMax(256);
			pScrol->setMin(50);
			pScrol->setPos(100);
			

			//선택 보기용 텍스트창
			pGuiEnv->addStaticText(L"--",irr::core::recti(250,0,350,20),true,true,0,1001,true);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();

				//직접 그리기
				{
					irr::core::matrix4 mat;//단위행렬로초기화
					mat.makeIdentity();
					pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

					irr::video::SMaterial m;
					m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.
					m.ZBuffer = irr::video::ECFN_NEVER; //깊이 테스트 하지않음

					pVideo->setMaterial(m);		
				}

				//그리기코드추가

				pVideo->endScene();	
			}
			pDevice->drop();
		}
	}

	//파일 다이얼로그
	namespace _06 {

		class CMyReceiver :  public irr::IEventReceiver
		{			
		public: 

			CMyReceiver(irr::gui::IGUIElement *pRootElement)
				:m_pRootElement(pRootElement)
			{}
			
			irr::gui::IGUIElement *m_pRootElement;

			virtual bool OnEvent(const irr::SEvent& event)
			{
				switch(event.EventType)
				{
				case irr::EET_GUI_EVENT:
					{
						printf("%d, caller ID:%d \n",
							event.GUIEvent.EventType,
							event.GUIEvent.Caller->getID()
							);		

						if(event.GUIEvent.EventType == irr::gui::EGET_FILE_SELECTED)
						{
							if(event.GUIEvent.Caller->getID() == 1000)
							{
								irr::gui::IGUIFileOpenDialog *pElem = 
									static_cast<irr::gui::IGUIFileOpenDialog *> (event.GUIEvent.Caller);	
								irr::core::stringw strw = pElem->getFileName();
								m_pRootElement->getElementFromId(1001)->setText(	
									strw.c_str()
									//pElem->getFileName()
									);

							}
						}
					}
					break;
				case irr::EET_KEY_INPUT_EVENT:
					{
					}
					break;
				case irr::EET_MOUSE_INPUT_EVENT:
					{
					}
					break;
				case irr::EET_USER_EVENT:
					break;
				}
				return false;
			}
		};  


		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);
			

			pDevice->setWindowCaption(L"Gui exam - tool bar");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			CMyReceiver MyRev(pGuiEnv->getRootGUIElement());
			pDevice->setEventReceiver(&MyRev);

			//리소스 디랙토리
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			{// 폰트 바꾸기
				irr::gui::IGUISkin* skin = pGuiEnv->getSkin();
				irr::gui::IGUIFont* font = pGuiEnv->getFont("lucida.xml");
				skin->setFont(font,irr::gui::EGDF_DEFAULT);		//모든 출력에 다적용.		
			}

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,10,20), irr::core::vector3df(0,5,0));	

			pGuiEnv->addFileOpenDialog(L"Please select a model file to open",true,0,1000);		

			//선택 보기용 텍스트창
			{
				irr::gui::IGUIStaticText *ptex = pGuiEnv->addStaticText(L"--",irr::core::recti(0,0,640,60),true,true,0,1001,true);				
			}

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();

				//직접 그리기
				{
					irr::core::matrix4 mat;//단위행렬로초기화
					mat.makeIdentity();
					pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

					irr::video::SMaterial m;
					m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.
					m.ZBuffer = irr::video::ECFN_NEVER; //깊이 테스트 하지않음

					pVideo->setMaterial(m);		
				}

				//그리기코드추가

				pVideo->endScene();	
			}
			pDevice->drop();
		}
	}

	//이미지버튼
	namespace _07
	{
		class CMyReceiver :  public irr::IEventReceiver
		{			
		public: 

			CMyReceiver(irr::gui::IGUIElement *pRootElement,irr::video::IVideoDriver *pVideo)
				:m_pRootElement(pRootElement),m_pVideo(pVideo)
			{}
			
			irr::video::IVideoDriver *m_pVideo;
			irr::gui::IGUIElement *m_pRootElement;

			virtual bool OnEvent(const irr::SEvent& event)
			{
				switch(event.EventType)
				{
				case irr::EET_GUI_EVENT:
					{
						printf("%d, caller ID:%d \n",
							event.GUIEvent.EventType,
							event.GUIEvent.Caller->getID()
							);		

						//버튼올리기
						if(event.GUIEvent.EventType == irr::gui::EGET_ELEMENT_LEFT)
						{
							if(event.GUIEvent.Caller->getID() == 100)
							{
								irr::gui::IGUIButton *pBtn = (irr::gui::IGUIButton *)event.GUIEvent.Caller;
								pBtn->setImage(m_pVideo->getTexture("portal1.bmp"));
							}
						}
						if(event.GUIEvent.EventType == irr::gui::EGET_ELEMENT_HOVERED)
						{	
							if(event.GUIEvent.Caller->getID() == 100)
							{
								irr::gui::IGUIButton *pBtn = (irr::gui::IGUIButton *)event.GUIEvent.Caller;
								pBtn->setImage(m_pVideo->getTexture("portal3.bmp"));
							}
						}
					}
					break;
				case irr::EET_KEY_INPUT_EVENT:
					{
					}
					break;
				case irr::EET_MOUSE_INPUT_EVENT:
					{
					}
					break;
				case irr::EET_USER_EVENT:
					break;
				}
				return false;
			}
		};  
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"Gui exam - tab ctrl");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			CMyReceiver myRev(pGuiEnv->getRootGUIElement(),pVideo);

			pDevice->setEventReceiver(&myRev);


			//리소스 디랙토리
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			{// 폰트 바꾸기
				irr::gui::IGUISkin* skin = pGuiEnv->getSkin();
				irr::gui::IGUIFont* font = pGuiEnv->getFont("lucida.xml");
				skin->setFont(font,irr::gui::EGDF_DEFAULT);		//모든 출력에 다적용.		
			}


			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,10,20), irr::core::vector3df(0,5,0));			

			{
				irr::gui::IGUIButton *pBtn = pGuiEnv->addButton(irr::core::recti(0,0,64,64),0,100);								
				pBtn->setImage(pVideo->getTexture("portal1.bmp"));
				pBtn->setPressedImage(pVideo->getTexture("portal2.bmp"));			
			}	


			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();

				//직접 그리기
				{
					irr::core::matrix4 mat;//단위행렬로초기화
					mat.makeIdentity();
					pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

					irr::video::SMaterial m;
					m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.
					m.ZBuffer = irr::video::ECFN_NEVER; //깊이 테스트 하지않음

					pVideo->setMaterial(m);		
				}

				//그리기코드추가

				pVideo->endScene();	
			}
			pDevice->drop();
		}
	}

	//addInOutFader
	namespace _08
	{
		class CMyReceiver :  public irr::IEventReceiver
		{			
		public: 

			CMyReceiver(irr::gui::IGUIElement *pRootElement,irr::video::IVideoDriver *pVideo)
				:m_pRootElement(pRootElement),m_pVideo(pVideo)
			{}
			
			irr::video::IVideoDriver *m_pVideo;
			irr::gui::IGUIElement *m_pRootElement;

			virtual bool OnEvent(const irr::SEvent& event)
			{
				switch(event.EventType)
				{
				case irr::EET_GUI_EVENT:
					{
						printf("%d, caller ID:%d \n",
							event.GUIEvent.EventType,
							event.GUIEvent.Caller->getID()
							);		

						//버튼올리기
						if(event.GUIEvent.EventType == irr::gui::EGET_ELEMENT_LEFT)
						{
							if(event.GUIEvent.Caller->getID() == 100)
							{
								irr::gui::IGUIButton *pBtn = (irr::gui::IGUIButton *)event.GUIEvent.Caller;
								pBtn->setImage(m_pVideo->getTexture("portal1.bmp"));
							}
						}
						if(event.GUIEvent.EventType == irr::gui::EGET_ELEMENT_HOVERED)
						{	
							if(event.GUIEvent.Caller->getID() == 100)
							{
								irr::gui::IGUIButton *pBtn = (irr::gui::IGUIButton *)event.GUIEvent.Caller;
								pBtn->setImage(m_pVideo->getTexture("portal3.bmp"));
							}
						}
						if(event.GUIEvent.EventType == irr::gui::EGET_BUTTON_CLICKED)
						{
							irr::gui::IGUIButton *pBtn = (irr::gui::IGUIButton *)event.GUIEvent.Caller;
							irr::gui::IGUIInOutFader *pFader = 
								(irr::gui::IGUIInOutFader *)pBtn->getElementFromId(200);
							pFader->fadeOut(3000);
						}
					}
					break;
				case irr::EET_KEY_INPUT_EVENT:
					{
					}
					break;
				case irr::EET_MOUSE_INPUT_EVENT:
					{
					}
					break;
				case irr::EET_USER_EVENT:
					break;
				}
				return false;
			}
		};  
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"Gui exam - tab ctrl");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			CMyReceiver myRev(pGuiEnv->getRootGUIElement(),pVideo);

			pDevice->setEventReceiver(&myRev);


			//리소스 디랙토리
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			{// 폰트 바꾸기
				irr::gui::IGUISkin* skin = pGuiEnv->getSkin();
				irr::gui::IGUIFont* font = pGuiEnv->getFont("lucida.xml");
				skin->setFont(font,irr::gui::EGDF_DEFAULT);		//모든 출력에 다적용.		
			}


			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,10,20), irr::core::vector3df(0,5,0));			

			{
				irr::gui::IGUIButton *pBtn = pGuiEnv->addButton(irr::core::recti(0,0,64,64),0,100);								
				pBtn->setImage(pVideo->getTexture("portal1.bmp"));
				pBtn->setPressedImage(pVideo->getTexture("portal2.bmp"));	
				irr::gui::IGUIInOutFader *pFader = pGuiEnv->addInOutFader(0,pBtn,200);						
			}			 

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();

				//직접 그리기
				{
					irr::core::matrix4 mat;//단위행렬로초기화
					mat.makeIdentity();
					pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

					irr::video::SMaterial m;
					m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.
					m.ZBuffer = irr::video::ECFN_NEVER; //깊이 테스트 하지않음

					pVideo->setMaterial(m);		
				}

				//그리기코드추가

				pVideo->endScene();	
			}
			pDevice->drop();
		}
	}

	//스킨예제
	namespace _09
	{
		class CMyReceiver :  public irr::IEventReceiver
		{			
		public: 

			CMyReceiver(irr::IrrlichtDevice *pDevice)
				:m_pDevice(pDevice)
			{}

			irr::IrrlichtDevice *m_pDevice;			

			virtual bool OnEvent(const irr::SEvent& event)
			{
				switch(event.EventType)
				{
				case irr::EET_GUI_EVENT:
					{
						printf("%d, caller ID:%d \n",
							event.GUIEvent.EventType,
							event.GUIEvent.Caller->getID()
							);							
					}
					break;
				case irr::EET_KEY_INPUT_EVENT:
					{
					}
					break;
				case irr::EET_MOUSE_INPUT_EVENT:
					{
					}
					break;
				case irr::EET_USER_EVENT:
					break;
				}
				return false;
			}
		};  
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"Gui exam - tab ctrl");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			CMyReceiver myRev(pDevice);

			pDevice->setEventReceiver(&myRev);


			//리소스 디랙토리
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			{// 폰트 바꾸기
				irr::gui::IGUISkin* skin = pGuiEnv->getSkin();
				irr::gui::IGUIFont* font = pGuiEnv->getFont("lucida.xml");
				skin->setFont(font,irr::gui::EGDF_DEFAULT);		//모든 출력에 다적용.		

				//면의 색깔 바꾸기
				skin->setColor(irr::gui::EGDC_3D_FACE,irr::video::SColor(255,255,0,0));
			}


			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,10,20), irr::core::vector3df(0,5,0));			

			{
				irr::gui::IGUIButton *pBtn = pGuiEnv->addButton(irr::core::recti(0,0,64,64),0,100);																
				pBtn->setText(L"Test");
			}			 

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();

				//직접 그리기
				{
					irr::core::matrix4 mat;//단위행렬로초기화
					mat.makeIdentity();
					pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

					irr::video::SMaterial m;
					m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.
					m.ZBuffer = irr::video::ECFN_NEVER; //깊이 테스트 하지않음

					pVideo->setMaterial(m);		
				}

				//그리기코드추가

				pVideo->endScene();	
			}
			pDevice->drop();
		}
	}



}