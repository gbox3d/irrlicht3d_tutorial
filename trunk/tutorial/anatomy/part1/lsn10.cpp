﻿//#include "part1.h"

#include <irrlicht.h>
#include <iostream>


//#include <SDKDDKVer.h>
//#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
//#include <windows.h>

//stl header
#include <vector>
//#include <map>

irr::core::position2di g_ImpPos;
irr::core::rect<irr::s32> g_Imp[2] =
{ 
	irr::core::rect<irr::s32>(349,15,385,78),
	irr::core::rect<irr::s32>(387,15,423,78)
};


class MyApp10 :  public irr::IEventReceiver
{
public:	

	virtual bool OnEvent(const irr::SEvent& event)
	{		
		switch(event.EventType)
		{
			//GUI이벤트
		case irr::EET_GUI_EVENT:
			{

			}
			break;
			//키보드 이벤트
		case irr::EET_KEY_INPUT_EVENT:
			{
				if(event.KeyInput.Key == irr::KEY_LEFT)
				{
					g_ImpPos.X--;
				}
				if(event.KeyInput.Key == irr::KEY_RIGHT)
				{
					g_ImpPos.X++;
				}					
				//printf("%d \n",event.KeyInput.Key,g_ImpPos.X);
			}
			break;
			//마우스이벤트
		case irr::EET_MOUSE_INPUT_EVENT:
			{

			}
			break;
		case irr::EET_USER_EVENT:
			break;
		}
		return false;
	}

};

MyApp10 g_App10;

void Lession10()
{
	irr::IrrlichtDevice *pDevice = irr::createDevice(
		irr::video::EDT_OPENGL,
#if(IRRLICHT_VERSION_MAJOR == 1 && IRRLICHT_VERSION_MINOR >= 6 )
		irr::core::dimension2d<irr::u32>(640, 480), 
#else
		irr::core::dimension2d<irr::s32>(640, 480), 
#endif
		32,
		false, false, true,
		&g_App10
		);

	pDevice->setWindowCaption(L"lesson 10");

	irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
	irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
	irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

	pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,30,-140), irr::core::vector3df(0,5,0));		

	irr::video::ITexture* pImage = pVideo->getTexture("../res/irr_exam/2ddemo.bmp");
	pVideo->makeColorKeyTexture(pImage, irr::core::position2d<irr::s32>(0,0)); //위치로컬러키지정

	//폰트얻기
	irr::gui::IGUIFont* font = pDevice->getGUIEnvironment()->getBuiltInFont();
	irr::gui::IGUIFont* pFont2 = pDevice->getGUIEnvironment()->getFont("../res/irr_exam/myfont.xml");

	//pGuiEnv->addEditBox(L"Test",irr::core::rect<irr::s32>(210,210,300,240),true,0,103);



	irr::u32 uLastTick = pDevice->getTimer()->getTime();	
	irr::u32 uFrame=0;
	irr::f32 fTick=0.0;
	g_ImpPos = irr::core::position2d<irr::s32>(164,125);

	while(pDevice->run())
	{	
		irr::u32 uTick = pDevice->getTimer()->getTime();
		irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f;	



		pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));		

		fTick += fDelta;

		if(fTick > .5f)
		{
			uFrame++;
			uFrame %=2;
			fTick = 0;
		}


		pVideo->draw2DImage(pImage,
			g_ImpPos,
			g_Imp[uFrame],
			0,
			irr::video::SColor(255,255,255,255),true);

		pSmgr->drawAll();
		pGuiEnv->drawAll();			

		pFont2->draw(L"hello가나다라마바사",irr::core::rect<irr::s32>(360,18,460,38),irr::video::SColor(255,255,0,0));
		pFont2->draw(L"hello",irr::core::rect<irr::s32>(360,38,460,68),irr::video::SColor(255,0,0,255));
		pFont2->draw(L"hello",irr::core::rect<irr::s32>(360,68,460,98),irr::video::SColor(255,0,255,0));

		pVideo->endScene();	

		uLastTick = pDevice->getTimer()->getTime();
	}

	pDevice->drop();
}

namespace lsn10
{
	//스프라이트 예제
	namespace _00
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL				
				);

			pDevice->setWindowCaption(L"2d exam");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			//밉멥속성제거
			pVideo->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS,false);
			irr::video::ITexture* pImage = pVideo->getTexture("2ddemo.bmp");
			//위치로 컬러키값을 지정
			//해당위치에 있는 픽샐이 컬러키가되며 알파체널이 0값으로 바뀐다.
			pVideo->makeColorKeyTexture(pImage, irr::core::position2d<irr::s32>(0,0)); 

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				//투명컬러적용
				pVideo->draw2DImage(pImage,
					irr::core::vector2di(100,70), //찍힐위치
					irr::core::recti(349,15,385,78), //원본위치
					0, //절단영역0이면 전체화면크기
					irr::video::SColor(255,255,255,255), //기본컬러
					true //투명컬러사용여부
					);

				//투명컬러비적용
				pVideo->draw2DImage(pImage,
					irr::core::vector2di(100,140),
					irr::core::recti(349,15,385,78),
					0,
					irr::video::SColor(255,255,255,255),
					false);

				//알파블랜딩
				pVideo->draw2DImage(pImage,
					irr::core::vector2di(100,210),
					irr::core::recti(349,15,385,78),
					0,
					irr::video::SColor(128,255,255,255),
					false);

				pVideo->draw2DImage(pImage,
					irr::core::vector2di(100,280),
					irr::core::recti(349,15,385,78),
					0,
					irr::video::SColor(128,255,255,255),
					true);

				// 스캐일링
				pVideo->draw2DImage(pImage,
					irr::core::recti(200,70,200+36*2,70+63*2),
					irr::core::recti(349,15,385,78),
					0,
					0,
					true);

				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}

	
	//폰트예제
	namespace _01
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				//irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"2d exam01");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			irr::gui::IGUIFont* pFont = pDevice->getGUIEnvironment()->getFont("lucida.xml");
			

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pFont->draw(L"Hello World ^^;",irr::core::rect<irr::s32>(320,200,460,68),irr::video::SColor(255,255,255,0));

				//pVideo->draw2DPolygon(irr::core::vector2di(100,100),50);

				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}

	
	//2D triangle list
	namespace _02
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				//irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"2d exam01");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");				

			//밉멥속성제거
			pVideo->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS,false);
			//이미 알파체널이 존재하면 따로 컬러키를 정해줄필요가 없다.
			irr::video::ITexture* pImage = pVideo->getTexture("irrlichtlogoalpha2.tga");
			

			std::vector<irr::video::S3DVertex> Vertices;

			Vertices.push_back(irr::video::S3DVertex(0,0,0, 0,0,-1,irr::video::SColor(255,255,255,255),0,0) );
			Vertices.push_back(irr::video::S3DVertex(128,0,0, 0,0,-1,irr::video::SColor(255,255,255,255),1,0) );
			Vertices.push_back(irr::video::S3DVertex(128,128,0, 0,0,-1,irr::video::SColor(255,255,255,255),1,1) );
			Vertices.push_back(irr::video::S3DVertex(0,128,0, 0,0,-1,irr::video::SColor(255,255,255,255),0,1) );
			

			std::vector<irr::u16> Indice4TriList;

			 
			 //인덱스 배열만들기
			Indice4TriList.push_back(0);
			Indice4TriList.push_back(1);
			Indice4TriList.push_back(2);
			Indice4TriList.push_back(3);
			Indice4TriList.push_back(0);
			Indice4TriList.push_back(2);

			//위치변환
			irr::core::matrix4 mat;
			mat.setTranslation(irr::core::vector3df(200,200,0));			

			int i;
			for(i=0;i<4;i++)
			{
				mat.transformVect(Vertices[i].Pos);
			}

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();			

				{
					irr::video::SMaterial m;
					m.setTexture(0,pImage);
					pVideo->setMaterial(m);	
				}		
				

				pVideo->draw2DVertexPrimitiveList(
					(irr::video::S3DVertex *)(&Vertices[0]),
					4,
					(irr::u16 *)&Indice4TriList[0],
					2,
					irr::video::EVT_STANDARD,
					irr::scene::EPT_TRIANGLES
					);


				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}

	//2D triangle list 이용하기 2.
	// 회전 변환예
	namespace _03
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				//irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"2d exam01");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");				

			//밉멥속성제거
			pVideo->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS,false);
			//이미 알파체널이 존재하면 따로 컬러키를 정해줄필요가 없다.
			irr::video::ITexture* pImage = pVideo->getTexture("irrlichtlogoalpha2.tga");
			

			std::vector<irr::video::S3DVertex> Vertices;

			Vertices.push_back(irr::video::S3DVertex(0,0,0, 0,0,-1,irr::video::SColor(255,255,255,255),0,0) );
			Vertices.push_back(irr::video::S3DVertex(128,0,0, 0,0,-1,irr::video::SColor(255,255,255,255),1,0) );
			Vertices.push_back(irr::video::S3DVertex(128,128,0, 0,0,-1,irr::video::SColor(255,255,255,255),1,1) );
			Vertices.push_back(irr::video::S3DVertex(0,128,0, 0,0,-1,irr::video::SColor(255,255,255,255),0,1) );
			

			std::vector<irr::u16> Indice4TriList;

			 
			 //인덱스 배열만들기
			Indice4TriList.push_back(0);
			Indice4TriList.push_back(1);
			Indice4TriList.push_back(2);
			Indice4TriList.push_back(3);
			Indice4TriList.push_back(0);
			Indice4TriList.push_back(2);

			

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();			

				{
					irr::video::SMaterial m;
					m.setTexture(0,pImage);
					pVideo->setMaterial(m);	
				}		
				

				{
					static irr::f32 rot = 0;
					std::vector<irr::video::S3DVertex> outVertices = Vertices;

					//위치변환
					irr::core::matrix4 mat;
					mat.setTranslation(irr::core::vector3df(200,200,0));//z always 0

					irr::core::matrix4 mat_r; //회전
					rot += fDelta*45.f;					
					mat_r.setRotationDegrees(irr::core::vector3df(0,0,rot));

					irr::core::matrix4 mat_base; //회전 중심점
					mat_base.setTranslation(irr::core::vector3df(-64,-64,0));					

					irr::core::matrix4 mat_scr;//크기
					mat_scr.setScale(irr::core::vector3df(2,2,2));
					
					// 로컬1(중심점 이동변환) * 로컬2(회전크기변환) * 월드변환
					mat = mat * ( mat_r * mat_scr)  * mat_base;

					int i;
					for(i=0;i<4;i++)
					{
						mat.transformVect(outVertices[i].Pos);
					}


					pVideo->draw2DVertexPrimitiveList(
						(irr::video::S3DVertex *)(&outVertices[0]),
						4,
						(irr::u16 *)&Indice4TriList[0],
						2,
						irr::video::EVT_STANDARD,
						irr::scene::EPT_TRIANGLES
						);
				}		


				{
					static irr::f32 rot = 0;
					std::vector<irr::video::S3DVertex> outVertices = Vertices;

					//위치변환
					irr::core::matrix4 mat;
					mat.setTranslation(irr::core::vector3df(200,200,0));	

					irr::core::matrix4 mat_r;
					rot += fDelta*15.f;					
					mat_r.setRotationDegrees(irr::core::vector3df(0,0,rot));

					irr::core::matrix4 mat_base; //중심점
					mat_base.setTranslation(irr::core::vector3df(0,0,0));

					mat = mat * (mat_r *mat_base);


					int i;
					for(i=0;i<4;i++)
					{
						mat.transformVect(outVertices[i].Pos);
					}


					pVideo->draw2DVertexPrimitiveList(
						(irr::video::S3DVertex *)(&outVertices[0]),
						4,
						(irr::u16 *)&Indice4TriList[0],
						2,
						irr::video::EVT_STANDARD,
						irr::scene::EPT_TRIANGLES
						);
				}		


				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}	


	//2디 도형 그리기함수
	namespace _04
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				//irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"2d exam01");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");		
			

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();						

				//폴리곤
				pVideo->draw2DPolygon(irr::core::vector2di(100,100),50);
				pVideo->draw2DPolygon(irr::core::vector2di(100,100),50,
					irr::video::SColor(255,255,0,0),4);
				pVideo->draw2DPolygon(irr::core::vector2di(100,100),50,
					irr::video::SColor(255,255,255,0),3);

				//직선
				pVideo->draw2DLine(irr::core::vector2di(200,100),
					irr::core::vector2di(250,200));

				//사각형
				pVideo->draw2DRectangle(
					irr::video::SColor(255,0,255,0),
					irr::core::recti(300,100,400,150));

				//에너지바
				pVideo->draw2DRectangle(
					irr::core::recti(300,200,400,220),
					irr::video::SColor(255,255,0,0),
					irr::video::SColor(255,255,255,0),
					irr::video::SColor(255,255,0,0),	
					irr::video::SColor(255,255,255,0)									
					);

				pVideo->draw2DRectangleOutline(irr::core::recti(300,200,500,220));
				pVideo->endScene();	
			}
			pDevice->drop();
		}
	}
}
