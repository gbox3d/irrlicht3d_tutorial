﻿//#include "part1.h"

#include <irrlicht.h>
#include <iostream>


//#include <SDKDDKVer.h>
//#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
//#include <windows.h>

//stl header
//#include <vector>
//#include <map>




//충돌 처리
//픽킹,바운딩박스, 선택자

//class Lsn11_App :  public irr::IEventReceiver
//{
//public:
//
//	irr::scene::ISceneManager *m_pSmgr;// = pDevice->getSceneManager();	
//	irr::scene::IAnimatedMeshSceneNode *m_pPickNode;
//
//	Lsn11_App()
//	{
//		m_pPickNode = NULL;
//
//	}
//
//	virtual bool OnEvent(const irr::SEvent& event)
//	{
//		switch(event.EventType)
//		{
//		case irr::EET_GUI_EVENT:
//			{					
//			}
//			break;
//		case irr::EET_KEY_INPUT_EVENT:
//			{					
//				irr::scene::ISceneNode *pNode1 = m_pSmgr->getSceneNodeFromName("syndy1");
//				irr::scene::ISceneNode *pNode2 = m_pSmgr->getSceneNodeFromName("syndy2");
//
//				switch(event.KeyInput.Key)
//				{
//				case irr::KEY_UP:
//					{
//						irr::core::vector3df vPos = pNode1->getPosition();
//						vPos += irr::core::vector3df(1,0,0);
//						pNode1->setPosition(vPos);
//					}
//					break;
//				case irr::KEY_DOWN:
//					{
//						irr::core::vector3df vPos = pNode1->getPosition();
//						vPos += irr::core::vector3df(-1,0,0);
//						pNode1->setPosition(vPos);
//					}
//					break;
//				}					
//			}
//			break;
//		case irr::EET_MOUSE_INPUT_EVENT:
//			{
//				if(event.MouseInput.Event == irr::EMIE_LMOUSE_LEFT_UP)
//				{
//					if(m_pPickNode)
//					{
//						m_pPickNode->setMD2Animation(irr::scene::EMAT_DEATH_FALLBACK);
//						m_pPickNode->setLoopMode(false);
//					}
//				}
//			}
//			break;
//		case irr::EET_USER_EVENT:
//			break;
//		}
//		return false;
//	}
//
//};
//
//Lsn11_App g_App;
//
//void getAnimateBoundingBox(irr::scene::IAnimatedMeshSceneNode *pAniSceneNode,irr::core::aabbox3df &box)
//{	
//	box = pAniSceneNode->getMesh()->getMesh((int)pAniSceneNode->getFrameNr())->getBoundingBox();
//	pAniSceneNode->getAbsoluteTransformation().transformBox(box);	
//}
//
//
//void Lession11()
//{
//	irr::IrrlichtDevice *pDevice = irr::createDevice(					
//		irr::video::EDT_OPENGL,
//#if(IRR_VERSION == 16)
//		irr::core::dimension2d<irr::u32>(640, 480), 
//#else
//		irr::core::dimension2d<irr::s32>(640, 480), 
//#endif
//		32,
//		false, false, true,
//		&g_App
//		);
//
//	pDevice->setWindowCaption(L"Lesson11");
//
//	irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
//	irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
//	irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
//
//	g_App.m_pSmgr = pSmgr;
//
//
//	//irr::scene::ICameraSceneNode *pCam = pSmgr->addCameraSceneNodeMaya();
//	//pCam->setTarget(irr::core::vector3df(0,5,0));
//	//pCam->setPosition(irr::core::vector3df(0,160,-200));
//
//	irr::scene::ICameraSceneNode *pCam = pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,120,-200), irr::core::vector3df(0,5,0));	
//
//	pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");
//
//
//
//	{
//		irr::scene::IAnimatedMesh *pAniMesh = pSmgr->getMesh("sydney.md2");
//		irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(pAniMesh);
//		pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);	
//		pNode->setMD2Animation ( irr::scene::EMAT_CROUCH_WALK );				
//		pNode->setMaterialTexture(0,pVideo->getTexture("sydney.bmp"));	
//		pNode->setPosition(irr::core::vector3df(-20,60,0));
//		pNode->setName("syndy1");
//
//		pNode = pSmgr->addAnimatedMeshSceneNode(pAniMesh);
//		pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);		
//		pNode->setMD2Animation ( irr::scene::EMAT_STAND );		
//		pNode->setMaterialTexture(0,pVideo->getTexture("sydney.bmp"));				
//		pNode->setPosition(irr::core::vector3df(20,40,0));		
//		pNode->setName("syndy2");
//	}
//
//	{		
//		irr::scene::ITriangleSelector *pSelector;
//		irr::scene::IAnimatedMesh *pAniMesh = pSmgr->getMesh("room.3ds");
//		pSmgr->getMeshManipulator()->makePlanarTextureMapping(
//			pAniMesh->getMesh(0), 0.004f);
//		irr::scene::ISceneNode *pNode = pSmgr->addOctreeSceneNode(pAniMesh->getMesh(0));
//
//		pNode->setMaterialTexture(0, pVideo->getTexture("wall.jpg"));
//		pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);	
//
//		pNode->setPosition(irr::core::vector3df(0,0,0));
//		pNode->setName("room");		
//
//		pSelector = pSmgr->createOctTreeTriangleSelector(pAniMesh->getMesh(0),pNode);
//		pNode->setTriangleSelector(pSelector);		
//
//		pSelector->drop();
//	}
//
//
//
//	{
//		irr::scene::IAnimatedMesh *pAniMesh = pSmgr->getMesh("jga/sign.3DS");
//		irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(pAniMesh);		
//		pNode->setMaterialTexture(0, pVideo->getTexture("jga/sign.tga"));
//		pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);		
//		pNode->setName("Arrow");		
//		pNode->setVisible(false);
//	}
//
//
//
//
//	irr::scene::ISceneNode *pPrePickNode = 0;
//	while(pDevice->run())
//	{		
//		pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));	
//
//		{
//			irr::core::position2di mouse_pos = pDevice->getCursorControl()->getPosition();
//
//			irr::core::line3df Ray;			
//			irr::core::vector3df v3Intersec;
//			irr::core::triangle3df tri;
//			irr::scene::ITriangleSelector *pSelector;
//
//			//2디 스크린좌표계로부터 피킹레이 얻기
//			Ray = pSmgr->getSceneCollisionManager()->getRayFromScreenCoordinates(mouse_pos);			
//
//			irr::scene::ISceneNode *pPickNode = 
//				pSmgr->getSceneCollisionManager()->getSceneNodeFromRayBB(Ray);		
//
//
//			if(pPickNode->getType() == irr::scene::ESNT_ANIMATED_MESH )
//			{
//				if(pPickNode->getType() == irr::scene::ESNT_ANIMATED_MESH)
//				{
//					g_App.m_pPickNode = (irr::scene::IAnimatedMeshSceneNode *)pPickNode;
//				}
//				else
//				{
//					g_App.m_pPickNode = NULL;
//				}
//			}
//
//
//			//샐랙터얻기
//			pSelector = pSmgr->getSceneNodeFromName("room")->getTriangleSelector();
//
//#if(IRR_VERSION >= 16)
//			const irr::scene::ISceneNode *pCollNode;
//			if(pSmgr->getSceneCollisionManager()->getCollisionPoint(Ray,pSelector,v3Intersec,tri,pCollNode))
//#else
//			if(pSmgr->getSceneCollisionManager()->getCollisionPoint(Ray,pSelector,v3Intersec,tri))
//
//#endif
//			{			
//
//				/*
//				irr::scene::ISceneNode *pArrow = pSmgr->getSceneNodeFromName("Arrow");
//				pArrow->setPosition(v3Intersec);
//				irr::core::vector3df vNor = tri.getNormal().normalize();
//				printf("%f,%f,%f \n",
//				vNor.X,
//				vNor.Y,
//				vNor.Z
//				);
//				pArrow->setRotation(vNor.getHorizontalAngle()-(irr::core::vector3df(270,0,0)));*/
//
//
//				irr::video::SMaterial m;
//				m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.
//				pVideo->setMaterial(m);				
//				pVideo->setTransform(irr::video::ETS_WORLD, irr::core::matrix4());				
//
//				pVideo->draw3DTriangle(tri, irr::video::SColor(0,255,0,0));
//
//			}		
//
//
//			if(pPickNode && pPickNode->getType() != irr::scene::ESNT_CAMERA )
//			{
//				if(pPrePickNode)
//				{
//					pPrePickNode->setDebugDataVisible(irr::scene::EDS_OFF);
//				}
//				pPrePickNode = pPickNode;				
//				pPickNode->setDebugDataVisible(irr::scene::EDS_BBOX);				
//			}
//		}
//
//		{//AABB 충돌처리
//			irr::scene::IAnimatedMeshSceneNode *pNode1 = (irr::scene::IAnimatedMeshSceneNode *)pSmgr->getSceneNodeFromName("syndy1");
//			irr::scene::IAnimatedMeshSceneNode *pNode2 = (irr::scene::IAnimatedMeshSceneNode *)pSmgr->getSceneNodeFromName("syndy2");
//
//
//			{
//				irr::core::matrix4 mat;//단위행렬로초기화
//				pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화
//
//				irr::video::SMaterial m;
//				m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.
//				//m.ZBuffer = false;
//
//				pVideo->setMaterial(m);
//			}
//
//			//애니메이션이 적용된 바운딩박스구하기
//
//			irr::core::aabbox3df box1;
//			irr::core::aabbox3df box2;			
//
//			//getAnimateBoundingBox(pNode1,box1);
//			//getAnimateBoundingBox(pNode2,box2);
//
//			box1 = pNode1->getTransformedBoundingBox();
//			box2 = pNode2->getTransformedBoundingBox();
//
//			if(box1.intersectsWithBox(box2) == true)
//			{
//				pVideo->draw3DBox(box1,irr::video::SColor(255,255,0,0));
//				pVideo->draw3DBox(box2,irr::video::SColor(255,255,0,0));
//			}
//			else
//			{
//				pVideo->draw3DBox(box1,irr::video::SColor(255,0,255,0));
//				pVideo->draw3DBox(box2,irr::video::SColor(255,0,255,0));
//			}
//		}
//
//		pSmgr->drawAll();
//		pGuiEnv->drawAll();	
//
//		pVideo->endScene();		
//	}
//
//	pDevice->drop();
//
//}


namespace lsn11 {

	//간단한 fps식 카메라 컨트롤 함수
	void ControlFPSCamera(irr::scene::ICameraSceneNode *pCam,irr::f32 fDelta)
	{
		//irr::scene::ICameraSceneNode *pCam = (irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2");

		irr::f32 fRotSpeed = 45.f;
		irr::f32 fSpeed = 5.0f;
		irr::core::vector3df vFront = (pCam->getTarget() - pCam->getAbsolutePosition());
		irr::core::vector3df vUp = pCam->getUpVector();

		//직교 재설정
		vFront.normalize();
		vUp.normalize();
		//좌향벡터구하기
		irr::core::vector3df vSide = vFront.crossProduct(vUp); //vFront X vUp
		vSide.normalize();
		//상방벡터구하기
		vUp = vSide.crossProduct(vFront);// vSide X vFront

		irr::core::vector3df vMoveFront = (vFront*fDelta * fSpeed);
		irr::core::vector3df vMoveUp = vUp*fDelta * fSpeed;
		irr::core::vector3df vMoveSide = vSide*fDelta * fSpeed;					
/*
		if(!(::GetAsyncKeyState(VK_LCONTROL) & 0x8001))
		{
			//전후진
			if( (::GetAsyncKeyState(VK_UP) & 0x8001))
			{
				pCam->setPosition(
					pCam->getAbsolutePosition() + vMoveFront
					);

			}
			if( (::GetAsyncKeyState(VK_DOWN) & 0x8001))
			{
				pCam->setPosition(
					pCam->getAbsolutePosition() - vMoveFront
					);

			}

			//좌우게걸음질
			if( (::GetAsyncKeyState(VK_LEFT) & 0x8001) )
			{
				pCam->setPosition(
					pCam->getAbsolutePosition() + vMoveSide
					);

			}

			if( (::GetAsyncKeyState(VK_RIGHT) & 0x8001) )
			{
				pCam->setPosition(
					pCam->getPosition() - vMoveSide
					);

			}

			//상하
			if( (::GetAsyncKeyState(VK_PRIOR) & 0x8001) )
			{
				pCam->setPosition(
					pCam->getPosition() + vMoveUp
					);

			}

			if( (::GetAsyncKeyState(VK_NEXT) & 0x8001) )
			{
				pCam->setPosition(
					pCam->getAbsolutePosition() - vMoveUp
					);

			}
		}
		else
		{//회전
			if( (::GetAsyncKeyState(VK_UP) & 0x8001) )
			{
				pCam->setRotation(
					pCam->getRotation() - irr::core::vector3df(fDelta*fRotSpeed,0,0)
					);
			}
			if( (::GetAsyncKeyState(VK_DOWN) & 0x8001))
			{
				pCam->setRotation(
					pCam->getRotation() + irr::core::vector3df(fDelta*fRotSpeed,0,0)
					);						
			}
			if( (::GetAsyncKeyState(VK_LEFT) & 0x8001) )
			{	
				pCam->setRotation(
					pCam->getRotation() - irr::core::vector3df(0,fDelta*fRotSpeed,0)
					);							
			}
			if( (::GetAsyncKeyState(VK_RIGHT) & 0x8001))
			{		
				pCam->setRotation(
					pCam->getRotation() + irr::core::vector3df(0,fDelta*fRotSpeed,0)
					);							
			}
		}
 */

		//전역변환 업데이트해주기
		pCam->updateAbsolutePosition();

		//업벡터와 시점벡터에 변환반영
		{
			irr::core::vector3df vTarget(0,0,1);
			irr::core::vector3df vUp(0,1,0);

			irr::core::matrix4 mat = pCam->getAbsoluteTransformation();

			mat.transformVect(vTarget);
			mat.transformVect(vUp);

			pCam->setTarget(vTarget);	
			pCam->setUpVector(vUp - pCam->getPosition());						
		}						
	}
	

	//1.7.1버전 이상에서는 활성 카메라만 골라서 뷰영역이 갱신되므로 수동으로 처리해줌
	void UpdateViewFrustum(irr::scene::ICameraSceneNode *pCam)
	{
		const irr::scene::SViewFrustum *pVf = pCam->getViewFrustum();			
		//1.7.1버전 이상에서는 활성 카메라만 골라서 뷰영역이 갱신되므로 수동으로 처리해줌
		//뷰영역 갱신(카메라 절두체 영역) 코드예
		{
			irr::scene::SViewFrustum ViewArea = *pVf;
			irr::core::vector3df pos = pCam->getAbsolutePosition();
			irr::core::vector3df Target = pCam->getTarget();


			irr::core::vector3df tgtv = Target - pos;
			tgtv.normalize();

			// if upvector and vector to the target are the same, we have a
			// problem. so solve this problem:
			irr::core::vector3df up = pCam->getUpVector();
			up.normalize();

			irr::f32 dp = tgtv.dotProduct(up);

			if ( irr::core::equals(irr::core::abs_<irr::f32>(dp), 1.f) )
			{
				up.X += 0.5f;
			}

			irr::core::matrix4 viewMat;
			viewMat.buildCameraLookAtMatrixLH(pos, Target, up);
			viewMat *= pCam->getViewMatrixAffector();

			ViewArea.getTransform(irr::video::ETS_VIEW) = viewMat;
			ViewArea.cameraPosition = pCam->getAbsolutePosition();

			irr::core::matrix4 m(irr::core::matrix4::EM4CONST_NOTHING);
			m.setbyproduct_nocheck(
				ViewArea.getTransform(irr::video::ETS_PROJECTION),
				ViewArea.getTransform(irr::video::ETS_VIEW)
				);
			ViewArea.setFrom(m);
			//강제로 써넣기...
			*((irr::scene::SViewFrustum *)pVf) = ViewArea;
		}
	}

	//카메라 절두체 그리기
	void DrawViewFrustum(irr::video::IVideoDriver *pVideo,irr::scene::ICameraSceneNode *pCam)
		//카메라 절두체그리기
	{
		const irr::scene::SViewFrustum *pVf = pCam->getViewFrustum();
		irr::core::matrix4 mat;//단위행렬로초기화
		pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

		irr::video::SMaterial m;
		m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.					
		pVideo->setMaterial(m);

		pVideo->draw3DLine(pCam->getAbsolutePosition(),
			pVf->getFarLeftDown());
		pVideo->draw3DLine(pCam->getAbsolutePosition(),
			pVf->getFarRightDown());
		pVideo->draw3DLine(pCam->getAbsolutePosition(),
			pVf->getFarLeftUp());
		pVideo->draw3DLine(pCam->getAbsolutePosition(),
			pVf->getFarRightUp());					
		pVideo->draw3DLine(pCam->getAbsolutePosition(),
			pVf->getFarRightUp());

		pVideo->draw3DLine(pVf->getFarLeftDown(),
			pVf->getFarRightDown(),irr::video::SColor(255,255,0,0));
		pVideo->draw3DLine(pVf->getFarLeftDown(),
			pVf->getFarLeftUp(),irr::video::SColor(255,255,0,0));
		pVideo->draw3DLine(pVf->getFarLeftUp(),
			pVf->getFarRightUp(),irr::video::SColor(255,255,0,0));
		pVideo->draw3DLine(pVf->getFarRightUp(),
			pVf->getFarRightDown(),irr::video::SColor(255,255,0,0));
	}	

	//객체 디버깅용 축노드 만들기
	irr::scene::ISceneNode *AddAxiesNode(irr::scene::ISceneManager *pSmgr)
	{
		irr::scene::ISceneNode *pParentNode = pSmgr->addEmptySceneNode();
		irr::scene::ISceneNode *pNode;

		{
			pNode = pSmgr->addAnimatedMeshSceneNode(
				pSmgr->addArrowMesh("usr/mesh/arrow/red",
				irr::video::SColor(255,255,0,0),
				irr::video::SColor(255,255,0,0)
				),
				pParentNode,
				-1,
				irr::core::vector3df(0,0,0),
				irr::core::vector3df(0,0,0)
				);
			pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
			pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);

			pNode = pSmgr->addAnimatedMeshSceneNode(
				pSmgr->addArrowMesh("usr/mesh/arrow/green",
				irr::video::SColor(255,0,255,0),
				irr::video::SColor(255,0,255,0)
				),
				pParentNode,
				-1,
				irr::core::vector3df(0,0,0),
				irr::core::vector3df(0,0,-90)
				);
			pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
			pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);

			pNode = pSmgr->addAnimatedMeshSceneNode(
				pSmgr->addArrowMesh("usr/mesh/arrow/blue",
				irr::video::SColor(255,0,0,255),
				irr::video::SColor(255,0,0,255)
				),
				pParentNode,
				-1,
				irr::core::vector3df(0,0,0),
				irr::core::vector3df(90,0,0)
				);
			pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
			pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);
		}

		return pParentNode;
	}


	//레이캐스팅 예제
	//마우스 피킹 예제
	namespace _00
	{
		//트라이앵글 샐랙터로 정확하게 픽킹하기
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,5,-10), irr::core::vector3df(0,0,0));

			//실린더 메쉬만들기
			{				
				irr::scene::IMesh *pMesh = pSmgr->getGeometryCreator()->createCylinderMesh(2,3,8);				
				irr::scene::IAnimatedMesh *pAniMesh = pSmgr->getMeshManipulator()->createAnimatedMesh(pMesh);
				pMesh->drop();				
				pSmgr->getMeshCache()->addMesh("usr/mesh/cylinder",pAniMesh); //씬메니져에 등록
				pAniMesh->drop();
			}			

			//씬노드 등록
			{				
				irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(
					pSmgr->getMesh("usr/mesh/cylinder"));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);				
				pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);				
				pNode->setPosition(irr::core::vector3df(0,0,0));				
				pNode->setName("usr/scene/cylinder");

				irr::scene::ITriangleSelector *pSelector;
				pSelector = pSmgr->createTriangleSelector(pNode);
				pNode->setTriangleSelector(pSelector); //선택자 등록
				pSelector->drop();

				irr::scene::ISceneNodeAnimator *pAnim = pSmgr->createRotationAnimator(irr::core::vector3df(1,1,0));
				pNode->addAnimator(pAnim);
				pAnim->drop();
			}


			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);


			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				irr::core::vector3df test(1,0,0);

				pSmgr->getSceneCollisionManager()->getScreenCoordinatesFrom3DPosition(test);


				{
					irr::core::line3df PickingRay;			
					irr::core::position2di mouse_pos = pDevice->getCursorControl()->getPosition();		
					//2디 스크린좌표계로부터 피킹레이 얻기
					PickingRay = pSmgr->getSceneCollisionManager()->getRayFromScreenCoordinates(mouse_pos);	

					irr::core::vector3df v3Intersec;
					irr::core::triangle3df tri;
					irr::scene::ITriangleSelector *pSelector;
					//샐랙터얻기
					pSelector = pSmgr->getSceneNodeFromName("usr/scene/cylinder")->getTriangleSelector();
					const irr::scene::ISceneNode *pCollNode;
					//if(pSmgr->getSceneCollisionManager()->getCollisionPoint(PickingRay,pSelector,v3Intersec,tri,pCollNode))
					if(pCollNode = pSmgr->getSceneCollisionManager()->getSceneNodeAndCollisionPointFromRay(
						PickingRay,
						v3Intersec,tri
						)
						)
					{	
						irr::video::SMaterial m;
						m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.
						pVideo->setMaterial(m);				
						pVideo->setTransform(irr::video::ETS_WORLD, irr::core::IdentityMatrix);				

						pVideo->draw3DTriangle(tri, irr::video::SColor(0,255,0,0));

					}	

				}

				pVideo->endScene();	
			}

			pDevice->drop();
		}




		// 트라이앵글샐랙터 없이 픽킹하기, 정확한 충돌점은 얻지못함 ,광선과 충돌한 씬노드 포인터만 받아옴
		namespace _01
		{
			void main()
			{
				irr::IrrlichtDevice *pDevice = irr::createDevice(					
					irr::video::EDT_OPENGL
					);

				pDevice->setWindowCaption(L"Type-A1");

				irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
				irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
				irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

				pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

				pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,5,-10), irr::core::vector3df(0,0,0));


				irr::scene::ISceneNode *pObjRoot = pSmgr->addEmptySceneNode();

				//실린더 메쉬만들기
				{				
					irr::scene::IMesh *pMesh = pSmgr->getGeometryCreator()->createCylinderMesh(2,3,8);				
					irr::scene::IAnimatedMesh *pAniMesh = pSmgr->getMeshManipulator()->createAnimatedMesh(pMesh);
					pMesh->drop();				
					pSmgr->getMeshCache()->addMesh("usr/mesh/cylinder",pAniMesh); //씬메니져에 등록
					pAniMesh->drop();
				}			

				//씬노드 등록
				{				
					irr::scene::IMeshSceneNode *pNode = 
						//옥트리도가능
						//pSmgr->addAnimatedMeshSceneNode(	pSmgr->getMesh("usr/mesh/cylinder"));
						pSmgr->addOctreeSceneNode(pSmgr->getMesh("usr/mesh/cylinder")->getMesh(0),pObjRoot);
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);				
					pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);				
					pNode->setPosition(irr::core::vector3df(0,0,0));				
					pNode->setName("usr/scene/cylinder");
					pNode->setMaterialTexture(0,	pVideo->getTexture("blue.png"));					


					irr::scene::ITriangleSelector *pSelector;
					pSelector = pSmgr->createOctreeTriangleSelector(pNode->getMesh(),pNode);
					pNode->setTriangleSelector(pSelector); //선택자 등록
					pSelector->drop();

					irr::scene::ISceneNodeAnimator *pAnim = pSmgr->createRotationAnimator(irr::core::vector3df(1,1,0));
					pNode->addAnimator(pAnim);
					pAnim->drop();
				}


				//프레임 레이트 표시용 유아이
				irr::gui::IGUIStaticText *pstextFPS = 
					pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);


				while(pDevice->run())
				{	
					static irr::u32 uLastTick=0;
					irr::u32 uTick = pDevice->getTimer()->getTime();						
					irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
					uLastTick = uTick;



					//프레임레이트 갱신, 삼각형수 표시
					{
						wchar_t wszbuf[256];
						swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
						pstextFPS->setText(wszbuf);
					}

					//레이 캐스팅
					{
						irr::core::line3df PickingRay;			
						irr::core::position2di mouse_pos = pDevice->getCursorControl()->getPosition();
						//2디 스크린좌표계로부터 피킹레이 얻기
						PickingRay = pSmgr->getSceneCollisionManager()->getRayFromScreenCoordinates(mouse_pos);
						irr::scene::ISceneNode *pNode = pSmgr->getSceneNodeFromName("usr/scene/cylinder");

						//주의: 광선충돌검사는 pObjRoot자신은 검사 하지 않고 하위 자식노드들만 검사한다.
						if(pSmgr->getSceneCollisionManager()->getSceneNodeFromRayBB(PickingRay,0,false,pObjRoot))
						{	
							pSmgr->getSceneNodeFromName("usr/scene/cylinder")->setMaterialTexture(0,
								pVideo->getTexture("red.png"));
						}
						else
						{
							pSmgr->getSceneNodeFromName("usr/scene/cylinder")->setMaterialTexture(0,
								pVideo->getTexture("blue.png"));
						}					
					}

					pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

					pSmgr->drawAll();
					pGuiEnv->drawAll();											
					pVideo->endScene();	
				}
				pDevice->drop();
			}
		}
	}


	//스키닝 메쉬 애니메이션 오브잭트의 충돌박스구하기
	//AABB 충돌 검출
	namespace _01
	{
		/*
		같은 스키닝 메쉬를 공유하는 애니메이션 메쉬 노드의 충돌박스 구하기 예
		하나의 메쉬를 여러개의 씬노드가 공유하므로 정확한 충돌정보를 얻기 위해서는 충돌박스를 얻기 전에
		얻고자하는 프레임에 대해서 메쉬버퍼의 충돌정보를 갱신 해주어야한다.
		그렇지 않으면 이전 노드의 프레임정보에 대한 충돌 박스를 얻어올수있다.(2개이상일경우)
		*/
		void getAnimateBoundingBox(irr::scene::IAnimatedMeshSceneNode *pAniSceneNode,irr::core::aabbox3df &box)
		{	
			irr::s32 frame = (irr::s32)pAniSceneNode->getFrameNr();	
			irr::scene::IMesh *pMesh = pAniSceneNode->getMesh()->getMesh(frame);

			box.reset(0,0,0);

			irr::u32 i;
			for(i=0;i<pMesh->getMeshBufferCount();i++)
			{

				pMesh->getMeshBuffer(i)->recalculateBoundingBox();				

				box.addInternalBox(pMesh->getMeshBuffer(0)->getBoundingBox());
			}

			pMesh->setBoundingBox(box);				
			pAniSceneNode->getAbsoluteTransformation().transformBox(box);	

			//box = pMesh->getBoundingBox();
			/*
			irr::s32 frame = (irr::s32)pAniSceneNode->getFrameNr();		

			std::cout << frame <<std::endl;

			pAniSceneNode->getMesh()->getMesh(frame)->getMeshBuffer(0)->recalculateBoundingBox();
			box = pAniSceneNode->getMesh()->getMesh(frame)->getMeshBuffer(0)->getBoundingBox();
			pAniSceneNode->getAbsoluteTransformation().transformBox(box);	
			*/

		}


		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,5,-20), irr::core::vector3df(0,0,0));

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			{
				irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(
					pSmgr->getMesh("ninja.b3d"));
				pNode->setName("usr/scene/b3d/ninja1");
				pNode->setMaterialTexture(0,pVideo->getTexture("nskinbl.jpg"));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setAnimationSpeed(30.f);
				pNode->setFrameLoop(206,250);
				pNode->setRotation(irr::core::vector3df(0,90,0));
				pNode->setPosition(irr::core::vector3df(-10,0,0));

				irr::scene::ITriangleSelector *pSelector;
				pSelector = pSmgr->createTriangleSelector(pNode->getMesh()->getMesh(0),pNode);
				pNode->setTriangleSelector(pSelector);
				pSelector->drop();
			}

			{
				irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(
					pSmgr->getMesh("ninja.b3d"));
				pNode->setName("usr/scene/b3d/ninja2");
				pNode->setMaterialTexture(0,pVideo->getTexture("nskinrd.jpg"));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setAnimationSpeed(30.f);
				pNode->setFrameLoop(184,205);
				pNode->setRotation(irr::core::vector3df(0,-90,0));
				pNode->setPosition(irr::core::vector3df(10,0,0));
			}			


			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);


			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;



				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

                /*
				{
					if(::GetAsyncKeyState(VK_LEFT) && 0x8000)
					{
						irr::scene::ISceneNode *pNode = pSmgr->getSceneNodeFromName("usr/scene/b3d/ninja1");
						pNode->setPosition(pNode->getPosition()-irr::core::vector3df(6,0,0) * fDelta);
					}
					if(::GetAsyncKeyState(VK_RIGHT) && 0x8000)
					{
						irr::scene::ISceneNode *pNode = pSmgr->getSceneNodeFromName("usr/scene/b3d/ninja1");
						pNode->setPosition(pNode->getPosition()+irr::core::vector3df(6,0,0) * fDelta);
					}
				}
                 */

				{
					irr::video::SMaterial m;
					m.Lighting = false;
					pVideo->setMaterial(m);   
					{
						pVideo->setTransform(irr::video::ETS_WORLD,irr::core::IdentityMatrix);
					}
					irr::scene::ISceneNode *pNode = pSmgr->getSceneNodeFromName("usr/scene/b3d/ninja1");
					irr::scene::ISceneNode *pNode2 = pSmgr->getSceneNodeFromName("usr/scene/b3d/ninja2");

					irr::core::aabbox3df box1;// = getAnimateBoundingBox(pNode,);//pNode2->getBoundingBox();
					getAnimateBoundingBox((irr::scene::IAnimatedMeshSceneNode *)pNode,box1);
					irr::core::aabbox3df box2;// = pNode2->getTransformedBoundingBox();
					getAnimateBoundingBox((irr::scene::IAnimatedMeshSceneNode *)pNode2,box2);

					pVideo->draw3DBox(box1,irr::video::SColor(255,255,0,0));

					if(box1.intersectsWithBox(box2) == true)
					{
						pVideo->draw3DBox(box1,irr::video::SColor(255,255,0,0));
					}
					else
					{
						pVideo->draw3DBox(box1,irr::video::SColor(255,0,255,0));
					}

					pVideo->draw3DBox(box2,irr::video::SColor(255,255,255,0));
				}

				pVideo->endScene();	
			}

			pDevice->drop();
		}

		namespace _01_01
		{
			void main()
			{
				irr::IrrlichtDevice *pDevice = irr::createDevice(					
					irr::video::EDT_OPENGL
					);

				pDevice->setWindowCaption(L"Type-A1");

				irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
				irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
				irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

				pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,5,-25), irr::core::vector3df(0,0,0));

				//실린더 메쉬만들기
				{				
					irr::scene::IMesh *pMesh = pSmgr->getGeometryCreator()->createCylinderMesh(2,3,8);				
					irr::scene::IAnimatedMesh *pAniMesh = pSmgr->getMeshManipulator()->createAnimatedMesh(pMesh);
					pMesh->drop();				
					pSmgr->getMeshCache()->addMesh("usr/mesh/cylinder",pAniMesh); //씬메니져에 등록
					pAniMesh->drop();
				}			

				//실린더 메쉬만들기
				{				
					irr::scene::IMesh *pMesh = pSmgr->getGeometryCreator()->createSphereMesh(2.5f);
					irr::scene::IAnimatedMesh *pAniMesh = pSmgr->getMeshManipulator()->createAnimatedMesh(pMesh);
					pMesh->drop();				
					pSmgr->getMeshCache()->addMesh("usr/mesh/sphere",pAniMesh); //씬메니져에 등록
					pAniMesh->drop();
				}	

				//씬노드 등록
				{				
					irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(
						pSmgr->getMesh("usr/mesh/cylinder"));
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);				
					pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);				
					pNode->setPosition(irr::core::vector3df(0,0,0));				
					pNode->setName("usr/scene/cylinder");					
				}

				//씬노드 등록
				{				
					irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(
						pSmgr->getMesh("usr/mesh/sphere"));
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);				
					pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);				
					pNode->setPosition(irr::core::vector3df(0,0,0));				
					pNode->setName("usr/scene/sphere");		

					irr::scene::ISceneNodeAnimator *pAnim = pSmgr->createFlyStraightAnimator(irr::core::vector3df(10,0,0),
						irr::core::vector3df(-10,0,0),5000,true,false);
					pNode->addAnimator(pAnim);
					pAnim->drop();
				}


				//프레임 레이트 표시용 유아이
				irr::gui::IGUIStaticText *pstextFPS = 
					pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);


				while(pDevice->run())
				{	
					static irr::u32 uLastTick=0;
					irr::u32 uTick = pDevice->getTimer()->getTime();						
					irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
					uLastTick = uTick;


					//프레임레이트 갱신, 삼각형수 표시
					{
						wchar_t wszbuf[256];
						swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
						pstextFPS->setText(wszbuf);
					}

					pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

					pSmgr->drawAll();
					pGuiEnv->drawAll();


					//충돌 박스 그리기
					{
						irr::video::SMaterial m;
						m.Lighting = false;
						pVideo->setMaterial(m);   
						{
							pVideo->setTransform(irr::video::ETS_WORLD,irr::core::IdentityMatrix);
						}
						irr::scene::ISceneNode *pNode1 = pSmgr->getSceneNodeFromName("usr/scene/cylinder");
						irr::scene::ISceneNode *pNode2 = pSmgr->getSceneNodeFromName("usr/scene/sphere");

						//변환적용된 충돌박스 구하기
						irr::core::aabbox3df box1 = pNode1->getTransformedBoundingBox();					
						irr::core::aabbox3df box2 = pNode2->getTransformedBoundingBox();

						pVideo->draw3DBox(box1,irr::video::SColor(255,255,0,0));

						if(box1.intersectsWithBox(box2) == true)
						{
							pVideo->draw3DBox(box1,irr::video::SColor(255,255,0,0));
						}
						else
						{
							pVideo->draw3DBox(box1,irr::video::SColor(255,0,255,0));
						}

						pVideo->draw3DBox(box2,irr::video::SColor(255,255,255,0));
					}

					pVideo->endScene();	
				}

				pDevice->drop();
			}
		}
	}	

	//충돌 반응 활동자 예제
	namespace _02
	{
		//기본 A1형
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,20,-30), irr::core::vector3df(0,0,0));

			{
				irr::scene::IAnimatedMeshSceneNode* pNode = 0;
				irr::scene::IAnimatedMesh* pMesh;			

				pMesh = pSmgr->addHillPlaneMesh("usr/mesh/hill",
					irr::core::dimension2d<irr::f32>(8,8),
					irr::core::dimension2d<irr::u32>(20,20), 0,10,
					irr::core::dimension2d<irr::f32>(2,2),
					irr::core::dimension2d<irr::f32>(8,8));

				pNode = pSmgr->addAnimatedMeshSceneNode(pMesh);
				//pNode->setPosition(irr::core::vector3df(0,30,-20));
				pNode->setMaterialTexture(0, pVideo->getTexture("wall.jpg"));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setName("usr/scene/hill");

				//샐랙터 등록
				irr::scene::ITriangleSelector *pSelector;
				pSelector = pSmgr->createTriangleSelector(pNode->getMesh()->getMesh(0),pNode);
				pNode->setTriangleSelector(pSelector);
				pSelector->drop();
			}

			irr::scene::ISceneNodeAnimatorCollisionResponse *pcrAnim;
			//닌자 오브잭트
			{
				irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(
					pSmgr->getMesh("walkers/ninja/ninja.b3d"));
				pNode->setName("usr/scene/b3d/ninja_root");
				pNode->setMaterialTexture(0,pVideo->getTexture("walkers/ninja/nskinbl.jpg"));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setAnimationSpeed(30.f);
				pNode->setFrameLoop(206,250);
				pNode->setRotation(irr::core::vector3df(0,90,0));
				pNode->setPosition(irr::core::vector3df(0,30,0));


				//충동반응활동자 등록
				irr::scene::ITriangleSelector* pSelector = pSmgr->getSceneNodeFromName("usr/scene/hill")->getTriangleSelector();

				pcrAnim = pSmgr->createCollisionResponseAnimator(
					pSelector,pNode,					
					irr::core::vector3df(2,5,2),
					irr::core::vector3df(0,-10,0),
					irr::core::vector3df(0,-5,0),					
					0.005f
					);
				pNode->addAnimator(pcrAnim);
			}

			//디버깅 충돌 바운딩 볼륨 정보출력용
			{
				irr::scene::ISceneNode *pNode = pSmgr->addSphereSceneNode(1.f,8);
				pNode->setScale(pcrAnim->getEllipsoidRadius());
				pNode->setPosition(-pcrAnim->getEllipsoidTranslation());
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->getMaterial(0).Wireframe = true;		
				pNode->getMaterial(0).DiffuseColor = irr::video::SColor(255,255,0,0);
				pNode->setName("usr/dbg/vol/1");

				irr::scene::ISceneNode *pRootNode = pSmgr->getSceneNodeFromName("usr/scene/b3d/ninja_root");
				pRootNode->addChild(pNode);
			}

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);


			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}				

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));
				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				{
					pVideo->setTransform(irr::video::ETS_WORLD,irr::core::IdentityMatrix);
					irr::video::SMaterial m;					
					m.Lighting = false;
					m.ZBuffer = irr::video::ECFN_NEVER;
					m.ZWriteEnable = false;
					pVideo->setMaterial(m);
				}

				pVideo->draw3DTriangle(pcrAnim->getCollisionTriangle(),irr::video::SColor(255,255,0,0));
				pVideo->draw3DBox(irr::core::aabbox3df(
					pcrAnim->getCollisionPoint()-irr::core::vector3df(1,1,1),pcrAnim->getCollisionPoint()+irr::core::vector3df(1,1,1)),
					irr::video::SColor(255,0,255,0));
				pVideo->draw3DBox(irr::core::aabbox3df(
					pcrAnim->getCollisionResultPosition()-irr::core::vector3df(1,1,1),pcrAnim->getCollisionResultPosition()+irr::core::vector3df(1,1,1)),
					irr::video::SColor(255,0,0,255));

				pVideo->endScene();	
			}

			pcrAnim->drop();
			pDevice->drop();
		}
	}

	//평면 픽킹예제(교점구하기)	
	namespace _03
	{

		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,10,-20), irr::core::vector3df(0,0,0));

			{
				pSmgr->addArrowMesh("usr/mesh/arrow"); //메쉬등록
			}

			//힐플래인 메쉬 추가
			{
				irr::scene::IAnimatedMesh *pMesh =
					pSmgr->addHillPlaneMesh("usr/mesh/myhill",
					irr::core::dimension2d<irr::f32>(8,8),
					irr::core::dimension2d<irr::u32>(8,8),
					0,0,
					irr::core::dimension2d<irr::f32>(0,0),
					irr::core::dimension2d<irr::f32>(8,8)
					);

				//irr::core::matrix4 mat;
				//mat.setRotationDegrees(irr::core::vector3df(-180.f,0,0));
				//pSmgr->getMeshManipulator()->transformMesh(pMesh->getMesh(0),mat);
			}

			{
				irr::scene::IAnimatedMeshSceneNode *pNode =
					pSmgr->addAnimatedMeshSceneNode(pSmgr->getMesh("usr/mesh/myhill"));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);

				pNode->setName("usr/scene/hill1");
			}

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);	


			//커서숨기기
			pDevice->getCursorControl()->setVisible(false);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				{
					irr::core::position2di mouse_pos = pDevice->getCursorControl()->getPosition();
					irr::core::line3df Ray;

					irr::core::plane3df plane;
					irr::core::vector3df v3Intersec;
					plane.setPlane(irr::core::vector3df(0,0,0),irr::core::vector3df(0,1,0)); //높이가 0인 지면설정
					Ray = pSmgr->getSceneCollisionManager()->getRayFromScreenCoordinates(mouse_pos);

					plane.getIntersectionWithLine(Ray.start,Ray.getVector().normalize(),v3Intersec); //지면과 광선 충돌점구하기

					//직접 그리기
					{
						irr::scene::IMesh *pMesh = pSmgr->getMesh("usr/mesh/arrow")->getMesh(0);

						irr::core::matrix4 mat;
						mat.setTranslation(v3Intersec-irr::core::vector3df(0,pMesh->getBoundingBox().getExtent().Y,0));
						mat.setRotationDegrees(irr::core::vector3df(180,0,0));
						pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

						irr::video::SMaterial m;						
						m.ZBuffer = false;						
						m.EmissiveColor= irr::video::SColor(255,255,255,0);
						pVideo->setMaterial(m);



						irr::u32 i;
						for(i=0;i<pMesh->getMeshBufferCount();i++)
						{
							pVideo->drawMeshBuffer(pMesh->getMeshBuffer(i));
						}						
					}


				}

				pVideo->endScene();	
			}

			pDevice->drop();
		}


		namespace _01
		{
			void main()
			{
				irr::IrrlichtDevice *pDevice = irr::createDevice(					
					irr::video::EDT_OPENGL
					);

				pDevice->setWindowCaption(L"");

				irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
				irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
				irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

				pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,10,-20), irr::core::vector3df(0,0,0));


				//프레임 레이트 표시용 유아이
				irr::gui::IGUIStaticText *pstextFPS = 
					pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);	


				//커서숨기기
				pDevice->getCursorControl()->setVisible(false);

				while(pDevice->run())
				{	
					static irr::u32 uLastTick=0;
					irr::u32 uTick = pDevice->getTimer()->getTime();						
					irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
					uLastTick = uTick;

					//프레임레이트 갱신, 삼각형수 표시
					{
						wchar_t wszbuf[256];
						swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
						pstextFPS->setText(wszbuf);
					}

					pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

					pSmgr->drawAll();
					pGuiEnv->drawAll();		

					{
						irr::core::position2di mouse_pos = pDevice->getCursorControl()->getPosition();
						irr::core::line3df Ray;

						irr::core::vector3df v3Intersec;
						Ray = pSmgr->getSceneCollisionManager()->getRayFromScreenCoordinates(mouse_pos);

						//pSmgr->getSceneCollisionManager()->getSceneNodeFromCameraBB();



					}

					pVideo->endScene();	
				}

				pDevice->drop();
			}
		}
	}


	//절두체관련 가시화 예제
	//
	namespace _04
	{		
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			{
				irr::scene::ICameraSceneNode *pCam = pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,20,-15), irr::core::vector3df(0,0,0));
				pCam->setName("usr/scne/camera/1");
			}

			//힐플래인 메쉬 추가
			{
				irr::scene::IAnimatedMesh *pMesh =
					pSmgr->addHillPlaneMesh("usr/mesh/myhill",
					irr::core::dimension2d<irr::f32>(8,8),
					irr::core::dimension2d<irr::u32>(8,8),
					0,0,
					irr::core::dimension2d<irr::f32>(0,0),
					irr::core::dimension2d<irr::f32>(8,8)
					);
			}

			//씬노드 추가
			{
				irr::scene::IAnimatedMeshSceneNode *pNode =
					pSmgr->addAnimatedMeshSceneNode(pSmgr->getMesh("usr/mesh/myhill"));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);
				//pNode->setMaterialTexture(0,pVideo->getTexture("wall.jpg"));

				pNode->setName("usr/scene/hill1");
			}			

			{			
				irr::scene::ICameraSceneNode *pCam = pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,3,-5), irr::core::vector3df(0,0,0));
				pCam->setName("usr/scne/camera/2");
				pCam->setFarValue(10);


				irr::scene::ISceneNode *pNode;

				{
					pNode = pSmgr->addAnimatedMeshSceneNode(
						pSmgr->addArrowMesh("usr/mesh/arrow/red",
						irr::video::SColor(255,255,0,0),
						irr::video::SColor(255,255,0,0)
						),
						pCam,
						-1,
						irr::core::vector3df(0,0,0),
						irr::core::vector3df(0,0,0)
						);
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
					pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);

					pNode = pSmgr->addAnimatedMeshSceneNode(
						pSmgr->addArrowMesh("usr/mesh/arrow/green",
						irr::video::SColor(255,0,255,0),
						irr::video::SColor(255,0,255,0)
						),
						pCam,
						-1,
						irr::core::vector3df(0,0,0),
						irr::core::vector3df(0,0,-90)
						);
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
					pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);

					pNode = pSmgr->addAnimatedMeshSceneNode(
						pSmgr->addArrowMesh("usr/mesh/arrow/blue",
						irr::video::SColor(255,0,0,255),
						irr::video::SColor(255,0,0,255)
						),
						pCam,
						-1,
						irr::core::vector3df(0,0,0),
						irr::core::vector3df(90,0,0)
						);
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
					pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);
				}
			}

			pSmgr->setActiveCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/1"));

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				//fps 식 카메라 캐릭터 컨트롤
				{
					irr::scene::ICameraSceneNode *pCam = (irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2");

					irr::f32 fRotSpeed = 45.f;
					irr::f32 fSpeed = 5.0f;
					irr::core::vector3df vFront = (pCam->getTarget() - pCam->getAbsolutePosition());
					irr::core::vector3df vUp = pCam->getUpVector();

					//직교 재설정
					vFront.normalize();
					vUp.normalize();
					//좌향벡터구하기
					irr::core::vector3df vSide = vFront.crossProduct(vUp); //vFront X vUp
					vSide.normalize();
					//상방벡터구하기
					vUp = vSide.crossProduct(vFront);// vSide X vFront

					irr::core::vector3df vMoveFront = (vFront*fDelta * fSpeed);
					irr::core::vector3df vMoveUp = vUp*fDelta * fSpeed;
					irr::core::vector3df vMoveSide = vSide*fDelta * fSpeed;					
/*
					if(!(::GetAsyncKeyState(VK_LCONTROL) & 0x8001))
					{

						if(::GetAsyncKeyState('1') & 0x8001)
						{
							pCam->setFarValue(pCam->getFarValue() + fDelta*10);
						}
						if(::GetAsyncKeyState('2') & 0x8001)
						{
							pCam->setFarValue(pCam->getFarValue() - fDelta*10);
						}

						//전후진
						if( (::GetAsyncKeyState(VK_UP) & 0x8001))
						{
							pCam->setPosition(
								pCam->getAbsolutePosition() + vMoveFront
								);

						}
						if( (::GetAsyncKeyState(VK_DOWN) & 0x8001))
						{
							pCam->setPosition(
								pCam->getAbsolutePosition() - vMoveFront
								);

						}

						//좌우게걸음질
						if( (::GetAsyncKeyState(VK_LEFT) & 0x8001) )
						{
							pCam->setPosition(
								pCam->getAbsolutePosition() + vMoveSide
								);

						}

						if( (::GetAsyncKeyState(VK_RIGHT) & 0x8001) )
						{
							pCam->setPosition(
								pCam->getPosition() - vMoveSide
								);

						}

						//상하
						if( (::GetAsyncKeyState(VK_PRIOR) & 0x8001) )
						{
							pCam->setPosition(
								pCam->getPosition() + vMoveUp
								);

						}

						if( (::GetAsyncKeyState(VK_NEXT) & 0x8001) )
						{
							pCam->setPosition(
								pCam->getAbsolutePosition() - vMoveUp
								);

						}
					}
					else
					{//회전
						if( (::GetAsyncKeyState(VK_UP) & 0x8001) )
						{
							pCam->setRotation(
								pCam->getRotation() - irr::core::vector3df(fDelta*fRotSpeed,0,0)
								);
						}
						if( (::GetAsyncKeyState(VK_DOWN) & 0x8001))
						{
							pCam->setRotation(
								pCam->getRotation() + irr::core::vector3df(fDelta*fRotSpeed,0,0)
								);						
						}
						if( (::GetAsyncKeyState(VK_LEFT) & 0x8001) )
						{	
							pCam->setRotation(
								pCam->getRotation() - irr::core::vector3df(0,fDelta*fRotSpeed,0)
								);							
						}
						if( (::GetAsyncKeyState(VK_RIGHT) & 0x8001))
						{		
							pCam->setRotation(
								pCam->getRotation() + irr::core::vector3df(0,fDelta*fRotSpeed,0)
								);							
						}
					}
 */

					//전역변환 업데이트해주기
					pCam->updateAbsolutePosition();

					//업벡터와 시점벡터에 변환반영
					{
						irr::core::vector3df vTarget(0,0,1);
						irr::core::vector3df vUp(0,1,0);

						irr::core::matrix4 mat = pCam->getAbsoluteTransformation();

						mat.transformVect(vTarget);
						mat.transformVect(vUp);

						pCam->setTarget(vTarget);	
						pCam->setUpVector(vUp - pCam->getPosition());						
					}
                    
                    /*

					if((::GetAsyncKeyState(VK_F1) & 0x8001) == 0x8001)
					{
						pSmgr->setActiveCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/1"));
					}
					if((::GetAsyncKeyState(VK_F2) & 0x8001) == 0x8001)
					{
						pSmgr->setActiveCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2"));
					}
                     
                     */
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				irr::scene::ICameraSceneNode *pCam = (irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2");			

				const irr::scene::SViewFrustum *pVf = pCam->getViewFrustum();

				//1.7.1버전 이상에서는 활성 카메라만 골라서 뷰영역이 갱신되므로 수동으로 처리해줌
				//뷰영역 갱신(카메라 절두체 영역) 코드예
				{
					irr::scene::SViewFrustum ViewArea = *pVf;
					irr::core::vector3df pos = pCam->getAbsolutePosition();
					irr::core::vector3df Target = pCam->getTarget();

					irr::core::vector3df tgtv = Target - pos;
					tgtv.normalize();

					// if upvector and vector to the target are the same, we have a
					// problem. so solve this problem:
					irr::core::vector3df up = pCam->getUpVector();
					up.normalize();

					irr::f32 dp = tgtv.dotProduct(up);

					if ( irr::core::equals(irr::core::abs_<irr::f32>(dp), 1.f) )
					{
						up.X += 0.5f;
					}

					irr::core::matrix4 viewMat;
					viewMat.buildCameraLookAtMatrixLH(pos, Target, up);
					viewMat *= pCam->getViewMatrixAffector();

					ViewArea.getTransform(irr::video::ETS_VIEW) = viewMat;
					ViewArea.cameraPosition = pCam->getAbsolutePosition();

					irr::core::matrix4 m(irr::core::matrix4::EM4CONST_NOTHING);
					m.setbyproduct_nocheck(
						ViewArea.getTransform(irr::video::ETS_PROJECTION),
						ViewArea.getTransform(irr::video::ETS_VIEW)
						);
					ViewArea.setFrom(m);
					//강제로 써넣기...
					*((irr::scene::SViewFrustum *)pVf) = ViewArea;

				}

				//카메라 절두체그리기
				{
					irr::core::matrix4 mat;//단위행렬로초기화
					pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

					irr::video::SMaterial m;
					m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.
					//m.ZBuffer = false;
					pVideo->setMaterial(m);
					pVideo->draw3DLine(pCam->getAbsolutePosition(),
						pVf->getFarLeftDown());
					pVideo->draw3DLine(pCam->getAbsolutePosition(),
						pVf->getFarRightDown());
					pVideo->draw3DLine(pCam->getAbsolutePosition(),
						pVf->getFarLeftUp());
					pVideo->draw3DLine(pCam->getAbsolutePosition(),
						pVf->getFarRightUp());					
					pVideo->draw3DLine(pCam->getAbsolutePosition(),
						pVf->getFarRightUp());

					pVideo->draw3DLine(pVf->getFarLeftDown(),
						pVf->getFarRightDown(),irr::video::SColor(255,255,0,0));
					pVideo->draw3DLine(pVf->getFarLeftDown(),
						pVf->getFarLeftUp(),irr::video::SColor(255,255,0,0));
					pVideo->draw3DLine(pVf->getFarLeftUp(),
						pVf->getFarRightUp(),irr::video::SColor(255,255,0,0));
					pVideo->draw3DLine(pVf->getFarRightUp(),
						pVf->getFarRightDown(),irr::video::SColor(255,255,0,0));
				}	
				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}

	//일리히트 카메라 관련 오브잭트 직접 컬링	
	namespace _05
	{		
		//박스 모양으로 컬링하기
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			{
				irr::scene::ICameraSceneNode *pCam = pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,20,-15), irr::core::vector3df(0,0,0));
				pCam->setName("usr/scne/camera/1");
			}

			//힐플래인 메쉬 추가
			{
				irr::scene::IAnimatedMesh *pMesh =
					pSmgr->addHillPlaneMesh("usr/mesh/myhill",
					irr::core::dimension2d<irr::f32>(8,8),
					irr::core::dimension2d<irr::u32>(8,8),
					0,0,
					irr::core::dimension2d<irr::f32>(0,0),
					irr::core::dimension2d<irr::f32>(8,8)
					);
			}

			//씬노드 추가
			{
				irr::scene::IAnimatedMeshSceneNode *pNode =
					pSmgr->addAnimatedMeshSceneNode(pSmgr->getMesh("usr/mesh/myhill"));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);
				//pNode->setMaterialTexture(0,pVideo->getTexture("wall.jpg"));

				pNode->setName("usr/scene/hill1");
			}

			//테스트용 씬노드
			{
				irr::scene::ISceneNode *pNode;
				pNode = pSmgr->addSphereSceneNode(2);
				pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setName("usr/scene/sphere/1");
			}

			{
				//irr::scene::ISceneNode *pRootNode = pSmgr->addEmptySceneNode();
				//pRootNode->setName("usr/scenenode/chr_root");

				irr::scene::ICameraSceneNode *pCam = pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,3,-5), irr::core::vector3df(0,0,0));
				pCam->setName("usr/scne/camera/2");
				pCam->setFarValue(10);


				irr::scene::ISceneNode *pNode;

				{
					pNode = pSmgr->addAnimatedMeshSceneNode(
						pSmgr->addArrowMesh("usr/mesh/arrow/red",
						irr::video::SColor(255,255,0,0),
						irr::video::SColor(255,255,0,0)
						),
						pCam,
						-1,
						irr::core::vector3df(0,0,0),
						irr::core::vector3df(0,0,0)
						);
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
					pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);

					pNode = pSmgr->addAnimatedMeshSceneNode(
						pSmgr->addArrowMesh("usr/mesh/arrow/green",
						irr::video::SColor(255,0,255,0),
						irr::video::SColor(255,0,255,0)
						),
						pCam,
						-1,
						irr::core::vector3df(0,0,0),
						irr::core::vector3df(0,0,-90)
						);
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
					pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);

					pNode = pSmgr->addAnimatedMeshSceneNode(
						pSmgr->addArrowMesh("usr/mesh/arrow/blue",
						irr::video::SColor(255,0,0,255),
						irr::video::SColor(255,0,0,255)
						),
						pCam,
						-1,
						irr::core::vector3df(0,0,0),
						irr::core::vector3df(90,0,0)
						);
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
					pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);
				}
			}

			pSmgr->setActiveCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/1"));

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				//fps 식 카메라 캐릭터 컨트롤
				ControlFPSCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2"),fDelta);
				
                /*

				if((::GetAsyncKeyState(VK_F1) & 0x8001) == 0x8001)
				{
					pSmgr->setActiveCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/1"));
				}
				if((::GetAsyncKeyState(VK_F2) & 0x8001) == 0x8001)
				{
					pSmgr->setActiveCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2"));
				}
                 */

				//fps 식 카메라 캐릭터 컨트롤
				ControlFPSCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2"),fDelta);
				//view frustum area update
				UpdateViewFrustum((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2"));

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				{
					irr::scene::ICameraSceneNode *pCam = (irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2");

					const irr::scene::SViewFrustum *pVf = pCam->getViewFrustum();					

					//테스트오브잭트 경계박스구하기
					irr::core::aabbox3df box2 = pSmgr->getSceneNodeFromName("usr/scene/sphere/1")->getTransformedBoundingBox();
					irr::core::aabbox3df box = pCam->getViewFrustum()->getBoundingBox(); //카메라 시야박스 구하기

					irr::core::matrix4 mat;//단위행렬로초기화
					pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

					irr::video::SMaterial m;
					m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.					
					pVideo->setMaterial(m);

					//카메라 시야경계박스그리기
					pVideo->draw3DBox(box,irr::video::SColor(255,255,0,0));

					//시야박스 충돌검사
					if(box.intersectsWithBox(box2)) //충돌했으면
					{
						pVideo->draw3DBox(box2,irr::video::SColor(255,255,0,0));
					}
					else
					{
						pVideo->draw3DBox(box2,irr::video::SColor(255,0,255,0));
					}		
				}

				//카메라 절두체그리기
				DrawViewFrustum(pVideo,(irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2"));

				pVideo->endScene();	
			}
			pDevice->drop();
		}


		//절두체 모양으로 직접 컬링하기
		namespace _01
		{
			void main()
			{
				irr::IrrlichtDevice *pDevice = irr::createDevice(					
					irr::video::EDT_OPENGL
					);

				pDevice->setWindowCaption(L"Type-A1");

				irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
				irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
				irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
				pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

				{
					irr::scene::ICameraSceneNode *pCam = pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,20,-15), irr::core::vector3df(0,0,0));
					pCam->setName("usr/scne/camera/1");
				}

				//힐플래인 메쉬 추가
				{
					irr::scene::IAnimatedMesh *pMesh =
						pSmgr->addHillPlaneMesh("usr/mesh/myhill",
						irr::core::dimension2d<irr::f32>(8,8),
						irr::core::dimension2d<irr::u32>(8,8),
						0,0,
						irr::core::dimension2d<irr::f32>(0,0),
						irr::core::dimension2d<irr::f32>(8,8)
						);
				}

				//씬노드 추가
				{
					irr::scene::IAnimatedMeshSceneNode *pNode =
						pSmgr->addAnimatedMeshSceneNode(pSmgr->getMesh("usr/mesh/myhill"));
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
					pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);
					//pNode->setMaterialTexture(0,pVideo->getTexture("wall.jpg"));

					pNode->setName("usr/scene/hill1");
				}

				//테스트용 씬노드
				{
					irr::scene::ISceneNode *pNode;
					pNode = pSmgr->addSphereSceneNode(2);
					pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
					pNode->setName("usr/scene/sphere/1");
				}

				{
					//irr::scene::ISceneNode *pRootNode = pSmgr->addEmptySceneNode();
					//pRootNode->setName("usr/scenenode/chr_root");

					irr::scene::ICameraSceneNode *pCam = pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,3,-5), irr::core::vector3df(0,0,0));
					pCam->setName("usr/scne/camera/2");
					pCam->setFarValue(10);


					irr::scene::ISceneNode *pNode;

					{
						pNode = pSmgr->addAnimatedMeshSceneNode(
							pSmgr->addArrowMesh("usr/mesh/arrow/red",
							irr::video::SColor(255,255,0,0),
							irr::video::SColor(255,255,0,0)
							),
							pCam,
							-1,
							irr::core::vector3df(0,0,0),
							irr::core::vector3df(0,0,0)
							);
						pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
						pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);

						pNode = pSmgr->addAnimatedMeshSceneNode(
							pSmgr->addArrowMesh("usr/mesh/arrow/green",
							irr::video::SColor(255,0,255,0),
							irr::video::SColor(255,0,255,0)
							),
							pCam,
							-1,
							irr::core::vector3df(0,0,0),
							irr::core::vector3df(0,0,-90)
							);
						pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
						pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);

						pNode = pSmgr->addAnimatedMeshSceneNode(
							pSmgr->addArrowMesh("usr/mesh/arrow/blue",
							irr::video::SColor(255,0,0,255),
							irr::video::SColor(255,0,0,255)
							),
							pCam,
							-1,
							irr::core::vector3df(0,0,0),
							irr::core::vector3df(90,0,0)
							);
						pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
						pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);
					}
				}

				pSmgr->setActiveCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/1"));

				//프레임 레이트 표시용 유아이
				irr::gui::IGUIStaticText *pstextFPS = 
					pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

				while(pDevice->run())
				{	
					static irr::u32 uLastTick=0;
					irr::u32 uTick = pDevice->getTimer()->getTime();						
					irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
					uLastTick = uTick;

					//프레임레이트 갱신, 삼각형수 표시
					{
						wchar_t wszbuf[256];
						swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
						pstextFPS->setText(wszbuf);
					}

					//fps 식 카메라 캐릭터 컨트롤
					ControlFPSCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2"),fDelta);
					//view frustum area update
					UpdateViewFrustum((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2"));

					//카메라 전환
                    /*
					if((::GetAsyncKeyState(VK_F1) & 0x8001) == 0x8001)
					{
						pSmgr->setActiveCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/1"));
					}
					if((::GetAsyncKeyState(VK_F2) & 0x8001) == 0x8001)
					{
						pSmgr->setActiveCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2"));
					}
                     */

					pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

					pSmgr->drawAll();
					pGuiEnv->drawAll();		
															
					//카메라 절두체그리기
					DrawViewFrustum(pVideo,(irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2"));

					//절투체 충돌 검사
					{
						irr::scene::ISceneNode *node =  pSmgr->getSceneNodeFromName("usr/scene/sphere/1");
						irr::scene::ICameraSceneNode *cam = (irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2");
						irr::scene::SViewFrustum frust = *cam->getViewFrustum();						

						irr::core::vector3df edges[8];
						node->getTransformedBoundingBox().getEdges(edges);

						bool boxInFrustum = true;
						
						for (irr::s32 i=0; i<irr::scene::SViewFrustum::VF_PLANE_COUNT; ++i)
						{
							bool edgeInFrustum=false;
							
							for (irr::u32 j=0; j<8; ++j)
							{
								if (frust.planes[i].classifyPointRelation(edges[j]) != irr::core::ISREL3D_FRONT) //앞면인지 판단
								{
									edgeInFrustum=true;
									break;
								}
							}

							if (!edgeInFrustum) //박스가 바깥에 있으면...
							{
								boxInFrustum = false;
								break;								
							}
						}

						irr::core::matrix4 mat;//단위행렬로초기화
						pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

						irr::video::SMaterial m;
						m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.					
						pVideo->setMaterial(m);

						if(boxInFrustum)
						{
							pVideo->draw3DBox(node->getTransformedBoundingBox(),irr::video::SColor(255,255,0,0));
						}
						else
						{
							pVideo->draw3DBox(node->getTransformedBoundingBox(),irr::video::SColor(255,0,255,0));
						}
					}

					pVideo->endScene();	
				}
				pDevice->drop();
			}
		}

	}

	/*	
	일리히트 씬노드의 카메라 컬링옵션은 1.5버전기준 두가지가있다.
	EAC_BOX
	EAC_FRUSTUM_BOX
	씬노드 생성할때 디폴트로 카메라시야박스(EAC_BOX)를 이용해서 한다.
	절두체에 비해 정확하지는 않지만 빠르게 검사할수있다.	
	*/	
	//일리히트 카메라 관련 오브잭트 컬링 EAC_FRUSTUM_BOX 모드 사용	
	//EAC_FRUSTUM_BOX 플래그이용 pNode->setAutomaticCulling(irr::scene::EAC_FRUSTUM_BOX);	
	namespace _06
	{		
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"절투체 컬링");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			{
				irr::scene::ICameraSceneNode *pCam = pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,20,-15), irr::core::vector3df(0,0,0));
				pCam->setName("usr/scne/camera/1");
			}

			//힐플래인 메쉬 추가
			{
				irr::scene::IAnimatedMesh *pMesh =
					pSmgr->addHillPlaneMesh("usr/mesh/myhill",
					irr::core::dimension2d<irr::f32>(8,8),
					irr::core::dimension2d<irr::u32>(8,8),
					0,0,
					irr::core::dimension2d<irr::f32>(0,0),
					irr::core::dimension2d<irr::f32>(8,8)
					);
			}

			//씬노드 추가
			{
				irr::scene::IAnimatedMeshSceneNode *pNode =
					pSmgr->addAnimatedMeshSceneNode(pSmgr->getMesh("usr/mesh/myhill"));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);
				//pNode->setMaterialTexture(0,pVideo->getTexture("wall.jpg"));

				pNode->setName("usr/scene/hill1");
			}

			//테스트용 씬노드
			{
				irr::scene::ISceneNode *pNode;
				pNode = pSmgr->addSphereSceneNode(2);
				pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setName("usr/scene/sphere/1");
				pNode->setAutomaticCulling(irr::scene::EAC_FRUSTUM_BOX); //절두체 컬링모드
			}

			{
				//irr::scene::ISceneNode *pRootNode = pSmgr->addEmptySceneNode();
				//pRootNode->setName("usr/scenenode/chr_root");

				irr::scene::ICameraSceneNode *pCam = pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,3,-5), irr::core::vector3df(0,0,0));
				pCam->setName("usr/scne/camera/2");
				pCam->setFarValue(10);


				irr::scene::ISceneNode *pNode;

				{
					pNode = pSmgr->addAnimatedMeshSceneNode(
						pSmgr->addArrowMesh("usr/mesh/arrow/red",
						irr::video::SColor(255,255,0,0),
						irr::video::SColor(255,255,0,0)
						),
						pCam,
						-1,
						irr::core::vector3df(0,0,0),
						irr::core::vector3df(0,0,0)
						);
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
					pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);

					pNode = pSmgr->addAnimatedMeshSceneNode(
						pSmgr->addArrowMesh("usr/mesh/arrow/green",
						irr::video::SColor(255,0,255,0),
						irr::video::SColor(255,0,255,0)
						),
						pCam,
						-1,
						irr::core::vector3df(0,0,0),
						irr::core::vector3df(0,0,-90)
						);
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
					pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);

					pNode = pSmgr->addAnimatedMeshSceneNode(
						pSmgr->addArrowMesh("usr/mesh/arrow/blue",
						irr::video::SColor(255,0,0,255),
						irr::video::SColor(255,0,0,255)
						),
						pCam,
						-1,
						irr::core::vector3df(0,0,0),
						irr::core::vector3df(90,0,0)
						);
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
					pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);
				}
			}

			pSmgr->setActiveCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/1"));

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				//fps 식 카메라 캐릭터 컨트롤
				ControlFPSCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2"),fDelta);

				//카메[라 전환
                /*
				if((::GetAsyncKeyState(VK_F1) & 0x8001) == 0x8001)
				{
					pSmgr->setActiveCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/1"));
				}
				if((::GetAsyncKeyState(VK_F2) & 0x8001) == 0x8001)
				{
					pSmgr->setActiveCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2"));
				}
                 */

				UpdateViewFrustum((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2"));

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				{
					irr::core::matrix4 mat;//단위행렬로초기화
					pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

					irr::video::SMaterial m;
					m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.					
					pVideo->setMaterial(m);					

					irr::scene::ISceneNode *pTestNode = pSmgr->getSceneNodeFromName("usr/scene/sphere/1");					

					//테스트오브잭트 경계박스구하기
					irr::core::aabbox3df box2 = pTestNode->getTransformedBoundingBox();
					//irr::core::aabbox3df box = pCam->getViewFrustum()->getBoundingBox(); //카메라 시야박스 구하기

					irr::scene::ICameraSceneNode *pOldActiveCam = pSmgr->getActiveCamera();
					pSmgr->setActiveCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2"));

					//카메라 절두체 포함 여부 판정
					if(!pSmgr->isCulled(pTestNode))
					{
						pVideo->draw3DBox(box2,irr::video::SColor(255,255,0,0));
					}
					else
					{
						pVideo->draw3DBox(box2,irr::video::SColor(255,0,255,0));
					}
					pSmgr->setActiveCamera(pOldActiveCam);

					//카메라 절두체그리기

					DrawViewFrustum(pVideo,(irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2"));
									
				}
				pVideo->endScene();	
			}
			pDevice->drop();
		}
	}



	//씬그라프 트리 구조를 이용한 카메라 시야 컬링 예제
	//setvisible 이용한방법
	namespace _07
	{	

		void CheckVisible(irr::scene::ICameraSceneNode *pCam, irr::scene::ISceneNode *pNode)
		{
			const irr::core::list<irr::scene::ISceneNode*> Children = pNode->getChildren();

			irr::core::aabbox3df box2 = pNode->getTransformedBoundingBox();
			irr::core::aabbox3df box = pCam->getViewFrustum()->getBoundingBox(); //카메라 시야박스 구하기

			if(box.intersectsWithBox(box2)) //충돌했으면
			{
				irr::core::list<irr::scene::ISceneNode*>::ConstIterator it = Children.begin();
				for (; it != Children.end(); ++it)
				{
					CheckVisible(pCam,(*it));
					//	(*it)->OnRegisterSceneNode();
				}
				pNode->setVisible(true);
			}
			else
			{
				pNode->setVisible(false);
			}

		}

		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			{
				irr::scene::ICameraSceneNode *pCam = pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,20,-15), irr::core::vector3df(0,0,0));
				pCam->setName("usr/scne/camera/1");
			}

			//힐플래인 메쉬 추가
			{
				irr::scene::IAnimatedMesh *pMesh =
					pSmgr->addHillPlaneMesh("usr/mesh/myhill",
					irr::core::dimension2d<irr::f32>(8,8),
					irr::core::dimension2d<irr::u32>(8,8),
					0,0,
					irr::core::dimension2d<irr::f32>(0,0),
					irr::core::dimension2d<irr::f32>(8,8)
					);
			}

			//씬노드 추가
			{
				irr::scene::IAnimatedMeshSceneNode *pNode =
					pSmgr->addAnimatedMeshSceneNode(pSmgr->getMesh("usr/mesh/myhill"));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);
				//pNode->setMaterialTexture(0,pVideo->getTexture("wall.jpg"));

				pNode->setName("usr/scene/hill1");
			}

			//테스트용 씬노드
			{
				irr::scene::ISceneNode *pNode,*pNode2;
				irr::scene::IAnimatedMesh *pAniMesh = pSmgr->addSphereMesh("usr/mesh/sphere1",2);
				//pNode = pSmgr->addOctTreeSceneNode(pAniMesh);
				pNode = pSmgr->addAnimatedMeshSceneNode(pAniMesh);
				pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setName("usr/scene/sphere/1");				

				//pNode2 = pSmgr->addOctTreeSceneNode(pAniMesh,pNode);
				pNode2 = pSmgr->addAnimatedMeshSceneNode(pAniMesh,pNode);
				pNode2->setMaterialFlag(irr::video::EMF_WIREFRAME,false);
				pNode2->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode2->setName("usr/scene/sphere/2");
				pNode2->setPosition(irr::core::vector3df(8,0,0));
			}

			//태스트용 카메라 만들기
			irr::scene::ICameraSceneNode *pCam = pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,3,-5), irr::core::vector3df(0,0,0));
			pCam->setName("usr/scne/camera/2");
			pCam->setFarValue(10);

			//축노드 추가
			pCam->addChild(AddAxiesNode(pSmgr));

			pSmgr->setActiveCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/1"));

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				//fps 식 카메라 캐릭터 컨트롤
				ControlFPSCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2"),fDelta);

				
                /*
                 if((::GetAsyncKeyState(VK_F1) & 0x8001) == 0x8001)
				{
					pSmgr->setActiveCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/1"));
				}
				if((::GetAsyncKeyState(VK_F2) & 0x8001) == 0x8001)
				{
					pSmgr->setActiveCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2"));
				}
                 */

				//카메라 뷰영역 갱신
				UpdateViewFrustum((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2"));		



				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				{
					irr::scene::ICameraSceneNode *pCam = (irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2");

					//트리순회하면서 가시화 체킹하기
					CheckVisible(pCam,pSmgr->getSceneNodeFromName("usr/scene/sphere/1"));					

					irr::core::matrix4 mat;//단위행렬로초기화
					pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

					irr::video::SMaterial m;
					m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.					
					pVideo->setMaterial(m);

					//테스트오브잭트 경계박스구하기
					{
						irr::scene::ISceneNode *pNode = pSmgr->getSceneNodeFromName("usr/scene/sphere/1");
						irr::core::aabbox3df box2 = pNode->getTransformedBoundingBox();
						irr::core::aabbox3df box = pCam->getViewFrustum()->getBoundingBox(); //카메라 시야박스 구하기

						pVideo->draw3DBox(box,irr::video::SColor(255,255,0,0));

						//시야박스 충돌검사
						if(box.intersectsWithBox(box2)) //충돌했으면
						{
							//pNode->setVisible(true);
							pVideo->draw3DBox(box2,irr::video::SColor(255,255,0,0));
						}
						else
						{
							//pNode->setVisible(false);
							pVideo->draw3DBox(box2,irr::video::SColor(255,0,255,0));
						}
					}

					//테스트오브잭트 경계박스구하기
					{
						irr::scene::ISceneNode *pNode = pSmgr->getSceneNodeFromName("usr/scene/sphere/2");
						irr::core::aabbox3df box2 = pNode->getTransformedBoundingBox();
						irr::core::aabbox3df box = pCam->getViewFrustum()->getBoundingBox(); //카메라 시야박스 구하기

						pVideo->draw3DBox(box,irr::video::SColor(255,255,0,0));

						//시야박스 충돌검사
						if(box.intersectsWithBox(box2)) //충돌했으면
						{
							pVideo->draw3DBox(box2,irr::video::SColor(255,255,0,0));
						}
						else
						{
							pVideo->draw3DBox(box2,irr::video::SColor(255,0,255,0));
						}
					}

					DrawViewFrustum(pVideo,(irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2"));
				}
				pVideo->endScene();	
			}
			pDevice->drop();
		}
	}



	//ABV씬노드이용해서 씬트리 트리 가시화 컬링해주기
	//카메라 시야 바운딩 박스이용한 방법 f4
	//카메라 절두체 를 이용한방법 f3
	namespace _08
	{
		class CABVSceneNode : public irr::scene::ISceneNode
		{
			irr::core::aabbox3d<irr::f32> Box;
			irr::scene::ICameraSceneNode *m_pCamera;

		public:
			CABVSceneNode(irr::scene::ISceneNode* parent, irr::scene::ISceneManager* mgr, irr::s32 id,irr::core::aabbox3d<irr::f32> abv,
				irr::scene::ICameraSceneNode *pCam=NULL)
				: irr::scene::ISceneNode(parent, mgr, id)
			{
				Box = abv;
				m_pCamera = pCam;
			}

			bool isCulled()
			{
				if(m_pCamera)
				{
					//가시화용 예제 절두체로 교체
					bool bCull;
					irr::scene::ICameraSceneNode *pOldCamera;
					pOldCamera = SceneManager->getActiveCamera();

					SceneManager->setActiveCamera(m_pCamera);

					bCull = SceneManager->isCulled(this);

					SceneManager->setActiveCamera(pOldCamera);

					return bCull;

				}
				else
				{
					return SceneManager->isCulled(this);
				}
			}

			virtual void OnRegisterSceneNode()
			{ 
				//카메라절두체 컬링 확인
				if(isCulled())
				{
				}
				else
				{
					if (IsVisible)    
						SceneManager->registerNodeForRendering(this);
					ISceneNode::OnRegisterSceneNode();
				}
			}		

			virtual void render()
			{  
				if(DebugDataVisible | irr::scene::EDS_BBOX)
				{
					SceneManager->getVideoDriver()->setTransform(irr::video::ETS_WORLD,irr::core::IdentityMatrix);
					irr::core::aabbox3df box2 = getTransformedBoundingBox();					
					SceneManager->getVideoDriver()->draw3DBox(box2,irr::video::SColor(255,0,255,0));					
				}
			}

			virtual const irr::core::aabbox3d<irr::f32>& getBoundingBox() const  
			{    
				return Box;  
			}

		};

		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");			

			{
				irr::scene::ICameraSceneNode *pCam = pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,20,-15), irr::core::vector3df(0,0,0));
				pCam->setName("usr/scne/camera/1");
			}

			//태스트용 카메라 만들기
			irr::scene::ICameraSceneNode *pCam = pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,3,-5), irr::core::vector3df(0,0,0));
			pCam->setName("usr/scne/camera/2");
			pCam->setFarValue(10);

			//축노드 추가
			pCam->addChild(AddAxiesNode(pSmgr));

			
			pSmgr->setActiveCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/1"));		


			irr::scene::IAnimatedMesh *pAniMesh = pSmgr->addSphereMesh("usr/mesh/sphere1",2);

			//바운딩박스 씬노드생성
			{
				irr::core::aabbox3df abv(irr::core::vector3df(-6,-2,-6),irr::core::vector3df(6,2,6));
				irr::scene::ISceneNode *pNode =	new CABVSceneNode(
					pSmgr->getRootSceneNode(),
					pSmgr,
					-1,
					abv,
					(irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2")
					);
				pNode->setName("usr/node/abv0");
				pNode->drop();			
			}

			irr::scene::ISceneNode *pNodeRoot = pSmgr->getSceneNodeFromName("usr/node/abv0");	

			{
				//바운딩박스볼륨노드추가
				irr::core::aabbox3df abv(irr::core::vector3df(-2,-2,-2),irr::core::vector3df(2,2,2));
				irr::scene::ISceneNode *pNode = new CABVSceneNode(
					pNodeRoot,					
					pSmgr,
					-1,
					abv,
					(irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2")
					);
				pNode->setName("usr/node/abv1");
				pNode->setPosition(irr::core::vector3df(3,0,3));
				pNode->drop();						

				//구 노드추가			
				pNode = pSmgr->addAnimatedMeshSceneNode(pAniMesh,pNode);
				pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setName("usr/scene/sphere/1");								
			}

			{
				//바운딩박스볼륨노드추가
				irr::core::aabbox3df abv(irr::core::vector3df(-2,-2,-2),irr::core::vector3df(2,2,2));
				irr::scene::ISceneNode *pNode = new CABVSceneNode(
					pNodeRoot,					
					pSmgr,
					-1,
					abv,
					(irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2")
					);
				pNode->setName("usr/node/abv2");
				pNode->setPosition(irr::core::vector3df(-3,0,3));
				pNode->drop();						

				//구 노드추가			
				pNode = pSmgr->addAnimatedMeshSceneNode(pAniMesh,pNode);
				pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setName("usr/scene/sphere/2");								
			}

			{
				//바운딩박스볼륨노드추가
				irr::core::aabbox3df abv(irr::core::vector3df(-2,-2,-2),irr::core::vector3df(2,2,2));
				irr::scene::ISceneNode *pNode = new CABVSceneNode(
					pNodeRoot,					
					pSmgr,
					-1,
					abv,
					(irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2")
					);
				pNode->setName("usr/node/abv3");
				pNode->setPosition(irr::core::vector3df(3,0,-3));
				pNode->drop();						

				//구 노드추가			
				pNode = pSmgr->addAnimatedMeshSceneNode(pAniMesh,pNode);
				pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setName("usr/scene/sphere/3");								
			}

			{
				//바운딩박스볼륨노드추가
				irr::core::aabbox3df abv(irr::core::vector3df(-2,-2,-2),irr::core::vector3df(2,2,2));
				irr::scene::ISceneNode *pNode = new CABVSceneNode(
					pNodeRoot,					
					pSmgr,
					-1,
					abv,
					(irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2")
					);
				pNode->setName("usr/node/abv4");
				pNode->setPosition(irr::core::vector3df(-3,0,-3));
				pNode->drop();						

				//구 노드추가			
				pNode = pSmgr->addAnimatedMeshSceneNode(pAniMesh,pNode);
				pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setName("usr/scene/sphere/4");								
			}		


			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				//fps 식 카메라 캐릭터 컨트롤
				ControlFPSCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2"),fDelta);
				UpdateViewFrustum((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2"));

				{
                    /*
					if((::GetAsyncKeyState(VK_F1) & 0x8001) == 0x8001)
					{
						pSmgr->setActiveCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/1"));
					}
					if((::GetAsyncKeyState(VK_F2) & 0x8001) == 0x8001)
					{
						pSmgr->setActiveCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2"));
					}

					if((::GetAsyncKeyState(VK_F3) & 0x8001) == 0x8001) //절두체 컬링
					{
						pSmgr->getSceneNodeFromName("usr/node/abv0")->setAutomaticCulling(irr::scene::EAC_FRUSTUM_BOX);
						pSmgr->getSceneNodeFromName("usr/node/abv1")->setAutomaticCulling(irr::scene::EAC_FRUSTUM_BOX);
						pSmgr->getSceneNodeFromName("usr/node/abv2")->setAutomaticCulling(irr::scene::EAC_FRUSTUM_BOX);
						pSmgr->getSceneNodeFromName("usr/node/abv3")->setAutomaticCulling(irr::scene::EAC_FRUSTUM_BOX);
						pSmgr->getSceneNodeFromName("usr/node/abv4")->setAutomaticCulling(irr::scene::EAC_FRUSTUM_BOX);
					}
					if((::GetAsyncKeyState(VK_F4) & 0x8001) == 0x8001)//aabbox 컬링
					{
						pSmgr->getSceneNodeFromName("usr/node/abv0")->setAutomaticCulling(irr::scene::EAC_BOX);
						pSmgr->getSceneNodeFromName("usr/node/abv1")->setAutomaticCulling(irr::scene::EAC_BOX);
						pSmgr->getSceneNodeFromName("usr/node/abv2")->setAutomaticCulling(irr::scene::EAC_BOX);
						pSmgr->getSceneNodeFromName("usr/node/abv3")->setAutomaticCulling(irr::scene::EAC_BOX);
						pSmgr->getSceneNodeFromName("usr/node/abv4")->setAutomaticCulling(irr::scene::EAC_BOX);
					}
                     */
				}				

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		


				{
					irr::scene::ICameraSceneNode *pCam = (irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2");					

					const irr::scene::SViewFrustum *pVf = pCam->getViewFrustum();

					irr::core::matrix4 mat;//단위행렬로초기화
					pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

					irr::video::SMaterial m;
					m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.					
					pVideo->setMaterial(m);										

					{
						irr::scene::ICameraSceneNode *poldCam;
						irr::scene::ISceneNode *pNode = pSmgr->getSceneNodeFromName("usr/node/abv0");					

						irr::core::aabbox3df box2 = pNode->getTransformedBoundingBox();						

						poldCam = pSmgr->getActiveCamera();
						pSmgr->setActiveCamera(pCam);

						if(!pSmgr->isCulled(pNode))
						{
							pVideo->draw3DBox(box2,irr::video::SColor(255,255,0,0));
						}
						else
						{
							pVideo->draw3DBox(box2,irr::video::SColor(255,0,255,0));
						}
						pSmgr->setActiveCamera(poldCam);
					}

					{
						irr::scene::ICameraSceneNode *poldCam;
						irr::scene::ISceneNode *pNode = pSmgr->getSceneNodeFromName("usr/node/abv1");					

						irr::core::aabbox3df box2 = pNode->getTransformedBoundingBox();					

						poldCam = pSmgr->getActiveCamera();
						pSmgr->setActiveCamera(pCam);

						if(!pSmgr->isCulled(pNode))
						{
							pVideo->draw3DBox(box2,irr::video::SColor(255,255,0,0));
						}
						else
						{
							pVideo->draw3DBox(box2,irr::video::SColor(255,0,255,0));
						}
						pSmgr->setActiveCamera(poldCam);
					}

					{
						irr::scene::ICameraSceneNode *poldCam;
						irr::scene::ISceneNode *pNode = pSmgr->getSceneNodeFromName("usr/node/abv2");					

						irr::core::aabbox3df box2 = pNode->getTransformedBoundingBox();					

						poldCam = pSmgr->getActiveCamera();
						pSmgr->setActiveCamera(pCam);

						if(!pSmgr->isCulled(pNode))
						{
							pVideo->draw3DBox(box2,irr::video::SColor(255,255,0,0));
						}
						else
						{
							pVideo->draw3DBox(box2,irr::video::SColor(255,0,255,0));
						}
						pSmgr->setActiveCamera(poldCam);
					}

					{
						irr::scene::ICameraSceneNode *poldCam;
						irr::scene::ISceneNode *pNode = pSmgr->getSceneNodeFromName("usr/node/abv3");					

						irr::core::aabbox3df box2 = pNode->getTransformedBoundingBox();					

						poldCam = pSmgr->getActiveCamera();
						pSmgr->setActiveCamera(pCam);

						if(!pSmgr->isCulled(pNode))
						{
							pVideo->draw3DBox(box2,irr::video::SColor(255,255,0,0));
						}
						else
						{
							pVideo->draw3DBox(box2,irr::video::SColor(255,0,255,0));
						}
						pSmgr->setActiveCamera(poldCam);
					}
					{
						irr::scene::ICameraSceneNode *poldCam;
						irr::scene::ISceneNode *pNode = pSmgr->getSceneNodeFromName("usr/node/abv4");					

						irr::core::aabbox3df box2 = pNode->getTransformedBoundingBox();					

						poldCam = pSmgr->getActiveCamera();
						pSmgr->setActiveCamera(pCam);

						if(!pSmgr->isCulled(pNode))
						{
							pVideo->draw3DBox(box2,irr::video::SColor(255,255,0,0));
						}
						else
						{
							pVideo->draw3DBox(box2,irr::video::SColor(255,0,255,0));
						}
						pSmgr->setActiveCamera(poldCam);
					}					

					////카메라 경계박스 그리기
					if(pSmgr->getSceneNodeFromName("usr/node/abv0")->getAutomaticCulling() == irr::scene::EAC_BOX)
					{	
						irr::core::aabbox3df box = pCam->getViewFrustum()->getBoundingBox(); //카메라 시야박스 구하기
						pVideo->draw3DBox(box,irr::video::SColor(255,255,0,0));					
					}


					//카메라 절두체 그리기
					DrawViewFrustum(pVideo,pCam);

				}

				pVideo->endScene();	
			}

			pDevice->drop();
		}	

		//퀄링된 씬노드의 애니메이터 적용 여부 확인 예제
		namespace _01
		{
			void main()
			{
				irr::IrrlichtDevice *pDevice = irr::createDevice(					
					irr::video::EDT_OPENGL
					);

				pDevice->setWindowCaption(L"");

				irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
				irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
				irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

				pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");			

				{
					irr::scene::ICameraSceneNode *pCam = pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,0,-25), irr::core::vector3df(0,0,0));
					pCam->setName("usr/scne/camera/1");
				}

				//태스트용 카메라 만들기
				irr::scene::ICameraSceneNode *pCam = pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,3,-5), irr::core::vector3df(0,0,0));
				pCam->setName("usr/scne/camera/2");
				pCam->setFarValue(10);

				//축노드 추가
				pCam->addChild(AddAxiesNode(pSmgr));


				pSmgr->setActiveCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/1"));		

				irr::scene::IAnimatedMesh *pAniMesh = pSmgr->addSphereMesh("usr/mesh/sphere1",2);

				{
					irr::scene::ISceneNode *pNode = pSmgr->addCubeSceneNode(4);
					pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
					pNode->setDebugDataVisible(irr::scene::EDS_BBOX);
					pNode->setName("usr/node/abv2");
					
					irr::scene::ISceneNodeAnimator *pAnim = pSmgr->createFlyStraightAnimator(irr::core::vector3df(15,0,0),irr::core::vector3df(-15,0,0),
						5000,true,true);
					pNode->addAnimator(pAnim);
					//pAnim->createClone(pNode);
					pAnim->drop();
					pNode->setAutomaticCulling(irr::scene::EAC_BOX);

					pNode->setVisible(false);

				}
				
				{
					//바운딩박스볼륨노드추가
					irr::core::aabbox3df abv(irr::core::vector3df(-2,-2,-2),irr::core::vector3df(2,2,2));
					irr::scene::ISceneNode *pNode = new CABVSceneNode(
						pSmgr->getRootSceneNode(),						
						pSmgr,
						-1,
						abv,
						(irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2")
						);
					pNode->setName("usr/node/abv1");
					pNode->setPosition(irr::core::vector3df(0,0,0));
					pNode->drop();						
					pNode->setDebugDataVisible(irr::scene::EDS_BBOX);	

					irr::scene::ISceneNodeAnimator *pAnim = pSmgr->createFlyStraightAnimator(irr::core::vector3df(15,0,0),irr::core::vector3df(-15,0,0),
						5000,true,true);
					pNode->addAnimator(pAnim);
					pAnim->drop();

					//구 노드추가			
					pNode = pSmgr->addAnimatedMeshSceneNode(pAniMesh,pNode);
					pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
					pNode->setName("usr/scene/sphere/1");								
					pNode->setDebugDataVisible(irr::scene::EDS_BBOX);	
					pNode->setPosition(irr::core::vector3df(5,0,0));		
					pNode->setAutomaticCulling(irr::scene::EAC_BOX);
				}			


				//프레임 레이트 표시용 유아이
				irr::gui::IGUIStaticText *pstextFPS = 
					pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

				while(pDevice->run())
				{	
					static irr::u32 uLastTick=0;
					irr::u32 uTick = pDevice->getTimer()->getTime();						
					irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
					uLastTick = uTick;

					//프레임레이트 갱신, 삼각형수 표시
					{
						wchar_t wszbuf[256];
						swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
						pstextFPS->setText(wszbuf);
					}

					//프레임레이트 갱신, 삼각형수 표시
					{
						wchar_t wszbuf[256];
						swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
						pstextFPS->setText(wszbuf);
					}

					//fps 식 카메라 캐릭터 컨트롤
					ControlFPSCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2"),fDelta);
					UpdateViewFrustum((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2"));

					{
                        /*
						if((::GetAsyncKeyState(VK_F1) & 0x8001) == 0x8001)
						{
							pSmgr->setActiveCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/1"));
						}
						if((::GetAsyncKeyState(VK_F2) & 0x8001) == 0x8001)
						{
							pSmgr->setActiveCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2"));
						}

						if((::GetAsyncKeyState(VK_F3) & 0x8001) == 0x8001) //절두체 컬링
						{							
							pSmgr->getSceneNodeFromName("usr/node/abv1")->setAutomaticCulling(irr::scene::EAC_FRUSTUM_BOX);							
						}
						if((::GetAsyncKeyState(VK_F4) & 0x8001) == 0x8001)//aabbox 컬링
						{							
							pSmgr->getSceneNodeFromName("usr/node/abv1")->setAutomaticCulling(irr::scene::EAC_BOX);						
						}
                         */

						//일반씬노드의 자식으로 붙이기
                        /*
						if((::GetAsyncKeyState(VK_F5) & 0x8001) == 0x8001) 
						{			
							pSmgr->getSceneNodeFromName("usr/node/abv2")->setVisible(true);
							pSmgr->getSceneNodeFromName(
								"usr/scene/sphere/1")->setParent(
									pSmgr->getSceneNodeFromName("usr/node/abv2")	
								);							
						}
						//커스텀바운딩박스볼륨 씬노드의 자식으로 붙이기
						if((::GetAsyncKeyState(VK_F6) & 0x8001) == 0x8001) 
						{			
							//pSmgr->getSceneNodeFromName("usr/node/abv2")->setVisible(true);
							pSmgr->getSceneNodeFromName(
								"usr/scene/sphere/1")->setParent(
								pSmgr->getSceneNodeFromName("usr/node/abv1")	
								);							
						}
                         */
					}				

					pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

					pSmgr->drawAll();
					pGuiEnv->drawAll();		


					{
						irr::scene::ICameraSceneNode *pCam = (irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2");					

						const irr::scene::SViewFrustum *pVf = pCam->getViewFrustum();

						irr::core::matrix4 mat;//단위행렬로초기화
						pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

						irr::video::SMaterial m;
						m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.					
						pVideo->setMaterial(m);										

						
												

						////카메라 경계박스 그리기
						if(pSmgr->getSceneNodeFromName("usr/node/abv1")->getAutomaticCulling() == irr::scene::EAC_BOX)
						{	
							irr::core::aabbox3df box = pCam->getViewFrustum()->getBoundingBox(); //카메라 시야박스 구하기
							pVideo->draw3DBox(box,irr::video::SColor(255,255,0,0));					
						}


						//카메라 절두체 그리기
						DrawViewFrustum(pVideo,pCam);

					}

					pVideo->endScene();	
				}

				pDevice->drop();
			}	

		}

	}


	//2D aabb 충돌검출
	namespace _09
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			irr::core::recti rectA(120,120,150,150),rectB(220,120,250,150);



			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);


			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;				

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}
                
                /*

				if((::GetAsyncKeyState(VK_LEFT) & 0x8001) == 0x8001)
				{
					rectA -= irr::core::vector2di(1,0);
				}
				if((::GetAsyncKeyState(VK_RIGHT) & 0x8001) == 0x8001)
				{
					rectA += irr::core::vector2di(1,0);
				}
                 
                 */

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();			

				if(rectA.isRectCollided(rectB))
				{
					pVideo->draw2DRectangleOutline(rectA,irr::video::SColor(255,255,0,0));
				}
				else
				{
					pVideo->draw2DRectangleOutline(rectA,irr::video::SColor(255,255,255,0));
				}
				pVideo->draw2DRectangleOutline(rectB,irr::video::SColor(255,255,255,0));

				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}

	//aabb박스와 lind3d 충돌 검사
	namespace _10
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,5,-10), irr::core::vector3df(0,0,0));

			irr::core::aabbox3df box(-1,-1,-1,1,1,1);

			//irr::core::line3df PickRay;

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);


			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;



				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				{	
					irr::video::SMaterial m;
					m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.
					pVideo->setMaterial(m);				
					pVideo->setTransform(irr::video::ETS_WORLD, irr::core::IdentityMatrix);



					pVideo->draw3DBox(box, irr::video::SColor(255,255,0,0));
				}

				//충돌점 구하기
				{
					irr::core::line3df PickingRay;			
					irr::core::position2di mouse_pos = pDevice->getCursorControl()->getPosition();		
					//2디 스크린좌표계로부터 피킹레이 얻기
					PickingRay = pSmgr->getSceneCollisionManager()->getRayFromScreenCoordinates(mouse_pos);	

					irr::core::vector3df  edges[8];

					box.getEdges(edges);

					irr::f32 fdist;
					bool bIsColl=false;
					irr::core::triangle3df tri;
					irr::core::vector3df PickPoint,_PickPoint;

					tri.set(edges[0],edges[1],edges[2]);					
					if(tri.getIntersectionWithLimitedLine(PickingRay,_PickPoint))
					{
						if( PickingRay.start.getDistanceFrom(_PickPoint) < fdist || bIsColl==false)
						{
							fdist = PickingRay.start.getDistanceFrom(_PickPoint);
							PickPoint = _PickPoint;							
						}
						bIsColl = true;
					}

					tri.set(edges[1],edges[2],edges[3]);					
					if(tri.getIntersectionWithLimitedLine(PickingRay,_PickPoint))
					{
						if( PickingRay.start.getDistanceFrom(_PickPoint) < fdist || bIsColl==false)
						{
							fdist = PickingRay.start.getDistanceFrom(_PickPoint);
							PickPoint = _PickPoint;							
						}
						bIsColl = true;
					}

					tri.set(edges[0],edges[1],edges[4]);					
					if(tri.getIntersectionWithLimitedLine(PickingRay,_PickPoint))
					{
						if( PickingRay.start.getDistanceFrom(_PickPoint) < fdist || bIsColl==false)
						{
							fdist = PickingRay.start.getDistanceFrom(_PickPoint);
							PickPoint = _PickPoint;							
						}
						bIsColl = true;
					}

					tri.set(edges[1],edges[5],edges[4]);					
					if(tri.getIntersectionWithLimitedLine(PickingRay,_PickPoint))
					{
						if( PickingRay.start.getDistanceFrom(_PickPoint) < fdist || bIsColl==false)
						{
							fdist = PickingRay.start.getDistanceFrom(_PickPoint);
							PickPoint = _PickPoint;							
						}
						bIsColl = true;
					}

					tri.set(edges[1],edges[3],edges[5]);					
					if(tri.getIntersectionWithLimitedLine(PickingRay,_PickPoint))
					{
						if( PickingRay.start.getDistanceFrom(_PickPoint) < fdist || bIsColl==false)
						{
							fdist = PickingRay.start.getDistanceFrom(_PickPoint);
							PickPoint = _PickPoint;							
						}
						bIsColl = true;
					}

					tri.set(edges[3],edges[5],edges[7]);					
					if(tri.getIntersectionWithLimitedLine(PickingRay,_PickPoint))
					{
						if( PickingRay.start.getDistanceFrom(_PickPoint) < fdist || bIsColl==false)
						{
							fdist = PickingRay.start.getDistanceFrom(_PickPoint);
							PickPoint = _PickPoint;							
						}
						bIsColl = true;
					}

					tri.set(edges[3],edges[2],edges[6]);					
					if(tri.getIntersectionWithLimitedLine(PickingRay,_PickPoint))
					{
						if( PickingRay.start.getDistanceFrom(_PickPoint) < fdist || bIsColl==false)
						{
							fdist = PickingRay.start.getDistanceFrom(_PickPoint);
							PickPoint = _PickPoint;							
						}
						bIsColl = true;
					}

					tri.set(edges[3],edges[7],edges[6]);					
					if(tri.getIntersectionWithLimitedLine(PickingRay,_PickPoint))
					{
						if( PickingRay.start.getDistanceFrom(_PickPoint) < fdist || bIsColl==false)
						{
							fdist = PickingRay.start.getDistanceFrom(_PickPoint);
							PickPoint = _PickPoint;							
						}
						bIsColl = true;
					}

					tri.set(edges[0],edges[2],edges[4]);					
					if(tri.getIntersectionWithLimitedLine(PickingRay,_PickPoint))
					{
						if( PickingRay.start.getDistanceFrom(_PickPoint) < fdist || bIsColl==false)
						{
							fdist = PickingRay.start.getDistanceFrom(_PickPoint);
							PickPoint = _PickPoint;							
						}
						bIsColl = true;
					}

					tri.set(edges[2],edges[4],edges[6]);					
					if(tri.getIntersectionWithLimitedLine(PickingRay,_PickPoint))
					{
						if( PickingRay.start.getDistanceFrom(_PickPoint) < fdist || bIsColl==false)
						{
							fdist = PickingRay.start.getDistanceFrom(_PickPoint);
							PickPoint = _PickPoint;							
						}
						bIsColl = true;
					}

					tri.set(edges[4],edges[5],edges[7]);					
					if(tri.getIntersectionWithLimitedLine(PickingRay,_PickPoint))
					{
						if( PickingRay.start.getDistanceFrom(_PickPoint) < fdist || bIsColl==false)
						{
							fdist = PickingRay.start.getDistanceFrom(_PickPoint);
							PickPoint = _PickPoint;							
						}
						bIsColl = true;
					}

					tri.set(edges[4],edges[6],edges[7]);					
					if(tri.getIntersectionWithLimitedLine(PickingRay,_PickPoint))
					{
						if( PickingRay.start.getDistanceFrom(_PickPoint) < fdist || bIsColl==false)
						{
							fdist = PickingRay.start.getDistanceFrom(_PickPoint);
							PickPoint = _PickPoint;							
						}
						bIsColl = true;
					}

					if(bIsColl) //충돌여부
					{	
						irr::video::SMaterial m;
						m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.
						pVideo->setMaterial(m);				
						pVideo->setTransform(irr::video::ETS_WORLD, irr::core::IdentityMatrix);

						pVideo->draw3DLine(
							irr::core::vector3df(0,0,0),
							PickPoint,//충돌점
							irr::video::SColor(255,255,255,0)
							);

						//pVideo->draw3DTriangle(tri, irr::video::SColor(0,255,0,0));

					}	
				}
				pVideo->endScene();	
			}
			pDevice->drop();
		}
	}


	//3d 좌표를 2d로 변환하기
	namespace _11
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"11-11");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			irr::scene::ICameraSceneNode *pCam = 
				pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,5,-25), irr::core::vector3df(0,0,0));			

			{
				irr::scene::ISceneNode *pNode;
				pNode = pSmgr->addCubeSceneNode();
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);
				pNode->setName("usr/scene/box");
				irr::scene::ISceneNodeAnimator *pAnim;
				pAnim = pSmgr->createFlyStraightAnimator(
					irr::core::vector3df(-10,0,0),
					irr::core::vector3df(10,0,0),5000,true,true);
				pNode->addAnimator(pAnim);
				pAnim->drop();
			}

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);


			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;				


				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				{		
					irr::scene::ISceneNode *pNode = pSmgr->getSceneNodeFromName("usr/scene/box");
					irr::core::vector2di pos2d = 
						pSmgr->getSceneCollisionManager()->getScreenCoordinatesFrom3DPosition(
						pNode->getAbsolutePosition(),pCam);

					pVideo->draw2DLine(irr::core::vector2di(320,0),pos2d);
				}

				pVideo->endScene();	
			}
			pDevice->drop();
		}
	}

	//2d를 3d로 변환하기
	namespace _12
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"11-11");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			irr::scene::ICameraSceneNode *pCam = 
				pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,20,-25), irr::core::vector3df(0,0,0));			

			{
				irr::scene::ISceneNode *pNode;
				pNode = pSmgr->addCubeSceneNode();
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);
				pNode->setName("usr/scene/box");
				/*irr::scene::ISceneNodeAnimator *pAnim;
				pAnim = pSmgr->createFlyStraightAnimator(
				irr::core::vector3df(-10,0,0),
				irr::core::vector3df(10,0,0),5000,true,true);
				pNode->addAnimator(pAnim);
				pAnim->drop();*/
			}

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);


			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;				


				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				{
					irr::core::vector2di mouse_pos = pDevice->getCursorControl()->getPosition();

					irr::core::line3df Ray;

					irr::core::plane3df plane;
					irr::core::vector3df pos3d; //3차원 공간상의 좌표
					plane.setPlane(irr::core::vector3df(0,0,0),irr::core::vector3df(0,1,0)); //높이가 0인 지면설정
					Ray = pSmgr->getSceneCollisionManager()->getRayFromScreenCoordinates(mouse_pos);
					//지면과 광선 충돌점구하기					
					plane.getIntersectionWithLine(
						Ray.start, //시작위치
						Ray.getVector().normalize(), //방향벡터
						pos3d);

					irr::scene::ISceneNode *pNode = 
						pSmgr->getSceneNodeFromName("usr/scene/box");

					pNode->setPosition(pos3d);
				}

				pVideo->endScene();	
			}
			pDevice->drop();
		}
	}
	
}


