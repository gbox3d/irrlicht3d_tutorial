﻿
#include <irrlicht.h>
#include <iostream>
//
//
////#include <SDKDDKVer.h>
////#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
//// Windows Header Files:
//#include <windows.h>
//
////stl header
//#include <vector>
//#include <map>


/////////////////////////////////////////////////
//지형 처리
//
/////////////////////////////////////////////////
#define _CRT_SECURE_NO_WARNINGS

namespace lsn12 {

	//터래인출력예제
	namespace _00
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"터래인 출력");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			{
				irr::scene::ICameraSceneNode* pCam =
					pSmgr->addCameraSceneNodeFPS(0,100.0f,1.2f);

				pCam->setPosition(irr::core::vector3df(2700*2,255*2,2600*2));
				pCam->setTarget(irr::core::vector3df(2397*2,343*2,2700*2));
				pCam->setFarValue(42000.0f);

				pCam->setName("usr/scene/camera/mainCam");

				// disable mouse cursor
				//마우스 커서 안보이게 하기
				pDevice->getCursorControl()->setVisible(false);		
			}


			{
				irr::scene::ITerrainSceneNode* pTerrain = pSmgr->addTerrainSceneNode( 
					"terrain-heightmap.bmp",
					0,										// parent node
					-1,										// node id
					irr::core::vector3df(0.f, 0.f, 0.f),			// position
					irr::core::vector3df(0.f, 0.f, 0.f),			// rotation
					irr::core::vector3df(40.f, 4.4f, 40.f),		// scale
					irr::video::SColor ( 255, 255, 255, 255 ),	// vertexColor,
					5,										// maxLOD
					irr::scene::ETPS_17,					// patchSize
					4										// smoothFactor
					);

				pTerrain->setMaterialFlag(irr::video::EMF_LIGHTING, false);

				pTerrain->setMaterialTexture(0, pVideo->getTexture("terrain-texture.jpg"));
				pTerrain->setMaterialTexture(1, pVideo->getTexture("detailmap3.jpg"));

				pTerrain->setMaterialType(irr::video::EMT_DETAIL_MAP);

				pTerrain->scaleTexture(1.0f, 20.0f);
				pTerrain->setName("Terrain");
			}

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick = 0;					
				irr::u32 uTick = pDevice->getTimer()->getTime();
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;


				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}		
	}





	//터래인 메쉬 바꾸기 예제(편집하기 예제)
	//버텍스 위치 바꾸기
	/*
	i,j,k,m 으로 선탣된 버텍스 바꾸기
	q,a 버텍스 들어올리고 내리기

	방향키,paup,pgdn : 카메라 이동
	방향기 + 컨트롤 :  카메라 회전

	*/
	namespace _01
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"터래인 출력");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			//irr::core::quaternion qtCam;
			{
				irr::scene::ICameraSceneNode* pCam = pSmgr->addCameraSceneNode(
					0,irr::core::vector3df(0,30,-3),irr::core::vector3df(0,0,0)
					);
				pCam->setName("usr/scene/camera/mainCam");
				irr::core::vector3df vFront = (pCam->getTarget() - pCam->getAbsolutePosition());
				vFront.normalize();
				pCam->setRotation(vFront.getHorizontalAngle());				
			}

			{
				irr::scene::ITerrainSceneNode* pTerrain = pSmgr->addTerrainSceneNode( 
					"terrain-heightmap.bmp",
					0,										// parent node
					-1,										// node id
					irr::core::vector3df(0.f, 0.f, 0.f),			// position
					irr::core::vector3df(0.f, 0.f, 0.f),			// rotation
					irr::core::vector3df(1.f, 1.f, 1.f),		// scale
					irr::video::SColor ( 255, 255, 255, 255 ),	// vertexColor,
					5,										// maxLOD
					irr::scene::ETPS_17,					// patchSize
					4										// smoothFactor
					);

				pTerrain->setMaterialFlag(irr::video::EMF_LIGHTING, false);
				pTerrain->setMaterialFlag(irr::video::EMF_WIREFRAME, true);					
				pTerrain->setName("usr/scene/Terrain");
			}		

			//1.5버전 이상에서는 Lod를 위한 DynamicMeshBuffer가 지원된다.
			{

				irr::scene::ITerrainSceneNode* pTerrain = 
					((irr::scene::ITerrainSceneNode*)pSmgr->getSceneNodeFromName("usr/scene/Terrain"));

				irr::u32 index =0;
				irr::scene::IMesh* pMesh = pTerrain->getMesh(); 
				irr::scene::IMeshBuffer* pMeshBuffer = pMesh->getMeshBuffer(0); 
				std::cout << pMesh->getMeshBufferCount() << std::endl;
				std::cout << pMeshBuffer->getIndexCount() << std::endl;

				irr::scene::CDynamicMeshBuffer dmb(irr::video::EVT_2TCOORDS,irr::video::EIT_32BIT);
				pTerrain->getMeshBufferForLOD(dmb);

				std::cout << dmb.getIndexCount() << std::endl;		

				irr::u32 i;
				for(i=0;i< pMeshBuffer->getVertexCount();i++)						
				{
					irr::video::S3DVertex2TCoords* pVertices = (irr::video::S3DVertex2TCoords*)pMeshBuffer->getVertices();						
					//irr::video::S3DVertex2TCoords* pVertices_rnd = (irr::video::S3DVertex2TCoords*)pTerrain->getRenderBuffer()->getVertices();
					pVertices[i].Pos.Y = 0; //높이를 0으로							
					//pVertices_rnd[i] = pVertices[i];							
				}
				pMeshBuffer->setDirty(irr::scene::EBT_VERTEX);
				pTerrain->setPosition(pTerrain->getPosition());

				{
					irr::video::S3DVertex2TCoords* pVertices = (irr::video::S3DVertex2TCoords*)pMeshBuffer->getVertices();												

					irr::core::vector3df pos = pVertices[0].Pos;
					std::cout << pos.X << "," << pos.Y << "," << pos.Z << std::endl;	
					pos = pVertices[256].Pos;
					std::cout << pos.X << "," << pos.Y << "," << pos.Z << std::endl;	
					pos = pVertices[256*2].Pos;
					std::cout << pos.X << "," << pos.Y << "," << pos.Z << std::endl;							
				}

				{
					irr::core::vector3df pos;

					pos = dmb.getVertexBuffer()[dmb.getIndexBuffer()[0]].Pos;
					std::cout << pos.X << "," << pos.Y << "," << pos.Z << std::endl;								

					pos = dmb.getVertexBuffer()[dmb.getIndexBuffer()[1]].Pos;
					std::cout << pos.X << "," << pos.Y << "," << pos.Z << std::endl;

					pos = dmb.getVertexBuffer()[dmb.getIndexBuffer()[2]].Pos;
					std::cout << pos.X << "," << pos.Y << "," << pos.Z << std::endl;	
				}
			}

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,200),true,true,0,100,true);

			int nSelectVertexidx = 0;
			irr::core::vector3df SelVertex_pos;				

			while(pDevice->run())
			{	
				static irr::u32 uLastTick = 0;					
				irr::u32 uTick = pDevice->getTimer()->getTime();
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;


				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d\
									 %.2f,%.2f,%.2f",										 
									 pVideo->getFPS(),pVideo->getPrimitiveCountDrawn(),
									 SelVertex_pos.X,
									 SelVertex_pos.Y,
									 SelVertex_pos.Z);
					pstextFPS->setText(wszbuf);
				}

				//카메라움직이기
				{
					irr::scene::ICameraSceneNode *pCam = pSmgr->getActiveCamera();

					irr::f32 fRotSpeed = 45.f;
					irr::f32 fSpeed = 5.0f;
					irr::core::vector3df vFront = (pCam->getTarget() - pCam->getAbsolutePosition());
					irr::core::vector3df vUp = pCam->getUpVector();

					vFront.normalize();
					vUp.normalize();
					irr::core::vector3df vSide = vFront.crossProduct(vUp); //vFront X vUp
					vSide.normalize();

					vUp = vSide.crossProduct(vFront);// vSide X vFront

					irr::core::vector3df vMoveFront = (vFront*fDelta * fSpeed);
					irr::core::vector3df vMoveUp = vUp*fDelta * fSpeed;
					irr::core::vector3df vMoveSide = vSide*fDelta * fSpeed;	
                    
                    /*

					if(!(::GetAsyncKeyState(VK_LCONTROL) & 0x8001))
					{
						//전후진
						if( (::GetAsyncKeyState(VK_UP) & 0x8001))
						{
							pCam->setPosition(
								pCam->getAbsolutePosition() + vMoveFront
								);

						}
						if( (::GetAsyncKeyState(VK_DOWN) & 0x8001))
						{
							pCam->setPosition(
								pCam->getAbsolutePosition() - vMoveFront
								);

						}

						//좌우게걸음질
						if( (::GetAsyncKeyState(VK_LEFT) & 0x8001) )
						{
							pCam->setPosition(
								pCam->getAbsolutePosition() + vMoveSide
								);

						}

						if( (::GetAsyncKeyState(VK_RIGHT) & 0x8001) )
						{
							pCam->setPosition(
								pCam->getPosition() - vMoveSide
								);

						}

						//상하
						if( (::GetAsyncKeyState(VK_PRIOR) & 0x8001) )
						{
							pCam->setPosition(
								pCam->getPosition() + vMoveUp
								);

						}

						if( (::GetAsyncKeyState(VK_NEXT) & 0x8001) )
						{
							pCam->setPosition(
								pCam->getAbsolutePosition() - vMoveUp
								);

						}
					}
					else
					{//회전
						if( (::GetAsyncKeyState(VK_UP) & 0x8001) )
						{
							pCam->setRotation(
								pCam->getRotation() - irr::core::vector3df(fDelta*fRotSpeed,0,0)
								);
							//pCam->get
						}
						if( (::GetAsyncKeyState(VK_DOWN) & 0x8001))
						{
							pCam->setRotation(
								pCam->getRotation() + irr::core::vector3df(fDelta*fRotSpeed,0,0)
								);

						}
						if( (::GetAsyncKeyState(VK_LEFT) & 0x8001) )
						{	
							pCam->setRotation(
								pCam->getRotation() - irr::core::vector3df(0,fDelta*fRotSpeed,0)
								);

						}
						if( (::GetAsyncKeyState(VK_RIGHT) & 0x8001))
						{		
							pCam->setRotation(
								pCam->getRotation() + irr::core::vector3df(0,fDelta*fRotSpeed,0)
								);							
						}
					}
                     */

					//전역변환 업데이트해주기
					pCam->updateAbsolutePosition();

					//업벡터와 시점벡터에 변환반영
					{
						irr::core::vector3df vTarget(0,0,1);
						irr::core::vector3df vUp(0,1,0);

						irr::core::matrix4 mat = pCam->getAbsoluteTransformation();

						mat.transformVect(vTarget);
						mat.transformVect(vUp);

						pCam->setTarget(vTarget);	
						pCam->setUpVector(vUp - pCam->getPosition());						
					}




				}

				//지형편집
				{
					irr::scene::ITerrainSceneNode* pTerrain = 
						((irr::scene::ITerrainSceneNode*)pSmgr->getSceneNodeFromName("usr/scene/Terrain"));

					irr::u32 index =0;
					irr::scene::IMesh* pMesh = pTerrain->getMesh(); 
					irr::scene::IMeshBuffer* pMeshBuffer = pMesh->getMeshBuffer(0); 
					irr::video::S3DVertex2TCoords* pVertices = (irr::video::S3DVertex2TCoords*)pMeshBuffer->getVertices();
                    
                    /*

					if((::GetAsyncKeyState('I') & 0x8001) == 0x8001 )
					{
						nSelectVertexidx++;

					}
					else if((::GetAsyncKeyState('M') & 0x8001) == 0x8001)
					{
						nSelectVertexidx--;

					}	
					else if((::GetAsyncKeyState('K') & 0x8001) == 0x8001)
					{
						nSelectVertexidx += 256;

					}
					else if((::GetAsyncKeyState('J') & 0x8001) == 0x8001)
					{
						nSelectVertexidx -= 256;							
					}


					if(nSelectVertexidx < 0)
						nSelectVertexidx = 0;

					SelVertex_pos = pVertices[nSelectVertexidx].Pos;

					if(::GetAsyncKeyState('Q') && 0x8000)
					{							
						pVertices[nSelectVertexidx].Pos.Y += (1.f * fDelta);							
					}
					if(::GetAsyncKeyState('A') && 0x8000)
					{							
						pVertices[nSelectVertexidx].Pos.Y -= (1.f * fDelta);							
					}	
                     
                     */



					pMeshBuffer->setDirty(irr::scene::EBT_VERTEX);
					pTerrain->setPosition(pTerrain->getPosition()); //정점을 바꾸고난뒤 이렇게 해줘야 랜더링하는데 반영되는듯하다.
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();	


				{
					{
						irr::video::SMaterial m;
						m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.
						m.ZBuffer = false;

						pVideo->setMaterial(m);
					}

					{//선택된 정점 위치에 박스출력
						irr::core::matrix4 matW;
						matW.setTranslation(SelVertex_pos);
						pVideo->setTransform(irr::video::ETS_WORLD,matW);
						pVideo->draw3DBox(irr::core::aabbox3df(
							irr::core::vector3df(-.1f,-.1f,-.1f),
							irr::core::vector3df(.1f,.1f,.1f)),
							irr::video::SColor(255,255,0,0));
					}
				}
				pVideo->endScene();	
			}
			pDevice->drop();
		}
	}	


	
	//터래인 상에서 걸어다니기 예제
	namespace _02
	{
		class Lsn12_App :  public irr::IEventReceiver
		{
		public:

			irr::IrrlichtDevice *m_pDevice;

			irr::video::IVideoDriver *m_pVideo;
			irr::scene::ISceneManager *m_pSmgr;
			irr::gui::IGUIEnvironment *m_pGuiEnv;

			

			Lsn12_App()
			{
				m_pDevice = irr::createDevice(					
					irr::video::EDT_OPENGL,
					irr::core::dimension2du(640,480), 32,
					false, false, true,
					this
					);
				m_pVideo = m_pDevice->getVideoDriver();
				m_pSmgr = m_pDevice->getSceneManager();
				m_pGuiEnv = m_pDevice->getGUIEnvironment();		


				m_pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

				// add camera
				{
					//irr::scene::ICameraSceneNode* pCam = 
						//m_pSmgr->addCameraSceneNodeFPS(0,100.0f,1200.f);

					irr::scene::ICameraSceneNode* pCam =
						m_pSmgr->addCameraSceneNodeFPS(0,100.0f,1.2f);


					pCam->setPosition(irr::core::vector3df(2700*2,255*2,2600*2));
					pCam->setTarget(irr::core::vector3df(2397*2,343*2,2700*2));
					pCam->setFarValue(42000.0f);
					/*
					pCam->setPosition(irr::core::vector3df(1900*2,255*2,3700*2));
					pCam->setTarget(irr::core::vector3df(2397*2,343*2,2700*2));
					pCam->setFarValue(12000.0f);
					*/
					pCam->setName("mainCam");
				}

				// add terrain scene node
				{
					irr::scene::ITerrainSceneNode* pTerrain = 
						m_pSmgr->addTerrainSceneNode( 
						"terrain-heightmap.bmp",
						0, // parent node
						-1,		// node id
						irr::core::vector3df(0.f, 0.f, 0.f),// position
						irr::core::vector3df(0.f, 0.f, 0.f),// rotation
						irr::core::vector3df(40.f, 4.4f, 40.f),	// scale
						irr::video::SColor ( 255, 255, 255, 255 ),// vertexColor,
						5,	// maxLOD
						irr::scene::ETPS_17,	// patchSize
						4	// smoothFactor
						);

					pTerrain->setMaterialFlag(irr::video::EMF_LIGHTING, false);
					pTerrain->setMaterialTexture(0, 
						m_pVideo->getTexture("terrain-texture.jpg"));
					pTerrain->setMaterialTexture(1, 
						m_pVideo->getTexture("detailmap3.jpg"));

					pTerrain->setMaterialType(irr::video::EMT_DETAIL_MAP);
					pTerrain->scaleTexture(1.0f, 20.0f);
					pTerrain->setName("Terrain");

				}

				{
					irr::scene::ISceneNode* pNode = 0;
					irr::scene::IAnimatedMesh* pMesh;
					pMesh = m_pSmgr->addHillPlaneMesh("myHill",
						irr::core::dimension2d<irr::f32>(200,200),
						irr::core::dimension2d<irr::u32>(40,40), 0,0,
						irr::core::dimension2d<irr::f32>(0,0),
						irr::core::dimension2d<irr::f32>(10,10));

					pNode = m_pSmgr->addWaterSurfaceSceneNode(pMesh->getMesh(0), 3.0f, 300.0f, 30.0f);
					pNode->setPosition(irr::core::vector3df(5000,150,5000));
					pNode->setMaterialTexture(0, m_pVideo->getTexture("water.jpg"));
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING, false);
					pNode->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);
				}

				// create skybox
				{

					m_pSmgr->addSkyBoxSceneNode(
						m_pVideo->getTexture("irrlicht2_up.jpg"),
						m_pVideo->getTexture("irrlicht2_dn.jpg"),
						m_pVideo->getTexture("irrlicht2_lf.jpg"),
						m_pVideo->getTexture("irrlicht2_rt.jpg"),
						m_pVideo->getTexture("irrlicht2_ft.jpg"),
						m_pVideo->getTexture("irrlicht2_bk.jpg"));
				}

				{
					// create triangle selector for the terrain	

					irr::scene::ICameraSceneNode* pCam = 
						(irr::scene::ICameraSceneNode*)m_pSmgr->getSceneNodeFromName("mainCam");
					irr::scene::ITerrainSceneNode* pTerrain = 		
						(irr::scene::ITerrainSceneNode* )m_pSmgr->getSceneNodeFromName("Terrain");

					irr::scene::ITriangleSelector* pSelector = 
						m_pSmgr->createTerrainTriangleSelector(pTerrain, 0);
					pTerrain->setTriangleSelector(pSelector);

					// create collision response animator and attach it to the camera
					irr::scene::ISceneNodeAnimator* pAnim = 
						m_pSmgr->createCollisionResponseAnimator(
						pSelector, pCam, 
						irr::core::vector3df(60,100,60),
						irr::core::vector3df(0,-9.8f,0), 
						irr::core::vector3df(0,50,0));
					pSelector->drop();
					pCam->addAnimator(pAnim);
					pAnim->drop();
				}

				//{
				//	scene::ITriangleSelector* selector
				//		= smgr->createTerrainTriangleSelector(terrain, 0);
				//	terrain->setTriangleSelector(selector);

				//	// create collision response animator and attach it to the camera
				//	scene::ISceneNodeAnimator* anim = smgr->createCollisionResponseAnimator(
				//		selector, camera, core::vector3df(60,100,60),
				//		core::vector3df(0,0,0),
				//		core::vector3df(0,50,0));
				//	selector->drop();
				//	camera->addAnimator(anim);
				//	anim->drop();
				//}


				m_pVideo->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS, true);
			}
			~Lsn12_App()
			{
				m_pDevice->drop();
			}

			//이벤트핸들러
			virtual bool OnEvent(const irr::SEvent& event)
			{
				switch(event.EventType)
				{
				case irr::EET_GUI_EVENT:
					{

					}
					break;
				case irr::EET_KEY_INPUT_EVENT:
					{
					}
					break;
				case irr::EET_MOUSE_INPUT_EVENT:
					{

					}
					break;
				case irr::EET_USER_EVENT:
					break;
				}
				return false;
			}	

		};


		void main()
		{
			Lsn12_App App;	

			while(App.m_pDevice->run())
			{		
				App.m_pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				App.m_pSmgr->drawAll();
				App.m_pGuiEnv->drawAll();

				App.m_pVideo->endScene();
			}
		}

	}

}

//class Lsn12_App :  public irr::IEventReceiver
//{
//public:
//
//	irr::IrrlichtDevice *m_pDevice;
//
//	irr::video::IVideoDriver *m_pVideo;
//	irr::scene::ISceneManager *m_pSmgr;
//	irr::gui::IGUIEnvironment *m_pGuiEnv;
//
//	Lsn12_App()
//	{
//		m_pDevice = irr::createDevice(					
//			irr::video::EDT_OPENGL,
//#if(IRR_VERSION == 16)
//		irr::core::dimension2d<irr::u32>(640, 480), 
//#else
//		irr::core::dimension2d<irr::s32>(640, 480), 
//#endif
//			32,
//			false, false, true,
//			this
//			);
//		m_pVideo = m_pDevice->getVideoDriver();
//		m_pSmgr = m_pDevice->getSceneManager();
//		m_pGuiEnv = m_pDevice->getGUIEnvironment();		
//		m_pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res/");
//
//		
//		{
//			irr::scene::ICameraSceneNode* pCam =
//				m_pSmgr->addCameraSceneNodeFPS(0,100.0f,1.2f);
//
//			pCam->setPosition(irr::core::vector3df(2700*2,255*2,2600*2));
//			pCam->setTarget(irr::core::vector3df(2397*2,343*2,2700*2));
//			pCam->setFarValue(42000.0f);
//
//			pCam->setName("mainCam");
//		}
//
//		// disable mouse cursor
//		m_pDevice->getCursorControl()->setVisible(false);		
//		
//		{
//			irr::scene::ITerrainSceneNode* pTerrain = m_pSmgr->addTerrainSceneNode( 
//				"terrain-heightmap.bmp",
//				0,										// parent node
//				-1,										// node id
//				irr::core::vector3df(0.f, 0.f, 0.f),			// position
//				irr::core::vector3df(0.f, 0.f, 0.f),			// rotation
//				irr::core::vector3df(40.f, 4.4f, 40.f),		// scale
//				irr::video::SColor ( 255, 255, 255, 255 ),	// vertexColor,
//				5,										// maxLOD
//				irr::scene::ETPS_17,							// patchSize
//				4										// smoothFactor
//				);
//
//			pTerrain->setMaterialFlag(irr::video::EMF_LIGHTING, false);
//			
//
//			pTerrain->setMaterialTexture(0, m_pVideo->getTexture("terrain-texture.jpg"));
//			pTerrain->setMaterialTexture(1, m_pVideo->getTexture("detailmap3.jpg"));
//
//			pTerrain->setMaterialType(irr::video::EMT_DETAIL_MAP);
//
//			pTerrain->scaleTexture(1.0f, 20.0f);
//			pTerrain->setName("Terrain");
//
//			
//		}
//
//		{
//			irr::scene::ISceneNode* pNode = 0;
//			irr::scene::IAnimatedMesh* pMesh;
//			pMesh = m_pSmgr->addHillPlaneMesh("myHill",
//				irr::core::dimension2d<irr::f32>(200,200),
//				irr::core::dimension2d<irr::u32>(40,40), 0,0,
//				irr::core::dimension2d<irr::f32>(0,0),
//				irr::core::dimension2d<irr::f32>(10,10));
//
//			pNode = m_pSmgr->addWaterSurfaceSceneNode(pMesh->getMesh(0), 6.0f, 300.0f, 30.0f);
//			pNode->setPosition(irr::core::vector3df(5000,150,5000));
//
//			//pNode->setMaterialTexture(0, m_pVideo->getTexture("../res/stones.jpg"));
//			pNode->setMaterialTexture(0, m_pVideo->getTexture("water.jpg"));
//
//			//pNode->setMaterialType(irr::video::EMT_REFLECTION_2_LAYER);
//			pNode->setMaterialFlag(irr::video::EMF_LIGHTING, false);
//		}
//
//		// create skybox
//		{
//			//m_pVideo->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS, false);
//
//			m_pSmgr->addSkyBoxSceneNode(
//				m_pVideo->getTexture("irrlicht2_up.jpg"),
//				m_pVideo->getTexture("irrlicht2_dn.jpg"),
//				m_pVideo->getTexture("irrlicht2_lf.jpg"),
//				m_pVideo->getTexture("irrlicht2_rt.jpg"),
//				m_pVideo->getTexture("irrlicht2_ft.jpg"),
//				m_pVideo->getTexture("irrlicht2_bk.jpg"));
//		}
//
//		{
//			// create triangle selector for the terrain	
//
//			irr::scene::ICameraSceneNode* pCam = (irr::scene::ICameraSceneNode*)m_pSmgr->getSceneNodeFromName("mainCam");
//			irr::scene::ITerrainSceneNode* pTerrain = (irr::scene::ITerrainSceneNode* )m_pSmgr->getSceneNodeFromName("Terrain");
//
//			irr::scene::ITriangleSelector* pSelector = m_pSmgr->createTerrainTriangleSelector(pTerrain, 0);
//			pTerrain->setTriangleSelector(pSelector);
//
//			// create collision response animator and attach it to the camera
//			
//			irr::scene::ISceneNodeAnimatorCollisionResponse* pAnim = m_pSmgr->createCollisionResponseAnimator(
//				pSelector, pCam, irr::core::vector3df(60,100,60),
//				irr::core::vector3df(0,0,0), 
//				irr::core::vector3df(0,50,0));			
//			
//			pSelector->drop();
//			pCam->addAnimator(pAnim);
//			pAnim->drop();
//			
//		}
//
//		/*{			
//			irr::scene::ITerrainSceneNode* pTerrain = 
//				(irr::scene::ITerrainSceneNode* )m_pSmgr->getSceneNodeFromName(
//				"Terrain");
//			irr::scene::ITriangleSelector* pSelector = pTerrain->getTriangleSelector();
//
//			irr::scene::ISceneNode *pNode = m_pSmgr->addCubeSceneNode();
//			pNode->setName("cube1");
//			pNode->setPosition(
//				irr::core::vector3df(1900*2,1000,3700*2-100)
//			);
//
//			irr::scene::ISceneNodeAnimator* pAnim = 
//				m_pSmgr->createCollisionResponseAnimator(
//					pSelector, pNode, irr::core::vector3df(60,100,60),
//					irr::core::vector3df(0,-10,0), 
//					irr::core::vector3df(0,50,0)
//					);
//
//			pNode->setMaterialTexture(0, m_pVideo->getTexture("../res/wall.jpg"));
//			pNode->setMaterialFlag(irr::video::EMF_LIGHTING, false);
//			pNode->addAnimator(pAnim);
//			pAnim->drop();
//		}*/
//
//		m_pVideo->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS, true);
//	}
//
//	~Lsn12_App()
//	{
//		m_pDevice->drop();
//	}	
//	
//	//이벤트 핸들러
//	virtual bool OnEvent(const irr::SEvent& event)
//	{
//		switch(event.EventType)
//		{
//			case irr::EET_GUI_EVENT:
//				{
//					
//				}
//				break;
//			case irr::EET_KEY_INPUT_EVENT:
//				{
//				}
//				break;
//			case irr::EET_MOUSE_INPUT_EVENT:
//				{
//
//				}
//				break;
//			case irr::EET_USER_EVENT:
//				break;
//		}
//		return false;
//	}	
//
//};
//
//
//void Lession12()
//{
//	Lsn12_App App;	
//
//	while(App.m_pDevice->run())
//	{		
//		App.m_pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));
//		
//		App.m_pSmgr->drawAll();
//		App.m_pGuiEnv->drawAll();
//
//		App.m_pVideo->endScene();
//	}
//}

//////////////////////////////////

//스카이돔
void Lession12_01()
{
	irr::IrrlichtDevice *pDevice = irr::createDevice(					
		irr::video::EDT_OPENGL,
#if(IRRLICHT_VERSION_MAJOR == 1 && IRRLICHT_VERSION_MINOR >= 6 )
		irr::core::dimension2d<irr::u32>(640, 480), 
#else
		irr::core::dimension2d<irr::s32>(640, 480), 
#endif
		32,
		false, false, false,
		NULL
		);

	pDevice->setWindowCaption(L"스카이돔 예제");

	irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
	irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
	irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

	pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

	pSmgr->addCameraSceneNodeFPS();

	// create skybox
	/*{
	pVideo->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS, false);

	pSmgr->addSkyBoxSceneNode(
	pVideo->getTexture("irrlicht2_up.jpg"),
	pVideo->getTexture("irrlicht2_dn.jpg"),
	pVideo->getTexture("irrlicht2_lf.jpg"),
	pVideo->getTexture("irrlicht2_rt.jpg"),
	pVideo->getTexture("irrlicht2_ft.jpg"),
	pVideo->getTexture("irrlicht2_bk.jpg"));
	}*/

	{
		pSmgr->addSkyDomeSceneNode(pVideo->getTexture("jga/jga_poster.jpg"));
	}

	//프레임 레이트 표시용 유아이
	irr::gui::IGUIStaticText *pstextFPS = 
		pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

	irr::u32 uLastTick = pDevice->getTimer()->getTime();
	while(pDevice->run())
	{	
		irr::u32 uTick = pDevice->getTimer()->getTime();
		irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
		uLastTick = pDevice->getTimer()->getTime();

		//프레임레이트 갱신
		{
			wchar_t wszbuf[256];
			swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());			
			pstextFPS->setText(wszbuf);
		}

		pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

		pSmgr->drawAll();
		pGuiEnv->drawAll();		

		pVideo->endScene();	
	}

	pDevice->drop();
}



