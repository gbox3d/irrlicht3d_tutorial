﻿
#include <irrlicht.h>
#include <iostream>
//#include <windows.h>
//#include <d3dx9.h>
//#include <assert.h>
//using namespace irr;

/////////////////////////////////////////////////
//
//광원,그림자,안개
//
/////////////////////////////////////////////////






class Lsn13_App : public irr::IEventReceiver


{
public:

	irr::IrrlichtDevice *m_pDevice;

	irr::video::IVideoDriver *m_pVideo;
	irr::scene::ISceneManager *m_pSmgr;
	irr::gui::IGUIEnvironment *m_pGuiEnv;

	Lsn13_App()
	{
		m_pDevice = irr::createDevice(					
			irr::video::EDT_OPENGL,
                                      irr::core::position2d<irr::u32>(640,480),
                                      
			32,
			false, true, true,
			this
			);
		m_pVideo = m_pDevice->getVideoDriver();
		m_pSmgr = m_pDevice->getSceneManager();
		m_pGuiEnv = m_pDevice->getGUIEnvironment();		

		irr::scene::ICameraSceneNode* pCamera = m_pSmgr->addCameraSceneNodeFPS();
		pCamera->setPosition(irr::core::vector3df(-50,50,-150));

		irr::video::IVideoDriver* driver = m_pVideo;
		irr::scene::ISceneManager* smgr = m_pSmgr;
		irr::IrrlichtDevice *device = m_pDevice;

		/*
		For our environment, we load a .3ds file. It is a small room I modelled
		with Anim8or and exported it into the 3ds format because the Irrlicht Engine
		did not support the .an8 format when I wrote this tutorial. I am a very bad
		3d graphic artist, and so the texture mapping is not very nice in this model.
		Luckily I am a better programmer than artist, and so the Irrlicht Engine
		is able to create a cool texture mapping for me: Just use the mesh manipulator
		and create a planar texture mapping for the mesh. If you want to see the mapping
		I made with Anim8or, uncomment this line. I also did not figure out how to
		set the material right in Anim8or, it has a specular light color which I don't really
		like. I'll switch it off too with this code.
		*/

		{
			irr::scene::IAnimatedMesh* mesh = m_pSmgr->getMesh(
				"../res/room.3ds");

			m_pSmgr->getMeshManipulator()->makePlanarTextureMapping(
				mesh->getMesh(0), 0.004f);

			irr::scene::ISceneNode* pNode = 0;		

			pNode = m_pSmgr->addAnimatedMeshSceneNode(mesh);
			pNode->setMaterialTexture(0,m_pVideo->getTexture("../res/wall.jpg"));
			pNode->getMaterial(0).SpecularColor.set(0,0,0,0);
		}

		//라이트추가하기
		{			
			
			irr::scene::ILightSceneNode *pLightNode;
			pLightNode = m_pSmgr->addLightSceneNode(0, irr::core::vector3df(0,0,0),
				irr::video::SColorf(1.0f, 0.6f, 0.7f, 1.0f), 1200.0f);

			//pLightNode->getLightData().Type = irr::video::ELT_DIRECTIONAL;

			irr::scene::ISceneNodeAnimator* anim = 0;
			anim = m_pSmgr->createFlyCircleAnimator (irr::core::vector3df(0,150,0),250.0f);
			pLightNode->addAnimator(anim);
			anim->drop();

			// attach billboard to light
			irr::scene::IBillboardSceneNode *pNode;
			pNode = m_pSmgr->addBillboardSceneNode(pLightNode, irr::core::dimension2d<irr::f32>(50, 50));
			pNode->setMaterialFlag(irr::video::EMF_LIGHTING, false);
			pNode->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);
			pNode->setMaterialTexture(0, m_pVideo->getTexture("../res/particlewhite.bmp"));			
			
		}

		
		{
			irr::scene::IAnimatedMesh* mesh;
			mesh = m_pSmgr->getMesh("../res/dwarf.x");
			irr::scene::IAnimatedMeshSceneNode* anode = 0;

			anode = m_pSmgr->addAnimatedMeshSceneNode(mesh);
			anode->setPosition(irr::core::vector3df(-50,20,-60));
			anode->setAnimationSpeed(15);

			// add shadow
			anode->addShadowVolumeSceneNode();
			m_pSmgr->setShadowColor(irr::video::SColor(150,0,0,0));

			// make the model a little bit bigger and normalize its normals
			// because of this for correct lighting
			anode->setScale(irr::core::vector3df(2,2,2));
			anode->setMaterialFlag(irr::video::EMF_NORMALIZE_NORMALS, true);

		}

		{
			irr::scene::ISceneNode* pNode = 0;
			irr::scene::IAnimatedMesh* pMesh;
			pMesh = smgr->addHillPlaneMesh("myHill",
				irr::core::dimension2d<irr::f32>(20,20),
				irr::core::dimension2d<irr::u32>(40,40), 0,0,
				irr::core::dimension2d<irr::f32>(0,0),
				irr::core::dimension2d<irr::f32>(10,10));

			pNode = smgr->addWaterSurfaceSceneNode(pMesh->getMesh(0), 3.0f, 300.0f, 30.0f);
			pNode->setPosition(irr::core::vector3df(0,7,0));

			pNode->setMaterialTexture(0, m_pVideo->getTexture("../res/stones.jpg"));
			pNode->setMaterialTexture(1, m_pVideo->getTexture("../res/water.jpg"));
		
			pNode->setMaterialType(irr::video::EMT_REFLECTION_2_LAYER);						
		}

		
		{
			irr::scene::ISceneNode* pNode = 0;
			irr::scene::IAnimatedMesh* pMesh;			

			pMesh = m_pSmgr->addHillPlaneMesh("myHill2",
				irr::core::dimension2d<irr::f32>(20,20),
				irr::core::dimension2d<irr::u32>(40,40), 0,100,
				irr::core::dimension2d<irr::f32>(0,0),
				irr::core::dimension2d<irr::f32>(10,10));

			pNode = m_pSmgr->addAnimatedMeshSceneNode(pMesh);
			pNode->setPosition(irr::core::vector3df(0,30,0));
			pNode->setMaterialTexture(0, m_pVideo->getTexture("../res/wall.jpg"));
		}
		

	}

	~Lsn13_App()
	{
		m_pDevice->drop();
	}	

	//이벤트 핸들러
	virtual bool OnEvent(const irr::SEvent& event)
	{
		switch(event.EventType)
		{
		case irr::EET_GUI_EVENT:
			{

			}
			break;
		case irr::EET_KEY_INPUT_EVENT:
			{
			}
			break;
		case irr::EET_MOUSE_INPUT_EVENT:
			{

			}
			break;
		case irr::EET_USER_EVENT:
			break;
		}
		return false;
	}	

};


void Lession13()
{
	Lsn13_App App;
	
	while(App.m_pDevice->run())
	{		
		App.m_pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));
		App.m_pSmgr->drawAll();	
		App.m_pVideo->endScene();
	}
}


#define RES_DIR "../../res"

namespace lsn13 {

	//기본 점광원예제
	namespace _00
	{		
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL,
				irr::core::dimension2du(640,480),32,
				false,
				true
				);
			

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
			
			pDevice->getFileSystem()->changeWorkingDirectoryTo(RES_DIR);			

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,9,-15), irr::core::vector3df(0,5,0));

			{
				irr::scene::ISceneNode *pNode = pSmgr->addSphereSceneNode(5,128);
				pNode->setPosition(irr::core::vector3df(0,3,0));
				//pNode->getMaterial(0).AmbientColor
			}

			irr::video::SColorf colWhite(1,1,1,1);
			
			//바닥추가
			{
				irr::scene::IAnimatedMeshSceneNode* pNode = 0;
				irr::scene::IAnimatedMesh* pMesh;			

				pMesh = pSmgr->addHillPlaneMesh("usr/mesh/hill",
					irr::core::dimension2d<irr::f32>(.8f,.8f),
					irr::core::dimension2d<irr::u32>(128,128), 0,0,
					irr::core::dimension2d<irr::f32>(2,2),
					irr::core::dimension2d<irr::f32>(8,8));

				pNode = pSmgr->addAnimatedMeshSceneNode(pMesh);			
				pNode->setMaterialTexture(0, pVideo->getTexture("wall.jpg"));			
				pNode->setName("usr/scene/hill");

				
			}			

			//라이트추가하기
			{
				irr::scene::ILightSceneNode *pLightNode;
				pLightNode = pSmgr->addLightSceneNode(0,
					irr::core::vector3df(0,0,0),
					irr::video::SColorf(1.0f, 1,1,1),
					20.0f);		
				
				pLightNode->getLightData().DiffuseColor = colWhite.getInterpolated(irr::video::SColorf(0,0,0,0),.06f);
				pLightNode->getLightData().AmbientColor = colWhite.getInterpolated(irr::video::SColorf(0,0,0,0),.12f);
				//pLightNode->getLightData().CastShadows

				irr::scene::ISceneNodeAnimator* anim = 0;
				anim = pSmgr->createFlyCircleAnimator(irr::core::vector3df(0,10,0),5.0f);
				pLightNode->addAnimator(anim);
				anim->drop();

				// attach billboard to light
				irr::scene::IBillboardSceneNode *pNode;
				pNode = pSmgr->addBillboardSceneNode(pLightNode, irr::core::dimension2d<irr::f32>(2,2));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING, false);
				pNode->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);
				pNode->setMaterialTexture(0, pVideo->getTexture("particlewhite.bmp"));			

			}			



			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);


			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}
	
	//점광원의 각종속성 조절해보기 예제
	namespace _01
	{		
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL,
				irr::core::dimension2du(800,600),32,
				false,
				true
				);
			

			pDevice->setWindowCaption(L"기본 점광원예제");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
			
			pDevice->getFileSystem()->changeWorkingDirectoryTo(RES_DIR);

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,9,-15), irr::core::vector3df(0,5,0));

			//테스트용 모델 로딩
			{
				irr::scene::ISceneNode *pNode = pSmgr->addSphereSceneNode(5,256);
				pNode->setPosition(irr::core::vector3df(0,3,0));
				
				//irr::scene::IAnimatedMeshSceneNode *pNode;
				//pNode = pSmgr->addAnimatedMeshSceneNode(pSmgr->getMesh("jga/skullocc.x"));
				pNode->setName("usr/scene/sphere/1");				
				pNode->setMaterialFlag(irr::video::EMF_NORMALIZE_NORMALS,true);

			}

			irr::video::SColorf colWhite(1,1,1,1);
			irr::video::SColor colDWhite(255,255,255,255);
			
			////바닥추가
			//{
			//	irr::scene::IAnimatedMeshSceneNode* pNode = 0;
			//	irr::scene::IAnimatedMesh* pMesh;			

			//	pMesh = pSmgr->addHillPlaneMesh("usr/mesh/hill",
			//		irr::core::dimension2d<irr::f32>(.2f,.2f),
			//		irr::core::dimension2d<irr::u32>(256,128), 0,0,
			//		irr::core::dimension2d<irr::f32>(2,2),
			//		irr::core::dimension2d<irr::f32>(8,8));
			//	

			//	pNode = pSmgr->addAnimatedMeshSceneNode(pMesh);			
			//	pNode->setMaterialTexture(0, pVideo->getTexture("wall.jpg"));			
			//	pNode->setName("usr/scene/hill");				
			//}			

			//라이트추가하기
			{
				irr::scene::ILightSceneNode *pLightNode;
				pLightNode = pSmgr->addLightSceneNode(
					0,
					irr::core::vector3df(0,0,0),
					irr::video::SColorf(1,1,1),
					20.0f);		
				pLightNode->setName("usr/scene/light/1");			
				

				irr::scene::ISceneNodeAnimator* anim = 0;
				anim = pSmgr->createFlyCircleAnimator(irr::core::vector3df(0,10,0),5.0f);
				pLightNode->addAnimator(anim);
				anim->drop();

				// attach billboard to light
				irr::scene::IBillboardSceneNode *pNode;
				pNode = pSmgr->addBillboardSceneNode(pLightNode, irr::core::dimension2d<irr::f32>(2,2));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING, false);
				pNode->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);
				pNode->setMaterialTexture(0, pVideo->getTexture("particlewhite.bmp"));
			}

			//gui
			{
				irr::gui::IGUISkin *pSkin = pGuiEnv->getSkin();
				//irr::gui::IGUIFont *pFont = 				
				pSkin->setFont(pGuiEnv->getFont("lucida.xml"));
			}



			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,200,320),true,true,0,100,true);


			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				
				//프레임레이트 갱신, 삼각형수 표시
				{					
					irr::video::SMaterial &mtl = pSmgr->getSceneNodeFromName("usr/scene/sphere/1")->getMaterial(0);
					irr::video::SLight &light = ((irr::scene::ILightSceneNode *)pSmgr->getSceneNodeFromName("usr/scene/light/1"))->getLightData();

					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d \
					    \nMamb %d,%d,%d \
						\nMdif %d,%d,%d \
						\nLAmb %.2f,%.2f,%.2f \
						\nLDif %.2f,%.2f,%.2f \
						\nLAtt %.2f,%.2f,%.2f \
						\nLShiness %.2f \
						\nLSpec %df,%d,%d \
						",
						pVideo->getFPS(),pVideo->getPrimitiveCountDrawn(),
						mtl.AmbientColor.getRed(),
						mtl.AmbientColor.getGreen(),
						mtl.AmbientColor.getBlue(),
						mtl.DiffuseColor.getRed(),
						mtl.DiffuseColor.getGreen(),
						mtl.DiffuseColor.getBlue(),
						light.AmbientColor.getRed(),
						light.AmbientColor.getGreen(),
						light.AmbientColor.getBlue(),
						light.DiffuseColor.getRed(),
						light.DiffuseColor.getGreen(),
						light.DiffuseColor.getBlue(),
						light.Attenuation.X,
						light.Attenuation.Y,
						light.Attenuation.Z,
						mtl.Shininess,
						mtl.SpecularColor.getRed(),
						mtl.SpecularColor.getGreen(),
						mtl.SpecularColor.getBlue()
						);
					pstextFPS->setText(wszbuf);
				}

				//라이트
				{
					static irr::f32 AmbientColor = .0f;
					static irr::f32 DiffuseColor = .0f;
					
					irr::scene::ILightSceneNode *pLightNode;
					pLightNode = (irr::scene::ILightSceneNode *)pSmgr->getSceneNodeFromName("usr/scene/light/1");

					irr::video::SLight &light = pLightNode->getLightData();

                    
                    /*
					if(GetAsyncKeyState('Q') && 0x8000)
					{						
						
						AmbientColor += (.2f * fDelta);
						//pNode->setPosition(pNode->getPosition() - (irr::core::vector3df(5,0,0) * fDelta));
					}
					if(GetAsyncKeyState('W') && 0x8000)
					{
						AmbientColor -= (.2f * fDelta);
						//pNode->setPosition(pNode->getPosition() + (irr::core::vector3df(5,0,0) * fDelta));
					}
					

					if(GetAsyncKeyState('A') && 0x8000)
					{						
						DiffuseColor += (.2f * fDelta);
						//pNode->setPosition(pNode->getPosition() - (irr::core::vector3df(5,0,0) * fDelta));
					}
					if(GetAsyncKeyState('S') && 0x8000)
					{
						DiffuseColor -= (.2f * fDelta);
						//pNode->setPosition(pNode->getPosition() + (irr::core::vector3df(5,0,0) * fDelta));
					}

					if(AmbientColor < 0) AmbientColor = 0;
					if(AmbientColor > 1) AmbientColor = 1;
					if(DiffuseColor < 0) DiffuseColor = 0;
					if(DiffuseColor > 1) DiffuseColor = 1;

					light.AmbientColor = colWhite.getInterpolated(irr::video::SColorf(0,0,0,0),AmbientColor);
					light.DiffuseColor = colWhite.getInterpolated(irr::video::SColorf(0,0,0,0),DiffuseColor);					


					//감쉐율
					if(GetAsyncKeyState('T') && 0x8000)
					{						
						light.Attenuation.X += (.2f * fDelta);						
					}
					if(GetAsyncKeyState('Y') && 0x8000)
					{
						light.Attenuation.X -= (.2f * fDelta);						
					}

					if(light.Attenuation.X  < 0) light.Attenuation.X = 0;
					if(light.Attenuation.X  > 1) light.Attenuation.X = 1;

					if(GetAsyncKeyState('G') && 0x8000)
					{						
						light.Attenuation.Y += (.2f * fDelta);						
					}
					if(GetAsyncKeyState('H') && 0x8000)
					{
						light.Attenuation.Y -= (.2f * fDelta);						
					}

					if(light.Attenuation.Y  < 0) light.Attenuation.Y = 0;
					if(light.Attenuation.Y  > 1) light.Attenuation.Y = 1;

					if(GetAsyncKeyState('B') && 0x8000)
					{						
						light.Attenuation.Z += (.2f * fDelta);						
					}
					if(GetAsyncKeyState('N') && 0x8000)
					{
						light.Attenuation.Z -= (.2f * fDelta);						
					}
                     */

					if(light.Attenuation.Z  < 0) light.Attenuation.Z = 0;
					if(light.Attenuation.Z  > 1) light.Attenuation.Z = 1;
				}

				//재질
				{
					static irr::f32 AmbientColor = 0.f;
					static irr::f32 DiffuseColor = 0.f;
					static irr::f32 Specular = 0.f;

                    
                    /*
					if(GetAsyncKeyState('E') && 0x8000)
					{						
						AmbientColor += (.2f * fDelta);						
					}
					if(GetAsyncKeyState('R') && 0x8000)
					{
						AmbientColor -= (.2f * fDelta);						
					}

					if(GetAsyncKeyState('D') && 0x8000)
					{						
						DiffuseColor += (.2f * fDelta);						
					}
					if(GetAsyncKeyState('F') && 0x8000)
					{
						DiffuseColor -= (.2f * fDelta);						
					}
                     */

					irr::scene::ISceneNode *pNode;
					pNode = pSmgr->getSceneNodeFromName("usr/scene/sphere/1");
					irr::video::SMaterial &mtl = pNode->getMaterial(0);

					if(AmbientColor < 0) AmbientColor = 0;
					if(AmbientColor > 1) AmbientColor = 1;
					if(DiffuseColor < 0) DiffuseColor = 0;
					if(DiffuseColor > 1) DiffuseColor = 1;

					mtl.AmbientColor = colDWhite.getInterpolated(irr::video::SColor(0,0,0,0),AmbientColor);
					mtl.DiffuseColor = colDWhite.getInterpolated(irr::video::SColor(0,0,0,0),DiffuseColor);

                    /*
					if(GetAsyncKeyState('U') && 0x8000)
					{					
						mtl.Shininess += (8.f * fDelta);
					}
					if(GetAsyncKeyState('I') && 0x8000)
					{	
						mtl.Shininess -= (8.f * fDelta);
					}
					if(GetAsyncKeyState('J') && 0x8000)
					{	
						Specular += (.2f * fDelta);		
												
					}
					if(GetAsyncKeyState('K') && 0x8000)
					{	
						Specular -= (.2f * fDelta);								
					}
                     */

					mtl.SpecularColor = colDWhite.getInterpolated(irr::video::SColor(0,0,0,0),Specular);					
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}	
	
	
	//점 광원 의 그림자, 정반사광 예제
	namespace _02
	{		
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL,
				irr::core::dimension2du(640,480),32,
				false,
				true
				);
			

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
			
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,9,-15), irr::core::vector3df(0,5,0));

			//닌자 추가
			{
				irr::scene::IAnimatedMeshSceneNode *pNode =
					pSmgr->addAnimatedMeshSceneNode(pSmgr->getMesh("ninja.b3d"));
				pNode->setMaterialTexture(0,pVideo->getTexture("nskinbl.jpg"));				
				pNode->setAnimationSpeed(30.f);
				pNode->setFrameLoop(206,250);
				pNode->setRotation(irr::core::vector3df(0,180,0));
				pNode->setPosition(irr::core::vector3df(0,0,0));
				pNode->setName("usr/scene/ninja2");

				//정반사광 재질설정
				pNode->setMaterialFlag(irr::video::EMF_NORMALIZE_NORMALS,true);
				pNode->getMaterial(0).Shininess = 128;
				pNode->getMaterial(0).SpecularColor = irr::video::SColor(255,255,255,255);				
				pNode->getMaterial(0).AmbientColor = irr::video::SColor(255,128,128,128);
				pNode->getMaterial(0).GouraudShading = true;				

				// add shadow
				pNode->addShadowVolumeSceneNode();
				pSmgr->setShadowColor(irr::video::SColor(150,0,0,0));

			}

			//바닥추가
			{
				irr::scene::IAnimatedMeshSceneNode* pNode = 0;
				irr::scene::IAnimatedMesh* pMesh;			

				pMesh = pSmgr->addHillPlaneMesh("usr/mesh/hill",
					irr::core::dimension2d<irr::f32>(8,8),
					irr::core::dimension2d<irr::u32>(20,20), 0,0,
					irr::core::dimension2d<irr::f32>(2,2),
					irr::core::dimension2d<irr::f32>(8,8));

				pNode = pSmgr->addAnimatedMeshSceneNode(pMesh);			
				pNode->setMaterialTexture(0, pVideo->getTexture("wall.jpg"));			
				pNode->setName("usr/scene/hill");

				
			}

			//라이트추가하기
			{
				irr::scene::ILightSceneNode *pLightNode;
				pLightNode = pSmgr->addLightSceneNode();	
				pLightNode->getLightData().DiffuseColor.set(.25f, .25f, .25f);
				pLightNode->getLightData().AmbientColor.set(.01f, .01f, .01f);

				irr::scene::ISceneNodeAnimator* anim = 0;
				anim = pSmgr->createFlyCircleAnimator(irr::core::vector3df(0,10,0),5.0f);
				pLightNode->addAnimator(anim);
				anim->drop();

				// attach billboard to light
				irr::scene::IBillboardSceneNode *pNode;
				pNode = pSmgr->addBillboardSceneNode(pLightNode, irr::core::dimension2d<irr::f32>(2,2));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING, false);
				pNode->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);
				pNode->setMaterialTexture(0, pVideo->getTexture("particlewhite.bmp"));			

			}			



			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);


			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}

	
	//방향성광
	namespace _03
	{		
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL,
				irr::core::dimension2du(640,480),32,
				false,
				true
				);
			

			pDevice->setWindowCaption(L"Directional Light tutorial v1.0");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
			
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");
			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,9,-15), irr::core::vector3df(0,5,0));

			//닌자 추가
			{
				irr::scene::IAnimatedMeshSceneNode *pNode =
					pSmgr->addAnimatedMeshSceneNode(pSmgr->getMesh("ninja.b3d"));
				pNode->setMaterialTexture(0,pVideo->getTexture("nskinbl.jpg"));				
				pNode->setAnimationSpeed(30.f);
				pNode->setFrameLoop(206,250);
				pNode->setRotation(irr::core::vector3df(0,180,0));
				pNode->setPosition(irr::core::vector3df(0,0,0));
				pNode->setName("usr/scene/ninja/1");

				//정반사광 재질설정
				pNode->setMaterialFlag(irr::video::EMF_NORMALIZE_NORMALS,true);
				pNode->getMaterial(0).Shininess = 128;
				pNode->getMaterial(0).SpecularColor = irr::video::SColor(255,255,255,255);				
				pNode->getMaterial(0).AmbientColor = irr::video::SColor(255,64,64,64);
				pNode->getMaterial(0).GouraudShading = true;				

				// add shadow
				pNode->addShadowVolumeSceneNode();
				pSmgr->setShadowColor(irr::video::SColor(150,0,0,0));

			}

			//바닥추가
			{
				irr::scene::IAnimatedMeshSceneNode* pNode = 0;
				irr::scene::IAnimatedMesh* pMesh;			

				pMesh = pSmgr->addHillPlaneMesh("usr/mesh/hill",
					irr::core::dimension2d<irr::f32>(8,8),
					irr::core::dimension2d<irr::u32>(20,20), 0,0,
					irr::core::dimension2d<irr::f32>(2,2),
					irr::core::dimension2d<irr::f32>(8,8));

				pNode = pSmgr->addAnimatedMeshSceneNode(pMesh);			
				pNode->setMaterialTexture(0, pVideo->getTexture("wall.jpg"));			
				pNode->setName("usr/scene/hill");


			}

			{
				//방향성 라이트추가하기

				irr::scene::ILightSceneNode *pLightNode;
				/*pLightNode = pSmgr->addLightSceneNode(
					0, 
					irr::core::vector3df(0,0,0),
					irr::video::SColorf(1.0f, 1.0f, 1.f, 1.0f), 
					0.0f);		*/
				pLightNode = pSmgr->addLightSceneNode(
					0,
					irr::core::vector3df(-1,1,0) * 10000000, //광원의 초기위치값 그림자 계산시참고함(방향광일지라도...)
					irr::video::SColorf(1.0f, 1,1,1),
					1000000000.f //빛의 범위도 충분히 주기
					);			

				pLightNode->getLightData().Type = irr::video::ELT_DIRECTIONAL; 
				pLightNode->getLightData().DiffuseColor.set(1.f, 1.f, 1.f);				

				////방향을 바꿈
				//pLightNode->setRotation(
				//	irr::core::vector3df(-45.0f,0,0)
				//	);

				pLightNode->setName("usr/scene/Light/1");
				////회전 애니메이터
				//{
				//	irr::scene::ISceneNodeAnimator* anim = 0;
				//	anim = pSmgr->createRotationAnimator(irr::core::vector3df(0,1.f,0));
				//	pLightNode->addAnimator(anim);
				//	anim->drop();
				//}				
			}

			irr::scene::ISceneNode *pArrowNode;
			{
				pArrowNode = pSmgr->addEmptySceneNode();
				irr::scene::IAnimatedMesh *pAniMesh =  pSmgr->addArrowMesh("arrow",
					irr::video::SColor(0,255,0,0),
					irr::video::SColor(0,0,255,0)
					);			
				pArrowNode->setPosition(irr::core::vector3df(10,10,0));

				//라이트로드를 부모로 붙임
				irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(pAniMesh,pArrowNode);
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);		
				pNode->setPosition(irr::core::vector3df(0,0,0));
				pNode->setRotation(irr::core::vector3df(90.0f,0,0));
				
			}

			irr::core::quaternion qt_main(irr::core::vector3df(-45.0f,45,0) * irr::core::DEGTORAD );
			//irr::core::quaternion qt_Yaw()
			//qt_main.makeIdentity();
			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);



			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

                /*
				if(GetAsyncKeyState(VK_CONTROL) && 0x8000)
				{			

					if( (::GetAsyncKeyState(VK_LEFT)&0x8000 ))
					{
						qt_main *= irr::core::quaternion(0,irr::core::degToRad(45*fDelta),0);
					}
					if( (::GetAsyncKeyState(VK_RIGHT)&0x8000 ))
					{
						qt_main *= irr::core::quaternion(0,irr::core::degToRad(-45*fDelta),0);				
					}
					if( (::GetAsyncKeyState(VK_UP)&0x8000 ))
					{
						qt_main *= irr::core::quaternion(irr::core::degToRad(45*fDelta),0,0);				
					}
					if( (::GetAsyncKeyState(VK_DOWN)&0x8000 ))
					{
						qt_main *= irr::core::quaternion(irr::core::degToRad(-45*fDelta),0,0);				
					}
				}
				else
				{
					irr::scene::ISceneNode *pNode;
					pNode = pSmgr->getSceneNodeFromName("usr/scene/ninja/1");
					if(GetAsyncKeyState(VK_LEFT) && 0x8000)
					{
						pNode->setPosition(pNode->getPosition() + (irr::core::vector3df(5,0,0) * fDelta));
					}
					if(GetAsyncKeyState(VK_RIGHT) && 0x8000)
					{
						pNode->setPosition(pNode->getPosition() - (irr::core::vector3df(5,0,0) * fDelta));
					}

				}
                 */

				//방향성광 적용
				{

					//1. 방향성광의 방향지정해주기
					//방향성라이트는 회전하지않은 상태에서 기본적으로(0,0,1)방향만 가리키며
					//SLight의 Direction멥버 변수는 랜더링 루푸에서 항상 0,0,1로 재초기화 되므로
					//pLightNode->getLightData().Direction = vector3df(-1,0,0); 이런식으로
					//사용자가 임의방향을 넣어 주는것은 아무런 의미가 없다.
					//방향을 바꾸고싶으면 회전을 수행해야한다.

					//2. 방향성광에서의 그림자
					//방향성광인데도 불구하고 그림자 계산은 광원의 위치에 따라서 한다.				
					//초기화 할때 넣어준 위치값(SLight구조체의 Position값)을 참고해서 그림자를 계산해주기때문에					
					//가장 간단하게는 광원 위치를 아주 멀리 해주면 방향성광 처럼 사용할수있다.
					////setPosition() 함수로 중간에 바꾸는것은 안된다.
					//대신에 pLightNode->getLightData().Position 의 값을 바꿔주면 
					//이값을 기준 으로 그림자를 계산하기때문에 실시간으로 그림자를 그려주는 기준을 바꿀수있다.

					irr::scene::ILightSceneNode *pLightNode = (irr::scene::ILightSceneNode *)pSmgr->getSceneNodeFromName("usr/scene/Light/1");		

					irr::core::vector3df Angle;
					/*qt_main.normalize();
					qt_main.toEuler(Angle);*/

					irr::core::matrix4 matEler;
					matEler = qt_main.getMatrix();

					pLightNode->setRotation(matEler.getRotationDegrees());
					pArrowNode->setRotation(matEler.getRotationDegrees());
					
					irr::core::vector3df vLightDir(0,0,1);				

					irr::core::quaternion qt = qt_main;
					qt.makeInverse();
					vLightDir = qt * vLightDir;

					//그림자 계산시 기준으로 삼는 광원위치
					pLightNode->getLightData().Position =  -vLightDir * 1000000.f;
					
				}



				

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}

	//직접 그림자 만들기
	//그림자 행렬 이용
	namespace _04
	{		
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL,
				irr::core::dimension2du(640,480),32,
				false,
				true
				);
			

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
			
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");
			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,9,-15), irr::core::vector3df(0,5,0));

			//닌자 추가
			{
				irr::scene::IAnimatedMeshSceneNode *pNode =
					pSmgr->addAnimatedMeshSceneNode(pSmgr->getMesh("ninja.b3d"));
				pNode->setMaterialTexture(0,pVideo->getTexture("nskinbl.jpg"));				
				pNode->setAnimationSpeed(30.f);
				pNode->setFrameLoop(206,250);
				pNode->setRotation(irr::core::vector3df(0,180,0));
				pNode->setPosition(irr::core::vector3df(0,0,0));
				pNode->setName("usr/scene/ninja/1");

				//정반사광 재질설정
				pNode->setMaterialFlag(irr::video::EMF_NORMALIZE_NORMALS,true);
				pNode->getMaterial(0).Shininess = 64;
				pNode->getMaterial(0).SpecularColor = irr::video::SColor(255,255,255,255);				
				pNode->getMaterial(0).AmbientColor = irr::video::SColor(255,128,128,128);
				pNode->getMaterial(0).GouraudShading = true;

				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);

				// add shadow
				//pNode->addShadowVolumeSceneNode();
				//pSmgr->setShadowColor(irr::video::SColor(150,0,0,0));

			}

			//바닥추가
			{
				irr::scene::IAnimatedMeshSceneNode* pNode = 0;
				irr::scene::IAnimatedMesh* pMesh;			

				pMesh = pSmgr->addHillPlaneMesh("usr/mesh/hill",
					irr::core::dimension2d<irr::f32>(8,8),
					irr::core::dimension2d<irr::u32>(20,20), 0,0,
					irr::core::dimension2d<irr::f32>(2,2),
					irr::core::dimension2d<irr::f32>(8,8));

				pNode = pSmgr->addAnimatedMeshSceneNode(pMesh);			
				pNode->setMaterialTexture(0, pVideo->getTexture("wall.jpg"));			
				pNode->setName("usr/scene/hill");
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);

			}

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);


			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				{				
					irr::scene::ISceneNode *pNode;
					pNode = pSmgr->getSceneNodeFromName("usr/scene/ninja/1");
                    /*
					if(GetAsyncKeyState(VK_RIGHT) && 0x8000)
					{
						pNode->setPosition(pNode->getPosition() - (irr::core::vector3df(5,0,0) * fDelta));
					}
					if(GetAsyncKeyState(VK_LEFT) && 0x8000)
					{
						pNode->setPosition(pNode->getPosition() + (irr::core::vector3df(5,0,0) * fDelta));
					}
                     */
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				{//수동으로 그림자 만들어 출력
					const irr::core::plane3df plane(0, .1f, 0, 0, 1, 0); 
					irr::core::matrix4 mat;
					mat.buildShadowMatrix(irr::core::vector3df(1,1,0),plane,0);

					irr::scene::IAnimatedMeshSceneNode *pNode = (irr::scene::IAnimatedMeshSceneNode *)pSmgr->getSceneNodeFromName(
						"usr/scene/ninja/1"
						);
					irr::scene::IMesh* mesh = pNode->getMesh()->getMesh(irr::core::round32(pNode->getFrameNr()));


					mat *= pNode->getAbsoluteTransformation(); 
					pVideo->setTransform(irr::video::ETS_WORLD, mat); 

					{
						irr::video::SMaterial m = pNode->getMaterial(0);
						m.setTexture(0,pVideo->getTexture("nskinrd.jpg"));
						pVideo->setMaterial(m);
					}

					irr::u32 b; 
					for (b = 0; b < mesh->getMeshBufferCount(); ++b) 
						pVideo->drawMeshBuffer(mesh->getMeshBuffer(b)); 

				}

				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}

	//안개효과
	namespace _05
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL,
				irr::core::dimension2du(640,480),32,
				false,
				true
				);
			

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
			
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");			

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,9,-10), irr::core::vector3df(0,5,0));

			{
				int i;
				for(i=0;i<10; i++)
				{
					irr::scene::ISceneNode *pNode = pSmgr->addCubeSceneNode(4);
					pNode->setPosition(irr::core::vector3df(0,0,i*20.f));				
					pNode->setMaterialFlag(irr::video::EMF_FOG_ENABLE,true);
					pNode->setMaterialTexture(0,pVideo->getTexture("jga/박예진.jpg"));
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				}
			}

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,120),true,true,0,100,true);


			while(pDevice->run())
			{	
				
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				static irr::f32 fogStart = 5.f;
				static irr::f32 fogEnd = 20.f;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d\
									 \nfog start %f\
									 \nfog end %f",
						pVideo->getFPS(),pVideo->getPrimitiveCountDrawn(),
						fogStart,
						fogEnd
						);
					pstextFPS->setText(wszbuf);
				}


				//안개 범위 지정
				{			
                    /*
					if(GetAsyncKeyState('Q') && 0x8000)
					{
						fogStart += (10.f * fDelta);					
					}
					if(GetAsyncKeyState('W') && 0x8000)
					{
						fogStart -= (10.f * fDelta);					
					}
					if(GetAsyncKeyState('A') && 0x8000)
					{
						fogEnd += (10.f * fDelta);					
					}
					if(GetAsyncKeyState('S') && 0x8000)
					{
						fogEnd -= (10.f * fDelta);					
					}
                     */

					pVideo->setFog(irr::video::SColor(0,255,255,255),irr::video::EFT_FOG_LINEAR,fogStart,fogEnd);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}

	}

	//스폿 라이트
	namespace _06
	{		
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL,
				irr::core::dimension2du(640,480),32,
				false,
				true
				);
			

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
			
			pDevice->getFileSystem()->changeWorkingDirectoryTo(RES_DIR);			

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,20,-15), irr::core::vector3df(0,5,0));

			{
				irr::scene::ISceneNode *pNode = pSmgr->addSphereSceneNode(5,128);
				pNode->setPosition(irr::core::vector3df(0,3,0));
				//pNode->getMaterial(0).AmbientColor
			}

			irr::video::SColorf colWhite(1,1,1,1);
			
			//바닥추가
			{
				irr::scene::IAnimatedMeshSceneNode* pNode = 0;
				irr::scene::IAnimatedMesh* pMesh;			

				pMesh = pSmgr->addHillPlaneMesh("usr/mesh/hill",
					irr::core::dimension2d<irr::f32>(.8f,.8f),
					irr::core::dimension2d<irr::u32>(128,128), 0,0,
					irr::core::dimension2d<irr::f32>(2,2),
					irr::core::dimension2d<irr::f32>(8,8));

				pNode = pSmgr->addAnimatedMeshSceneNode(pMesh);			
				pNode->setMaterialTexture(0, pVideo->getTexture("wall.jpg"));			
				pNode->setName("usr/scene/hill");

				
			}			

			//라이트추가하기
			{
				irr::scene::ILightSceneNode *pLightNode;
				pLightNode = pSmgr->addLightSceneNode(0,
					irr::core::vector3df(0,0,0),
					irr::video::SColorf(1.0f, 1,1,1),
					20.0f);		
				
				pLightNode->getLightData().DiffuseColor = colWhite.getInterpolated(irr::video::SColorf(0,0,0,0),.06f);
				pLightNode->getLightData().AmbientColor = colWhite.getInterpolated(irr::video::SColorf(0,0,0,0),.12f);				
				


				//스폿라이트는 방향과 위치를 가진다.				
				pLightNode->setLightType(irr::video::ELT_SPOT);
				pLightNode->setName("usr/scene/Light/1");
				

				irr::scene::ISceneNodeAnimator* anim = 0;
				anim = pSmgr->createFlyCircleAnimator(irr::core::vector3df(0,10,0),5.0f);
				pLightNode->addAnimator(anim);
				anim->drop();

				// attach billboard to light
				irr::scene::IBillboardSceneNode *pNode;
				pNode = pSmgr->addBillboardSceneNode(pLightNode, irr::core::dimension2d<irr::f32>(2,2));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING, false);
				pNode->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);
				pNode->setMaterialTexture(0, pVideo->getTexture("particlewhite.bmp"));	

				//화살표 붙이기
				{
					irr::scene::IAnimatedMesh *pMesh = pSmgr->addArrowMesh("usr/mesh/arrow/1"
						,irr::video::SColor(255,255,0,0)
						,irr::video::SColor(255,0,0,255)
						);
					irr::core::matrix4 mat;
					mat.setRotationDegrees(irr::core::vector3df(90.f,0,0));
					pSmgr->getMeshManipulator()->transform(pMesh,mat);
					irr::scene::IAnimatedMeshSceneNode *pArrowNode = pSmgr->addAnimatedMeshSceneNode(pMesh);
					
					pArrowNode->setParent(pLightNode);
					pArrowNode->setMaterialFlag(irr::video::EMF_LIGHTING, false);
				}

				{
					//콘메쉬 만들기
					irr::scene::IMesh *pMesh = pSmgr->getGeometryCreator()->createConeMesh(2,3,8);
					
					//메쉬 회전
					irr::core::matrix4 mat;
					mat.setRotationDegrees(irr::core::vector3df(-90.f,0,0));
					pSmgr->getMeshManipulator()->transform(pMesh,mat);

					irr::scene::SAnimatedMesh *pAniMesh = new irr::scene::SAnimatedMesh(pMesh);						
					pMesh->drop();

					pAniMesh->recalculateBoundingBox();
					pSmgr->getMeshCache()->addMesh("usr/mesh/cone/1",pAniMesh); //씬메니져에 등록
					
					pAniMesh->drop();

					irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(
						pSmgr->getMesh("usr/mesh/cone/1"));
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);				
					pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);		
					pNode->setParent(pLightNode);
					//pNode->setPosition(irr::core::vector3df(0,0,0));

					/*irr::core::matrix4 mat;
					mat.setRotationDegrees(irr::core::vector3df(90.f,0,0));
					pSmgr->getMeshManipulator()->transform(pMesh,mat);
					irr::scene::IAnimatedMeshSceneNode *pArrowNode = pSmgr->addAnimatedMeshSceneNode(pMesh);
					
					pArrowNode->setParent(pLightNode);
					pArrowNode->setMaterialFlag(irr::video::EMF_LIGHTING, false);*/
				}
				
			}			



			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,320),true,true,0,100,true);


			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					irr::scene::ILightSceneNode *pLightNode;
					pLightNode = (irr::scene::ILightSceneNode *)pSmgr->getSceneNodeFromName("usr/scene/Light/1");

					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d\
									 \ndir %2.3f,%2.3f,%2.3f\
									 \ninnerCone :%2.3f\
									 \noutter cone: %2.3f\
									 \nfalloff: %2.3f",
						pVideo->getFPS(),pVideo->getPrimitiveCountDrawn(),
						pLightNode->getLightData().Direction.X,
						pLightNode->getLightData().Direction.Y,
						pLightNode->getLightData().Direction.Z,
						pLightNode->getLightData().InnerCone,
						pLightNode->getLightData().OuterCone,
						pLightNode->getLightData().Falloff
						);
					pstextFPS->setText(wszbuf);
				}

				{
					static irr::f32 AmbientColor = .0f;
					static irr::f32 DiffuseColor = .0f;

					irr::scene::ILightSceneNode *pLightNode;
					pLightNode = (irr::scene::ILightSceneNode *)pSmgr->getSceneNodeFromName("usr/scene/Light/1");

					irr::video::SLight &light = pLightNode->getLightData();

                    /*
					if(GetAsyncKeyState(VK_UP) && 0x8000)
					{	
						pLightNode->setRotation(pLightNode->getRotation() + irr::core::vector3df(45.0f,0,0)*fDelta);	
					}
					if(GetAsyncKeyState(VK_DOWN) && 0x8000)
					{
						pLightNode->setRotation(pLightNode->getRotation() - irr::core::vector3df(45.0f,0,0)*fDelta);	
					}

					if(GetAsyncKeyState(VK_LEFT) && 0x8000)
					{	
						pLightNode->setRotation(pLightNode->getRotation() - irr::core::vector3df(.0f,45.0f,0)*fDelta);	
					}
					if(GetAsyncKeyState(VK_RIGHT) && 0x8000)
					{
						pLightNode->setRotation(pLightNode->getRotation() + irr::core::vector3df(.0f,45.f,0)*fDelta);	
					}

					if(!(::GetAsyncKeyState(VK_LCONTROL) & 0x8001))
					{
						//스폿광원의 속성
						if(GetAsyncKeyState('U') && 0x8000)
						{	
							pLightNode->getLightData().InnerCone += fDelta * 10;
						}
						if(GetAsyncKeyState('O') && 0x8000)
						{	
							pLightNode->getLightData().InnerCone -= fDelta * 10;
						}

						if(GetAsyncKeyState('I') && 0x8000)
						{	
							pLightNode->getLightData().Falloff += fDelta * 1; //내,외부 콘의 경계 강도
						}

					}
					else
					{
						if(GetAsyncKeyState('U') && 0x8000)
						{	
							pLightNode->getLightData().OuterCone += fDelta * 10;
						}
						if(GetAsyncKeyState('O') && 0x8000)
						{	
							pLightNode->getLightData().OuterCone -= fDelta * 10;
						}

						if(GetAsyncKeyState('I') && 0x8000)
						{	
							pLightNode->getLightData().Falloff -= fDelta * 1; //내,외부 콘의 경계 강도
						}

					}
                     */

				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}
}




