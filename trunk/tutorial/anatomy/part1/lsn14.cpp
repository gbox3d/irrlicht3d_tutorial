﻿#include <irrlicht.h>
#include <iostream>
//#include <Windows.h>


/////////////////////////////////////////////////
//
//파티클
//
/////////////////////////////////////////////////

namespace lsn14 { 

	//파티클기초
	namespace _00 {

		class Lsn14_App :  public irr::IEventReceiver
		{
		public:

			irr::IrrlichtDevice *m_pDevice;
			irr::video::IVideoDriver *m_pVideo;
			irr::scene::ISceneManager *m_pSmgr;
			irr::gui::IGUIEnvironment *m_pGuiEnv;

			Lsn14_App()
			{
				m_pDevice = irr::createDevice(
					irr::video::EDT_OPENGL					
					);

				m_pVideo = m_pDevice->getVideoDriver();
				m_pSmgr = m_pDevice->getSceneManager();
				m_pGuiEnv = m_pDevice->getGUIEnvironment();

				irr::scene::ICameraSceneNode* pCamera = m_pSmgr->addCameraSceneNodeMaya();
				pCamera->setPosition(irr::core::vector3df(-50,50,-150));	

				m_pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

				//라이트추가하기
				{	

					irr::scene::ILightSceneNode *pLightNode;
					pLightNode = m_pSmgr->addLightSceneNode(0, irr::core::vector3df(0,0,0),
						irr::video::SColorf(1.0f, 0.6f, 0.7f, 1.0f), 1200.0f);

					pLightNode->setPosition(irr::core::vector3df(50,150,50));			

					// attach billboard to light
					irr::scene::IBillboardSceneNode *pNode;
					pNode = m_pSmgr->addBillboardSceneNode(pLightNode, irr::core::dimension2d<irr::f32>(50, 50));
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING, false);
					pNode->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);
					pNode->setMaterialTexture(0, m_pVideo->getTexture("particlewhite.bmp"));
				}	

				//수면 만들기
				{			
					irr::scene::ISceneNode* pNode = 0;
					irr::scene::IAnimatedMesh* pMesh;
					pMesh = m_pSmgr->addHillPlaneMesh("myHill",
						irr::core::dimension2d<irr::f32>(20,20),
						irr::core::dimension2d<irr::u32>(40,40), 0,0,
						irr::core::dimension2d<irr::f32>(0,0),
						irr::core::dimension2d<irr::f32>(10,10));

					pNode = m_pSmgr->addWaterSurfaceSceneNode(pMesh->getMesh(0), 3.0f, 300.0f, 30.0f);
					pNode->setPosition(irr::core::vector3df(0,0,0));

					pNode->setMaterialTexture(0, m_pVideo->getTexture("stones.jpg"));
					pNode->setMaterialTexture(1, m_pVideo->getTexture("water.jpg"));

					pNode->setMaterialType(irr::video::EMT_REFLECTION_2_LAYER);			
				}

				//파티클 생성
				{
					irr::scene::IParticleSystemSceneNode* ps = 0;
					ps = m_pSmgr->addParticleSystemSceneNode(false);
					ps->setPosition(irr::core::vector3df(0,0,0));					

					ps->setParticleSize(irr::core::dimension2d<irr::f32>(20.0f, 20.0f));

					irr::scene::IParticleEmitter* em = ps->createBoxEmitter(
						irr::core::aabbox3d<irr::f32>(-7,0,-7,7,1,7),
						irr::core::vector3df(0.0f,0.06f,0.0f),
						80,100,
						irr::video::SColor(0,255,255,255), irr::video::SColor(0,255,255,255),
						800,2000);

					ps->setEmitter(em);
					em->drop();

					irr::scene::IParticleAffector* paf =
						ps->createFadeOutParticleAffector();

					ps->addAffector(paf);
					paf->drop();

					ps->setMaterialFlag(irr::video::EMF_LIGHTING, false);
					ps->setMaterialFlag(irr::video::EMF_ZWRITE_ENABLE, false);
					ps->setMaterialTexture(0, m_pVideo->getTexture("fire.bmp"));
					ps->setMaterialType(irr::video::EMT_TRANSPARENT_VERTEX_ALPHA);
				}

				{
					irr::scene::IParticleSystemSceneNode* ps = 0;
					ps = m_pSmgr->addParticleSystemSceneNode(false);
					ps->setPosition(irr::core::vector3df(50,0,0));

					ps->setParticleSize(irr::core::dimension2d<irr::f32>(20.0f, 20.0f));					

					irr::scene::IParticleEmitter* em = ps->createRingEmitter(
						irr::core::vector3df(0,0,0),
						30,10,
						irr::core::vector3df(0,0.01f,0)
						);
					em->setMinStartColor(irr::video::SColor(0,255,255,255));
					em->setMaxStartColor(irr::video::SColor(0,255,255,255));

					ps->setEmitter(em);
					em->drop();

					irr::scene::IParticleAffector* paf =
						ps->createFadeOutParticleAffector();

					ps->addAffector(paf);
					paf->drop();

					ps->setMaterialFlag(irr::video::EMF_LIGHTING, false);
					ps->setMaterialFlag(irr::video::EMF_ZWRITE_ENABLE, false);
					ps->setMaterialTexture(0, m_pVideo->getTexture("fire.bmp"));
					ps->setMaterialType(irr::video::EMT_TRANSPARENT_VERTEX_ALPHA);
				}


				{
					irr::scene::ISceneNode *pNode =  m_pSmgr->addCubeSceneNode();
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
					pNode->setMaterialTexture(0,m_pVideo->getTexture("t351sml.jpg"));	
				}


				{//노말멥


					irr::scene::IAnimatedMesh *pMesh =  m_pSmgr->addSphereMesh("Sphere",50);
					irr::scene::ISceneNode* pNode = 0;
					irr::video::ITexture* pColorMap = m_pVideo->getTexture("rockwall.bmp");
					irr::video::ITexture* pNormalMap = m_pVideo->getTexture("rockwall_height.bmp");

					m_pVideo->makeNormalMapTexture(pNormalMap, 9.0f);

					irr::scene::IMesh* pTangentMesh = m_pSmgr->getMeshManipulator()->createMeshWithTangents(pMesh->getMesh(0));

					pNode = m_pSmgr->addMeshSceneNode(pTangentMesh);

					pNode->setMaterialTexture(0, pColorMap);
					pNode->setMaterialTexture(1, pNormalMap);

					pNode->getMaterial(0).SpecularColor.set(0,0,0,0);

					pNode->setMaterialFlag(irr::video::EMF_FOG_ENABLE, true);
					pNode->setMaterialType(irr::video::EMT_NORMAL_MAP_SOLID);
					pNode->getMaterial(0).MaterialTypeParam = 0.035f; // adjust height for parallax effect

					pNode->setPosition(irr::core::vector3df(-50,150,0));

					// drop mesh because we created it with a create.. call.
					pTangentMesh->drop();
				}

				{
					// add particle system
					irr::scene::IParticleSystemSceneNode* ps =
						m_pSmgr->addParticleSystemSceneNode(false);

					ps->setParticleSize(irr::core::dimension2d<irr::f32>(30.0f, 40.0f));

					// create and set emitter
					irr::scene::IParticleEmitter* em = ps->createBoxEmitter(
						irr::core::aabbox3d<irr::f32>(-3,0,-3,3,1,3),
						irr::core::vector3df(0.0f,0.03f,0.0f),
						80,100,
						irr::video::SColor(0,255,255,255), irr::video::SColor(0,255,255,255),
						400,1100);
					ps->setEmitter(em);
					em->drop();

					// create and set affector
					irr::scene::IParticleAffector* paf = ps->createFadeOutParticleAffector();
					ps->addAffector(paf);
					paf->drop();

					// adjust some material settings
					ps->setMaterialFlag(irr::video::EMF_LIGHTING, false);
					ps->setMaterialFlag(irr::video::EMF_ZWRITE_ENABLE, false);
					ps->setMaterialTexture(0, m_pVideo->getTexture("fireball.bmp"));
					ps->setMaterialType(irr::video::EMT_TRANSPARENT_VERTEX_ALPHA);

					irr::scene::ISceneNodeAnimator* pAnim;
					// add fly circle animator to light 2
					pAnim = m_pSmgr->createFlyCircleAnimator (irr::core::vector3df(0,150,0),200.0f, 0.001f, irr::core::vector3df ( 0.2f, 0.9f, 0.f ));
					ps->addAnimator(pAnim);
					pAnim->drop();	

				}
			}


			~Lsn14_App()
			{
				m_pDevice->drop();
			}	

			//이벤트 핸들러
			virtual bool OnEvent(const irr::SEvent& event)
			{
				switch(event.EventType)
				{
				case irr::EET_GUI_EVENT:
					{

					}
					break;
				case irr::EET_KEY_INPUT_EVENT:
					{
					}
					break;
				case irr::EET_MOUSE_INPUT_EVENT:
					{

					}
					break;
				case irr::EET_USER_EVENT:
					break;
				}
				return false;
			}	

		};
		void main()
		{
			Lsn14_App App;	

			while(App.m_pDevice->run())
			{		
				App.m_pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				App.m_pSmgr->drawAll();				

				App.m_pVideo->endScene();
			}
		}

		//파티클 씬노드, 이미터, 어펙터 관계 예제
		//화염 방사기 효과 구현
		namespace _01
		{
			void main()
			{
				irr::IrrlichtDevice *pDevice = irr::createDevice(					
					irr::video::EDT_OPENGL
					);

				pDevice->setWindowCaption(L"Type-A1");
				pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

				irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
				irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
				irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

				pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,100,-200), irr::core::vector3df(0,0,0));

				//프레임 레이트 표시용 유아이
				irr::gui::IGUIStaticText *pstextFPS = 
					pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

				irr::scene::IParticleSystemSceneNode* psNode;			
				{

					psNode = pSmgr->addParticleSystemSceneNode(false);

					psNode->setParticleSize(irr::core::dimension2d<irr::f32>(5.0f,5.0f));
					//재질설정
					psNode->setMaterialFlag(irr::video::EMF_LIGHTING, false);
					psNode->setMaterialFlag(irr::video::EMF_ZWRITE_ENABLE, false);
					psNode->setMaterialTexture(0, pVideo->getTexture("fire.bmp"));
					psNode->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);
				}

				irr::scene::IParticleSystemSceneNode* psNode2;		
				{

					psNode2 = pSmgr->addParticleSystemSceneNode(false);

					psNode2->setParticleSize(irr::core::dimension2d<irr::f32>(5.0f,5.0f));
					psNode2->setPosition(irr::core::vector3df(10,0,0));
					//재질설정
					psNode2->setMaterialFlag(irr::video::EMF_LIGHTING, false);
					psNode2->setMaterialFlag(irr::video::EMF_ZWRITE_ENABLE, false);
					psNode2->setMaterialTexture(0, pVideo->getTexture("fire.bmp"));
					psNode2->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);
				}

				irr::scene::IParticleBoxEmitter* emBox; 
				//분출기 만들기
				{
					emBox = psNode->createBoxEmitter(
						irr::core::aabbox3df(-10,-10,-10,10,10,10),
						irr::core::vector3df(0.0f,0.05f,0),
						5,1000,
						irr::video::SColor(255,0,0,0),irr::video::SColor(255,255,255,255),
						2000,4000,
						45,
						irr::core::dimension2df(5.f,5.f),
						irr::core::dimension2df(5.f,5.f)
						);

					emBox->setMaxParticlesPerSecond(1000);

					//psNode->setEmitter(emBox);
					//emBox->drop();

					psNode->setName("usr/particle/1");					
				}

				irr::scene::IParticleAffector *pScaleAffector;
				{
					pScaleAffector = psNode->createScaleParticleAffector(irr::core::dimension2df(10.0f,10.f));
				}

				irr::scene::IParticleAffector *pfadeAffector;
				{
					pfadeAffector = psNode->createFadeOutParticleAffector(
						irr::video::SColor(0,0,0,0),2000);
				}

				irr::scene::IParticlePointEmitter *emPoint;
				{

					emPoint = psNode->createPointEmitter(
						irr::core::vector3df(0.0f,0.05f,0),
						50,100,
						irr::video::SColor(255,255,255,255),
						irr::video::SColor(255,255,255,255),
						1000,2000
						);								
				}			

				irr::u32 uLastTick = pDevice->getTimer()->getTime();
				while(pDevice->run())
				{	
					irr::u32 uTick = pDevice->getTimer()->getTime();
					irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
					uLastTick = pDevice->getTimer()->getTime();

					//프레임레이트 갱신
					{
						wchar_t wszbuf[256];
						swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
						pstextFPS->setText(wszbuf);
					}
                    /*

					if(::GetAsyncKeyState(VK_F1) && 0x8000)
					{
						psNode->setEmitter(emBox);

					}

					if(::GetAsyncKeyState(VK_F2) && 0x8000)
					{
						psNode->setEmitter(emPoint);
					}

					if(::GetAsyncKeyState(VK_F3) && 0x8000)
					{
						psNode->setEmitter(NULL);
					}

					if(::GetAsyncKeyState(VK_F4) && 0x8000)
					{
						psNode2->setEmitter(emPoint);
					}

					if(::GetAsyncKeyState(VK_F5) && 0x8000)
					{
						psNode->addAffector(pScaleAffector);			
					}
					if(::GetAsyncKeyState(VK_F6) && 0x8000)
					{
						psNode->addAffector(pfadeAffector); //페이트 아웃어펙터 추가
					}

					if((::GetAsyncKeyState(VK_F7) & 0x8001) == 0x8001 )
					//if(::GetAsyncKeyState(VK_F7) && 0x8000)
					{
						irr::scene::ISceneNodeAnimator *pAnim = 
							pSmgr->createFlyCircleAnimator(irr::core::vector3df(0,0,0),
							20.f,0.01f);

						psNode->addAnimator(pAnim);

						pAnim->drop();						
					}


					//파티클 갯수 늘리기
					if(::GetAsyncKeyState('Q') && 0x8000)
					{
						emPoint->setMaxParticlesPerSecond(
							emPoint->getMaxParticlesPerSecond() + 1000 * fDelta
							);
					}

					if(::GetAsyncKeyState('W') && 0x8000)
					{
						emPoint->setMaxParticlesPerSecond(
							emPoint->getMaxParticlesPerSecond() -( 1000 * fDelta)
							);
					}

					//방향 회전시키기
					if(::GetAsyncKeyState('A') && 0x8000)
					{
						irr::core::vector3df dir = emPoint->getDirection();
						dir.rotateXYBy(45.f* fDelta);
						emPoint->setDirection(dir);
					}				

					if(::GetAsyncKeyState('S') && 0x8000)
					{
						irr::core::vector3df dir = emPoint->getDirection();
						dir.rotateXYBy(-45.f* fDelta);
						emPoint->setDirection(dir);
					}		
                     
                     */

					pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

					pSmgr->drawAll();
					pGuiEnv->drawAll();		

					pVideo->endScene();	
				}

				emBox->drop();
				emPoint->drop();
				pScaleAffector->drop();
				pfadeAffector->drop();

				pDevice->drop();
			}

		}
	}



	//빅뱅예제
	/*
	씬노드를 임의방향으로 고속 회전 시켜서 임의방향으로 파티클이 분출되도록 했다.
	중력 어펙터가 없으면 파티클노드를 회전시켜도 회전이 먹지않는다.
	중력 방향을(0,0,0) 으로하는 중력 어펙터를 붙이고 회전을 시켜야 빅뱅 효과를 얻을수있다.
	*/
	namespace _01 {

		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			//박스 에미터 이용
			{
				irr::scene::IParticleSystemSceneNode* ps = 0;
				ps = pSmgr->addParticleSystemSceneNode();
				ps->setPosition(irr::core::vector3df(-200,200,0));
				ps->setParticleSize(irr::core::dimension2d<irr::f32>(5.0f,5.0f));
				//재질설정
				ps->setMaterialFlag(irr::video::EMF_LIGHTING, false);
				ps->setMaterialFlag(irr::video::EMF_ZWRITE_ENABLE, false);
				ps->setMaterialTexture(0, pVideo->getTexture("fire.bmp"));
				ps->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);

				irr::scene::IParticleBoxEmitter* em = ps->createBoxEmitter();						
				em->setDirection(irr::core::vector3df(0,0.05f,0));
				em->setMaxParticlesPerSecond(1000);

				ps->setEmitter(em);
				em->drop();

				// 중력 어펙터
				{
					irr::scene::IParticleAffector* paf =
						ps->createGravityAffector(irr::core::vector3df(0.f,0.f,0.f),10000);
					ps->addAffector(paf);
					paf->drop();					
				}

				{
					irr::scene::ISceneNodeAnimator *pAnim = pSmgr->createRotationAnimator(irr::core::vector3df(0,0,90));
					ps->addAnimator(pAnim);
					pAnim->drop();
				}					

				//ps->setRotation(irr::core::vector3df(0,0,90));
			}

			//포인트 에미터 이용
			{
				irr::scene::IParticleSystemSceneNode* ps = 0;
				ps = pSmgr->addParticleSystemSceneNode();
				ps->setPosition(irr::core::vector3df(0,200,0));
				ps->setParticleSize(irr::core::dimension2d<irr::f32>(5.0f,5.0f));
				//재질설정
				ps->setMaterialFlag(irr::video::EMF_LIGHTING, false);
				ps->setMaterialFlag(irr::video::EMF_ZWRITE_ENABLE, false);
				ps->setMaterialTexture(0, pVideo->getTexture("fire.bmp"));
				ps->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);

				irr::scene::IParticleEmitter* em = ps->createPointEmitter();
				em->setDirection(irr::core::vector3df(0,0.05f,0));
				em->setMaxParticlesPerSecond(1000);


				ps->setEmitter(em);
				em->drop();

				// 중력 어펙터
				{
					irr::scene::IParticleAffector* paf =
						ps->createGravityAffector(irr::core::vector3df(0.f,0.f,0.f),10000);
					ps->addAffector(paf);
					paf->drop();					
				}

				{
					irr::scene::ISceneNodeAnimator *pAnim = pSmgr->createRotationAnimator(irr::core::vector3df(0,0,90));
					ps->addAnimator(pAnim);
					pAnim->drop();
				}					

			}

			//구 에미터 이용
			{
				irr::scene::IParticleSystemSceneNode* ps = 0;
				ps = pSmgr->addParticleSystemSceneNode();
				ps->setPosition(irr::core::vector3df(200,200,0));
				ps->setParticleSize(irr::core::dimension2d<irr::f32>(5.0f,5.0f));
				//재질설정
				ps->setMaterialFlag(irr::video::EMF_LIGHTING, false);
				ps->setMaterialFlag(irr::video::EMF_ZWRITE_ENABLE, false);
				ps->setMaterialTexture(0, pVideo->getTexture("fire.bmp"));
				ps->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);

				irr::scene::IParticleEmitter* em = ps->createSphereEmitter(irr::core::vector3df(0,0,0),10);
				em->setDirection(irr::core::vector3df(0,0.05f,0));
				em->setMaxParticlesPerSecond(1000);


				ps->setEmitter(em);
				em->drop();

				// 중력 어펙터
				{
					irr::scene::IParticleAffector* paf =
						ps->createGravityAffector(irr::core::vector3df(0.f,0.f,0.f),10000);
					ps->addAffector(paf);
					paf->drop();					
				}

				{
					irr::scene::ISceneNodeAnimator *pAnim = pSmgr->createRotationAnimator(irr::core::vector3df(0,0,90));
					ps->addAnimator(pAnim);
					pAnim->drop();
				}										
			}


			//어펙터 회전시키지않고 분사각으로 빅뱅 만들기
			//박스 에미터 이용
			{
				irr::scene::IParticleSystemSceneNode* ps = 0;
				ps = pSmgr->addParticleSystemSceneNode();
				ps->setPosition(irr::core::vector3df(-200,-200,0));
				ps->setParticleSize(irr::core::dimension2d<irr::f32>(5.0f,5.0f));
				//재질설정
				ps->setMaterialFlag(irr::video::EMF_LIGHTING, false);
				ps->setMaterialFlag(irr::video::EMF_ZWRITE_ENABLE, false);
				ps->setMaterialTexture(0, pVideo->getTexture("fire.bmp"));
				ps->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);

				irr::scene::IParticleBoxEmitter* em = 
					ps->createBoxEmitter(
					irr::core::aabbox3df(-10,-10,-10,10,10,10),
					irr::core::vector3df(0.0f,.1f,0),//분사방향(속도)
					5,1000,//파티클분출수
					irr::video::SColor(255,0,255,0),irr::video::SColor(255,255,0,0),//컬러값 범위
					500,1000, //라이프타임
					360,
					irr::core::dimension2df(5.f,5.f),
					irr::core::dimension2df(5.f,5.f)
					);
				ps->setEmitter(em);
				em->drop();					
			}


			//카메라
			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,0,-500), irr::core::vector3df(0,0,0));

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			irr::u32 uLastTick = pDevice->getTimer()->getTime();
			while(pDevice->run())
			{	
				irr::u32 uTick = pDevice->getTimer()->getTime();
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = pDevice->getTimer()->getTime();

				//프레임레이트 갱신
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}


	}

	/*
	에미터 회전 예제
	*/	
	namespace _02 {


		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL,
#if(IRRLICHT_VERSION_MAJOR == 1 && IRRLICHT_VERSION_MINOR >= 6 )
				irr::core::dimension2d<irr::u32>(640, 480), 
#else
				irr::core::dimension2d<irr::s32>(640, 480), 
#endif
				32,
				false, false, false,
				NULL
				);

			pDevice->setWindowCaption(L"Type-A1");
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,0,-200), irr::core::vector3df(0,0,0));

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);


			//박스 에미터 이용
			{
				irr::scene::IParticleSystemSceneNode* ps;
				ps = pSmgr->addParticleSystemSceneNode();
				//ps->setPosition(irr::core::vector3df(-200,200,0));
				ps->setParticleSize(irr::core::dimension2d<irr::f32>(5.0f,5.0f));
				//재질설정
				ps->setMaterialFlag(irr::video::EMF_LIGHTING, false);
				ps->setMaterialFlag(irr::video::EMF_ZWRITE_ENABLE, false);
				ps->setMaterialTexture(0, pVideo->getTexture("fire.bmp"));
				ps->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);

				irr::scene::IParticleBoxEmitter* em = ps->createBoxEmitter(
					irr::core::aabbox3df(-10,-10,-10,10,10,10),
					irr::core::vector3df(0.0f,0.05f,0),
					5,1000,
					irr::video::SColor(255,0,0,0),irr::video::SColor(255,255,255,255),
					2000,4000,
					45,
					irr::core::dimension2df(5.f,5.f),
					irr::core::dimension2df(5.f,5.f)
					);						
				//em->setDirection(irr::core::vector3df(0.0f,0.0f,0));
				em->setMaxParticlesPerSecond(1000);

				ps->setEmitter(em);
				em->drop();

				ps->setName("usr/particle/1");


			}

			irr::u32 uLastTick = pDevice->getTimer()->getTime();
			while(pDevice->run())
			{	
				irr::u32 uTick = pDevice->getTimer()->getTime();
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = pDevice->getTimer()->getTime();

				//프레임레이트 갱신
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				//에미터의 분출 방향 회전시키기
				{
					irr::scene::IParticleSystemSceneNode* ps = 
						(irr::scene::IParticleSystemSceneNode*)pSmgr->getSceneNodeFromName("usr/particle/1");
					irr::core::vector3df vDir = ps->getEmitter()->getDirection();
					std::cout << vDir.X << " " << vDir.Y << " " << vDir.Z << std::endl; 
					vDir.rotateXYBy(360.f*fDelta,irr::core::vector3df(0,0,0));
					ps->getEmitter()->setDirection(vDir);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}

	}

	/*
	파티클 이펙트 시스템 예제
	*/
	namespace _03 {

		struct S_TimeTrack {			
			irr::f32 m_fTimeToRun;
			irr::core::stringc m_strEvent;			
		};

		class CJPaticleEffectSystem
		{
		public:
			irr::scene::IParticleSystemSceneNode *m_ps;

			int m_nStep;
			bool m_IsDie;

			irr::core::dimension2df m_ParicleStart;
			irr::core::dimension2df m_ParicleEnd;
			irr::f32 m_fTotalTick;
			irr::f32 m_fAcctick;


			void Update(irr::f32 fDelta)
			{
				m_fAcctick += fDelta;

				if(m_fAcctick > m_fTotalTick)
					m_fAcctick = m_fTotalTick;


				irr::core::dimension2df curParticle = m_ParicleEnd.getInterpolated(
					m_ParicleStart, 
					m_fAcctick/m_fTotalTick);

				std::cout << curParticle.Width << std::endl;

				m_ps->getEmitter()->setMaxParticlesPerSecond(irr::u32(curParticle.Height));
				m_ps->getEmitter()->setMinParticlesPerSecond(irr::u32(curParticle.Width));
			}

			void Init(irr::IrrlichtDevice *pDevice)
			{
				m_fAcctick = 0;

				m_fTotalTick = 3;
				m_ParicleStart = irr::core::dimension2df(5000,5000);
				m_ParicleEnd = irr::core::dimension2df(0,0);

				irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
				irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
				//박스 에미터 이용
				irr::scene::IParticleSystemSceneNode* ps;
				ps = pSmgr->addParticleSystemSceneNode();

				//ps->setPosition(irr::core::vector3df(-200,200,0));
				ps->setParticleSize(irr::core::dimension2d<irr::f32>(5.0f,5.0f));

				//재질설정
				ps->setMaterialFlag(irr::video::EMF_LIGHTING, false);
				ps->setMaterialFlag(irr::video::EMF_ZWRITE_ENABLE, false);
				ps->setMaterialTexture(0, pVideo->getTexture("fire.bmp"));
				ps->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);

				irr::scene::IParticleBoxEmitter* em = ps->createBoxEmitter(
					irr::core::aabbox3df(-10,-10,-10,10,10,10),
					irr::core::vector3df(0.0f,0.05f,0),
					0,0,
					irr::video::SColor(255,0,0,0),irr::video::SColor(255,255,255,255),
					200,1000,
					360,
					irr::core::dimension2df(5.f,5.f),
					irr::core::dimension2df(5.f,5.f)
					);				
				ps->setEmitter(em);
				em->drop();			

				ps->setName("usr/scene/particle");		


				//pDevice->getFileSystem()->
				//m_ps->serializeAttributes(
				//pSmgr->saveScene("test.irr");

				m_ps = ps;				
			}
		};

		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL,
#if(IRRLICHT_VERSION_MAJOR == 1 && IRRLICHT_VERSION_MINOR >= 6 )
				irr::core::dimension2d<irr::u32>(640, 480), 
#else
				irr::core::dimension2d<irr::s32>(640, 480), 
#endif
				32,
				false, false, false,
				NULL
				);

			pDevice->setWindowCaption(L"lsn14-3");
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			CJPaticleEffectSystem jpes;
			jpes.Init(pDevice);

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,10,-200), irr::core::vector3df(0,0,0));

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			irr::u32 uLastTick = pDevice->getTimer()->getTime();
			while(pDevice->run())
			{	
				irr::u32 uTick = pDevice->getTimer()->getTime();
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = pDevice->getTimer()->getTime();

				//프레임레이트 갱신
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				jpes.Update(fDelta);

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}

	// 	
	//에미터(분출기) 예제
	namespace _04 {

		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL,
#if(IRRLICHT_VERSION_MAJOR == 1 && IRRLICHT_VERSION_MINOR >= 6 )
				irr::core::dimension2d<irr::u32>(640, 480), 
#else
				irr::core::dimension2d<irr::s32>(640, 480), 
#endif
				32,
				false, false, false,
				NULL
				);

			pDevice->setWindowCaption(L"Type-A1");
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,80,-200), irr::core::vector3df(0,0,0));

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);


			//박스 에미터 이용
			{
				irr::scene::IParticleSystemSceneNode* ps;
				ps = pSmgr->addParticleSystemSceneNode();					
				ps->setParticleSize(irr::core::dimension2d<irr::f32>(5.0f,5.0f));
				//재질설정
				ps->setMaterialFlag(irr::video::EMF_LIGHTING, false);
				ps->setMaterialFlag(irr::video::EMF_ZWRITE_ENABLE, false);
				ps->setMaterialTexture(0, pVideo->getTexture("fire.bmp"));
				ps->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);

				irr::scene::IParticleBoxEmitter* em = ps->createBoxEmitter(
					irr::core::aabbox3df(-10,-10,-10,10,10,10),
					irr::core::vector3df(0.0f,0.05f,0),
					5,1000,
					irr::video::SColor(255,0,0,0),irr::video::SColor(255,255,255,255),
					2000,4000,
					45, //분사각 범위
					irr::core::dimension2df(5.f,5.f),
					irr::core::dimension2df(5.f,5.f)
					);											

				ps->setEmitter(em);
				em->drop();
				ps->setName("usr/particle/box");					
			}

			//포인트 에미터 이용
			{
				irr::scene::IParticleSystemSceneNode* ps;
				ps = pSmgr->addParticleSystemSceneNode();					
				ps->setParticleSize(irr::core::dimension2d<irr::f32>(5.0f,5.0f));
				//재질설정
				ps->setMaterialFlag(irr::video::EMF_LIGHTING, false);
				ps->setMaterialFlag(irr::video::EMF_ZWRITE_ENABLE, false);
				ps->setMaterialTexture(0, pVideo->getTexture("fire.bmp"));
				ps->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);

				irr::scene::IParticlePointEmitter* em = ps->createPointEmitter(						
					irr::core::vector3df(0.0f,0.05f,0),
					5,1000,
					irr::video::SColor(255,0,0,0),
					irr::video::SColor(255,255,255,255),
					2000,4000,
					45,
					irr::core::dimension2df(5.f,5.f),
					irr::core::dimension2df(5.f,5.f)
					);											

				ps->setEmitter(em);
				em->drop();
				ps->setName("usr/particle/point");		
				ps->setPosition(irr::core::vector3df(100,0,0));
			}

			//링 에미터 이용
			{
				irr::scene::IParticleSystemSceneNode* ps;
				ps = pSmgr->addParticleSystemSceneNode();					
				ps->setParticleSize(irr::core::dimension2d<irr::f32>(5.0f,5.0f));
				//재질설정
				ps->setMaterialFlag(irr::video::EMF_LIGHTING, false);
				ps->setMaterialFlag(irr::video::EMF_ZWRITE_ENABLE, false);
				ps->setMaterialTexture(0, pVideo->getTexture("fire.bmp"));
				ps->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);

				irr::scene::IParticleEmitter* em = ps->createRingEmitter(
					irr::core::vector3df(0,0,0),
					20,10,
					irr::core::vector3df(0.0f,0.05f,0),
					500,1000,
					irr::video::SColor(255,0,0,0),
					irr::video::SColor(255,255,255,255),
					2000,4000,
					0,
					irr::core::dimension2df(5.f,5.f),
					irr::core::dimension2df(5.f,5.f)
					);				

				ps->setEmitter(em);
				em->drop();
				ps->setName("usr/particle/ring");		
				ps->setPosition(irr::core::vector3df(-100,0,0));
			}			
			irr::u32 uLastTick = pDevice->getTimer()->getTime();
			while(pDevice->run())
			{	
				irr::u32 uTick = pDevice->getTimer()->getTime();
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = pDevice->getTimer()->getTime();

				//프레임레이트 갱신
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}			

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}

	}




	//어펙터(효과기) 예제
	namespace _05 {

		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_OPENGL,
#if(IRRLICHT_VERSION_MAJOR == 1 && IRRLICHT_VERSION_MINOR >= 6 )
				irr::core::dimension2d<irr::u32>(640, 480), 
#else
				irr::core::dimension2d<irr::s32>(640, 480), 
#endif
				32,
				false, false, false,
				NULL
				);

			pDevice->setWindowCaption(L"Type-A1");
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,80,-200), irr::core::vector3df(0,0,0));

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			{
				irr::scene::IParticleSystemSceneNode* ps;
				ps = pSmgr->addParticleSystemSceneNode();					
				ps->setParticleSize(irr::core::dimension2d<irr::f32>(5.0f,5.0f));
				//재질설정
				ps->setMaterialFlag(irr::video::EMF_LIGHTING, false);
				ps->setMaterialFlag(irr::video::EMF_ZWRITE_ENABLE, false);
				ps->setMaterialTexture(0, pVideo->getTexture("fire.bmp"));
				ps->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);

				//분출기 등록
				irr::scene::IParticlePointEmitter* em = ps->createPointEmitter(						
					irr::core::vector3df(0.0f,0.05f,0),
					5,100,
					irr::video::SColor(255,0,0,0),
					irr::video::SColor(255,255,255,255),
					2000,8000,
					15,
					irr::core::dimension2df(5.f,5.f),
					irr::core::dimension2df(5.f,5.f)
					);											

				ps->setEmitter(em);
				em->drop();

				// 중력 어펙터(효과기)
				{
					irr::scene::IParticleAffector* paf =
						ps->createGravityAffector(
						irr::core::vector3df(0,-0.03f,0), //중력 힘의 크기와 방향
						2000 //중력 정점 돌달시간
						);
					ps->addAffector(paf);
					paf->drop();					
				}
				ps->setName("usr/particle/point/gravity");
			}			


			irr::u32 uLastTick = pDevice->getTimer()->getTime();
			while(pDevice->run())
			{	
				irr::u32 uTick = pDevice->getTimer()->getTime();
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = pDevice->getTimer()->getTime();

				//프레임레이트 갱신
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,256,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}			

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}

	}

}


