﻿#include <irrlicht.h>
#pragma comment(lib,"irrlicht.lib")

#include <map>
#include <iostream>
#include <string>

typedef void (*fPtr)();
std::map<std::string, fPtr > mapExam;

void test()
{
	std::cout << "hello world" << std::endl;
}

namespace lsn00{ //샘플예제 유형별 스캘래톤 코드 
	namespace _00{void main();} // 기본 skeleton		
	namespace _01{void main();}//씬매니져를 같이쓰면서 직접그리기 skeleton
	namespace _02{void main();} //씬파일로딩 skeleton
	namespace _03{void main();}//씬매니져 없이 직접메트릭스접근방식의 퍼스펙티브뷰 기반 skeleton
	
		
}

namespace lsn01 {
	namespace _00 {void main();	}
	namespace _01 {void main();	} //zip파일읽기 예제
	namespace _02 {void main();	} //암호화된 zip파일읽기 예제
	namespace _03 {void main();	} //파일 아카이브 덤프(파일 목록)
	namespace _04{void main();}//tokenizer
	namespace _05{void main();} //xml 파싱(읽기)
	namespace _06{void main();} //xml 쓰기
	namespace _07{void main();}
	namespace _08{void main();} // 유니코드 멀티 바이트 변환
    namespace _09{void main();} // 파일쓰기
    
}

//기본 도형 그리기 예제
namespace lsn02 {
	namespace _00 {void main();	} //선,삼각형,박스
	namespace _01 {void main();	} //drawIndexedTriangle
	namespace _02 {void main();	} //drawIndexedTriangleFan
	namespace _03 {void main();	} //drawVertexPrimitiveList , line
	namespace _04 {void main();	} //drawVertexPrimitiveList, 포인트스프라이트
	namespace _05 {void main();	} ////drawVertexPrimitiveList, EPT_TRIANGLE_....
}


//수학 예제
namespace lsn03 {	
	namespace _00 {
		void main();
		namespace _01 {//dx 고정밀도 부동소수 옵션에 관한 오류예제
			void main();
		}
	}//부동소수점 정밀도 관련 오류 다루기 예제
	namespace _01 {void main();}//벡터의 뎃셈 뺄셈예	
	namespace _02 {void main();}//외적의 왼손 법칙예	

	namespace _03 {void main();}//직접행렬 대입 변환예제	
	namespace _04 {void main();}//행렬변환예제	

	namespace _05 {void main();}//직선과 임의점과의 근접점 찾기 예제	
	namespace _06 {
		void main(); //평면과 직선의 접점 찾고 접점이 삼각형 안에 있는지 검사하기(피킹구현용)
		namespace _01{void main();} //직선과 삼각형 충돌검출 (좀더 간편하게...)
	}
	namespace _07 {void main();}//평면 (p·n + d =0) 가시화	
	namespace _08 {void main();}//벡터의 내적 가시화	
	namespace _09 {void main();}//평면 직선 접점 구하는 과정 가시화	
	namespace _10 {void main();}//임의 정점이 평면 앞뒤면 인지 판별	

	namespace _11 {void main();}//소프트웨어 파이프라인 시뮬레이션
	namespace _12 {void main();}//소프트웨어 파이프라인 시뮬레이션2
	namespace _13 {void main();}//소프트웨어 파이프라인 시뮬레이션3 (클리핑)

	namespace _14 {void main();}//전 반사 벡터 구하기

	//회전의 이해
	namespace _15 {
		void main();//오일러 회전 짐벌락발생예
		namespace _01
		{
			void main();// 오일러각->사원수-> 방향벡터구하기
		}	
	}
	namespace _16 {void main();}//오일러 회전 2 트랙볼을 포기한 노짐벌락
	namespace _17 {void main();}//단일사원수 이용한 노짐벌락 트랙볼 구현
	namespace _18 {void main();}//3개의 사원수 회전1
	namespace _19 {void main();}//3개의 사원수 회전2
	namespace _20 {void main();}//3개의 사원수 회전3
	namespace _21 {void main();}//사원수의 구형보간과 선형보간의 비교예제
	namespace _22 {void main();}//사원수의 보간으로 구현한 유토탄 예제(3d에서 목표 바라보기 예)	

	namespace _23 {void main();}//임의의 축에 대한 회전

	namespace _24 {void main();} //쿼터니온 이용한 fps식 카메라 회전

	namespace _25 {void main();} //역전치 행렬의 이해

	//쿼터니온 응용
	namespace _30 {void main();} //원하는 방향으로 회전
	namespace _31 {void main();} ////유도탄구현 응용 예제

	//벡터 응용 예제
	namespace _40 {void main();} //두벡터의 각도차구하기(축과는 관계없는 최단거리 각도차)
	namespace _41 {void main();} //getHorizontalAngle 사용하기
    
    //2디 수학 관련 예제    
    namespace _50 {void main();} 
}



//정적메쉬 메쉬 다루기
//
namespace lsn04
{
	namespace _00	{	void main(); }//기본도형출력(메쉬 없는...)
	namespace _01	{	void main(); }//화살표 메쉬 노드
	namespace _02	{	void main(); }//GeometryCreator 이용 메쉬노드 만들기
	namespace _03	{	
		void main();
		namespace _01 {void main();}//getMeshManipulator이용 해서 메쉬의 버텍스 컬러바꾸기
	}//MeshManipulator 응용
	namespace _04	{	void main(); }//힐플래인 메쉬
	namespace _05	{	void main(); }//메쉬 파일로딩
	namespace _06	{	void main(); }//메쉬 수동으로 직접 만들기
	namespace _07	{	void main(); }//메모리에 올린 메쉬를 파일로 저장하기

	namespace _08	{	void main(); } //메쉬 직접 만들어 drawMeshBuffer 함수로 직접출력하기
	namespace _09	{	void main(); } //대용량 메쉬 출력위한 하드웨어 버퍼 사용하기,IMesh::setHardwareMappingHint
}

//기본 애니메이션메쉬씬노드 예제
//동적 메쉬 다루기(animated mesh)
namespace lsn05
{
	namespace _00	{	void main(); } //애니씬노드 예제
	namespace _01	{	void main(); } //detaile level
	namespace _02	{	void main(); } //md2 정점애니메이션
	namespace _03	{	void main(); } //애니메쉬 직접프레임추가
	namespace _04	{	void main(); }
	namespace _05	{	void main(); }
	namespace _10	{	void main(); } //애니메이션 종료 콜백 예제

}

void Lession06(); //씬그래프 계층구조이해
//씬매니징 예제
namespace lsn06
{
	namespace _00 {void main();} // 씬매니져없이 직접 메쉬 변환 직접 적용
	namespace _01 {void main();} //씬노드없이 메쉬에 직접 변환적용 쿼터니온 적용예
	namespace _02 {void main();} //씬매니져 의 addToDeletionQueue 사용 해서 노드 안전하게 제거하기
	namespace _03 {void main();} //씬그라프 순회하기
}

void Lession07(); //스키닝 애니메이션,조인트 다루기
void Lession08(); //애니메이터
void Lession08_01();

//애니메이터 기반 애니메이션 제어
//회전, 카메라 제어
namespace lsn08{
	namespace _00{void main();}

	namespace _10{void main();}//기본 fps
	namespace _11{void main();}//비행시뮬
	namespace _12{void main();}//갸우뚱 구현
	namespace _13{void main();} //트랙볼
}

//GUI 예제
namespace lsn09 {
	void main(); //기본 컨트롤 출력예제
	namespace _01{void main();} //탭컨트롤 예제
	namespace _02{void main();} //메뉴
	namespace _03{void main();} //툴바
	namespace _04{void main();} //list 박스
	namespace _05{void main();} //스크롤바
	namespace _06{void main();} //파일 다이얼로그
	namespace _07{void main();} // 이미지 버튼예제
	namespace _08{void main();} //in out fader
	namespace _09{void main();} //스킨 예제

}



void Lession10(); //2D 그래픽스 폰트직접출력

//2D 그래픽스
namespace lsn10{
	namespace _00{void main();} //스프라이트 출력
	namespace _01{void main();} //폰트출력
	namespace _02{void main();}//2D triangle list 이용하기 1.
	namespace _03{void main();}//2D triangle list 이용하기 2.
	namespace _04{void main();} //기타도형출력
}


//void Lession11(); //충돌 처리(바운딩박스,픽킹,선택자)
namespace lsn11{
	
	namespace _00 {
		void main();
		namespace _01 {void main();}
	} //mouse picking

	namespace _01 {
		void main(); //aabb 충돌검출

		namespace _01_01 {void main();} //간략화한 aabb 충돌검출
	} 
	namespace _02 {void main();} //충돌반응 활동자
	namespace _03 {void main();} //평면에 마우스 포인터 피킹하기

	//카메라 절두체 관련 예제
	namespace _04 {void main();} //카메라 절두체 관련 예제
	namespace _05 {
		void main();//카메라 시야바운딩박스(aabbox) 이용한 직접 오브잭트 컬링
		namespace _01 {
			void main();//카메라 시야 절두체(SViewFrustum) 이용한 직접 오브잭트 컬링
		}
	
	} 
	namespace _06 {void main();} //카메라 절두체 이용한 오브잭트 컬링
	namespace _07 {void main();} //공간처리 응용 가시화 트리 검색 예
	namespace _08 {
		void main(); //AABV 컬링 커스텀씬 노드 (공간관리트리 응용)
		namespace _01
		{
			void main();//퀄링된 씬노드의 애니메이터 적용 여부 확인 예제
		}
	} 

	namespace _09 {void main();}//2D aabb 충돌검출

	namespace _10 {void main();} //박스피킹//aabb박스와 lind3d 충돌 검사
	namespace _11 {void main();} // 3d->2d
	namespace _12 {void main();} // 2d->3d
}


//배경 처리 기술 예제
//실외지형 CLOD , 
//실내 BSP 카메라 관련예제
namespace lsn12{
	namespace _00 {void main();} //지형다루기	
	namespace _01 {void main();} //지형 편집 예제
	namespace _02 {void main();} //충돌 반응 활동자로 지형위에서 돌아다니기 예
	
}

//빛,그림자, 안개
namespace lsn13 {
	namespace _00 {void main();}//기본 점광원예제
	namespace _01 {void main();}//점광원의 각종속성
	namespace _02 {void main();}//점 광원 의 그림자, 정반사광 예제
	namespace _03 {void main();}//방향성광
	namespace _04 {void main();}//그림자 행렬 이용, 직접 그림자 만들기
	namespace _05 {void main();}//안개효과
	namespace _06 {void main();}
}

namespace lsn14 {
	namespace _00 {
		void main();
		namespace _01{void main();}	 //파티클 속성 테스트
	}

	namespace _01 {void main();}
	namespace _02 {void main();}
	namespace _03 {void main();}
	namespace _04 {void main();}
}



int main()
{
	/*mapExam["test"] = test;
	mapExam["test"]();*/

#ifdef _DEBUG
	::_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	//_CrtSetBreakAlloc(1933);
#endif

	std::cout << "input sample id : ";

	mapExam["0-1"] = lsn00::_01::main;
	mapExam["0-2"] = lsn00::_02::main;
	mapExam["0-3"] = lsn00::_03::main;

	mapExam["1-0"] = lsn01::_00::main;
	mapExam["1-1"] = lsn01::_01::main;
	mapExam["1-2"] = lsn01::_02::main;
	mapExam["1-3"] = lsn01::_03::main;
	mapExam["1-4"] = lsn01::_04::main;
	mapExam["1-5"] = lsn01::_05::main;
	mapExam["1-6"] = lsn01::_06::main;
	mapExam["1-7"] = lsn01::_07::main;
	mapExam["1-8"] = lsn01::_08::main;
    mapExam["1-9"] = lsn01::_09::main;

	mapExam["2-0"] = lsn02::_00::main;
	mapExam["2-1"] = lsn02::_01::main;
	mapExam["2-2"] = lsn02::_02::main;
	mapExam["2-3"] = lsn02::_03::main;
	mapExam["2-4"] = lsn02::_04::main;
	mapExam["2-5"] = lsn02::_05::main;

	mapExam["3-0"] = lsn03::_00::main;
	mapExam["3-0-1"] = lsn03::_00::_01::main;
	mapExam["3-1"] = lsn03::_01::main;
	mapExam["3-2"] = lsn03::_02::main;
	mapExam["3-3"] = lsn03::_03::main;
	mapExam["3-4"] = lsn03::_04::main;
	mapExam["3-5"] = lsn03::_05::main;
	mapExam["3-6"] = lsn03::_06::main;
	mapExam["3-6-1"] = lsn03::_06::_01::main;
	mapExam["3-7"] = lsn03::_07::main;
	mapExam["3-8"] = lsn03::_08::main;
	mapExam["3-9"] = lsn03::_09::main;
	mapExam["3-10"] = lsn03::_10::main;
	mapExam["3-11"] = lsn03::_11::main;
	mapExam["3-12"] = lsn03::_12::main;
	mapExam["3-13"] = lsn03::_13::main;
	mapExam["3-14"] = lsn03::_14::main;
	mapExam["3-15"] = lsn03::_15::main;
	mapExam["3-15-1"] = lsn03::_15::_01::main;
	mapExam["3-16"] = lsn03::_16::main;
	mapExam["3-17"] = lsn03::_17::main;
	mapExam["3-18"] = lsn03::_18::main;
	mapExam["3-19"] = lsn03::_19::main;
	mapExam["3-20"] = lsn03::_20::main;
	mapExam["3-21"] = lsn03::_21::main;
	mapExam["3-22"] = lsn03::_22::main;
	mapExam["3-23"] = lsn03::_23::main;
	mapExam["3-24"] = lsn03::_24::main;
	mapExam["3-25"] = lsn03::_25::main;

	mapExam["3-30"] = lsn03::_30::main;
	mapExam["3-31"] = lsn03::_31::main;

	mapExam["3-40"] = lsn03::_40::main;
	mapExam["3-41"] = lsn03::_41::main;	
    
    mapExam["3-50"] = lsn03::_50::main;	

	mapExam["4-0"] = lsn04::_00::main;	
	mapExam["4-1"] = lsn04::_01::main;	
	mapExam["4-2"] = lsn04::_02::main;	
	mapExam["4-3"] = lsn04::_03::main;	
	mapExam["4-3-1"] = lsn04::_03::_01::main;	
	mapExam["4-4"] = lsn04::_04::main;	
	mapExam["4-5"] = lsn04::_05::main;	
	mapExam["4-6"] = lsn04::_06::main;	
	mapExam["4-7"] = lsn04::_07::main;	
	mapExam["4-8"] = lsn04::_08::main;	
	mapExam["4-9"] = lsn04::_09::main;	

	mapExam["5-0"] = lsn05::_00::main;	
	mapExam["5-1"] = lsn05::_01::main;	
	mapExam["5-2"] = lsn05::_02::main;	
	mapExam["5-3"] = lsn05::_03::main;		
	mapExam["5-4"] = lsn05::_04::main;		
	mapExam["5-5"] = lsn05::_05::main;		
	mapExam["5-10"] = lsn05::_10::main;	

	mapExam["6-0"] = lsn06::_00::main;	
	mapExam["6-1"] = lsn06::_01::main;	
	mapExam["6-2"] = lsn06::_02::main;	
	mapExam["6-3"] = lsn06::_03::main;	

	mapExam["8-0"] = lsn08::_00::main;	
	mapExam["8-10"] = lsn08::_10::main;	
	mapExam["8-11"] = lsn08::_11::main;	
	mapExam["8-12"] = lsn08::_12::main;	
	mapExam["8-13"] = lsn08::_13::main;	

	mapExam["9-0"] = lsn09::main;
	mapExam["9-1"] = lsn09::_01::main;
	mapExam["9-2"] = lsn09::_02::main;
	mapExam["9-3"] = lsn09::_03::main;
	mapExam["9-4"] = lsn09::_04::main;
	mapExam["9-5"] = lsn09::_05::main;	
	mapExam["9-6"] = lsn09::_06::main;	
	mapExam["9-7"] = lsn09::_07::main;	
	mapExam["9-8"] = lsn09::_08::main;	
	mapExam["9-9"] = lsn09::_09::main;	


	mapExam["10-0"] = lsn10::_00::main;	
	mapExam["10-1"] = lsn10::_01::main;	
	mapExam["10-2"] = lsn10::_02::main;	
	mapExam["10-3"] = lsn10::_03::main;	
	mapExam["10-4"] = lsn10::_04::main;	
	

	mapExam["11-0"] = lsn11::_00::main;	
	mapExam["11-0-1"] = lsn11::_00::_01::main;	
	mapExam["11-1"] = lsn11::_01::main;	
	mapExam["11-1-1"] = lsn11::_01::_01_01::main;
	mapExam["11-2"] = lsn11::_02::main;	
	mapExam["11-3"] = lsn11::_03::main;	
	mapExam["11-4"] = lsn11::_04::main;	
	mapExam["11-5"] = lsn11::_05::main;	
	mapExam["11-5-1"] = lsn11::_05::_01::main;
	mapExam["11-6"] = lsn11::_06::main;	
	mapExam["11-7"] = lsn11::_07::main;	
	mapExam["11-8"] = lsn11::_08::main;	
	mapExam["11-8-1"] = lsn11::_08::_01::main;	
	mapExam["11-9"] = lsn11::_09::main;	
	mapExam["11-10"] = lsn11::_10::main;
	mapExam["11-11"] = lsn11::_11::main;
	mapExam["11-12"] = lsn11::_12::main;

	mapExam["12-0"] = lsn12::_00::main;
	mapExam["12-1"] = lsn12::_01::main;
	mapExam["12-2"] = lsn12::_02::main;

	mapExam["13-0"] = lsn13::_00::main;
	mapExam["13-1"] = lsn13::_01::main;
	mapExam["13-2"] = lsn13::_02::main;
	mapExam["13-3"] = lsn13::_03::main;
	mapExam["13-4"] = lsn13::_04::main;
	mapExam["13-5"] = lsn13::_05::main;
	mapExam["13-6"] = lsn13::_06::main;

	mapExam["14-0"] = lsn14::_00::main;
	mapExam["14-0-1"] = lsn14::_00::_01::main;
	mapExam["14-1"] = lsn14::_01::main;
	mapExam["14-2"] = lsn14::_02::main;
	mapExam["14-3"] = lsn14::_03::main;
	mapExam["14-4"] = lsn14::_04::main;


	std::string input;
	std::cin >> input;

	if(mapExam.find(input) != mapExam.end())
	{
		mapExam[input]();
	}
	else
	{
		std::cout << "not found fuction" << std::endl;
	}
    return 0;
	
}



