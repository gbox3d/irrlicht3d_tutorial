﻿#define CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

#include <irrlicht.h>
//#include <assert.h>


// 모델파일 다루기 예제
namespace Lesson01
{
	//b3d 파일 읽기 예제
	namespace _00
	{

		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9,
				irr::core::dimension2du(640,480), 32,
				false, false, true,
				NULL
				);

			//창이름정하기
			pDevice->setWindowCaption(L"hello irrlicht 3d engine");
			//비디오객체얻기
			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			//씬매니져 얻기
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			//GUI객체얻기
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res/jga");


			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,30,-40), irr::core::vector3df(0,5,0));

			{
				irr::scene::IAnimatedMesh *pAniMesh = pSmgr->getMesh("skintest2.b3d");
				irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(pAniMesh);
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);						
				pNode->setMaterialTexture(0,pVideo->getTexture("박예진.jpg"));		
			}

			while(pDevice->run())
			{		
				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));		
				pSmgr->drawAll();
				pGuiEnv->drawAll();			
				pVideo->endScene();		
			}
			pDevice->drop();
		}
	}
	
	namespace _01
	{		
	}
}

