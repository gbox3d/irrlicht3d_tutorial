﻿#define WIN32_LEAN_AND_MEAN 
//#include <d3d9.h>
//#include <d3dx9.h>
#include <iostream>
#include <string>
#include <vector>
#include <irrlicht.h>

//쉐이더 샘플
namespace Lesson02 {

	//쉐이더기초
	//기초적인 변환,텍스춰링
	namespace _00 {


		class ShaderCB :  public irr::video::IShaderConstantSetCallBack
		{
		public:

			virtual void OnSetConstants(irr::video::IMaterialRendererServices *services,irr::s32 userData)
			{
				irr::video::IVideoDriver *driver = services->getVideoDriver();

				irr::core::matrix4 matWVP;

				matWVP = driver->getTransform(irr::video::ETS_PROJECTION);
				matWVP *= driver->getTransform(irr::video::ETS_VIEW);
				matWVP *= driver->getTransform(irr::video::ETS_WORLD);

				services->setVertexShaderConstant("matWVP",matWVP.pointer(),16);
			}			

		};


		void main()
		{
			irr::IrrlichtDevice *pDevice;

			irr::video::IVideoDriver *pVideo;
			irr::scene::ISceneManager *pSmgr;
			irr::gui::IGUIEnvironment *pGuiEnv;

			irr::s32 newMatrialType;

			//Jga_App ()

			pDevice = irr::createDevice(     
				irr::video::EDT_DIRECT3D9,
				irr::core::dimension2du(640,480),32
				);
			pVideo = pDevice->getVideoDriver();
			pSmgr = pDevice->getSceneManager();
			pGuiEnv = pDevice->getGUIEnvironment(); 

			//scene inittialize here

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			irr::scene::ICameraSceneNode *pCam = 
				pSmgr->addCameraSceneNode();
			pCam->setPosition(irr::core::vector3df(0,20,-150));
			pCam->setTarget(irr::core::vector3df(0,0,0));
			pCam->setName("usr/scene/cam/1");

			ShaderCB TestSCB; //쉐이더 콜백
			{
				irr::c8 *ShaderFileName = "shader/simple.hlsl";

				irr::video::IGPUProgrammingServices *gpu =
					pVideo->getGPUProgrammingServices();

				newMatrialType = gpu->addHighLevelShaderMaterialFromFiles(
					ShaderFileName,"vsMain",irr::video::EVST_VS_2_0,//버텍쉐이더
					ShaderFileName,"psMain",irr::video::EPST_PS_2_0,
					&TestSCB
					);			
			}

			{
				irr::scene::ISceneNode *pNode =
					pSmgr->addSphereSceneNode(20);
				pNode->setPosition(irr::core::vector3df(0,0,0));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setName("usr/scene/sphere/1");

				irr::scene::ISceneNodeAnimator *pAnim =
					pSmgr->createRotationAnimator(irr::core::vector3df(0,0.1f,0));
				pNode->addAnimator(pAnim);   
				pAnim->drop();

				pNode->setMaterialType((irr::video::E_MATERIAL_TYPE)newMatrialType);
				pNode->setMaterialTexture(0,pVideo->getTexture("dotnetback.jpg"));
				//	pNode->setMaterialFlag(irr::video::EMF_BILINEAR_FILTER,false);
				//pNode->setMaterialTexture(1,pVideo->getTexture("spheremap.jpg"));
			}

			while(pDevice->run())
			{		
				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));		
				pSmgr->drawAll();
				pGuiEnv->drawAll();			
				pVideo->endScene();		
			}
			pDevice->drop();
		}
	}


	//깜박이 쉐이더
	namespace _01 {

		class ShaderCB :  public irr::video::IShaderConstantSetCallBack
		{
		public:
			irr::IrrlichtDevice *m_pDevice;

			virtual void OnSetConstants(irr::video::IMaterialRendererServices *services,irr::s32 userData)
			{
				irr::video::IVideoDriver *driver = services->getVideoDriver();

				irr::core::matrix4 matWVP;

				matWVP = driver->getTransform(irr::video::ETS_PROJECTION);
				matWVP *= driver->getTransform(irr::video::ETS_VIEW);
				matWVP *= driver->getTransform(irr::video::ETS_WORLD);

				services->setVertexShaderConstant("matWVP",matWVP.pointer(),16);

				irr::f32 fTime;
				fTime = ((irr::f32)(m_pDevice->getTimer()->getRealTime())) / 1000.f;
				services->setVertexShaderConstant("time",&fTime,1); //시간값 넘겨주기
			}
		};


		void main()
		{
			irr::IrrlichtDevice *m_pDevice;

			irr::video::IVideoDriver *m_pVideo;
			irr::scene::ISceneManager *m_pSmgr;
			irr::gui::IGUIEnvironment *m_pGuiEnv;

			irr::s32 m_newMatrialType;

			m_pDevice = irr::createDevice(     
				irr::video::EDT_DIRECT3D9,
				irr::core::dimension2du(640,480), 32								
				);
			m_pVideo = m_pDevice->getVideoDriver();
			m_pSmgr = m_pDevice->getSceneManager();
			m_pGuiEnv = m_pDevice->getGUIEnvironment(); 

			m_pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			//scene inittialize here

			irr::scene::ICameraSceneNode *pCam = 
				m_pSmgr->addCameraSceneNode();
			pCam->setPosition(irr::core::vector3df(0,20,-150));
			pCam->setTarget(irr::core::vector3df(0,0,0));
			pCam->setName("usr/scene/cam/1");

			ShaderCB shaderCB;
			shaderCB.m_pDevice = m_pDevice;
			{

				irr::c8 *ShaderFileName = "shader/exam2_1.hlsl"; //깜박이쉐이더 로딩

				irr::video::IGPUProgrammingServices *gpu =
					m_pVideo->getGPUProgrammingServices();

				m_newMatrialType = gpu->addHighLevelShaderMaterialFromFiles(
					ShaderFileName,"vsMain",irr::video::EVST_VS_2_0,//버텍쉐이더
					ShaderFileName,"psMain",irr::video::EPST_PS_2_0,
					&shaderCB
					);
			}


			{
				irr::scene::ISceneNode *pNode =
					m_pSmgr->addSphereSceneNode(20);
				pNode->setPosition(irr::core::vector3df(0,0,0));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setName("usr/scene/sphere/1");

				irr::scene::ISceneNodeAnimator *pAnim =
					m_pSmgr->createRotationAnimator(irr::core::vector3df(0.f,0.1f,0.f));
				pNode->addAnimator(pAnim);   
				pAnim->drop();

				pNode->setMaterialType((irr::video::E_MATERIAL_TYPE)m_newMatrialType);
			}

			while(m_pDevice->run())
			{		
				m_pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));		
				m_pSmgr->drawAll();
				m_pGuiEnv->drawAll();			
				m_pVideo->endScene();		
			}
			m_pDevice->drop();
		}
	}

	//multi textureing
	namespace _02 {
		class Jga_App :  public irr::IEventReceiver,
			public irr::video::IShaderConstantSetCallBack
		{
		public:

			irr::IrrlichtDevice *m_pDevice;

			irr::video::IVideoDriver *m_pVideo;
			irr::scene::ISceneManager *m_pSmgr;
			irr::gui::IGUIEnvironment *m_pGuiEnv;

			irr::s32 m_newMatrialType;

			Jga_App ()
			{
				m_pDevice = irr::createDevice(     
					irr::video::EDT_DIRECT3D9,
					irr::core::dimension2du(640,480), 32,
					false, false, true,
					this
					);
				m_pVideo = m_pDevice->getVideoDriver();
				m_pSmgr = m_pDevice->getSceneManager();
				m_pGuiEnv = m_pDevice->getGUIEnvironment(); 

				m_pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

				//scene inittialize here

				irr::scene::ICameraSceneNode *pCam = 
					m_pSmgr->addCameraSceneNode();
				pCam->setPosition(irr::core::vector3df(0,20,-150));
				pCam->setTarget(irr::core::vector3df(0,0,0));
				pCam->setName("usr/scene/cam/1");

				{

					irr::c8 *ShaderFileName = "shader/exam2_2.hlsl";

					irr::video::IGPUProgrammingServices *gpu =
						m_pVideo->getGPUProgrammingServices();

					m_newMatrialType = gpu->addHighLevelShaderMaterialFromFiles(
						ShaderFileName,"vsMain",irr::video::EVST_VS_2_0,//버텍쉐이더
						ShaderFileName,"psMain",irr::video::EPST_PS_2_0,
						this
						);
				}

				{
					irr::scene::ISceneNode *pNode =
						m_pSmgr->addSphereSceneNode(20);
					pNode->setPosition(irr::core::vector3df(0,0,0));
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
					pNode->setName("usr/scene/sphere/1");

					pNode->setMaterialTexture(0,m_pVideo->getTexture("wall.bmp"));
					pNode->setMaterialTexture(1,m_pVideo->getTexture("wall.jpg"));
					pNode->setMaterialTexture(2,m_pVideo->getTexture("Particle.tga"));

					irr::scene::ISceneNodeAnimator *pAnim =
						m_pSmgr->createRotationAnimator(irr::core::vector3df(0,0.1f,0));
					pNode->addAnimator(pAnim);   
					pAnim->drop();

					pNode->setMaterialType((irr::video::E_MATERIAL_TYPE)m_newMatrialType);
				}
			}

			~Jga_App ()
			{
				m_pDevice->drop();
			} 

			virtual void OnSetConstants(irr::video::IMaterialRendererServices *services,irr::s32 userData)
			{
				irr::video::IVideoDriver *driver = services->getVideoDriver();

				irr::core::matrix4 matWVP;

				matWVP = driver->getTransform(irr::video::ETS_PROJECTION);
				matWVP *= driver->getTransform(irr::video::ETS_VIEW);
				matWVP *= driver->getTransform(irr::video::ETS_WORLD);

				services->setVertexShaderConstant("matWVP",matWVP.pointer(),16);		

			}

			//이벤트 핸들러
			virtual bool OnEvent(const irr::SEvent& event)
			{
				switch(event.EventType)
				{
				case irr::EET_GUI_EVENT:
					{

					}
					break;
				case irr::EET_KEY_INPUT_EVENT:
					{
					}
					break;
				case irr::EET_MOUSE_INPUT_EVENT:
					{

					}
					break;
				case irr::EET_USER_EVENT:
					break;
				}
				return false;
			}

			void Update()
			{

			}

		};

		void main()
		{
			Jga_App App;

			while(App.m_pDevice->run())
			{ 

				App.Update();

				App.m_pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				App.m_pSmgr->drawAll();
				App.m_pGuiEnv->drawAll();

				App.m_pVideo->endScene();
			}
		}
	}

	//뚱보 쉐이더
	namespace _03
	{
		class Jga_App :  public irr::IEventReceiver,
			public irr::video::IShaderConstantSetCallBack
		{
		public:

			irr::IrrlichtDevice *m_pDevice;

			irr::video::IVideoDriver *m_pVideo;
			irr::scene::ISceneManager *m_pSmgr;
			irr::gui::IGUIEnvironment *m_pGuiEnv;

			irr::s32 m_newMatrialType;

			Jga_App ()
			{
				m_pDevice = irr::createDevice(     
					irr::video::EDT_DIRECT3D9,
					irr::core::dimension2du(640,480), 32,
					false, false, true,
					this
					);
				m_pVideo = m_pDevice->getVideoDriver();
				m_pSmgr = m_pDevice->getSceneManager();
				m_pGuiEnv = m_pDevice->getGUIEnvironment(); 

				m_pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

				//scene inittialize here

				irr::scene::ICameraSceneNode *pCam = 
					m_pSmgr->addCameraSceneNode();
				pCam->setPosition(irr::core::vector3df(0,20,-100));
				pCam->setTarget(irr::core::vector3df(0,0,0));
				pCam->setName("usr/scene/cam/1");

				{

					irr::c8 *ShaderFileName = "shader/exam2_3.hlsl";

					irr::video::IGPUProgrammingServices *gpu =
						m_pVideo->getGPUProgrammingServices();

					m_newMatrialType = gpu->addHighLevelShaderMaterialFromFiles(
						ShaderFileName,"vsMain",irr::video::EVST_VS_2_0,//버텍쉐이더
						ShaderFileName,"psMain",irr::video::EPST_PS_2_0,
						this
						);
				}


				{
					irr::scene::IAnimatedMesh *pAniMesh =
						m_pSmgr->getMesh("sydney.md2");
					
					irr::scene::IAnimatedMeshSceneNode *pNode =
						m_pSmgr->addAnimatedMeshSceneNode(pAniMesh);

					pNode->setPosition(irr::core::vector3df(0,0,0));
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
					pNode->setName("usr/scene/md2/1");
					
					pNode->setAnimationSpeed(30.f);
					pNode->setMD2Animation(irr::scene::EMAT_STAND);			

					pNode->setMaterialTexture(0,m_pVideo->getTexture("sydney.bmp"));

					irr::scene::ISceneNodeAnimator *pAnim =
						m_pSmgr->createRotationAnimator(irr::core::vector3df(0,0.3f,0));
					pNode->addAnimator(pAnim);   
					pAnim->drop();

					pNode->setMaterialType((irr::video::E_MATERIAL_TYPE)m_newMatrialType);
				}
			}

			~Jga_App ()
			{
				m_pDevice->drop();
			} 

			virtual void OnSetConstants(irr::video::IMaterialRendererServices *services,irr::s32 userData)
			{
				irr::video::IVideoDriver *driver = services->getVideoDriver();

				irr::core::matrix4 matWVP;

				matWVP = driver->getTransform(irr::video::ETS_PROJECTION);
				matWVP *= driver->getTransform(irr::video::ETS_VIEW);
				matWVP *= driver->getTransform(irr::video::ETS_WORLD);

				services->setVertexShaderConstant("matWVP",matWVP.pointer(),16);

				irr::f32 fTime;
				fTime = ((irr::f32)(m_pDevice->getTimer()->getRealTime())) / 1000.f;
				services->setVertexShaderConstant("time",&fTime,1); //시간값 넘겨주기		

			}

			//이벤트 핸들러
			virtual bool OnEvent(const irr::SEvent& event)
			{
				switch(event.EventType)
				{
				case irr::EET_GUI_EVENT:
					{

					}
					break;
				case irr::EET_KEY_INPUT_EVENT:
					{
					}
					break;
				case irr::EET_MOUSE_INPUT_EVENT:
					{

					}
					break;
				case irr::EET_USER_EVENT:
					break;
				}
				return false;
			}

			void Update()
			{

			}

		};
		void main()
		{
			Jga_App App;

			while(App.m_pDevice->run())
			{ 

				App.Update();

				App.m_pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				App.m_pSmgr->drawAll();
				App.m_pGuiEnv->drawAll();

				App.m_pVideo->endScene();
			}
		}


	}

	//난반사광 쉐이더
	namespace _04
	{
		class Jga_App :  public irr::IEventReceiver,
			public irr::video::IShaderConstantSetCallBack
		{
		public:

			irr::IrrlichtDevice *m_pDevice;

			irr::video::IVideoDriver *m_pVideo;
			irr::scene::ISceneManager *m_pSmgr;
			irr::gui::IGUIEnvironment *m_pGuiEnv;

			irr::s32 m_newMatrialType;

			Jga_App ()
			{
				m_pDevice = irr::createDevice(     
					irr::video::EDT_DIRECT3D9,
					irr::core::dimension2du(640,480), 32,
					false, false, true,
					this
					);
				m_pVideo = m_pDevice->getVideoDriver();
				m_pSmgr = m_pDevice->getSceneManager();
				m_pGuiEnv = m_pDevice->getGUIEnvironment(); 

				m_pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

				//scene inittialize here

				irr::scene::ICameraSceneNode *pCam = 
					m_pSmgr->addCameraSceneNode();
				pCam->setPosition(irr::core::vector3df(0,20,-150));
				pCam->setTarget(irr::core::vector3df(0,0,0));
				pCam->setName("usr/scene/cam/1");

				{

					irr::c8 *ShaderFileName = "shader/exam2_4.hlsl";

					irr::video::IGPUProgrammingServices *gpu =
						m_pVideo->getGPUProgrammingServices();

					m_newMatrialType = gpu->addHighLevelShaderMaterialFromFiles(
						ShaderFileName,"vsMain",irr::video::EVST_VS_2_0,//버텍쉐이더
						ShaderFileName,"psMain",irr::video::EPST_PS_2_0,
						this
						);
				}


				{
					irr::scene::ISceneNode *pNode =
						m_pSmgr->addSphereSceneNode(20);
					pNode->setPosition(irr::core::vector3df(0,0,0));
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
					pNode->setName("usr/scene/sphere/1");
					pNode->setMaterialTexture(0,m_pVideo->getTexture("wall.jpg"));

					irr::scene::ISceneNodeAnimator *pAnim =
						m_pSmgr->createRotationAnimator(irr::core::vector3df(0,0.1f,0));
					pNode->addAnimator(pAnim);   
					pAnim->drop();

					pNode->setMaterialType((irr::video::E_MATERIAL_TYPE)m_newMatrialType);
				}
			}

			~Jga_App ()
			{
				m_pDevice->drop();
			} 

			virtual void OnSetConstants(irr::video::IMaterialRendererServices *services,irr::s32 userData)
			{
				irr::video::IVideoDriver *driver = services->getVideoDriver();

				irr::core::matrix4 matWVP;

				matWVP = driver->getTransform(irr::video::ETS_PROJECTION);
				matWVP *= driver->getTransform(irr::video::ETS_VIEW);
				matWVP *= driver->getTransform(irr::video::ETS_WORLD);

				services->setVertexShaderConstant("matWVP",matWVP.pointer(),16);

				irr::core::matrix4 matWIT;

				//역전치행렬
				matWIT = driver->getTransform(irr::video::ETS_WORLD);
				matWIT.makeInverse();
				matWIT = matWIT.getTransposed();
				services->setVertexShaderConstant("matWIT",matWIT.pointer(),16);

				//방향성 라이트
				irr::core::vector3df vLightDir = irr::core::vector3df(1,1,0);
				services->setVertexShaderConstant("vLightDir",(irr::f32*)&vLightDir,3);

				//광원밝기
				{
					irr::video::SColorf I_A = irr::video::SColorf(.3f,.3f,.3f,0); 
					services->setVertexShaderConstant("I_a",(irr::f32*)&I_A,4);

					irr::video::SColorf I_D = irr::video::SColorf(.7f,.7f,.7f,.7f); 
					services->setVertexShaderConstant("I_d",(irr::f32*)&I_D,4);
				}

				//반사율
				{
					irr::video::SColorf K_A = irr::video::SColorf(1,1,1,1); 
					services->setVertexShaderConstant("K_a",(irr::f32*)&K_A,4);

					irr::video::SColorf K_D = irr::video::SColorf(1,1,1,1); 
					services->setVertexShaderConstant("K_d",(irr::f32*)&K_D,4);
				}
			}

			//이벤트 핸들러
			virtual bool OnEvent(const irr::SEvent& event)
			{
				switch(event.EventType)
				{
				case irr::EET_GUI_EVENT:
					{

					}
					break;
				case irr::EET_KEY_INPUT_EVENT:
					{
					}
					break;
				case irr::EET_MOUSE_INPUT_EVENT:
					{

					}
					break;
				case irr::EET_USER_EVENT:
					break;
				}
				return false;
			}

			void Update()
			{

			}

		};

		void main()
		{
			Jga_App App;

			while(App.m_pDevice->run())
			{ 

				App.Update();

				App.m_pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				App.m_pSmgr->drawAll();
				App.m_pGuiEnv->drawAll();

				App.m_pVideo->endScene();
			}
		}
	}

	//정반사광 쉐이더
	namespace _05
	{
		class Jga_App :  public irr::IEventReceiver,
			public irr::video::IShaderConstantSetCallBack
		{
		public:

			irr::IrrlichtDevice *m_pDevice;

			irr::video::IVideoDriver *m_pVideo;
			irr::scene::ISceneManager *m_pSmgr;
			irr::gui::IGUIEnvironment *m_pGuiEnv;

			irr::s32 m_newMatrialType;

			Jga_App ()
			{
				m_pDevice = irr::createDevice(     
					irr::video::EDT_DIRECT3D9,
					irr::core::dimension2du(640,480), 32,
					false, false, true,
					this
					);
				m_pVideo = m_pDevice->getVideoDriver();
				m_pSmgr = m_pDevice->getSceneManager();
				m_pGuiEnv = m_pDevice->getGUIEnvironment(); 

				m_pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res/");

				//scene inittialize here

				irr::scene::ICameraSceneNode *pCam = 
					m_pSmgr->addCameraSceneNode();
				pCam->setPosition(irr::core::vector3df(0,20,-100));
				pCam->setTarget(irr::core::vector3df(0,0,0));
				pCam->setName("usr/scene/cam/1");

				{

					irr::c8 *ShaderFileName = "shader/exam2_5.hlsl";

					irr::video::IGPUProgrammingServices *gpu =
						m_pVideo->getGPUProgrammingServices();

					m_newMatrialType = gpu->addHighLevelShaderMaterialFromFiles(
						ShaderFileName,"vsMain",irr::video::EVST_VS_2_0,//버텍쉐이더
						ShaderFileName,"psMain",irr::video::EPST_PS_2_0,
						this
						);
				}


				{
					irr::scene::ISceneNode *pNode =
						m_pSmgr->addSphereSceneNode(20);
					pNode->setPosition(irr::core::vector3df(0,0,0));
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
					pNode->setName("usr/scene/sphere/1");
					pNode->setMaterialTexture(0,m_pVideo->getTexture("wall.bmp"));

					irr::scene::ISceneNodeAnimator *pAnim =
						m_pSmgr->createRotationAnimator(irr::core::vector3df(0,0.1f,0));
					pNode->addAnimator(pAnim);   
					pAnim->drop();

					pNode->setMaterialType((irr::video::E_MATERIAL_TYPE)m_newMatrialType);
				}
			}

			~Jga_App ()
			{
				m_pDevice->drop();
			} 

			virtual void OnSetConstants(irr::video::IMaterialRendererServices *services,irr::s32 userData)
			{
				irr::video::IVideoDriver *driver = services->getVideoDriver();

				irr::core::matrix4 matWVP;

				matWVP = driver->getTransform(irr::video::ETS_PROJECTION);
				matWVP *= driver->getTransform(irr::video::ETS_VIEW);
				matWVP *= driver->getTransform(irr::video::ETS_WORLD);

				services->setVertexShaderConstant("matWVP",matWVP.pointer(),16);

				{
					//이렇게 미리 계산해서 넘겨주면 퍼포먼스가 향상된다.
					irr::core::vector3df vLightDir = irr::core::vector3df(0.577f,0.577f,0.577f);
					irr::core::vector3df v;
					irr::core::matrix4 m;			
					m = driver->getTransform(irr::video::ETS_WORLD);
					m.makeInverse();

					m.transformVect(v,vLightDir);
					v.normalize(); //로컬좌표계로 변환			

					services->setVertexShaderConstant("vLightDir",(irr::f32*)&v,3);
				}

				//시점
				{
					irr::core::matrix4 m;
					irr::core::vector3df v(0,0,0);

					m = driver->getTransform(irr::video::ETS_VIEW);
					m *= driver->getTransform(irr::video::ETS_WORLD);

					m.makeInverse();

					m.transformVect(v);

					services->setVertexShaderConstant("vEyePos",(irr::f32*)&v,3);
				}


				//광원밝기
				{
					irr::video::SColorf I_A = irr::video::SColorf(.3f,.3f,.3f,0); 
					services->setVertexShaderConstant("I_a",(irr::f32*)&I_A,4);

					irr::video::SColorf I_D = irr::video::SColorf(.7f,.7f,.7f,.7f); 
					services->setVertexShaderConstant("I_d",(irr::f32*)&I_D,4);

					irr::video::SColorf I_S = irr::video::SColorf(.7f,.7f,.7f,.7f); 
					services->setVertexShaderConstant("I_s",(irr::f32*)&I_S,4);
				}

				//반사율
				{
					irr::video::SColorf K_A = irr::video::SColorf(1,1,1,1); 
					services->setVertexShaderConstant("K_a",(irr::f32*)&K_A,4);

					irr::video::SColorf K_D = irr::video::SColorf(1,1,1,1); 
					services->setVertexShaderConstant("K_d",(irr::f32*)&K_D,4);

					irr::video::SColorf K_S = irr::video::SColorf(1,1,1,1); 
					services->setVertexShaderConstant("K_s",(irr::f32*)&K_S,4);
				}

				{
					irr::f32 fPower = 32;
					services->setVertexShaderConstant("fPower",(irr::f32*)&fPower,1);
				}

			}

			//이벤트 핸들러
			virtual bool OnEvent(const irr::SEvent& event)
			{
				switch(event.EventType)
				{
				case irr::EET_GUI_EVENT:
					{

					}
					break;
				case irr::EET_KEY_INPUT_EVENT:
					{
					}
					break;
				case irr::EET_MOUSE_INPUT_EVENT:
					{

					}
					break;
				case irr::EET_USER_EVENT:
					break;
				}
				return false;
			}

			void Update()
			{

			}

		};

		void main()
		{
			Jga_App App;

			while(App.m_pDevice->run())
			{ 

				App.Update();

				App.m_pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				App.m_pSmgr->drawAll();
				App.m_pGuiEnv->drawAll();

				App.m_pVideo->endScene();
			}
		}


	}

	//포스트 프로세싱용 쿼드 출력예제
	namespace _10 {

		void drawQuad(irr::video::IVideoDriver *pVideo,irr::video::ITexture *pTexture,
			irr::video::SMaterial &matrial,
			irr::core::vector2df pos = irr::core::vector2df(0,0),
			irr::core::vector2df size = irr::core::vector2df(-1,-1), 
			irr::core::vector2df rotation = irr::core::vector2df(0,0)		
			)
		{
			//원본텍스춰크기로출력
			if(size == irr::core::vector2df(-1,-1))
			{			
				size.set((irr::f32)pTexture->getSize().Width,(irr::f32)pTexture->getSize().Height);
			}
			{
				//irr::video::ITexture *pTexture=0;
				//pTexture = testScreenQuad.rt[1];
				//크기가 1인 3D 평면 만들기
				irr::u16 triList[] = {0,1,2,0,2,3};
				irr::video::S3DVertex triVer[] =
				{
					irr::video::S3DVertex( .5f, .5f,0,  0,0,-1, 0xFFFFFFFF, 1,0),
					irr::video::S3DVertex( .5f,-.5f,0,  0,0,-1, 0xFFFFFFFF, 1,1),
					irr::video::S3DVertex(-.5f,-.5f,0,  0,0,-1, 0xFFFFFFFF, 0,1),
					irr::video::S3DVertex(-.5f, .5f,0,  0,0,-1, 0xFFFFFFFF, 0,0)
				};

				//월드변환
				{
					irr::core::vector2df zeroPoint(
						-((irr::f32)pVideo->getViewPort().getWidth()/2 - size.X/2),
						(irr::f32)pVideo->getViewPort().getHeight()/2 - size.Y/2
						);

					pos.X += zeroPoint.X;
					pos.Y = zeroPoint.Y - pos.Y;


					irr::core::matrix4 matWorld,matWorld_scl,matWorld_rot,matWorld_trn;

					//matWorld_trn.setTranslation(irr::core::vector3df(pos.X,pos.Y,0));
					matWorld_trn.setTranslation(irr::core::vector3df(pos.X,pos.Y,0));
					matWorld_rot.setRotationDegrees(irr::core::vector3df(rotation.X,rotation.Y,0));
					matWorld_scl.setScale(irr::core::vector3df(size.X,size.Y,0));

					matWorld = matWorld_trn * matWorld_rot * matWorld_scl;

					pVideo->setTransform(
						irr::video::ETS_WORLD,
						matWorld
						); 
				}

				//뷰매트릭스만들기
				{
					irr::core::matrix4 matView;
					matView.buildCameraLookAtMatrixLH(
						irr::core::vector3df(0,0,-100), //카메라위치
						irr::core::vector3df(0,0,0), //시점
						irr::core::vector3df(0,1,0) //업벡터
						);
					pVideo->setTransform(irr::video::ETS_VIEW,matView);
				}

				//직교투영 프로잭션 매트릭스
				{
					irr::core::matrix4 matPrj;
					matPrj.buildProjectionMatrixOrthoLH(
						(irr::f32)pVideo->getViewPort().getWidth(),
						(irr::f32)pVideo->getViewPort().getHeight(),1,256);
					pVideo->setTransform(irr::video::ETS_PROJECTION,matPrj);
				}

				//메트리얼설정
				{
					//irr::video::SMaterial m;
					//m.Lighting = false; //라이트를꺼야색이제데로나온다.
					//m.ZBuffer = false;										
					matrial.setTexture(0,pTexture);
					pVideo->setMaterial(matrial);
				}

				pVideo->drawIndexedTriangleList(triVer,4,triList,2);
			}
		}




		class ShaderCB :  public irr::video::IShaderConstantSetCallBack
		{
		public:

			virtual void OnSetConstants(irr::video::IMaterialRendererServices *services,irr::s32 userData)
			{
				irr::video::IVideoDriver *driver = services->getVideoDriver();
			}
		};

		void main()
		{
			irr::IrrlichtDevice *pDevice;

			irr::video::IVideoDriver *pVideo;
			irr::scene::ISceneManager *pSmgr;
			irr::gui::IGUIEnvironment *pGuiEnv;

			irr::s32 newMatrialType;


			//Jga_App ()

			pDevice = irr::createDevice(     
				irr::video::EDT_DIRECT3D9,
				irr::core::dimension2du(800,600),32
				);
			pVideo = pDevice->getVideoDriver();
			pSmgr = pDevice->getSceneManager();
			pGuiEnv = pDevice->getGUIEnvironment(); 

			//scene inittialize here

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			irr::scene::ICameraSceneNode *pCam = 
				pSmgr->addCameraSceneNode();
			pCam->setPosition(irr::core::vector3df(0,20,-150));
			pCam->setTarget(irr::core::vector3df(0,0,0));
			pCam->setName("usr/scene/cam/1");

			ShaderCB TestSCB; //쉐이더 콜백
			{
				irr::c8 *ShaderFileName = "shader/QuadDraw.hlsl"; //전체화면 사각형 그리기 쉐이더

				irr::video::IGPUProgrammingServices *gpu =
					pVideo->getGPUProgrammingServices();

				newMatrialType = gpu->addHighLevelShaderMaterialFromFiles(
					ShaderFileName,"vsMain",irr::video::EVST_VS_2_0,//버텍쉐이더
					ShaderFileName,"psMain",irr::video::EPST_PS_2_0,
					&TestSCB
					);			
			}	

			pVideo->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS,false);
			irr::video::ITexture *pTexture = pVideo->getTexture("jga/tex_test2.bmp");

			while(pDevice->run())
			{		
				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));		
				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				{
					irr::u16 triList[] = {0,1,2,0,2,3};
					irr::video::S3DVertex triVer[] =
					{
						irr::video::S3DVertex( 1.0f, 1.0f,0,  0,0,-1, 0xFFFFFFFF, 1,0),
						irr::video::S3DVertex( 1.0f,-1.0f,0,  0,0,-1, 0xFFFFFFFF, 1,1),
						irr::video::S3DVertex(-1.0f,-1.0f,0,  0,0,-1, 0xFFFFFFFF, 0,1),
						irr::video::S3DVertex(-1.0f, 1.0f,0,  0,0,-1, 0xFFFFFFFF, 0,0)
					};

					//메트리얼설정
					{
						irr::video::SMaterial m;
						m.Lighting = false; //라이트를꺼야색이제데로나온다.
						m.ZBuffer = false;						
						m.MaterialType = (irr::video::E_MATERIAL_TYPE)newMatrialType;
						m.setTexture(0,pTexture);

						//텍스춰 랩핑모드
						m.setFlag(irr::video::EMF_TEXTURE_WRAP,irr::video::ETC_CLAMP);

						//모든필터링제거
						m.setFlag(irr::video::EMF_BILINEAR_FILTER,false);
						m.setFlag(irr::video::EMF_TRILINEAR_FILTER,false);
						m.setFlag(irr::video::EMF_ANISOTROPIC_FILTER,false);

						pVideo->setMaterial(m);
					}

					pVideo->drawIndexedTriangleList(triVer,4,triList,2);
				}

				//{
				//	irr::video::SMaterial m;
				//	m.Lighting = false; //라이트를꺼야색이제데로나온다.

				//	//텍스춰 랩핑모드
				//	m.setFlag(irr::video::EMF_TEXTURE_WRAP,irr::video::ETC_CLAMP);

				//	//모든필터링제거
				//	m.setFlag(irr::video::EMF_BILINEAR_FILTER,false);
				//	m.setFlag(irr::video::EMF_TRILINEAR_FILTER,false);
				//	m.setFlag(irr::video::EMF_ANISOTROPIC_FILTER,false);

				//	m.setFlag(irr::video::EMF_ZBUFFER,false);

				//	drawQuad(pVideo,pTexture,m,
				//		irr::core::vector2df(0,0),										
				//		irr::core::vector2df(512,512)
				//		);
				//}
				pVideo->endScene();
			}
			pDevice->drop();

		}

	}



	//포스트 프로세싱용 쿼드 출력예제 2.
	//필터모드 설정예제
	//랩핑모드
	namespace _11 {

		void draw25D(irr::video::IVideoDriver *pVideo,irr::video::ITexture *pTexture,
			irr::core::vector2df pos = irr::core::vector2df(0,0),
			irr::core::vector2df size = irr::core::vector2df(-1,-1), 
			irr::core::vector2df rotation = irr::core::vector2df(0,0)		
			)
		{
			//원본텍스춰크기로출력
			if(size == irr::core::vector2df(-1,-1))
			{			
				size.set((irr::f32)pTexture->getSize().Width,(irr::f32)pTexture->getSize().Height);
			}
			{
				//irr::video::ITexture *pTexture=0;
				//pTexture = testScreenQuad.rt[1];
				//크기가 1인 3D 평면 만들기
				irr::u16 triList[] = {0,1,2,0,2,3};
				irr::video::S3DVertex triVer[] =
				{
					irr::video::S3DVertex( .5f, .5f,0,  0,0,-1, 0xFFFFFFFF, 1,0),
					irr::video::S3DVertex( .5f,-.5f,0,  0,0,-1, 0xFFFFFFFF, 1,1),
					irr::video::S3DVertex(-.5f,-.5f,0,  0,0,-1, 0xFFFFFFFF, 0,1),
					irr::video::S3DVertex(-.5f, .5f,0,  0,0,-1, 0xFFFFFFFF, 0,0)
				};

				//월드변환
				{
					irr::core::vector2df zeroPoint(
						-((irr::f32)pVideo->getViewPort().getWidth()/2 - size.X/2),
						(irr::f32)pVideo->getViewPort().getHeight()/2 - size.Y/2
						);

					pos.X += zeroPoint.X;
					pos.Y = zeroPoint.Y - pos.Y;


					irr::core::matrix4 matWorld,matWorld_scl,matWorld_rot,matWorld_trn;

					//matWorld_trn.setTranslation(irr::core::vector3df(pos.X,pos.Y,0));
					matWorld_trn.setTranslation(irr::core::vector3df(pos.X,pos.Y,0));
					matWorld_rot.setRotationDegrees(irr::core::vector3df(rotation.X,rotation.Y,0));
					matWorld_scl.setScale(irr::core::vector3df(size.X,size.Y,0));

					matWorld = matWorld_trn * matWorld_rot * matWorld_scl;

					pVideo->setTransform(
						irr::video::ETS_WORLD,
						matWorld
						); 
				}

				//뷰매트릭스만들기
				{
					irr::core::matrix4 matView;
					matView.buildCameraLookAtMatrixLH(
						irr::core::vector3df(0,0,-100), //카메라위치
						irr::core::vector3df(0,0,0), //시점
						irr::core::vector3df(0,1,0) //업벡터
						);
					pVideo->setTransform(irr::video::ETS_VIEW,matView);
				}

				//직교투영 프로잭션 매트릭스
				{
					irr::core::matrix4 matPrj;
					matPrj.buildProjectionMatrixOrthoLH(800,600,1,3000);
					pVideo->setTransform(irr::video::ETS_PROJECTION,matPrj);
				}

				//메트리얼설정
				{
					irr::video::SMaterial m;
					m.Lighting = false; //라이트를꺼야색이제데로나온다.
					m.ZBuffer = irr::video::ECFN_NEVER; //Z 버퍼 무시					
					m.setTexture(0,pTexture);

					//텍스춰 랩핑모드
					m.setFlag(irr::video::EMF_TEXTURE_WRAP,irr::video::ETC_CLAMP);

					//모든필터링제거
					m.setFlag(irr::video::EMF_BILINEAR_FILTER,false);
					m.setFlag(irr::video::EMF_TRILINEAR_FILTER,false);
					m.setFlag(irr::video::EMF_ANISOTROPIC_FILTER,false);

					pVideo->setMaterial(m);
				}

				pVideo->drawIndexedTriangleList(triVer,4,triList,2);
			}
		}



		class ShaderCB :  public irr::video::IShaderConstantSetCallBack
		{
		public:

			virtual void OnSetConstants(irr::video::IMaterialRendererServices *services,irr::s32 userData)
			{
				irr::video::IVideoDriver *driver = services->getVideoDriver();
			}
		};

		void main()
		{
			irr::IrrlichtDevice *pDevice;

			irr::video::IVideoDriver *pVideo;
			irr::scene::ISceneManager *pSmgr;
			irr::gui::IGUIEnvironment *pGuiEnv;

			irr::s32 newMatrialType;


			//Jga_App ()

			pDevice = irr::createDevice(     
				irr::video::EDT_DIRECT3D9,
				irr::core::dimension2du(640,480),32
				);
			pVideo = pDevice->getVideoDriver();
			pSmgr = pDevice->getSceneManager();
			pGuiEnv = pDevice->getGUIEnvironment(); 

			//scene inittialize here

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			irr::scene::ICameraSceneNode *pCam = 
				pSmgr->addCameraSceneNode();
			pCam->setPosition(irr::core::vector3df(0,20,-150));
			pCam->setTarget(irr::core::vector3df(0,0,0));
			pCam->setName("usr/scene/cam/1");

			ShaderCB TestSCB; //쉐이더 콜백
			{
				irr::c8 *ShaderFileName = "shader/QuadDraw.hlsl";

				irr::video::IGPUProgrammingServices *gpu =
					pVideo->getGPUProgrammingServices();

				newMatrialType = gpu->addHighLevelShaderMaterialFromFiles(
					ShaderFileName,"vsMain",irr::video::EVST_VS_2_0,//버텍쉐이더
					ShaderFileName,"psMain",irr::video::EPST_PS_2_0,
					&TestSCB
					);			
			}	

			irr::video::ITexture *pTexture = pVideo->getTexture("jga/tex_test.PNG");

			while(pDevice->run())
			{		
				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));		
				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				draw25D(pVideo,pTexture,irr::core::vector2df(10,10),irr::core::vector2df(256,256));

				pVideo->endScene();		
			}
			pDevice->drop();

		}

	}
}

