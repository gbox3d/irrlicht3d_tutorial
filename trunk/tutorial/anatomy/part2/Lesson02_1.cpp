#include <irrlicht.h>
#include <assert.h>

#pragma comment(lib,"irrlicht.lib")

//깜박이 쉐이더예제

namespace lesson02_1 {


class Jga_App :  public irr::IEventReceiver,
	public irr::video::IShaderConstantSetCallBack
{
public:

	irr::IrrlichtDevice *m_pDevice;

	irr::video::IVideoDriver *m_pVideo;
	irr::scene::ISceneManager *m_pSmgr;
	irr::gui::IGUIEnvironment *m_pGuiEnv;

	irr::s32 m_newMatrialType;

	Jga_App ()
	{
		m_pDevice = irr::createDevice(     
			irr::video::EDT_DIRECT3D9,
			irr::core::dimension2du(640,480), 32,
			false, false, true,
			this
			);
		m_pVideo = m_pDevice->getVideoDriver();
		m_pSmgr = m_pDevice->getSceneManager();
		m_pGuiEnv = m_pDevice->getGUIEnvironment(); 

		//scene inittialize here

		irr::scene::ICameraSceneNode *pCam = 
			m_pSmgr->addCameraSceneNode();
		pCam->setPosition(irr::core::vector3df(0,20,-150));
		pCam->setTarget(irr::core::vector3df(0,0,0));
		pCam->setName("usr/scene/cam/1");

		{

			irr::c8 *ShaderFileName = "../res/shader/exam2_1.hlsl"; //깜박이쉐이더 로딩

			irr::video::IGPUProgrammingServices *gpu =
				m_pVideo->getGPUProgrammingServices();

			m_newMatrialType = gpu->addHighLevelShaderMaterialFromFiles(
				ShaderFileName,"vsMain",irr::video::EVST_VS_2_0,//버텍쉐이더
				ShaderFileName,"psMain",irr::video::EPST_PS_2_0,
				this
				);
		}


		{
			irr::scene::ISceneNode *pNode =
				m_pSmgr->addSphereSceneNode(20);
			pNode->setPosition(irr::core::vector3df(0,0,0));
			pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
			pNode->setName("usr/scene/sphere/1");

			irr::scene::ISceneNodeAnimator *pAnim =
				m_pSmgr->createRotationAnimator(irr::core::vector3df(0.f,0.1f,0.f));
			pNode->addAnimator(pAnim);   
			pAnim->drop();

			pNode->setMaterialType((irr::video::E_MATERIAL_TYPE)m_newMatrialType);
		}
	}

	~Jga_App ()
	{
		m_pDevice->drop();
	} 

	virtual void OnSetConstants(irr::video::IMaterialRendererServices *services,irr::s32 userData)
	{
		irr::video::IVideoDriver *driver = services->getVideoDriver();

		irr::core::matrix4 matWVP;

		matWVP = driver->getTransform(irr::video::ETS_PROJECTION);
		matWVP *= driver->getTransform(irr::video::ETS_VIEW);
		matWVP *= driver->getTransform(irr::video::ETS_WORLD);

		services->setVertexShaderConstant("matWVP",matWVP.pointer(),16);
	
		irr::f32 fTime;
		fTime = ((irr::f32)(m_pDevice->getTimer()->getRealTime())) / 1000.f;
		services->setVertexShaderConstant("time",&fTime,1); //시간값 넘겨주기

	}

	//이벤트 핸들러
	virtual bool OnEvent(const irr::SEvent& event)
	{
		switch(event.EventType)
		{
		case irr::EET_GUI_EVENT:
			{

			}
			break;
		case irr::EET_KEY_INPUT_EVENT:
			{
			}
			break;
		case irr::EET_MOUSE_INPUT_EVENT:
			{

			}
			break;
		case irr::EET_USER_EVENT:
			break;
		}
		return false;
	}

	void Update()
	{

	}

};

}

using namespace lesson02_1;

void Lesson02_1()
{
	Jga_App App;

	while(App.m_pDevice->run())
	{ 

		App.Update();

		App.m_pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

		App.m_pSmgr->drawAll();
		App.m_pGuiEnv->drawAll();

		App.m_pVideo->endScene();
	}
}

