#include <irrlicht.h>
#include <assert.h>

#pragma comment(lib,"irrlicht.lib")

//확산광 쉐이더 예제

namespace lesson02_4 {


class Jga_App :  public irr::IEventReceiver,
	public irr::video::IShaderConstantSetCallBack
{
public:

	irr::IrrlichtDevice *m_pDevice;

	irr::video::IVideoDriver *m_pVideo;
	irr::scene::ISceneManager *m_pSmgr;
	irr::gui::IGUIEnvironment *m_pGuiEnv;

	irr::s32 m_newMatrialType;

	Jga_App ()
	{
		m_pDevice = irr::createDevice(     
			irr::video::EDT_DIRECT3D9,
			irr::core::dimension2du(640,480), 32,
			false, false, true,
			this
			);
		m_pVideo = m_pDevice->getVideoDriver();
		m_pSmgr = m_pDevice->getSceneManager();
		m_pGuiEnv = m_pDevice->getGUIEnvironment(); 

		//scene inittialize here

		irr::scene::ICameraSceneNode *pCam = 
			m_pSmgr->addCameraSceneNode();
		pCam->setPosition(irr::core::vector3df(0,20,-150));
		pCam->setTarget(irr::core::vector3df(0,0,0));
		pCam->setName("usr/scene/cam/1");

		{

			irr::c8 *ShaderFileName = "../res/shader/exam2_4.hlsl";

			irr::video::IGPUProgrammingServices *gpu =
				m_pVideo->getGPUProgrammingServices();

			m_newMatrialType = gpu->addHighLevelShaderMaterialFromFiles(
				ShaderFileName,"vsMain",irr::video::EVST_VS_2_0,//버텍쉐이더
				ShaderFileName,"psMain",irr::video::EPST_PS_2_0,
				this
				);
		}


		{
			irr::scene::ISceneNode *pNode =
				m_pSmgr->addSphereSceneNode(20);
			pNode->setPosition(irr::core::vector3df(0,0,0));
			pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
			pNode->setName("usr/scene/sphere/1");
			pNode->setMaterialTexture(0,m_pVideo->getTexture("../res/wall.jpg"));

			irr::scene::ISceneNodeAnimator *pAnim =
				m_pSmgr->createRotationAnimator(irr::core::vector3df(0,0.1f,0));
			pNode->addAnimator(pAnim);   
			pAnim->drop();

			pNode->setMaterialType((irr::video::E_MATERIAL_TYPE)m_newMatrialType);
		}
	}

	~Jga_App ()
	{
		m_pDevice->drop();
	} 

	virtual void OnSetConstants(irr::video::IMaterialRendererServices *services,irr::s32 userData)
	{
		irr::video::IVideoDriver *driver = services->getVideoDriver();

		irr::core::matrix4 matWVP;

		matWVP = driver->getTransform(irr::video::ETS_PROJECTION);
		matWVP *= driver->getTransform(irr::video::ETS_VIEW);
		matWVP *= driver->getTransform(irr::video::ETS_WORLD);

		services->setVertexShaderConstant("matWVP",matWVP.pointer(),16);

		irr::core::matrix4 matWIT;

		//역전치행렬
		matWIT = driver->getTransform(irr::video::ETS_WORLD);
		matWIT.makeInverse();
		matWIT = matWIT.getTransposed();
		services->setVertexShaderConstant("matWIT",matWIT.pointer(),16);

		//방향성 라이트
		irr::core::vector3df vLightDir = irr::core::vector3df(1,1,0);
		services->setVertexShaderConstant("vLightDir",(irr::f32*)&vLightDir,3);

		//광원밝기
		{
			irr::video::SColorf I_A = irr::video::SColorf(.3f,.3f,.3f,0); 
			services->setVertexShaderConstant("I_a",(irr::f32*)&I_A,4);

			irr::video::SColorf I_D = irr::video::SColorf(.7f,.7f,.7f,.7f); 
			services->setVertexShaderConstant("I_d",(irr::f32*)&I_D,4);
		}

		//반사율
		{
			irr::video::SColorf K_A = irr::video::SColorf(1,1,1,1); 
			services->setVertexShaderConstant("K_a",(irr::f32*)&K_A,4);

			irr::video::SColorf K_D = irr::video::SColorf(1,1,1,1); 
			services->setVertexShaderConstant("K_d",(irr::f32*)&K_D,4);
		}
	}

	//이벤트 핸들러
	virtual bool OnEvent(const irr::SEvent& event)
	{
		switch(event.EventType)
		{
		case irr::EET_GUI_EVENT:
			{

			}
			break;
		case irr::EET_KEY_INPUT_EVENT:
			{
			}
			break;
		case irr::EET_MOUSE_INPUT_EVENT:
			{

			}
			break;
		case irr::EET_USER_EVENT:
			break;
		}
		return false;
	}

	void Update()
	{

	}

};

}

using namespace lesson02_4;

void Lesson02_4()
{
	Jga_App App;

	while(App.m_pDevice->run())
	{ 

		App.Update();

		App.m_pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

		App.m_pSmgr->drawAll();
		App.m_pGuiEnv->drawAll();

		App.m_pVideo->endScene();
	}
}

////////////////////////////////////////////////////////////////

namespace lesson02_5 {


class Jga_App :  public irr::IEventReceiver,
	public irr::video::IShaderConstantSetCallBack
{
public:

	irr::IrrlichtDevice *m_pDevice;

	irr::video::IVideoDriver *m_pVideo;
	irr::scene::ISceneManager *m_pSmgr;
	irr::gui::IGUIEnvironment *m_pGuiEnv;

	irr::s32 m_newMatrialType;

	Jga_App ()
	{
		m_pDevice = irr::createDevice(     
			irr::video::EDT_DIRECT3D9,
			irr::core::dimension2du(640,480), 32,
			false, false, true,
			this
			);
		m_pVideo = m_pDevice->getVideoDriver();
		m_pSmgr = m_pDevice->getSceneManager();
		m_pGuiEnv = m_pDevice->getGUIEnvironment(); 

		m_pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res/");

		//scene inittialize here

		irr::scene::ICameraSceneNode *pCam = 
			m_pSmgr->addCameraSceneNode();
		pCam->setPosition(irr::core::vector3df(0,20,-100));
		pCam->setTarget(irr::core::vector3df(0,0,0));
		pCam->setName("usr/scene/cam/1");

		{

			irr::c8 *ShaderFileName = "shader/exam2_5.hlsl";

			irr::video::IGPUProgrammingServices *gpu =
				m_pVideo->getGPUProgrammingServices();

			m_newMatrialType = gpu->addHighLevelShaderMaterialFromFiles(
				ShaderFileName,"vsMain",irr::video::EVST_VS_2_0,//버텍쉐이더
				ShaderFileName,"psMain",irr::video::EPST_PS_2_0,
				this
				);
		}


		{
			irr::scene::ISceneNode *pNode =
				m_pSmgr->addSphereSceneNode(20);
			pNode->setPosition(irr::core::vector3df(0,0,0));
			pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
			pNode->setName("usr/scene/sphere/1");
			pNode->setMaterialTexture(0,m_pVideo->getTexture("wall.bmp"));

			irr::scene::ISceneNodeAnimator *pAnim =
				m_pSmgr->createRotationAnimator(irr::core::vector3df(0,0.1f,0));
			pNode->addAnimator(pAnim);   
			pAnim->drop();

			pNode->setMaterialType((irr::video::E_MATERIAL_TYPE)m_newMatrialType);
		}
	}

	~Jga_App ()
	{
		m_pDevice->drop();
	} 

	virtual void OnSetConstants(irr::video::IMaterialRendererServices *services,irr::s32 userData)
	{
		irr::video::IVideoDriver *driver = services->getVideoDriver();

		irr::core::matrix4 matWVP;

		matWVP = driver->getTransform(irr::video::ETS_PROJECTION);
		matWVP *= driver->getTransform(irr::video::ETS_VIEW);
		matWVP *= driver->getTransform(irr::video::ETS_WORLD);

		services->setVertexShaderConstant("matWVP",matWVP.pointer(),16);
		
		{
			//이렇게 미리 계산해서 넘겨주면 퍼포먼스가 향상된다.
			irr::core::vector3df vLightDir = irr::core::vector3df(0.577f,0.577f,0.577f);
			irr::core::vector3df v;
			irr::core::matrix4 m;			
			m = driver->getTransform(irr::video::ETS_WORLD);
			m.makeInverse();

			m.transformVect(v,vLightDir);
			v.normalize(); //로컬좌표계로 변환			

			services->setVertexShaderConstant("vLightDir",(irr::f32*)&v,3);
		}

		//시점
		{
			irr::core::matrix4 m;
			irr::core::vector3df v(0,0,0);

			m = driver->getTransform(irr::video::ETS_VIEW);
			m *= driver->getTransform(irr::video::ETS_WORLD);

			m.makeInverse();

			m.transformVect(v);

			services->setVertexShaderConstant("vEyePos",(irr::f32*)&v,3);
		}


		//광원밝기
		{
			irr::video::SColorf I_A = irr::video::SColorf(.3f,.3f,.3f,0); 
			services->setVertexShaderConstant("I_a",(irr::f32*)&I_A,4);

			irr::video::SColorf I_D = irr::video::SColorf(.7f,.7f,.7f,.7f); 
			services->setVertexShaderConstant("I_d",(irr::f32*)&I_D,4);

			irr::video::SColorf I_S = irr::video::SColorf(.7f,.7f,.7f,.7f); 
			services->setVertexShaderConstant("I_s",(irr::f32*)&I_S,4);
		}

		//반사율
		{
			irr::video::SColorf K_A = irr::video::SColorf(1,1,1,1); 
			services->setVertexShaderConstant("K_a",(irr::f32*)&K_A,4);

			irr::video::SColorf K_D = irr::video::SColorf(1,1,1,1); 
			services->setVertexShaderConstant("K_d",(irr::f32*)&K_D,4);

			irr::video::SColorf K_S = irr::video::SColorf(1,1,1,1); 
			services->setVertexShaderConstant("K_s",(irr::f32*)&K_S,4);
		}

		{
			irr::f32 fPower = 32;
			services->setVertexShaderConstant("fPower",(irr::f32*)&fPower,1);
		}
		
	}

	//이벤트 핸들러
	virtual bool OnEvent(const irr::SEvent& event)
	{
		switch(event.EventType)
		{
		case irr::EET_GUI_EVENT:
			{

			}
			break;
		case irr::EET_KEY_INPUT_EVENT:
			{
			}
			break;
		case irr::EET_MOUSE_INPUT_EVENT:
			{

			}
			break;
		case irr::EET_USER_EVENT:
			break;
		}
		return false;
	}

	void Update()
	{

	}

};

}




void Lesson02_5()
{
	lesson02_5::Jga_App App;

	while(App.m_pDevice->run())
	{ 

		App.Update();

		App.m_pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

		App.m_pSmgr->drawAll();
		App.m_pGuiEnv->drawAll();

		App.m_pVideo->endScene();
	}
}

