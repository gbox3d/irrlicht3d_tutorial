﻿#include <irrlicht.h>

//씬메니져 없이 비디오드라이버객체만으로 직접 그리기 기본 예제
#define WIN32_LEAN_AND_MEAN 

#include <windows.h>
#include <iostream>

using namespace std;

namespace Lesson03
{
	//비디오 드라이버 직접 다루기
	namespace _00
	{
		void main()
		{
			
			irr::core::vector3df 벡터1(0,0,0),벡터2(320,240,0);

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9,
				irr::core::dimension2du(640,480) 
				); 
			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver(); 

			while(pDevice->run())
			{
				pVideo->beginScene();

				{	
					pVideo->setTransform(
						irr::video::ETS_WORLD,
						irr::core::IdentityMatrix
						); //변환초기화

					//뷰매트릭스만들기
					irr::core::matrix4 matView;
					matView.buildCameraLookAtMatrixLH(
						irr::core::vector3df(0,0,-100), //카메라위치
						irr::core::vector3df(0,0,0), //시점
						irr::core::vector3df(0,1,0) //업벡터
						);
					pVideo->setTransform(irr::video::ETS_VIEW,matView);

					//프로잭션 메트릭스
					irr::core::matrix4 matPrj;
					matPrj.buildProjectionMatrixOrthoLH(640,480,1,3000);
					pVideo->setTransform(irr::video::ETS_PROJECTION,matPrj);

					//메트리얼설정
					irr::video::SMaterial m;
					m.Lighting = false; //라이트를꺼야색이제데로나온다.
					m.ZBuffer = false;
					pVideo->setMaterial(m);

					pVideo->draw3DLine(
						벡터1,
						벡터2,    
						irr::video::SColor(0,0,255,0));   
					pVideo->draw3DLine(
						벡터1,
						-벡터2,    
						irr::video::SColor(0,0,0,255));   
				}

				pVideo->endScene();
			}
			pDevice->drop();
		}
	}



	//박스안에 삼각형이 들어있는지 검사하기
	namespace _01
	{
		//삼각형이 박스 안에 있는지 여부검사
		irr::core::vector3df RandVector()
		{
			irr::core::vector3df vt;

			vt.X =  (rand()% 200) / 100.f -1.f;
			vt.Y =  (rand()% 200) / 100.f -1.f;
			vt.Z =  0;//(rand()% 200) / 200.f -.5f;
			return vt;
		}

		const int MAXTRIANGLE = 1;

		void main()//Lesson03_1()
		{

			irr::core::vector3df vt1(0,0,0),vt2(0.3f,.3f,0);
			irr::core::vector3df vt3(0.1f,0.2f,0);	

			irr::core::triangle3df Test_tri[MAXTRIANGLE];	

			irr::core::aabbox3df test_box(
				irr::core::vector3df(-.8f,-.8f,-.8f),
				irr::core::vector3df(.8f,.8f,.8f)

				);	

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9,
				irr::core::dimension2du(640,480) 
				); 
			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver(); 

			srand(pDevice->getTimer()->getTime());				

			while(pDevice->run())
			{
				pVideo->beginScene();

				if(::GetAsyncKeyState(VK_SPACE) & 0x8000)
				{				
					int i;
					for(i=0;i<MAXTRIANGLE;i++)
					{
						Test_tri[i].set(
							RandVector(),
							RandVector(),
							RandVector());
					}
				}

				{	
					pVideo->setTransform(
						irr::video::ETS_WORLD,
						irr::core::IdentityMatrix
						); //월드 변환초기화			

					//메트리얼설정
					irr::video::SMaterial m;
					m.Lighting = false; //라이트를꺼야색이제데로나온다.
					m.ZBuffer = false;
					pVideo->setMaterial(m);

					pVideo->draw3DBox(test_box);

					{
						int i;
						for(i=0;i<MAXTRIANGLE;i++)
						{
							if(Test_tri[i].isTotalInsideBox(test_box))//박스안에들어오면...	
								pVideo->draw3DTriangle(Test_tri[i],irr::video::SColor(0,0,0,255));		
							else
								pVideo->draw3DTriangle(Test_tri[i],irr::video::SColor(0,0,255,255));

						}
					}
				}	

				pVideo->endScene();

			}
			pDevice->drop();
		}
	}

	//씬메니져 없이 비디오드라이버객체만으로 직접 그리기 기본 예제
	//프로잭션 메트릭스 직접설정예제
	//aspect ratio(시야각) 테스트 예제
	//정비율을 얻기 위해0서는 시야각을 (화면넓이화소수 / 화면높이화소수) 로 해줘야한다.

	namespace _02
	{
		void main()//Lesson03_2()
		{	

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9,
				irr::core::dimension2du(640,480) 
				); 
			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver(); 

			//pDevice->getSceneManager()->addCameraSceneNode();


			while(pDevice->run())
			{
				pVideo->beginScene();

				{	
					static irr::f32 fov = 1.f;
					static irr::u32 uLastTick;
					irr::u32 deltaTick =  pDevice->getTimer()->getTime() - uLastTick;
					uLastTick = pDevice->getTimer()->getTime();			

					if(::GetAsyncKeyState(VK_LEFT) & 0x8000)			
					{
						fov += (.5f* deltaTick/1000.f);
						printf("%f \n",fov);
					}
					if(::GetAsyncKeyState(VK_RIGHT) & 0x8000)
					{
						fov -= (.5f* deltaTick/1000.f);
						printf("%f \n",fov);
					}

					if(fov > irr::core::PI)
					{
						fov = irr::core::PI;
					}

					if(fov < -irr::core::PI)
					{
						fov = -irr::core::PI;
					}


					pVideo->setTransform(
						irr::video::ETS_WORLD,
						irr::core::IdentityMatrix
						); //변환초기화

					//뷰매트릭스만들기
					irr::core::matrix4 matView;
					matView.buildCameraLookAtMatrixLH(
						irr::core::vector3df(0,0,-100), //카메라위치
						irr::core::vector3df(0,0,0), //시점
						irr::core::vector3df(0,1,0) //업벡터
						);
					pVideo->setTransform(irr::video::ETS_VIEW,matView);

					//프로잭션 메트릭스
					irr::core::matrix4 matPrj;
					matPrj.buildProjectionMatrixPerspectiveFovLH(fov,640.f/480.f,1,3000);
					pVideo->setTransform(irr::video::ETS_PROJECTION,matPrj);			

					//메트리얼설정
					irr::video::SMaterial m;
					m.Lighting = false; //라이트를꺼야색이제데로나온다.
					m.ZBuffer = false;
					pVideo->setMaterial(m);


					{
						irr::core::aabbox3df box1(irr::core::vector3df(-1,-1,-1),irr::core::vector3df(1,1,1));
						irr::core::matrix4 mat;
						mat.makeIdentity();
						mat.setScale(irr::core::vector3df(10.f,10.f,10.f));
						mat.transformBox(box1);
						pVideo->draw3DBox(box1);
					}

					{
						irr::core::aabbox3df box1(irr::core::vector3df(-1,-1,-1),irr::core::vector3df(1,1,1));
						irr::core::matrix4 mat;
						mat.makeIdentity();
						mat.setScale(irr::core::vector3df(5.f,10.f,5.f));
						mat.setTranslation(irr::core::vector3df(10.f,0.f,-20.f));

						mat.transformBox(box1);
						pVideo->draw3DBox(box1);
					}

					{
						irr::core::aabbox3df box1(irr::core::vector3df(-1,-1,-1),irr::core::vector3df(1,1,1));
						irr::core::matrix4 mat;
						mat.makeIdentity();
						mat.setScale(irr::core::vector3df(10.f,20.f,10.f));
						mat.setTranslation(irr::core::vector3df(-20.f,0.f,-15.f));

						mat.transformBox(box1);
						pVideo->draw3DBox(box1);
					}
				}

				pVideo->endScene();
			}
			pDevice->drop();
		}




	}


	//씬메니져 없이 비디오드라이버객체만으로 직접 그리기 기본 예제
	//프로잭션 메트릭스 직접설정예제
	//aspect ratio(시야각) 테스트 예제 2번째 

	/*
	3차원 자표계를 2차원 좌표계로 바꿔주는 변환
	실제로는 원근 투영기법이 많이 사용된다.
	거리에따라 투영되는 비율이달라지는 기법.


	buildProjectionMatrixPerspectiveLH(
	f32 widthOfViewVolume, //투영평면(카메라 절두체의near 평면)의 넓이
	f32 heightOfViewVolume, //투영평면의 높이
	f32 zNear, f32 zFar)

	*/
	namespace _03
	{
		void main()//Lesson03_3()
		{	

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9,
				irr::core::dimension2du(640,480) 
				); 
			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver(); 


			while(pDevice->run())
			{
				pVideo->beginScene();

				{	

					static irr::f32 fovX = 1.f;
					static irr::f32 fovY = 1.f;
					static irr::u32 uLastTick;

					irr::u32 deltaTick =  pDevice->getTimer()->getTime() - uLastTick;
					uLastTick = pDevice->getTimer()->getTime();

					//조이스틱처럼 입력처리
					//irr::f32 fmoX = pDevice->getCursorControl()->getRelativePosition().X;
					//irr::f32 fmoY = pDevice->getCursorControl()->getRelativePosition().Y;

					//if(fmoX > 0.8f)
					if(::GetAsyncKeyState(VK_LEFT) & 0x8000)			
					{
						fovX += (.5f* deltaTick/1000.f);
						printf("fovX : %f \n",fovX);
					}

					//if(fmoX < 0.2f)
					if(::GetAsyncKeyState(VK_RIGHT) & 0x8000)			
					{
						fovX -= (.5f* deltaTick/1000.f);
						printf("fovX : %f \n",fovX);
					}

					//if(fmoY > 0.8f)
					if(::GetAsyncKeyState(VK_UP) & 0x8000)			
					{
						fovY += (.5f* deltaTick/1000.f);
						printf("fovY : %f \n",fovY);
					}

					//if(fmoY < 0.2f)
					if(::GetAsyncKeyState(VK_DOWN) & 0x8000)			
					{
						fovY -= (.5f* deltaTick/1000.f);
						printf("fovY: %f \n",fovY);
					}

					if(fovX > irr::core::PI)
					{
						fovX = irr::core::PI;
					}

					if(fovX < 0)
					{
						fovX = 0;
					}

					//월드변환
					{
						pVideo->setTransform(
							irr::video::ETS_WORLD,
							irr::core::IdentityMatrix
							); //변환초기화
					}

					//뷰매트릭스만들기
					{
						irr::core::matrix4 matView;
						matView.buildCameraLookAtMatrixLH(
							irr::core::vector3df(0,0,-100), //카메라위치
							irr::core::vector3df(0,0,0), //시점
							irr::core::vector3df(0,1,0) //업벡터
							);
						pVideo->setTransform(irr::video::ETS_VIEW,matView);		
					}
					//프로잭션 메트릭스
					{
						irr::core::matrix4 matPrj;
						matPrj.buildProjectionMatrixPerspectiveLH(fovX,fovY,1,3000);
						pVideo->setTransform(irr::video::ETS_PROJECTION,matPrj);
					}
					//메트리얼설정
					{
						irr::video::SMaterial m;
						m.Lighting = false; //라이트를꺼야색이제데로나온다.
						m.ZBuffer = false;
						pVideo->setMaterial(m);
					}


					{
						irr::core::aabbox3df box1(irr::core::vector3df(-1,-1,-1),irr::core::vector3df(1,1,1));


						irr::core::matrix4 mat;
						mat.makeIdentity();
						mat.setScale(irr::core::vector3df(10.f,10.f,10.f));

						mat.transformBox(box1);


						pVideo->draw3DBox(box1);
					}

					{
						irr::core::aabbox3df box1(irr::core::vector3df(-1,-1,-1),irr::core::vector3df(1,1,1));
						irr::core::matrix4 mat;
						mat.makeIdentity();
						mat.setScale(irr::core::vector3df(5.f,10.f,5.f));
						mat.setTranslation(irr::core::vector3df(10.f,0.f,-20.f));

						mat.transformBox(box1);
						pVideo->draw3DBox(box1);
					}

					{
						irr::core::aabbox3df box1(irr::core::vector3df(-1,-1,-1),irr::core::vector3df(1,1,1));
						irr::core::matrix4 mat;
						mat.makeIdentity();
						mat.setScale(irr::core::vector3df(10.f,20.f,10.f));
						mat.setTranslation(irr::core::vector3df(-20.f,0.f,-15.f));

						mat.transformBox(box1);
						pVideo->draw3DBox(box1);
					}	

				}
				pVideo->endScene();
			}
			pDevice->drop();
		}
	}

	//카메라노드 직접적용 예제
	namespace _04
	{
		void main()
		{
			irr::core::vector3df 벡터1(0,0,0),벡터2(320,240,0);

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9,
				irr::core::dimension2du(640,480) 
				); 
			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver(); 

			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();

			pSmgr->addCameraSceneNode(0,irr::core::vector3df(0,0,-500),irr::core::vector3df(0,0,0));

			while(pDevice->run())
			{
				pVideo->beginScene();

				{	

					//카메라노드 직접적용
					{
						irr::scene::ICameraSceneNode *pCam = pSmgr->getActiveCamera();
						pCam->OnRegisterSceneNode();
						pCam->render();
					}
					//pSmgr->drawAll();

					pVideo->setTransform(
						irr::video::ETS_WORLD,
						irr::core::IdentityMatrix
						); //변환초기화

					////뷰매트릭스만들기
					//irr::core::matrix4 matView;
					//matView.buildCameraLookAtMatrixLH(
					//	irr::core::vector3df(0,0,-100), //카메라위치
					//	irr::core::vector3df(0,0,0), //시점
					//	irr::core::vector3df(0,1,0) //업벡터
					//	);
					//pVideo->setTransform(irr::video::ETS_VIEW,matView);

					////프로잭션 메트릭스
					//irr::core::matrix4 matPrj;
					//matPrj.buildProjectionMatrixOrthoLH(640,480,1,3000);
					//pVideo->setTransform(irr::video::ETS_PROJECTION,matPrj);

					//메트리얼설정
					irr::video::SMaterial m;
					m.Lighting = false; //라이트를꺼야색이제데로나온다.
					m.ZBuffer = false;
					pVideo->setMaterial(m);

					pVideo->draw3DLine(
						벡터1,
						벡터2,    
						irr::video::SColor(0,0,255,0));   
					pVideo->draw3DLine(
						벡터1,
						-벡터2,    
						irr::video::SColor(0,0,0,255));   
				}

				pVideo->endScene();
			}
			pDevice->drop();
		}
	}


	

}