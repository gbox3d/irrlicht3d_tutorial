﻿#include <irrlicht.h>

//직접 텍스춰 다루기 예제
//drawIndexedTriangleList 이용 출력

#include <iostream>

#ifdef USE_MS_DX9

#include <d3dx9.h>

#endif

using namespace std;

namespace Lesson04
{
	namespace _00
	{

		void main()
		{
			//irr::core::vector3df 벡터1(0,0,0),벡터2(320,240,0);

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9,
				irr::core::dimension2du(640,480) 
				); 
			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver(); 

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			//크기가 1인 3D 평면 만들기
			irr::u16 triList[] = {0,1,2,0,2,3};
			irr::video::S3DVertex triVer[] =
			{
				irr::video::S3DVertex( .5f, .5f,0,  0,0,-1, 0xFFFFFFFF, 1,0),
				irr::video::S3DVertex( .5f,-.5f,0,  0,0,-1, 0xFFFFFFFF, 1,1),
				irr::video::S3DVertex(-.5f,-.5f,0,  0,0,-1, 0xFFFFFFFF, 0,1),
				irr::video::S3DVertex(-.5f, .5f,0,  0,0,-1, 0xFFFFFFFF, 0,0)
			};

			irr::video::ITexture *pTexture = pVideo->getTexture("sean7706.jpg");


			while(pDevice->run())
			{
				pVideo->beginScene();

				{	
					//월드변환
					{
						irr::core::matrix4 matWorld,matWorld_scl,matWorld_rot,matWorld_trn;

						matWorld_trn.setTranslation(irr::core::vector3df(0,0,0));
						matWorld_rot.setRotationDegrees(irr::core::vector3df(0,0,0));
						matWorld_scl.setScale(irr::core::vector3df(
							(irr::f32)pVideo->getViewPort().getWidth(),(irr::f32)pVideo->getViewPort().getHeight(),0));

						matWorld = matWorld_trn * matWorld_rot * matWorld_scl;

						pVideo->setTransform(
							irr::video::ETS_WORLD,
							matWorld
							); 
					}

					//뷰매트릭스만들기
					irr::core::matrix4 matView;
					matView.buildCameraLookAtMatrixLH(
						irr::core::vector3df(0,0,-100), //카메라위치
						irr::core::vector3df(0,0,0), //시점
						irr::core::vector3df(0,1,0) //업벡터
						);
					pVideo->setTransform(irr::video::ETS_VIEW,matView);

					//직교투영 프로잭션 매트릭스
					irr::core::matrix4 matPrj;
					matPrj.buildProjectionMatrixOrthoLH(640,480,1,3000);
					pVideo->setTransform(irr::video::ETS_PROJECTION,matPrj);

					//메트리얼설정
					irr::video::SMaterial m;
					m.Lighting = false; //라이트를꺼야색이제데로나온다.
					m.ZBuffer = false;					
					m.setTexture(0,pTexture);
					pVideo->setMaterial(m);

					pVideo->drawIndexedTriangleList(triVer,4,triList,2);

				}

				pVideo->endScene();
			}
			pDevice->drop();
		}

	}
#ifdef USE_MS_DX9
	//직접 텍스춰 다루기 예제
	//DXtexture로 ITexture 직접 생성
	namespace _01
	{

		class CD3D9Texture : public irr::video::ITexture
		{
		public:

			CD3D9Texture(
				LPDIRECT3DTEXTURE9 pD3dTexture,
				LPDIRECT3DDEVICE9 pDevice, 
				const irr::core::stringc& name,
				const irr::core::dimension2d<irr::u32>& size				
				)
				: ITexture(irr::io::path(name.c_str())), Texture(pD3dTexture), RTTSurface(0),// Driver(driver),// DepthSurface(0),
				TextureSize(size), ImageSize(size), Pitch(0),
				HasMipMaps(false), HardwareMipMaps(false), IsRenderTarget(true)
			{
#ifdef _DEBUG
				setDebugName("CD3D9Texture");
#endif	
				Device = pDevice; //driver->getExposedVideoData().D3D9.D3DDev9;
				if (Device)
					Device->AddRef();
				//createRenderTarget();
			}


			virtual ~CD3D9Texture(){};


			//! Returns original size of the texture.
			inline const irr::core::dimension2d<irr::u32>& getOriginalSize() const
			{
				return ImageSize;
			}


			//! Returns (=size) of the texture.
			inline const irr::core::dimension2d<irr::u32>& getSize() const
			{
				return TextureSize;
			}


			////! returns the size of a texture which would be the optimize size for rendering it
			inline irr::s32 getTextureSizeFromSurfaceSize(irr::s32 size) const
			{
				irr::s32 ts = 0x01;

				while(ts < size)
					ts <<= 1;

				return ts;
			}



			//! returns driver type of texture (=the driver, who created the texture)
			inline irr::video::E_DRIVER_TYPE getDriverType() const
			{
				return irr::video::EDT_DIRECT3D9;
			}



			//! returns color format of texture
			inline irr::video::ECOLOR_FORMAT getColorFormat() const
			{
				return ColorFormat;
			}



			//! returns pitch of texture (in bytes)
			inline irr::u32 getPitch() const
			{
				return Pitch;
			}



			//! returns the DIRECT3D9 Texture
			inline IDirect3DBaseTexture9* getDX9Texture() const
			{
				return Texture;
			}


			inline void regenerateMipMapLevels(void* mipmapData=0)
			{
				//if (HasMipMaps)
				//	createMipMaps();
			}


			//! returns if it is a render target
			inline bool isRenderTarget() const
			{
				return IsRenderTarget;
			}


			//! Returns pointer to the render target surface
			inline IDirect3DSurface9* getRenderTargetSurface()
			{
				if (!IsRenderTarget)
					return 0;

				IDirect3DSurface9 *pRTTSurface = 0;
				if (Texture)
					Texture->GetSurfaceLevel(0, &pRTTSurface);

				if (pRTTSurface)
					pRTTSurface->Release();

				return pRTTSurface;
			}	

			inline void* lock(bool readOnly = false, irr::u32 mipmapLevel=0)			
			//inline void* lock(bool readOnly)
			{
				if (!Texture)
					return 0;

				HRESULT hr;
				D3DLOCKED_RECT rect;
				if(!IsRenderTarget)
				{
					hr = Texture->LockRect(0, &rect, 0, readOnly?D3DLOCK_READONLY:0);
					if (FAILED(hr))
					{
						//os::Printer::log("Could not lock DIRECT3D9 Texture.", ELL_ERROR);
						return 0;
					}

					return rect.pBits;
				}
				else
				{
					D3DSURFACE_DESC desc;
					Texture->GetLevelDesc(0, &desc);
					if (!RTTSurface)
					{
						hr = Device->CreateOffscreenPlainSurface(desc.Width, desc.Height, desc.Format, D3DPOOL_SYSTEMMEM, &RTTSurface, 0);
						if (FAILED(hr))
						{
							//os::Printer::log("Could not lock DIRECT3D9 Texture", "Offscreen surface creation failed.", ELL_ERROR);
							return 0;
						}
					}

					IDirect3DSurface9 *surface = 0;
					hr = Texture->GetSurfaceLevel(0, &surface);
					if (FAILED(hr))
					{
						//os::Printer::log("Could not lock DIRECT3D9 Texture", "Could not get surface.", ELL_ERROR);
						return 0;
					}
					hr = Device->GetRenderTargetData(surface, RTTSurface);
					surface->Release();
					if(FAILED(hr))
					{
						//os::Printer::log("Could not lock DIRECT3D9 Texture", "Data copy failed.", ELL_ERROR);
						return 0;
					}
					hr = RTTSurface->LockRect(&rect, 0, readOnly?D3DLOCK_READONLY:0);
					if(FAILED(hr))
					{
						//os::Printer::log("Could not lock DIRECT3D9 Texture", "LockRect failed.", ELL_ERROR);
						return 0;
					}
					return rect.pBits;
				}
			}


			
			//! unlock function
			inline void unlock()
			{
				if (!Texture)
					return;

				if (!IsRenderTarget)
					Texture->UnlockRect(0);
				else if (RTTSurface)
					RTTSurface->UnlockRect();
			}	

		private:	

			IDirect3DDevice9* Device;
			IDirect3DTexture9* Texture;
			IDirect3DSurface9* RTTSurface;	
			//SDepthSurface* DepthSurface;
			irr::core::dimension2d<irr::u32> TextureSize;
			irr::core::dimension2d<irr::u32> ImageSize;
			irr::s32 Pitch;
			irr::video::ECOLOR_FORMAT ColorFormat;

			bool HasMipMaps;
			bool HardwareMipMaps;
			bool IsRenderTarget;
		};	






		void main()
		{
			//irr::core::vector3df 벡터1(0,0,0),벡터2(320,240,0);

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9,
				irr::core::dimension2du(640,480) 
				); 
			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver(); 

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			//크기가 1인 3D 평면 만들기
			irr::u16 triList[] = {0,1,2,0,2,3};
			irr::video::S3DVertex triVer[] =
			{
				irr::video::S3DVertex( .5f, .5f,0,  0,0,-1, 0xFFFFFFFF, 1,0),
				irr::video::S3DVertex( .5f,-.5f,0,  0,0,-1, 0xFFFFFFFF, 1,1),
				irr::video::S3DVertex(-.5f,-.5f,0,  0,0,-1, 0xFFFFFFFF, 0,1),
				irr::video::S3DVertex(-.5f, .5f,0,  0,0,-1, 0xFFFFFFFF, 0,0)
			};

			//따로 디엑스 텍스춰 얻기
			LPDIRECT3DTEXTURE9 pD3D9Texture;
			{		
				if(FAILED(D3DXCreateTextureFromFile(
					pVideo->getExposedVideoData().D3D9.D3DDev9,
					L"sean7706.jpg",&pD3D9Texture)))
				{
					std::cout << "can not load texture" << std::endl;
				}

			}

			//dx9 텍스춰로 일리히트 텍스춰 생성
			Lesson04::_01::CD3D9Texture *pMyTexture = new Lesson04::_01::CD3D9Texture(
				pD3D9Texture,pVideo->getExposedVideoData().D3D9.D3DDev9,		
				irr::core::stringc("mytexture"),
				irr::core::dimension2du(0,0)
				);

			while(pDevice->run())
			{
				pVideo->beginScene();

				{	
					//월드변환
					{
						irr::core::matrix4 matWorld,matWorld_scl,matWorld_rot,matWorld_trn;

						matWorld_trn.setTranslation(irr::core::vector3df(0,0,0));
						matWorld_rot.setRotationDegrees(irr::core::vector3df(0,0,0));
						matWorld_scl.setScale(irr::core::vector3df(pVideo->getViewPort().getWidth(),pVideo->getViewPort().getHeight(),0));

						matWorld = matWorld_trn * matWorld_rot * matWorld_scl;

						pVideo->setTransform(
							irr::video::ETS_WORLD,
							matWorld
							); 
					}

					//뷰매트릭스만들기
					irr::core::matrix4 matView;
					matView.buildCameraLookAtMatrixLH(
						irr::core::vector3df(0,0,-100), //카메라위치
						irr::core::vector3df(0,0,0), //시점
						irr::core::vector3df(0,1,0) //업벡터
						);
					pVideo->setTransform(irr::video::ETS_VIEW,matView);

					//직교투영 프로잭션 매트릭스
					irr::core::matrix4 matPrj;
					matPrj.buildProjectionMatrixOrthoLH(640,480,1,3000);
					pVideo->setTransform(irr::video::ETS_PROJECTION,matPrj);

					//메트리얼설정
					irr::video::SMaterial m;
					m.Lighting = false; //라이트를꺼야색이제데로나온다.
					m.ZBuffer = false;
					m.setTexture(0,pMyTexture);
					pVideo->setMaterial(m);

					pVideo->drawIndexedTriangleList(triVer,4,triList,2);

				}

				pVideo->endScene();
			}

			pDevice->drop();

			//delete pMyTexture;

			pD3D9Texture->Release();


		}

	}



	//직접 텍스춰 다루기 예제
	//DXtexture로 ITexture 직접 생성
	/*
	LPDIRECT3DTEXTURE9,LPDIRECT3DSURFACE9 응용 예제

	텍스춰에서 서페이스를 얻고 
	서페이스에서 메모리를 얻어서 직접 이미지 수정하기 예제

	*/

	namespace _02
	{

		void main()
		{

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9,
				irr::core::dimension2du(640,480) 
				); 
			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver(); 

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			irr::u16 triList[] = {0,1,2,0,2,3};
			irr::video::S3DVertex triVer[] =
			{
				irr::video::S3DVertex( 320, 240,0, 0,0,-1, 0xFFFFFFFF, 1,0),
				irr::video::S3DVertex( 320,-240,0, 0,0,-1, 0xFFFFFFFF, 1,1),
				irr::video::S3DVertex(-320,-240,0, 0,0,-1, 0xFFFFFFFF, 0,1),
				irr::video::S3DVertex(-320, 240,0, 0,0,-1, 0xFFFFFFFF, 0,0)
			};


			LPDIRECT3DTEXTURE9 pD3D9Texture;
			{		
				if(FAILED(D3DXCreateTextureFromFile(
					pVideo->getExposedVideoData().D3D9.D3DDev9,
					L"../res/dotnetback.jpg",&pD3D9Texture)))
				{
					std::cout << "can not load texture" << std::endl;
				}

				std::cout << "surface count :" << pD3D9Texture->GetLevelCount() << std::endl;

				LPDIRECT3DSURFACE9 pSurface;

				pD3D9Texture->GetSurfaceLevel(0,&pSurface);

				D3DSURFACE_DESC dsd;
				pSurface->GetDesc(&dsd);

				std::cout << "format : " << dsd.Format << std::endl;
				std::cout << "Width : " << dsd.Width << std::endl;
				std::cout << "Height : " << dsd.Height << std::endl;


				D3DLOCKED_RECT dlr;
				pSurface->LockRect(&dlr,NULL,D3DLOCK_DISCARD);

				std::cout << "Pitch : " << dlr.Pitch << std::endl;
				std::cout << "Bpp : " << dlr.Pitch/dsd.Width << std::endl;		

				pSurface->UnlockRect();
			}


			LPDIRECT3DTEXTURE9 pD3D9Texture2;
			{		
				if(FAILED(D3DXCreateTextureFromFile(
					pVideo->getExposedVideoData().D3D9.D3DDev9,
					L"../res/irrlichtlogo.jpg",&pD3D9Texture2)))
				{
					std::cout << "can not load texture" << std::endl;
				}

				std::cout << "surface count :" << pD3D9Texture2->GetLevelCount() << std::endl;
			}

			//임의 위치에 사각형 그리기 예
			{
				LPDIRECT3DSURFACE9 pSurface;
				D3DSURFACE_DESC dsd;
				D3DLOCKED_RECT dlr;

				pD3D9Texture->GetSurfaceLevel(0,&pSurface);
				pSurface->GetDesc(&dsd);	
				pSurface->LockRect(&dlr,NULL,D3DLOCK_DISCARD);		

				irr::u32 col,row;

				irr::u32 draw_px = 0;
				irr::u32 draw_py = 0;
				irr::u32 draw_wsize = 32;
				irr::u32 draw_hsize = 32;
				irr::u32 Bps = dlr.Pitch/dsd.Width; //바이트퍼 픽셀
				irr::video::SColor color(255,255,0,0);

				for(row=0;row < draw_hsize ;row++)
				{
					for(col=0;col < draw_wsize ;col++)
					{
						irr::u8 *pDestBit = static_cast<byte *>(dlr.pBits);

						irr::u32 offset = ((row*dlr.Pitch) + (draw_py*dlr.Pitch + draw_px) ) + (col*Bps);
						pDestBit += offset;

						//32비트컬러용A8R8G8B8				
						pDestBit[0] = color.getBlue();  //B
						pDestBit[1] = color.getGreen(); //G
						pDestBit[2] = color.getRed();   //R
						pDestBit[3] = color.getAlpha(); //A
					}
				}

				pSurface->UnlockRect();
			}

			//임의 위치에 이미지 출력
			{
				LPDIRECT3DSURFACE9 pSurface,pSurface2;
				D3DSURFACE_DESC dsd,dsd2;
				D3DLOCKED_RECT dlr,dlr2;

				pD3D9Texture->GetSurfaceLevel(0,&pSurface);
				//첫번째 인자인 레벨은 LOD값이다. 레벨별로 
				//최대 레벨은 이미지에따라 다르며
				//수치가 높을수록 정밀도가 낮은 이미지를 얻는다.(원거리용)
				pD3D9Texture2->GetSurfaceLevel(0,&pSurface2);

				pSurface->GetDesc(&dsd);
				pSurface2->GetDesc(&dsd2);

				pSurface->LockRect(&dlr,NULL,D3DLOCK_DISCARD);
				pSurface2->LockRect(&dlr2,NULL,D3DLOCK_DISCARD);

				irr::u32 col,row;

				irr::u32 draw_px = 100; // 위치지정
				irr::u32 draw_py = 100;
				//소스 이미지 싸이즈
				irr::u32 draw_wsize = dsd2.Width;
				irr::u32 draw_hsize = dsd2.Height;
				irr::u32 Bps = dlr.Pitch/dsd.Width; //바이트퍼 픽셀
				irr::video::SColor color(255,255,0,0);

				for(row=0;row < draw_hsize ;row++)
				{
					for(col=0;col < draw_wsize ;col++)
					{
						irr::u8 *pDestBit = static_cast<byte *>(dlr.pBits);
						irr::u8 *pSrcBit = static_cast<byte *>(dlr2.pBits);

						irr::u32 dest_offset = ((row*dlr.Pitch) + (draw_py*dlr.Pitch + draw_px) ) + (col* Bps);
						irr::u32 src_offset = ((row*dlr2.Pitch) ) + (col* Bps);

						pDestBit += (dest_offset );				
						pSrcBit += (src_offset );

						pDestBit[0] = pSrcBit[0];//B
						pDestBit[1] = pSrcBit[1]; //G
						pDestBit[2] = pSrcBit[2]; //R
						pDestBit[3] = pSrcBit[3]; //A

						//memcpy(pDestBit,pSrcBit,Bps);				
					}
				}
				pSurface->UnlockRect();
				pSurface2->UnlockRect();
			}	

			while(pDevice->run())
			{
				pVideo->beginScene();

				{	
					pVideo->setTransform(
						irr::video::ETS_WORLD,
						irr::core::IdentityMatrix
						); //변환초기화

					//뷰매트릭스만들기
					irr::core::matrix4 matView;
					matView.buildCameraLookAtMatrixLH(
						irr::core::vector3df(0,0,-100), //카메라위치
						irr::core::vector3df(0,0,0), //시점
						irr::core::vector3df(0,1,0) //업벡터
						);
					pVideo->setTransform(irr::video::ETS_VIEW,matView);

					//직교투영 프로잭션 매트릭스
					irr::core::matrix4 matPrj;
					matPrj.buildProjectionMatrixOrthoLH(640,480,1,3000);
					pVideo->setTransform(irr::video::ETS_PROJECTION,matPrj);

					//메트리얼설정
					irr::video::SMaterial m;
					m.Lighting = false; //라이트를꺼야색이제데로나온다.
					m.ZBuffer = false;
					//m.setTexture(0,pMyTexture);			
					pVideo->setMaterial(m);

					{
						pVideo->getExposedVideoData().D3D9.D3DDev9->SetTexture(0,pD3D9Texture);
					}

					pVideo->drawIndexedTriangleList(triVer,4,triList,2);

				}

				pVideo->endScene();
			}
			pDevice->drop();

			pD3D9Texture->Release();
		}

	}

#endif
	//직접 텍스춰 다루기 예제
	//이미지 직접 제어 예제(텍스춰에 직접 점찍기)
	//lock , unlock 함수 이용
	namespace _03
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9,
				irr::core::dimension2du(640,480) 
				); 

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver(); 

			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,10,20), irr::core::vector3df(0,5,0));

			// camera 
			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,10,20), irr::core::vector3df(0,5,0));

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");


			//평면 사각형 버텍스와 인덱스 리스트
			irr::u16 triList[] = {0,1,2,0,2,3};	
			irr::video::S3DVertex triVer[] =
			{
				irr::video::S3DVertex( 320, 240,0,  0,0,-1, 0xFFFFFFFF, 1,0),
				irr::video::S3DVertex( 320,-240,0,  0,0,-1, 0xFFFFFFFF, 1,1),
				irr::video::S3DVertex(-320,-240,0,  0,0,-1, 0xFFFFFFFF, 0,1),
				irr::video::S3DVertex(-320, 240,0,  0,0,-1, 0xFFFFFFFF, 0,0)
			};


			irr::video::ITexture *pTestTexture;
			{
				irr::video::ITexture *pTexture;		
				irr::video::IImage *pImg = pVideo->createImage(irr::video::ECF_A8R8G8B8,irr::core::dimension2du(256,256));

				//밉멥속성을 참으로 해주면 멀리서 볼경우 다른 밉멥을 보여주므로 텍스춰에 직접 그리기 한것을 볼수없다.
				pVideo->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS,false);		
				pTexture = pVideo->addTexture("usr/texture/test",pImg);		
				pImg->drop();

				pTestTexture = pTexture;
			}

			// 테스트용 큐브 띠우기
			{
				irr::scene::ISceneNode *pNode = pSmgr->addCubeSceneNode(5);
				pNode->setName("usr/snode/cube/1");
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setMaterialTexture(0,pTestTexture);	
				pNode->setPosition(irr::core::vector3df(10,0,0));

				irr::scene::ISceneNodeAnimator *pAnim;
				pAnim = pSmgr->createRotationAnimator(irr::core::vector3df(0.3f,0.5f,0));
				pNode->addAnimator(pAnim);
				pAnim->drop();		
			}


			while(pDevice->run())
			{
				//이미지 프로세싱
				{
					irr::video::ITexture *pTexture;			
					pTexture = pTestTexture;
					irr::u8 *pBuffer = static_cast<irr::u8 *>(pTexture->lock());

					//setpixel x, y, color
					int x,y;
					x = rand() % 256;
					y = rand() % 128;

					irr::u32 index = (y * pTexture->getPitch()) + x*4;

					pBuffer[index++] = rand() % 0xff;//B
					pBuffer[index++] = rand() % 0xff;//G
					pBuffer[index++] = rand() % 0xff;//R
					pBuffer[index] = 0xff;		//A	

					pTexture->unlock();
				}

				pVideo->beginScene();


				{	
					pVideo->setTransform(
						irr::video::ETS_WORLD,
						irr::core::IdentityMatrix
						); //변환초기화

					//뷰매트릭스만들기
					irr::core::matrix4 matView;
					matView.buildCameraLookAtMatrixLH(
						irr::core::vector3df(0,0,-100), //카메라위치
						irr::core::vector3df(0,0,0), //시점
						irr::core::vector3df(0,1,0) //업벡터
						);
					pVideo->setTransform(irr::video::ETS_VIEW,matView);

					//직교투영 프로잭션 매트릭스
					irr::core::matrix4 matPrj;
					matPrj.buildProjectionMatrixOrthoLH(640,480,1,3000);
					pVideo->setTransform(irr::video::ETS_PROJECTION,matPrj);

					//메트리얼설정

					irr::video::SMaterial m;
					m.Lighting = false; //라이트를꺼야색이제데로나온다.
					m.ZBuffer = false;
					m.setTexture(0,pTestTexture); //텍스춰 적용
					pVideo->setMaterial(m);

					pVideo->drawIndexedTriangleList(triVer,4,triList,2);

				}

				pSmgr->drawAll();

				pVideo->endScene();
			}
			pDevice->drop();
		}

	}




	//직접 텍스춰 다루기 예제
	//drawIndexedTriangleList 이용 출력
	//copyToScaling 사용 예제
	//copyTo
	//이미지간 데이타전송 예제
	//CopyImg2Tex, 이미지-> 텍스춰로 데이터 전송 예
	namespace _04
	{
		void CopyImg2Tex(irr::video::IImage *pImg,irr::video::ITexture *pTexture)
		{
			irr::u8 *pDes = static_cast<irr::u8 *>(pTexture->lock());
			if(pDes)
			{
				irr::u8 *pSrc = static_cast<irr::u8 *>(pImg->lock());
				if(pSrc)
				{
					irr::u32 col,row;
					irr::u32 Width,Height,Pitch,dPitch;
					irr::u32 bps_s,bps_d;

					Width  = min(pTexture->getSize().Width,pImg->getDimension().Width);// pCamShow->getWidth();
					Height = min(pTexture->getSize().Height,pImg->getDimension().Height);//pCamShow->getHight();
					Pitch  = pImg->getPitch();
					dPitch = pTexture->getPitch();

					bps_s = pImg->getBytesPerPixel();
					bps_d = pTexture->getPitch() / pTexture->getSize().Width;

					for(row = 0;row<Height;row++)
					{
						irr::u8 *pSrc_old = pSrc;                
						irr::u8 *pDes_old = pDes;				

						for(col=0;col <Width;col++)
						{
							pDes[0] = pSrc[0];
							pDes[1] = pSrc[1];
							pDes[2] = pSrc[2];
							pDes[3] = pSrc[3];					

							pDes += bps_d;
							pSrc += bps_s;
						}

						pSrc = pSrc_old + Pitch;
						pDes = pDes_old + dPitch;
					}
					pImg->unlock();
				}

				pTexture->unlock();
			}

		}



		void main()
		{
			//irr::core::vector3df 벡터1(0,0,0),벡터2(320,240,0);

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9,
				irr::core::dimension2du(640,480) 
				); 
			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver(); 

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			//크기가 1인 3D 평면 만들기
			irr::u16 triList[] = {0,1,2,0,2,3};
			irr::video::S3DVertex triVer[] =
			{
				irr::video::S3DVertex( .5f, .5f,0,  0,0,-1, 0xFFFFFFFF, 1,0),
				irr::video::S3DVertex( .5f,-.5f,0,  0,0,-1, 0xFFFFFFFF, 1,1),
				irr::video::S3DVertex(-.5f,-.5f,0,  0,0,-1, 0xFFFFFFFF, 0,1),
				irr::video::S3DVertex(-.5f, .5f,0,  0,0,-1, 0xFFFFFFFF, 0,0)
			};

			//이미지 로딩
			irr::video::ITexture *pTexture;
			irr::video::IImage *pImg = pVideo->createImageFromFile("jga/jga_poster.jpg");
			irr::video::IImage *pImg1 = pVideo->createImageFromFile("jga/박예진.jpg");
			irr::video::IImage *pImg_Back = pVideo->createImage(irr::video::ECF_A8R8G8B8,irr::core::dimension2du(512,512));
			{
				//밉멥속성을 참으로 해주면 멀리서 볼경우 다른 밉멥을 보여주므로 텍스춰에 직접 그리기 한것을 볼수없다.
				pVideo->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS,false);		
				pTexture = pVideo->addTexture("usr/texture/test",pImg_Back);
			}		

			irr::core::dimension2du copySize(1,1);			

			while(pDevice->run())
			{
				pVideo->beginScene();

				{
					//pImg->copyToScaling(pImg_Back);//아래것과 같은 결과임
					pImg->copyToScaling(pImg_Back->lock(),
						pImg_Back->getDimension().Width,pImg_Back->getDimension().Height,
						pImg_Back->getColorFormat()
						);
					pImg_Back->unlock(); //언락을 빼먹지 말것!

					pImg1->copyTo(pImg_Back,irr::core::vector2di(10,10),
						irr::core::recti(irr::core::vector2di(64,64),copySize)
						);

					copySize += irr::core::dimension2du(1,1);

					if(copySize.Height > 256)
					{
						copySize = irr::core::dimension2du(1,1);
					}


					CopyImg2Tex(pImg_Back,pTexture);
				}

				{	
					//월드변환
					{
						irr::core::matrix4 matWorld,matWorld_scl,matWorld_rot,matWorld_trn;

						matWorld_trn.setTranslation(irr::core::vector3df(0,0,0));
						matWorld_rot.setRotationDegrees(irr::core::vector3df(0,0,0));
						matWorld_scl.setScale(irr::core::vector3df(
							(irr::f32)pVideo->getViewPort().getWidth(),(irr::f32)pVideo->getViewPort().getHeight(),0));

						matWorld = matWorld_trn * matWorld_rot * matWorld_scl;

						pVideo->setTransform(
							irr::video::ETS_WORLD,
							matWorld
							); 
					}

					//뷰매트릭스만들기
					irr::core::matrix4 matView;
					matView.buildCameraLookAtMatrixLH(
						irr::core::vector3df(0,0,-100), //카메라위치
						irr::core::vector3df(0,0,0), //시점
						irr::core::vector3df(0,1,0) //업벡터
						);
					pVideo->setTransform(irr::video::ETS_VIEW,matView);

					//직교투영 프로잭션 매트릭스
					irr::core::matrix4 matPrj;
					matPrj.buildProjectionMatrixOrthoLH(640,480,1,3000);
					pVideo->setTransform(irr::video::ETS_PROJECTION,matPrj);

					//메트리얼설정
					irr::video::SMaterial m;
					m.Lighting = false; //라이트를꺼야색이제데로나온다.
					m.ZBuffer = false;
					m.setTexture(0,pTexture);
					pVideo->setMaterial(m);

					pVideo->drawIndexedTriangleList(triVer,4,triList,2);

				}

				pVideo->endScene();
			}

			pImg->drop();
			pImg1->drop();
			pImg_Back->drop();

			pDevice->drop();
		}

	}

	//직접 텍스춰 다루기 예제
	//삼차원 공간상에서 3디랜더러로 2디 하기 예제(Draw25D)
	//EMT_TRANSPARENT_ALPHA_CHANNEL 과 z 버퍼의 관계 예제
	namespace _05
	{

		void Draw25D(irr::video::IVideoDriver *pVideo,irr::core::vector3df pos,irr::c8 *szTexName)
		{	
			irr::video::ITexture *pTex = pVideo->getTexture(szTexName);

			//크기가 1인 3D 평면 만들기
			irr::u16 triList[] = {0,1,2,0,2,3};
			irr::video::S3DVertex triVer[] =
			{
				irr::video::S3DVertex( .5f, .5f,0,  0,0,-1, 0xFFFFFFFF, 1,0),
				irr::video::S3DVertex( .5f,-.5f,0,  0,0,-1, 0xFFFFFFFF, 1,1),
				irr::video::S3DVertex(-.5f,-.5f,0,  0,0,-1, 0xFFFFFFFF, 0,1),
				irr::video::S3DVertex(-.5f, .5f,0,  0,0,-1, 0xFFFFFFFF, 0,0)
			};

			//월드변환
			{
				irr::core::matrix4 matWorld,matWorld_scl,matWorld_rot,matWorld_trn;

				matWorld_trn.setTranslation(pos);//irr::core::vector3df(pos.X,pos.Y,0));
				matWorld_rot.setRotationDegrees(irr::core::vector3df(0,0,0));

				//irr::core::dimension2du osize =  pTex->getOriginalSize();
				//irr::core::dimension2du tsize =  pTex->getSize();

				matWorld_scl.setScale(
					irr::core::vector3df(pTex->getSize().Width,pTex->getSize().Height,0)
					);

				matWorld = matWorld_trn * matWorld_rot * matWorld_scl;

				pVideo->setTransform(
					irr::video::ETS_WORLD,
					matWorld
					); 
			}

			{
				//뷰매트릭스만들기
				irr::core::matrix4 matView;
				matView.buildCameraLookAtMatrixLH(
					irr::core::vector3df(0,0,-100), //카메라위치
					irr::core::vector3df(0,0,0), //시점
					irr::core::vector3df(0,1,0) //업벡터
					);
				pVideo->setTransform(irr::video::ETS_VIEW,matView);
			}

			{

				//직교투영 프로잭션 매트릭스
				irr::core::matrix4 matPrj;
				matPrj.buildProjectionMatrixOrthoLH(640,480,1,3000);
				pVideo->setTransform(irr::video::ETS_PROJECTION,matPrj);
			}

			{
				//메트리얼설정
				irr::video::SMaterial m;
				m.Lighting = false; //라이트를꺼야색이제데로나온다.			
				
				//m.ZWriteEnable = false;
				//irr::video::ECFN_NEVER것과 같은결과
				m.MaterialType = irr::video::EMT_TRANSPARENT_ALPHA_CHANNEL; //z 버퍼가 무효화됨 
			

				//
				//z 버퍼가 유효, m.ZWriteEnable = true;것과 같은결과
				//m.MaterialType = irr::video::EMT_TRANSPARENT_ALPHA_CHANNEL_REF; 
				
				m.setTexture(0,pTex);
				pVideo->setMaterial(m);				
			}

			pVideo->drawIndexedTriangleList(triVer,4,triList,2);
		}

		void main()
		{
			//irr::core::vector3df 벡터1(0,0,0),벡터2(320,240,0);

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9,
				irr::core::dimension2du(640,480) 
				); 
			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver(); 

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");			

			irr::video::ITexture *pTexture = pVideo->getTexture("sean7706.jpg");
			pTexture = pVideo->getTexture("jga/alpha_test.tga");

			while(pDevice->run())
			{
				pVideo->beginScene();							

				Draw25D(pVideo,irr::core::vector3df(0,0,0),"sean7706.jpg");						
				//z 버퍼가 켜져있으면 다음이 출력 되서는 안된다.
				//z버퍼를 끄거나 메트리얼 타입을 조정하는 z 버퍼가 무효화된다.
				Draw25D(pVideo,irr::core::vector3df(0,0,1),"jga/alpha_test.tga");				

				pVideo->endScene();
			}
			pDevice->drop();
		}

	}

	//컬러키 테스트 예제
	namespace _06
	{

		void main()
		{
			//irr::core::vector3df 벡터1(0,0,0),벡터2(320,240,0);

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9,
				//irr::video::EDT_OPENGL,
				//irr::video::EDT_BURNINGSVIDEO,
				irr::core::dimension2du(640,480) 
				); 
			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver(); 

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			//크기가 1인 3D 평면 만들기
			irr::u16 triList[] = {0,1,2,0,2,3};
			irr::video::S3DVertex triVer[] =
			{
				irr::video::S3DVertex( .5f, .5f,0,  0,0,-1, 0xFFFFFFFF, 1,0),
				irr::video::S3DVertex( .5f,-.5f,0,  0,0,-1, 0xFFFFFFFF, 1,1),
				irr::video::S3DVertex(-.5f,-.5f,0,  0,0,-1, 0xFFFFFFFF, 0,1),
				irr::video::S3DVertex(-.5f, .5f,0,  0,0,-1, 0xFFFFFFFF, 0,0)
			};

			irr::video::ITexture *pTexture = pVideo->getTexture("sean7706.jpg");
			
			//pVideo->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS,false);	
			irr::video::ITexture *pTexture2 = pVideo->getTexture("Particle.bmp");
			pVideo->makeColorKeyTexture(pTexture2,0x00000000);
			
			//pTexture2->regenerateMipMapLevels();
			


			while(pDevice->run())
			{
				pVideo->beginScene();

				{	
					//월드변환
					{
						irr::core::matrix4 matWorld,matWorld_scl,matWorld_rot,matWorld_trn;

						matWorld_trn.setTranslation(irr::core::vector3df(0,0,0));
						matWorld_rot.setRotationDegrees(irr::core::vector3df(0,0,0));
						matWorld_scl.setScale(irr::core::vector3df(
							(irr::f32)pVideo->getViewPort().getWidth(),(irr::f32)pVideo->getViewPort().getHeight(),0));

						matWorld = matWorld_trn * matWorld_rot * matWorld_scl;

						pVideo->setTransform(
							irr::video::ETS_WORLD,
							matWorld
							); 
					}

					//뷰매트릭스만들기
					irr::core::matrix4 matView;
					matView.buildCameraLookAtMatrixLH(
						irr::core::vector3df(0,0,-100), //카메라위치
						irr::core::vector3df(0,0,0), //시점
						irr::core::vector3df(0,1,0) //업벡터
						);
					pVideo->setTransform(irr::video::ETS_VIEW,matView);

					//직교투영 프로잭션 매트릭스
					irr::core::matrix4 matPrj;
					matPrj.buildProjectionMatrixOrthoLH(640,480,1,3000);
					pVideo->setTransform(irr::video::ETS_PROJECTION,matPrj);

					//메트리얼설정
					{
						irr::video::SMaterial m;
						m.Lighting = false; //라이트를꺼야색이제데로나온다.
						m.ZBuffer = false;					
						m.setTexture(0,pTexture);
						pVideo->setMaterial(m);
					}

					pVideo->drawIndexedTriangleList(triVer,4,triList,2);

					//메트리얼설정
					{
						irr::video::SMaterial m;
						m.Lighting = false; 
						m.ZBuffer = false;
						m.MaterialType = irr::video::EMT_TRANSPARENT_ALPHA_CHANNEL; //투명컬러 적용
						m.setTexture(0,pTexture2);
						pVideo->setMaterial(m);						
					}
					
					pVideo->drawIndexedTriangleList(triVer,4,triList,2);

				}

				pVideo->endScene();
			}
			pDevice->drop();
		}

	}


	//RTT 예제
	namespace _07
	{

		void main()
		{
			//irr::core::vector3df 벡터1(0,0,0),벡터2(320,240,0);

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9,
				irr::core::dimension2du(640,480) 
				); 

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver(); 
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();

			irr::scene::ICameraSceneNode *pCam1 = pSmgr->addCameraSceneNode(0,irr::core::vector3df(0,5,-15),irr::core::vector3df(0,0,0));
			irr::scene::ICameraSceneNode *pCam2 = pSmgr->addCameraSceneNode(0,irr::core::vector3df(0,5,15),irr::core::vector3df(0,0,0));

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			{
				irr::scene::IAnimatedMeshSceneNode *pNode =
					pSmgr->addAnimatedMeshSceneNode(pSmgr->getMesh("walkers/ninja/ninja.b3d"));
				pNode->setFrameLoop(1,14);
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
			}

			//크기가 1인 3D 평면 만들기
			irr::u16 triList[] = {0,1,2,0,2,3};
			irr::video::S3DVertex triVer[] =
			{
				irr::video::S3DVertex( .5f, .5f,0,  0,0,-1, 0xFFFFFFFF, 1,0),
				irr::video::S3DVertex( .5f,-.5f,0,  0,0,-1, 0xFFFFFFFF, 1,1),
				irr::video::S3DVertex(-.5f,-.5f,0,  0,0,-1, 0xFFFFFFFF, 0,1),
				irr::video::S3DVertex(-.5f, .5f,0,  0,0,-1, 0xFFFFFFFF, 0,0)
			};

			//irr::video::ITexture *pTexture = pVideo->getTexture("sean7706.jpg");

			irr::video::ITexture *pTexture=0;

			if (pVideo->queryFeature(irr::video::EVDF_RENDER_TO_TARGET))
			{
				pTexture = pVideo->addRenderTargetTexture(irr::core::dimension2du(256,256), "RTT1");				
			}
			else
			{
				std::cout << "Your hardware or this renderer is not able to use the " << std::endl;				
			}



			while(pDevice->run())
			{
				pVideo->beginScene();	
				

				if(pTexture)
				{	
					//텍스춰에 먼저 랜더링
					pVideo->setRenderTarget(pTexture,true,true,irr::video::SColor(0,0,0,255));
					pSmgr->setActiveCamera(pCam2);
					pSmgr->drawAll();

					pVideo->setRenderTarget(0);
					pSmgr->setActiveCamera(pCam1);
					pSmgr->drawAll();

					//월드변환
					{
						irr::core::matrix4 matWorld,matWorld_scl,matWorld_rot,matWorld_trn;

						matWorld_trn.setTranslation(irr::core::vector3df(
							(irr::f32)pVideo->getViewPort().getWidth()/4,
							-(irr::f32)pVideo->getViewPort().getHeight()/4,0));
						matWorld_rot.setRotationDegrees(irr::core::vector3df(0,0,0));
						matWorld_scl.setScale(irr::core::vector3df(
							(irr::f32)pVideo->getViewPort().getWidth()/2, 
							(irr::f32)pVideo->getViewPort().getHeight()/2,
							0)
							);
						/*matWorld_trn.setTranslation(irr::core::vector3df(0,0,0));
							
						matWorld_rot.setRotationDegrees(irr::core::vector3df(0,0,0));
						matWorld_scl.setScale(irr::core::vector3df(
							(irr::f32)pVideo->getViewPort().getWidth(), 
							(irr::f32)pVideo->getViewPort().getHeight(),
							0)
							);*/

						matWorld = matWorld_trn * matWorld_rot * matWorld_scl;

						pVideo->setTransform(
							irr::video::ETS_WORLD,
							matWorld
							); 
					}

					//뷰매트릭스만들기
					{
						irr::core::matrix4 matView;
						matView.buildCameraLookAtMatrixLH(
							irr::core::vector3df(0,0,-100), //카메라위치
							irr::core::vector3df(0,0,0), //시점
							irr::core::vector3df(0,1,0) //업벡터
							);
						pVideo->setTransform(irr::video::ETS_VIEW,matView);
					}

					//직교투영 프로잭션 매트릭스
					{
						irr::core::matrix4 matPrj;
						matPrj.buildProjectionMatrixOrthoLH(640,480,1,3000);
						pVideo->setTransform(irr::video::ETS_PROJECTION,matPrj);
					}

					//메트리얼설정
					{
						irr::video::SMaterial m;
						m.Lighting = false; //라이트를꺼야색이제데로나온다.
						m.ZBuffer = false;						
						m.MaterialType = irr::video::EMT_TRANSPARENT_ADD_COLOR;
						m.setTexture(0,pTexture);
						pVideo->setMaterial(m);
					}

					pVideo->drawIndexedTriangleList(triVer,4,triList,2);

				}

				pVideo->endScene();
			}
			pDevice->drop();
		}

	}

	//RTT 직접쓰기예제
	namespace _08
	{

		void main()
		{
			//irr::core::vector3df 벡터1(0,0,0),벡터2(320,240,0);

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9,
				irr::core::dimension2du(640,480) 
				); 

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver(); 
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();

			irr::scene::ICameraSceneNode *pCam1 = pSmgr->addCameraSceneNode(0,irr::core::vector3df(0,5,-15),irr::core::vector3df(0,0,0));
			irr::scene::ICameraSceneNode *pCam2 = pSmgr->addCameraSceneNode(0,irr::core::vector3df(0,5,15),irr::core::vector3df(0,0,0));

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			{
				irr::scene::IAnimatedMeshSceneNode *pNode =
					pSmgr->addAnimatedMeshSceneNode(pSmgr->getMesh("walkers/ninja/ninja.b3d"));
				pNode->setFrameLoop(1,14);
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
			}

			//크기가 1인 3D 평면 만들기
			irr::u16 triList[] = {0,1,2,0,2,3};
			irr::video::S3DVertex triVer[] =
			{
				irr::video::S3DVertex( .5f, .5f,0,  0,0,-1, 0xFFFFFFFF, 1,0),
				irr::video::S3DVertex( .5f,-.5f,0,  0,0,-1, 0xFFFFFFFF, 1,1),
				irr::video::S3DVertex(-.5f,-.5f,0,  0,0,-1, 0xFFFFFFFF, 0,1),
				irr::video::S3DVertex(-.5f, .5f,0,  0,0,-1, 0xFFFFFFFF, 0,0)
			};

			//irr::video::ITexture *pTexture = pVideo->getTexture("sean7706.jpg");

			irr::video::ITexture *pTexture=0;

			if (pVideo->queryFeature(irr::video::EVDF_RENDER_TO_TARGET))
			{
				pTexture = pVideo->addRenderTargetTexture(irr::core::dimension2du(256,256), "RTT1");				
			}
			else
			{
				std::cout << "Your hardware or this renderer is not able to use the " << std::endl;				
			}



			while(pDevice->run())
			{
				pVideo->beginScene();	
				

				if(pTexture)
				{	
					//텍스춰에 먼저 랜더링
					pVideo->setRenderTarget(pTexture,true,true,irr::video::SColor(0,0,0,255));
					pSmgr->setActiveCamera(pCam2);
					pSmgr->drawAll();

					pVideo->setRenderTarget(0);
					pSmgr->setActiveCamera(pCam1);
					pSmgr->drawAll();

					//월드변환
					{
						irr::core::matrix4 matWorld,matWorld_scl,matWorld_rot,matWorld_trn;

						matWorld_trn.setTranslation(irr::core::vector3df(
							(irr::f32)pVideo->getViewPort().getWidth()/4,
							-(irr::f32)pVideo->getViewPort().getHeight()/4,0));
						matWorld_rot.setRotationDegrees(irr::core::vector3df(0,0,0));
						matWorld_scl.setScale(irr::core::vector3df(
							(irr::f32)pVideo->getViewPort().getWidth()/2, 
							(irr::f32)pVideo->getViewPort().getHeight()/2,
							0)
							);
						/*matWorld_trn.setTranslation(irr::core::vector3df(0,0,0));
							
						matWorld_rot.setRotationDegrees(irr::core::vector3df(0,0,0));
						matWorld_scl.setScale(irr::core::vector3df(
							(irr::f32)pVideo->getViewPort().getWidth(), 
							(irr::f32)pVideo->getViewPort().getHeight(),
							0)
							);*/

						matWorld = matWorld_trn * matWorld_rot * matWorld_scl;

						pVideo->setTransform(
							irr::video::ETS_WORLD,
							matWorld
							); 
					}

					//뷰매트릭스만들기
					{
						irr::core::matrix4 matView;
						matView.buildCameraLookAtMatrixLH(
							irr::core::vector3df(0,0,-100), //카메라위치
							irr::core::vector3df(0,0,0), //시점
							irr::core::vector3df(0,1,0) //업벡터
							);
						pVideo->setTransform(irr::video::ETS_VIEW,matView);
					}

					//직교투영 프로잭션 매트릭스
					{
						irr::core::matrix4 matPrj;
						matPrj.buildProjectionMatrixOrthoLH(640,480,1,3000);
						pVideo->setTransform(irr::video::ETS_PROJECTION,matPrj);
					}

					//메트리얼설정
					{
						irr::video::SMaterial m;
						m.Lighting = false; //라이트를꺼야색이제데로나온다.
						m.ZBuffer = false;						
						m.MaterialType = irr::video::EMT_TRANSPARENT_ADD_COLOR;
						m.setTexture(0,pTexture);
						pVideo->setMaterial(m);
					}

					pVideo->drawIndexedTriangleList(triVer,4,triList,2);

				}

				pVideo->endScene();
			}
			pDevice->drop();
		}

	}
}



