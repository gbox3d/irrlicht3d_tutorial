﻿#include <irrlicht.h>
#include <iostream>
#include <iomanip>
using namespace std;

//메쉬 편집하기 예제( 일명 메쉬 더럽히기 예)
//힐플래인 메쉬 이용


namespace Lesson05
{
	namespace _00
	{		
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9,
#if(IRRLICHT_VERSION_MAJOR == 1 && IRRLICHT_VERSION_MINOR >= 6)
				irr::core::dimension2d<irr::u32>(640, 480),
#else
				irr::core::dimension2d<irr::s32>(640, 480), 
#endif
				32,
				false, false, false,
				NULL
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,50,-20), irr::core::vector3df(0,5,0));


			{
				irr::scene::ISceneNode* pNode = 0;
				irr::scene::IAnimatedMesh* pMesh;			

				pMesh = pSmgr->addHillPlaneMesh(
					"usr/mesh/myHill",
					irr::core::dimension2d<irr::f32>(8,8),
					irr::core::dimension2d<irr::u32>(1,1), 
					0,0,
					irr::core::dimension2d<irr::f32>(0,0),
					irr::core::dimension2d<irr::f32>(4,4));

				pNode = pSmgr->addAnimatedMeshSceneNode(pMesh);
				pNode->setPosition(irr::core::vector3df(0,0,0));		
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);
			}


			{
				irr::scene::IAnimatedMesh* pMesh;

				pMesh = pSmgr->getMesh("usr/mesh/myHill");

				irr::scene::IMeshBuffer *pmb = pMesh->getMeshBuffer(0);
				irr::video::S3DVertex *pVb = (irr::video::S3DVertex *)pmb->getVertices();	

				irr::u32 i;
				for(i=0;i<pmb->getVertexCount();i++)
				{
					cout << setw(4) << i << setw(8) << pVb[i].Pos.X << setw(8) << pVb[i].Pos.Y << setw(8) << pVb[i].Pos.Z << endl;
				}	
			}

			{
				irr::scene::IAnimatedMesh* pMesh;

				pMesh = pSmgr->getMesh("usr/mesh/myHill");


				irr::scene::IMeshBuffer *pmb = pMesh->getMeshBuffer(0);
				//메쉬버퍼에서 버텍스 얻어내기
				irr::video::S3DVertex *pVb = (irr::video::S3DVertex *)pmb->getVertices();	

				//버텍스 다루기
				pVb[1].Pos.Y = 4.f;

			}

			//인덱스버퍼 출력
			{
				cout << setw(4) << "num" << setw(8) << "index" << endl;
				irr::scene::IAnimatedMesh* pMesh;
				pMesh = pSmgr->getMesh("usr/mesh/myHill");
				irr::scene::IMeshBuffer *pmb = pMesh->getMeshBuffer(0);
				irr::u16 *pIb = pmb->getIndices();

				irr::u32 i;
				for(i=0;i<pmb->getIndexCount();i++)
				{
					cout << setw(4) << i << setw(8) << pIb[i] << endl;
				}
			}


			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			irr::u32 uLastTick = pDevice->getTimer()->getTime();
			while(pDevice->run())
			{	
				irr::u32 uTick = pDevice->getTimer()->getTime();
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = pDevice->getTimer()->getTime();

				//프레임레이트 갱신
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d",pVideo->getFPS());
					pstextFPS->setText(wszbuf);
				}		

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}


	}

	//
	//힐플래인 메쉬 이용
	// UV좌표 편집
	namespace _01
	{

		//기본 A1형
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9,
#if(IRRLICHT_VERSION_MAJOR == 1 && IRRLICHT_VERSION_MINOR >= 6)
				irr::core::dimension2d<irr::u32>(640, 480),
#else
				irr::core::dimension2d<irr::s32>(640, 480), 
#endif
				32,
				false, false, false,
				NULL
				);
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,50,-20), irr::core::vector3df(0,5,0));


			{
				irr::scene::ISceneNode* pNode = 0;
				irr::scene::IAnimatedMesh* pMesh;			

				pMesh = pSmgr->addHillPlaneMesh(
					"usr/mesh/pannel",
					irr::core::dimension2df(1,1),
					irr::core::dimension2du(1,1)
					);
			}	


			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			irr::u32 uLastTick = pDevice->getTimer()->getTime();
			while(pDevice->run())
			{	
				irr::u32 uTick = pDevice->getTimer()->getTime();
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = pDevice->getTimer()->getTime();

				//프레임레이트 갱신
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d",pVideo->getFPS());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				{
					//월드변환
					{
						irr::core::matrix4 matWorld,matWorld_scl,matWorld_rot,matWorld_trn;

						matWorld_trn.setTranslation(irr::core::vector3df(100,0,0));
						matWorld_rot.setRotationDegrees(irr::core::vector3df(0,45,0));
						matWorld_scl.setScale(irr::core::vector3df(150,1,150));

						matWorld = matWorld_trn * matWorld_rot * matWorld_scl;

						pVideo->setTransform(
							irr::video::ETS_WORLD,
							matWorld
							); 
					}

					//뷰변환
					{
						irr::core::matrix4 matView;
						matView.buildCameraLookAtMatrixLH(
							irr::core::vector3df(0,100,0), //카메라위치
							irr::core::vector3df(0,0,0), //시점
							irr::core::vector3df(0,0,1) //업벡터
							);
						pVideo->setTransform(irr::video::ETS_VIEW,matView);
					}

					//직교투영 프로잭션 매트릭스
					{
						irr::core::matrix4 matPrj;
						matPrj.buildProjectionMatrixOrthoLH(640,480,1,3000);
						pVideo->setTransform(irr::video::ETS_PROJECTION,matPrj);
					}

					//메트리얼설정
					{
						irr::video::ITexture *pTexture = pVideo->getTexture("jga/samurai.jpg");
						irr::video::SMaterial m;
						m.Lighting = false; //라이트를꺼야색이제데로나온다.
						//m.ZBuffer = false;
						m.setTexture(0,pTexture);
						pVideo->setMaterial(m);

					}

					irr::scene::IAnimatedMesh* pMesh;
					pMesh = pSmgr->getMesh("usr/mesh/pannel");

					pVideo->drawMeshBuffer(pMesh->getMeshBuffer(0));
				}		

				pVideo->endScene();	
			}

			pDevice->drop();
		}

	}
}