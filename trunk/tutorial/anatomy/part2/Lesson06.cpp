﻿#include <irrlicht.h>

//스크립트 처리를 위한
//멀티쓰레드 콘솔창 예제

#include <iostream>
#include <string>
#include <windows.h>

using namespace std;

namespace Lesson06
{
	namespace _00
	{

		HANDLE g_hThread_console;
		DWORD g_ThreadID_console;
		std::string g_strMessage;

		DWORD WINAPI ConsoleThread(LPVOID lpPram)
		{
			while(1)
			{	
				char str[256];
				std::cin.getline(str,256);					
				g_strMessage = str;		

				if(g_strMessage == "exit")
					return 0;
				
				//std::cout << "echo : " << g_strMessage << std::endl;			
			}
			return 0;
		}


		void main()
		{	
			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9,
				irr::core::dimension2du(640,480) 
				); 

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver(); 
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");	

			g_hThread_console = ::CreateThread(NULL,0,ConsoleThread,0,0,&g_ThreadID_console);

			irr::scene::ICameraSceneNode *pCam = pSmgr->addCameraSceneNode(0,irr::core::vector3df(0,0,-100),irr::core::vector3df(0,0,0));




			while(pDevice->run())
			{

				//씬노드 추가
				if(g_strMessage == "add cube")
				{
					irr::scene::ISceneNode *pNode =  pSmgr->addCubeSceneNode();				
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
					pNode->setMaterialTexture(0,pVideo->getTexture("t351sml.jpg"));	
					g_strMessage = "";
					std::cout << "add cube success !!" << std::endl;
				}
				if(g_strMessage == "exit")
				{
					//::TerminateThread(g_hThread_console,0);
					//::CloseHandle(g_hThread_console);
					pDevice->closeDevice();
				}

				pVideo->beginScene();
				pSmgr->drawAll();

				pVideo->endScene();
			}			
			pDevice->drop();
			
		}
	}
}



