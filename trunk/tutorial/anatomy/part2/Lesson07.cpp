﻿
#include <irrlicht.h>
#include <math.h>

#include <iostream>
#include <vector>


#ifdef USE_MS_DX9

#include <d3dx9.h>

#include <comutil.h>
#pragma comment (lib,"comsuppw.lib" )



/*
다이랙트엑스 연동해서 다루기
*/

namespace Lesson07 {

	const float INFINITY = FLT_MAX;
	const float EPSILON  = 0.001f;

	struct BoundingBox
	{
		BoundingBox();

		bool isPointInside(D3DXVECTOR3& p);

		D3DXVECTOR3 _min;
		D3DXVECTOR3 _max;
	};

	struct BoundingSphere
	{
		BoundingSphere();

		D3DXVECTOR3 _center;
		float       _radius;
	};

	struct Ray
	{
		D3DXVECTOR3 _origin;
		D3DXVECTOR3 _direction;
	};

	D3DLIGHT9 InitDirectionalLight(D3DXVECTOR3* direction, D3DXCOLOR* color)
	{
		D3DLIGHT9 light;
		::ZeroMemory(&light, sizeof(light));

		light.Type      = D3DLIGHT_DIRECTIONAL;
		light.Ambient   = *color * 0.4f;
		light.Diffuse   = *color;
		light.Specular  = *color * 0.6f;
		light.Direction = *direction;

		return light;
	}

	BoundingBox::BoundingBox()
	{
		// infinite small 
		_min.x = INFINITY;
		_min.y = INFINITY;
		_min.z = INFINITY;

		_max.x = -INFINITY;
		_max.y = -INFINITY;
		_max.z = -INFINITY;
	}

	bool BoundingBox::isPointInside(D3DXVECTOR3& p)
	{
		if( p.x >= _min.x && p.y >= _min.y && p.z >= _min.z &&
			p.x <= _max.x && p.y <= _max.y && p.z <= _max.z )
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	BoundingSphere::BoundingSphere()
	{
		_radius = 0.0f;
	}

	D3DLIGHT9 InitPointLight(D3DXVECTOR3* position, D3DXCOLOR* color)
	{
		D3DLIGHT9 light;
		::ZeroMemory(&light, sizeof(light));

		light.Type      = D3DLIGHT_POINT;
		light.Ambient   = *color * 0.4f;
		light.Diffuse   = *color;
		light.Specular  = *color * 0.6f;
		light.Position  = *position;
		light.Range        = 1000.0f;
		light.Falloff      = 1.0f;
		light.Attenuation0 = 1.0f;
		light.Attenuation1 = 0.0f;
		light.Attenuation2 = 0.0f;

		return light;
	}

	D3DLIGHT9 InitSpotLight(D3DXVECTOR3* position, D3DXVECTOR3* direction, D3DXCOLOR* color)
	{
		D3DLIGHT9 light;
		::ZeroMemory(&light, sizeof(light));

		light.Type      = D3DLIGHT_SPOT;
		light.Ambient   = *color * 0.4f;
		light.Diffuse   = *color;
		light.Specular  = *color * 0.6f;
		light.Position  = *position;
		light.Direction = *direction;
		light.Range        = 1000.0f;
		light.Falloff      = 1.0f;
		light.Attenuation0 = 1.0f;
		light.Attenuation1 = 0.0f;
		light.Attenuation2 = 0.0f;
		light.Theta        = 0.5f;
		light.Phi          = 0.7f;

		return light;
	}

	D3DMATERIAL9 InitMtrl(D3DXCOLOR a, D3DXCOLOR d, D3DXCOLOR s, D3DXCOLOR e, float p)
	{
		D3DMATERIAL9 mtrl;
		mtrl.Ambient  = a;
		mtrl.Diffuse  = d;
		mtrl.Specular = s;
		mtrl.Emissive = e;
		mtrl.Power    = p;
		return mtrl;
	}

	const D3DXCOLOR      WHITE( D3DCOLOR_XRGB(255, 255, 255) );
	const D3DXCOLOR      BLACK( D3DCOLOR_XRGB(  0,   0,   0) );
	const D3DXCOLOR        RED( D3DCOLOR_XRGB(255,   0,   0) );
	const D3DXCOLOR      GREEN( D3DCOLOR_XRGB(  0, 255,   0) );
	const D3DXCOLOR       BLUE( D3DCOLOR_XRGB(  0,   0, 255) );
	const D3DXCOLOR     YELLOW( D3DCOLOR_XRGB(255, 255,   0) );
	const D3DXCOLOR       CYAN( D3DCOLOR_XRGB(  0, 255, 255) );
	const D3DXCOLOR    MAGENTA( D3DCOLOR_XRGB(255,   0, 255) );

	//
	// Materials
	//
	const D3DMATERIAL9 WHITE_MTRL  = InitMtrl(WHITE, WHITE, WHITE, BLACK, 2.0f);
	const D3DMATERIAL9 RED_MTRL    = InitMtrl(RED, RED, RED, BLACK, 2.0f);
	const D3DMATERIAL9 GREEN_MTRL  = InitMtrl(GREEN, GREEN, GREEN, BLACK, 2.0f);
	const D3DMATERIAL9 BLUE_MTRL   = InitMtrl(BLUE, BLUE, BLUE, BLACK, 2.0f);
	const D3DMATERIAL9 YELLOW_MTRL = InitMtrl(YELLOW, YELLOW, YELLOW, BLACK, 2.0f);


	void DX2IrrMatrix(D3DXMATRIX &in,irr::core::matrix4 &out)
	{
		int i,j;
		for(i=0;i<4;i++)
		{
			for(j=0;j<4;j++)
			{
				out(i,j) = in(i,j);

			}
		}
	}

	//DX9 + irrlicht 기본 프레임웍
	namespace _00
	{			
		const int Width  = 640;
		const int Height = 480;

		void main()
		{		

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9,
				irr::core::dimension2du(Width,Height)
				); 

			//다이랙트 3디 디바이스얻기
			LPDIRECT3DDEVICE9 pD9Device = pDevice->getVideoDriver()->getExposedVideoData().D3D9.D3DDev9;			

			while(pDevice->run())
			{
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//esc키누루면 종료
				if(::GetAsyncKeyState(VK_ESCAPE) && 0x8000)
				{
					pDevice->closeDevice();
				}

				//윈도7에서는 랜더링루프에서 매번 변환을 재설정해줘야한다.
				//그렇지않으면 변환정보가 어디에선가에서 리셋이 되는듯하다.
				{
					IDirect3DDevice9* Device = pD9Device;
					
					// Set the projection matrix.
					//투영행렬 설정
					{
						D3DXMATRIX proj;
						D3DXMatrixPerspectiveFovLH(
							&proj,                        // result
							D3DX_PI * 0.5f,               // 90 - degrees
							(float)Width / (float)Height, // aspect ratio
							1.0f,                         // near plane
							1000.0f);                     // far plane
						Device->SetTransform(D3DTS_PROJECTION, &proj);
					}

					//뷰행렬 설정
					//카메라 세팅
					{
						D3DXVECTOR3 position(0.0f, 0.0f, -1.0f);
						D3DXVECTOR3 target(0.0f, 0.0f, 0.0f);
						D3DXVECTOR3 up(0.0f, 1.0f, 0.0f);
						D3DXMATRIX V;
						D3DXMatrixLookAtLH(&V, &position, &target, &up);

						Device->SetTransform(D3DTS_VIEW, &V);
					}					

				}

				pD9Device->Clear(0,0,D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER,0x000000ff,1.0f,0);

				pD9Device->Present(0,0,0,0);
			}
			pDevice->drop();
		}

	}	

	//삼각형 출력해보기 예제
	namespace _01
	{	
		
		struct Vertex
		{
		Vertex(){}

		Vertex(float x, float y, float z)
		{
		_x = x;	 _y = y;  _z = z;
		}

		float _x, _y, _z;

		static const DWORD FVF;
		};
		const DWORD Vertex::FVF = D3DFVF_XYZ;

		const int Width  = 640;
		const int Height = 480;

		void main()
		{		

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9  
				); 
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();

			//다이랙트 3디 디바이스얻기

			LPDIRECT3DDEVICE9 pD9Device = pDevice->getVideoDriver()->getExposedVideoData().D3D9.D3DDev9;


			LPDIRECT3DVERTEXBUFFER9 Triangle;			

			//준비
			{
				IDirect3DDevice9* Device = pD9Device;
				Device->CreateVertexBuffer(
					3 * sizeof(Vertex), // size in bytes
					D3DUSAGE_WRITEONLY, // flags
					Vertex::FVF,        // vertex format
					D3DPOOL_MANAGED,    // managed memory pool
					&Triangle,          // return create vertex buffer
					0);                 // not used - set to 0

				//
				// Fill the buffers with the triangle data.
				//

				Vertex* vertices;
				Triangle->Lock(0, 0, (void**)&vertices, 0);

				vertices[0] = Vertex(-1.0f, 0.0f, 2.0f);
				vertices[1] = Vertex( 0.0f, 1.0f, 2.0f);
				vertices[2] = Vertex( 1.0f, 0.0f, 2.0f);

				Triangle->Unlock();			

				
			}		

			while(pDevice->run())
			{
				//esc키누루면 종료
				if(::GetAsyncKeyState(VK_ESCAPE) && 0x8000)
				{
					pDevice->closeDevice();
				}

				{
					IDirect3DDevice9* Device = pD9Device;
					//
					// Set the projection matrix.
					//투영행렬 설정

					{
						D3DXMATRIX proj;
						D3DXMatrixPerspectiveFovLH(
							&proj,                        // result
							D3DX_PI * 0.5f,               // 90 - degrees
							(float)Width / (float)Height, // aspect ratio
							1.0f,                         // near plane
							1000.0f);                     // far plane
						Device->SetTransform(D3DTS_PROJECTION, &proj);
					}


					//뷰행렬 설정
					//카메라 세팅
					{
						D3DXVECTOR3 position(0.0f, 0.0f, -1.0f);
						D3DXVECTOR3 target(0.0f, 0.0f, 0.0f);
						D3DXVECTOR3 up(0.0f, 1.0f, 0.0f);
						D3DXMATRIX V;
						D3DXMatrixLookAtLH(&V, &position, &target, &up);

						Device->SetTransform(D3DTS_VIEW, &V);
					}					

				}

				pD9Device->Clear(0,0,D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER,0x000000ff,1.0f,0);

				pD9Device->BeginScene();
				
				DWORD oldFVF;				
				DWORD oldFillMode;
				DWORD oldLighting;

				//이전 랜더링 상태저장
				pD9Device->GetFVF(&oldFVF); 				
				pD9Device->GetRenderState(D3DRS_FILLMODE,&oldFillMode);
				pD9Device->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);				
				pD9Device->GetRenderState(D3DRS_LIGHTING,&oldLighting);
				pD9Device->SetRenderState(D3DRS_LIGHTING,FALSE);


				pD9Device->SetStreamSource(0, Triangle, 0, sizeof(Vertex));
				pD9Device->SetFVF(Vertex::FVF);

				// Draw one triangle.
				pD9Device->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 1);

				//랜더링 상태값복귀
				pD9Device->SetFVF(oldFVF);
				pD9Device->SetRenderState(D3DRS_FILLMODE, oldFillMode);
				pD9Device->SetRenderState(D3DRS_LIGHTING, oldLighting);

				pD9Device->EndScene();			

				pD9Device->Present(0,0,0,0);
			}

			Triangle->Release();

			pDevice->drop();
		}

	}

	namespace _02
	{	
		//DX에서 기본제공하는 오브잭트 생성해서 출력하기 
		//차주전자,원통,도나쓰,구,박스
		void main()
		{		

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9  
				); 			

			const int Width  = 640;
			const int Height = 480;

			// Store 5 objects.
			ID3DXMesh* Objects[5] = {0, 0, 0, 0, 0};
			// World matrices for each object.  These matrices
			// specify the locations of the objects in the world.
			D3DXMATRIX ObjWorldMatrices[5];

			//다이랙트 3디 디바이스얻기
			LPDIRECT3DDEVICE9 pD9Device = pDevice->getVideoDriver()->getExposedVideoData().D3D9.D3DDev9;	

			{
				LPDIRECT3DDEVICE9 Device = pD9Device;
				//
				// Create the objects.
				//

				D3DXCreateTeapot(
					Device,
					&Objects[0],
					0);

				D3DXCreateBox(
					Device,
					2.0f, // width
					2.0f, // height
					2.0f, // depth
					&Objects[1],
					0);

				// cylinder is built aligned on z-axis
				D3DXCreateCylinder(
					Device,
					1.0f, // radius at negative z end
					1.0f, // radius at positive z end
					3.0f, // length of cylinder
					10,   // slices
					10,   // stacks
					&Objects[2],
					0);

				D3DXCreateTorus(
					Device,
					1.0f, // inner radius
					3.0f, // outer radius
					10,   // sides
					10,   // rings
					&Objects[3],
					0);

				D3DXCreateSphere(
					Device,
					1.0f, // radius
					10,   // slices
					10,   // stacks
					&Objects[4],
					0);

				//
				// Build world matrices - position the objects in world space.
				// For example, ObjWorldMatrices[1] will position Objects[1] at
				// (-5, 0, 5).  Likewise, ObjWorldMatrices[2] will position
				// Objects[2] at (5, 0, 5).
				//

				D3DXMatrixTranslation(&ObjWorldMatrices[0],  0.0f, 0.0f,  0.0f);
				D3DXMatrixTranslation(&ObjWorldMatrices[1], -5.0f, 0.0f,  5.0f);
				D3DXMatrixTranslation(&ObjWorldMatrices[2],  5.0f, 0.0f,  5.0f);
				D3DXMatrixTranslation(&ObjWorldMatrices[3], -5.0f, 0.0f, -5.0f);
				D3DXMatrixTranslation(&ObjWorldMatrices[4],  5.0f, 0.0f, -5.0f);

				

			}

			while(pDevice->run())
			{
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//esc키누루면 종료
				if(::GetAsyncKeyState(VK_ESCAPE) && 0x8000)
				{
					pDevice->closeDevice();
				}

				{//카메라 애니메이션
					LPDIRECT3DDEVICE9 Device = pD9Device;
					static float angle = (3.0f * D3DX_PI) / 2.0f;
					static float cameraHeight = 0.0f;
					static float cameraHeightDirection = 5.0f;

					D3DXVECTOR3 position( cosf(angle) * 10.0f, cameraHeight, sinf(angle) * 10.0f );

					// the camera is targetted at the origin of the world
					D3DXVECTOR3 target(0.0f, 0.0f, 0.0f);

					// the worlds up vector
					D3DXVECTOR3 up(0.0f, 1.0f, 0.0f);

					D3DXMATRIX V;
					D3DXMatrixLookAtLH(&V, &position, &target, &up);
					Device->SetTransform(D3DTS_VIEW, &V);

					// compute the position for the next frame
					angle += fDelta;
					if( angle >= 6.28f )
						angle = 0.0f;

					// compute the height of the camera for the next frame
					cameraHeight += cameraHeightDirection * fDelta;
					if( cameraHeight >= 10.0f )
						cameraHeightDirection = -5.0f;

					if( cameraHeight <= -10.0f )
						cameraHeightDirection = 5.0f;
				}


				{
					//
					// Set the projection matrix.
					//					
					LPDIRECT3DDEVICE9 Device = pD9Device;
					D3DXMATRIX proj;
					D3DXMatrixPerspectiveFovLH(
						&proj,
						D3DX_PI * 0.5f, // 90 - degree
						(float)Width / (float)Height,
						1.0f,
						1000.0f);
					Device->SetTransform(D3DTS_PROJECTION, &proj);
				}

				pD9Device->Clear(0,0,D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER,0x000000ff,1.0f,0);

				{
					LPDIRECT3DDEVICE9 Device = pD9Device;

					Device->BeginScene();

					DWORD oldFVF;				
					DWORD oldFillMode;
					DWORD oldLighting;

					//이전 랜더링 상태저장
					pD9Device->GetFVF(&oldFVF); 				
					pD9Device->GetRenderState(D3DRS_FILLMODE,&oldFillMode);
					pD9Device->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);				
					pD9Device->GetRenderState(D3DRS_LIGHTING,&oldLighting);
					pD9Device->SetRenderState(D3DRS_LIGHTING,FALSE);

					int i;
					for(i = 0; i < 5; i++)
					{
						// Set the world matrix that positions the object.
						Device->SetTransform(D3DTS_WORLD, &ObjWorldMatrices[i]);

						// Draw the object using the previously set world matrix.
						Objects[i]->DrawSubset(0);

					}			

					//랜더링 상태값복귀
					pD9Device->SetFVF(oldFVF);
					pD9Device->SetRenderState(D3DRS_FILLMODE, oldFillMode);
					pD9Device->SetRenderState(D3DRS_LIGHTING, oldLighting);

					Device->EndScene();

					


				}

				pD9Device->Present(0,0,0,0);
			}

			//객체 해제
			{
				for(int i = 0; i < 5; i++)
					Objects[i]->Release();
			}

			pDevice->drop();
		}

	}

	//인덱스버퍼 활용
	namespace _03
	{		
		const int Width  = 640;
		const int Height = 480;
		//
		// Classes and Structures
		//
		struct Vertex
		{
			Vertex(){}
			Vertex(float x, float y, float z)
			{
				_x = x;  _y = y;  _z = z;
			}
			float _x, _y, _z;
			static const DWORD FVF;
		};
		const DWORD Vertex::FVF = D3DFVF_XYZ;


		void main()
		{		

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9  
				); 

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			//다이랙트 3디 디바이스얻기
			LPDIRECT3DDEVICE9 pD9Device = pDevice->getVideoDriver()->getExposedVideoData().D3D9.D3DDev9;			


			IDirect3DVertexBuffer9* VB = 0;
			IDirect3DIndexBuffer9*  IB = 0;

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,3,-5), irr::core::vector3df(0,0,0));

			
			{
				IDirect3DDevice9* Device = pD9Device;
				//
				// Create vertex and index buffers.
				//

				Device->CreateVertexBuffer(
					8 * sizeof(Vertex), 
					D3DUSAGE_WRITEONLY,
					Vertex::FVF,
					D3DPOOL_MANAGED,
					&VB,
					0);

				Device->CreateIndexBuffer(
					36 * sizeof(WORD),
					D3DUSAGE_WRITEONLY,
					D3DFMT_INDEX16,
					D3DPOOL_MANAGED,
					&IB,
					0);

				//
				// Fill the buffers with the cube data.
				//

				// define unique vertices:
				Vertex* vertices;
				VB->Lock(0, 0, (void**)&vertices, 0);

				// vertices of a unit cube
				vertices[0] = Vertex(-1.0f, -1.0f, -1.0f);
				vertices[1] = Vertex(-1.0f,  1.0f, -1.0f);
				vertices[2] = Vertex( 1.0f,  1.0f, -1.0f);
				vertices[3] = Vertex( 1.0f, -1.0f, -1.0f);
				vertices[4] = Vertex(-1.0f, -1.0f,  1.0f);
				vertices[5] = Vertex(-1.0f,  1.0f,  1.0f);
				vertices[6] = Vertex( 1.0f,  1.0f,  1.0f);
				vertices[7] = Vertex( 1.0f, -1.0f,  1.0f);

				VB->Unlock();

				// define the triangles of the cube:
				WORD* indices = 0;
				IB->Lock(0, 0, (void**)&indices, 0);

				// front side
				indices[0]  = 0; indices[1]  = 1; indices[2]  = 2;
				indices[3]  = 0; indices[4]  = 2; indices[5]  = 3;

				// back side
				indices[6]  = 4; indices[7]  = 6; indices[8]  = 5;
				indices[9]  = 4; indices[10] = 7; indices[11] = 6;

				// left side
				indices[12] = 4; indices[13] = 5; indices[14] = 1;
				indices[15] = 4; indices[16] = 1; indices[17] = 0;

				// right side
				indices[18] = 3; indices[19] = 2; indices[20] = 6;
				indices[21] = 3; indices[22] = 6; indices[23] = 7;

				// top
				indices[24] = 1; indices[25] = 5; indices[26] = 6;
				indices[27] = 1; indices[28] = 6; indices[29] = 2;

				// bottom
				indices[30] = 4; indices[31] = 0; indices[32] = 3;
				indices[33] = 4; indices[34] = 3; indices[35] = 7;

				IB->Unlock();

				//
				// Position and aim the camera.
				//

				D3DXVECTOR3 position(0.0f, 0.0f, -5.0f);
				D3DXVECTOR3 target(0.0f, 0.0f, 0.0f);
				D3DXVECTOR3 up(0.0f, 1.0f, 0.0f);
				D3DXMATRIX V;
				D3DXMatrixLookAtLH(&V, &position, &target, &up);

				Device->SetTransform(D3DTS_VIEW, &V);

				//
				// Set the projection matrix.
				//

				D3DXMATRIX proj;
				D3DXMatrixPerspectiveFovLH(
					&proj,
					D3DX_PI * 0.5f, // 90 - degree
					(float)Width / (float)Height,
					1.0f,
					1000.0f);
				Device->SetTransform(D3DTS_PROJECTION, &proj);

				//
				// Switch to wireframe mode.
				//

				Device->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);
			}

			{
				//뷰변환 
				D3DXVECTOR3 position(0.0f, 0.0f, -5.0f);
				D3DXVECTOR3 target(0.0f, 0.0f, 0.0f);
				D3DXVECTOR3 up(0.0f, 1.0f, 0.0f);
				D3DXMATRIX V;
				D3DXMatrixLookAtLH(&V, &position, &target, &up);
				pD9Device->SetTransform(D3DTS_VIEW, &V);

				//투영변환
				D3DXMATRIX proj;
				D3DXMatrixPerspectiveFovLH(
					&proj,
					D3DX_PI * 0.5f, // 90 - degree
					(float)Width / (float)Height,
					1.0f,
					1000.0f);
				pD9Device->SetTransform(D3DTS_PROJECTION, &proj);
			}

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);


			while(pDevice->run())
			{
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				if(::GetAsyncKeyState(VK_ESCAPE) && 0x8000)
				{
					pDevice->closeDevice();
				}
				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				

				//pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));
				//pSmgr->drawAll();


				//메트리얼설정
				{
					//노드 그릴때 메트리얼설정을 해주지않으면
					//지유아이출력하면 제데로안됨
					irr::video::SMaterial m;
					m.Lighting = false; //라이트를꺼야색이제데로나온다.
					m.ZBuffer = true;
					//m.TextureLayer[0].Texture = pTex;
					pVideo->setMaterial(m);
				}

				{
					//애니메이션
					IDirect3DDevice9* Device = pD9Device;
					D3DXMATRIX Rx, Ry;

					// rotate 45 degrees on x-axis
					D3DXMatrixRotationX(&Rx, 3.14f / 4.0f);

					// incremement y-rotation angle each frame
					static float y = 0.0f;
					D3DXMatrixRotationY(&Ry, y);
					y += fDelta;

					// reset angle to zero when angle reaches 2*PI
					if( y >= 6.28f )
						y = 0.0f;

					// combine x- and y-axis rotation transformations.
					D3DXMATRIX p = Rx * Ry;

					pD9Device->SetTransform(D3DTS_WORLD, &p);
				}

				pD9Device->Clear(0,0,D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER,0x000000ff,1.0f,0);
				{
					IDirect3DDevice9* Device = pD9Device;
					Device->BeginScene();

					Device->SetStreamSource(0, VB, 0, sizeof(Vertex));
					Device->SetIndices(IB);
					Device->SetFVF(Vertex::FVF);

					// Draw cube.
					Device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 8, 0, 12);
					Device->EndScene();
				}
				pD9Device->Present(0,0,0,0);

				//pD9Device->Clear(0,0,D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER,0x000000ff,1.0f,0);
				//pD9Device->BeginScene();				
				//{				

				//	DWORD oldFVF;
				//	pD9Device->GetFVF(&oldFVF); //이전 랜더링 상태저장

				//	DWORD oldFillMode;
				//	pD9Device->GetRenderState(D3DRS_FILLMODE,&oldFillMode);
				//	pD9Device->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);				
				//	pD9Device->SetRenderState(D3DRS_LIGHTING,FALSE);

				//	pD9Device->SetStreamSource(0, VB, 0, sizeof(Vertex));
				//	pD9Device->SetIndices(IB);
				//	pD9Device->SetFVF(Vertex::FVF);
				//	// Draw cube.
				//	pD9Device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 8, 0, 12);

				//	//랜더링 상태값복귀
				//	pD9Device->SetFVF(oldFVF);
				//	pD9Device->SetRenderState(D3DRS_FILLMODE, oldFillMode);
				//}				
				//pD9Device->EndScene();
				//pD9Device->Present(0,0,0,0);
				
				//pVideo->endScene();
				
			}

			//while(pDevice->run())
			//{
			//	static irr::u32 uLastTick=0;
			//	irr::u32 uTick = pDevice->getTimer()->getTime();						
			//	irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
			//	uLastTick = uTick;

			//	//카메라웍
			//	{
			//		IDirect3DDevice9* Device = pD9Device;
			//		D3DXMATRIX Rx, Ry;

			//		// rotate 45 degrees on x-axis
			//		D3DXMatrixRotationX(&Rx, 3.14f / 4.0f);

			//		// incremement y-rotation angle each frame
			//		static float y = 0.0f;
			//		D3DXMatrixRotationY(&Ry, y);
			//		y += fDelta;

			//		// reset angle to zero when angle reaches 2*PI
			//		if( y >= 6.28f )
			//			y = 0.0f;

			//		// combine x- and y-axis rotation transformations.
			//		D3DXMATRIX p = Rx * Ry;

			//		Device->SetTransform(D3DTS_WORLD, &p);

			//	}

			//	//esc키누루면 종료
			//	if(::GetAsyncKeyState(VK_ESCAPE) && 0x8000)
			//	{
			//		pDevice->closeDevice();
			//	}

			//	pD9Device->Clear(0,0,D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER,0x000000ff,1.0f,0);

			//	{
			//		IDirect3DDevice9* Device = pD9Device;
			//		Device->BeginScene();

			//		Device->SetStreamSource(0, VB, 0, sizeof(Vertex));
			//		Device->SetIndices(IB);
			//		Device->SetFVF(Vertex::FVF);

			//		// Draw cube.
			//		Device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 8, 0, 12);

			//		Device->EndScene();
			//	}

			//	pD9Device->Present(0,0,0,0);
			//}

			VB->Release();
			IB->Release();

			pDevice->drop();
		}
	}
	
	//버텍스컬러 예제
	namespace _04
	{	
		struct ColorVertex
		{
			ColorVertex(){}

			ColorVertex(float x, float y, float z, D3DCOLOR c)
			{
				_x = x;	 _y = y;  _z = z;  _color = c;
			}

			float _x, _y, _z;
			D3DCOLOR _color;

			static const DWORD FVF;
		};
		const DWORD ColorVertex::FVF = D3DFVF_XYZ | D3DFVF_DIFFUSE;

		void main()
		{		

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9  
				); 

			//다이랙트 3디 디바이스얻기
			LPDIRECT3DDEVICE9 pD9Device = pDevice->getVideoDriver()->getExposedVideoData().D3D9.D3DDev9;	

			IDirect3DVertexBuffer9* Triangle = 0; 

			{
				IDirect3DDevice9* Device = pD9Device;
				//
				// Create the vertex buffer.
				//

				Device->CreateVertexBuffer(
					3 * sizeof(ColorVertex), 
					D3DUSAGE_WRITEONLY,
					ColorVertex::FVF,
					D3DPOOL_MANAGED,
					&Triangle,
					0);

				//
				// Fill the buffer with the triangle data.
				//

				ColorVertex* v;
				Triangle->Lock(0, 0, (void**)&v, 0);

				v[0] = ColorVertex(-1.0f, 0.0f, 2.0f, D3DCOLOR_XRGB(255,   0,   0));//빨
				v[1] = ColorVertex( 0.0f, 1.0f, 2.0f, D3DCOLOR_XRGB(  0, 255,   0));//녹
				v[2] = ColorVertex( 1.0f, 0.0f, 2.0f, D3DCOLOR_XRGB(  0,   0, 255));//파

				Triangle->Unlock();
				
			}

			while(pDevice->run())
			{
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//esc키누루면 종료
				if(::GetAsyncKeyState(VK_ESCAPE) && 0x8000)
				{
					pDevice->closeDevice();
				}

				{
					IDirect3DDevice9* Device = pD9Device;
					//
					// Set the projection matrix.
					//

					D3DXMATRIX proj;
					D3DXMatrixPerspectiveFovLH(
						&proj,
						D3DX_PI * 0.5f, // 90 - degree
						(irr::f32)pDevice->getVideoDriver()->getScreenSize().Width/(irr::f32)pDevice->getVideoDriver()->getScreenSize().Height,
						//(float)Width / (float)Height,
						1.0f,
						1000.0f);
					Device->SetTransform(D3DTS_PROJECTION, &proj);

					//뷰행렬 설정
					//카메라 세팅
					{
						D3DXVECTOR3 position(0.0f, 0.0f, -1.0f);
						D3DXVECTOR3 target(0.0f, 0.0f, 0.0f);
						D3DXVECTOR3 up(0.0f, 1.0f, 0.0f);
						D3DXMATRIX V;
						D3DXMatrixLookAtLH(&V, &position, &target, &up);

						Device->SetTransform(D3DTS_VIEW, &V);
					}					
				}

				pD9Device->Clear(0,0,D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER,0x000000ff,1.0f,0);


				{
					IDirect3DDevice9* Device = pD9Device;

					Device->BeginScene();

					Device->SetFVF(ColorVertex::FVF);
					Device->SetStreamSource(0, Triangle, 0, sizeof(ColorVertex));

					D3DXMATRIX WorldMatrix;
					// draw the triangle to the left with flat shading
					D3DXMatrixTranslation(&WorldMatrix, -1.25f, 0.0f, 0.0f);
					Device->SetTransform(D3DTS_WORLD, &WorldMatrix);

					Device->SetRenderState(D3DRS_LIGHTING, false);	
					Device->SetRenderState(D3DRS_SHADEMODE, D3DSHADE_FLAT);
					Device->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 1);

					// draw the triangle to the right with gouraud shading
					D3DXMatrixTranslation(&WorldMatrix, 1.25f, 0.0f, 0.0f);
					Device->SetTransform(D3DTS_WORLD, &WorldMatrix);

					Device->SetRenderState(D3DRS_SHADEMODE, D3DSHADE_GOURAUD);
					Device->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 1);
					Device->EndScene();
				}				

				pD9Device->Present(0,0,0,0);
			}

			Triangle->Release();
			pDevice->drop();
		}

	}

	//방향성광 예제 (피라미드)
	namespace _05
	{	

		const D3DXCOLOR      WHITE( D3DCOLOR_XRGB(255, 255, 255) );
		const D3DXCOLOR      BLACK( D3DCOLOR_XRGB(  0,   0,   0) );
		const D3DXCOLOR        RED( D3DCOLOR_XRGB(255,   0,   0) );
		const D3DXCOLOR      GREEN( D3DCOLOR_XRGB(  0, 255,   0) );
		const D3DXCOLOR       BLUE( D3DCOLOR_XRGB(  0,   0, 255) );
		const D3DXCOLOR     YELLOW( D3DCOLOR_XRGB(255, 255,   0) );
		const D3DXCOLOR       CYAN( D3DCOLOR_XRGB(  0, 255, 255) );
		const D3DXCOLOR    MAGENTA( D3DCOLOR_XRGB(255,   0, 255) );

		struct Vertex
		{
			Vertex(){}

			Vertex(float x, float y, float z, float nx, float ny, float nz)
			{
				_x  = x;  _y  = y;	_z  = z;
				_nx = nx; _ny = ny; _nz = nz;
			}
			float  _x,  _y,  _z;
			float _nx, _ny, _nz;

			static const DWORD FVF;
		};
		const DWORD Vertex::FVF = D3DFVF_XYZ | D3DFVF_NORMAL;

		void main()
		{		

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9  
				); 

			//다이랙트 3디 디바이스얻기
			LPDIRECT3DDEVICE9 pD9Device = pDevice->getVideoDriver()->getExposedVideoData().D3D9.D3DDev9;	
			IDirect3DVertexBuffer9* Pyramid = 0;

			{
				LPDIRECT3DDEVICE9 Device = pD9Device;
				//
				// Turn on lighting.
				//
				Device->SetRenderState(D3DRS_LIGHTING, true);

				//
				// Create the vertex buffer for the pyramid.
				//

				Device->CreateVertexBuffer(
					12 * sizeof(Vertex), 
					D3DUSAGE_WRITEONLY,
					Vertex::FVF,
					D3DPOOL_MANAGED,
					&Pyramid,
					0);

				//
				// Fill the vertex buffer with pyramid data.
				//

				Vertex* v;
				Pyramid->Lock(0, 0, (void**)&v, 0);

				// front face
				v[0] = Vertex(-1.0f, 0.0f, -1.0f, 0.0f, 0.707f, -0.707f);
				v[1] = Vertex( 0.0f, 1.0f,  0.0f, 0.0f, 0.707f, -0.707f);
				v[2] = Vertex( 1.0f, 0.0f, -1.0f, 0.0f, 0.707f, -0.707f);

				// left face
				v[3] = Vertex(-1.0f, 0.0f,  1.0f, -0.707f, 0.707f, 0.0f);
				v[4] = Vertex( 0.0f, 1.0f,  0.0f, -0.707f, 0.707f, 0.0f);
				v[5] = Vertex(-1.0f, 0.0f, -1.0f, -0.707f, 0.707f, 0.0f);

				// right face
				v[6] = Vertex( 1.0f, 0.0f, -1.0f, 0.707f, 0.707f, 0.0f);
				v[7] = Vertex( 0.0f, 1.0f,  0.0f, 0.707f, 0.707f, 0.0f);
				v[8] = Vertex( 1.0f, 0.0f,  1.0f, 0.707f, 0.707f, 0.0f);

				// back face
				v[9]  = Vertex( 1.0f, 0.0f,  1.0f, 0.0f, 0.707f, 0.707f);
				v[10] = Vertex( 0.0f, 1.0f,  0.0f, 0.0f, 0.707f, 0.707f);
				v[11] = Vertex(-1.0f, 0.0f,  1.0f, 0.0f, 0.707f, 0.707f);

				Pyramid->Unlock();
			}

			while(pDevice->run())
			{
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//esc키누루면 종료
				if(::GetAsyncKeyState(VK_ESCAPE) && 0x8000)
				{
					pDevice->closeDevice();
				}

				pD9Device->Clear(0,0,D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER,0x00000000,1.0f,0);

				{
					LPDIRECT3DDEVICE9 Device = pD9Device;
					// 
					// Update the scene: Rotate the pyramid.
					//

					//
					// Create and set the material.
					//
					D3DMATERIAL9 mtrl;

					mtrl.Ambient  = WHITE;//D3DXCOLOR(1,1,1,1);
					mtrl.Diffuse  = WHITE;
					mtrl.Specular = WHITE;
					mtrl.Emissive = BLACK;
					mtrl.Power    = 5.0f;

					Device->SetMaterial(&mtrl);

					//
					// Setup a directional light.
					//

					D3DLIGHT9 dir;
					::ZeroMemory(&dir, sizeof(dir));
					dir.Type      = D3DLIGHT_DIRECTIONAL;
					dir.Diffuse   = WHITE;
					dir.Specular  = WHITE * 0.3f;
					dir.Ambient   = WHITE * 0.6f;
					dir.Direction = D3DXVECTOR3(1.0f, 0.0f, 0.0f);

					//
					// Set and Enable the light.
					//

					Device->SetLight(0, &dir);
					Device->LightEnable(0, true);

					//
					// Turn on specular lighting and instruct Direct3D
					// to renormalize normals.
					//

					Device->SetRenderState(D3DRS_NORMALIZENORMALS, true);
					Device->SetRenderState(D3DRS_SPECULARENABLE, true);

					//
					// Position and aim the camera.
					//

					D3DXVECTOR3 pos(0.0f, 1.0f, -3.0f);
					D3DXVECTOR3 target(0.0f, 0.0f, 0.0f);
					D3DXVECTOR3 up(0.0f, 1.0f, 0.0f);
					D3DXMATRIX V;
					D3DXMatrixLookAtLH(&V, &pos, &target, &up);
					Device->SetTransform(D3DTS_VIEW, &V);

					//
					// Set the projection matrix.
					//

					D3DXMATRIX proj;
					D3DXMatrixPerspectiveFovLH(
						&proj,
						D3DX_PI * 0.5f, // 90 - degree
						(float)pDevice->getVideoDriver()->getScreenSize().Width / (float)pDevice->getVideoDriver()->getScreenSize().Height,
						1.0f,
						1000.0f);
					Device->SetTransform(D3DTS_PROJECTION, &proj);


					//월드변환
					D3DXMATRIX yRot;
					static float y = 0.0f;
					D3DXMatrixRotationY(&yRot, y);
					y += fDelta;
					if( y >= 6.28f )
						y = 0.0f;
					Device->SetTransform(D3DTS_WORLD, &yRot);
					

					//
					// Draw the scene:
					//
					Device->BeginScene();

					Device->SetStreamSource(0, Pyramid, 0, sizeof(Vertex));
					Device->SetFVF(Vertex::FVF);
					Device->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 4);

					Device->EndScene();

				}

				pD9Device->Present(0,0,0,0);
			}

			pDevice->drop();
		}

	}

	//방향성광 예제 (dx 기본 도형)
	namespace _06
	{			
		void main()
		{		

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9  
				); 

			//다이랙트 3디 디바이스얻기
			LPDIRECT3DDEVICE9 pD9Device = pDevice->getVideoDriver()->getExposedVideoData().D3D9.D3DDev9;			


			ID3DXMesh* Objects[4] = {0, 0, 0, 0};
			D3DXMATRIX  Worlds[4];
			D3DMATERIAL9 Mtrls[4];

			{
				LPDIRECT3DDEVICE9 Device = pD9Device;
				//
				// Create objects.
				//

				D3DXCreateTeapot(Device, &Objects[0], 0);
				D3DXCreateSphere(Device, 1.0f, 20, 20, &Objects[1], 0);
				D3DXCreateTorus(Device, 0.5f, 1.0f, 20, 20, &Objects[2], 0);
				D3DXCreateCylinder(Device, 0.5f, 0.5f, 2.0f, 20, 20, &Objects[3], 0);

				//
				// Build world matrices - position the objects in world space.
				//

				D3DXMatrixTranslation(&Worlds[0],  0.0f,  2.0f, 0.0f);
				D3DXMatrixTranslation(&Worlds[1],  0.0f, -2.0f, 0.0f);
				D3DXMatrixTranslation(&Worlds[2], -3.0f,  0.0f, 0.0f);
				D3DXMatrixTranslation(&Worlds[3],  3.0f,  0.0f, 0.0f);
			}

			

			while(pDevice->run())
			{
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//esc키누루면 종료
				if(::GetAsyncKeyState(VK_ESCAPE) && 0x8000)
				{
					pDevice->closeDevice();
				}

				pD9Device->Clear(0,0,D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER,0x000000ff,1.0f,0);

				{
					LPDIRECT3DDEVICE9 Device = pD9Device;
					// 
					// Update the scene: update camera position.
					//

					static float angle  = (3.0f * D3DX_PI) / 2.0f;
					static float height = 5.0f;

					if( ::GetAsyncKeyState(VK_LEFT) & 0x8000f )
						angle -= 0.5f * fDelta;

					if( ::GetAsyncKeyState(VK_RIGHT) & 0x8000f )
						angle += 0.5f * fDelta;

					if( ::GetAsyncKeyState(VK_UP) & 0x8000f )
						height += 5.0f * fDelta;

					if( ::GetAsyncKeyState(VK_DOWN) & 0x8000f )
						height -= 5.0f * fDelta;

					D3DXVECTOR3 position( cosf(angle) * 7.0f, height, sinf(angle) * 7.0f );
					D3DXVECTOR3 target(0.0f, 0.0f, 0.0f);
					D3DXVECTOR3 up(0.0f, 1.0f, 0.0f);
					D3DXMATRIX V;
					D3DXMatrixLookAtLH(&V, &position, &target, &up);

					Device->SetTransform(D3DTS_VIEW, &V);


					//
					// Setup the object's materials.
					//

					{
						D3DMATERIAL9 mtrl;
						mtrl.Ambient  = D3DXCOLOR(1,1,1,1);
						mtrl.Diffuse  = D3DXCOLOR(1,1,1,1);
						mtrl.Specular = D3DXCOLOR(1,1,1,1);
						mtrl.Emissive = D3DXCOLOR(0,0,0,0);
						mtrl.Power    = .2f;
						Mtrls[0] = mtrl;					
					}

					{
						D3DMATERIAL9 mtrl;
						mtrl.Ambient  = D3DXCOLOR(1,1,1,1);
						mtrl.Diffuse  = D3DXCOLOR(1,1,1,1);
						mtrl.Specular = D3DXCOLOR(1,1,1,1);
						mtrl.Emissive = D3DXCOLOR(0,0,0,0);
						mtrl.Power    = .2f;
						Mtrls[1] = mtrl;					
					}

					{
						D3DMATERIAL9 mtrl;
						mtrl.Ambient  = D3DXCOLOR(1,1,1,1);
						mtrl.Diffuse  = D3DXCOLOR(1,1,1,1);
						mtrl.Specular = D3DXCOLOR(1,1,1,1);
						mtrl.Emissive = D3DXCOLOR(0,0,0,0);
						mtrl.Power    = .2f;
						Mtrls[2] = mtrl;					
					}

					{
						D3DMATERIAL9 mtrl;
						mtrl.Ambient  = D3DXCOLOR(1,1,1,1);
						mtrl.Diffuse  = D3DXCOLOR(1,1,1,1);
						mtrl.Specular = D3DXCOLOR(1,1,1,1);
						mtrl.Emissive = D3DXCOLOR(0,0,0,0);
						mtrl.Power    = .2f;
						Mtrls[3] = mtrl;					
					}


					//Mtrls[1] = d3d::BLUE_MTRL;
					//Mtrls[2] = d3d::GREEN_MTRL;
					//Mtrls[3] = d3d::YELLOW_MTRL;

					//
					// Setup a directional light.
					//

					D3DXVECTOR3 dir(1.0f, -0.0f, 0.25f);
					D3DXCOLOR   c = D3DXCOLOR(1,1,1,1);
					D3DLIGHT9 dirLight;
					dirLight.Type      = D3DLIGHT_DIRECTIONAL;
					dirLight.Diffuse   = D3DXCOLOR(1,1,1,1);
					dirLight.Specular  = D3DXCOLOR(1,1,1,1) * 0.3f;
					dirLight.Ambient   = D3DXCOLOR(1,1,1,1) * 0.6f;
					dirLight.Direction = D3DXVECTOR3(1.0f, 0.0f, 0.0f);

					//
					// Set and Enable the light.
					//

					Device->SetLight(0, &dirLight);
					Device->LightEnable(0, true);

					//
					// Set lighting related render states.
					//

					Device->SetRenderState(D3DRS_NORMALIZENORMALS, true);
					Device->SetRenderState(D3DRS_SPECULARENABLE, false);

					//
					// Set the projection matrix.
					//

					D3DXMATRIX proj;
					D3DXMatrixPerspectiveFovLH(
						&proj,
						D3DX_PI * 0.25f, // 45 - degree
						(float)pDevice->getVideoDriver()->getScreenSize().Width / (float)pDevice->getVideoDriver()->getScreenSize().Height,
						1.0f,
						1000.0f);
					Device->SetTransform(D3DTS_PROJECTION, &proj);
					

					//
					// Draw the scene:
					//				
					Device->BeginScene();

					for(int i = 0; i < 4; i++)
					{
						// set material and world matrix for ith object, then render
						// the ith object.
						Device->SetMaterial(&Mtrls[i]);
						Device->SetTransform(D3DTS_WORLD, &Worlds[i]);
						Objects[i]->DrawSubset(0);
					}
					Device->EndScene();
				}
				pD9Device->Present(0,0,0,0);
			}

			pDevice->drop();
		}

	}

	//포인트라이트 예제
	namespace _07
	{			
		void main()
		{		

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9  
				); 

			//다이랙트 3디 디바이스얻기
			LPDIRECT3DDEVICE9 pD9Device = pDevice->getVideoDriver()->getExposedVideoData().D3D9.D3DDev9;			


			ID3DXMesh* Objects[4] = {0, 0, 0, 0};
			D3DXMATRIX  Worlds[4];
			D3DMATERIAL9 Mtrls[4];

			{
				LPDIRECT3DDEVICE9 Device = pD9Device;
				//
				// Create objects.
				//

				D3DXCreateTeapot(Device, &Objects[0], 0);
				D3DXCreateSphere(Device, 1.0f, 20, 20, &Objects[1], 0);
				D3DXCreateTorus(Device, 0.5f, 1.0f, 20, 20, &Objects[2], 0);
				D3DXCreateCylinder(Device, 0.5f, 0.5f, 2.0f, 20, 20, &Objects[3], 0);

				//
				// Build world matrices - position the objects in world space.
				//

				D3DXMatrixTranslation(&Worlds[0],  0.0f,  2.0f, 0.0f);
				D3DXMatrixTranslation(&Worlds[1],  0.0f, -2.0f, 0.0f);
				D3DXMatrixTranslation(&Worlds[2], -3.0f,  0.0f, 0.0f);
				D3DXMatrixTranslation(&Worlds[3],  3.0f,  0.0f, 0.0f);

			

			}

			while(pDevice->run())
			{
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//esc키누루면 종료
				if(::GetAsyncKeyState(VK_ESCAPE) && 0x8000)
				{
					pDevice->closeDevice();
				}

				pD9Device->Clear(0,0,D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER,0x00000000,1.0f,0);

				{
					LPDIRECT3DDEVICE9 Device = pD9Device;

					//
					// Setup the object's materials.
					//

					{
						D3DMATERIAL9 mtrl;
						mtrl.Ambient  = D3DXCOLOR(1,1,1,1);
						mtrl.Diffuse  = D3DXCOLOR(1,1,1,1);
						mtrl.Specular = D3DXCOLOR(1,1,1,1);
						mtrl.Emissive = D3DXCOLOR(0,0,0,0);
						mtrl.Power    = .2f;
						Mtrls[0] = mtrl;					
					}

					{
						D3DMATERIAL9 mtrl;
						mtrl.Ambient  = D3DXCOLOR(1,1,1,1);
						mtrl.Diffuse  = D3DXCOLOR(1,1,1,1);
						mtrl.Specular = D3DXCOLOR(1,1,1,1);
						mtrl.Emissive = D3DXCOLOR(0,0,0,0);
						mtrl.Power    = .2f;
						Mtrls[1] = mtrl;					
					}

					{
						D3DMATERIAL9 mtrl;
						mtrl.Ambient  = D3DXCOLOR(1,1,1,1);
						mtrl.Diffuse  = D3DXCOLOR(1,1,1,1);
						mtrl.Specular = D3DXCOLOR(1,1,1,1);
						mtrl.Emissive = D3DXCOLOR(0,0,0,0);
						mtrl.Power    = .2f;
						Mtrls[2] = mtrl;					
					}

					{
						D3DMATERIAL9 mtrl;
						mtrl.Ambient  = D3DXCOLOR(1,1,1,1);
						mtrl.Diffuse  = D3DXCOLOR(1,1,1,1);
						mtrl.Specular = D3DXCOLOR(1,1,1,1);
						mtrl.Emissive = D3DXCOLOR(0,0,0,0);
						mtrl.Power    = .2f;
						Mtrls[3] = mtrl;					
					}


					//포인트 라이트 세팅				
					//D3DXVECTOR3 pos(0.0f, 0.0f, 0.0f);
					//D3DXCOLOR   c = d3d::WHITE;

					D3DLIGHT9 light;
					::ZeroMemory(&light, sizeof(light));

					light.Type      = D3DLIGHT_POINT;
					light.Ambient   = D3DXCOLOR(1,1,1,1) * 0.6f;
					light.Diffuse   = D3DXCOLOR(1,1,1,1);
					light.Specular  = D3DXCOLOR(1,1,1,1) * 0.6f;
					light.Position  = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
					light.Range        = 1000.0f;
					light.Falloff      = 1.0f;
					light.Attenuation0 = 1.0f;
					light.Attenuation1 = 0.0f;
					light.Attenuation2 = 0.0f;

					//
					// Set and Enable the light.
					//

					Device->SetLight(0, &light);
					Device->LightEnable(0, true);

					//
					// Set lighting related render states.
					//

					Device->SetRenderState(D3DRS_NORMALIZENORMALS, true);
					Device->SetRenderState(D3DRS_SPECULARENABLE, false);

					//
					// Set the projection matrix.
					//

					D3DXMATRIX proj;
					D3DXMatrixPerspectiveFovLH(
						&proj,
						D3DX_PI * 0.25f, // 45 - degree
						(float)pDevice->getVideoDriver()->getScreenSize().Width / (float)pDevice->getVideoDriver()->getScreenSize().Height,
						1.0f,
						1000.0f);
					Device->SetTransform(D3DTS_PROJECTION, &proj);

					// 
					// Update the scene: update camera position.
					//

					static float angle  = (3.0f * D3DX_PI) / 2.0f;
					static float height = 5.0f;

					if( ::GetAsyncKeyState(VK_LEFT) & 0x8000f )
						angle -= 0.5f * fDelta;

					if( ::GetAsyncKeyState(VK_RIGHT) & 0x8000f )
						angle += 0.5f * fDelta;

					if( ::GetAsyncKeyState(VK_UP) & 0x8000f )
						height += 5.0f * fDelta;

					if( ::GetAsyncKeyState(VK_DOWN) & 0x8000f )
						height -= 5.0f * fDelta;

					D3DXVECTOR3 position( cosf(angle) * 7.0f, height, sinf(angle) * 7.0f );
					D3DXVECTOR3 target(0.0f, 0.0f, 0.0f);
					D3DXVECTOR3 up(0.0f, 1.0f, 0.0f);
					D3DXMATRIX V;
					D3DXMatrixLookAtLH(&V, &position, &target, &up);

					Device->SetTransform(D3DTS_VIEW, &V);

					//
					// Draw the scene:
					//				
					Device->BeginScene();

					for(int i = 0; i < 4; i++)
					{
						// set material and world matrix for ith object, then render
						// the ith object.
						Device->SetMaterial(&Mtrls[i]);
						Device->SetTransform(D3DTS_WORLD, &Worlds[i]);
						Objects[i]->DrawSubset(0);
					}

					Device->EndScene();

				}

				pD9Device->Present(0,0,0,0);
			}

			Objects[0]->Release();
			Objects[1]->Release();
			Objects[2]->Release();
			Objects[3]->Release();

			pDevice->drop();
		}

	}

	//스폿라이트 예제
	namespace _08
	{			
		void main()
		{		

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9  
				); 

			//다이랙트 3디 디바이스얻기
			LPDIRECT3DDEVICE9 pD9Device = pDevice->getVideoDriver()->getExposedVideoData().D3D9.D3DDev9;			


			ID3DXMesh* Objects[4] = {0, 0, 0, 0};
			D3DXMATRIX  Worlds[4];
			D3DMATERIAL9 Mtrls[4];

			{
				LPDIRECT3DDEVICE9 Device = pD9Device;
				//
				// Create objects.
				//

				D3DXCreateTeapot(Device, &Objects[0], 0);
				D3DXCreateSphere(Device, 1.0f, 20, 20, &Objects[1], 0);
				D3DXCreateTorus(Device, 0.5f, 1.0f, 20, 20, &Objects[2], 0);
				D3DXCreateCylinder(Device, 0.5f, 0.5f, 2.0f, 20, 20, &Objects[3], 0);

				//
				// Build world matrices - position the objects in world space.
				//

				D3DXMatrixTranslation(&Worlds[0],  0.0f,  2.0f, 0.0f);
				D3DXMatrixTranslation(&Worlds[1],  0.0f, -2.0f, 0.0f);
				D3DXMatrixTranslation(&Worlds[2], -3.0f,  0.0f, 0.0f);
				D3DXMatrixTranslation(&Worlds[3],  3.0f,  0.0f, 0.0f);

				

			}

			while(pDevice->run())
			{
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//esc키누루면 종료
				if(::GetAsyncKeyState(VK_ESCAPE) && 0x8000)
				{
					pDevice->closeDevice();
				}

				pD9Device->Clear(0,0,D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER,0x00000000,1.0f,0);

				{
					LPDIRECT3DDEVICE9 Device = pD9Device;


					//
					// Setup the object's materials.
					//

					{
						D3DMATERIAL9 mtrl;
						mtrl.Ambient  = D3DXCOLOR(1,1,1,1);
						mtrl.Diffuse  = D3DXCOLOR(1,1,1,1);
						mtrl.Specular = D3DXCOLOR(1,1,1,1);
						mtrl.Emissive = D3DXCOLOR(0,0,0,0);
						mtrl.Power    = .2f;
						Mtrls[0] = mtrl;					
					}

					{
						D3DMATERIAL9 mtrl;
						mtrl.Ambient  = D3DXCOLOR(1,1,1,1);
						mtrl.Diffuse  = D3DXCOLOR(1,1,1,1);
						mtrl.Specular = D3DXCOLOR(1,1,1,1);
						mtrl.Emissive = D3DXCOLOR(0,0,0,0);
						mtrl.Power    = .2f;
						Mtrls[1] = mtrl;					
					}

					{
						D3DMATERIAL9 mtrl;
						mtrl.Ambient  = D3DXCOLOR(1,1,1,1);
						mtrl.Diffuse  = D3DXCOLOR(1,1,1,1);
						mtrl.Specular = D3DXCOLOR(1,1,1,1);
						mtrl.Emissive = D3DXCOLOR(0,0,0,0);
						mtrl.Power    = .2f;
						Mtrls[2] = mtrl;					
					}

					{
						D3DMATERIAL9 mtrl;
						mtrl.Ambient  = D3DXCOLOR(1,1,1,1);
						mtrl.Diffuse  = D3DXCOLOR(1,1,1,1);
						mtrl.Specular = D3DXCOLOR(1,1,1,1);
						mtrl.Emissive = D3DXCOLOR(0,0,0,0);
						mtrl.Power    = .2f;
						Mtrls[3] = mtrl;					
					}

					//스폿트라이트 세팅

					D3DXVECTOR3 pos(0.0f, 0.0f, -5.0f);
					D3DXVECTOR3 dir(0.0f, .5f,  1.0f);
					D3DXCOLOR   c = D3DXCOLOR(1,1,1,1);

					D3DLIGHT9 light;
					::ZeroMemory(&light, sizeof(light));

					light.Type      = D3DLIGHT_SPOT;
					light.Ambient   = c * 0.0f;
					light.Diffuse   = c;
					light.Specular  = c * 0.6f;
					light.Position  = pos;
					light.Direction = dir;
					light.Range        = 1000.0f;
					light.Falloff      = 1.0f;
					light.Attenuation0 = 1.0f;
					light.Attenuation1 = 0.0f;
					light.Attenuation2 = 0.0f;
					light.Theta        = 0.4f;
					light.Phi          = 0.9f;

					//
					// Set and Enable the light.
					//

					Device->SetLight(0, &light);
					Device->LightEnable(0, true);

					//
					// Set lighting related render states.
					//

					Device->SetRenderState(D3DRS_NORMALIZENORMALS, true);
					Device->SetRenderState(D3DRS_SPECULARENABLE, true);

					//
					// Set the projection matrix.
					//

					D3DXMATRIX proj;
					D3DXMatrixPerspectiveFovLH(
						&proj,
						D3DX_PI * 0.25f, // 45 - degree
						(float)pDevice->getVideoDriver()->getScreenSize().Width / (float)pDevice->getVideoDriver()->getScreenSize().Height,
						1.0f,
						1000.0f);
					Device->SetTransform(D3DTS_PROJECTION, &proj);

					// 
					// Update the scene: update camera position.
					//

					static float angle  = (3.0f * D3DX_PI) / 2.0f;
					static float height = 5.0f;

					if( ::GetAsyncKeyState(VK_LEFT) & 0x8000f )
						angle -= 0.5f * fDelta;

					if( ::GetAsyncKeyState(VK_RIGHT) & 0x8000f )
						angle += 0.5f * fDelta;

					if( ::GetAsyncKeyState(VK_UP) & 0x8000f )
						height += 5.0f * fDelta;

					if( ::GetAsyncKeyState(VK_DOWN) & 0x8000f )
						height -= 5.0f * fDelta;

					D3DXVECTOR3 position( cosf(angle) * 7.0f, height, sinf(angle) * 7.0f );
					D3DXVECTOR3 target(0.0f, 0.0f, 0.0f);
					D3DXVECTOR3 up(0.0f, 1.0f, 0.0f);
					D3DXMATRIX V;
					D3DXMatrixLookAtLH(&V, &position, &target, &up);

					Device->SetTransform(D3DTS_VIEW, &V);

					//
					// Draw the scene:
					//				
					Device->BeginScene();

					for(int i = 0; i < 4; i++)
					{
						// set material and world matrix for ith object, then render
						// the ith object.
						Device->SetMaterial(&Mtrls[i]);
						Device->SetTransform(D3DTS_WORLD, &Worlds[i]);
						Objects[i]->DrawSubset(0);
					}

					Device->EndScene();

				}

				pD9Device->Present(0,0,0,0);
			}

			pDevice->drop();
		}

	}

	
	//texture 입히기
	namespace _09
	{			
		struct Vertex
		{
			Vertex(){}
			Vertex(
				float x, float y, float z,
				float nx, float ny, float nz,
				float u, float v)
			{
				_x  = x;  _y  = y;  _z  = z;
				_nx = nx; _ny = ny; _nz = nz;
				_u  = u;  _v  = v;
			}
			float _x, _y, _z;
			float _nx, _ny, _nz;
			float _u, _v; // texture coordinates

			static const DWORD FVF;
		};
		const DWORD Vertex::FVF = D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1;

		IDirect3DVertexBuffer9* Quad = 0;
		IDirect3DTexture9*      Tex  = 0;

		void main()
		{		

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9  
				); 

			//다이랙트 3디 디바이스얻기
			LPDIRECT3DDEVICE9 pD9Device = pDevice->getVideoDriver()->getExposedVideoData().D3D9.D3DDev9;						

			{
				LPDIRECT3DDEVICE9 Device = pD9Device;
				//
				// Create the quad vertex buffer and fill it with the
				// quad geoemtry.
				//

				Device->CreateVertexBuffer(
					6 * sizeof(Vertex), 
					D3DUSAGE_WRITEONLY,
					Vertex::FVF,
					D3DPOOL_MANAGED,
					&Quad,
					0);

				Vertex* v;
				Quad->Lock(0, 0, (void**)&v, 0);

				// quad built from two triangles, note texture coordinates:
				v[0] = Vertex(-1.0f, -1.0f, 1.25f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f);
				v[1] = Vertex(-1.0f,  1.0f, 1.25f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f);
				v[2] = Vertex( 1.0f,  1.0f, 1.25f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f);

				v[3] = Vertex(-1.0f, -1.0f, 1.25f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f);
				v[4] = Vertex( 1.0f,  1.0f, 1.25f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f);
				v[5] = Vertex( 1.0f, -1.0f, 1.25f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f);

				Quad->Unlock();

				//
				// Create the texture and set filters.
				//

				D3DXCreateTextureFromFile(
					Device,
					L"../../res/dx5_logo.bmp",
					&Tex);

				
			}		
			

			while(pDevice->run())
			{
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//esc키누루면 종료
				if(::GetAsyncKeyState(VK_ESCAPE) && 0x8000)
				{
					pDevice->closeDevice();
					break;
				}

				pD9Device->Clear(0,0,D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER,0x000000ff,1.0f,0);

				{
					LPDIRECT3DDEVICE9 Device = pD9Device;

					Device->SetTexture(0, Tex);

					Device->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
					Device->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
					Device->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_POINT);

					//
					// Don't use lighting for this sample.
					//
					Device->SetRenderState(D3DRS_LIGHTING, false);

					//뷰메트릭스
					{
						D3DXVECTOR3 position( 0,0,-1);
						D3DXVECTOR3 target(0.0f, 0.0f, 0.0f);
						D3DXVECTOR3 up(0.0f, 1.0f, 0.0f);
						D3DXMATRIX V;
						D3DXMatrixLookAtLH(&V, &position, &target, &up);

						Device->SetTransform(D3DTS_VIEW, &V);
					}
					//
					// Set the projection matrix.
					//

					D3DXMATRIX proj;
					D3DXMatrixPerspectiveFovLH(
						&proj,
						D3DX_PI * 0.5f, // 90 - degree
						//(float)Width / (float)Height,
						(float)pDevice->getVideoDriver()->getScreenSize().Width / (float)pDevice->getVideoDriver()->getScreenSize().Height,
						1.0f,
						1000.0f);
					Device->SetTransform(D3DTS_PROJECTION, &proj);					

					Device->BeginScene();

					Device->SetStreamSource(0, Quad, 0, sizeof(Vertex));
					Device->SetFVF(Vertex::FVF);
					Device->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 2);

					Device->EndScene();
				}

				pD9Device->Present(0,0,0,0);
			}

			pDevice->drop();
		}

	}

	//큐브텍스춰
	namespace _10
	{			
		struct Vertex
		{
			Vertex(){}
			Vertex(
				float x, float y, float z,
				float nx, float ny, float nz,
				float u, float v)
			{
				_x  = x;  _y  = y;  _z  = z;
				_nx = nx; _ny = ny; _nz = nz;
				_u  = u;  _v  = v;
			}
			float _x, _y, _z;
			float _nx, _ny, _nz;
			float _u, _v; // texture coordinates
		};
#define FVF_VERTEX (D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1)


		void main()
		{		

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9  
				); 

			//일리히트 1.7.1버전부터는 동적으로 윈도우 크기조절을 지원하므로 run() 함수가 최초로 호출될때
			//윈도우 리싸이징 처리를 하는데 이때 디엑스정보가 일부 유실이 발생한다(트랜스폼, 메트리얼 설정값등등..)
			//그래서 일부러 처음에 한번 호출후 디엑스 정보를 등록해주면 값이 유실이되는걸막을숭있지만...
			//루프내에서 프리미티브를 그리기전에 매번 다시 설정해주는것을 추천한다.
			pDevice->run();

			//다이랙트 3디 디바이스얻기
			LPDIRECT3DDEVICE9 pD9Device = pDevice->getVideoDriver()->getExposedVideoData().D3D9.D3DDev9;

			IDirect3DVertexBuffer9* _vb;
			IDirect3DIndexBuffer9*  _ib;
			IDirect3DTexture9*      Tex  = 0;

			{
				// save a ptr to the device
				IDirect3DDevice9* _device = pD9Device;

				_device->CreateVertexBuffer(
					24 * sizeof(Vertex), 
					D3DUSAGE_WRITEONLY,
					FVF_VERTEX,
					D3DPOOL_MANAGED,
					&_vb,
					0);

				Vertex* v;
				_vb->Lock(0, 0, (void**)&v, 0);

				// build box

				// fill in the front face vertex data
				v[0] = Vertex(-1.0f, -1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f);
				v[1] = Vertex(-1.0f,  1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f);
				v[2] = Vertex( 1.0f,  1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f);
				v[3] = Vertex( 1.0f, -1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f);

				// fill in the back face vertex data
				v[4] = Vertex(-1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f);
				v[5] = Vertex( 1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f);
				v[6] = Vertex( 1.0f,  1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f);
				v[7] = Vertex(-1.0f,  1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f);

				// fill in the top face vertex data
				v[8]  = Vertex(-1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f);
				v[9]  = Vertex(-1.0f, 1.0f,  1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f);
				v[10] = Vertex( 1.0f, 1.0f,  1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f);
				v[11] = Vertex( 1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f);

				// fill in the bottom face vertex data
				v[12] = Vertex(-1.0f, -1.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f);
				v[13] = Vertex( 1.0f, -1.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f);
				v[14] = Vertex( 1.0f, -1.0f,  1.0f, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f);
				v[15] = Vertex(-1.0f, -1.0f,  1.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f);

				// fill in the left face vertex data
				v[16] = Vertex(-1.0f, -1.0f,  1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f);
				v[17] = Vertex(-1.0f,  1.0f,  1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f);
				v[18] = Vertex(-1.0f,  1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f);
				v[19] = Vertex(-1.0f, -1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f);

				// fill in the right face vertex data
				v[20] = Vertex( 1.0f, -1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f);
				v[21] = Vertex( 1.0f,  1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f);
				v[22] = Vertex( 1.0f,  1.0f,  1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f);
				v[23] = Vertex( 1.0f, -1.0f,  1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f);

				_vb->Unlock();

				_device->CreateIndexBuffer(
					36 * sizeof(WORD),
					D3DUSAGE_WRITEONLY,
					D3DFMT_INDEX16,
					D3DPOOL_MANAGED,
					&_ib,
					0);

				WORD* i = 0;
				_ib->Lock(0, 0, (void**)&i, 0);

				// fill in the front face index data
				i[0] = 0; i[1] = 1; i[2] = 2;
				i[3] = 0; i[4] = 2; i[5] = 3;

				// fill in the back face index data
				i[6] = 4; i[7]  = 5; i[8]  = 6;
				i[9] = 4; i[10] = 6; i[11] = 7;

				// fill in the top face index data
				i[12] = 8; i[13] =  9; i[14] = 10;
				i[15] = 8; i[16] = 10; i[17] = 11;

				// fill in the bottom face index data
				i[18] = 12; i[19] = 13; i[20] = 14;
				i[21] = 12; i[22] = 14; i[23] = 15;

				// fill in the left face index data
				i[24] = 16; i[25] = 17; i[26] = 18;
				i[27] = 16; i[28] = 18; i[29] = 19;

				// fill in the right face index data
				i[30] = 20; i[31] = 21; i[32] = 22;
				i[33] = 20; i[34] = 22; i[35] = 23;

				_ib->Unlock();


				//
				// Create the texture and set filters.
				//

				D3DXCreateTextureFromFile(
					_device,
					L"../../res/dx5_logo.bmp",
					&Tex);

				_device->SetTexture(0, Tex);

				_device->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
				_device->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
				_device->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_POINT);


				D3DXMATRIX proj;
				D3DXMatrixPerspectiveFovLH(
					&proj,
					D3DX_PI * 0.5f, // 90 - degree
					//(float)Width / (float)Height,
					(float)pDevice->getVideoDriver()->getScreenSize().Width / (float)pDevice->getVideoDriver()->getScreenSize().Height,
					1.0f,
					1000.0f);
				_device->SetTransform(D3DTS_PROJECTION, &proj);


				_device->SetRenderState(D3DRS_LIGHTING,false);

			}

			while(pDevice->run())
			{
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//esc키누루면 종료
				if(::GetAsyncKeyState(VK_ESCAPE) && 0x8000)
				{
					pDevice->closeDevice();
				}

				pD9Device->Clear(0,0,D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER,0x000000ff,1.0f,0);

				{
					LPDIRECT3DDEVICE9 _device = pD9Device;


					{
						// 
						// Update the scene: update camera position.
						//

						static float angle  = (3.0f * D3DX_PI) / 2.0f;
						static float height = 2.0f;

						if( ::GetAsyncKeyState(VK_LEFT) & 0x8000f )
							angle -= 0.5f * fDelta;

						if( ::GetAsyncKeyState(VK_RIGHT) & 0x8000f )
							angle += 0.5f * fDelta;

						if( ::GetAsyncKeyState(VK_UP) & 0x8000f )
							height += 5.0f * fDelta;

						if( ::GetAsyncKeyState(VK_DOWN) & 0x8000f )
							height -= 5.0f * fDelta;

						D3DXVECTOR3 position( cosf(angle) * 3.0f, height, sinf(angle) * 3.0f );
						D3DXVECTOR3 target(0.0f, 0.0f, 0.0f);
						D3DXVECTOR3 up(0.0f, 1.0f, 0.0f);
						D3DXMATRIX V;
						D3DXMatrixLookAtLH(&V, &position, &target, &up);

						_device->SetTransform(D3DTS_VIEW, &V);
					}

					_device->BeginScene();
					_device->SetStreamSource(0, _vb, 0, sizeof(Vertex));
					_device->SetIndices(_ib);
					_device->SetFVF(FVF_VERTEX);
					_device->DrawIndexedPrimitive(
						D3DPT_TRIANGLELIST, 
						0,                  
						0,                  
						24,
						0,
						12);
					_device->EndScene();
				}
				pD9Device->Present(0,0,0,0);
			}

			pDevice->drop();
		}

	}


	//텍스춰 어드래스모드 예제
	namespace _11
	{			
		struct Vertex
		{
			Vertex(){}
			Vertex(
				float x, float y, float z,
				float nx, float ny, float nz,
				float u, float v)
			{
				_x  = x;  _y  = y;  _z  = z;
				_nx = nx; _ny = ny; _nz = nz;
				_u  = u;  _v  = v;
			}
			float _x, _y, _z;
			float _nx, _ny, _nz;
			float _u, _v; // texture coordinates

			static const DWORD FVF;
		};
		const DWORD Vertex::FVF = D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1;

		IDirect3DVertexBuffer9* Quad = 0;
		IDirect3DTexture9*      Tex  = 0;

		void main()
		{		

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9  
				); 

			

			//다이랙트 3디 디바이스얻기
			LPDIRECT3DDEVICE9 pD9Device = pDevice->getVideoDriver()->getExposedVideoData().D3D9.D3DDev9;			

			{
				LPDIRECT3DDEVICE9 Device = pD9Device;
				//
				// Create the quad vertex buffer and fill it with the
				// quad geoemtry.
				//

				Device->CreateVertexBuffer(
					6 * sizeof(Vertex), 
					D3DUSAGE_WRITEONLY,
					Vertex::FVF,
					D3DPOOL_MANAGED,
					&Quad,
					0);

				Vertex* v;
				Quad->Lock(0, 0, (void**)&v, 0);

				//// quad built from two triangles, note texture coordinates:
				//v[0] = Vertex(-1.0f, -1.0f, 1.25f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f);
				//v[1] = Vertex(-1.0f,  1.0f, 1.25f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f);
				//v[2] = Vertex( 1.0f,  1.0f, 1.25f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f);

				//v[3] = Vertex(-1.0f, -1.0f, 1.25f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f);
				//v[4] = Vertex( 1.0f,  1.0f, 1.25f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f);
				//v[5] = Vertex( 1.0f, -1.0f, 1.25f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f);

				// quad built from two triangles:
				v[0] = Vertex(-1.0f, -1.0f, 1.25f, 0.0f, 0.0f, -1.0f, 0.0f, 3.0f);
				v[1] = Vertex(-1.0f,  1.0f, 1.25f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f);
				v[2] = Vertex( 1.0f,  1.0f, 1.25f, 0.0f, 0.0f, -1.0f, 3.0f, 0.0f);

				v[3] = Vertex(-1.0f, -1.0f, 1.25f, 0.0f, 0.0f, -1.0f, 0.0f, 3.0f);
				v[4] = Vertex( 1.0f,  1.0f, 1.25f, 0.0f, 0.0f, -1.0f, 3.0f, 0.0f);
				v[5] = Vertex( 1.0f, -1.0f, 1.25f, 0.0f, 0.0f, -1.0f, 3.0f, 3.0f);

				Quad->Unlock();

				//
				// Create the texture and set filters.
				//

				D3DXCreateTextureFromFile(
					Device,
					L"../../res/dx5_logo.bmp",
					&Tex);

				


			}


			while(pDevice->run())
			{
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//esc키누루면 종료
				if(::GetAsyncKeyState(VK_ESCAPE) && 0x8000)
				{
					pDevice->closeDevice();
				}

				pD9Device->Clear(0,0,D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER,0x000000ff,1.0f,0);

				{
					LPDIRECT3DDEVICE9 Device = pD9Device;

					{
						Device->SetTexture(0, Tex);

						Device->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
						Device->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
						Device->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_POINT);

						//
						// Don't use lighting for this sample.
						//
						Device->SetRenderState(D3DRS_LIGHTING, false);

						//뷰메트릭스
						{
							D3DXVECTOR3 position( 0,0,-1);
							D3DXVECTOR3 target(0.0f, 0.0f, 0.0f);
							D3DXVECTOR3 up(0.0f, 1.0f, 0.0f);
							D3DXMATRIX V;
							D3DXMatrixLookAtLH(&V, &position, &target, &up);

							Device->SetTransform(D3DTS_VIEW, &V);
						}
						//
						// Set the projection matrix.
						//

						D3DXMATRIX proj;
						D3DXMatrixPerspectiveFovLH(
							&proj,
							D3DX_PI * 0.5f, // 90 - degree
							//(float)Width / (float)Height,
							(float)pDevice->getVideoDriver()->getScreenSize().Width / (float)pDevice->getVideoDriver()->getScreenSize().Height,
							1.0f,
							1000.0f);
						Device->SetTransform(D3DTS_PROJECTION, &proj);
					}

					{
						// 
						// Update the scene:
						//

						// set wrap address mode
						if( ::GetAsyncKeyState('W') & 0x8000f )
						{
							Device->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
							Device->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);
						}

						// set border color address mode
						if( ::GetAsyncKeyState('B') & 0x8000f )
						{
							Device->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_BORDER);
							Device->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_BORDER);
							Device->SetSamplerState(0,  D3DSAMP_BORDERCOLOR, 0x000000ff);
						}

						// set clamp address mode
						if( ::GetAsyncKeyState('C') & 0x8000f )
						{
							Device->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
							Device->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
						}

						// set mirror address mode
						if( ::GetAsyncKeyState('M') & 0x8000f )
						{
							Device->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_MIRROR);
							Device->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_MIRROR);
						}	

						//
						// Draw the scene:
						//
						//Device->Clear(0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 0xffffffff, 1.0f, 0);
						Device->BeginScene();

						Device->SetStreamSource(0, Quad, 0, sizeof(Vertex));
						Device->SetFVF(Vertex::FVF);
						Device->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 2);

						Device->EndScene();
						//Device->Present(0, 0, 0, 0);
					}
				}

				pD9Device->Present(0,0,0,0);
			}

			pDevice->drop();
		}

	}

	//알파 블랜딩
	namespace _12
	{			

		ID3DXMesh*   Teapot = 0;
		D3DMATERIAL9 TeapotMtrl;

		IDirect3DVertexBuffer9* BkGndQuad = 0;
		IDirect3DTexture9*      BkGndTex  = 0;
		D3DMATERIAL9            BkGndMtrl;

		//
		// Classes and Structures
		//
		struct Vertex
		{
			Vertex(){}
			Vertex(
				float x, float y, float z,
				float nx, float ny, float nz,
				float u, float v)
			{
				_x  = x;  _y  = y;  _z  = z;
				_nx = nx; _ny = ny; _nz = nz;
				_u  = u;  _v  = v;
			}
			float _x, _y, _z;
			float _nx, _ny, _nz;
			float _u, _v; // texture coordinates

			static const DWORD FVF;
		};
		const DWORD Vertex::FVF = D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1;

		void main()
		{		

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9  
				); 

			pDevice->run();

			//다이랙트 3디 디바이스얻기
			LPDIRECT3DDEVICE9 pD9Device = pDevice->getVideoDriver()->getExposedVideoData().D3D9.D3DDev9;

			{
				LPDIRECT3DDEVICE9 Device = pD9Device;
				// 
				// Init Materials
				//

				//TeapotMtrl = d3d::RED_MTRL;
				TeapotMtrl.Ambient = D3DXCOLOR(1,0,0,1);				
				TeapotMtrl.Diffuse = D3DXCOLOR(1,0,0,1);
				TeapotMtrl.Specular  = D3DXCOLOR(1,0,0,1);
				TeapotMtrl.Emissive  = D3DXCOLOR(0,0,0,1);
				TeapotMtrl.Power = 2.0f;
				TeapotMtrl.Diffuse.a = 0.5f; // set alpha to 50% opacity

				//BkGndMtrl = d3d::WHITE_MTRL;
				BkGndMtrl.Ambient = D3DXCOLOR(1,1,1,1);
				BkGndMtrl.Diffuse = D3DXCOLOR(1,1,1,1);
				BkGndMtrl.Specular = D3DXCOLOR(1,1,1,1);
				BkGndMtrl.Power = 2.0f;
				BkGndMtrl.Emissive = D3DXCOLOR(0,0,0,1);


				//
				// Create the teapot.
				//

				D3DXCreateTeapot(Device, &Teapot, 0);

				//
				// Create the background quad.
				//

				Device->CreateVertexBuffer(
					6 * sizeof(Vertex), 
					D3DUSAGE_WRITEONLY,
					Vertex::FVF,
					D3DPOOL_MANAGED,
					&BkGndQuad,
					0);

				Vertex* v;
				BkGndQuad->Lock(0, 0, (void**)&v, 0);

				v[0] = Vertex(-10.0f, -10.0f, 5.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f);
				v[1] = Vertex(-10.0f,  10.0f, 5.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f);
				v[2] = Vertex( 10.0f,  10.0f, 5.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f);

				v[3] = Vertex(-10.0f, -10.0f, 5.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f);
				v[4] = Vertex( 10.0f,  10.0f, 5.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f);
				v[5] = Vertex( 10.0f, -10.0f, 5.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f);

				BkGndQuad->Unlock();

				//
				// Setup a directional light.
				//

				D3DLIGHT9 dir;
				::ZeroMemory(&dir, sizeof(dir));
				dir.Type      = D3DLIGHT_DIRECTIONAL;
				dir.Diffuse   = D3DXCOLOR(1,1,1,1);
				dir.Specular  = D3DXCOLOR(1,1,1,1) * 0.2f;
				dir.Ambient   = D3DXCOLOR(1,1,1,1) * 0.6f;
				dir.Direction = D3DXVECTOR3(0.707f, 0.0f, 0.707f);

				Device->SetLight(0, &dir);
				Device->LightEnable(0, true);

				Device->SetRenderState(D3DRS_NORMALIZENORMALS, true);
				Device->SetRenderState(D3DRS_SPECULARENABLE, true);

				//
				// Create texture and set texture filters.
				//

				D3DXCreateTextureFromFile(
					Device,
					L"../../res/crate.jpg",
					&BkGndTex);

				Device->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
				Device->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
				Device->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_POINT);

				//
				// Set alpha blending states.
				//

				// use alpha in material's diffuse component for alpha
				Device->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_DIFFUSE);
				Device->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);

				// set blending factors so that alpha component determines transparency
				Device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
				Device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

				//
				// Set camera.
				//

				D3DXVECTOR3 pos(0.0f, 0.0f, -3.0f);
				D3DXVECTOR3 target(0.0f, 0.0f, 0.0f);
				D3DXVECTOR3 up(0.0f, 1.0f, 0.0f);
				D3DXMATRIX V;
				D3DXMatrixLookAtLH(&V, &pos, &target, &up);

				Device->SetTransform(D3DTS_VIEW, &V);

				//
				// Set projection matrix.
				//

				D3DXMATRIX proj;
				D3DXMatrixPerspectiveFovLH(
					&proj,
					D3DX_PI * 0.5f, // 90 - degree
					//(float)Width / (float)Height,
					(float)pDevice->getVideoDriver()->getScreenSize().Width / (float)pDevice->getVideoDriver()->getScreenSize().Height,
					1.0f,
					1000.0f);
				Device->SetTransform(D3DTS_PROJECTION, &proj);	
			}




			while(pDevice->run())
			{
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//esc키누루면 종료
				if(::GetAsyncKeyState(VK_ESCAPE) && 0x8000)
				{
					pDevice->closeDevice();
				}

				{
					LPDIRECT3DDEVICE9 Device = pD9Device;
					//
					// Update
					//

					// increase/decrease alpha via keyboard input
					if( ::GetAsyncKeyState('A') & 0x8000 )
						TeapotMtrl.Diffuse.a += 0.3f * fDelta;
					if( ::GetAsyncKeyState('S') & 0x8000 )
						TeapotMtrl.Diffuse.a -= 0.3f * fDelta;

					// force alpha to [0, 1] interval
					if(TeapotMtrl.Diffuse.a > 1.0f)
						TeapotMtrl.Diffuse.a = 1.0f;
					if(TeapotMtrl.Diffuse.a < 0.0f)
						TeapotMtrl.Diffuse.a = 0.0f;

					//
					// Render
					//

					Device->Clear(0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 0xffffffff, 1.0f, 0);
					Device->BeginScene();

					// Draw the background
					D3DXMATRIX W;
					D3DXMatrixIdentity(&W);
					Device->SetTransform(D3DTS_WORLD, &W);
					Device->SetFVF(Vertex::FVF);
					Device->SetStreamSource(0, BkGndQuad, 0, sizeof(Vertex));
					Device->SetMaterial(&BkGndMtrl);
					Device->SetTexture(0, BkGndTex);
					Device->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 2);

					// Draw the teapot
					Device->SetRenderState(D3DRS_ALPHABLENDENABLE, true);

					D3DXMatrixScaling(&W, 1.5f, 1.5f, 1.5f);
					Device->SetTransform(D3DTS_WORLD, &W);
					Device->SetMaterial(&TeapotMtrl);
					Device->SetTexture(0, 0);
					Teapot->DrawSubset(0);  

					Device->SetRenderState(D3DRS_ALPHABLENDENABLE, false);

					Device->EndScene();
					Device->Present(0, 0, 0, 0);
				}
			}

			pDevice->drop();
		}

	}

	//알파 텍스춰 블랜딩 예제
	namespace _13
	{
		struct Vertex
		{
			Vertex(){}
			Vertex(
				float x, float y, float z,
				float nx, float ny, float nz,
				float u, float v)
			{
				_x  = x;  _y  = y;  _z  = z;
				_nx = nx; _ny = ny; _nz = nz;
				_u  = u;  _v  = v;
			}
			float _x, _y, _z;
			float _nx, _ny, _nz;
			float _u, _v; // texture coordinates
		};
#define FVF_VERTEX (D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1)

		IDirect3DTexture9*      CrateTex  = 0;
		//Cube*                   Box       = 0;
		D3DXMATRIX              CubeWorldMatrix;

		IDirect3DVertexBuffer9* BackDropVB  = 0;
		IDirect3DTexture9*      BackDropTex = 0;

		void main()
		{		

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9  
				); 

			pDevice->run();

			//다이랙트 3디 디바이스얻기
			LPDIRECT3DDEVICE9 pD9Device = pDevice->getVideoDriver()->getExposedVideoData().D3D9.D3DDev9;		

			{
				LPDIRECT3DDEVICE9 Device = pD9Device;
				//
				// Create the BackDrop quad.
				//

				Device->CreateVertexBuffer(
					6 * sizeof(Vertex), 
					D3DUSAGE_WRITEONLY,
					FVF_VERTEX,
					D3DPOOL_MANAGED,
					&BackDropVB,
					0);

				Vertex* v;
				BackDropVB->Lock(0, 0, (void**)&v, 0);

				// quad built from two triangles, note texture coordinates:
				v[0] = Vertex(-10.0f, -10.0f, 5.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f);
				v[1] = Vertex(-10.0f,  10.0f, 5.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f);
				v[2] = Vertex( 10.0f,  10.0f, 5.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f);

				v[3] = Vertex(-10.0f, -10.0f, 5.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f);
				v[4] = Vertex( 10.0f,  10.0f, 5.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f);
				v[5] = Vertex( 10.0f, -10.0f, 5.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f);

				BackDropVB->Unlock();

				//
				// Create the cube.
				//

				//Box = new Cube(Device);

				//
				// Load the textures and set filters.
				//

				D3DXCreateTextureFromFile(
					Device,
					L"../../res/cratewalpha.dds",
					&CrateTex);

				D3DXCreateTextureFromFile(
					Device,
					L"../../res/lobbyxpos.jpg",
					&BackDropTex);

				Device->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
				Device->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
				Device->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_POINT);

				//
				// set alpha blending stuff
				//

				// use alpha channel in texture for alpha
				Device->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
				Device->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);

				// set blending factors so that alpha component determines transparency
				Device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
				Device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

				// 
				// disable lighting
				//
				Device->SetRenderState(D3DRS_LIGHTING, false);

				//
				// set camera
				//
				D3DXVECTOR3 pos(0.0f, 0.f, -2.5f);
				D3DXVECTOR3 target(0.0f, 0.0f, 0.0f);
				D3DXVECTOR3 up(0.0f, 1.0f, 0.0f);

				D3DXMATRIX V;
				D3DXMatrixLookAtLH(
					&V,
					&pos,
					&target,
					&up);

				Device->SetTransform(D3DTS_VIEW, &V);

				//
				// Set projection matrix
				//
				D3DXMATRIX proj;
				D3DXMatrixPerspectiveFovLH(
					&proj,
					D3DX_PI * 0.5f, // 90 - degree
					//(float)Width / (float)Height,
					(float)pDevice->getVideoDriver()->getScreenSize().Width / (float)pDevice->getVideoDriver()->getScreenSize().Height,
					1.0f,
					1000.0f);
				Device->SetTransform(D3DTS_PROJECTION, &proj);


			}

			IDirect3DVertexBuffer9* _vb;
			IDirect3DIndexBuffer9*  _ib;

			{
				// save a ptr to the device
				LPDIRECT3DDEVICE9 _device = pD9Device;

				_device->CreateVertexBuffer(
					24 * sizeof(Vertex), 
					D3DUSAGE_WRITEONLY,
					FVF_VERTEX,
					D3DPOOL_MANAGED,
					&_vb,
					0);

				Vertex* v;
				_vb->Lock(0, 0, (void**)&v, 0);

				// build box

				// fill in the front face vertex data
				v[0] = Vertex(-1.0f, -1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f);
				v[1] = Vertex(-1.0f,  1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f);
				v[2] = Vertex( 1.0f,  1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f);
				v[3] = Vertex( 1.0f, -1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f);

				// fill in the back face vertex data
				v[4] = Vertex(-1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f);
				v[5] = Vertex( 1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f);
				v[6] = Vertex( 1.0f,  1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f);
				v[7] = Vertex(-1.0f,  1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f);

				// fill in the top face vertex data
				v[8]  = Vertex(-1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f);
				v[9]  = Vertex(-1.0f, 1.0f,  1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f);
				v[10] = Vertex( 1.0f, 1.0f,  1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f);
				v[11] = Vertex( 1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f);

				// fill in the bottom face vertex data
				v[12] = Vertex(-1.0f, -1.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f);
				v[13] = Vertex( 1.0f, -1.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f);
				v[14] = Vertex( 1.0f, -1.0f,  1.0f, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f);
				v[15] = Vertex(-1.0f, -1.0f,  1.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f);

				// fill in the left face vertex data
				v[16] = Vertex(-1.0f, -1.0f,  1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f);
				v[17] = Vertex(-1.0f,  1.0f,  1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f);
				v[18] = Vertex(-1.0f,  1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f);
				v[19] = Vertex(-1.0f, -1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f);

				// fill in the right face vertex data
				v[20] = Vertex( 1.0f, -1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f);
				v[21] = Vertex( 1.0f,  1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f);
				v[22] = Vertex( 1.0f,  1.0f,  1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f);
				v[23] = Vertex( 1.0f, -1.0f,  1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f);

				_vb->Unlock();

				_device->CreateIndexBuffer(
					36 * sizeof(WORD),
					D3DUSAGE_WRITEONLY,
					D3DFMT_INDEX16,
					D3DPOOL_MANAGED,
					&_ib,
					0);

				WORD* i = 0;
				_ib->Lock(0, 0, (void**)&i, 0);

				// fill in the front face index data
				i[0] = 0; i[1] = 1; i[2] = 2;
				i[3] = 0; i[4] = 2; i[5] = 3;

				// fill in the back face index data
				i[6] = 4; i[7]  = 5; i[8]  = 6;
				i[9] = 4; i[10] = 6; i[11] = 7;

				// fill in the top face index data
				i[12] = 8; i[13] =  9; i[14] = 10;
				i[15] = 8; i[16] = 10; i[17] = 11;

				// fill in the bottom face index data
				i[18] = 12; i[19] = 13; i[20] = 14;
				i[21] = 12; i[22] = 14; i[23] = 15;

				// fill in the left face index data
				i[24] = 16; i[25] = 17; i[26] = 18;
				i[27] = 16; i[28] = 18; i[29] = 19;

				// fill in the right face index data
				i[30] = 20; i[31] = 21; i[32] = 22;
				i[33] = 20; i[34] = 22; i[35] = 23;

				_ib->Unlock();
			}


			while(pDevice->run())
			{
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//esc키누루면 종료
				if(::GetAsyncKeyState(VK_ESCAPE) && 0x8000)
				{
					pDevice->closeDevice();
				}

				{
					LPDIRECT3DDEVICE9 Device = pD9Device;
					//
					// Update: Rotate the cube.
					//

					D3DXMATRIX xRot;
					D3DXMatrixRotationX(&xRot, D3DX_PI * 0.2f);

					static float y = 0.0f;
					D3DXMATRIX yRot;
					D3DXMatrixRotationY(&yRot, y);
					y += fDelta;

					if( y >= 6.28f )
						y = 0.0f;

					CubeWorldMatrix = xRot * yRot;

					//
					// Render
					//

					Device->Clear(0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 0xff0000ff, 1.0f, 0);
					Device->BeginScene();

					// draw back drop
					D3DXMATRIX I;
					D3DXMatrixIdentity(&I);
					Device->SetStreamSource(0, BackDropVB, 0, sizeof(Vertex));
					Device->SetFVF(FVF_VERTEX);
					Device->SetTexture(0, BackDropTex);
					Device->SetTransform(D3DTS_WORLD, &I);
					Device->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 2);

					// draw cube
					Device->SetRenderState(D3DRS_ALPHABLENDENABLE, true);
					//if(Box)
					//	Box->draw(&CubeWorldMatrix, 0, CrateTex);

					{
						LPDIRECT3DDEVICE9 _device = pD9Device;
						//if( world )
						_device->SetTransform(D3DTS_WORLD, &CubeWorldMatrix);
						//if( mtrl )
						//	_device->SetMaterial(mtrl);
						//if( tex )
						_device->SetTexture(0, CrateTex);

						_device->SetStreamSource(0, _vb, 0, sizeof(Vertex));
						_device->SetIndices(_ib);
						_device->SetFVF(FVF_VERTEX);
						_device->DrawIndexedPrimitive(
							D3DPT_TRIANGLELIST, 
							0,                  
							0,                  
							24,
							0,
							12);  
					}

					Device->SetRenderState(D3DRS_ALPHABLENDENABLE, false);
					Device->EndScene();
					Device->Present(0, 0, 0, 0);
				}
			}
			pDevice->drop();
		}
	}


	// 거울 변환 메트릭스 예제 
	namespace _14
	{			

		IDirect3DVertexBuffer9* VB = 0;

		IDirect3DTexture9* FloorTex  = 0;
		IDirect3DTexture9* WallTex   = 0;
		IDirect3DTexture9* MirrorTex = 0;

		D3DMATERIAL9 FloorMtrl; // = d3d::WHITE_MTRL;
		D3DMATERIAL9 WallMtrl;  // = d3d::WHITE_MTRL;
		D3DMATERIAL9 MirrorMtrl;// = d3d::WHITE_MTRL;

		ID3DXMesh* Teapot = 0;
		D3DXVECTOR3 TeapotPosition(0.0f, 3.0f, -7.5f);
		D3DMATERIAL9 TeapotMtrl;// = d3d::YELLOW_MTRL;

		struct Vertex
		{
			Vertex(){}
			Vertex(float x, float y, float z, 
				float nx, float ny, float nz,
				float u, float v)
			{
				_x  = x;  _y  = y;  _z  = z;
				_nx = nx; _ny = ny; _nz = nz;
				_u  = u;  _v  = v;
			}
			float _x, _y, _z;
			float _nx, _ny, _nz;
			float _u, _v;

			static const DWORD FVF;
		};
		const DWORD Vertex::FVF = D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1;

		void main()
		{		

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9,
				irr::core::dimension2du(640,480),
				32,
				false,
				true
				); 

			pDevice->run();

			//다이랙트 3디 디바이스얻기
			LPDIRECT3DDEVICE9 pD9Device = pDevice->getVideoDriver()->getExposedVideoData().D3D9.D3DDev9;	


			{
				LPDIRECT3DDEVICE9 Device =  pD9Device;

				D3DMATERIAL9 white_mtl;
				white_mtl.Ambient = D3DXCOLOR(1,1,1,1);
				white_mtl.Diffuse = D3DXCOLOR(1,1,1,1);
				white_mtl.Specular = D3DXCOLOR(1,1,1,1);
				white_mtl.Emissive = D3DXCOLOR(0,0,0,1);
				white_mtl.Power = 2.0f;

				D3DMATERIAL9 yellow_mtl;
				yellow_mtl.Ambient = D3DXCOLOR(1,1,0,1);
				yellow_mtl.Diffuse = D3DXCOLOR(1,1,0,1);
				yellow_mtl.Specular = D3DXCOLOR(1,1,0,1);
				yellow_mtl.Emissive = D3DXCOLOR(0,0,0,1);
				yellow_mtl.Power = 2.0f;

				FloorMtrl = white_mtl;
				WallMtrl = white_mtl;
				MirrorMtrl = white_mtl;
				TeapotMtrl = yellow_mtl;

				//
				// Make walls have low specular reflectance - 20%.
				//

				WallMtrl.Specular = D3DXCOLOR(1,1,1,1) * 0.2f;

				//
				// Create the teapot.
				//

				D3DXCreateTeapot(Device, &Teapot, 0);				

				//
				// Lights.
				//

				D3DXVECTOR3 lightDir(0.707f, -0.707f, 0.707f);
				D3DXCOLOR color(1.0f, 1.0f, 1.0f, 1.0f);
				//D3DLIGHT9 light;


				D3DLIGHT9 light;
				::ZeroMemory(&light, sizeof(light));
				light.Type      = D3DLIGHT_DIRECTIONAL;
				light.Ambient   = color * 0.4f;
				light.Diffuse   = color;
				light.Specular  = color * 0.6f;
				light.Direction = lightDir;

				//D3DLIGHT9 light = d3d::InitDirectionalLight(&lightDir, &color);
				Device->SetLight(0, &light);
				Device->LightEnable(0, true);

				Device->SetRenderState(D3DRS_NORMALIZENORMALS, true);
				Device->SetRenderState(D3DRS_SPECULARENABLE, true);

				//
				// Set Camera.
				//

				D3DXVECTOR3    pos(-10.0f, 3.0f, -15.0f);
				D3DXVECTOR3 target(0.0, 0.0f, 0.0f);
				D3DXVECTOR3     up(0.0f, 1.0f, 0.0f);

				D3DXMATRIX V;
				D3DXMatrixLookAtLH(&V, &pos, &target, &up);

				Device->SetTransform(D3DTS_VIEW, &V);

				//
				// Set projection matrix.
				//
				D3DXMATRIX proj;
				D3DXMatrixPerspectiveFovLH(
					&proj,
					D3DX_PI / 4.0f, // 45 - degree
					//(float)Width / (float)Height,
					(float)pDevice->getVideoDriver()->getScreenSize().Width / (float)pDevice->getVideoDriver()->getScreenSize().Height,
					1.0f,
					1000.0f);
				Device->SetTransform(D3DTS_PROJECTION, &proj);	
			}

			while(pDevice->run())
			{
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//esc키누루면 종료
				if(::GetAsyncKeyState(VK_ESCAPE) && 0x8000)
				{
					pDevice->closeDevice();
				}

				// 시점 조절
				{
					LPDIRECT3DDEVICE9 Device =  pD9Device;
					static float radius = 20.0f;

					if( ::GetAsyncKeyState(VK_LEFT) & 0x8000f )
						TeapotPosition.x -= 3.0f * fDelta;

					if( ::GetAsyncKeyState(VK_RIGHT) & 0x8000f )
						TeapotPosition.x += 3.0f * fDelta;

					if( ::GetAsyncKeyState(VK_UP) & 0x8000f )
						radius -= 2.0f * fDelta;

					if( ::GetAsyncKeyState(VK_DOWN) & 0x8000f )
						radius += 2.0f * fDelta;


					static float angle = (3.0f * D3DX_PI) / 2.0f;

					if( ::GetAsyncKeyState('A') & 0x8000f )
						angle -= 0.5f * fDelta;

					if( ::GetAsyncKeyState('S') & 0x8000f )
						angle += 0.5f * fDelta;

					D3DXVECTOR3 position( cosf(angle) * radius, 3.0f, sinf(angle) * radius );
					D3DXVECTOR3 target(0.0f, 0.0f, 0.0f);
					D3DXVECTOR3 up(0.0f, 1.0f, 0.0f);
					D3DXMATRIX V;
					D3DXMatrixLookAtLH(&V, &position, &target, &up);
					Device->SetTransform(D3DTS_VIEW, &V);
				}

				//
				// Draw the scene:
				//				

				pD9Device->Clear(0,0,
					D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER | D3DCLEAR_STENCIL,
					0x000000ff,1.0f,0);

				pD9Device->BeginScene();


				{
					LPDIRECT3DDEVICE9 Device =  pD9Device;
					// draw teapot
					Device->SetMaterial(&TeapotMtrl);
					Device->SetTexture(0, 0);
					D3DXMATRIX W;
					D3DXMatrixTranslation(&W,
						TeapotPosition.x, 
						TeapotPosition.y,
						TeapotPosition.z);

					Device->SetTransform(D3DTS_WORLD, &W);
					Teapot->DrawSubset(0);											
				}	

				{
					LPDIRECT3DDEVICE9 Device =  pD9Device;

					D3DXMATRIX W, T, R;

					//방향이 (0,0,1)이고 원점과 거리가 0인평면
					D3DXPLANE plane(0.0f, 0.0f, 1.0f, 0.0f); // xy plane

					D3DXMatrixReflect(&R, &plane);


					D3DXMatrixTranslation(&T,
						TeapotPosition.x, 
						TeapotPosition.y,
						TeapotPosition.z); 

					W = T * R;					

					// Finally, draw the reflected teapot
					Device->SetTransform(D3DTS_WORLD, &W);
					Device->SetMaterial(&TeapotMtrl);
					Device->SetTexture(0, 0);

					Device->SetRenderState(D3DRS_CULLMODE, D3DCULL_CW);
					Teapot->DrawSubset(0);
					Device->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
				}				

				pD9Device->EndScene();

				pD9Device->Present(0,0,0,0);
			}

			pDevice->drop();
		}

	}

	//스텐실 응용 거울 효과
	namespace _15
	{			

		IDirect3DVertexBuffer9* VB = 0;

		IDirect3DTexture9* FloorTex  = 0;
		IDirect3DTexture9* WallTex   = 0;
		IDirect3DTexture9* MirrorTex = 0;

		D3DMATERIAL9 FloorMtrl; // = d3d::WHITE_MTRL;
		D3DMATERIAL9 WallMtrl;  // = d3d::WHITE_MTRL;
		D3DMATERIAL9 MirrorMtrl;// = d3d::WHITE_MTRL;

		ID3DXMesh* Teapot = 0;
		D3DXVECTOR3 TeapotPosition(0.0f, 3.0f, -7.5f);
		D3DMATERIAL9 TeapotMtrl;// = d3d::YELLOW_MTRL;

		struct Vertex
		{
			Vertex(){}
			Vertex(float x, float y, float z, 
				float nx, float ny, float nz,
				float u, float v)
			{
				_x  = x;  _y  = y;  _z  = z;
				_nx = nx; _ny = ny; _nz = nz;
				_u  = u;  _v  = v;
			}
			float _x, _y, _z;
			float _nx, _ny, _nz;
			float _u, _v;

			static const DWORD FVF;
		};
		const DWORD Vertex::FVF = D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1;

		void main()
		{		

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9,
				irr::core::dimension2du(640,480),
				32,
				false,
				true
				); 

			pDevice->run();

			//다이랙트 3디 디바이스얻기
			LPDIRECT3DDEVICE9 pD9Device = pDevice->getVideoDriver()->getExposedVideoData().D3D9.D3DDev9;	


			{
				LPDIRECT3DDEVICE9 Device =  pD9Device;

				D3DMATERIAL9 white_mtl;
				white_mtl.Ambient = D3DXCOLOR(1,1,1,1);
				white_mtl.Diffuse = D3DXCOLOR(1,1,1,1);
				white_mtl.Specular = D3DXCOLOR(1,1,1,1);
				white_mtl.Emissive = D3DXCOLOR(0,0,0,1);
				white_mtl.Power = 2.0f;

				D3DMATERIAL9 yellow_mtl;
				yellow_mtl.Ambient = D3DXCOLOR(1,1,0,1);
				yellow_mtl.Diffuse = D3DXCOLOR(1,1,0,1);
				yellow_mtl.Specular = D3DXCOLOR(1,1,0,1);
				yellow_mtl.Emissive = D3DXCOLOR(0,0,0,1);
				yellow_mtl.Power = 2.0f;

				FloorMtrl = white_mtl;
				WallMtrl = white_mtl;
				MirrorMtrl = white_mtl;
				TeapotMtrl = yellow_mtl;

				//
				// Make walls have low specular reflectance - 20%.
				//

				WallMtrl.Specular = D3DXCOLOR(1,1,1,1) * 0.2f;

				//
				// Create the teapot.
				//

				D3DXCreateTeapot(Device, &Teapot, 0);

				//
				// Create and specify geometry.  For this sample we draw a floor
				// and a wall with a mirror on it.  We put the floor, wall, and
				// mirror geometry in one vertex buffer.
				//
				//   |----|----|----|
				//   |Wall|Mirr|Wall|
				//   |    | or |    |
				//   /--------------/
				//  /   Floor      /
				// /--------------/
				//
				Device->CreateVertexBuffer(
					24 * sizeof(Vertex),
					0, // usage
					Vertex::FVF,
					D3DPOOL_MANAGED,
					&VB,
					0);

				Vertex* v = 0;
				VB->Lock(0, 0, (void**)&v, 0);

				// floor
				v[0] = Vertex(-7.5f, 0.0f, -10.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f);
				v[1] = Vertex(-7.5f, 0.0f,   0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f);
				v[2] = Vertex( 7.5f, 0.0f,   0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f);

				v[3] = Vertex(-7.5f, 0.0f, -10.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f);
				v[4] = Vertex( 7.5f, 0.0f,   0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f);
				v[5] = Vertex( 7.5f, 0.0f, -10.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f);

				// wall
				v[6]  = Vertex(-7.5f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f);
				v[7]  = Vertex(-7.5f, 5.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f);
				v[8]  = Vertex(-2.5f, 5.0f, 0.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f);

				v[9]  = Vertex(-7.5f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f);
				v[10] = Vertex(-2.5f, 5.0f, 0.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f);
				v[11] = Vertex(-2.5f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f);

				// Note: We leave gap in middle of walls for mirror

				v[12] = Vertex(2.5f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f);
				v[13] = Vertex(2.5f, 5.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f);
				v[14] = Vertex(7.5f, 5.0f, 0.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f);

				v[15] = Vertex(2.5f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f);
				v[16] = Vertex(7.5f, 5.0f, 0.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f);
				v[17] = Vertex(7.5f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f);

				// mirror
				v[18] = Vertex(-2.5f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f);
				v[19] = Vertex(-2.5f, 5.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f);
				v[20] = Vertex( 2.5f, 5.0f, 0.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f);

				v[21] = Vertex(-2.5f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f);
				v[22] = Vertex( 2.5f, 5.0f, 0.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f);
				v[23] = Vertex( 2.5f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f);

				VB->Unlock();

				//
				// Load Textures, set filters.
				//

				D3DXCreateTextureFromFile(Device, L"../../res/dx_media/checker.jpg", &FloorTex);
				D3DXCreateTextureFromFile(Device, L"../../res/dx_media/brick0.jpg", &WallTex);
				D3DXCreateTextureFromFile(Device, L"../../res/dx_media/ice.bmp", &MirrorTex);

				Device->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
				Device->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
				Device->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR);

				//
				// Lights.
				//

				D3DXVECTOR3 lightDir(0.707f, -0.707f, 0.707f);
				D3DXCOLOR color(1.0f, 1.0f, 1.0f, 1.0f);
				//D3DLIGHT9 light;


				D3DLIGHT9 light;
				::ZeroMemory(&light, sizeof(light));
				light.Type      = D3DLIGHT_DIRECTIONAL;
				light.Ambient   = color * 0.4f;
				light.Diffuse   = color;
				light.Specular  = color * 0.6f;
				light.Direction = lightDir;

				//D3DLIGHT9 light = d3d::InitDirectionalLight(&lightDir, &color);
				Device->SetLight(0, &light);
				Device->LightEnable(0, true);

				Device->SetRenderState(D3DRS_NORMALIZENORMALS, true);
				Device->SetRenderState(D3DRS_SPECULARENABLE, true);

				//
				// Set Camera.
				//

				D3DXVECTOR3    pos(-10.0f, 3.0f, -15.0f);
				D3DXVECTOR3 target(0.0, 0.0f, 0.0f);
				D3DXVECTOR3     up(0.0f, 1.0f, 0.0f);

				D3DXMATRIX V;
				D3DXMatrixLookAtLH(&V, &pos, &target, &up);

				Device->SetTransform(D3DTS_VIEW, &V);

				//
				// Set projection matrix.
				//
				D3DXMATRIX proj;
				D3DXMatrixPerspectiveFovLH(
					&proj,
					D3DX_PI / 4.0f, // 45 - degree
					//(float)Width / (float)Height,
					(float)pDevice->getVideoDriver()->getScreenSize().Width / (float)pDevice->getVideoDriver()->getScreenSize().Height,
					1.0f,
					1000.0f);
				Device->SetTransform(D3DTS_PROJECTION, &proj);	
			}

			while(pDevice->run())
			{
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//esc키누루면 종료
				if(::GetAsyncKeyState(VK_ESCAPE) && 0x8000)
				{
					pDevice->closeDevice();
				}

				// 시점 조절
				{
					LPDIRECT3DDEVICE9 Device =  pD9Device;
					static float radius = 20.0f;

					if( ::GetAsyncKeyState(VK_LEFT) & 0x8000f )
						TeapotPosition.x -= 3.0f * fDelta;

					if( ::GetAsyncKeyState(VK_RIGHT) & 0x8000f )
						TeapotPosition.x += 3.0f * fDelta;

					if( ::GetAsyncKeyState(VK_UP) & 0x8000f )
						radius -= 2.0f * fDelta;

					if( ::GetAsyncKeyState(VK_DOWN) & 0x8000f )
						radius += 2.0f * fDelta;


					static float angle = (3.0f * D3DX_PI) / 2.0f;

					if( ::GetAsyncKeyState('A') & 0x8000f )
						angle -= 0.5f * fDelta;

					if( ::GetAsyncKeyState('S') & 0x8000f )
						angle += 0.5f * fDelta;

					D3DXVECTOR3 position( cosf(angle) * radius, 3.0f, sinf(angle) * radius );
					D3DXVECTOR3 target(0.0f, 0.0f, 0.0f);
					D3DXVECTOR3 up(0.0f, 1.0f, 0.0f);
					D3DXMATRIX V;
					D3DXMatrixLookAtLH(&V, &position, &target, &up);
					Device->SetTransform(D3DTS_VIEW, &V);
				}

				//
				// Draw the scene:
				//				

				pD9Device->Clear(0,0,
					D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER | D3DCLEAR_STENCIL,
					0x000000ff,1.0f,0);

				pD9Device->BeginScene();


				{
					LPDIRECT3DDEVICE9 Device =  pD9Device;
					// draw teapot
					Device->SetMaterial(&TeapotMtrl);
					Device->SetTexture(0, 0);
					D3DXMATRIX W;
					D3DXMatrixTranslation(&W,
						TeapotPosition.x, 
						TeapotPosition.y,
						TeapotPosition.z);

					Device->SetTransform(D3DTS_WORLD, &W);
					Teapot->DrawSubset(0);

					D3DXMATRIX I;
					D3DXMatrixIdentity(&I);
					Device->SetTransform(D3DTS_WORLD, &I);

					Device->SetStreamSource(0, VB, 0, sizeof(Vertex));
					Device->SetFVF(Vertex::FVF);

					// draw the floor
					Device->SetMaterial(&FloorMtrl);
					Device->SetTexture(0, FloorTex);
					Device->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 2);

					// draw the walls
					Device->SetMaterial(&WallMtrl);
					Device->SetTexture(0, WallTex);
					Device->DrawPrimitive(D3DPT_TRIANGLELIST, 6, 4);

					// draw the mirror
					Device->SetMaterial(&MirrorMtrl);
					Device->SetTexture(0, MirrorTex);
					Device->DrawPrimitive(D3DPT_TRIANGLELIST, 18, 2);
				}				


				{
					//
					// Draw Mirror quad to stencil buffer ONLY.  In this way
					// only the stencil bits that correspond to the mirror will
					// be on.  Therefore, the reflected teapot can only be rendered
					// where the stencil bits are turned on, and thus on the mirror 
					// only.
					//

					LPDIRECT3DDEVICE9 Device =  pD9Device;

					Device->SetRenderState(D3DRS_STENCILENABLE,    true);
					Device->SetRenderState(D3DRS_STENCILFUNC,      D3DCMP_ALWAYS);
					Device->SetRenderState(D3DRS_STENCILREF,       0x1);
					Device->SetRenderState(D3DRS_STENCILMASK,      0xffffffff);
					Device->SetRenderState(D3DRS_STENCILWRITEMASK, 0xffffffff);
					Device->SetRenderState(D3DRS_STENCILZFAIL,     D3DSTENCILOP_KEEP);
					Device->SetRenderState(D3DRS_STENCILFAIL,      D3DSTENCILOP_KEEP);
					Device->SetRenderState(D3DRS_STENCILPASS,      D3DSTENCILOP_REPLACE);

					// disable writes to the depth and back buffers
					Device->SetRenderState(D3DRS_ZWRITEENABLE, false);
					Device->SetRenderState(D3DRS_ALPHABLENDENABLE, true);
					Device->SetRenderState(D3DRS_SRCBLEND,  D3DBLEND_ZERO);
					Device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);

					// draw the mirror to the stencil buffer
					Device->SetStreamSource(0, VB, 0, sizeof(Vertex));
					Device->SetFVF(Vertex::FVF);
					Device->SetMaterial(&MirrorMtrl);
					Device->SetTexture(0, MirrorTex);
					D3DXMATRIX I;
					D3DXMatrixIdentity(&I);
					Device->SetTransform(D3DTS_WORLD, &I);
					Device->DrawPrimitive(D3DPT_TRIANGLELIST, 18, 2);

					// re-enable depth writes
					Device->SetRenderState( D3DRS_ZWRITEENABLE, true );

					// only draw reflected teapot to the pixels where the mirror
					// was drawn to.
					Device->SetRenderState(D3DRS_STENCILFUNC,  D3DCMP_EQUAL);
					Device->SetRenderState(D3DRS_STENCILPASS,  D3DSTENCILOP_KEEP);

					// position reflection
					D3DXMATRIX W, T, R;
					D3DXPLANE plane(0.0f, 0.0f, 1.0f, 0.0f); // xy plane
					D3DXMatrixReflect(&R, &plane);					

					D3DXMatrixTranslation(&T,
						TeapotPosition.x, 
						TeapotPosition.y,
						TeapotPosition.z); 

					W = T * R;

					// clear depth buffer and blend the reflected teapot with the mirror
					Device->Clear(0, 0, D3DCLEAR_ZBUFFER, 0, 1.0f, 0);
					Device->SetRenderState(D3DRS_SRCBLEND,  D3DBLEND_DESTCOLOR);
					Device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ZERO);

					// Finally, draw the reflected teapot
					Device->SetTransform(D3DTS_WORLD, &W);
					Device->SetMaterial(&TeapotMtrl);
					Device->SetTexture(0, 0);

					Device->SetRenderState(D3DRS_CULLMODE, D3DCULL_CW);
					Teapot->DrawSubset(0);

					// Restore render states.
					Device->SetRenderState(D3DRS_ALPHABLENDENABLE, false);
					Device->SetRenderState( D3DRS_STENCILENABLE, false);
					Device->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
				}

				pD9Device->EndScene();

				pD9Device->Present(0,0,0,0);
			}

			pDevice->drop();
		}

	}

	//그림자 효과
	namespace _16
	{			

		IDirect3DVertexBuffer9* VB = 0;

		IDirect3DTexture9* FloorTex  = 0;
		IDirect3DTexture9* WallTex   = 0;
		IDirect3DTexture9* MirrorTex = 0;

		D3DMATERIAL9 FloorMtrl; // = d3d::WHITE_MTRL;
		D3DMATERIAL9 WallMtrl;  // = d3d::WHITE_MTRL;
		D3DMATERIAL9 MirrorMtrl;// = d3d::WHITE_MTRL;

		ID3DXMesh* Teapot = 0;
		D3DXVECTOR3 TeapotPosition(0.0f, 3.0f, -7.5f);
		D3DMATERIAL9 TeapotMtrl;// = d3d::YELLOW_MTRL;

		struct Vertex
		{
			Vertex(){}
			Vertex(float x, float y, float z, 
				float nx, float ny, float nz,
				float u, float v)
			{
				_x  = x;  _y  = y;  _z  = z;
				_nx = nx; _ny = ny; _nz = nz;
				_u  = u;  _v  = v;
			}
			float _x, _y, _z;
			float _nx, _ny, _nz;
			float _u, _v;

			static const DWORD FVF;
		};
		const DWORD Vertex::FVF = D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1;

		void main()
		{		

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9,
				irr::core::dimension2du(640,480),
				32,
				false,
				true
				); 

			pDevice->run();

			//다이랙트 3디 디바이스얻기
			LPDIRECT3DDEVICE9 pD9Device = pDevice->getVideoDriver()->getExposedVideoData().D3D9.D3DDev9;	


			{
				LPDIRECT3DDEVICE9 Device =  pD9Device;

				D3DMATERIAL9 white_mtl;
				white_mtl.Ambient = D3DXCOLOR(1,1,1,1);
				white_mtl.Diffuse = D3DXCOLOR(1,1,1,1);
				white_mtl.Specular = D3DXCOLOR(1,1,1,1);
				white_mtl.Emissive = D3DXCOLOR(0,0,0,1);
				white_mtl.Power = 2.0f;

				D3DMATERIAL9 yellow_mtl;
				yellow_mtl.Ambient = D3DXCOLOR(1,1,0,1);
				yellow_mtl.Diffuse = D3DXCOLOR(1,1,0,1);
				yellow_mtl.Specular = D3DXCOLOR(1,1,0,1);
				yellow_mtl.Emissive = D3DXCOLOR(0,0,0,1);
				yellow_mtl.Power = 2.0f;

				FloorMtrl = white_mtl;
				WallMtrl = white_mtl;
				MirrorMtrl = white_mtl;
				TeapotMtrl = yellow_mtl;

				//
				// Make walls have low specular reflectance - 20%.
				//

				WallMtrl.Specular = D3DXCOLOR(1,1,1,1) * 0.2f;

				//
				// Create the teapot.
				//

				D3DXCreateTeapot(Device, &Teapot, 0);

				//
				// Create and specify geometry.  For this sample we draw a floor
				// and a wall with a mirror on it.  We put the floor, wall, and
				// mirror geometry in one vertex buffer.
				//
				//   |----|----|----|
				//   |Wall|Mirr|Wall|
				//   |    | or |    |
				//   /--------------/
				//  /   Floor      /
				// /--------------/
				//
				Device->CreateVertexBuffer(
					24 * sizeof(Vertex),
					0, // usage
					Vertex::FVF,
					D3DPOOL_MANAGED,
					&VB,
					0);

				Vertex* v = 0;
				VB->Lock(0, 0, (void**)&v, 0);

				// floor
				v[0] = Vertex(-7.5f, 0.0f, -10.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f);
				v[1] = Vertex(-7.5f, 0.0f,   0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f);
				v[2] = Vertex( 7.5f, 0.0f,   0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f);

				v[3] = Vertex(-7.5f, 0.0f, -10.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f);
				v[4] = Vertex( 7.5f, 0.0f,   0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f);
				v[5] = Vertex( 7.5f, 0.0f, -10.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f);

				// wall
				v[6]  = Vertex(-7.5f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f);
				v[7]  = Vertex(-7.5f, 5.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f);
				v[8]  = Vertex(-2.5f, 5.0f, 0.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f);

				v[9]  = Vertex(-7.5f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f);
				v[10] = Vertex(-2.5f, 5.0f, 0.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f);
				v[11] = Vertex(-2.5f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f);

				// Note: We leave gap in middle of walls for mirror

				v[12] = Vertex(2.5f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f);
				v[13] = Vertex(2.5f, 5.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f);
				v[14] = Vertex(7.5f, 5.0f, 0.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f);

				v[15] = Vertex(2.5f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f);
				v[16] = Vertex(7.5f, 5.0f, 0.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f);
				v[17] = Vertex(7.5f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f);

				// mirror
				v[18] = Vertex(-2.5f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f);
				v[19] = Vertex(-2.5f, 5.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f);
				v[20] = Vertex( 2.5f, 5.0f, 0.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f);

				v[21] = Vertex(-2.5f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f);
				v[22] = Vertex( 2.5f, 5.0f, 0.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f);
				v[23] = Vertex( 2.5f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f);

				VB->Unlock();

				//
				// Load Textures, set filters.
				//

				D3DXCreateTextureFromFile(Device, L"../../res/dx_media/checker.jpg", &FloorTex);
				D3DXCreateTextureFromFile(Device, L"../../res/dx_media/brick0.jpg", &WallTex);
				D3DXCreateTextureFromFile(Device, L"../../res/dx_media/ice.bmp", &MirrorTex);

				Device->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
				Device->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
				Device->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR);

				//
				// Lights.
				//

				D3DXVECTOR3 lightDir(0.707f, -0.707f, 0.707f);
				D3DXCOLOR color(1.0f, 1.0f, 1.0f, 1.0f);
				//D3DLIGHT9 light;


				D3DLIGHT9 light;
				::ZeroMemory(&light, sizeof(light));
				light.Type      = D3DLIGHT_DIRECTIONAL;
				light.Ambient   = color * 0.4f;
				light.Diffuse   = color;
				light.Specular  = color * 0.6f;
				light.Direction = lightDir;

				//D3DLIGHT9 light = d3d::InitDirectionalLight(&lightDir, &color);
				Device->SetLight(0, &light);
				Device->LightEnable(0, true);

				Device->SetRenderState(D3DRS_NORMALIZENORMALS, true);
				Device->SetRenderState(D3DRS_SPECULARENABLE, true);

				//
				// Set Camera.
				//

				D3DXVECTOR3    pos(-10.0f, 3.0f, -15.0f);
				D3DXVECTOR3 target(0.0, 0.0f, 0.0f);
				D3DXVECTOR3     up(0.0f, 1.0f, 0.0f);

				D3DXMATRIX V;
				D3DXMatrixLookAtLH(&V, &pos, &target, &up);

				Device->SetTransform(D3DTS_VIEW, &V);

				//
				// Set projection matrix.
				//
				D3DXMATRIX proj;
				D3DXMatrixPerspectiveFovLH(
					&proj,
					D3DX_PI / 4.0f, // 45 - degree
					//(float)Width / (float)Height,
					(float)pDevice->getVideoDriver()->getScreenSize().Width / (float)pDevice->getVideoDriver()->getScreenSize().Height,
					1.0f,
					1000.0f);
				Device->SetTransform(D3DTS_PROJECTION, &proj);	
			}

			while(pDevice->run())
			{
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//esc키누루면 종료
				if(::GetAsyncKeyState(VK_ESCAPE) && 0x8000)
				{
					pDevice->closeDevice();
				}

				// 시점 조절
				{
					LPDIRECT3DDEVICE9 Device =  pD9Device;
					static float radius = 20.0f;

					if( ::GetAsyncKeyState(VK_LEFT) & 0x8000f )
						TeapotPosition.x -= 3.0f * fDelta;

					if( ::GetAsyncKeyState(VK_RIGHT) & 0x8000f )
						TeapotPosition.x += 3.0f * fDelta;

					if( ::GetAsyncKeyState(VK_UP) & 0x8000f )
						radius -= 2.0f * fDelta;

					if( ::GetAsyncKeyState(VK_DOWN) & 0x8000f )
						radius += 2.0f * fDelta;


					static float angle = (3.0f * D3DX_PI) / 2.0f;

					if( ::GetAsyncKeyState('A') & 0x8000f )
						angle -= 0.5f * fDelta;

					if( ::GetAsyncKeyState('S') & 0x8000f )
						angle += 0.5f * fDelta;

					D3DXVECTOR3 position( cosf(angle) * radius, 3.0f, sinf(angle) * radius );
					D3DXVECTOR3 target(0.0f, 0.0f, 0.0f);
					D3DXVECTOR3 up(0.0f, 1.0f, 0.0f);
					D3DXMATRIX V;
					D3DXMatrixLookAtLH(&V, &position, &target, &up);
					Device->SetTransform(D3DTS_VIEW, &V);
				}

				//
				// Draw the scene:
				//				

				pD9Device->Clear(0,0,
					D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER | D3DCLEAR_STENCIL,
					0x000000ff,1.0f,0);

				pD9Device->BeginScene();


				{
					LPDIRECT3DDEVICE9 Device =  pD9Device;
					// draw teapot
					Device->SetMaterial(&TeapotMtrl);
					Device->SetTexture(0, 0);
					D3DXMATRIX W;
					D3DXMatrixTranslation(&W,
						TeapotPosition.x, 
						TeapotPosition.y,
						TeapotPosition.z);

					Device->SetTransform(D3DTS_WORLD, &W);
					Teapot->DrawSubset(0);

					D3DXMATRIX I;
					D3DXMatrixIdentity(&I);
					Device->SetTransform(D3DTS_WORLD, &I);

					Device->SetStreamSource(0, VB, 0, sizeof(Vertex));
					Device->SetFVF(Vertex::FVF);

					// draw the floor
					Device->SetMaterial(&FloorMtrl);
					Device->SetTexture(0, FloorTex);
					Device->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 2);

					// draw the walls
					Device->SetMaterial(&WallMtrl);
					Device->SetTexture(0, WallTex);
					Device->DrawPrimitive(D3DPT_TRIANGLELIST, 6, 4);

					// draw the mirror
					Device->SetMaterial(&MirrorMtrl);
					Device->SetTexture(0, MirrorTex);
					Device->DrawPrimitive(D3DPT_TRIANGLELIST, 18, 2);
				}

				{
					LPDIRECT3DDEVICE9 Device =  pD9Device;

					Device->SetRenderState(D3DRS_STENCILENABLE,    true);
					Device->SetRenderState(D3DRS_STENCILFUNC,      D3DCMP_EQUAL);
					Device->SetRenderState(D3DRS_STENCILREF,       0x0);
					Device->SetRenderState(D3DRS_STENCILMASK,      0xffffffff);
					Device->SetRenderState(D3DRS_STENCILWRITEMASK, 0xffffffff);
					Device->SetRenderState(D3DRS_STENCILZFAIL,     D3DSTENCILOP_KEEP);
					Device->SetRenderState(D3DRS_STENCILFAIL,      D3DSTENCILOP_KEEP);
					Device->SetRenderState(D3DRS_STENCILPASS,      D3DSTENCILOP_INCR); // increment to 1

					// position shadow
					D3DXVECTOR4 lightDirection(0.707f, -0.707f, 0.707f, 0.0f);
					D3DXPLANE groundPlane(0.0f, -1.0f, 0.0f, 0.0f);

					D3DXMATRIX S;
					D3DXMatrixShadow(
						&S,
						&lightDirection,
						&groundPlane);

					D3DXMATRIX T;
					D3DXMatrixTranslation(
						&T,
						TeapotPosition.x,
						TeapotPosition.y,
						TeapotPosition.z);

					D3DXMATRIX W = T * S;
					Device->SetTransform(D3DTS_WORLD, &W);

					// alpha blend the shadow
					Device->SetRenderState(D3DRS_ALPHABLENDENABLE, true);
					Device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
					Device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

					D3DMATERIAL9 mtrl;

					mtrl.Ambient = D3DXCOLOR(0,0,0,1);
					mtrl.Diffuse = D3DXCOLOR(0,0,0,1);
					mtrl.Emissive = D3DXCOLOR(0,0,0,1);
					mtrl.Specular = D3DXCOLOR(0,0,0,1);
					mtrl.Power = 0;
					mtrl.Diffuse.a = 0.5f; // 50% transparency.

					// Disable depth buffer so that z-fighting doesn't occur when we
					// render the shadow on top of the floor.
					Device->SetRenderState(D3DRS_ZENABLE, false);

					Device->SetMaterial(&mtrl);
					Device->SetTexture(0, 0);
					Teapot->DrawSubset(0);

					Device->SetRenderState(D3DRS_ZENABLE, true);
					Device->SetRenderState(D3DRS_ALPHABLENDENABLE, false);
					Device->SetRenderState(D3DRS_STENCILENABLE,    false);
				}



				pD9Device->EndScene();

				pD9Device->Present(0,0,0,0);
			}

			pDevice->drop();
		}

	}

	//DX 메쉬생성 예제
	namespace _17
	{			

		struct Vertex
		{
			Vertex(){}
			Vertex(float x, float y, float z, 
				float nx, float ny, float nz, float u, float v)
			{
				_x = x;   _y = y;   _z = z;
				_nx = nx; _ny = ny; _nz = nz;
				_u = u;   _v = v;
			}

			float _x, _y, _z, _nx, _ny, _nz, _u, _v;

			static const DWORD FVF;
		};
		const DWORD Vertex::FVF = D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1;


		void main()
		{		

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9  
				); 

			pDevice->run();

			//다이랙트 3디 디바이스얻기
			LPDIRECT3DDEVICE9 pD9Device = pDevice->getVideoDriver()->getExposedVideoData().D3D9.D3DDev9;


			ID3DXMesh*         Mesh = 0;
			const DWORD        NumSubsets = 3;
			IDirect3DTexture9* Textures[3] = {0, 0, 0};// texture for each subset

			//
			// Framework functions
			//

			{
				LPDIRECT3DDEVICE9 Device =  pD9Device;
				HRESULT hr = 0;

				//
				// We are going to fill the empty mesh with the geometry of a box,
				// so we need 12 triangles and 24 vetices.
				//

				hr = D3DXCreateMeshFVF(
					12,
					24,
					D3DXMESH_MANAGED,
					Vertex::FVF,
					Device,
					&Mesh);

				if(FAILED(hr))
				{
					::MessageBox(0, L"D3DXCreateMeshFVF() - FAILED", 0, 0);
					//return false;
				}

				//
				// Fill in vertices of a box
				//
				Vertex* v = 0;
				Mesh->LockVertexBuffer(0, (void**)&v);

				// fill in the front face vertex data
				v[0] = Vertex(-1.0f, -1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f);
				v[1] = Vertex(-1.0f,  1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f);
				v[2] = Vertex( 1.0f,  1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f);
				v[3] = Vertex( 1.0f, -1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f);

				// fill in the back face vertex data
				v[4] = Vertex(-1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f);
				v[5] = Vertex( 1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f);
				v[6] = Vertex( 1.0f,  1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f);
				v[7] = Vertex(-1.0f,  1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f);

				// fill in the top face vertex data
				v[8]  = Vertex(-1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f);
				v[9]  = Vertex(-1.0f, 1.0f,  1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f);
				v[10] = Vertex( 1.0f, 1.0f,  1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f);
				v[11] = Vertex( 1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f);

				// fill in the bottom face vertex data
				v[12] = Vertex(-1.0f, -1.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f);
				v[13] = Vertex( 1.0f, -1.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f);
				v[14] = Vertex( 1.0f, -1.0f,  1.0f, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f);
				v[15] = Vertex(-1.0f, -1.0f,  1.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f);

				// fill in the left face vertex data
				v[16] = Vertex(-1.0f, -1.0f,  1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f);
				v[17] = Vertex(-1.0f,  1.0f,  1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f);
				v[18] = Vertex(-1.0f,  1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f);
				v[19] = Vertex(-1.0f, -1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f);

				// fill in the right face vertex data
				v[20] = Vertex( 1.0f, -1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f);
				v[21] = Vertex( 1.0f,  1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f);
				v[22] = Vertex( 1.0f,  1.0f,  1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f);
				v[23] = Vertex( 1.0f, -1.0f,  1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f);

				Mesh->UnlockVertexBuffer();

				//
				// Define the triangles of the box
				//
				WORD* i = 0;
				Mesh->LockIndexBuffer(0, (void**)&i);

				// fill in the front face index data
				i[0] = 0; i[1] = 1; i[2] = 2;
				i[3] = 0; i[4] = 2; i[5] = 3;

				// fill in the back face index data
				i[6] = 4; i[7]  = 5; i[8]  = 6;
				i[9] = 4; i[10] = 6; i[11] = 7;

				// fill in the top face index data
				i[12] = 8; i[13] =  9; i[14] = 10;
				i[15] = 8; i[16] = 10; i[17] = 11;

				// fill in the bottom face index data
				i[18] = 12; i[19] = 13; i[20] = 14;
				i[21] = 12; i[22] = 14; i[23] = 15;

				// fill in the left face index data
				i[24] = 16; i[25] = 17; i[26] = 18;
				i[27] = 16; i[28] = 18; i[29] = 19;

				// fill in the right face index data
				i[30] = 20; i[31] = 21; i[32] = 22;
				i[33] = 20; i[34] = 22; i[35] = 23;

				Mesh->UnlockIndexBuffer();

				//
				// Specify the subset each triangle belongs to, in this example
				// we will use three subsets, the first two faces of the cube specified
				// will be in subset 0, the next two faces will be in subset 1 and
				// the the last two faces will be in subset 2.
				//
				DWORD* attributeBuffer = 0;
				Mesh->LockAttributeBuffer(0, &attributeBuffer);

				for(int a = 0; a < 4; a++)
					attributeBuffer[a] = 0;

				for(int b = 4; b < 8; b++)
					attributeBuffer[b] = 1;

				for(int c = 8; c < 12; c++)
					attributeBuffer[c] = 2;

				Mesh->UnlockAttributeBuffer();

				//
				// Optimize the mesh to generate an attribute table.
				// 어드제이슨트라고 발음한다.

				std::vector<DWORD> adjacencyBuffer(Mesh->GetNumFaces() * 3);
				Mesh->GenerateAdjacency(0.0f, &adjacencyBuffer[0]);


				hr = Mesh->OptimizeInplace(		
					D3DXMESHOPT_ATTRSORT |
					D3DXMESHOPT_COMPACT  |
					D3DXMESHOPT_VERTEXCACHE,
					&adjacencyBuffer[0],
					0, 0, 0);

				//
				// Dump the Mesh Data to file.
				//

				/*OutFile.open("Mesh Dump.txt");

				dumpVertices(OutFile, Mesh);
				dumpIndices(OutFile, Mesh);
				dumpAttributeTable(OutFile, Mesh); 	
				dumpAttributeBuffer(OutFile, Mesh);		
				dumpAdjacencyBuffer(OutFile, Mesh);

				OutFile.close();*/

				//
				// Load the textures and set filters.
				//

				D3DXCreateTextureFromFile(
					Device,
					L"../../res/dx_media/brick0.jpg",
					&Textures[0]);

				D3DXCreateTextureFromFile(
					Device,
					L"../../res/dx_media/brick1.jpg",
					&Textures[1]);

				D3DXCreateTextureFromFile(
					Device,
					L"../../res/dx_media/checker.jpg",
					&Textures[2]);

				Device->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
				Device->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
				Device->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_POINT);

				// 
				// Disable lighting.
				//

				Device->SetRenderState(D3DRS_LIGHTING, false);

				//
				// Set camera.
				//

				D3DXVECTOR3 pos(0.0f, 0.f, -4.0f);
				D3DXVECTOR3 target(0.0f, 0.0f, 0.0f);
				D3DXVECTOR3 up(0.0f, 1.0f, 0.0f);

				D3DXMATRIX V;
				D3DXMatrixLookAtLH(
					&V,
					&pos,
					&target,
					&up);

				Device->SetTransform(D3DTS_VIEW, &V);

				//
				// Set projection matrix.
				//

				D3DXMATRIX proj;
				D3DXMatrixPerspectiveFovLH(
					&proj,
					D3DX_PI * 0.5f, // 90 - degree
					//(float)Width / (float)Height,
					(float)pDevice->getVideoDriver()->getScreenSize().Width / (float)pDevice->getVideoDriver()->getScreenSize().Height,
					1.0f,
					1000.0f);
				Device->SetTransform(D3DTS_PROJECTION, &proj);

			}

			while(pDevice->run())
			{
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//esc키누루면 종료
				if(::GetAsyncKeyState(VK_ESCAPE) && 0x8000)
				{
					pDevice->closeDevice();
				}

				pD9Device->Clear(0,0,D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER,0x000000ff,1.0f,0);

				{
					LPDIRECT3DDEVICE9 Device =  pD9Device;
					//
					// Update: Rotate the cube.
					//

					D3DXMATRIX xRot;
					D3DXMatrixRotationX(&xRot, D3DX_PI * 0.2f);

					static float y = 0.0f;
					D3DXMATRIX yRot;
					D3DXMatrixRotationY(&yRot, y);
					y += fDelta;

					if( y >= 6.28f )
						y = 0.0f;

					D3DXMATRIX World = xRot * yRot;

					Device->SetTransform(D3DTS_WORLD, &World);

					//
					// Render
					//

					//Device->Clear(0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 0x00000000, 1.0f, 0);
					Device->BeginScene();

					for(int i = 0; i < NumSubsets; i++)
					{
						Device->SetTexture( 0, Textures[i] );
						Mesh->DrawSubset( i );
					}

					Device->EndScene();
					//Device->Present(0, 0, 0, 0);
				}


				pD9Device->Present(0,0,0,0);
			}
			pDevice->drop();
		}
	}

	//X파일읽기
	namespace _18
	{			
		void main()
		{		

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9  
				); 
			pDevice->run();

			//다이랙트 3디 디바이스얻기
			LPDIRECT3DDEVICE9 pD9Device = pDevice->getVideoDriver()->getExposedVideoData().D3D9.D3DDev9;	

			ID3DXMesh*                      Mesh = 0;
			std::vector<D3DMATERIAL9>       Mtrls(0);
			std::vector<IDirect3DTexture9*> Textures(0);

			//
			// Framework functions
			//

			{
				LPDIRECT3DDEVICE9 Device =  pD9Device;

				HRESULT hr = 0;

				//
				// Load the XFile data.
				//

				ID3DXBuffer* adjBuffer  = 0;
				ID3DXBuffer* mtrlBuffer = 0;
				DWORD        numMtrls   = 0;

				hr = D3DXLoadMeshFromX(  
					L"../../res/dx_media/bigship1.x",
					//L"/res/jga/swatcharacter/swat_1a_mako.X",
					D3DXMESH_MANAGED,
					Device,
					&adjBuffer,//인접정보
					&mtrlBuffer,//재질정보
					0,
					&numMtrls,
					&Mesh);

				if(FAILED(hr))
				{
					::MessageBox(0, L"D3DXLoadMeshFromX() - FAILED", 0, 0);		
				}

				//
				// Extract the materials, and load textures.
				//

				if( mtrlBuffer != 0 && numMtrls != 0 )
				{
					D3DXMATERIAL* mtrls = (D3DXMATERIAL*)mtrlBuffer->GetBufferPointer();

					irr::u32 i;
					for( i=0; i < numMtrls; i++)
					{
						// the MatD3D property doesn't have an ambient value set
						// when its loaded, so set it now:
						mtrls[i].MatD3D.Ambient = mtrls[i].MatD3D.Diffuse;

						// save the ith material
						Mtrls.push_back( mtrls[i].MatD3D );

						// check if the ith material has an associative texture
						if( mtrls[i].pTextureFilename != 0 )
						{
							// yes, load the texture for the ith subset
							IDirect3DTexture9* tex = 0;
							D3DXCreateTextureFromFile(
								Device,
								_bstr_t(mtrls[i].pTextureFilename),
								&tex);

							// save the loaded texture
							Textures.push_back( tex );
						}
						else
						{
							// no texture for the ith subset
							Textures.push_back( 0 );
						}
					}
				}
				mtrlBuffer->Release();

				//d3d::Release<ID3DXBuffer*>(mtrlBuffer); // done w/ buffer

				//
				// Optimize the mesh.
				//

				hr = Mesh->OptimizeInplace(
					D3DXMESHOPT_ATTRSORT |
					D3DXMESHOPT_COMPACT  |
					D3DXMESHOPT_VERTEXCACHE,
					(DWORD*)adjBuffer->GetBufferPointer(),
					0, 0, 0);

				adjBuffer->Release();
				//d3d::Release<ID3DXBuffer*>(adjBuffer); // done w/ buffer

				if(FAILED(hr))
				{
					::MessageBox(0, L"OptimizeInplace() - FAILED", 0, 0);

				}

				//
				// Set texture filters.
				//

				Device->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
				Device->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
				Device->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_POINT);

				// 
				// Set Lights.
				//

				D3DXVECTOR3 dir(1.0f, -1.0f, 1.0f);
				D3DXCOLOR col(1.0f, 1.0f, 1.0f, 1.0f);
				D3DLIGHT9 light;// = d3d::InitDirectionalLight(&dir, &col);
				light.Direction = dir;
				light.Type = D3DLIGHT_DIRECTIONAL;
				light.Diffuse = col;
				light.Ambient = col * .4f;
				light.Specular = col * .6f;

				Device->SetLight(0, &light);
				Device->LightEnable(0, true);
				Device->SetRenderState(D3DRS_NORMALIZENORMALS, true);
				Device->SetRenderState(D3DRS_SPECULARENABLE, true);

				//
				// Set camera.
				//

				D3DXVECTOR3 pos(4.0f, 4.0f, -23.0f);
				D3DXVECTOR3 target(0.0f, 0.0f, 0.0f);
				D3DXVECTOR3 up(0.0f, 1.0f, 0.0f);

				D3DXMATRIX V;
				D3DXMatrixLookAtLH(
					&V,
					&pos,
					&target,
					&up);

				Device->SetTransform(D3DTS_VIEW, &V);

				//
				// Set projection matrix.
				//

				D3DXMATRIX proj;
				D3DXMatrixPerspectiveFovLH(
					&proj,
					D3DX_PI * 0.5f, // 90 - degree
					//(float)Width / (float)Height,
					(float)pDevice->getVideoDriver()->getScreenSize().Width / (float)pDevice->getVideoDriver()->getScreenSize().Height,					
					1.0f,
					1000.0f);
				Device->SetTransform(D3DTS_PROJECTION, &proj);

				//return true;
			}


			while(pDevice->run())
			{
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//esc키누루면 종료
				if(::GetAsyncKeyState(VK_ESCAPE) && 0x8000)
				{
					pDevice->closeDevice();
				}

				pD9Device->Clear(0,0,D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER,0x000000ff,1.0f,0);

				{
					LPDIRECT3DDEVICE9 Device =  pD9Device;
					//
					// Update: Rotate the mesh.
					//

					static float y = 0.0f;
					D3DXMATRIX yRot;
					D3DXMatrixRotationY(&yRot, y);
					y += fDelta;

					if( y >= 6.28f )
						y = 0.0f;

					D3DXMATRIX World = yRot;

					Device->SetTransform(D3DTS_WORLD, &World);

					//
					// Render
					//

					//Device->Clear(0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 0xffffffff, 1.0f, 0);
					Device->BeginScene();

					for(DWORD i = 0; i < Mtrls.size(); i++)
					{
						Device->SetMaterial( &Mtrls[i] );
						Device->SetTexture(0, Textures[i]);
						Mesh->DrawSubset(i);
					}	

					Device->EndScene();
					//Device->Present(0, 0, 0, 0);
				}

				pD9Device->Present(0,0,0,0);
			}

			Mesh->Release();

			pDevice->drop();
		}

	}

	//프로그래시브 메쉬 예제
	namespace _19
	{			
		void main()
		{		

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9  
				); 
			pDevice->run();

			//다이랙트 3디 디바이스얻기
			LPDIRECT3DDEVICE9 pD9Device = pDevice->getVideoDriver()->getExposedVideoData().D3D9.D3DDev9;		

			ID3DXMesh*                      SourceMesh = 0;
			ID3DXPMesh*                     PMesh      = 0; // progressive mesh
			std::vector<D3DMATERIAL9>       Mtrls(0);
			std::vector<IDirect3DTexture9*> Textures(0);

			//
			// Framework functions
			//
			//bool Setup()
			{
				LPDIRECT3DDEVICE9 Device =  pD9Device;

				HRESULT hr = 0;
				//
				// Load the XFile data.
				//

				ID3DXBuffer* adjBuffer  = 0;
				ID3DXBuffer* mtrlBuffer = 0;
				DWORD        numMtrls   = 0;

				hr = D3DXLoadMeshFromX(  
					L"../../res/dx_media/bigship1.x",
					D3DXMESH_MANAGED,
					Device,
					&adjBuffer,
					&mtrlBuffer,
					0,
					&numMtrls,
					&SourceMesh);

				if(FAILED(hr))
				{
					::MessageBox(0, L"D3DXLoadMeshFromX() - FAILED", 0, 0);
					//return false;
				}

				//
				// Extract the materials, load textures.
				//

				if( mtrlBuffer != 0 && numMtrls != 0 )
				{
					D3DXMATERIAL* mtrls = (D3DXMATERIAL*)mtrlBuffer->GetBufferPointer();

					irr::u32 i;
					for( i = 0; i < numMtrls; i++)
					{
						// the MatD3D property doesn't have an ambient value set
						// when its loaded, so set it now:
						mtrls[i].MatD3D.Ambient = mtrls[i].MatD3D.Diffuse;

						// save the ith material
						Mtrls.push_back( mtrls[i].MatD3D );

						// check if the ith material has an associative texture
						if( mtrls[i].pTextureFilename != 0 )
						{
							// yes, load the texture for the ith subset
							IDirect3DTexture9* tex = 0;
							D3DXCreateTextureFromFile(
								Device,
								bstr_t(mtrls[i].pTextureFilename),
								&tex);

							// save the loaded texture
							Textures.push_back( tex );
						}
						else
						{
							// no texture for the ith subset
							Textures.push_back( 0 );
						}
					}
				}
				mtrlBuffer->Release();
				//d3d::Release<ID3DXBuffer*>(); // done w/ buffer
				//
				// Optimize the mesh.
				//

				hr = SourceMesh->OptimizeInplace(		
					D3DXMESHOPT_ATTRSORT |
					D3DXMESHOPT_COMPACT  |
					D3DXMESHOPT_VERTEXCACHE,
					(DWORD*)adjBuffer->GetBufferPointer(),
					(DWORD*)adjBuffer->GetBufferPointer(), // new adjacency info
					0, 0);

				if(FAILED(hr))
				{
					::MessageBox(0, L"OptimizeInplace() - FAILED", 0, 0);
					adjBuffer->Release();
					//d3d::Release<ID3DXBuffer*>(adjBuffer); // free
					//return false;
				}

				//
				// Generate the progressive mesh. 
				//

				hr = D3DXGeneratePMesh(
					SourceMesh,
					(DWORD*)adjBuffer->GetBufferPointer(), // adjacency
					0,                  // default vertex attribute weights
					0,                  // default vertex weights
					1,                  // simplify as low as possible
					D3DXMESHSIMP_FACE,  // simplify by face count
					&PMesh);

				SourceMesh->Release();
				adjBuffer->Release();

				//d3d::Release<ID3DXMesh*>(SourceMesh);  // done w/ source mesh
				//d3d::Release<ID3DXBuffer*>(adjBuffer); // done w/ buffer

				if(FAILED(hr))
				{
					::MessageBox(0, L"D3DXGeneratePMesh() - FAILED", 0, 0);
					//return false;
				}

				// set to original detail
				DWORD maxFaces = PMesh->GetMaxFaces();
				PMesh->SetNumFaces(maxFaces);

				//
				// Set texture filters.
				//

				Device->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
				Device->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
				Device->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_POINT);

				// 
				// Set Lights.
				//

				D3DXVECTOR3 dir(1.0f, -1.0f, 1.0f);
				D3DXCOLOR col(1.0f, 1.0f, 1.0f, 1.0f);
				D3DLIGHT9 light;// = d3d::InitDirectionalLight(&dir, &col);
				light.Direction = dir;
				light.Type = D3DLIGHT_DIRECTIONAL;
				light.Diffuse = col;
				light.Ambient = col * .4f;
				light.Specular = col * .6f;

				Device->SetLight(0, &light);
				Device->LightEnable(0, true);
				Device->SetRenderState(D3DRS_NORMALIZENORMALS, true);
				Device->SetRenderState(D3DRS_SPECULARENABLE, true);

				//
				// Set camera.
				//

				D3DXVECTOR3 pos(-8.0f, 4.0f, -12.0f);
				D3DXVECTOR3 target(0.0f, 0.0f, 0.0f);
				D3DXVECTOR3 up(0.0f, 1.0f, 0.0f);

				D3DXMATRIX V;
				D3DXMatrixLookAtLH(
					&V,
					&pos,
					&target,
					&up);

				Device->SetTransform(D3DTS_VIEW, &V);

				//
				// Set projection matrix.
				//

				D3DXMATRIX proj;
				D3DXMatrixPerspectiveFovLH(
					&proj,
					D3DX_PI * 0.5f, // 90 - degree
					//(float)Width / (float)Height,
					(float)pDevice->getVideoDriver()->getScreenSize().Width / (float)pDevice->getVideoDriver()->getScreenSize().Height,					
					1.0f,
					1000.0f);
				Device->SetTransform(D3DTS_PROJECTION, &proj);

				//	return true;
			}

			while(pDevice->run())
			{
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//esc키누루면 종료
				if(::GetAsyncKeyState(VK_ESCAPE) && 0x8000)
				{
					pDevice->closeDevice();
				}

				{
					int numFaces = PMesh->GetNumFaces();

					// Add a face, note the SetNumFaces() will  automatically
					// clamp the specified value if it goes out of bounds.
					if( ::GetAsyncKeyState('A') & 0x8000f )
					{
						// Sometimes we must add more than one face to invert
						// an edge collapse transformation
						PMesh->SetNumFaces( numFaces + 1 );
						if( PMesh->GetNumFaces() == numFaces )
							PMesh->SetNumFaces( numFaces + 2 );
					}

					// Remove a face, note the SetNumFaces() will  automatically
					// clamp the specified value if it goes out of bounds.
					if( ::GetAsyncKeyState('S') & 0x8000f )
						PMesh->SetNumFaces( numFaces - 1 );
				}

				pD9Device->Clear(0,0,D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER,0x000000ff,1.0f,0);

				{
					LPDIRECT3DDEVICE9 Device =  pD9Device;

					Device->BeginScene();

					irr::u32 i;
					for(i = 0; i < Mtrls.size(); i++)
					{
						// draw pmesh
						Device->SetMaterial( &Mtrls[i] );
						Device->SetTexture(0, Textures[i]);
						PMesh->DrawSubset(i);

						// draw wireframe outline

						D3DMATERIAL9 mtrl;
						mtrl.Ambient = D3DXCOLOR(1,1,0,1) * .4f;
						mtrl.Diffuse = D3DXCOLOR(1,1,0,1);
						mtrl.Specular = D3DXCOLOR(1,1,0,1) * .6f;
						mtrl.Emissive = D3DXCOLOR(0,0,0,0);
						mtrl.Power = 2.0f;
						Device->SetMaterial(&mtrl);
						Device->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);
						PMesh->DrawSubset(i);
						Device->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
					}	
					Device->EndScene();
				}

				pD9Device->Present(0,0,0,0);
			}

			pDevice->drop();
		}
	}

	//픽킹예제
	namespace _20
	{

		//2d 를 3D 광선으로 만들기
		Ray CalcPickingRay(LPDIRECT3DDEVICE9 Device,int x, int y)
		{
			float px = 0.0f;
			float py = 0.0f;

			D3DVIEWPORT9 vp;
			Device->GetViewport(&vp);

			D3DXMATRIX proj;
			Device->GetTransform(D3DTS_PROJECTION, &proj);

			px = ((( 2.0f*x) / vp.Width)  - 1.0f) / proj(0, 0);
			py = (((-2.0f*y) / vp.Height) + 1.0f) / proj(1, 1);

			Ray ray;
			ray._origin    = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
			ray._direction = D3DXVECTOR3(px, py, 1.0f);

			return ray;
		}

		void TransformRay(Ray* ray, D3DXMATRIX* T)
		{
			// transform the ray's origin, w = 1.
			D3DXVec3TransformCoord(
				&ray->_origin,
				&ray->_origin,
				T);

			// transform the ray's direction, w = 0.
			D3DXVec3TransformNormal(
				&ray->_direction,
				&ray->_direction,
				T);

			// normalize the direction
			D3DXVec3Normalize(&ray->_direction, &ray->_direction);
		}

		bool RaySphereIntTest(Ray* ray, BoundingSphere* sphere)
		{
			D3DXVECTOR3 v = ray->_origin - sphere->_center;

			float b = 2.0f * D3DXVec3Dot(&ray->_direction, &v);
			float c = D3DXVec3Dot(&v, &v) - (sphere->_radius * sphere->_radius);

			// find the discriminant
			float discriminant = (b * b) - (4.0f * c);

			// test for imaginary number
			if( discriminant < 0.0f )
				return false;

			discriminant = sqrtf(discriminant);

			float s0 = (-b + discriminant) / 2.0f;
			float s1 = (-b - discriminant) / 2.0f;

			// if a solution is >= 0, then we intersected the sphere
			if( s0 >= 0.0f || s1 >= 0.0f )
				return true;

			return false;
		}



		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9  
				); 
			pDevice->run();

			//다이랙트 3디 디바이스얻기
			LPDIRECT3DDEVICE9 pD9Device = pDevice->getVideoDriver()->getExposedVideoData().D3D9.D3DDev9;		

			ID3DXMesh* Teapot = 0;
			ID3DXMesh* Sphere = 0;

			D3DXMATRIX World;
			BoundingSphere BSphere;

			{
				LPDIRECT3DDEVICE9 Device = pD9Device;
				//
				// Create the teapot.
				//

				D3DXCreateTeapot(Device, &Teapot, 0);

				//
				// Compute the bounding sphere.
				//

				BYTE* v = 0;
				Teapot->LockVertexBuffer(0, (void**)&v);

				D3DXComputeBoundingSphere(
					(D3DXVECTOR3*)v,
					Teapot->GetNumVertices(),
					D3DXGetFVFVertexSize(Teapot->GetFVF()),
					&BSphere._center,
					&BSphere._radius);

				Teapot->UnlockVertexBuffer();

				//
				// Build a sphere mesh that describes the teapot's bounding sphere.
				//

				D3DXCreateSphere(Device, BSphere._radius, 20, 20, &Sphere, 0);

				//
				// Set light.
				//

				D3DXVECTOR3 dir(0.707f, -0.0f, 0.707f);
				D3DXCOLOR col(1.0f, 1.0f, 1.0f, 1.0f);
				D3DLIGHT9 light = InitDirectionalLight(&dir, &col);

				Device->SetLight(0, &light);
				Device->LightEnable(0, true);
				Device->SetRenderState(D3DRS_NORMALIZENORMALS, true);
				Device->SetRenderState(D3DRS_SPECULARENABLE, false);	

				//
				// Set view matrix.
				//

				D3DXVECTOR3 pos(0.0f, 0.0f, -10.0f);
				D3DXVECTOR3 target(0.0f, 0.0f, 0.0f);
				D3DXVECTOR3 up(0.0f, 1.0f, 0.0f);

				D3DXMATRIX V;
				D3DXMatrixLookAtLH(&V, &pos, &target, &up);
				Device->SetTransform(D3DTS_VIEW, &V);

				//
				// Set projection matrix.
				//

				D3DXMATRIX proj;
				D3DXMatrixPerspectiveFovLH(
					&proj,
					D3DX_PI * 0.25f, // 45 - degree
					(float)pDevice->getVideoDriver()->getScreenSize().Width / (float)pDevice->getVideoDriver()->getScreenSize().Height,					
					1.0f,
					1000.0f);
				Device->SetTransform(D3DTS_PROJECTION, &proj);

				//return true;
			}

			while(pDevice->run())
			{
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//esc키누루면 종료
				if(::GetAsyncKeyState(VK_ESCAPE) && 0x8000)
				{
					pDevice->closeDevice();
				}

				if(::GetAsyncKeyState(VK_LBUTTON) && 0x8000)
				{
					irr::core::position2di mpos = pDevice->getCursorControl()->getPosition();
					std::cout << mpos.X << "/" << mpos.Y <<std::endl;
					//pDevice->closeDevice();
				}

				

				pD9Device->Clear(0,0,D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER,0x000000ff,1.0f,0);

				{
					LPDIRECT3DDEVICE9 Device = pD9Device;
					//
					// Update: Update Teapot.
					//

					static float r     = 0.0f;
					static float v     = 1.0f;
					static float angle = 0.0f;

					D3DXMatrixTranslation(&World, cosf(angle) * r, sinf(angle) * r, 10.0f);

					// transfrom the bounding sphere to match the teapots position in the
					// world.
					BSphere._center = D3DXVECTOR3(cosf(angle)*r, sinf(angle)*r, 10.0f);

					r += v * fDelta;

					if( r >= 8.0f )
						v = -v; // reverse direction

					if( r <= 0.0f )
						v = -v; // reverse direction

					angle += 1.0f * D3DX_PI * fDelta;
					if( angle >= D3DX_PI * 2.0f )
						angle = 0.0f;

					//
					// Render
					//

					//Device->Clear(0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 0xffffffff, 1.0f, 0);
					Device->BeginScene();

					// Render the teapot.
					Device->SetTransform(D3DTS_WORLD, &World);
					Device->SetMaterial(&YELLOW_MTRL);
					Teapot->DrawSubset(0);

					// Render the bounding sphere with alpha blending so we can see 
					// through it.
					Device->SetRenderState(D3DRS_ALPHABLENDENABLE, true);
					Device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
					Device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

					{
						irr::core::position2di mpos = pDevice->getCursorControl()->getPosition();
						Ray ray = CalcPickingRay(pD9Device,mpos.X,mpos.Y); //뷰공간상의 피킹레이구하기

						D3DXMATRIX view,view_Inv;

						pD9Device->GetTransform(D3DTS_VIEW,&view);
						D3DXMatrixInverse(&view_Inv,0,&view);

						TransformRay(&ray,&view_Inv); //피킹광선 월드 공간으로 보내기

						if(RaySphereIntTest(&ray,&BSphere))		//픽킹태스트				
						{
							
							D3DMATERIAL9 blue = RED_MTRL;
							blue.Diffuse.a = 0.25f; // 25% opacity
							Device->SetMaterial(&blue);

						}
						else
						{
							
							D3DMATERIAL9 blue = BLUE_MTRL;
							blue.Diffuse.a = 0.25f; // 25% opacity
							Device->SetMaterial(&blue);
						}
					}

					
					Sphere->DrawSubset(0);

					Device->SetRenderState(D3DRS_ALPHABLENDENABLE, false);

					Device->EndScene();
					//Device->Present(0, 0, 0, 0);
				}

				pD9Device->Present(0,0,0,0);
			}

			pDevice->drop();
		}

	}

	//셰이더 샘플 용책 16장
	namespace _21
	{			
		void main()
		{		

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9  
				); 

			pDevice->run();

			//다이랙트 3디 디바이스얻기
			LPDIRECT3DDEVICE9 pD9Device = pDevice->getVideoDriver()->getExposedVideoData().D3D9.D3DDev9;	

			IDirect3DVertexShader9* TransformShader    = 0;
			ID3DXConstantTable* TransformConstantTable = 0;

			ID3DXMesh* Teapot = 0;

			D3DXHANDLE TransformViewProjHandle = 0;

			D3DXMATRIX ProjMatrix;



			{
				LPDIRECT3DDEVICE9 Device = pD9Device;
				HRESULT hr = 0;

				//
				// Create geometry.
				//

				D3DXCreateTeapot(Device, &Teapot, 0);

				//
				// Compile shader.
				//

				ID3DXBuffer* shader      = 0;
				ID3DXBuffer* errorBuffer = 0;

				hr = D3DXCompileShaderFromFile(
					L"../../res/dx_media/transform.txt",
					0,
					0,
					"Main",  // entry point function name
					"vs_1_1",// shader version to compile to
					D3DXSHADER_DEBUG, 
					&shader,
					&errorBuffer,
					&TransformConstantTable);

				// output any error messages
				if( errorBuffer )
				{
					::MessageBox(0, bstr_t((char*)errorBuffer->GetBufferPointer()), 0, 0);
					errorBuffer->Release();
				}

				if(FAILED(hr))
				{
					::MessageBox(0, L"D3DXCreateEffectFromFile() - FAILED", 0, 0);
					return;
				}

				hr = Device->CreateVertexShader(
					(DWORD*)shader->GetBufferPointer(),
					&TransformShader);

				if(FAILED(hr))
				{
					::MessageBox(0, L"CreateVertexShader - FAILED", 0, 0);
					return;// false;
				}

				shader->Release();
				//d3d::Release<ID3DXBuffer*>(shader);
				// 
				// Get Handles.
				//

				TransformViewProjHandle = TransformConstantTable->GetConstantByName(0, "ViewProjMatrix");

				//
				// Set shader constants:
				//

				TransformConstantTable->SetDefaults(Device);

				//
				// Set Projection Matrix.
				//

				D3DXMatrixPerspectiveFovLH(
					&ProjMatrix, D3DX_PI * 0.25f, 
					//(float)Width / (float)Height, 
					(float)pDevice->getVideoDriver()->getScreenSize().Width / (float)pDevice->getVideoDriver()->getScreenSize().Height,					
					1.0f, 1000.0f);

				//
				// Set Misc. States.
				//




				Device->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);

				//return true;
			}

			D3DXMATRIX WorldMatrix;
			D3DXMatrixTranslation(&WorldMatrix,5,2,1);

			int i,j;
			for(i=0;i<4;i++)
			{
				for(j=0;j<4;j++)
				{
					std::cout << WorldMatrix(i,j) << ",";
				}
				std::cout << std::endl;
			}

			while(pDevice->run())
			{
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//esc키누루면 종료
				if(::GetAsyncKeyState(VK_ESCAPE) && 0x8000)
				{
					pDevice->closeDevice();
				}

				{
					LPDIRECT3DDEVICE9 Device = pD9Device;
					// 
					// Update the scene: Allow user to rotate around scene.
					//

					static float angle  = (3.0f * D3DX_PI) / 2.0f;
					static float height = 5.0f;

					if( ::GetAsyncKeyState(VK_LEFT) & 0x8000f )
						angle -= 0.5f * fDelta;

					if( ::GetAsyncKeyState(VK_RIGHT) & 0x8000f )
						angle += 0.5f * fDelta;

					if( ::GetAsyncKeyState(VK_UP) & 0x8000f )
						height += 5.0f * fDelta;

					if( ::GetAsyncKeyState(VK_DOWN) & 0x8000f )
						height -= 5.0f * fDelta;

					D3DXVECTOR3 position( cosf(angle) * 10.0f, height, sinf(angle) * 10.0f );
					D3DXVECTOR3 target(0.0f, 0.0f, 0.0f);
					D3DXVECTOR3 up(0.0f, 1.0f, 0.0f);
					D3DXMATRIX V;
					D3DXMatrixLookAtLH(&V, &position, &target, &up);					

					// combine view and projection transformations
					D3DXMATRIX ViewProj = V * ProjMatrix;					

					TransformConstantTable->SetMatrix(
						Device,
						TransformViewProjHandle, 
						&ViewProj//(const D3DXMATRIX *)matVP.pointer()
						);						
					//
					// Render
					//

					Device->Clear(0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 0xffffffff, 1.0f, 0);
					Device->BeginScene();

					Device->SetVertexShader(TransformShader);

					Teapot->DrawSubset(0);

					//일리히트 사용할 경우
					{
						irr::core::matrix4 matW;
						irr::core::matrix4 matV;
						irr::core::matrix4 matP;
						irr::core::matrix4 matVP;


						DX2IrrMatrix(ProjMatrix,matP);
						DX2IrrMatrix(WorldMatrix,matW);
						matV.buildCameraLookAtMatrixLH(
							irr::core::vector3df( cosf(angle) * 10.0f, height, sinf(angle) * 10.0f ),
							irr::core::vector3df(0.0f, 0.0f, 0.0f),
							irr::core::vector3df(0.0f, 1.0f, 0.0f));

						matVP = matP * matV * matW; //곱하는 순서가 DX와 서로 다르다.

						TransformConstantTable->SetMatrix(
							Device,
							TransformViewProjHandle, 
							(const D3DXMATRIX *)matVP.pointer()
							);						
						Device->SetVertexShader(TransformShader);
						Teapot->DrawSubset(0);
					}



					Device->EndScene();
					Device->Present(0, 0, 0, 0);
				}				
			}

			TransformConstantTable->Release();
			Teapot->Release();
			TransformShader->Release();
			pDevice->drop();
		}
	}

	//난반사광 버텍 쉐이더 17장
	namespace _22
	{			
		void main()
		{		

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9  
				); 
			pDevice->run();

			//다이랙트 3디 디바이스얻기
			LPDIRECT3DDEVICE9 pD9Device = pDevice->getVideoDriver()->getExposedVideoData().D3D9.D3DDev9;	

			IDirect3DVertexShader9* DiffuseShader = 0;
			ID3DXConstantTable* DiffuseConstTable = 0;
			ID3DXMesh* Teapot            = 0;
			D3DXHANDLE ViewMatrixHandle     = 0;
			D3DXHANDLE ViewProjMatrixHandle = 0;
			D3DXHANDLE AmbientMtrlHandle    = 0;
			D3DXHANDLE DiffuseMtrlHandle    = 0;
			D3DXHANDLE LightDirHandle       = 0;
			D3DXMATRIX Proj;

			{
				LPDIRECT3DDEVICE9 Device = pD9Device;
				HRESULT hr = 0;

				//
				// Create geometry.
				//

				D3DXCreateTeapot(Device, &Teapot, 0);

				//
				// Compile shader.
				//

				ID3DXBuffer* shader      = 0;
				ID3DXBuffer* errorBuffer = 0;

				hr = D3DXCompileShaderFromFile(
					L"../../res/dx_media/diffuse.txt",
					0,
					0,
					"Main",  // entry point function name
					"vs_1_1",// shader version to compile to
					D3DXSHADER_DEBUG|D3DXSHADER_ENABLE_BACKWARDS_COMPATIBILITY, 
					&shader,
					&errorBuffer,
					&DiffuseConstTable);

				// output any error messages
				if( errorBuffer )
				{
					::MessageBox(0, bstr_t((char*)errorBuffer->GetBufferPointer()), 0, 0);
					errorBuffer->Release();
				}

				if(FAILED(hr))
				{
					::MessageBox(0, L"D3DXCreateEffectFromFile() - FAILED", 0, 0);
					return;
				}

				hr = Device->CreateVertexShader(
					(DWORD*)shader->GetBufferPointer(),
					&DiffuseShader);

				if(FAILED(hr))
				{
					::MessageBox(0, L"CreateVertexShader - FAILED", 0, 0);
					return;// false;
				}

				shader->Release();
				// 
				// Get Handles
				//

				ViewMatrixHandle    = DiffuseConstTable->GetConstantByName(0, "ViewMatrix");
				ViewProjMatrixHandle= DiffuseConstTable->GetConstantByName(0, "ViewProjMatrix");
				AmbientMtrlHandle   = DiffuseConstTable->GetConstantByName(0, "AmbientMtrl");
				DiffuseMtrlHandle   = DiffuseConstTable->GetConstantByName(0, "DiffuseMtrl");
				LightDirHandle      = DiffuseConstTable->GetConstantByName(0, "LightDirection");


				//
				// Set shader constants:
				//

				// Light direction:
				D3DXVECTOR4 directionToLight(-0.57f, 0.57f, -0.57f, 0.0f);
				DiffuseConstTable->SetVector(Device, LightDirHandle, &directionToLight);

				// Materials:
				D3DXVECTOR4 ambientMtrl(0.0f, 0.0f, 1.0f, 1.0f);
				D3DXVECTOR4 diffuseMtrl(0.0f, 0.0f, 1.0f, 1.0f);

				DiffuseConstTable->SetVector(Device,AmbientMtrlHandle,&ambientMtrl);
				DiffuseConstTable->SetVector(Device,DiffuseMtrlHandle,&diffuseMtrl);
				DiffuseConstTable->SetDefaults(Device);

				//
				// Set Projection Matrix.
				//
				D3DXMatrixPerspectiveFovLH(
					&Proj, D3DX_PI * 0.25f, 
					//(float)Width / (float)Height, 
					(float)pDevice->getVideoDriver()->getScreenSize().Width / (float)pDevice->getVideoDriver()->getScreenSize().Height,					
					1.0f, 1000.0f);

			}			

			while(pDevice->run())
			{
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//esc키누루면 종료
				if(::GetAsyncKeyState(VK_ESCAPE) && 0x8000)
				{
					pDevice->closeDevice();
				}

				{
					LPDIRECT3DDEVICE9 Device = pD9Device;
					// 
					// Update the scene: Allow user to rotate around scene.
					//

					static float angle  = (3.0f * D3DX_PI) / 2.0f;
					static float height = 5.0f;

					if( ::GetAsyncKeyState(VK_LEFT) & 0x8000f )
						angle -= 0.5f * fDelta;

					if( ::GetAsyncKeyState(VK_RIGHT) & 0x8000f )
						angle += 0.5f * fDelta;

					if( ::GetAsyncKeyState(VK_UP) & 0x8000f )
						height += 5.0f * fDelta;

					if( ::GetAsyncKeyState(VK_DOWN) & 0x8000f )
						height -= 5.0f * fDelta;

					D3DXVECTOR3 position( cosf(angle) * 10.0f, height, sinf(angle) * 10.0f );
					D3DXVECTOR3 target(0.0f, 0.0f, 0.0f);
					D3DXVECTOR3 up(0.0f, 1.0f, 0.0f);
					D3DXMATRIX V;
					D3DXMatrixLookAtLH(&V, &position, &target, &up);					

					DiffuseConstTable->SetMatrix(Device, ViewMatrixHandle, &V);

					D3DXMATRIX ViewProj = V * Proj;
					DiffuseConstTable->SetMatrix(Device, ViewProjMatrixHandle, &ViewProj);
					//
					// Render
					//

					Device->Clear(0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 0xffffffff, 1.0f, 0);
					Device->BeginScene();

					Device->SetVertexShader(DiffuseShader);
					Teapot->DrawSubset(0);

					Device->EndScene();
					Device->Present(0, 0, 0, 0);
				}				
			}

			DiffuseConstTable->Release();
			Teapot->Release();
			DiffuseShader->Release();


			pDevice->drop();
		}

	}


	//카툰랜더
	namespace _23
	{			
		void main()
		{		

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9  
				); 
			pDevice->run();

			//다이랙트 3디 디바이스얻기
			LPDIRECT3DDEVICE9 pD9Device = pDevice->getVideoDriver()->getExposedVideoData().D3D9.D3DDev9;	

			IDirect3DVertexShader9* ToonShader = 0;
			ID3DXConstantTable* ToonConstTable = 0;

			ID3DXMesh*  Meshes[4] = {0, 0, 0, 0};
			D3DXMATRIX  WorldMatrices[4];
			D3DXVECTOR4 MeshColors[4];

			D3DXMATRIX ProjMatrix;

			IDirect3DTexture9* ShadeTex  = 0;

			D3DXHANDLE WorldViewHandle     = 0;
			D3DXHANDLE WorldViewProjHandle = 0;
			D3DXHANDLE ColorHandle         = 0;
			D3DXHANDLE LightDirHandle      = 0;

			{
				HRESULT hr = 0;

				//
				// Create geometry and compute corresponding world matrix and color
				// for each mesh.
				//

				D3DXCreateTeapot(pD9Device, &Meshes[0], 0);
				D3DXCreateSphere(pD9Device, 1.0f, 20, 20, &Meshes[1], 0);
				D3DXCreateTorus(pD9Device, 0.5f, 1.0f, 20, 20, &Meshes[2], 0);
				D3DXCreateCylinder(pD9Device, 0.5f, 0.5f, 2.0f, 20, 20, &Meshes[3], 0);

				D3DXMatrixTranslation(&WorldMatrices[0],  0.0f,  2.0f, 0.0f);
				D3DXMatrixTranslation(&WorldMatrices[1],  0.0f, -2.0f, 0.0f);
				D3DXMatrixTranslation(&WorldMatrices[2], -3.0f,  0.0f, 0.0f);
				D3DXMatrixTranslation(&WorldMatrices[3],  3.0f,  0.0f, 0.0f);

				MeshColors[0] = D3DXVECTOR4(1.0f, 0.0f, 0.0f, 1.0f);
				MeshColors[1] = D3DXVECTOR4(0.0f, 1.0f, 0.0f, 1.0f);
				MeshColors[2] = D3DXVECTOR4(0.0f, 0.0f, 1.0f, 1.0f);
				MeshColors[3] = D3DXVECTOR4(1.0f, 1.0f, 0.0f, 1.0f);

				//
				// Compile shader
				//

				ID3DXBuffer* shader      = 0;
				ID3DXBuffer* errorBuffer = 0;

				hr = D3DXCompileShaderFromFile(
					L"../../res/dx_media/toon.txt",
					0,
					0,
					"Main", // entry point function name
					"vs_1_1",
					D3DXSHADER_DEBUG | D3DXSHADER_ENABLE_BACKWARDS_COMPATIBILITY, 
					&shader,
					&errorBuffer,
					&ToonConstTable);

				// output any error messages
				if( errorBuffer )
				{
					::MessageBox(0, bstr_t((char*)errorBuffer->GetBufferPointer()), 0, 0);
					//d3d::Release<ID3DXBuffer*>(errorBuffer);
					errorBuffer->Release();
				}

				if(FAILED(hr))
				{
					::MessageBox(0, L"D3DXCompileShaderFromFile() - FAILED", 0, 0);
					//return false;
				}

				hr = pD9Device->CreateVertexShader(
					(DWORD*)shader->GetBufferPointer(),
					&ToonShader);

				if(FAILED(hr))
				{
					::MessageBox(0, L"CreateVertexShader - FAILED", 0, 0);
					//return false;
				}

				shader->Release();
				//d3d::Release<ID3DXBuffer*>(shader);

				//
				// Load textures.
				//

				D3DXCreateTextureFromFile(pD9Device, L"../../res/dx_media/toonshade.bmp", &ShadeTex);

				pD9Device->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_POINT);
				pD9Device->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_POINT);
				pD9Device->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_NONE);

				// 
				// Get Handles
				//

				WorldViewHandle     = ToonConstTable->GetConstantByName(0, "WorldViewMatrix");
				WorldViewProjHandle = ToonConstTable->GetConstantByName(0, "WorldViewProjMatrix");
				ColorHandle         = ToonConstTable->GetConstantByName(0, "Color");
				LightDirHandle      = ToonConstTable->GetConstantByName(0, "LightDirection");

				//
				// Set shader constants:
				//

				// Light direction:
				D3DXVECTOR4 directionToLight(-0.57f, 0.57f, -0.57f, 0.0f);

				ToonConstTable->SetVector(
					pD9Device, 
					LightDirHandle,
					&directionToLight);

				ToonConstTable->SetDefaults(pD9Device);

				// 
				// Compute projection matrix.
				// 

				D3DXMatrixPerspectiveFovLH(
					&ProjMatrix, D3DX_PI * 0.25f, 
					640.f/480.f,
					1.0f, 1000.0f);

			}


			while(pDevice->run())
			{
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//esc키누루면 종료
				if(::GetAsyncKeyState(VK_ESCAPE) && 0x8000)
				{
					pDevice->closeDevice();
				}

				{					
					// 
					// Update the scene: Allow user to rotate around scene.
					//

					static float angle  = (3.0f * D3DX_PI) / 2.0f;
					static float height = 5.0f;

					if( ::GetAsyncKeyState(VK_LEFT) & 0x8000f )
						angle -= 0.5f * fDelta;

					if( ::GetAsyncKeyState(VK_RIGHT) & 0x8000f )
						angle += 0.5f * fDelta;

					if( ::GetAsyncKeyState(VK_UP) & 0x8000f )
						height += 5.0f * fDelta;

					if( ::GetAsyncKeyState(VK_DOWN) & 0x8000f )
						height -= 5.0f * fDelta;


					D3DXVECTOR3 position( cosf(angle) * 7.0f, height, sinf(angle) * 7.0f );
					D3DXVECTOR3 target(0.0f, 0.0f, 0.0f);
					D3DXVECTOR3 up(0.0f, 1.0f, 0.0f);
					D3DXMATRIX view;
					D3DXMatrixLookAtLH(&view, &position, &target, &up);

					//
					// Render
					//

					pD9Device->Clear(0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 0xffffffff, 1.0f, 0);
					pD9Device->BeginScene();

					pD9Device->SetVertexShader(ToonShader);
					pD9Device->SetTexture(0, ShadeTex);

					D3DXMATRIX WorldView;
					D3DXMATRIX WorldViewProj;
					for(int i = 0; i < 4; i++)
					{
						WorldView     = WorldMatrices[i] * view;
						WorldViewProj = WorldMatrices[i] * view * ProjMatrix;

						ToonConstTable->SetMatrix(
							pD9Device, 
							WorldViewHandle,
							&WorldView);

						ToonConstTable->SetMatrix(
							pD9Device,
							WorldViewProjHandle,
							&WorldViewProj);

						ToonConstTable->SetVector(
							pD9Device,
							ColorHandle,
							&MeshColors[i]);

						Meshes[i]->DrawSubset(0);
					}

					pD9Device->EndScene();
					pD9Device->Present(0, 0, 0, 0);
				}				
			}
			pDevice->drop();
		}

	}



	//X파일 읽기 예제2
	namespace _24
	{			
		void main()
		{		

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9  
				); 
			pDevice->run();

			//다이랙트 3디 디바이스얻기
			LPDIRECT3DDEVICE9 pD9Device = pDevice->getVideoDriver()->getExposedVideoData().D3D9.D3DDev9;	

			ID3DXMesh*                      Mesh = 0;
			std::vector<D3DMATERIAL9>       Mtrls(0);
			std::vector<IDirect3DTexture9*> Textures(0);

			//
			// Framework functions
			//

			{
				LPDIRECT3DDEVICE9 Device =  pD9Device;

				HRESULT hr = 0;

				//
				// Load the XFile data.
				//

				ID3DXBuffer* adjBuffer  = 0;
				ID3DXBuffer* mtrlBuffer = 0;
				DWORD        numMtrls   = 0;

				irr::core::stringw strwResDir = L"../../res/dx_media/tiny/";

				hr = D3DXLoadMeshFromX(
					(strwResDir + L"tiny.x").c_str(),					
					D3DXMESH_MANAGED,
					Device,
					&adjBuffer,//인접정보
					&mtrlBuffer,//재질정보
					0,
					&numMtrls,
					&Mesh);

				if(FAILED(hr))
				{
					::MessageBox(0, L"D3DXLoadMeshFromX() - FAILED", 0, 0);		
				}

				//
				// Extract the materials, and load textures.
				//

				if( mtrlBuffer != 0 && numMtrls != 0 )
				{
					D3DXMATERIAL* mtrls = (D3DXMATERIAL*)mtrlBuffer->GetBufferPointer();

					irr::u32 i;
					for( i=0; i < numMtrls; i++)
					{
						// the MatD3D property doesn't have an ambient value set
						// when its loaded, so set it now:
						mtrls[i].MatD3D.Ambient = mtrls[i].MatD3D.Diffuse;

						// save the ith material
						Mtrls.push_back( mtrls[i].MatD3D );

						// check if the ith material has an associative texture
						if( mtrls[i].pTextureFilename != 0 )
						{
							// yes, load the texture for the ith subset
							IDirect3DTexture9* tex = 0;

							D3DXCreateTextureFromFile(
								Device,
								(strwResDir + irr::core::stringw(mtrls[i].pTextureFilename)).c_str(),
								//_bstr_t(mtrls[i].pTextureFilename),
								&tex);

							// save the loaded texture
							Textures.push_back( tex );
						}
						else
						{
							// no texture for the ith subset
							Textures.push_back( 0 );
						}
					}
				}
				mtrlBuffer->Release();

				//d3d::Release<ID3DXBuffer*>(mtrlBuffer); // done w/ buffer

				//
				// Optimize the mesh.
				//

				hr = Mesh->OptimizeInplace(
					D3DXMESHOPT_ATTRSORT |
					D3DXMESHOPT_COMPACT  |
					D3DXMESHOPT_VERTEXCACHE,
					(DWORD*)adjBuffer->GetBufferPointer(),
					0, 0, 0);

				adjBuffer->Release();
				//d3d::Release<ID3DXBuffer*>(adjBuffer); // done w/ buffer

				if(FAILED(hr))
				{
					::MessageBox(0, L"OptimizeInplace() - FAILED", 0, 0);

				}

				//
				// Set texture filters.
				//

				Device->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
				Device->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
				Device->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_POINT);

				// 
				// Set Lights.
				//

				D3DXVECTOR3 dir(1.0f, -1.0f, 1.0f);
				D3DXCOLOR col(1.0f, 1.0f, 1.0f, 1.0f);
				D3DLIGHT9 light;// = d3d::InitDirectionalLight(&dir, &col);
				light.Direction = dir;
				light.Type = D3DLIGHT_DIRECTIONAL;
				light.Diffuse = col;
				light.Ambient = col * .4f;
				light.Specular = col * .6f;

				Device->SetLight(0, &light);
				Device->LightEnable(0, true);
				Device->SetRenderState(D3DRS_NORMALIZENORMALS, true);
				Device->SetRenderState(D3DRS_SPECULARENABLE, true);

				//
				// Set camera.
				//

				D3DXVECTOR3 pos(0.0f, 10.0f, -800.0f);
				D3DXVECTOR3 target(0.0f, 0.0f, 0.0f);
				D3DXVECTOR3 up(0.0f, 1.0f, 0.0f);

				D3DXMATRIX V;
				D3DXMatrixLookAtLH(
					&V,
					&pos,
					&target,
					&up);

				Device->SetTransform(D3DTS_VIEW, &V);

				//
				// Set projection matrix.
				//

				D3DXMATRIX proj;
				D3DXMatrixPerspectiveFovLH(
					&proj,
					D3DX_PI * 0.5f, // 90 - degree
					//(float)Width / (float)Height,
					(float)pDevice->getVideoDriver()->getScreenSize().Width / (float)pDevice->getVideoDriver()->getScreenSize().Height,					
					1.0f,
					1000.0f);
				Device->SetTransform(D3DTS_PROJECTION, &proj);

				//return true;
			}


			while(pDevice->run())
			{
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//esc키누루면 종료
				if(::GetAsyncKeyState(VK_ESCAPE) && 0x8000)
				{
					pDevice->closeDevice();
				}

				pD9Device->Clear(0,0,D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER,0x000000ff,1.0f,0);

				{
					LPDIRECT3DDEVICE9 Device =  pD9Device;
					//
					// Update: Rotate the mesh.
					//

					static float y = 0.0f;
					D3DXMATRIX yRot;
					D3DXMatrixRotationY(&yRot, y);
					y += fDelta;

					if( y >= 6.28f )
						y = 0.0f;

					D3DXMATRIX World = yRot;

					Device->SetTransform(D3DTS_WORLD, &World);

					//
					// Render
					//

					//Device->Clear(0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 0xffffffff, 1.0f, 0);
					Device->BeginScene();

					for(DWORD i = 0; i < Mtrls.size(); i++)
					{
						Device->SetMaterial( &Mtrls[i] );
						Device->SetTexture(0, Textures[i]);
						Mesh->DrawSubset(i);
					}	

					Device->EndScene();
					//Device->Present(0, 0, 0, 0);
				}

				pD9Device->Present(0,0,0,0);
			}

			Mesh->Release();

			pDevice->drop();
		}

	}

	//DX용 그림자 행렬 사용하기
	namespace _25
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9,
				irr::core::dimension2du(640,480),32,
				false,
				true
				);
			pDevice->run();


			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");
			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,9,-15), irr::core::vector3df(0,5,0));

			//닌자 추가
			{
				irr::scene::IAnimatedMeshSceneNode *pNode =
					pSmgr->addAnimatedMeshSceneNode(pSmgr->getMesh("ninja.b3d"));
				pNode->setMaterialTexture(0,pVideo->getTexture("nskinbl.jpg"));				
				pNode->setAnimationSpeed(30.f);
				pNode->setFrameLoop(206,250);
				pNode->setRotation(irr::core::vector3df(0,180,0));
				pNode->setPosition(irr::core::vector3df(0,0,0));
				pNode->setName("usr/scene/ninja/1");

				//정반사광 재질설정
				pNode->setMaterialFlag(irr::video::EMF_NORMALIZE_NORMALS,true);
				pNode->getMaterial(0).Shininess = 64;
				pNode->getMaterial(0).SpecularColor = irr::video::SColor(255,255,255,255);				
				pNode->getMaterial(0).AmbientColor = irr::video::SColor(255,128,128,128);
				pNode->getMaterial(0).GouraudShading = true;

				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);

				// add shadow
				//pNode->addShadowVolumeSceneNode();
				//pSmgr->setShadowColor(irr::video::SColor(150,0,0,0));

			}

			//바닥추가
			{
				irr::scene::IAnimatedMeshSceneNode* pNode = 0;
				irr::scene::IAnimatedMesh* pMesh;			

				pMesh = pSmgr->addHillPlaneMesh("usr/mesh/hill",
					irr::core::dimension2d<irr::f32>(8,8),
					irr::core::dimension2d<irr::u32>(20,20), 0,0,
					irr::core::dimension2d<irr::f32>(2,2),
					irr::core::dimension2d<irr::f32>(8,8));

				pNode = pSmgr->addAnimatedMeshSceneNode(pMesh);			
				pNode->setMaterialTexture(0, pVideo->getTexture("wall.jpg"));			
				pNode->setName("usr/scene/hill");
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);

			}

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);


			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				{				
					irr::scene::ISceneNode *pNode;
					pNode = pSmgr->getSceneNodeFromName("usr/scene/ninja/1");
					if(GetAsyncKeyState(VK_RIGHT) && 0x8000)
					{
						pNode->setPosition(pNode->getPosition() - (irr::core::vector3df(5,0,0) * fDelta));
					}
					if(GetAsyncKeyState(VK_LEFT) && 0x8000)
					{
						pNode->setPosition(pNode->getPosition() + (irr::core::vector3df(5,0,0) * fDelta));
					}
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				{//수동으로 그림자 만들어 출력
					const irr::core::plane3df plane(0, .1f, 0, 0, 1, 0); 
					irr::core::matrix4 matShadow;
					//mat.buildShadowMatrix(irr::core::vector3df(1,1,0),plane,0);

					// position shadow
					D3DXVECTOR4 lightDirection(0.707f, -0.707f, 0.707f, 0.0f);
					D3DXPLANE groundPlane(0.0f, -1.0f, 0.0f, 0.1f);//인자값 (a,b,c,d) 는 ax + by + cx - d = 0

					D3DXMATRIX S;
					D3DXMatrixShadow(
						&S,
						&lightDirection,
						&groundPlane);

					irr::s32 r,c;
					for(r = 0;r<4;r++)
						for(c = 0;c<4;c++)
							matShadow(r,c) = S(r,c);


					irr::scene::IAnimatedMeshSceneNode *pNode = (irr::scene::IAnimatedMeshSceneNode *)pSmgr->getSceneNodeFromName(
						"usr/scene/ninja/1"
						);
					irr::scene::IMesh* mesh = pNode->getMesh()->getMesh(irr::core::round32(pNode->getFrameNr()));


					matShadow *= pNode->getAbsoluteTransformation(); 
					pVideo->setTransform(irr::video::ETS_WORLD, matShadow); 

					{
						irr::video::SMaterial m = pNode->getMaterial(0);
						m.setTexture(0,pVideo->getTexture("nskinrd.jpg"));
						pVideo->setMaterial(m);
					}

					irr::u32 b; 
					for (b = 0; b < mesh->getMeshBufferCount(); ++b) 
						pVideo->drawMeshBuffer(mesh->getMeshBuffer(b)); 

				}

				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}


}

#endif
