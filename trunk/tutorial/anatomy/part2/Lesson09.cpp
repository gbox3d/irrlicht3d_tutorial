﻿#include <irrlicht.h>
#include <iostream>

#include <comutil.h>
#pragma comment (lib,"comsuppw.lib" )
#include <windows.h> // this example only runs with windows


//고급 GUI 예제

namespace Lesson09 {


	//폰트예제
	namespace _00
	{
		class myEventHandler : public irr::IEventReceiver
		{
			bool OnEvent(const irr::SEvent& eve)
			{

				return false;
			}
		};

		void main()
		{	

			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9
				//irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"폰트 예제");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res/");

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,10,-20), irr::core::vector3df(0,0,0));

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);	

			irr::gui::IGUIFont* font = pGuiEnv->getFont("jga/myfont.xml");			

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				font->draw(L"Hello",irr::core::recti(100,100,200,200),irr::video::SColor(255,255,255,0));
				
				font->draw(L"한글날 맞이 이벤트!!",
					irr::core::recti(100,150,200,200),
					irr::video::SColor(255,255,255,0));
				
				font->draw(L"특수 한글도 지원함 ^^ 예> 밞똚밹햏힣",irr::core::recti(100,200,200,200),irr::video::SColor(255,255,255,0));

				pVideo->endScene();	
			}

			pDevice->drop();

		}
	}

	//테이블 예제
	namespace _01
	{
		class myEventHandler : public irr::IEventReceiver
		{
			bool OnEvent(const irr::SEvent& eve)
			{
				if(eve.EventType == irr::EET_GUI_EVENT)
				{
					if(eve.GUIEvent.EventType == irr::gui::EGET_TABLE_SELECTED_AGAIN)
					{
						irr::gui::IGUITable *pTable = (irr::gui::IGUITable *)eve.GUIEvent.Caller;
					//	pTable->setActiveColumn(
						std::cout << pTable->getActiveColumn() << std::endl;
					}
					if(eve.GUIEvent.EventType == irr::gui::EGET_TABLE_CHANGED)
					{
						irr::gui::IGUITable *pTable = (irr::gui::IGUITable *)eve.GUIEvent.Caller;
						std::cout << pTable->getActiveColumn() << "," << pTable->getSelected() << std::endl;
					}
				}

				return false;
			}
		};

		void main()
		{	

			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9
				//irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"태이블예제");

			myEventHandler Receiver;
			pDevice->setEventReceiver(&Receiver);

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,10,-20), irr::core::vector3df(0,0,0));

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			irr::gui::IGUITable *pTable = 
				pGuiEnv->addTable(irr::core::recti(0,60,220,140),0,666);

			pTable->addColumn(L"colm1",0);
			pTable->addColumn(L"colm2",1);
			pTable->setColumnWidth(0,60);
			pTable->setColumnWidth(1,160);
			pTable->addRow(1);
			pTable->setCellText(0,0,L"0_0");
			pTable->setCellText(0,1,L"0_1");

			pTable->addRow(2);
			pTable->setCellText(1,0,L"1_0");
			pTable->setCellText(1,1,L"1_1");

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();

		}
	}


	//한글입력예제한글
	namespace _02
	{
		irr::gui::IGUIEditBox *pEdBox;

		static LRESULT CALLBACK CustomWndProc(HWND hWnd, UINT message,
			WPARAM wParam, LPARAM lParam)
		{
			switch (message)
			{			
			case WM_IME_CHAR:
				{
					wchar_t ch = (wchar_t)wParam;
					std::cout << bstr_t(ch) << std::endl;

					irr::SEvent ev;
					ev.EventType = irr::EET_KEY_INPUT_EVENT;
					ev.KeyInput.Control = false;
					ev.KeyInput.Char = ch;
					ev.KeyInput.PressedDown = true;
					pEdBox->OnEvent(ev);
					
					//return 0;
				}

				break;
			case WM_DESTROY:
				PostQuitMessage(0);
				return 0;

			}

			return DefWindowProc(hWnd, message, wParam, lParam);
		}

		void main()
		{
			HINSTANCE hInstance = 0;
			// create dialog

			const WCHAR* Win32ClassName = L"CIrrlichtWindowsTestDialog";

			WNDCLASSEX wcex;
			wcex.cbSize			= sizeof(WNDCLASSEX);
			wcex.style			= CS_HREDRAW | CS_VREDRAW;
			wcex.lpfnWndProc	= (WNDPROC)CustomWndProc;
			wcex.cbClsExtra		= 0;
			wcex.cbWndExtra		= DLGWINDOWEXTRA;
			wcex.hInstance		= hInstance;
			wcex.hIcon			= NULL;
			wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
			wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW);
			wcex.lpszMenuName	= 0;
			wcex.lpszClassName	= Win32ClassName;
			wcex.hIconSm		= 0;

			RegisterClassEx(&wcex);

			DWORD style = WS_SYSMENU | WS_BORDER | WS_CAPTION |
				WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_MAXIMIZEBOX | WS_MINIMIZEBOX | WS_SIZEBOX;

			int windowWidth = 640;
			int windowHeight = 480;

			HWND hWnd;

			hWnd = CreateWindow( Win32ClassName, L"Irrlicht Win32 IME 한글입력예제",
				style, 100, 100, windowWidth, windowHeight,
				NULL, NULL, hInstance, NULL);

			RECT clientRect;
			GetClientRect(hWnd, &clientRect);
			windowWidth = clientRect.right;
			windowHeight = clientRect.bottom;

			// create ok button

			//hOKButton = CreateWindow("BUTTON", "OK - Close", WS_CHILD | WS_VISIBLE | BS_TEXT,
			//	windowWidth - 160, windowHeight - 40, 150, 30, hWnd, NULL, hInstance, NULL);

			//// create some text

			//CreateWindow("STATIC", "This is Irrlicht running inside a standard Win32 window.\n"\
			//	"Also mixing with MFC and .NET Windows.Forms is possible.",
			//	WS_CHILD | WS_VISIBLE, 20, 20, 400, 40, hWnd, NULL, hInstance, NULL);

			//// create window to put irrlicht in

			//HWND hIrrlichtWindow = CreateWindow("BUTTON", "",
			//	WS_CHILD | WS_VISIBLE | BS_OWNERDRAW,
			//	50, 80, 320, 220, hWnd, NULL, hInstance, NULL);

			/*
			So now that we have some window, we can create an Irrlicht device
			inside of it. We use Irrlicht createEx() function for this. We only
			need the handle (HWND) to that window, set it as windowsID parameter
			and start up the engine as usual. That's it.
			*/
			// create irrlicht device in the button window

			irr::SIrrlichtCreationParameters param;
			param.WindowId = reinterpret_cast<void*>(hWnd);
			param.DriverType = irr::video::EDT_DIRECT3D9;

			irr::IrrlichtDevice* pDevice = irr::createDeviceEx(param);			

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res/");
			irr::gui::IGUIFont* font = pGuiEnv->getFont("jga/myfont.xml");		
			pGuiEnv->getSkin()->setFont(font);

			pEdBox = pGuiEnv->addEditBox(L"한글",irr::core::recti(100,100,400,130));

			

			ShowWindow(hWnd , SW_SHOW);
			UpdateWindow(hWnd);

			while (pDevice->run())
			{
				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));
				pSmgr->drawAll();
				pGuiEnv->drawAll();
				pVideo->endScene();
			}

			pDevice->closeDevice();
			pDevice->drop();

		}

	}


	
	//gui 에디터 연동하기
	namespace _03
	{
		class myEventHandler : public irr::IEventReceiver
		{
		public:
			irr::gui::IGUIEnvironment *m_pGuiEnv;

			bool OnEvent(const irr::SEvent& eve)
			{
				if(eve.EventType == irr::EET_GUI_EVENT)
				{
					
					
				}
				if(eve.EventType == irr::EET_KEY_INPUT_EVENT)
				{
					//f1을 누루면 창이뜬다.
					if(eve.KeyInput.Key == irr::KEY_F1)
					{
						if(eve.KeyInput.PressedDown == false)
						{
							irr::gui::IGUIWindow *pElm = 
								(irr::gui::IGUIWindow *)m_pGuiEnv->getRootGUIElement()->getElementFromId(100);
						
							//창이 존재하면 다시뜨지않는다.
							if(!pElm)
							{
								m_pGuiEnv->loadGUI("guiTest.xml");		
							}
							else
							{
								pElm->setVisible(true);
							}
						}						
					}
				}



				return false;
			}
		};

		void main()
		{	

			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9
				//irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"gui editor예제");

			myEventHandler Receiver;
			pDevice->setEventReceiver(&Receiver);

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
			
			Receiver.m_pGuiEnv =  pGuiEnv;

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,10,-20), irr::core::vector3df(0,0,0));

			////프레임 레이트 표시용 유아이
			//irr::gui::IGUIStaticText *pstextFPS = 
			//	pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);		
			//파일에서읽기
			pGuiEnv->loadGUI("guiTest.xml");

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				////프레임레이트 갱신, 삼각형수 표시
				//{
				//	wchar_t wszbuf[256];
				//	swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
				//	pstextFPS->setText(wszbuf);
				//}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();

		}
	}

	//메뉴예제
	namespace _04
	{
		class myEventHandler : public irr::IEventReceiver
		{
		public:
			irr::gui::IGUIEnvironment *m_pGuiEnv;

			void Init(irr::gui::IGUIEnvironment *pGuiEnv)
			{
				m_pGuiEnv = pGuiEnv;
				irr::gui::IGUIContextMenu* menu = pGuiEnv->addMenu();
				irr::gui::IGUIContextMenu* submenu;
				
				menu->addItem(L"File", -1, true, true);
				submenu = menu->getSubMenu(0); //첫번째 항목의 서브메뉴
				submenu->addItem(L"Open",100);
				
				menu->addItem(L"View", -1, true, true);
				menu->addItem(L"Camera", -1, true, true);
				menu->addItem(L"Help", -1, true, true);

			}
			bool OnEvent(const irr::SEvent& eve)
			{
				if(eve.EventType == irr::EET_GUI_EVENT)
				{
					if(eve.GUIEvent.EventType == irr::gui::EGET_MENU_ITEM_SELECTED)
					{
						irr::gui::IGUIContextMenu *pMenuItem = 	(irr::gui::IGUIContextMenu*)eve.GUIEvent.Caller;

						irr::s32 id = pMenuItem->getItemCommandId(pMenuItem->getSelectedItem());

						if(id == 100)
						{							
							irr::io::path oldpath = m_pGuiEnv->getFileSystem()->getWorkingDirectory();
							m_pGuiEnv->getFileSystem()->changeWorkingDirectoryTo("../../res"); //시작 디랙토리지정
							m_pGuiEnv->addFileOpenDialog(L"test");
							m_pGuiEnv->getFileSystem()->changeWorkingDirectoryTo(oldpath); //원래 대로 복귀						
						}
						
					}
					else if(eve.GUIEvent.EventType == irr::gui::EGET_FILE_SELECTED)
					{
						irr::gui::IGUIFileOpenDialog* dialog =
							(irr::gui::IGUIFileOpenDialog*)eve.GUIEvent.Caller;
						irr::core::stringc strTemp = dialog->getFileName(); //와이드캐릭터를 멀티바이트로변환.
						printf("%s \n",strTemp.c_str() );
					}

				}
				if(eve.EventType == irr::EET_KEY_INPUT_EVENT)
				{					
					if(eve.KeyInput.Key == irr::KEY_F10)
					{
						if(eve.KeyInput.PressedDown == false)
						{
							
						}						
					}
				}

				return false;
			}
		};

		void main()
		{	
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9								
				);

			pDevice->setWindowCaption(L"gui [] 메뉴 & 파일다얄로그 예제");

			myEventHandler Receiver;
			pDevice->setEventReceiver(&Receiver);

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
			
			Receiver.Init(pGuiEnv);// =  pGuiEnv;		

			while(pDevice->run())
			{
				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();

		}
	}

	//컨트롤에 직접 폰트 적용 시키기
	//setOverrideFont 이용
	namespace _05
	{
		class myEventHandler : public irr::IEventReceiver
		{
		public:
			irr::gui::IGUIEnvironment *m_pGuiEnv;

			bool OnEvent(const irr::SEvent& eve)
			{
				if(eve.EventType == irr::EET_GUI_EVENT)
				{
					
					
				}
				if(eve.EventType == irr::EET_KEY_INPUT_EVENT)
				{					
					if(eve.KeyInput.Key == irr::KEY_F10)
					{
						if(eve.KeyInput.PressedDown == false)
						{
							
						}						
					}
				}

				return false;
			}
		};

		void main()
		{	
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9								
				);

			pDevice->setWindowCaption(L"gui [] 예제");

			myEventHandler Receiver;
			pDevice->setEventReceiver(&Receiver);

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
			
			Receiver.m_pGuiEnv =  pGuiEnv;		

			irr::gui::IGUIFont *m_pFont = pGuiEnv->getFont("../../res/lucida.xml");

			pGuiEnv->addButton(irr::core::recti( 0,0,100,100), 0, 1 );

			{
				irr::gui::IGUIStaticText *pStaticTextTop = 
					(irr::gui::IGUIStaticText *)pGuiEnv->getRootGUIElement()->getElementFromId( 1 );		
				pStaticTextTop->setOverrideFont( m_pFont );
				pStaticTextTop->setText(L"Hello");
			}

			pGuiEnv->addButton(irr::core::recti( 0,100,100,200), 0, 2 );
			{
				irr::gui::IGUIStaticText *pStaticTextTop = 
					(irr::gui::IGUIStaticText *)pGuiEnv->getRootGUIElement()->getElementFromId( 2 );		
				//pStaticTextTop->setOverrideFont( m_pFont );
				pStaticTextTop->setText(L"Hello");
			}

			

			while(pDevice->run())
			{
				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();

		}

	}

	//GUI 기본 프레임웍
	namespace _99
	{
		class myEventHandler : public irr::IEventReceiver
		{
		public:
			irr::gui::IGUIEnvironment *m_pGuiEnv;

			bool OnEvent(const irr::SEvent& eve)
			{
				if(eve.EventType == irr::EET_GUI_EVENT)
				{
					
					
				}
				if(eve.EventType == irr::EET_KEY_INPUT_EVENT)
				{					
					if(eve.KeyInput.Key == irr::KEY_F10)
					{
						if(eve.KeyInput.PressedDown == false)
						{
							
						}						
					}
				}

				return false;
			}
		};

		void main()
		{	
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9								
				);

			pDevice->setWindowCaption(L"gui [] 예제");

			myEventHandler Receiver;
			pDevice->setEventReceiver(&Receiver);

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
			
			Receiver.m_pGuiEnv =  pGuiEnv;			
			

			while(pDevice->run())
			{
				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();

		}
	}
}