﻿#include <irrlicht.h>

#include <math.h>
#include <iostream>
#include <vector>

//#ifdef USE_MS_DX9
#include <d3d9.h>
#include <comutil.h>
#pragma comment (lib,"comsuppw.lib" )
//#endif

//캐릭터 애니 메이션 예제모음
namespace Lesson08 {

	//사원수 VS 오일러 비교 예제
	namespace _00
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9
				);
			pDevice->setWindowCaption(L"키프레임 애니 : 사원수 VS 오일러 비교 예제");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,2,-5), irr::core::vector3df(0,0,0));

			{				
				pSmgr->addArrowMesh("usr/mesh/arrow"); //메쉬등록	
				pSmgr->addArrowMesh("usr/mesh/arrow/red",
					irr::video::SColor(255,255,0,0),
					irr::video::SColor(255,255,0,0)
					); //메쉬등록	
				pSmgr->addArrowMesh("usr/mesh/arrow/green",
					irr::video::SColor(255,0,255,0),
					irr::video::SColor(255,0,255,0)
					); //메쉬등록	
			}

			irr::core::vector3df ani_pos[2] = {irr::core::vector3df(0,0,0),irr::core::vector3df(3,0,0)};
			irr::core::quaternion ani_qrot[2] = {
				irr::core::quaternion(irr::core::vector3df(0,0,0)), 
				irr::core::quaternion(irr::core::vector3df(90,180,-120)*irr::core::DEGTORAD)
			};
			irr::core::vector3df ani_rot[2] = {irr::core::vector3df(0,0,0),irr::core::vector3df(90,180,-120) };

			{
				irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(
					pSmgr->getMesh("usr/mesh/arrow"));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);

				pNode->setPosition(ani_pos[0]);
				irr::core::vector3df Euer_rot;
				ani_qrot[0].toEuler(Euer_rot);
				pNode->setRotation(Euer_rot * irr::core::RADTODEG);
			}

			{				
				irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(
					pSmgr->getMesh("usr/mesh/arrow"));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);				

				pNode->setPosition(ani_pos[1]);
				irr::core::vector3df Euer_rot;
				ani_qrot[1].toEuler(Euer_rot);
				pNode->setRotation(Euer_rot * irr::core::RADTODEG);
			}


			{				
				irr::s32 i;
				irr::f32 t = .1f;

				for(i=0;i<12;i++)
				{
					irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(
						pSmgr->getMesh("usr/mesh/arrow/red"));
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
					pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);				

					irr::core::vector3df pos = ani_pos[0];
					pos.interpolate(ani_pos[0],ani_pos[1],1.0-t);

					irr::core::quaternion qt;
					qt.slerp(ani_qrot[0],ani_qrot[1],t);


					pNode->setPosition(pos);
					irr::core::vector3df Euer_rot;
					qt.toEuler(Euer_rot);
					pNode->setRotation(Euer_rot * irr::core::RADTODEG);

					t += .08f;
				}
			}

			{				
				irr::s32 i;
				irr::f32 t = .1f;

				for(i=0;i<12;i++)
				{
					irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(
						pSmgr->getMesh("usr/mesh/arrow/green"));
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
					pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);				

					irr::core::vector3df pos = ani_pos[0];
					pos.interpolate(ani_pos[0],ani_pos[1],1.f-t);

					irr::core::vector3df rot;
					rot.interpolate(ani_rot[0],ani_rot[1],1.f-t);


					pNode->setPosition(pos);					
					pNode->setRotation(rot);
					t += .08f;
				}
			}



			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);


			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}


	//오일러 회전의 선형 보간 예
	namespace _01
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9
				);

			pDevice->setWindowCaption(L"키프레임 애니메이션 예제-1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,2,-5), irr::core::vector3df(0,0,0));

			{				
				pSmgr->addArrowMesh("usr/mesh/arrow"); //메쉬등록	
				pSmgr->addArrowMesh("usr/mesh/arrow/red",
					irr::video::SColor(255,255,0,0),
					irr::video::SColor(255,255,0,0)
					); //메쉬등록	
			}

			irr::core::vector3df ani_pos[2] = {irr::core::vector3df(0,0,0),irr::core::vector3df(3,0,0)};
			irr::core::vector3df ani_rot[2] = {irr::core::vector3df(0,0,0),irr::core::vector3df(90,180,-120) };

			//목표
			{
				irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(
					pSmgr->getMesh("usr/mesh/arrow"));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);

				pNode->setPosition(ani_pos[0]);				
				pNode->setRotation(ani_rot[0]);
			}

			//결과물
			{				
				irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(
					pSmgr->getMesh("usr/mesh/arrow"));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);				

				pNode->setPosition(ani_pos[1]);				
				pNode->setRotation(ani_rot[1]);
			}


			//중간 결과물
			{				
				irr::s32 i;
				irr::f32 t = .1f;

				for(i=0;i<10;i++)
				{
					irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(
						pSmgr->getMesh("usr/mesh/arrow/red"));
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
					pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);				

					irr::core::vector3df pos = ani_pos[0];
					pos.interpolate(ani_pos[0],ani_pos[1],1.f-t);

					irr::core::vector3df rot;
					rot.interpolate(ani_rot[0],ani_rot[1],1.f-t);


					pNode->setPosition(pos);
					pNode->setRotation(rot);
					t += .08f;
				}
			}


			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);


			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}

	//쿼터니온 회전이용 키프레임애니메이션 원리 보여주기
	//회전 보간으로 사원수사용
	namespace _02
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,2,-5), irr::core::vector3df(0,0,0));

			{				
				pSmgr->addArrowMesh("usr/mesh/arrow"); //메쉬등록	
				pSmgr->addArrowMesh("usr/mesh/arrow/red",
					irr::video::SColor(255,255,0,0),
					irr::video::SColor(255,255,0,0)
					); //메쉬등록	
			}

			irr::core::vector3df ani_pos[2] = {irr::core::vector3df(0,0,0),irr::core::vector3df(3,0,0)};
			irr::core::quaternion ani_rot[2] = {
				irr::core::quaternion(irr::core::vector3df(0,0,0)), 
				irr::core::quaternion(irr::core::vector3df(90,180,-120)*irr::core::DEGTORAD)
			};

			{
				irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(
					pSmgr->getMesh("usr/mesh/arrow"));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);

				pNode->setPosition(ani_pos[0]);
				irr::core::vector3df Euer_rot;
				ani_rot[0].toEuler(Euer_rot);
				pNode->setRotation(Euer_rot * irr::core::RADTODEG);
			}

			{				
				irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(
					pSmgr->getMesh("usr/mesh/arrow"));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);				

				pNode->setPosition(ani_pos[1]);
				irr::core::vector3df Euer_rot;
				ani_rot[1].toEuler(Euer_rot);
				pNode->setRotation(Euer_rot * irr::core::RADTODEG);
			}


			{				
				irr::s32 i;
				irr::f32 t = .1f;

				for(i=0;i<10;i++)
				{
					irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(
						pSmgr->getMesh("usr/mesh/arrow/red"));
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
					pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);				

					irr::core::vector3df pos = ani_pos[0];
					pos.interpolate(ani_pos[0],ani_pos[1],1.0-t);

					irr::core::quaternion qt;
					qt.slerp(ani_rot[0],ani_rot[1],t);


					pNode->setPosition(pos);
					irr::core::vector3df Euer_rot;
					qt.toEuler(Euer_rot);
					pNode->setRotation(Euer_rot * irr::core::RADTODEG);

					t += .08f;
				}
			}


			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);


			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}

	//스키닝
	namespace _03
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9
				);

			pDevice->setWindowCaption(L"hello irrlicht 3d engine");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res/");


			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,8,-10), irr::core::vector3df(0,5,0));

			//{		
			//	irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(
			//		pSmgr->getMesh("walkers/ninja/ninja.b3d") 
			//		);
			//	pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);				
			//	pNode->setFrameLoop(206,250);//대기동작
			//	pNode->setAnimationSpeed(30);
			//	pNode->setMaterialTexture(0,pVideo->getTexture("walkers/ninja/nskingr.jpg"));	
			//	
			//	pNode->setName("usr/scene/b3d/ninja/1");	
			//}

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);


			while(pDevice->run())
			{		
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				static irr::f32 fFrame = 1;
				if(::GetAsyncKeyState('1') & 0x8000)
				{
					fFrame += fDelta * 10.f;
				}
				if(::GetAsyncKeyState('2') & 0x8000)
				{
					fFrame -= fDelta * 10.f;
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));					

				pSmgr->drawAll();
				pGuiEnv->drawAll();	

				{	

					irr::scene::ISkinnedMesh *pSkinnedMesh = (irr::scene::ISkinnedMesh *)pSmgr->getMesh("walkers/ninja/ninja.b3d");												
					pSkinnedMesh->animateMesh(fFrame,1);					
					pSkinnedMesh->skinMesh();//스키닝적용

					irr::scene::IMeshBuffer *pmb = pSkinnedMesh->getMeshBuffer(0);		//칼빼고 몸통만....			


					{//월드변환
						pVideo->setTransform(irr::video::ETS_WORLD,
							irr::core::IdentityMatrix); //변환초기화					
					}

					{
						irr::video::SMaterial m;
						m.Lighting = false; //라이트를꺼야색이제데로나온다.
						//m.ZBuffer = false;
						m.Wireframe = true;
						pVideo->setMaterial(m);
					}

					pVideo->drawMeshBuffer(pmb);
				}

				pVideo->endScene();		
			}

			pDevice->drop();
		}
	}

	//에니메이션 체인지
	//다른 오브잭트의 에니메이션 사용하기 ,useAnimationFrom
	//메쉬 체인지로 응용가능	
	namespace _04
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9
				);

			pDevice->setWindowCaption(L"hello irrlicht 3d engine");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res/jga/anichg");


			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,8,-450), irr::core::vector3df(0,5,0));

			//{		
			//	irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(
			//		pSmgr->getMesh("walkers/ninja/ninja.b3d") 
			//		);
			//	pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);				
			//	pNode->setFrameLoop(206,250);//대기동작
			//	pNode->setAnimationSpeed(30);
			//	pNode->setMaterialTexture(0,pVideo->getTexture("walkers/ninja/nskingr.jpg"));	
			//	
			//	pNode->setName("usr/scene/b3d/ninja/1");	
			//}

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);


			while(pDevice->run())
			{		
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d \n ",									 
									 pVideo->getFPS(),
									 pVideo->getPrimitiveCountDrawn()							 
									 );
					pstextFPS->setText(wszbuf);
				}

				static int anisel=1;
				if(::GetAsyncKeyState('1') & 0x8000)
				{
					anisel = 1;
				}
				if(::GetAsyncKeyState('2') & 0x8000)
				{
					anisel = 2;
				}
				if(::GetAsyncKeyState('3') & 0x8000)
				{
					anisel = 3;
				}

				static irr::f32 fFrame = 0;						

				fFrame += fDelta * 30;
				

				//printf("%f \n",fFrame);
				
				
				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));					

				pSmgr->drawAll();
				pGuiEnv->drawAll();	

				{	

					irr::scene::ISkinnedMesh *pSkinnedMesh = (irr::scene::ISkinnedMesh *)pSmgr->getMesh("testfish.x");		
					irr::scene::ISkinnedMesh *pSkinnedMesh_ani;

					switch(anisel)
					{
					case 1:					
						pSkinnedMesh_ani = (irr::scene::ISkinnedMesh *)pSmgr->getMesh("testfish_swim_ani.x");						
						break;
					case 2:
						pSkinnedMesh_ani = (irr::scene::ISkinnedMesh *)pSmgr->getMesh("testfish_bite_ani.x");
						break;
					case 3:
						pSkinnedMesh_ani = (irr::scene::ISkinnedMesh *)pSmgr->getMesh("testfish_cap_ani.x");
						break;
					}

					//애니메이션 체인지
					pSkinnedMesh->useAnimationFrom(pSkinnedMesh_ani);

					if( pSkinnedMesh_ani->getFrameCount() < (irr::u32)fFrame)
					{
						fFrame = 0.f;
					}

					
					//라스트프래임 재설정					
					pSkinnedMesh->animateMesh(fFrame,1);
					pSkinnedMesh->skinMesh();
					
					irr::scene::IMeshBuffer *pmb = pSkinnedMesh->getMeshBuffer(0);

					{//월드변환
						pVideo->setTransform(irr::video::ETS_WORLD,
							irr::core::IdentityMatrix); //변환초기화					
					}

					{
						irr::video::SMaterial m;
						m.Lighting = false; //라이트를꺼야색이제데로나온다.
						//m.ZBuffer = false;
						m.Wireframe = true;
						pVideo->setMaterial(m);
					}

					pVideo->drawMeshBuffer(pmb);


					////두번째 그리기
					////애니메이션 체인지
					//{
					//	pSkinnedMesh->useAnimationFrom((irr::scene::ISkinnedMesh *)pSmgr->getMesh("testfish_cap_ani.x"));						

					//	if( pSkinnedMesh_ani->getFrameCount() < (irr::u32)fFrame2)
					//	{
					//		fFrame2 = 0.f;
					//	}
					//	
					//	//라스트프래임 재설정
					//	//pSkinnedMesh->animateMesh(fLastFrame2,1);
					//	//pSkinnedMesh->skinMesh();

					//	//pSkinnedMesh->animateMesh(fFrame2,1);
					//	//pSkinnedMesh->skinMesh();

					//	//pSkinnedMesh->getMesh(fLastFrame2);
					//	pSkinnedMesh->getMesh(fFrame2);

					//	fLastFrame2 = fFrame2;

					//	irr::scene::IMeshBuffer *pmb = pSkinnedMesh->getMeshBuffer(0);					


					//	{//월드변환
					//		irr::core::matrix4 mat;
					//		mat.setTranslation(irr::core::vector3df(0,0,100));
					//		pVideo->setTransform(irr::video::ETS_WORLD,
					//			mat
					//			); //변환초기화					
					//	}

					//	{
					//		irr::video::SMaterial m;
					//		m.Lighting = false; //라이트를꺼야색이제데로나온다.
					//		//m.ZBuffer = false;
					//		m.Wireframe = true;
					//		pVideo->setMaterial(m);
					//	}

					//	pVideo->drawMeshBuffer(pmb);
					//}
				}

				pVideo->endScene();		
			}

			pDevice->drop();
		}
	}


	//IK 직접구현 예제
	namespace _05 {


		const irr::f32 EPSILON = 0.001f;

		/**
		* Lengths of the two links
		*/
		float m_l1, m_l2;

		/**
		* Angles of the two joings
		*/
		float m_theta1, m_theta2;

		/**
		* useful here and there, allows the user to 'snap' a variable to lie
		* within the bounds of two inputted vars
		*/
		template <class T> void	 Snap( T &a, T min, T max )
		{
			if( a < min ) 
				a = min;
			if( a > max ) 
				a = max;
		}		


		void FindJointAngles( float x, float y )
		{
			float minD = (float)fabs(m_l1 - m_l2 );
			float maxD = m_l1 + m_l2;

			float L1 = m_l1;
			float L2 = m_l2;

			/**
			* Find the standard theta and distance
			*/
			float dist = (float)sqrt(x*x+y*y);
			float theta = (float)atan2(y,x);

			/**
			* Snap the distance to values we can reach
			*/
			Snap( dist, minD + EPSILON, maxD - EPSILON );

			/**
			* Adjust the x and y to match the new distance
			*/
			x = (float)cos(theta)*dist;
			y = (float)sin(theta)*dist;

			/**
			* Find thetaHat using the law of cosines
			*/

			float thetaHat = (float)acos( ( L2*L2 - L1*L1 - dist*dist ) / (-2*dist*L1) );

			/**
			* theta - thetaHat is theta 1
			*/
			m_theta1 = theta - thetaHat;

			/**
			* Use the law of cosines to get thetaArm
			*/
			float thetaArm = (float)acos( ( dist*dist - L1*L1 - L2*L2 ) / (-2*L2*L1) );

			/**
			* With thetaArm we can easily find theta2
			*/
			m_theta2 = irr::core::PI - thetaArm;

		}
		void main()
		{

			m_l1 = 10.0;
			m_l2 = 8.0;			
			m_theta1 = 0.f;
			m_theta2 = 0.f;

			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9
				);

			pDevice->setWindowCaption(L"IK 직접구현 예제");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,0,-20), irr::core::vector3df(0,0,0));


			{
				irr::scene::ISceneNode *pNode = pSmgr->addCubeSceneNode(2);
				pNode->setName("usr/scene/cube/1");

				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);
				pNode->setPosition(irr::core::vector3df(5,0,0));
			}			


			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,120),true,true,0,100,true);



			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d\
									 \n%f \
									 \n%f \
									 ",
									 pVideo->getFPS(),pVideo->getPrimitiveCountDrawn(),
									 m_theta1,
									 m_theta2
									 );
					pstextFPS->setText(wszbuf);
				}




				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();	

				{
					irr::scene::ISceneNode *pNode;
					pNode = pSmgr->getSceneNodeFromName("usr/scene/cube/1");

					if(::GetAsyncKeyState('I') && 0x8000)
					{
						pNode->setPosition(pNode->getAbsolutePosition() +
							irr::core::vector3df(0,5,0) * fDelta);

					}
					if(::GetAsyncKeyState('J') && 0x8000)
					{
						pNode->setPosition(pNode->getAbsolutePosition() +
							irr::core::vector3df(-5,0,0)* fDelta);
					}
					if(::GetAsyncKeyState('K') && 0x8000)
					{
						pNode->setPosition(pNode->getAbsolutePosition() +
							irr::core::vector3df(5,0,0)* fDelta);
					}
					if(::GetAsyncKeyState('M') && 0x8000)
					{
						pNode->setPosition(pNode->getAbsolutePosition() +
							irr::core::vector3df(0,-5,0)* fDelta);
					}				

					FindJointAngles(pNode->getAbsolutePosition().X,pNode->getAbsolutePosition().Y);
				}



				{//월드변환
					pVideo->setTransform(irr::video::ETS_WORLD,
						irr::core::IdentityMatrix); //변환초기화					
				}

				{
					irr::video::SMaterial m;
					m.Lighting = false; //라이트를꺼야색이제데로나온다.
					m.ZBuffer = false;
					pVideo->setMaterial(m);
				}

				irr::core::vector3df v(1,0,0);
				irr::core::vector3df v1;
				irr::core::vector3df v2;

				//첫번째팔 그리기
				v1 = v;
				v1.rotateXYBy(m_theta1*irr::core::RADTODEG);
				v1 *= m_l1;
				pVideo->draw3DLine(irr::core::vector3df(0,0,0),v1,irr::video::SColor(255,255,0,0));

				//두번째팔 그리기
				v2 = v1;
				v2.normalize();
				v2.rotateXYBy(m_theta2*irr::core::RADTODEG);
				v2 *= m_l2;				
				pVideo->draw3DLine(v1,v1+v2,irr::video::SColor(255,0,255,0));

				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}	

	//본제어
	//조인트 수동으로 추가하기예제
	namespace _06
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9
				);

			pDevice->setWindowCaption(L"hello irrlicht 3d engine");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res/");

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,30,-50), irr::core::vector3df(0,30,0));

			{				
				pSmgr->addArrowMesh("usr/mesh/arrow"); //메쉬등록	
				pSmgr->addArrowMesh("usr/mesh/arrow/red",
					irr::video::SColor(255,255,0,0),
					irr::video::SColor(255,255,0,0),
					4,8,
					10,6,.5f,3
					); //메쉬등록	
				pSmgr->addArrowMesh("usr/mesh/arrow/green",
					irr::video::SColor(255,0,255,0),
					irr::video::SColor(255,0,255,0),
					4,8,
					10,6,.5f,3
					); //메쉬등록	
			}			

			irr::scene::ISkinnedMesh *pSkinnedMesh = (irr::scene::ISkinnedMesh *)pSmgr->getMesh("jga/skintest2.b3d");	

			irr::scene::ISkinnedMesh::SJoint *joint = pSkinnedMesh->getAllJoints()[pSkinnedMesh->getJointNumber("Bone01")];

			{
				irr::u32 i;
				for(i=0;i<joint->Weights.size();i++)
				{
					std::cout << "Bone01-----------weight-------- : " << i << std::endl;
					std::cout << "buffer_id : " << joint->Weights[i].buffer_id << std::endl;
					std::cout << "vertex_id : " << joint->Weights[i].vertex_id << std::endl;
					std::cout << "strength : " << joint->Weights[i].strength << std::endl;

					//joint->Weights[i].strength = 0.5;
				}
			}	

			joint = pSkinnedMesh->getAllJoints()[pSkinnedMesh->getJointNumber("Bone02")];

			{
				irr::u32 i;
				for(i=0;i<joint->Weights.size();i++)
				{
					std::cout << "Bone02-----------weight-------- : " << i << std::endl;
					std::cout << "buffer_id : " << joint->Weights[i].buffer_id << std::endl;
					std::cout << "vertex_id : " << joint->Weights[i].vertex_id << std::endl;
					std::cout << "strength : " << joint->Weights[i].strength << std::endl;

					//joint->Weights[i].strength = 0.5;
				}
			}	

			//조인트추가하기
			{
				irr::scene::ISkinnedMesh::SJoint *Injoint = pSkinnedMesh->getAllJoints()[pSkinnedMesh->getJointNumber("Bone01")];

				irr::scene::ISkinnedMesh::SJoint *joint = pSkinnedMesh->addJoint(Injoint);

				joint->Name = "Bone03";
				joint->Animatedposition = irr::core::vector3df(10,10,0);

				irr::core::matrix4 posMat;
				posMat.setTranslation(joint->Animatedposition);

				joint->LocalAnimatedMatrix = posMat;
				joint->GlobalAnimatedMatrix = Injoint->GlobalAnimatedMatrix * joint->LocalAnimatedMatrix;

				//애니메이션키 추가해주기
				irr::scene::ISkinnedMesh::SPositionKey *key = pSkinnedMesh->addPositionKey(joint);
				key->frame = 0;
				key->position = irr::core::vector3df(0,0,0);

				key = pSkinnedMesh->addPositionKey(joint);
				key->frame = 50;
				key->position = irr::core::vector3df(15,5,0);			

				key = pSkinnedMesh->addPositionKey(joint);
				key->frame = 100;
				key->position = irr::core::vector3df(0,0,0);			

				pSkinnedMesh->useAnimationFrom(pSkinnedMesh);				
			}			

			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{		
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;



				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));					

				pSmgr->drawAll();
				pGuiEnv->drawAll();	
				{	
					static irr::f32 fFrame= 0;
					if(::GetAsyncKeyState('1') & 0x8000)
					{
						fFrame += 30*fDelta;
					}
					if(::GetAsyncKeyState('2') & 0x8000)
					{
						fFrame -= 30*fDelta;
					}

					if(fFrame > pSkinnedMesh->getFrameCount())
					{
						fFrame = 0;
					}					

					pSkinnedMesh->animateMesh(fFrame,1);//프레임지정 블랜딩값이 1 완전히 새로운 프레임으로
					pSkinnedMesh->skinMesh(); //스키닝적용

					irr::scene::IMeshBuffer *pmb = pSkinnedMesh->getMeshBuffer(0);

					{//월드변환
						pVideo->setTransform(irr::video::ETS_WORLD,
							irr::core::IdentityMatrix); //변환초기화					
					}

					{
						irr::video::SMaterial m;
						m.Lighting = false; //라이트를꺼야색이제데로나온다.
						m.ZBuffer = false;
						m.Wireframe = true;
						pVideo->setMaterial(m);
					}

					pVideo->drawMeshBuffer(pmb);

					irr::core::array<irr::scene::ISkinnedMesh::SJoint *> joints = pSkinnedMesh->getAllJoints();

					irr::u32 i;
					for(i=0;i< joints.size();i++)
					{
						if(joints[i]->Name == "Bone01" || joints[i]->Name == "Bone02")
						{
							{//월드변환
								pVideo->setTransform(irr::video::ETS_WORLD,
									joints[i]->GlobalAnimatedMatrix); //변환초기화
							}
							irr::scene::IMesh *pMesh = pSmgr->getMesh("usr/mesh/arrow/red")->getMesh(0);
							irr::u32 j;
							for(j=0;j< pMesh->getMeshBufferCount();j++)
								pVideo->drawMeshBuffer(pMesh->getMeshBuffer(j));
						}
						if(joints[i]->Name == "Bone03")
						{
							{//월드변환

								irr::core::matrix4 mat = joints[i]->GlobalAnimatedMatrix;
								pVideo->setTransform(irr::video::ETS_WORLD,
									joints[i]->GlobalAnimatedMatrix); //변환초기화

							}
							irr::scene::IMesh *pMesh = pSmgr->getMesh("usr/mesh/arrow/green")->getMesh(0);
							irr::u32 j;
							for(j=0;j< pMesh->getMeshBufferCount();j++)
								pVideo->drawMeshBuffer(pMesh->getMeshBuffer(j));
						}
					}
				}
				pVideo->endScene();		
			}
			pDevice->drop();
		}
	}


	//메쉬 체인지 1 .직접 구현 예제
	namespace _07
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9
				);

			pDevice->setWindowCaption(L"hello irrlicht 3d engine");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res/");

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,30,-50), irr::core::vector3df(0,30,0));

			{				
				pSmgr->addArrowMesh("usr/mesh/arrow"); //메쉬등록	
				pSmgr->addArrowMesh("usr/mesh/arrow/red",
					irr::video::SColor(255,255,0,0),
					irr::video::SColor(255,255,0,0),
					4,8,
					10,6,.5f,3
					); //메쉬등록	
				pSmgr->addArrowMesh("usr/mesh/arrow/green",
					irr::video::SColor(255,0,255,0),
					irr::video::SColor(255,0,255,0),
					4,8,
					10,6,.5f,3
					); //메쉬등록	
			}

			irr::scene::ISkinnedMesh *psm_ani = (irr::scene::ISkinnedMesh *)pSmgr->getMesh("jga/mc_ani.b3d");
			irr::scene::ISkinnedMesh *psm_cylinder = (irr::scene::ISkinnedMesh *)pSmgr->getMesh("jga/mc_cy.b3d");
			irr::scene::ISkinnedMesh *psm_box = (irr::scene::ISkinnedMesh *)pSmgr->getMesh("jga/mc_bx.b3d");

			psm_cylinder->useAnimationFrom(psm_ani);
			psm_box->useAnimationFrom(psm_ani);

			irr::scene::ISkinnedMesh *pSkinnedMesh = psm_ani;			

			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{		
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;



				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));					

				pSmgr->drawAll();
				pGuiEnv->drawAll();	
				{	
					static irr::f32 fFrame= 0;
					if(::GetAsyncKeyState('1') & 0x8000)
					{
						pSkinnedMesh = psm_cylinder;

					}
					if(::GetAsyncKeyState('2') & 0x8000)
					{
						pSkinnedMesh = psm_box;						
					}

					fFrame += 30*fDelta;

					if(fFrame > 50)
					{
						fFrame = 0;
					}	

					/*{
					irr::scene::SSkinMeshBuffer *pmb = (irr::scene::SSkinMeshBuffer *)pSkinnedMesh->getMeshBuffer(0);

					irr::u32 i;
					for(i=0;i<pmb->getVertexCount();i++)
					{
					std::cout << pmb->Vertices_Standard[i].Pos.X << 
					"," << 
					pmb->Vertices_Standard[i].Pos.Y << 
					"," <<
					pmb->Vertices_Standard[i].Pos.Z << std::endl;
					}


					}*/

					pSkinnedMesh->animateMesh(fFrame,1);//프레임지정 블랜딩값이 1 완전히 새로운 프레임으로
					pSkinnedMesh->skinMesh(); //스키닝적용



					/*{
					irr::scene::SSkinMeshBuffer *pmb = (irr::scene::SSkinMeshBuffer *)pSkinnedMesh->getMeshBuffer(0);

					irr::u32 i;
					for(i=0;i<pmb->getVertexCount();i++)
					{
					std::cout << pmb->Vertices_Standard[i].Pos.X << 
					"," << 
					pmb->Vertices_Standard[i].Pos.Y << 
					"," <<
					pmb->Vertices_Standard[i].Pos.Z << std::endl;
					}


					}*/

					irr::scene::IMeshBuffer *pmb = pSkinnedMesh->getMeshBuffer(0);

					{//월드변환
						pVideo->setTransform(irr::video::ETS_WORLD,
							irr::core::IdentityMatrix); //변환초기화					
					}

					{
						irr::video::SMaterial m;
						m.Lighting = false; //라이트를꺼야색이제데로나온다.
						m.ZBuffer = false;
						m.Wireframe = true;
						pVideo->setMaterial(m);
					}

					pVideo->drawMeshBuffer(pmb);


					irr::core::array<irr::scene::ISkinnedMesh::SJoint *> joints = pSkinnedMesh->getAllJoints();

					irr::u32 i;
					for(i=0;i< joints.size();i++)
					{
						if(joints[i]->Name == "Bone01" || joints[i]->Name == "Bone02")
						{
							{//월드변환
								pVideo->setTransform(irr::video::ETS_WORLD,
									joints[i]->GlobalAnimatedMatrix); //변환초기화
							}
							irr::scene::IMesh *pMesh = pSmgr->getMesh("usr/mesh/arrow/red")->getMesh(0);
							irr::u32 j;
							for(j=0;j< pMesh->getMeshBufferCount();j++)
								pVideo->drawMeshBuffer(pMesh->getMeshBuffer(j));
						}
						if(joints[i]->Name == "Bone03")
						{
							{//월드변환

								irr::core::matrix4 mat = joints[i]->GlobalAnimatedMatrix;
								pVideo->setTransform(irr::video::ETS_WORLD,
									joints[i]->GlobalAnimatedMatrix); //변환초기화

							}
							irr::scene::IMesh *pMesh = pSmgr->getMesh("usr/mesh/arrow/green")->getMesh(0);
							irr::u32 j;
							for(j=0;j< pMesh->getMeshBufferCount();j++)
								pVideo->drawMeshBuffer(pMesh->getMeshBuffer(j));
						}
					}

				}
				pVideo->endScene();		
			}

			//pSkinnedMesh->drop();
			pDevice->drop();
		}
	}

	//메쉬 체인지 2 . 씬노드 상에서 구현 예제
	//맥스 에서 작업을할때 본작업을 하고 여러개의 메쉬를 동시에 그린다음
	//먼저 본과 애니메이션 정보만 빼내기위해서 메쉬를 모두 지운다음 b3d로 익스포트하고 mc_ani.b3d"
	//박스 메쉬만 남겨놓고 애니메이션정보는 언체킹한후 익스포트한다. mc_bx.b3d
	//실린더 메쉬만 남겨놓고 애니메이션정보는 언체킹한후 익스포트한다. mc_cy.b3d
	//본과 메쉬만 체인지하면 애니메이션은 그대로 이고 메쉬만 바뀌게된다.

	namespace _08
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9
				);

			pDevice->setWindowCaption(L"hello irrlicht 3d engine");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res/");

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(20,60,-50), irr::core::vector3df(0,30,0));

			{				
				pSmgr->addArrowMesh("usr/mesh/arrow"); //메쉬등록	
				pSmgr->addArrowMesh("usr/mesh/arrow/red",
					irr::video::SColor(255,255,0,0),
					irr::video::SColor(255,255,0,0),
					4,8,
					10,6,.5f,3
					); //메쉬등록	
				pSmgr->addArrowMesh("usr/mesh/arrow/green",
					irr::video::SColor(255,0,255,0),
					irr::video::SColor(255,0,255,0),
					4,8,
					10,6,.5f,3
					); //메쉬등록	
			}

			/*
			psm_ani 는 메쉬는 없고 본과 애니메이션만 존재한다.
			실린더와 박스는 메쉬와 본만 존재하고 애니메이션은 존재하지않는다.
			useAnimationFrom함수를 이용해서 애니메이션을 지정해준다.
			*/
			{
				irr::scene::ISkinnedMesh *psm_ani = (irr::scene::ISkinnedMesh *)pSmgr->getMesh("jga/mc_ani.b3d");
				irr::scene::ISkinnedMesh *psm_cylinder = (irr::scene::ISkinnedMesh *)pSmgr->getMesh("jga/mc_cy.b3d");
				irr::scene::ISkinnedMesh *psm_box = (irr::scene::ISkinnedMesh *)pSmgr->getMesh("jga/mc_bx.b3d");

				//애니메이션 지정
				psm_cylinder->useAnimationFrom(psm_ani);
				psm_box->useAnimationFrom(psm_ani);
			}


			{
				irr::scene::IAnimatedMeshSceneNode *pAniNode = pSmgr->addAnimatedMeshSceneNode(pSmgr->getMesh("jga/mc_ani.b3d"));
				pAniNode->setName("usr/scene/skmnode/1");
				pAniNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pAniNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);
				pAniNode->setAnimationSpeed(30.f);
				pAniNode->setFrameLoop(0,50);
			}

			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{		
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;		

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d",
						pVideo->getFPS(),pVideo->getPrimitiveCountDrawn()									 
						);
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));					

				pSmgr->drawAll();
				pGuiEnv->drawAll();	
				{						
					if(::GetAsyncKeyState('1') & 0x8000)
					{
						irr::scene::IAnimatedMeshSceneNode *pAniNode =
							(irr::scene::IAnimatedMeshSceneNode *)pSmgr->getSceneNodeFromName("usr/scene/skmnode/1");
						irr::f32 frame = pAniNode->getFrameNr();
						pAniNode->setMesh(pSmgr->getMesh("jga/mc_bx.b3d"));
						pAniNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
						pAniNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);
						pAniNode->setFrameLoop(0,50);
						pAniNode->setCurrentFrame(frame);						

					}
					if(::GetAsyncKeyState('2') & 0x8000)
					{
						irr::scene::IAnimatedMeshSceneNode *pAniNode =
							(irr::scene::IAnimatedMeshSceneNode *)pSmgr->getSceneNodeFromName("usr/scene/skmnode/1");
						irr::f32 frame = pAniNode->getFrameNr();
						pAniNode->setMesh(pSmgr->getMesh("jga/mc_cy.b3d"));
						pAniNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
						pAniNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);
						pAniNode->setFrameLoop(0,50);
						pAniNode->setCurrentFrame(frame);										

					}

				}
				//본그리기
				{
					{
						irr::video::SMaterial m;
						m.Lighting = false; //라이트를꺼야색이제데로나온다.
						m.ZBuffer = false;
						m.Wireframe = true;
						pVideo->setMaterial(m);
					}
					irr::scene::IAnimatedMeshSceneNode *pAniNode =
						(irr::scene::IAnimatedMeshSceneNode *)pSmgr->getSceneNodeFromName("usr/scene/skmnode/1");


					{
						irr::scene::IBoneSceneNode *pBone = 
							pAniNode->getJointNode("Bone01");
						pVideo->setTransform(irr::video::ETS_WORLD,
							pBone->getAbsoluteTransformation()); //본의 변환정보로 월드변환적용
						irr::scene::IMesh *pMesh = pSmgr->getMesh("usr/mesh/arrow/red")->getMesh(0);
						irr::u32 j;
						for(j=0;j< pMesh->getMeshBufferCount();j++) //본의위치에 화살표그리기
							pVideo->drawMeshBuffer(pMesh->getMeshBuffer(j)); 
					}	

					{
						irr::scene::IBoneSceneNode *pBone = 
							pAniNode->getJointNode("Bone02");
						pVideo->setTransform(irr::video::ETS_WORLD,
							pBone->getAbsoluteTransformation());
						irr::scene::IMesh *pMesh = pSmgr->getMesh("usr/mesh/arrow/red")->getMesh(0);
						irr::u32 j;
						for(j=0;j< pMesh->getMeshBufferCount();j++)
							pVideo->drawMeshBuffer(pMesh->getMeshBuffer(j));
					}	

					{
						irr::scene::IBoneSceneNode *pBone = 
							pAniNode->getJointNode("Bone03");
						pVideo->setTransform(irr::video::ETS_WORLD,
							pBone->getAbsoluteTransformation());
						irr::scene::IMesh *pMesh = pSmgr->getMesh("usr/mesh/arrow/green")->getMesh(0);
						irr::u32 j;
						for(j=0;j< pMesh->getMeshBufferCount();j++)
							pVideo->drawMeshBuffer(pMesh->getMeshBuffer(j));
					}	

				}
				pVideo->endScene();		
			}			
			pDevice->drop();
		}
	}
	
	//조인트에 무기 또는 아이템달기 예제
	namespace _09
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(	
				irr::video::EDT_DIRECT3D9
				);

			pDevice->setWindowCaption(L"Custom Joint");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res/");

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,5,20), irr::core::vector3df(0,5,0));	


			{//조인트제어 애니관련 예제임

				irr::scene::ISkinnedMesh* pSkinMesh = (irr::scene::ISkinnedMesh*)pSmgr->getMesh( "walkers/dwarf/dwarf2.b3d" );		

				irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(pSkinMesh);
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING, false);
				
				pNode->setMaterialTexture(0,pVideo->getTexture("walkers/dwarf/dwarf2.jpg"));	

				//도끼 메트리얼 지정				
				pNode->getMaterial(3).setTexture(0,pVideo->getTexture("walkers/dwarf/axe.jpg")); 
				pNode->getMaterial(3).Wireframe = true;

				pNode->setAnimationSpeed(15.f);
				pNode->setFrameLoop(2,14);// 걷기				
				pNode->setPosition(irr::core::vector3df(5,0,0));
				pNode->setScale(irr::core::vector3df(.1f,0.1f,0.1f));

				pNode->setName("usr/scene/skmesh/testman");
				//pNode->getJointMode(irr::scene::EJUOR_CONTROL);				

				//조인트 전부출력하기
				{
					irr::scene::IAnimatedMeshSceneNode *pNode;
					pNode = (irr::scene::IAnimatedMeshSceneNode *)pSmgr->getSceneNodeFromName("usr/scene/skmesh/testman");	
					//조인트 구하기
					irr::core::array<irr::scene::ISkinnedMesh::SJoint *> AllJoint = 				
						((irr::scene::ISkinnedMesh*)pNode->getMesh()->getMesh(0))->getAllJoints();	

					irr::u32 i;
					for(i=0;i<AllJoint.size();i++)
					{
						printf(" %s \n",AllJoint[i]->Name.c_str());
					}
				}

				pSmgr->addArrowMesh("usr/mesh/arrow"); //메쉬등록	

				//조인트에 물체달기
				{
					irr::scene::IBoneSceneNode *pBone = pNode->getJointNode("weapon"); //무기 조인트얻기
					irr::scene::IAnimatedMeshSceneNode *pArrowNode = 
						pSmgr->addAnimatedMeshSceneNode(pSmgr->getMesh("usr/mesh/arrow"),pBone);
					pArrowNode->setName("usr/scene/node/arrow");
					pArrowNode->setScale(irr::core::vector3df(15,15,15));
					pArrowNode->setRotation(irr::core::vector3df(90,0,0));
					pArrowNode->setMaterialFlag(irr::video::EMF_LIGHTING, false);
					pArrowNode->setMaterialFlag(irr::video::EMF_WIREFRAME, true);
				}	
			}		

			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;		

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d",
						pVideo->getFPS(),pVideo->getPrimitiveCountDrawn()									 
						);
					pstextFPS->setText(wszbuf);
				}

				if((::GetAsyncKeyState('1') & 0x8001) == 0x8001)
				{
					//캐릭터 노드얻기
					irr::scene::IAnimatedMeshSceneNode *pNode = 
						(irr::scene::IAnimatedMeshSceneNode *)pSmgr->getSceneNodeFromName("usr/scene/skmesh/testman");	

					//무기 조인트얻기
					irr::scene::IBoneSceneNode *pBone = pNode->getJointNode("weapon"); 
					//화살표노드얻기
					irr::scene::IAnimatedMeshSceneNode *pArrowNode = 
						(irr::scene::IAnimatedMeshSceneNode *)pSmgr->getSceneNodeFromName("usr/scene/node/arrow");	
					pArrowNode->setParent(pSmgr->getRootSceneNode());						
				}
				if((::GetAsyncKeyState('2') & 0x8001) == 0x8001)
				{
					//캐릭터 노드얻기
					irr::scene::IAnimatedMeshSceneNode *pNode = 
						(irr::scene::IAnimatedMeshSceneNode *)pSmgr->getSceneNodeFromName("usr/scene/skmesh/testman");	

					//무기 조인트얻기
					irr::scene::IBoneSceneNode *pBone = pNode->getJointNode("weapon"); 
					//화살표노드얻기
					irr::scene::IAnimatedMeshSceneNode *pArrowNode = 
						(irr::scene::IAnimatedMeshSceneNode *)pSmgr->getSceneNodeFromName("usr/scene/node/arrow");	
					pArrowNode->setParent(pBone);	
				}


				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));
				
				pSmgr->drawAll();
				pGuiEnv->drawAll();

				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}

	
	//조인트 제어 기초예제 예제
	//스키닝공간 선택, 로컬이면 상위노드의 변환을 이어받아서 변환적용, 글로벌이면 상위노드변환을 무시하고 스키닝적용(3,4번키로 선택)
	//노드변환무시 할것인지 선택(5,6번키로 선택
	namespace _10
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(	
				irr::video::EDT_DIRECT3D9
				);

			pDevice->setWindowCaption(L"Custom Joint");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res/");

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,5,10), irr::core::vector3df(0,5,0));	


			{//조인트제어 애니관련예제임

				irr::scene::ISkinnedMesh* pSkinMesh = (irr::scene::ISkinnedMesh*)pSmgr->getMesh( "walkers/dwarf/dwarf2.b3d" );		

				irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(pSkinMesh);
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING, false);

				
				pNode->setMaterialTexture(0,pVideo->getTexture("walkers/dwarf/dwarf2.jpg"));				
				pNode->getMaterial(3).setTexture(0,pVideo->getTexture("walkers/dwarf/axe.jpg")); //도끼 메트리얼 지정
				

				pNode->setAnimationSpeed(15.f);				
				pNode->setFrameLoop(292,325); //idle
				pNode->setPosition(irr::core::vector3df(5,0,0));
				pNode->setScale(irr::core::vector3df(.1f,0.1f,0.1f));

				pNode->setName("usr/scene/skmesh/testman");
				pNode->setJointMode(irr::scene::EJUOR_CONTROL);
				//pNode->setRenderFromIdentity(true);			
			}

			irr::core::vector3df headrot;
			
			while(pDevice->run())
			{					

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				irr::core::vector3df vHead,vTarget;
				{
					irr::scene::IAnimatedMeshSceneNode *pNode;
					pNode = (irr::scene::IAnimatedMeshSceneNode *)pSmgr->getSceneNodeFromName("usr/scene/skmesh/testman");					

					pNode->animateJoints();

					irr::scene::IBoneSceneNode *pBone = pNode->getJointNode("head"); //머리 바이페드얻기					


					if((::GetAsyncKeyState('1') & 0x8001) == 0x8001)
					{
						headrot += irr::core::vector3df(0,3.0f,0);
						
					}
					if((::GetAsyncKeyState('2') & 0x8001) == 0x8001)
					{
						headrot += irr::core::vector3df(0,-3.0f,0);
						
					}

					//스키닝공간지정
					if((::GetAsyncKeyState('3') & 0x8001) == 0x8001)
					{
						std::cout << "setSkinningSpace(irr::scene::EBSS_LOCAL)" << std::endl;
						pBone->setSkinningSpace(irr::scene::EBSS_LOCAL);
					}
					if((::GetAsyncKeyState('4') & 0x8001) == 0x8001)
					{
						std::cout << "setSkinningSpace(irr::scene::EBSS_GLOBAL)" << std::endl;
						pBone->setSkinningSpace(irr::scene::EBSS_GLOBAL);
					}

					//노드의 변환 무시할지 안할지결정
					if((::GetAsyncKeyState('5') & 0x8001) == 0x8001)
					{
						std::cout << "setRenderFromIdentity(true)" << std::endl;
						pNode->setRenderFromIdentity(true);			
						
					}
					if((::GetAsyncKeyState('6') & 0x8001) == 0x8001)
					{
						std::cout << "setRenderFromIdentity(false)" << std::endl;
						pNode->setRenderFromIdentity(false);			
						
					}

					//회전적용
					pBone->setRotation(headrot);
					

				}	

				pSmgr->drawAll();
				pGuiEnv->drawAll();

				{

					irr::core::matrix4 mat;//단위행렬로초기화
					pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

					irr::video::SMaterial m;
					m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.
					m.ZBuffer = false;

					pVideo->setMaterial(m);

					pVideo->draw3DLine(vHead,vTarget,irr::video::SColor(0,0,255,0));						
				}



				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}


	
	//조인트 제어 응용 예제(해바라기 버전)	
	//원하는 곳 동적으로 바라보기 예
	namespace _11
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(	
				irr::video::EDT_DIRECT3D9
				);

			pDevice->setWindowCaption(L"Custom Joint");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res/");

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,5,10), irr::core::vector3df(0,5,0));	


			{//조인트제어 애니관련예제임

				irr::scene::ISkinnedMesh* pSkinMesh = (irr::scene::ISkinnedMesh*)pSmgr->getMesh( "walkers/dwarf/dwarf2.b3d" );		

				irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(pSkinMesh);
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING, false);

				
				pNode->setMaterialTexture(0,pVideo->getTexture("walkers/dwarf/dwarf2.jpg"));				
				pNode->getMaterial(3).setTexture(0,pVideo->getTexture("walkers/dwarf/axe.jpg")); //도끼 메트리얼 지정
				

				pNode->setAnimationSpeed(15.f);
				pNode->setFrameLoop(2,14);// 걷기
				//pNode->setFrameLoop(292,325); //idle
				pNode->setPosition(irr::core::vector3df(5,0,0));
				pNode->setScale(irr::core::vector3df(.1f,0.1f,0.1f));

				pNode->setName("usr/scene/skmesh/testman");
				pNode->setJointMode(irr::scene::EJUOR_CONTROL);
				//pNode->setRenderFromIdentity(true);

				{
					irr::scene::IAnimatedMeshSceneNode *pNode;
					pNode = (irr::scene::IAnimatedMeshSceneNode *)pSmgr->getSceneNodeFromName("usr/scene/skmesh/testman");	
					//조인트 구하기
					irr::core::array<irr::scene::ISkinnedMesh::SJoint *> AllJoint = 				
						((irr::scene::ISkinnedMesh*)pNode->getMesh()->getMesh(0))->getAllJoints();	

					irr::u32 i;
					for(i=0;i<AllJoint.size();i++)
					{
						printf(" %s \n",AllJoint[i]->Name.c_str());
					}

				}
			}

			{
				irr::scene::ISceneNode *pNode = pSmgr->addBillboardSceneNode(0,
					irr::core::dimension2df(3,3));
				pNode->setMaterialTexture(0,pVideo->getTexture("particlered.bmp"));
				pNode->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING, false);
				pNode->setName("usr/scene/etc/lookat");
				irr::core::array<irr::core::vector3df> points;
				points.push_back(irr::core::vector3df(3,7,3));
				points.push_back(irr::core::vector3df(3,5,3));
				points.push_back(irr::core::vector3df(-3,7,3));
				points.push_back(irr::core::vector3df(-3,5,3));
				irr::scene::ISceneNodeAnimator *pAnim = pSmgr->createFollowSplineAnimator(pDevice->getTimer()->getTime(),
					points);
				pNode->addAnimator(pAnim);
				pAnim->drop();
			}


			irr::u32 uLastTick = pDevice->getTimer()->getTime();
			while(pDevice->run())
			{	
				irr::u32 uTick = pDevice->getTimer()->getTime();
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f;		

				uLastTick = pDevice->getTimer()->getTime();

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				irr::core::vector3df vHead,vTarget;
				{
					irr::scene::IAnimatedMeshSceneNode *pNode,*pNodeLookat;

					pNode = (irr::scene::IAnimatedMeshSceneNode *)pSmgr->getSceneNodeFromName("usr/scene/skmesh/testman");
					pNodeLookat = (irr::scene::IAnimatedMeshSceneNode *)pSmgr->getSceneNodeFromName("usr/scene/etc/lookat");			

					pNode->animateJoints();

					irr::scene::IBoneSceneNode *pBone = pNode->getJointNode("head"); //머리 바이페드얻기

					vHead = pBone->getAbsolutePosition();//GetWorldPosition(pBone);
					vTarget = pNodeLookat->getAbsolutePosition();

					//바라보는 방향 벡터구하기
					irr::core::vector3df lookat = vTarget - vHead;
					lookat.normalize();

					irr::core::quaternion qt;
					irr::core::vector3df vBaseDir(0,0,1);

					//(0,0,1)기준으로 바라보는 회전 각도구하기
					qt.rotationFromTo(vBaseDir,lookat);
					irr::core::vector3df headrot;
					qt.toEuler(headrot);			
					headrot *= irr::core::RADTODEG;

					//회전적용
					pBone->setRotation(headrot);

				}	

				pSmgr->drawAll();
				pGuiEnv->drawAll();

				{

					irr::core::matrix4 mat;//단위행렬로초기화
					pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

					irr::video::SMaterial m;
					m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.
					m.ZBuffer = false;

					pVideo->setMaterial(m);

					pVideo->draw3DLine(vHead,vTarget,irr::video::SColor(0,0,255,0));						
				}



				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}

	//멀티 애니메이션
	//useAnimationForm을 이용하면 애니메이션의 프레임이 겹치게된다.
	//animateMesh 함수로 애니메이션프레임에따라 본의 위치를 바꿀때
	//이전 프레임과 새로운프레임이 같으면 아무처리도 안해주는데 여기서 문제가 발생한다.
	//이것을 해결하기위해 스키닝 전에 현재프레임을 기준 프레임으로 수정을 해준다음
	//animateMesh를 호출한다.
	namespace _12
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9
				);

			pDevice->setWindowCaption(L"hello irrlicht 3d engine");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res/jga/anichg");


			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,8,-450), irr::core::vector3df(0,5,0));
			
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{		
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d \n ",									 
									 pVideo->getFPS(),
									 pVideo->getPrimitiveCountDrawn()							 
									 );
					pstextFPS->setText(wszbuf);
				}				

				
				
				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));					

				pSmgr->drawAll();
				pGuiEnv->drawAll();	

				{
					irr::scene::ISkinnedMesh *pSkinnedMesh = (irr::scene::ISkinnedMesh *)pSmgr->getMesh("testfish.x");		
					irr::scene::ISkinnedMesh *pSkinnedMesh_ani = (irr::scene::ISkinnedMesh *)pSmgr->getMesh("swim_addframe.x");
					static irr::f32 fFrame = 1.f;	
					fFrame += fDelta * 10;

					//애니메이션 체인지
					pSkinnedMesh->useAnimationFrom(pSkinnedMesh_ani);

					if( (irr::f32)pSkinnedMesh_ani->getFrameCount() < fFrame)
					{
						fFrame = 1.f;
					}
					
					//라스트프래임 재설정	
					pSkinnedMesh->animateMesh(0,1);
					pSkinnedMesh->animateMesh(fFrame,1);
					pSkinnedMesh->skinMesh();
					
					irr::scene::IMeshBuffer *pmb = pSkinnedMesh->getMeshBuffer(0);

					{//월드변환
						pVideo->setTransform(irr::video::ETS_WORLD,
							irr::core::IdentityMatrix); //변환초기화					
					}

					{
						irr::video::SMaterial m;
						m.Lighting = false; //라이트를꺼야색이제데로나온다.
						//m.ZBuffer = false;
						m.Wireframe = true;
						pVideo->setMaterial(m);
					}

					pVideo->drawMeshBuffer(pmb);
				}

				{
					irr::scene::ISkinnedMesh *pSkinnedMesh = (irr::scene::ISkinnedMesh *)pSmgr->getMesh("testfish.x");		
					irr::scene::ISkinnedMesh *pSkinnedMesh_ani = (irr::scene::ISkinnedMesh *)pSmgr->getMesh("bite_addframe.x");		
					static irr::f32 fFrame = 1;	
					fFrame += fDelta * 10;


					/*if((::GetAsyncKeyState(VK_SPACE) & 0x8001) == 0x8001)
					{
						fFrame += 0.5f;
					}					*/

					//애니메이션 체인지
					pSkinnedMesh->useAnimationFrom(pSkinnedMesh_ani);					

					if( (irr::f32)pSkinnedMesh_ani->getFrameCount() < fFrame)
					{
						fFrame = 1.f;
					}

					//std::cout << fFrame << std::endl;
					
					//라스트프래임 재설정				
					pSkinnedMesh->animateMesh(0,1);
					pSkinnedMesh->animateMesh(fFrame,1);
					pSkinnedMesh->skinMesh();
					
					irr::scene::IMeshBuffer *pmb = pSkinnedMesh->getMeshBuffer(0);

					{//월드변환
						irr::core::matrix4 mat;
						mat.setTranslation(irr::core::vector3df(0,150,0));
						pVideo->setTransform(
							irr::video::ETS_WORLD,
							mat
							); //변환초기화					
					}

					{
						irr::video::SMaterial m;
						m.Lighting = false; //라이트를꺼야색이제데로나온다.
						//m.ZBuffer = false;
						m.Wireframe = true;
						pVideo->setMaterial(m);
					}

					pVideo->drawMeshBuffer(pmb);
				}

				pVideo->endScene();		
			}

			pDevice->drop();
		}
	}







}