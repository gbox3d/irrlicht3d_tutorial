﻿
#include <irrlicht.h>
#include <math.h>


#include <iostream>
#include <vector>


#ifdef USE_MS_DX9

#include <d3dx9.h>

#ifndef SAFE_RELEASE
#define SAFE_RELEASE(p)      { if (p) { (p)->Release(); (p)=NULL; } }
#endif

#endif

namespace Lesson10
{
	//일리히트 그리기 전용함수이용해서 일리히트씬노드 유지한체 직접그리기예제 
	//
	namespace _00
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,0,-2), irr::core::vector3df(0,0,0));

			irr::video::S3DVertex Vertices[4];
			Vertices[0] = irr::video::S3DVertex(-.5,-.5,0, 0,0,-1,irr::video::SColor(0,0,255,255),0,1);
			Vertices[1] = irr::video::S3DVertex(-.5,.5,0, 0,0,-1,irr::video::SColor(0,255,0,255),0,0);
			Vertices[2] = irr::video::S3DVertex(.5,.5,0, 0,0,-1,irr::video::SColor(0,255,255,0),1,0);
			Vertices[3] = irr::video::S3DVertex(.5,-.5,0, 0,0,-1,irr::video::SColor(0,0,255,0),1,1);
			irr::u16 indices[] = { 0,1,2,3,0,2 };

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			//택스춰 등록
			irr::video::ITexture *pTex = pVideo->getTexture("jga/samurai.jpg");


			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->setTransform(irr::video::ETS_WORLD, irr::core::IdentityMatrix);

				//메트리얼설정
				{
					irr::video::SMaterial m;
					m.Lighting = false; //라이트를꺼야색이제데로나온다.
					m.ZBuffer = false;
					m.TextureLayer[0].Texture = pTex;

					pVideo->setMaterial(m);
				}

				pVideo->drawIndexedTriangleList(Vertices,4,indices,2);

				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}


#ifdef USE_MS_DX9

	//DX응용 직접그리기예제
	//일리히트 랜더링 파이프라인과 DX 그리기 혼합예제
	namespace _01
	{
		/*const int Width  = 640;
		const int Height = 480;*/

		struct Vertex
		{
			Vertex(){}
			Vertex(float x, float y, float z)
			{
				_x = x;  _y = y;  _z = z;
			}
			float _x, _y, _z;
			static const DWORD FVF;
		};
		const DWORD Vertex::FVF = D3DFVF_XYZ;


		void main()
		{		

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9  
				); 

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,3,-5), irr::core::vector3df(0,0,0));

			//다이랙트 3디 디바이스얻기
			LPDIRECT3DDEVICE9 pD9Device = pDevice->getVideoDriver()->getExposedVideoData().D3D9.D3DDev9;			


			IDirect3DVertexBuffer9* VB = 0;
			IDirect3DIndexBuffer9*  IB = 0;

			//
			// Classes and Structures
			//
			{
				IDirect3DDevice9* Device = pD9Device;
				//
				// Create vertex and index buffers.
				//
				Device->CreateVertexBuffer(
					8 * sizeof(Vertex), 
					D3DUSAGE_WRITEONLY,
					Vertex::FVF,
					D3DPOOL_MANAGED,
					&VB,
					0);

				Device->CreateIndexBuffer(
					36 * sizeof(WORD),
					D3DUSAGE_WRITEONLY,
					D3DFMT_INDEX16,
					D3DPOOL_MANAGED,
					&IB,
					0);

				//
				// Fill the buffers with the cube data.
				//

				// define unique vertices:
				Vertex* vertices;
				VB->Lock(0, 0, (void**)&vertices, 0);

				// vertices of a unit cube
				vertices[0] = Vertex(-1.0f, -1.0f, -1.0f);
				vertices[1] = Vertex(-1.0f,  1.0f, -1.0f);
				vertices[2] = Vertex( 1.0f,  1.0f, -1.0f);
				vertices[3] = Vertex( 1.0f, -1.0f, -1.0f);
				vertices[4] = Vertex(-1.0f, -1.0f,  1.0f);
				vertices[5] = Vertex(-1.0f,  1.0f,  1.0f);
				vertices[6] = Vertex( 1.0f,  1.0f,  1.0f);
				vertices[7] = Vertex( 1.0f, -1.0f,  1.0f);

				VB->Unlock();

				// define the triangles of the cube:
				WORD* indices = 0;
				IB->Lock(0, 0, (void**)&indices, 0);

				// front side
				indices[0]  = 0; indices[1]  = 1; indices[2]  = 2;
				indices[3]  = 0; indices[4]  = 2; indices[5]  = 3;

				// back side
				indices[6]  = 4; indices[7]  = 6; indices[8]  = 5;
				indices[9]  = 4; indices[10] = 7; indices[11] = 6;

				// left side
				indices[12] = 4; indices[13] = 5; indices[14] = 1;
				indices[15] = 4; indices[16] = 1; indices[17] = 0;

				// right side
				indices[18] = 3; indices[19] = 2; indices[20] = 6;
				indices[21] = 3; indices[22] = 6; indices[23] = 7;

				// top
				indices[24] = 1; indices[25] = 5; indices[26] = 6;
				indices[27] = 1; indices[28] = 6; indices[29] = 2;

				// bottom
				indices[30] = 4; indices[31] = 0; indices[32] = 3;
				indices[33] = 4; indices[34] = 3; indices[35] = 7;

				IB->Unlock();

			}

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);


			while(pDevice->run())
			{
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				if(::GetAsyncKeyState(VK_ESCAPE) && 0x8000)
				{
					pDevice->closeDevice();
				}
				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();


				//메트리얼설정
				{
					//노드 그릴때 메트리얼설정을 해주지않으면
					//지유아이출력하면 제데로안됨
					irr::video::SMaterial m;
					m.Lighting = false; //라이트를꺼야색이제데로나온다.
					m.ZBuffer = true;
					//m.TextureLayer[0].Texture = pTex;
					pVideo->setMaterial(m);
				}

				pVideo->setTransform(irr::video::ETS_WORLD, irr::core::IdentityMatrix);				
				{				

					DWORD oldFVF;
					pD9Device->GetFVF(&oldFVF);

					DWORD oldFillMode;
					pD9Device->GetRenderState(D3DRS_FILLMODE,&oldFillMode);
					pD9Device->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);				
					pD9Device->SetRenderState(D3DRS_LIGHTING,FALSE);

					pD9Device->SetStreamSource(0, VB, 0, sizeof(Vertex));
					pD9Device->SetIndices(IB);
					pD9Device->SetFVF(Vertex::FVF);
					// Draw cube.
					pD9Device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 8, 0, 12);

					//랜더링 상태값복귀
					pD9Device->SetFVF(oldFVF);
					pD9Device->SetRenderState(D3DRS_FILLMODE, oldFillMode);
				}

				pGuiEnv->drawAll();

				pVideo->endScene();

				//{
				//	irr::scene::ICameraSceneNode *pCam = pSmgr->getActiveCamera();
				//	pCam->OnRegisterSceneNode();
				//	pCam->render();
				//}

				//pVideo->setTransform(irr::video::ETS_WORLD, irr::core::IdentityMatrix);				


				//pD9Device->Clear(0,0,D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER,0x000000ff,1.0f,0);

				//{
				//	IDirect3DDevice9* Device = pD9Device;
				//	Device->BeginScene();

				//	Device->SetStreamSource(0, VB, 0, sizeof(Vertex));
				//	Device->SetIndices(IB);
				//	Device->SetFVF(Vertex::FVF);

				//	// Draw cube.
				//	Device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 8, 0, 12);

				//	Device->EndScene();
				//}

				//pD9Device->Present(0,0,0,0);
			}

			VB->Release();
			IB->Release();

			pDevice->drop();
		}
	}



	//DX응용 직접그리기예제
	//일리히트 고정랜더링 파이프라인과 DX 그리기 혼합예제
	//큐브에 텍스춰 출력하기 버전
	namespace _02
	{			
		struct Vertex
		{
			Vertex(){}
			Vertex(
				float x, float y, float z,
				float nx, float ny, float nz,
				float u, float v)
			{
				_x  = x;  _y  = y;  _z  = z;
				_nx = nx; _ny = ny; _nz = nz;
				_u  = u;  _v  = v;
			}
			float _x, _y, _z;
			float _nx, _ny, _nz;
			float _u, _v; // texture coordinates
			static const DWORD FVF;
		};
		const DWORD Vertex::FVF = D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1;

		void main()
		{		

			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9  
				); 

			//다이랙트 3디 디바이스얻기
			LPDIRECT3DDEVICE9 pD9Device = pDevice->getVideoDriver()->getExposedVideoData().D3D9.D3DDev9;

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			IDirect3DVertexBuffer9* _vb;
			IDirect3DIndexBuffer9*  _ib;
			//IDirect3DTexture9*      Tex  = 0;

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../res");
			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,3,-5), irr::core::vector3df(0,0,0));

			//dx 관련 초기화
			{
				// save a ptr to the device
				IDirect3DDevice9* _device = pD9Device;

				_device->CreateVertexBuffer(
					24 * sizeof(Vertex), 
					D3DUSAGE_WRITEONLY,
					Vertex::FVF,
					D3DPOOL_MANAGED,
					&_vb,
					0);

				Vertex* v;
				_vb->Lock(0, 0, (void**)&v, 0);

				// build box

				// fill in the front face vertex data
				v[0] = Vertex(-1.0f, -1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f);
				v[1] = Vertex(-1.0f,  1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f);
				v[2] = Vertex( 1.0f,  1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f);
				v[3] = Vertex( 1.0f, -1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f);

				// fill in the back face vertex data
				v[4] = Vertex(-1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f);
				v[5] = Vertex( 1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f);
				v[6] = Vertex( 1.0f,  1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f);
				v[7] = Vertex(-1.0f,  1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f);

				// fill in the top face vertex data
				v[8]  = Vertex(-1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f);
				v[9]  = Vertex(-1.0f, 1.0f,  1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f);
				v[10] = Vertex( 1.0f, 1.0f,  1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f);
				v[11] = Vertex( 1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f);

				// fill in the bottom face vertex data
				v[12] = Vertex(-1.0f, -1.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f);
				v[13] = Vertex( 1.0f, -1.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f);
				v[14] = Vertex( 1.0f, -1.0f,  1.0f, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f);
				v[15] = Vertex(-1.0f, -1.0f,  1.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f);

				// fill in the left face vertex data
				v[16] = Vertex(-1.0f, -1.0f,  1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f);
				v[17] = Vertex(-1.0f,  1.0f,  1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f);
				v[18] = Vertex(-1.0f,  1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f);
				v[19] = Vertex(-1.0f, -1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f);

				// fill in the right face vertex data
				v[20] = Vertex( 1.0f, -1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f);
				v[21] = Vertex( 1.0f,  1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f);
				v[22] = Vertex( 1.0f,  1.0f,  1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f);
				v[23] = Vertex( 1.0f, -1.0f,  1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f);

				_vb->Unlock();

				_device->CreateIndexBuffer(
					36 * sizeof(WORD),
					D3DUSAGE_WRITEONLY,
					D3DFMT_INDEX16,
					D3DPOOL_MANAGED,
					&_ib,
					0);

				WORD* i = 0;
				_ib->Lock(0, 0, (void**)&i, 0);

				// fill in the front face index data
				i[0] = 0; i[1] = 1; i[2] = 2;
				i[3] = 0; i[4] = 2; i[5] = 3;

				// fill in the back face index data
				i[6] = 4; i[7]  = 5; i[8]  = 6;
				i[9] = 4; i[10] = 6; i[11] = 7;

				// fill in the top face index data
				i[12] = 8; i[13] =  9; i[14] = 10;
				i[15] = 8; i[16] = 10; i[17] = 11;

				// fill in the bottom face index data
				i[18] = 12; i[19] = 13; i[20] = 14;
				i[21] = 12; i[22] = 14; i[23] = 15;

				// fill in the left face index data
				i[24] = 16; i[25] = 17; i[26] = 18;
				i[27] = 16; i[28] = 18; i[29] = 19;

				// fill in the right face index data
				i[30] = 20; i[31] = 21; i[32] = 22;
				i[33] = 20; i[34] = 22; i[35] = 23;

				_ib->Unlock();


				
			}

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;				

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				//esc키누루면 종료
				if(::GetAsyncKeyState(VK_ESCAPE) && 0x8000)
				{
					pDevice->closeDevice();
				}				

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				
				pSmgr->drawAll();

				pGuiEnv->drawAll();						
				

				{
					//변환재적용
					pVideo->setTransform(irr::video::ETS_WORLD, irr::core::IdentityMatrix);
					pVideo->setTransform(irr::video::ETS_VIEW,pSmgr->getActiveCamera()->getViewMatrix());
					pVideo->setTransform(irr::video::ETS_PROJECTION,pSmgr->getActiveCamera()->getProjectionMatrix());

					{
						//노드 그릴때 메트리얼설정을 해주지않으면
						//지유아이출력하면 제데로안됨
						irr::video::SMaterial m;
						m.TextureLayer[0].Texture = pVideo->getTexture("dx5_logo.bmp");

						//일리히트엔진 메트리얼구조체는setMaterial함수가 호출되는시점에서 메트리얼이
						//하드웨어에 적용되지않고 그리기 함수부분에서 나눠서적용된다.(예> drawIndexedTriangleList)
						//그러나 텍스춰는 setMaterial부분에서 바로 하드웨어에 적용된다.						
						pVideo->setMaterial(m);

						//위와같이 해주면 0번 스테이지에 텍스춰가 일단 적용된것이다.
					}					

					DWORD oldFVF;
					pD9Device->GetFVF(&oldFVF); //버텍스포멧은 다시 복원시켜줘야한다.

					DWORD oldLighting,oldZEnable;
					pD9Device->GetRenderState(D3DRS_LIGHTING,&oldLighting);
					pD9Device->GetRenderState(D3DRS_ZENABLE,&oldZEnable);

					pD9Device->SetRenderState(D3DRS_LIGHTING,FALSE);						
					pD9Device->SetRenderState(D3DRS_ZENABLE,TRUE);

					pD9Device->SetStreamSource(0, _vb, 0, sizeof(Vertex));
					pD9Device->SetIndices(_ib);
					pD9Device->SetFVF(Vertex::FVF);
					pD9Device->DrawIndexedPrimitive(
						D3DPT_TRIANGLELIST, 
						0,                  
						0,                  
						24,
						0,
						12);

					pD9Device->SetRenderState(D3DRS_LIGHTING,oldLighting);						
					pD9Device->SetRenderState(D3DRS_ZENABLE,oldZEnable);

					pD9Device->SetFVF(oldFVF);
				}
				pVideo->endScene();
			}

			pDevice->drop();
		}

	}





	//FX 이펙트 파일 리더
	namespace _03 {

		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9
				);

			pDevice->setWindowCaption(L"FX reader-1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,0,-2.5), irr::core::vector3df(0,0,0));	

			std::vector<irr::video::S3DVertex> Vertices;

			Vertices.push_back(irr::video::S3DVertex(-.5,-.5,0, 0,0,-1,irr::video::SColor(255,255,255,255),0,1) );
			Vertices.push_back(irr::video::S3DVertex(-.5,.5,0, 0,0,-1,irr::video::SColor(255,255,255,255),0,0) );
			Vertices.push_back(irr::video::S3DVertex(.5,.5,0, 0,0,-1,irr::video::SColor(255,255,255,255),1,0) );
			Vertices.push_back(irr::video::S3DVertex(.5,-.5,0, 0,0,-1,irr::video::SColor(255,255,255,255),1,1) );

			std::vector<irr::u16> Indice;

			 //0,1,2,3,0,2 
			Indice.push_back(0);
			Indice.push_back(1);
			Indice.push_back(2);
			Indice.push_back(3);
			Indice.push_back(0);
			Indice.push_back(2);


			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);


			////////////fx

			LPDIRECT3DTEXTURE9      g_pMeshTexture   = NULL; /// 텍스처

			LPDIRECT3DVERTEXDECLARATION9	g_pDecl=0;	/// 정점들 선언정보
			LPD3DXEFFECT					g_pEffect=0;	/// fx파일을 사용하기 위한 인터페이스

			D3DXMATRIXA16			g_matWorld;		/// world행렬
			D3DXMATRIXA16			g_matView;		/// view행렬
			D3DXMATRIXA16			g_matProj;		/// projection행렬
			LPDIRECT3DDEVICE9       g_pd3dDevice = NULL; // 렌더링에 사용될 D3D디바이스	

			g_pd3dDevice = pDevice->getVideoDriver()->getExposedVideoData().D3D9.D3DDev9;

			//택스춰 로딩
			{
				D3DXCreateTextureFromFile( g_pd3dDevice, L"dotnetback.jpg", &g_pMeshTexture );
			}

			//쉐이더 설정
			{
				DWORD dwShaderFlags = D3DXFX_NOT_CLONEABLE;
				

				D3DVERTEXELEMENT9	decl[MAX_FVF_DECL_SIZE];
				D3DXDeclaratorFromFVF(D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_DIFFUSE | D3DFVF_TEX1,decl);
				g_pd3dDevice->CreateVertexDeclaration( decl, &g_pDecl );
				

				D3DXCreateEffectFromFile( g_pd3dDevice, L"sk3d/SimpleSample_vi.fx", NULL, NULL, dwShaderFlags, 
					NULL, &g_pEffect, NULL );

				// Set effect variables as needed
				D3DXCOLOR colorMtrlDiffuse(1.0f, 1.0f, 1.0f, 1.0f);
				D3DXCOLOR colorMtrlAmbient(0.35f, 0.35f, 0.35f, 0);

				( g_pEffect->SetValue("g_MaterialAmbientColor", &colorMtrlAmbient, sizeof(D3DXCOLOR) ) );
				( g_pEffect->SetValue("g_MaterialDiffuseColor", &colorMtrlDiffuse, sizeof(D3DXCOLOR) ) );    
				( g_pEffect->SetTexture( "g_MeshTexture", g_pMeshTexture) );					
			}

			while(pDevice->run())
			{	
				
				static irr::u32 uLastTick=0;
				//밀리세컨드값얻기
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();

				//일리히트 기본기능으로 그리기
				{

				}
				

				{//쉐이더 상수전달
					D3DXMATRIXA16 mWorld;
					D3DXMATRIXA16 mView;
					D3DXMATRIXA16 mProj;
					D3DXMATRIXA16 mWorldViewProjection;

					// Get the projection & view matrix from the camera class						
					irr::scene::ICameraSceneNode *pCam = pSmgr->getActiveCamera();
					irr::core::matrix4 worldMat;
					worldMat.makeIdentity(); //월드변환은 단위행렬
					irr::core::matrix4 viewMat = pCam->getViewMatrix();
					irr::core::matrix4 prjMat = pCam->getProjectionMatrix();						

					int col,row;
					for(row=0;row<4;row++)
					{
						for(col=0;col<4;col++)
						{
							mWorld(row,col) =  worldMat(row,col);
							mProj(row,col) = prjMat(row,col);
							mView(row,col) = viewMat(row,col);
						}
					}						

					mWorldViewProjection = mWorld * mView * mProj;

					// Update the effect's variables.  Instead of using strings, it would 
					// be more efficient to cache a handle to the parameter by calling 
					// ID3DXEffect::GetParameterByName
					g_pEffect->SetMatrix( "g_mWorldViewProjection", &mWorldViewProjection );
					g_pEffect->SetMatrix( "g_mWorld", &mWorld);
					g_pEffect->SetFloat( "g_fTime", (float)(uLastTick/1000.f) );


					D3DXVECTOR3 vLightDir(0,0,-1);
					D3DXCOLOR   vLightDiffuse(1.f,1.f,1.f,1.f);

					g_pEffect->SetValue( "g_LightDir", &vLightDir, sizeof(D3DXVECTOR3)  );
					g_pEffect->SetValue( "g_LightDiffuse", &vLightDiffuse, sizeof(D3DXVECTOR4)  );

					D3DXCOLOR vWhite = D3DXCOLOR(1,1,1,1);
					g_pEffect->SetValue("g_MaterialDiffuseColor", &vWhite, sizeof(D3DXCOLOR)  );
				}

				{
					//정점선언값 지정
					LPDIRECT3DVERTEXDECLARATION9 pOldVertexDecl;
					g_pd3dDevice->GetVertexDeclaration(&pOldVertexDecl);
					g_pd3dDevice->SetVertexDeclaration( g_pDecl );

					g_pEffect->SetTechnique( "RenderScene" );

					UINT iPass, cPasses;
					g_pEffect->Begin(&cPasses, 0);

					for (iPass = 0; iPass < cPasses; iPass++)
					{

						g_pEffect->BeginPass(iPass);						

						pVideo->drawIndexedTriangleList((irr::video::S3DVertex *)(&Vertices[0]),
							4,
							(irr::u16 *)&Indice[0],
							2
							);

						g_pEffect->EndPass();
					}					 
					g_pEffect->End();	

					g_pd3dDevice->SetVertexDeclaration( pOldVertexDecl );
				}

				pVideo->endScene();	
			}
			pDevice->drop();
			
			SAFE_RELEASE(g_pEffect);
			SAFE_RELEASE(g_pDecl);
		}
	}


#endif



	//일리히트 기본 그리기함수이용한 커스텀씬노드 예제
	namespace _10
	{
		class CSampleSceneNode : public irr::scene::ISceneNode
		{
			irr::core::aabbox3d<irr::f32> Box;			
			irr::video::SMaterial Material;

			irr::core::vector3df m_vLine[2];

		public:
			CSampleSceneNode(irr::scene::ISceneNode* parent, irr::scene::ISceneManager* mgr, irr::s32 id)
				: irr::scene::ISceneNode(parent, mgr, id)
			{ 
				//Material.Wireframe = false;
				Material.Lighting = false;

				m_vLine[0] = irr::core::vector3df(0,0,0);
				m_vLine[1] = irr::core::vector3df(10,10,0);

				Box.MaxEdge = irr::core::vector3df(10,10,10); 		
				Box.MinEdge = irr::core::vector3df(-10,-10,-10);
			}

			virtual void OnRegisterSceneNode(){  
				if (IsVisible)    
					SceneManager->registerNodeForRendering(this);
				ISceneNode::OnRegisterSceneNode();
			}		

			virtual void render()
			{  	
				if (IsVisible == false) return;

				irr::video::IVideoDriver* driver = SceneManager->getVideoDriver();

				driver->setMaterial(Material);					
				driver->setTransform(irr::video::ETS_WORLD, AbsoluteTransformation);
				driver->draw3DLine(m_vLine[0],m_vLine[1],irr::video::SColor(255,255,0,0));


				if(DebugDataVisible)
				{//디버깅정보출력
					irr::video::SMaterial _Material;


					_Material.Lighting = false;
					_Material.Wireframe = false;				

					driver->setMaterial(_Material);			
					irr::core::aabbox3df box1 = getBoundingBox();			
					driver->draw3DBox(box1);				
				}
			}

			virtual const irr::core::aabbox3d<irr::f32>& getBoundingBox() const  
			{    
				return Box;  
			}

			virtual irr::u32 getMaterialCount() const
			{
				return 1;
			}

			virtual irr::video::SMaterial& getMaterial(irr::u32 num)
			{
				return Material;

			}
		};

		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");
			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,10,-50), irr::core::vector3df(0,0,0));

			//사용자 노드생성
			CSampleSceneNode *myNode = new CSampleSceneNode(pSmgr->getRootSceneNode(), pSmgr,-1);
			myNode->drop();			
			myNode->setDebugDataVisible(irr::scene::EDS_FULL);



			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();					

				pVideo->endScene();	
			}

			pDevice->drop();
		}	

	}

	//일리히트 기본 그리기함수이용 커스텀씬노드 예제2
	//drawIndexedTriangleList
	namespace _11
	{
		class CSampleSceneNode : public irr::scene::ISceneNode
		{
			irr::core::aabbox3d<irr::f32> Box;
			irr::video::S3DVertex Vertices[4];
			irr::video::SMaterial Material;

		public:
			CSampleSceneNode(irr::scene::ISceneNode* parent, irr::scene::ISceneManager* mgr, irr::s32 id)
				: irr::scene::ISceneNode(parent, mgr, id)
			{ 

				Material.Wireframe = false;
				Material.Lighting = false;

				Vertices[0] = irr::video::S3DVertex(-.5,-.5,0, 0,0,-1,irr::video::SColor(0,0,255,255),0,1);
				Vertices[1] = irr::video::S3DVertex(-.5,.5,0, 0,0,-1,irr::video::SColor(0,255,0,255),0,0);
				Vertices[2] = irr::video::S3DVertex(.5,.5,0, 0,0,-1,irr::video::SColor(0,255,255,0),1,0);
				Vertices[3] = irr::video::S3DVertex(.5,-.5,0, 0,0,-1,irr::video::SColor(0,0,255,0),1,1);

				Box.reset(Vertices[0].Pos); 		
				for (irr::s32 i=1; i<4; ++i) 			
					Box.addInternalPoint(Vertices[i].Pos);
			}

			virtual void OnRegisterSceneNode(){  
				//if(SceneManager->isCulled(this))
				{
					if (IsVisible)    
					{
						SceneManager->registerNodeForRendering(this);
					}
					ISceneNode::OnRegisterSceneNode();
				}
			}		

			virtual void render()
			{  	
				if (IsVisible == false) return;

				irr::u16 indices[] = { 0,1,2,3,0,2 };
				irr::video::IVideoDriver* driver = SceneManager->getVideoDriver();
				driver->setMaterial(Material);				

				driver->setTransform(irr::video::ETS_WORLD, AbsoluteTransformation);
				driver->drawIndexedTriangleList(&Vertices[0], 4, &indices[0], 2);

				if(DebugDataVisible)
				{//디버깅정보출력
					irr::video::SMaterial _Material;


					_Material.Lighting = false;
					_Material.Wireframe = false;				

					driver->setMaterial(_Material);			
					irr::core::aabbox3df box1 = getBoundingBox();			
					driver->draw3DBox(box1);				

				}
			}

			virtual const irr::core::aabbox3d<irr::f32>& getBoundingBox() const  
			{    
				return Box;  
			}

			virtual irr::u32 getMaterialCount() const
			{
				return 1;
			}

			virtual irr::video::SMaterial& getMaterial(irr::u32 num)
			{
				return Material;

			}
		};

		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");
			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,10,-50), irr::core::vector3df(0,0,0));

			//사용자 노드생성
			CSampleSceneNode *myNode = new CSampleSceneNode(pSmgr->getRootSceneNode(), pSmgr,-1);
			myNode->drop();
			myNode->setScale(irr::core::vector3df(50,50,1));

			//irr::scene::ISceneNodeAnimator *pAnim = pSmgr->createRotationAnimator(irr::core::vector3df(1.f,0,0));

			//	myNode->addAnimator(pAnim);

			//	pAnim->drop();


			//택스춰 등록
			irr::video::ITexture *pTex = pVideo->getTexture("jga/samurai.jpg");
			myNode->setMaterialTexture(0,pTex);	

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();					

				pVideo->endScene();	
			}
			pDevice->drop();
		}	

	}


#ifdef USE_MS_DX9
	//DX 커스텀씬노드 예제 1. 와이어 프레임 큐브,인덱스버퍼이용
	namespace _12
	{
		struct Vertex
		{
			Vertex(){}
			Vertex(float x, float y, float z)
			{
				_x = x;  _y = y;  _z = z;
			}
			float _x, _y, _z;
			static const DWORD FVF;
		};
		const DWORD Vertex::FVF = D3DFVF_XYZ;

		class CSampleSceneNode : public irr::scene::ISceneNode
		{
			irr::core::aabbox3d<irr::f32> Box;

			//irr::video::S3DVertex Vertices[4];

			IDirect3DVertexBuffer9* VB;
			IDirect3DIndexBuffer9*  IB;

			irr::video::SMaterial Material;

		public:
			CSampleSceneNode(irr::scene::ISceneNode* parent, irr::scene::ISceneManager* mgr, irr::s32 id)
				: irr::scene::ISceneNode(parent, mgr, id)
			{ 

				Material.Wireframe = false;
				Material.Lighting = false;				

				//
				// Classes and Structures
				//

				{
					//DX only!
					IDirect3DDevice9* Device = mgr->getVideoDriver()->getExposedVideoData().D3D9.D3DDev9;

					//
					// Create vertex and index buffers.
					//
					Device->CreateVertexBuffer(
						8 * sizeof(Vertex), 
						D3DUSAGE_WRITEONLY,
						Vertex::FVF,
						D3DPOOL_MANAGED,
						&VB,
						0);

					Device->CreateIndexBuffer(
						36 * sizeof(WORD),
						D3DUSAGE_WRITEONLY,
						D3DFMT_INDEX16,
						D3DPOOL_MANAGED,
						&IB,
						0);

					//
					// Fill the buffers with the cube data.
					//

					// define unique vertices:
					Vertex* vertices;
					VB->Lock(0, 0, (void**)&vertices, 0);

					// vertices of a unit cube
					vertices[0] = Vertex(-1.0f, -1.0f, -1.0f);
					vertices[1] = Vertex(-1.0f,  1.0f, -1.0f);
					vertices[2] = Vertex( 1.0f,  1.0f, -1.0f);
					vertices[3] = Vertex( 1.0f, -1.0f, -1.0f);
					vertices[4] = Vertex(-1.0f, -1.0f,  1.0f);
					vertices[5] = Vertex(-1.0f,  1.0f,  1.0f);
					vertices[6] = Vertex( 1.0f,  1.0f,  1.0f);
					vertices[7] = Vertex( 1.0f, -1.0f,  1.0f);

					VB->Unlock();

					// define the triangles of the cube:
					WORD* indices = 0;
					IB->Lock(0, 0, (void**)&indices, 0);

					// front side
					indices[0]  = 0; indices[1]  = 1; indices[2]  = 2;
					indices[3]  = 0; indices[4]  = 2; indices[5]  = 3;

					// back side
					indices[6]  = 4; indices[7]  = 6; indices[8]  = 5;
					indices[9]  = 4; indices[10] = 7; indices[11] = 6;

					// left side
					indices[12] = 4; indices[13] = 5; indices[14] = 1;
					indices[15] = 4; indices[16] = 1; indices[17] = 0;

					// right side
					indices[18] = 3; indices[19] = 2; indices[20] = 6;
					indices[21] = 3; indices[22] = 6; indices[23] = 7;

					// top
					indices[24] = 1; indices[25] = 5; indices[26] = 6;
					indices[27] = 1; indices[28] = 6; indices[29] = 2;

					// bottom
					indices[30] = 4; indices[31] = 0; indices[32] = 3;
					indices[33] = 4; indices[34] = 3; indices[35] = 7;

					IB->Unlock();

					Box.reset(irr::core::vector3df(vertices[0]._x,vertices[0]._y,vertices[0]._z)); 		
					for (irr::s32 i=1; i<4; ++i) 			
						Box.addInternalPoint(
						irr::core::vector3df(vertices[i]._x,vertices[i]._y,vertices[i]._z)
						);
				}		

			}

			virtual void OnRegisterSceneNode(){  
				if (IsVisible)    
					SceneManager->registerNodeForRendering(this);
				ISceneNode::OnRegisterSceneNode();
			}		

			virtual void render()
			{  	
				if (IsVisible == false) return;

				/*irr::u16 indices[] = { 0,1,2,3,0,2 };
				irr::video::IVideoDriver* driver = SceneManager->getVideoDriver();
				driver->setMaterial(Material);				

				driver->setTransform(irr::video::ETS_WORLD, AbsoluteTransformation);
				driver->drawIndexedTriangleList(&[0], 4, &indices[0], 2);*/

				irr::video::IVideoDriver* driver = SceneManager->getVideoDriver();
				IDirect3DDevice9* pD9Device = driver->getExposedVideoData().D3D9.D3DDev9;

				driver->setMaterial(Material);
				driver->setTransform(irr::video::ETS_WORLD, AbsoluteTransformation);

				{
					pD9Device->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);				
					pD9Device->SetRenderState(D3DRS_LIGHTING,FALSE);
				}

				{
					//IDirect3DVertexBuffer9* _VB = 0;
					//IDirect3DIndexBuffer9*  _IB = 0;					
					//pD9Device->GetStreamSource(0,_VB,0,

					DWORD oldFVF;
					pD9Device->GetFVF(&oldFVF);					

					pD9Device->SetStreamSource(0, VB, 0, sizeof(Vertex));
					pD9Device->SetIndices(IB);
					pD9Device->SetFVF(Vertex::FVF);
					// Draw cube.
					pD9Device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 8, 0, 12);

					pD9Device->SetFVF(oldFVF);
				}

				{
					pD9Device->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);				
					pD9Device->SetRenderState(D3DRS_LIGHTING,FALSE);
				}




				if(DebugDataVisible)
				{//디버깅정보출력
					irr::video::SMaterial _Material;


					_Material.Lighting = false;
					_Material.Wireframe = false;				

					driver->setMaterial(_Material);			
					irr::core::aabbox3df box1 = getBoundingBox();			
					driver->draw3DBox(box1);				

				}
			}

			virtual const irr::core::aabbox3d<irr::f32>& getBoundingBox() const  
			{    
				return Box;  
			}

			virtual irr::u32 getMaterialCount() const
			{
				return 1;
			}

			virtual irr::video::SMaterial& getMaterial(irr::u32 num)
			{
				return Material;

			}
		};

		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");
			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,0,-5), irr::core::vector3df(0,0,0));

			//사용자 노드생성
			CSampleSceneNode *myNode = new CSampleSceneNode(pSmgr->getRootSceneNode(), pSmgr,-1);
			myNode->drop();
			//myNode->setScale(irr::core::vector3df(50,50,1));


			//택스춰 등록
			//irr::video::ITexture *pTex = pVideo->getTexture("jga/samurai.jpg");
			//myNode->setMaterialTexture(0,pTex);	

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();					

				pVideo->endScene();	
			}

			pDevice->drop();
		}	

	}


	//DX 커스텀씬노드 예제 2. 버텍스 프리미티브로 삼각형출력하기
	namespace _13
	{	

		struct Vertex
		{
			Vertex(){}

			Vertex(float x, float y, float z)
			{
				_x = x;  _y = y;  _z = z;
			}

			float _x, _y, _z;

			static const DWORD FVF;
		};
		const DWORD Vertex::FVF = D3DFVF_XYZ;

		class CSampleSceneNode : public irr::scene::ISceneNode
		{
			irr::core::aabbox3d<irr::f32> Box;
			//irr::video::S3DVertex Vertices[4];
			irr::video::SMaterial Material;
			//ID3DXMesh* Objects;
			IDirect3DVertexBuffer9* Triangle;

		public:
			CSampleSceneNode(irr::scene::ISceneNode* parent, irr::scene::ISceneManager* mgr, irr::s32 id)
				: irr::scene::ISceneNode(parent, mgr, id)
			{ 				

				IDirect3DDevice9 *pDevice = mgr->getVideoDriver()->getExposedVideoData().D3D9.D3DDev9;

				/*D3DXCreateTeapot(
				pDevice,
				&Objects,
				0);*/

				IDirect3DDevice9* Device = pDevice;
				Device->CreateVertexBuffer(
					3 * sizeof(Vertex), // size in bytes
					D3DUSAGE_WRITEONLY, // flags
					Vertex::FVF,        // vertex format
					D3DPOOL_MANAGED,    // managed memory pool
					&Triangle,          // return create vertex buffer
					0);                 // not used - set to 0

				//
				// Fill the buffers with the triangle data.
				//

				Vertex* vertices;
				Triangle->Lock(0, 0, (void**)&vertices, 0);

				vertices[0] = Vertex(-1.0f, 0.0f, 2.0f);
				vertices[1] = Vertex( 0.0f, 1.0f, 2.0f);
				vertices[2] = Vertex( 1.0f, 0.0f, 2.0f);

				Triangle->Unlock();

			}

			virtual ~CSampleSceneNode()
			{
				//Objects->Release();
			}

			virtual void OnRegisterSceneNode(){  
				if (IsVisible)    
					SceneManager->registerNodeForRendering(this);
				ISceneNode::OnRegisterSceneNode();
			}  

			virtual void render()
			{   
				if (IsVisible == false) return;		


				irr::video::IVideoDriver* driver = SceneManager->getVideoDriver();
				IDirect3DDevice9* pD9Device = driver->getExposedVideoData().D3D9.D3DDev9;

				driver->setMaterial(Material);

				driver->setTransform(irr::video::ETS_WORLD, AbsoluteTransformation);

				//현제상태저장
				DWORD fillmode,lightmode;
				pD9Device->GetRenderState(D3DRS_FILLMODE,&fillmode);
				pD9Device->GetRenderState(D3DRS_LIGHTING,&lightmode);


				pD9Device->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);				
				pD9Device->SetRenderState(D3DRS_LIGHTING,FALSE);


				{
					DWORD oldFVF;
					pD9Device->GetFVF(&oldFVF);					

					pD9Device->SetStreamSource(0, Triangle, 0, sizeof(Vertex));					
					pD9Device->SetFVF(Vertex::FVF);


					//pD9Device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 8, 0, 12);
					pD9Device->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 1);

					pD9Device->SetFVF(oldFVF);
				}


				//복귀
				pD9Device->SetRenderState(D3DRS_FILLMODE, fillmode);				
				pD9Device->SetRenderState(D3DRS_LIGHTING,lightmode);



				if(DebugDataVisible)
				{//디버깅정보출력
					irr::video::SMaterial _Material;


					_Material.Lighting = false;
					_Material.Wireframe = false;    

					driver->setMaterial(_Material);   
					irr::core::aabbox3df box1 = getBoundingBox();   
					driver->draw3DBox(box1);    

				}
			}

			virtual const irr::core::aabbox3d<irr::f32>& getBoundingBox() const  
			{    
				return Box;  
			}

			virtual irr::u32 getMaterialCount() const
			{
				return 1;
			}

			virtual irr::video::SMaterial& getMaterial(irr::u32 num)
			{
				return Material;

			}
		};

		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(     
				irr::video::EDT_DIRECT3D9
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");
			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,0,-5), irr::core::vector3df(0,0,0));

			//사용자 노드생성
			CSampleSceneNode *myNode = new CSampleSceneNode(pSmgr->getRootSceneNode(), pSmgr,-1);
			myNode->drop();
			//myNode->setScale(irr::core::vector3df(50,50,1));


			//택스춰 등록
			//irr::video::ITexture *pTex = pVideo->getTexture("dx_media/lobbyxpos.jpg");
			//myNode->setMaterialTexture(0,pTex); 

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{ 
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();      
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();     

				pVideo->endScene(); 
			}

			pDevice->drop();
		} 


	}	

	//인덱스버퍼 텍스춰 멥핑
	namespace _14
	{	
		struct Vertex
		{
			Vertex(){}
			Vertex(
				float x, float y, float z,
				float nx, float ny, float nz,
				float u, float v)
			{
				_x  = x;  _y  = y;  _z  = z;
				_nx = nx; _ny = ny; _nz = nz;
				_u  = u;  _v  = v;
			}
			float _x, _y, _z;
			float _nx, _ny, _nz;
			float _u, _v; // texture coordinates
			static const DWORD FVF;
		};
		const DWORD Vertex::FVF = D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1;

		class CSampleSceneNode : public irr::scene::ISceneNode
		{
			irr::core::aabbox3d<irr::f32> Box;			
			irr::video::SMaterial Material;	

			IDirect3DVertexBuffer9* m_VertexBuffer;
			IDirect3DIndexBuffer9*  m_IndexBuffer;

		public:
			CSampleSceneNode(irr::scene::ISceneNode* parent, irr::scene::ISceneManager* mgr, irr::s32 id)
				: irr::scene::ISceneNode(parent, mgr, id)
			{
				IDirect3DDevice9 *pDevice = mgr->getVideoDriver()->getExposedVideoData().D3D9.D3DDev9;			

				//dx 관련 초기화
				{
					IDirect3DVertexBuffer9* _vb;
					IDirect3DIndexBuffer9*  _ib;

					// save a ptr to the device
					IDirect3DDevice9* _device = pDevice;

					_device->CreateVertexBuffer(
						24 * sizeof(Vertex), 
						D3DUSAGE_WRITEONLY,
						Vertex::FVF,
						D3DPOOL_MANAGED,
						&_vb,
						0);

					Vertex* v;
					_vb->Lock(0, 0, (void**)&v, 0);

					// build box

					// fill in the front face vertex data
					v[0] = Vertex(-1.0f, -1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f);
					v[1] = Vertex(-1.0f,  1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f);
					v[2] = Vertex( 1.0f,  1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f);
					v[3] = Vertex( 1.0f, -1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f);

					// fill in the back face vertex data
					v[4] = Vertex(-1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f);
					v[5] = Vertex( 1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f);
					v[6] = Vertex( 1.0f,  1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f);
					v[7] = Vertex(-1.0f,  1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f);

					// fill in the top face vertex data
					v[8]  = Vertex(-1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f);
					v[9]  = Vertex(-1.0f, 1.0f,  1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f);
					v[10] = Vertex( 1.0f, 1.0f,  1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f);
					v[11] = Vertex( 1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f);

					// fill in the bottom face vertex data
					v[12] = Vertex(-1.0f, -1.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f);
					v[13] = Vertex( 1.0f, -1.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f);
					v[14] = Vertex( 1.0f, -1.0f,  1.0f, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f);
					v[15] = Vertex(-1.0f, -1.0f,  1.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f);

					// fill in the left face vertex data
					v[16] = Vertex(-1.0f, -1.0f,  1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f);
					v[17] = Vertex(-1.0f,  1.0f,  1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f);
					v[18] = Vertex(-1.0f,  1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f);
					v[19] = Vertex(-1.0f, -1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f);

					// fill in the right face vertex data
					v[20] = Vertex( 1.0f, -1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f);
					v[21] = Vertex( 1.0f,  1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f);
					v[22] = Vertex( 1.0f,  1.0f,  1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f);
					v[23] = Vertex( 1.0f, -1.0f,  1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f);

					_vb->Unlock();

					_device->CreateIndexBuffer(
						36 * sizeof(WORD),
						D3DUSAGE_WRITEONLY,
						D3DFMT_INDEX16,
						D3DPOOL_MANAGED,
						&_ib,
						0);

					WORD* i = 0;
					_ib->Lock(0, 0, (void**)&i, 0);

					// fill in the front face index data
					i[0] = 0; i[1] = 1; i[2] = 2;
					i[3] = 0; i[4] = 2; i[5] = 3;

					// fill in the back face index data
					i[6] = 4; i[7]  = 5; i[8]  = 6;
					i[9] = 4; i[10] = 6; i[11] = 7;

					// fill in the top face index data
					i[12] = 8; i[13] =  9; i[14] = 10;
					i[15] = 8; i[16] = 10; i[17] = 11;

					// fill in the bottom face index data
					i[18] = 12; i[19] = 13; i[20] = 14;
					i[21] = 12; i[22] = 14; i[23] = 15;

					// fill in the left face index data
					i[24] = 16; i[25] = 17; i[26] = 18;
					i[27] = 16; i[28] = 18; i[29] = 19;

					// fill in the right face index data
					i[30] = 20; i[31] = 21; i[32] = 22;
					i[33] = 20; i[34] = 22; i[35] = 23;

					_ib->Unlock();

					m_IndexBuffer = _ib;
					m_VertexBuffer = _vb;

				}

			}

			virtual ~CSampleSceneNode()
			{
				//Objects->Release();
			}

			virtual void OnRegisterSceneNode(){  
				if (IsVisible)    
					SceneManager->registerNodeForRendering(this);
				ISceneNode::OnRegisterSceneNode();
			}  

			virtual void render()
			{   
				if (IsVisible == false) return;		


				irr::video::IVideoDriver* driver = SceneManager->getVideoDriver();
				IDirect3DDevice9* pD9Device = driver->getExposedVideoData().D3D9.D3DDev9;

				driver->setMaterial(Material);

				driver->setTransform(irr::video::ETS_WORLD, AbsoluteTransformation);

				//현제상태저장
				DWORD fillmode,lightmode;
				pD9Device->GetRenderState(D3DRS_FILLMODE,&fillmode);
				pD9Device->GetRenderState(D3DRS_LIGHTING,&lightmode);


				//pD9Device->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);				
				pD9Device->SetRenderState(D3DRS_LIGHTING,FALSE);


				{
					DWORD oldFVF;
					pD9Device->GetFVF(&oldFVF);					

					pD9Device->SetStreamSource(0, m_VertexBuffer, 0, sizeof(Vertex));					
					pD9Device->SetIndices(m_IndexBuffer);
					pD9Device->SetFVF(Vertex::FVF);

					pD9Device->DrawIndexedPrimitive(
						D3DPT_TRIANGLELIST, 
						0,                  
						0,                  
						24,
						0,
						12);
					//pD9Device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 8, 0, 12);
					//pD9Device->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 1);

					pD9Device->SetFVF(oldFVF);
				}


				//복귀
				pD9Device->SetRenderState(D3DRS_FILLMODE, fillmode);				
				pD9Device->SetRenderState(D3DRS_LIGHTING,lightmode);



				if(DebugDataVisible)
				{//디버깅정보출력
					irr::video::SMaterial _Material;


					_Material.Lighting = false;
					_Material.Wireframe = false;    

					driver->setMaterial(_Material);   
					irr::core::aabbox3df box1 = getBoundingBox();   
					driver->draw3DBox(box1);    

				}
			}

			virtual const irr::core::aabbox3d<irr::f32>& getBoundingBox() const  
			{    
				return Box;  
			}

			virtual irr::u32 getMaterialCount() const
			{
				return 1;
			}

			virtual irr::video::SMaterial& getMaterial(irr::u32 num)
			{
				return Material;

			}
		};

		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(     
				irr::video::EDT_DIRECT3D9
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();			

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");
			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,2,-4), irr::core::vector3df(0,0,0));

			//사용자 노드생성
			{
				CSampleSceneNode *myNode = new CSampleSceneNode(pSmgr->getRootSceneNode(), pSmgr,-1);
				myNode->setMaterialTexture(0,pVideo->getTexture("dx5_logo.bmp"));
				myNode->setName("usr/node/dxcustom");				

				irr::scene::ISceneNodeAnimator *pAnim = pSmgr->createRotationAnimator(irr::core::vector3df(1.f,1.0f,0));
				myNode->addAnimator(pAnim);

				pAnim->drop();
				myNode->drop();				
			}

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{ 
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();      
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();     

				pVideo->endScene(); 
			}

			pDevice->drop();
		} 		

	}
#endif
}