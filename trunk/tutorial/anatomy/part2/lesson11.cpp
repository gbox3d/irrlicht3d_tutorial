#include <irrlicht.h>
//#include <assert.h>
#include "../../../com/CSampleApp.h"

//직교투영 관련예제
namespace Lesson11
{	
	CSampleApp theApp(irr::core::vector2di(100,300));

	namespace _00
	{

		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9,
				irr::core::dimension2du(640,480), 32,
				false, false, true,
				NULL
				);

			pDevice->setEventReceiver(&theApp);

			//창이름정하기
			pDevice->setWindowCaption(L"otho graphics exam-1");
			//비디오객체얻기
			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			//씬매니져 얻기
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			//GUI객체얻기
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();	

			irr::scene::ICameraSceneNode *pCam = pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,0,-10.f), irr::core::vector3df(0,0,0));

			{//직교 카메라
				irr::core::matrix4 orthoMatrix; 
				orthoMatrix.buildProjectionMatrixOrthoLH(
					(irr::f32)pVideo->getScreenSize().Width/10, 
					(irr::f32)pVideo->getScreenSize().Height/10, -500.0f, 500.0f);
				pCam->setProjectionMatrix(orthoMatrix);
			}	

			irr::scene::ISceneNode *pNode = pSmgr->addCubeSceneNode();
			pNode->setName("fornt");
			pNode->setPosition(irr::core::vector3df(0,0,0));
			pNode->setScale(irr::core::vector3df(5,1,1));
			pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
			pNode->setMaterialType(irr::video::EMT_TRANSPARENT_ALPHA_CHANNEL_REF);
			pNode->setMaterialTexture(0,pVideo->getTexture("../../res/irrlichtlogo2.png"));

			pNode = pSmgr->addCubeSceneNode();
			pNode->setName("back");
			pNode->setPosition(irr::core::vector3df(0,-5,1));
			pNode->setScale(irr::core::vector3df(5,1,1));
			pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
			pNode->setMaterialType(irr::video::EMT_TRANSPARENT_ALPHA_CHANNEL_REF);
			pNode->setMaterialTexture(0,pVideo->getTexture("../../res/irrlichtlogo2.png"));

			while(pDevice->run())
			{		
				static irr::u32 uLastTick;
				irr::u32 uTick = pDevice->getTimer()->getTime();
				irr::f32 fDelta;
				fDelta = (float)((float)uTick - (float)uLastTick)/1000.f;
				uLastTick = uTick;

				if(theApp.m_bDirCmd[2])
				{
					irr::scene::ISceneNode *pNode = pSmgr->getSceneNodeFromName("fornt");
					pNode->setPosition(pNode->getPosition() + irr::core::vector3df(1,0,0) * fDelta*10);

				}

				if(theApp.m_bDirCmd[6])
				{
					irr::scene::ISceneNode *pNode = pSmgr->getSceneNodeFromName("fornt");
					pNode->setPosition(pNode->getPosition() - irr::core::vector3df(1,0,0) * fDelta*10);

				}

				if(theApp.m_bTrigerCmd[0])
				{
					pSmgr->getSceneNodeFromName("fornt")->setMaterialType(irr::video::EMT_TRANSPARENT_ALPHA_CHANNEL_REF);
					pSmgr->getSceneNodeFromName("back")->setMaterialType(irr::video::EMT_TRANSPARENT_ALPHA_CHANNEL_REF);
				}

				if(theApp.m_bTrigerCmd[1])
				{
					pSmgr->getSceneNodeFromName("fornt")->setMaterialType(irr::video::EMT_TRANSPARENT_ALPHA_CHANNEL);
					pSmgr->getSceneNodeFromName("back")->setMaterialType(irr::video::EMT_TRANSPARENT_ALPHA_CHANNEL);
				}

				if(theApp.m_bTrigerCmd[2]) //카메라와 거리 측정
				{
					irr::core::vector3df pos = pSmgr->getActiveCamera()->getPosition();

					printf("to front: %.2f, to bakck : %.2f\n", 
						(pSmgr->getSceneNodeFromName("fornt")->getPosition() - pos).getLength(),
						(pSmgr->getSceneNodeFromName("back")->getPosition() - pos).getLength()
						);

					//pSmgr->getSceneNodeFromName("fornt")->setMaterialType(irr::video::EMT_TRANSPARENT_ALPHA_CHANNEL_REF);
					//pSmgr->getSceneNodeFromName("back")->setMaterialType(irr::video::EMT_TRANSPARENT_ALPHA_CHANNEL_REF);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));		
				pSmgr->drawAll();
				pGuiEnv->drawAll();			

				theApp.DrawControlButton(pVideo);

				pVideo->endScene();		
			}
			pDevice->drop();
		}
	}

	
}

