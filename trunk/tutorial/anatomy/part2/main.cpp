﻿#define _CRT_SECURE_NO_WARNINGS

#include <irrlicht.h>
#include <assert.h>
#include <iostream>

#include <map>
#include <iostream>
#include <string>

typedef void (*fPtr)();
std::map<std::string, fPtr > mapExam;

#pragma comment(lib,"irrlicht.lib")

//void Lesson01(); //b3d파일 로딩
//void Lesson02(); //쉐이더샘플
//void Lesson02_1();
//void Lesson02_2();
//void Lesson02_3();
//void Lesson02_4();
//void Lesson02_5();

namespace Lesson01
{
	namespace _00 {void main();}
}

namespace Lesson02
{
	namespace _00 {void main();}
	namespace _01 {void main();} //깜빡이
	namespace _02 {void main();} //멀티텍스춰링
	namespace _03 {void main();} //뚱보
	namespace _04 {void main();}
	namespace _05 {void main();}

	namespace _10 {void main();}
	namespace _11 {void main();}
}


namespace Lesson03
{
	namespace _00 {void main();}//비디오 드라이버 직접 다루기
	namespace _01 {void main();}//박스안에 삼각형이 들어있는지 검사하기
	namespace _02 {void main();}//fov, aspect ratio(시야각)로 만드는 투영행렬
	namespace _03 {void main();}//fovX,fovY 로 만드는 프로잭션 투영행렬
	namespace _04 {void main();}//카메라노드 직접적용 예제
}

//텍스춰 직접다루기 예제

namespace Lesson04
{
	namespace _00 {void main();}

#ifdef USE_MS_DX9
	namespace _01 {void main();}
	namespace _02 {void main();}
#endif

	namespace _03 {void main();}
	namespace _04 {void main();}
	namespace _05 {void main();}
	namespace _06 {void main();}
	namespace _07 {void main();} //RTT 예제
	namespace _08 {void main();}
}


//메쉬직접다루기예제
namespace Lesson05
{
	namespace _00{void main();}
	namespace _01{void main();}
	namespace _02{void main();}	
}


//스크립트 처리를 위한
//멀티쓰레드 콘솔창 예제
namespace Lesson06
{
	namespace _00{void main();}
}


//다이랙트 엑스 예제
namespace Lesson07
{
	#ifdef USE_MS_DX9
	namespace _00{void main();}	
	namespace _01{void main();} //정점 버퍼만 이용해서 삼각형 그리기
	namespace _02{void main();} //DX 기본 제공 도형 출력
	namespace _03{void main();} //인덱스버퍼활용 큐브 그리기
	namespace _04{void main();} //버텍스컬러 예제
	
	namespace _05{void main();} //방향광 예제1
	namespace _06{void main();} // 방향광 예제2
	namespace _07{void main();} // 포인트라이트(점광원) 예제
	namespace _08{void main();} // 스포트라이트 예제

	namespace _09{void main();} //텍스춰
	namespace _10{void main();} //큐브텍스춰
	namespace _11{void main();} //텍스춰 어드래스모드

	namespace _12{void main();} //알파 블랜딩 예제1 메트리얼 컬러 블랜딩
	namespace _13{void main();} //알파 블랜딩 예제2 텍스춰 블랜딩

	namespace _14{void main();} //오브잭트반사시키기
	namespace _15{void main();} //스탠실버퍼 거울
	namespace _16{void main();} //스탠실버퍼 그림자

	namespace _17{void main();} //메쉬 생성하기
	namespace _18{void main();} //Xfile 읽기
	namespace _19{void main();} //프로그래시브 메쉬
	namespace _20{void main();} //픽킹
	namespace _21{void main();} //기본 변환 버텍 셰이더 
	namespace _22{void main();}
	namespace _23{void main();}

	namespace _24{void main();} //file 읽기2
	namespace _25{void main();}//DX용 그림자 행렬 사용하기
#endif
}

//고급 캐릭터 애니메이션
namespace Lesson08
{
	namespace _00{void main();} //
	namespace _01{void main();} //
	namespace _02{void main();} //
	namespace _03{void main();} //스키닝
	namespace _04{void main();} //useAnimationFrom, 애니메이션 체인지
	namespace _05{void main();} //IK
	namespace _06{void main();}
	namespace _07{void main();} //메쉬 체인지 1.
	namespace _08{void main();} //메쉬 체인지 2.

	namespace _09{void main();} //본제어 1. 조인트에 무기 아이템 달기 예제
	namespace _10{void main();} //본제어 2. 조인트의 여러가지 모드
	namespace _11{void main();} //본제어 3. 해바라기 응용

	namespace _12{void main();} //멀티애니메이션(에니메이션 체인지 응용,클로닝문제 해법)

}

namespace Lesson09
{
	namespace _00{void main();}
	namespace _01{void main();} //태이블예제
	namespace _02{void main();}
	namespace _03{void main();} //gui 에디터 연동예제
	namespace _04{void main();} //메뉴예제
	namespace _05{void main();} 
}


namespace Lesson10
{	
	namespace _00{void main();}//일리히트 그리기 전용함수이용해서 일리히트씬노드 유지한체 직접그리기예제 		
#ifdef USE_MS_DX9
	namespace _01{void main();}//일리히트 랜더링 파이프라인과 DX 그리기 혼합예제1
	namespace _02{void main();}//일리히트 랜더링 파이프라인과 DX 그리기 혼합예제2, 큐브에 텍스춰 출력하기 버전
	namespace _03{void main();}//FX 이펙트 파일 리더
#endif
}

namespace Lesson11
{
	namespace _00{void main();}
}



void main()
{	
	//printf("lesson  part2 1 ~ 30 \n");
	//printf("Select? => \n");

	//메모리릭 탐지
#ifdef _DEBUG
	::_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	//_CrtSetBreakAlloc(1933);
#endif

	mapExam["1-0"] = Lesson01::_00::main;
	mapExam["2-0"] = Lesson02::_00::main;
	mapExam["2-1"] = Lesson02::_01::main;
	mapExam["2-2"] = Lesson02::_02::main;
	mapExam["2-3"] = Lesson02::_03::main;
	mapExam["2-4"] = Lesson02::_04::main;
	mapExam["2-5"] = Lesson02::_05::main;

	mapExam["3-0"] = Lesson03::_00::main;
	mapExam["3-1"] = Lesson03::_01::main;
	mapExam["3-2"] = Lesson03::_02::main;
	mapExam["3-3"] = Lesson03::_03::main;
	mapExam["3-4"] = Lesson03::_04::main;

	mapExam["4-0"] = Lesson04::_00::main;
#ifdef USE_MS_DX9
	mapExam["4-1"] = Lesson04::_01::main;
	mapExam["4-2"] = Lesson04::_02::main;
#endif
	mapExam["4-3"] = Lesson04::_03::main;
	mapExam["4-4"] = Lesson04::_04::main;
	mapExam["4-5"] = Lesson04::_05::main;
	mapExam["4-6"] = Lesson04::_06::main;
	mapExam["4-7"] = Lesson04::_07::main;
	mapExam["4-8"] = Lesson04::_08::main;

	mapExam["5-0"] = Lesson05::_00::main;
	mapExam["5-1"] = Lesson05::_01::main;

	mapExam["6-0"] = Lesson06::_00::main;

#ifdef USE_MS_DX9
	mapExam["7-0"] = Lesson07::_00::main;
	mapExam["7-1"] = Lesson07::_01::main;
	mapExam["7-2"] = Lesson07::_02::main;
	mapExam["7-3"] = Lesson07::_03::main;
	mapExam["7-4"] = Lesson07::_04::main;
	mapExam["7-5"] = Lesson07::_05::main;
	mapExam["7-6"] = Lesson07::_06::main;
	mapExam["7-7"] = Lesson07::_07::main;
	mapExam["7-8"] = Lesson07::_08::main;
	mapExam["7-9"] = Lesson07::_09::main;
	mapExam["7-10"] = Lesson07::_10::main;
	mapExam["7-11"] = Lesson07::_11::main;
	mapExam["7-12"] = Lesson07::_12::main;
	mapExam["7-13"] = Lesson07::_13::main;
	mapExam["7-14"] = Lesson07::_14::main;
	mapExam["7-15"] = Lesson07::_15::main;
	mapExam["7-16"] = Lesson07::_16::main;
	mapExam["7-17"] = Lesson07::_17::main;
	mapExam["7-18"] = Lesson07::_18::main;
	mapExam["7-19"] = Lesson07::_19::main;
	mapExam["7-20"] = Lesson07::_20::main;
	mapExam["7-21"] = Lesson07::_21::main;
	mapExam["7-22"] = Lesson07::_22::main;
	mapExam["7-23"] = Lesson07::_23::main;
	mapExam["7-24"] = Lesson07::_24::main;	
	mapExam["7-25"] = Lesson07::_25::main;	
#endif

	mapExam["8-0"] = Lesson08::_00::main;
	mapExam["8-1"] = Lesson08::_01::main;
	mapExam["8-2"] = Lesson08::_02::main;
	mapExam["8-3"] = Lesson08::_03::main;
	mapExam["8-4"] = Lesson08::_04::main;
	mapExam["8-5"] = Lesson08::_05::main;
	mapExam["8-6"] = Lesson08::_06::main;
	mapExam["8-7"] = Lesson08::_07::main;
	mapExam["8-8"] = Lesson08::_08::main;
	mapExam["8-9"] = Lesson08::_09::main;
	mapExam["8-10"] = Lesson08::_10::main;
	mapExam["8-11"] = Lesson08::_11::main;
	mapExam["8-12"] = Lesson08::_12::main;

	mapExam["9-0"] = Lesson09::_00::main;
	mapExam["9-1"] = Lesson09::_01::main;
	mapExam["9-2"] = Lesson09::_02::main;
	mapExam["9-3"] = Lesson09::_03::main;
	mapExam["9-4"] = Lesson09::_04::main;
	mapExam["9-5"] = Lesson09::_05::main;

	mapExam["10-0"] = Lesson10::_00::main;

	mapExam["11-0"] = Lesson11::_00::main;

#ifdef USE_MS_DX9
	mapExam["10-1"] = Lesson10::_01::main;
#endif

	std::cout << "Select example =>";

	std::string input;
	std::cin >> input;

	if(mapExam.find(input) != mapExam.end())
	{
		mapExam[input]();
	}
	else
	{
		std::cout << "not found fuction" << std::endl;
	}


	//if(irr::core::stringc("1") == szbuf)	{ Lesson01::_00::main();}
	//else if(irr::core::stringc("2") == szbuf)	{Lesson02::_00::main();}
	////else if(irr::core::stringc("2-1") == szbuf)	{Lesson02_1();	}
	//else if(irr::core::stringc("2-1") == szbuf)	{Lesson02::_01::main();	}
	//else if(irr::core::stringc("2-2") == szbuf)	{Lesson02_2();	}	
	//else if(irr::core::stringc("2-3") == szbuf)	{Lesson02_3();	}	
	//else if(irr::core::stringc("2-4") == szbuf)	{Lesson02_4();	}	
	//else if(irr::core::stringc("2-5") == szbuf)	{Lesson02_5();	}	
	//else if(irr::core::stringc("2-10") == szbuf)	{Lesson02::_10::main();	}	
	//else if(irr::core::stringc("2-11") == szbuf)	{Lesson02::_11::main();	}	

	//else if(irr::core::stringc("3") == szbuf)	{Lesson03::_00::main();	}	
	//else if(irr::core::stringc("3-1") == szbuf)	{Lesson03::_01::main();	}	
	//else if(irr::core::stringc("3-2") == szbuf)	{Lesson03::_02::main();}	
	//else if(irr::core::stringc("3-3") == szbuf)	{Lesson03::_03::main();	}	
	//else if(irr::core::stringc("3-4") == szbuf)	{Lesson03::_04::main();	}	

	//else if(irr::core::stringc("4") == szbuf)	{Lesson04::_00::main();	}	
	//else if(irr::core::stringc("4-1") == szbuf)	{Lesson04::_01::main();	}
	//else if(irr::core::stringc("4-2") == szbuf)	{Lesson04::_02::main();}	
	//else if(irr::core::stringc("4-3") == szbuf)	{Lesson04::_03::main();}	
	//else if(irr::core::stringc("4-4") == szbuf)	{Lesson04::_04::main();}	
	//else if(irr::core::stringc("4-5") == szbuf)	{Lesson04::_05::main();}	
	//else if(irr::core::stringc("4-6") == szbuf)	{Lesson04::_06::main();}	
	//else if(irr::core::stringc("4-7") == szbuf)	{Lesson04::_07::main();}	
	//else if(irr::core::stringc("4-8") == szbuf)	{Lesson04::_08::main();}	

	//else if(irr::core::stringc("5") == szbuf)   {Lesson05::_00::main();}	
	//else if(irr::core::stringc("5-1") == szbuf)	{Lesson05::_01::main();	}	
	//else if(irr::core::stringc("6")   == szbuf) {Lesson06::_00::main();}	

	//else if(irr::core::stringc("7")   == szbuf) {Lesson07::_00::main();	}		

	//else if(irr::core::stringc("7-1") == szbuf)	{Lesson07::_01::main();	}	
	//else if(irr::core::stringc("7-2") == szbuf)	{Lesson07::_02::main();	}	
	//else if(irr::core::stringc("7-3") == szbuf)	{Lesson07::_03::main();	}	
	//else if(irr::core::stringc("7-4") == szbuf)	{Lesson07::_04::main();	}	
	//else if(irr::core::stringc("7-5") == szbuf)	{Lesson07::_05::main();	}	
	//else if(irr::core::stringc("7-6") == szbuf)	{Lesson07::_06::main();	}	
	//else if(irr::core::stringc("7-7") == szbuf)	{Lesson07::_07::main();	}	
	//else if(irr::core::stringc("7-8") == szbuf)	{Lesson07::_08::main();	}	
	//else if(irr::core::stringc("7-9") == szbuf)	{Lesson07::_09::main();	}	
	//else if(irr::core::stringc("7-10") == szbuf)	{Lesson07::_10::main();	}	
	//else if(irr::core::stringc("7-11") == szbuf)	{Lesson07::_11::main();	}	
	//else if(irr::core::stringc("7-12") == szbuf)	{Lesson07::_12::main();	}	
	//else if(irr::core::stringc("7-13") == szbuf)	{Lesson07::_13::main();	}	
	//else if(irr::core::stringc("7-14") == szbuf)	{Lesson07::_14::main();	}	
	//else if(irr::core::stringc("7-15") == szbuf)	{Lesson07::_15::main();	}	
	//else if(irr::core::stringc("7-16") == szbuf)	{Lesson07::_16::main();	}	
	//else if(irr::core::stringc("7-17") == szbuf)	{Lesson07::_17::main();	}	
	//else if(irr::core::stringc("7-18") == szbuf)	{Lesson07::_18::main();	}	
	//else if(irr::core::stringc("7-19") == szbuf)	{Lesson07::_19::main();	}	
	//else if(irr::core::stringc("7-20") == szbuf)	{Lesson07::_20::main();	}	
	//else if(irr::core::stringc("7-21") == szbuf)	{Lesson07::_21::main();	}	
	//else if(irr::core::stringc("7-22") == szbuf)	{Lesson07::_22::main();	}	
	//else if(irr::core::stringc("7-23") == szbuf)	{Lesson07::_23::main();	}	
	//else if(irr::core::stringc("7-24") == szbuf)	{Lesson07::_24::main();	}	

	//else if(irr::core::stringc("8") == szbuf)	{Lesson08::_00::main();	}	
	//else if(irr::core::stringc("8-1") == szbuf)	{Lesson08::_01::main();	}	
	//else if(irr::core::stringc("8-2") == szbuf)	{Lesson08::_02::main();	}	
	//else if(irr::core::stringc("8-3") == szbuf)	{Lesson08::_03::main();	}	
	//else if(irr::core::stringc("8-4") == szbuf)	{Lesson08::_04::main();	}	
	//else if(irr::core::stringc("8-5") == szbuf)	{Lesson08::_05::main();	}	
	//else if(irr::core::stringc("8-6") == szbuf)	{Lesson08::_06::main();	}	
	//else if(irr::core::stringc("8-7") == szbuf)	{Lesson08::_07::main();	}	
	//else if(irr::core::stringc("8-8") == szbuf)	{Lesson08::_08::main();	}	
	//else if(irr::core::stringc("8-9") == szbuf)	{Lesson08::_09::main();	}	
	//else if(irr::core::stringc("8-10") == szbuf)	{Lesson08::_10::main();	}	
	//else if(irr::core::stringc("8-11") == szbuf)	{Lesson08::_11::main();	}	
	//else if(irr::core::stringc("8-12") == szbuf)	{Lesson08::_12::main();	}	

	//else if(irr::core::stringc("9") == szbuf)	{Lesson09::_00::main();	}	
	//else if(irr::core::stringc("9-1") == szbuf)	{Lesson09::_01::main();	}	
	//else if(irr::core::stringc("9-2") == szbuf)	{Lesson09::_02::main();	}	
	//else if(irr::core::stringc("9-3") == szbuf)	{Lesson09::_03::main();	}	
	//else if(irr::core::stringc("9-4") == szbuf)	{Lesson09::_04::main();	}		

	//else if(irr::core::stringc("10-0") == szbuf)	{Lesson10::_00::main();	}		
	//else if(irr::core::stringc("10-1") == szbuf)	{Lesson10::_01::main();	}		

	
	//char *ptr = new char [20];
	//std::string str("sasa");
	//_CrtDumpMemoryLeaks(); 
}


