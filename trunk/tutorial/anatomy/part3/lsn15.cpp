﻿#include <irrlicht.h>
#include <windows.h>

//#include <d3dx9.h>


//기타 특수효과 , 특정용도 씬노드 활용예
//빌보드,스카이박스,스카이돔
//그림자

namespace lsn15
{
	//스카이박스예제
	namespace _00
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9
				);

			pDevice->setWindowCaption(L"스카이박스");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			{//fps카메라 만들기
				irr::scene::ICameraSceneNode* pCam =
					pSmgr->addCameraSceneNodeFPS(0,100.0f,1.2f);

				pCam->setPosition(irr::core::vector3df(2700*2,255*2,2600*2));
				pCam->setTarget(irr::core::vector3df(2397*2,343*2,2700*2));
				pCam->setFarValue(42000.0f);

				pCam->setName("usr/scene/camera/mainCam");
				
				// disable mouse cursor
				//마우스 커서 안보이게 하기
				pDevice->getCursorControl()->setVisible(false);		
			}

		// create skybox
		{
			//밉멥방지 
			//항상 일정한 해상도의 텍스춰를 보여 줘야하므로 밉멥이 불필요하다
			pVideo->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS, false);

			pSmgr->addSkyBoxSceneNode(
				pVideo->getTexture("irrlicht2_up.jpg"), //상
				pVideo->getTexture("irrlicht2_dn.jpg"),//하
				pVideo->getTexture("irrlicht2_lf.jpg"),//좌
				pVideo->getTexture("irrlicht2_rt.jpg"),//우
				pVideo->getTexture("irrlicht2_ft.jpg"),//앞
				pVideo->getTexture("irrlicht2_bk.jpg"));//뒤
		}


			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick;
				uLastTick = pDevice->getTimer()->getTime();
				irr::u32 uTick = pDevice->getTimer()->getTime();
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = pDevice->getTimer()->getTime();

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}	
	}

	//스카이돔 예제
	namespace _01
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9
				);

			pDevice->setWindowCaption(L"스카이 돔");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			{//fps카메라 만들기
				irr::scene::ICameraSceneNode* pCam =
					pSmgr->addCameraSceneNodeFPS(0,100.0f,1.2f);

				pCam->setPosition(irr::core::vector3df(2700*2,255*2,2600*2));
				pCam->setTarget(irr::core::vector3df(2397*2,343*2,2700*2));
				pCam->setFarValue(42000.0f);

				pCam->setName("usr/scene/camera/mainCam");
				
				// disable mouse cursor
				//마우스 커서 안보이게 하기
				pDevice->getCursorControl()->setVisible(false);		
			}		

			//스카이돔 만들기
			{
				//밉멥방지 
				//항상 일정한 해상도의 텍스춰를 보여 줘야하므로 밉멥이 불필요하다.
				pVideo->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS, false);
				pSmgr->addSkyDomeSceneNode(pVideo->getTexture("jga/sky_dom_sample.jpg"));
			}


			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick;
				uLastTick = pDevice->getTimer()->getTime();
				irr::u32 uTick = pDevice->getTimer()->getTime();
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = pDevice->getTimer()->getTime();

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}	
	}

	
	
	//워터씬노드
	namespace _02
	{		
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,20,-20), irr::core::vector3df(0,0,0));

			{
				irr::scene::ISceneNode* pNode = 0;
				irr::scene::IAnimatedMesh* pMesh;
				pMesh = pSmgr->addHillPlaneMesh("usr/mesh/watersurf",
					irr::core::dimension2d<irr::f32>(20,20),
					irr::core::dimension2d<irr::u32>(40,40), 0,0,
					irr::core::dimension2d<irr::f32>(0,0),
					irr::core::dimension2d<irr::f32>(10,10));

				pNode = pSmgr->addWaterSurfaceSceneNode(pMesh->getMesh(0), 3.0f, 300.0f, 30.0f);
				pNode->setPosition(irr::core::vector3df(0,0,0));

				pNode->setMaterialTexture(0, pVideo->getTexture("stones.jpg"));
				pNode->setMaterialTexture(1, pVideo->getTexture("water.jpg"));

				pNode->setMaterialType(irr::video::EMT_REFLECTION_2_LAYER);			

				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
			}

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);


			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		



				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}

	
	//빌보드
	namespace _03
	{		
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,20,-20), irr::core::vector3df(0,0,0));


			{
				irr::scene::IAnimatedMeshSceneNode* pNode = 0;
				irr::scene::IAnimatedMesh* pMesh;			

				pMesh = pSmgr->addHillPlaneMesh("usr/mesh/hill",
					irr::core::dimension2d<irr::f32>(8,8),
					irr::core::dimension2d<irr::u32>(20,20), 0,0,
					irr::core::dimension2d<irr::f32>(2,2),
					irr::core::dimension2d<irr::f32>(8,8));

				pNode = pSmgr->addAnimatedMeshSceneNode(pMesh);
				//pNode->setPosition(irr::core::vector3df(0,30,-20));
				pNode->setMaterialTexture(0, pVideo->getTexture("wall.jpg"));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setName("usr/scene/hill");
			}

			// billboard 
			//해바라기 빌보드
			{				
				irr::scene::IBillboardSceneNode *pNode;
				pNode = pSmgr->addBillboardSceneNode();				
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING, false);
				//pNode->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);
				pNode->setMaterialTexture(0, pVideo->getTexture("particlewhite.bmp"));			

				pNode = pSmgr->addBillboardSceneNode();				
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING, false);
				//pNode->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);
				pNode->setMaterialTexture(0, pVideo->getTexture("particlewhite.bmp"));			
				pNode->setPosition(irr::core::vector3df(-10,0,10));
			}

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			{
				irr::scene::ISceneNodeAnimator *pAnim = pSmgr->createFlyCircleAnimator(
					irr::core::vector3df(0,0,0),40,0.001f,irr::core::vector3df(1,1,0));
				pSmgr->getActiveCamera()->addAnimator(pAnim);
				pAnim->drop();
			}


			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		



				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}


	//빌보드 행렬이용 빌보드 만들기
	//일반 빌보드와 트리빌보드 비교 예제
	namespace _04 
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9
				);

			pDevice->setWindowCaption(L"일반 빌보드와 트리빌보드 비교 예제");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			irr::scene::ICameraSceneNode *pCam = pSmgr->addCameraSceneNodeFPS(0,10,0.001f);
			pCam->setPosition(irr::core::vector3df(0,1,-2));

			

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			{
				irr::scene::IAnimatedMeshSceneNode* pNode = 0;
				irr::scene::IAnimatedMesh* pMesh;			

				pMesh = pSmgr->addHillPlaneMesh("usr/mesh/hill",
					irr::core::dimension2d<irr::f32>(8,8),
					irr::core::dimension2d<irr::u32>(20,20), 0,0,
					irr::core::dimension2d<irr::f32>(2,2),
					irr::core::dimension2d<irr::f32>(8,8));

				pNode = pSmgr->addAnimatedMeshSceneNode(pMesh);
				//pNode->setPosition(irr::core::vector3df(0,30,-20));
				pNode->setMaterialTexture(0, pVideo->getTexture("wall.jpg"));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setName("usr/scene/hill");
			}

			{	//트리빌보드용 메쉬
				irr::scene::IAnimatedMesh* pMesh;			
				pMesh = pSmgr->addHillPlaneMesh("usr/mesh/billboard",
					irr::core::dimension2d<irr::f32>(2,4),
					irr::core::dimension2d<irr::u32>(1,1)
					);
				irr::core::matrix4 mat;
				mat.setRotationDegrees(irr::core::vector3df(-90,0,0));				
				pSmgr->getMeshManipulator()->transformMesh(pMesh,mat);
			}

			{//트리 빌보드용씬노드
				irr::scene::IAnimatedMeshSceneNode* pNode = 0;
				//pNode = pSmgr->addAnimatedMeshSceneNode(pMesh);				
				pNode = pSmgr->addAnimatedMeshSceneNode(pSmgr->getMesh("usr/mesh/billboard"));				
				pNode->setMaterialTexture(0, pVideo->getTexture("jga/tree05.tga"));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setName("usr/scene/billboard/1");				
				pNode->setMaterialType(irr::video::EMT_TRANSPARENT_ALPHA_CHANNEL_REF);
				pNode->setPosition(irr::core::vector3df(2,1.9f,0));
				
			}

			// billboard 
			//해바라기 빌보드
			{				
				irr::scene::IBillboardSceneNode *pNode;
				pNode = pSmgr->addBillboardSceneNode(0,irr::core::dimension2df(2,4));				
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING, false);			
				pNode->setMaterialType(irr::video::EMT_TRANSPARENT_ALPHA_CHANNEL_REF);
				pNode->setMaterialTexture(0, pVideo->getTexture("jga/tree05.tga"));
				pNode->setPosition(irr::core::vector3df(-2,1.9f,0));
			}


			pSmgr->getActiveCamera()->OnAnimate(0);

			/*{
				irr::scene::ISceneNodeAnimator *pAnim;
				pAnim = pSmgr->createFlyCircleAnimator(irr::core::vector3df(0,2,0),
					5,0.0001,
					irr::core::vector3df(0,1,0),
					3.14f
					);
				pSmgr->getActiveCamera()->addAnimator(pAnim);
				pAnim->drop();
			}*/


			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				{//트리 빌보드 행렬 만들어 적용하기
					irr::scene::ISceneNode *pNode = pSmgr->getSceneNodeFromName("usr/scene/billboard/1");
					irr::core::matrix4 mat;
					mat.buildAxisAlignedBillboard(
						pSmgr->getActiveCamera()->getPosition(),
						pSmgr->getActiveCamera()->getTarget(),						
						irr::core::vector3df(0,0,0),
						irr::core::vector3df(0,1,0),//up vector
						irr::core::vector3df(0,0,-1)
						);
					pNode->setRotation(mat.getRotationDegrees());
					//std::cout << mat.getRotationDegrees().X << std::endl;
					//pNode->setPosition(mat.getTranslation());
					//pNode->getAbsoluteTransformation() *= mat;					
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}

	//빌보드 행렬 직접 만들기
	//빌보드의 원리 이해 예제
	namespace _05
	{		
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			{
				irr::scene::ICameraSceneNode *pCam = pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,5,-10), irr::core::vector3df(0,0,0));
				pCam->setName("usr/scne/camera/1");
			}

			//힐플래인 메쉬 추가
			{
				irr::scene::IAnimatedMesh *pMesh =
					pSmgr->addHillPlaneMesh("usr/mesh/myhill",
					irr::core::dimension2d<irr::f32>(8,8),
					irr::core::dimension2d<irr::u32>(8,8),
					0,0,
					irr::core::dimension2d<irr::f32>(0,0),
					irr::core::dimension2d<irr::f32>(8,8)
					);
			}

			//씬노드 추가
			{
				irr::scene::IAnimatedMeshSceneNode *pNode =
					pSmgr->addAnimatedMeshSceneNode(pSmgr->getMesh("usr/mesh/myhill"));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setMaterialTexture(0,pVideo->getTexture("wall.jpg"));

				pNode->setName("usr/scene/hill1");
			}

			{
				//irr::scene::ISceneNode *pRootNode = pSmgr->addEmptySceneNode();
				//pRootNode->setName("usr/scenenode/chr_root");

				irr::scene::ICameraSceneNode *pCam = pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,3,-5), irr::core::vector3df(0,0,0));
				pCam->setName("usr/scne/camera/2");


				irr::scene::ISceneNode *pNode;

				{
					pNode = pSmgr->addAnimatedMeshSceneNode(
						pSmgr->addArrowMesh("usr/mesh/arrow/red",
						irr::video::SColor(255,255,0,0),
						irr::video::SColor(255,255,0,0)
						),
						pCam,
						-1,
						irr::core::vector3df(0,0,0),
						irr::core::vector3df(0,0,0)
						);
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
					pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);

					pNode = pSmgr->addAnimatedMeshSceneNode(
						pSmgr->addArrowMesh("usr/mesh/arrow/green",
						irr::video::SColor(255,0,255,0),
						irr::video::SColor(255,0,255,0)
						),
						pCam,
						-1,
						irr::core::vector3df(0,0,0),
						irr::core::vector3df(0,0,-90)
						);
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
					pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);

					pNode = pSmgr->addAnimatedMeshSceneNode(
						pSmgr->addArrowMesh("usr/mesh/arrow/blue",
						irr::video::SColor(255,0,0,255),
						irr::video::SColor(255,0,0,255)
						),
						pCam,
						-1,
						irr::core::vector3df(0,0,0),
						irr::core::vector3df(90,0,0)
						);
					pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
					pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);
				}
			}

			pSmgr->setActiveCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/1"));

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				//fps 식 카메라 캐릭터 컨트롤
				{
					irr::scene::ICameraSceneNode *pCam = (irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2");

					irr::f32 fRotSpeed = 45.f;
					irr::f32 fSpeed = 5.0f;
					irr::core::vector3df vFront = (pCam->getTarget() - pCam->getAbsolutePosition());
					irr::core::vector3df vUp = pCam->getUpVector();

					//직교 재설정
					vFront.normalize();
					vUp.normalize();
					//좌향벡터구하기
					irr::core::vector3df vSide = vFront.crossProduct(vUp); //vFront X vUp
					vSide.normalize();
					//상방벡터구하기
					vUp = vSide.crossProduct(vFront);// vSide X vFront

					irr::core::vector3df vMoveFront = (vFront*fDelta * fSpeed);
					irr::core::vector3df vMoveUp = vUp*fDelta * fSpeed;
					irr::core::vector3df vMoveSide = vSide*fDelta * fSpeed;					

					if(!(::GetAsyncKeyState(VK_LCONTROL) & 0x8001))
					{
						//전후진
						if( (::GetAsyncKeyState(VK_UP) & 0x8001))
						{
							pCam->setPosition(
								pCam->getAbsolutePosition() + vMoveFront
								);

						}
						if( (::GetAsyncKeyState(VK_DOWN) & 0x8001))
						{
							pCam->setPosition(
								pCam->getAbsolutePosition() - vMoveFront
								);

						}

						//좌우게걸음질
						if( (::GetAsyncKeyState(VK_LEFT) & 0x8001) )
						{
							pCam->setPosition(
								pCam->getAbsolutePosition() + vMoveSide
								);

						}

						if( (::GetAsyncKeyState(VK_RIGHT) & 0x8001) )
						{
							pCam->setPosition(
								pCam->getPosition() - vMoveSide
								);

						}

						//상하
						if( (::GetAsyncKeyState(VK_PRIOR) & 0x8001) )
						{
							pCam->setPosition(
								pCam->getPosition() + vMoveUp
								);

						}

						if( (::GetAsyncKeyState(VK_NEXT) & 0x8001) )
						{
							pCam->setPosition(
								pCam->getAbsolutePosition() - vMoveUp
								);

						}
					}
					else
					{//회전
						if( (::GetAsyncKeyState(VK_UP) & 0x8001) )
						{
							pCam->setRotation(
								pCam->getRotation() - irr::core::vector3df(fDelta*fRotSpeed,0,0)
								);
						}
						if( (::GetAsyncKeyState(VK_DOWN) & 0x8001))
						{
							pCam->setRotation(
								pCam->getRotation() + irr::core::vector3df(fDelta*fRotSpeed,0,0)
								);						
						}
						if( (::GetAsyncKeyState(VK_LEFT) & 0x8001) )
						{	
							pCam->setRotation(
								pCam->getRotation() - irr::core::vector3df(0,fDelta*fRotSpeed,0)
								);							
						}
						if( (::GetAsyncKeyState(VK_RIGHT) & 0x8001))
						{		
							pCam->setRotation(
								pCam->getRotation() + irr::core::vector3df(0,fDelta*fRotSpeed,0)
								);							
						}
					}

					//전역변환 업데이트해주기
					pCam->updateAbsolutePosition();

					//업벡터와 시점벡터에 변환반영
					{
						irr::core::vector3df vTarget(0,0,1);
						irr::core::vector3df vUp(0,1,0);

						irr::core::matrix4 mat = pCam->getAbsoluteTransformation();

						mat.transformVect(vTarget);
						mat.transformVect(vUp);

						pCam->setTarget(vTarget);	
						pCam->setUpVector(vUp - pCam->getPosition());						
					}

					if((::GetAsyncKeyState(VK_F1) & 0x8001) == 0x8001)
					{
						pSmgr->setActiveCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/1"));
					}
					if((::GetAsyncKeyState(VK_F2) & 0x8001) == 0x8001)
					{
						pSmgr->setActiveCamera((irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2"));
					}
					
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				{
					irr::video::SMaterial m;
					m.Lighting = false; //라이트를꺼야색이제데로나온다.				
					m.Wireframe = true;
					pVideo->setMaterial(m);
				}

				//트리빌보드
				{
					irr::scene::ICameraSceneNode *pCam = (irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2");
					irr::core::matrix4 matView = pCam->getViewMatrix();
					irr::scene::IMesh *pMesh = pSmgr->getMesh("usr/mesh/arrow/blue")->getMesh(0);

					irr::core::matrix4 MatTM;
					MatTM(0,0) = matView(0,0);
					MatTM(0,2) = matView(0,2);
					MatTM(2,0) = matView(2,0);
					MatTM(2,2) = matView(2,2);
					MatTM = MatTM.getTransposed(); //회전의 역구하기

					//위치 변환(0,3,0)
					MatTM(3,0) = 0;
					MatTM(3,1) = 3;
					MatTM(3,2) = 0;

					irr::core::matrix4 localMat;
					localMat.setRotationDegrees(irr::core::vector3df(-90,0,0));

					MatTM = MatTM * localMat;

					pVideo->setTransform(irr::video::ETS_WORLD,MatTM);

					irr::u32 i;
					for(i=0;i<pMesh->getMeshBufferCount();i++)
					{
						pVideo->drawMeshBuffer(pMesh->getMeshBuffer(i));
					}

				}

				//해바라기 빌보드
				{
					irr::scene::ICameraSceneNode *pCam = (irr::scene::ICameraSceneNode *)pSmgr->getSceneNodeFromName("usr/scne/camera/2");
					irr::core::matrix4 matView = pCam->getViewMatrix();
					irr::scene::IMesh *pMesh = pSmgr->getMesh("usr/mesh/arrow/green")->getMesh(0);

					irr::core::matrix4 MatTM;
					MatTM(0,0) = matView(0,0);
					MatTM(0,1) = matView(0,1);
					MatTM(0,2) = matView(0,2);
					MatTM(1,0) = matView(1,0);
					MatTM(1,1) = matView(1,1);
					MatTM(1,2) = matView(1,2);
					MatTM(2,0) = matView(2,0);
					MatTM(2,1) = matView(2,1);
					MatTM(2,2) = matView(2,2);
					
					MatTM = MatTM.getTransposed(); //회전의 역구하기

					//위치변환 (3,3,0)
					MatTM(3,0) = 3;
					MatTM(3,1) = 3;
					MatTM(3,2) = 0;

					irr::core::matrix4 localMat;
					localMat.setRotationDegrees(irr::core::vector3df(-90,0,0));

					MatTM = MatTM * localMat;

					pVideo->setTransform(irr::video::ETS_WORLD,MatTM);

					irr::u32 i;
					for(i=0;i<pMesh->getMeshBufferCount();i++)
					{
						pVideo->drawMeshBuffer(pMesh->getMeshBuffer(i));
					}

				}

				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}

	//빌보드 텍스트씬노드 & 텍스트 씬노드
	namespace _06
	{
		//기본 A1형
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9
				//irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			irr::scene::ICameraSceneNode *pCam = pSmgr->addCameraSceneNodeMaya();			
			pCam->setTarget(irr::core::vector3df(0,0,0));

			irr::gui::IGUIFont* pFont = pDevice->getGUIEnvironment()->getFont("lucida.xml");

			//텍스트 빌보드 만들기
			pSmgr->addBillboardTextSceneNode(pFont,
				L"HP: 100/300",
				0,
				irr::core::dimension2df(20,10), //빌보드크기
				irr::core::vector3df(0,10,0), //위치
				-1,
				irr::video::SColor(255,255,0,0), //상단컬러
				irr::video::SColor(255,255,0,0) //하단 컬러
				);	

			//텍스트 씬노드
			pSmgr->addTextSceneNode(pFont,
				L"HP: 100/300",
				irr::video::SColor(255,255,0,0),
				0,
				irr::core::vector3df(0,-10,0), //위치
				-1
				);

			//큐브 노드 만들기
			{
				irr::scene::ISceneNode *pNode =  pSmgr->addCubeSceneNode();				

				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);
			}


			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}

	//노말멥핑	
	namespace _07
	{
		//기본 A1형
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			//pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,10,-20), irr::core::vector3df(0,0,0));

			irr::scene::ICameraSceneNode *pCam = pSmgr->addCameraSceneNodeMaya();			
			pCam->setTarget(irr::core::vector3df(0,0,0));

			{//노말멥

				//irr::scene::IAnimatedMesh *pMesh =  m_pSmgr->addSphereMesh("Sphere",50);
				irr::scene::ISceneNode* pNode = 0;

				//힐플래인 메쉬 추가
				irr::scene::IAnimatedMesh *pMesh =
					pSmgr->addHillPlaneMesh("usr/mesh/myhill",
					irr::core::dimension2d<irr::f32>(8,8),
					irr::core::dimension2d<irr::u32>(8,8),
					0,0,
					irr::core::dimension2d<irr::f32>(0,0),
					irr::core::dimension2d<irr::f32>(8,8)
					);				

				irr::video::ITexture* pColorMap = pVideo->getTexture("rockwall.bmp");
				irr::video::ITexture* pNormalMap = pVideo->getTexture("rockwall_height.bmp");
				pVideo->makeNormalMapTexture(pNormalMap, 3.0f);

				//탄젠트메쉬 생성
				irr::scene::IMesh* pTangentMesh = pSmgr->getMeshManipulator()->createMeshWithTangents(pMesh->getMesh(0));				

				pNode = pSmgr->addMeshSceneNode(pTangentMesh);

				pNode->setMaterialTexture(0, pColorMap);
				pNode->setMaterialTexture(1, pNormalMap);

				//pNode->getMaterial(0).SpecularColor.set(0,0,0,0);				
				//pNode->getMaterial(0).MaterialTypeParam = 0.035f; // adjust height for parallax effect
				
				pNode->setMaterialType(irr::video::EMT_NORMAL_MAP_SOLID);


				////탄젠트 메쉬등록
				irr::scene::SAnimatedMesh *pAniMesh = new irr::scene::SAnimatedMesh(pTangentMesh);
				pTangentMesh->drop();				
				pAniMesh->recalculateBoundingBox();
				pSmgr->getMeshCache()->addMesh("usr/mesh/TangentMesh",pAniMesh); //씬메니져에 등록
				pAniMesh->drop();								
				
			}

			


			{	

				irr::scene::ILightSceneNode *pLightNode;
				pLightNode = pSmgr->addLightSceneNode(0, 
					irr::core::vector3df(0,0,0),
					irr::video::SColorf(1.0f, 0.6f, 0.7f, 1.0f),10
				);

				//pLightNode->setPosition(irr::core::vector3df(1,8,0));			

				// attach billboard to light
				irr::scene::IBillboardSceneNode *pNode;
				pNode = pSmgr->addBillboardSceneNode(pLightNode, irr::core::dimension2d<irr::f32>(1, 1));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING, false);
				pNode->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);
				pNode->setMaterialTexture(0, pVideo->getTexture("particlewhite.bmp"));

				irr::scene::ISceneNodeAnimator *pAnim = 
					pSmgr->createFlyCircleAnimator(irr::core::vector3df(0,8,0),
					5);
				pLightNode->addAnimator(pAnim);
				pAnim->drop();


			}	

			//pSmgr->saveScene("nmtest.irr");

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}

	//애니메이션메쉬의 노말멥
	//애니메이트 메쉬 탄젠트 메쉬로 바꾸기
	namespace _08
	{
		//기본 A1형
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			//pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,10,-20), irr::core::vector3df(0,0,0));

			irr::scene::ICameraSceneNode *pCam = pSmgr->addCameraSceneNodeMaya();			
			pCam->setTarget(irr::core::vector3df(0,0,0));			

			//애니메이션 메쉬
			{		
				irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(
					pSmgr->getMesh("walkers/ninja/ninja.b3d") 
					);
				
				pNode->setFrameLoop(206,250);//대기동작
				pNode->setAnimationSpeed(30);
				pNode->setMaterialTexture(0,pVideo->getTexture("walkers/ninja/nskingr.jpg"));	

				irr::video::ITexture* pNormalMap = pVideo->getTexture("rockwall_height.bmp");
				pVideo->makeNormalMapTexture(pNormalMap, 3.0f);
				pNode->setMaterialTexture(1, pNormalMap); //노멀멥 세팅

				//pNode->getMaterial(0).SpecularColor.set(255,255,255,255);								
				pNode->setMaterialType(irr::video::EMT_NORMAL_MAP_SOLID);

				//애니메이트 메쉬를 탄젠트 메쉬로 바꾸기
				irr::scene::ISkinnedMesh *pMesh = (irr::scene::ISkinnedMesh *)pNode->getMesh();
				pMesh->convertMeshToTangents();

				pNode->setName("usr/scene/b3d/ninja/1");	
			}


			{	

				irr::scene::ILightSceneNode *pLightNode;
				pLightNode = pSmgr->addLightSceneNode(0, 
					irr::core::vector3df(0,0,0),
					irr::video::SColorf(1.0f, 0.6f, 0.7f, 1.0f),10
				);

				//pLightNode->setPosition(irr::core::vector3df(1,8,0));			

				// attach billboard to light
				irr::scene::IBillboardSceneNode *pNode;
				pNode = pSmgr->addBillboardSceneNode(pLightNode, irr::core::dimension2d<irr::f32>(1, 1));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING, false);
				pNode->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);
				pNode->setMaterialTexture(0, pVideo->getTexture("particlewhite.bmp"));

				irr::scene::ISceneNodeAnimator *pAnim = 
					pSmgr->createFlyCircleAnimator(irr::core::vector3df(0,8,0),
					5);
				pLightNode->addAnimator(pAnim);
				pAnim->drop();


			}	

			//pSmgr->saveScene("nmtest.irr");

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}	


}

//using namespace irr;

/////////////////////////////////////////////////
//
//쉐이더
//
/////////////////////////////////////////////////


class Lsn15_App :  public irr::IEventReceiver, public irr::video::IShaderConstantSetCallBack
{
public:

	irr::IrrlichtDevice *m_pDevice;

	irr::video::IVideoDriver *m_pVideo;
	irr::scene::ISceneManager *m_pSmgr;
	irr::gui::IGUIEnvironment *m_pGuiEnv;	

	Lsn15_App()
	{		
		m_pDevice = irr::createDevice(					
			irr::video::EDT_DIRECT3D9/*,
#if(IRR_VERSION == 16)
		irr::core::dimension2d<irr::u32>(640, 480), 
#else
		irr::core::dimension2d<irr::s32>(640, 480), 
#endif
			32,
			false, true, true,
			this*/
			);
		m_pVideo = m_pDevice->getVideoDriver();
		m_pSmgr = m_pDevice->getSceneManager();
		m_pGuiEnv = m_pDevice->getGUIEnvironment();		

		irr::scene::ICameraSceneNode* pCamera = m_pSmgr->addCameraSceneNode();
		pCamera->setPosition(irr::core::vector3df(0,10,-150));	


		irr::c8* vsFileName = 0; // filename for the vertex shader
		irr::c8* psFileName = 0; // filename for the pixel shader
	
		//psFileName = "../../media/d3d9.hlsl";
		//vsFileName = psFileName; // both shaders are in the same file

		psFileName = "../res/shader/exam2.hlsl";
		vsFileName = "../res/shader/exam2.hlsl";

		{
			// create materials

			irr::video::IGPUProgrammingServices* gpu = m_pVideo->getGPUProgrammingServices();
			irr::s32 newMaterialType1 = 0;
			irr::s32 newMaterialType2 = 0;

			if (gpu)
			{	
				// create material from high level shaders (hlsl or glsl)
				{
					newMaterialType1 = gpu->addHighLevelShaderMaterialFromFiles(
						vsFileName, "vsMain", irr::video::EVST_VS_2_0,
						psFileName, "psMain", irr::video::EPST_PS_2_0,
						this, irr::video::EMT_SOLID);					
				}				
				
				// create test scene node 1, with the new created material type 1
				{
					irr::scene::ISceneNode* node = m_pSmgr->addSphereSceneNode(50);
					node->setPosition(irr::core::vector3df(0,0,0));
					node->setMaterialTexture(0, m_pVideo->getTexture("../res/wall.bmp"));
					node->setMaterialTexture(1, m_pVideo->getTexture("../res/wall.jpg"));
					node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
					node->setMaterialType((irr::video::E_MATERIAL_TYPE)newMaterialType1);					

					//애니메이터붙이기
					irr::scene::ISceneNodeAnimator* anim = m_pSmgr->createRotationAnimator(
						irr::core::vector3df(0,0.3f,0));
					node->addAnimator(anim);
					anim->drop();
				}
			}
		}

		



	}

	~Lsn15_App()
	{
		m_pDevice->drop();
	}	

	//이벤트 핸들러
	virtual bool OnEvent(const irr::SEvent& event)
	{
		switch(event.EventType)
		{
		case irr::EET_GUI_EVENT:
			{

			}
			break;
		case irr::EET_KEY_INPUT_EVENT:
			{
			}
			break;
		case irr::EET_MOUSE_INPUT_EVENT:
			{

			}
			break;
		case irr::EET_USER_EVENT:
			break;
		}
		return false;
	}	


	//쉐이더콜백함수
	virtual void OnSetConstants(irr::video::IMaterialRendererServices* services, irr::s32 userData)
	{
		irr::video::IVideoDriver* driver = services->getVideoDriver();		

		// set inverted world matrix
		// if we are using highlevel shaders (the user can select this when
		// starting the program), we must set the constants by name.

		//irr::core::matrix4 invWorld = driver->getTransform(irr::video::ETS_WORLD);
		//invWorld.makeInverse();		
		//services->setVertexShaderConstant("mInvWorld", invWorld.pointer(), 16);// 역행렬설정

		//// set clip matrix
		irr::core::matrix4 worldViewProj;		
		worldViewProj = driver->getTransform(irr::video::ETS_PROJECTION);			
		worldViewProj *= driver->getTransform(irr::video::ETS_VIEW);
		worldViewProj *= driver->getTransform(irr::video::ETS_WORLD);
		
		services->setVertexShaderConstant("mWVP", worldViewProj.pointer(), 16);


		////// set clip matrix
		irr::core::matrix4 worldView;		
		worldView = driver->getTransform(irr::video::ETS_VIEW);
		worldView *= driver->getTransform(irr::video::ETS_WORLD);
		//
		services->setVertexShaderConstant("mWV", worldView.pointer(), 16);

		//// set camera position

		irr::core::vector3df pos = irr::core::vector3df(1,1,0);
		

		services->setVertexShaderConstant("mLightDir", reinterpret_cast<irr::f32*>(&pos), 3);

		//// set light color 

		//irr::video::SColorf col(1.0f,1.0f,1.0f,0.0f);
		//
		//services->setVertexShaderConstant("mLightColor",
		//			reinterpret_cast<irr::f32*>(&col), 4);

		//// set transposed world matrix			
		//irr::core::matrix4 world = driver->getTransform(irr::video::ETS_WORLD);
		//world = world.getTransposed();		
		//services->setVertexShaderConstant("mTransWorld", world.pointer(), 16);
	}

};


void Lession15()
{
	Lsn15_App App;	

	while(App.m_pDevice->run())
	{		
		App.m_pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

		App.m_pSmgr->drawAll();
		App.m_pGuiEnv->drawAll();
		App.m_pVideo->endScene();
	}
}