﻿
#include <irrlicht.h>
#include <assert.h>
#include <list>
#include <iostream>
#include <windows.h>
//#include <d3dx9.h>

/////////////////////////////////////////////////
//
// 씬노드 클로닝 예제
//
// '+' 복사
// '-' 복사 오브잭트 제거
/////////////////////////////////////////////////

class Lsn16_App :  public irr::IEventReceiver
{
public:

	irr::IrrlichtDevice *m_pDevice;

	irr::video::IVideoDriver *m_pVideo;
	irr::scene::ISceneManager *m_pSmgr;
	irr::gui::IGUIEnvironment *m_pGuiEnv;

	irr::s32 m_CloneCount;	

	Lsn16_App()
	{
		m_CloneCount = 0;
		m_pDevice = irr::createDevice(					
			irr::video::EDT_DIRECT3D9/*,
#if(IRR_VERSION == 16)
		irr::core::dimension2d<irr::u32>(640, 480), 
#else
		irr::core::dimension2d<irr::s32>(640, 480), 
#endif
			32,
			false, false, true,
			this*/
			);
		m_pVideo = m_pDevice->getVideoDriver();
		m_pSmgr = m_pDevice->getSceneManager();
		m_pGuiEnv = m_pDevice->getGUIEnvironment();	

		//작업디랙토리 지정
		m_pDevice->getFileSystem()->changeWorkingDirectoryTo("../res/");

		m_pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,30,-40), irr::core::vector3df(0,5,0));		
		{
			irr::scene::ISceneManager *pSmgr= m_pSmgr;
			irr::video::IVideoDriver *pVideo = m_pVideo;

			irr::scene::ISceneNode *pNode = pSmgr->addCubeSceneNode();			
			pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
			pNode->setMaterialTexture(0,pVideo->getTexture("t351sml.jpg"));
			pNode->setPosition(irr::core::vector3df(-3,0,0));

			irr::scene::ISceneNode *pNode2 = pSmgr->addEmptySceneNode();			

			irr::scene::ISceneNode *pNode3 = pSmgr->addSphereSceneNode();			
			pNode3->setMaterialFlag(irr::video::EMF_LIGHTING,false);
			pNode3->setMaterialTexture(0,pVideo->getTexture("wall.jpg"));
			pNode3->setPosition(irr::core::vector3df(15,0,0));
			

			//pNode -> pNode2 ->pNode3
			pNode2->setParent(pNode);
			pNode3->setParent(pNode2);			

			pNode->OnAnimate(0); //계층적 변환 업데이트		
			pNode->setName("Original");
			pNode->setVisible(false);

			//pNode4->OnAnimate(0);
		}
		
	}

	~Lsn16_App()
	{
		m_pDevice->drop();
	}	
	
	//이벤트 핸들러
	virtual bool OnEvent(const irr::SEvent& event)
	{
		switch(event.EventType)
		{
			case irr::EET_GUI_EVENT:
				{
					
				}
				break;
			case irr::EET_KEY_INPUT_EVENT:
				{
					switch(event.KeyInput.Key)
					{
					case irr::KEY_PLUS:
						{
							if(!event.KeyInput.PressedDown)
							{
								irr::scene::ISceneNode *pOriNode = m_pSmgr->getSceneNodeFromName("Original");
								//pOriNode->setVisible(false);
								//씬노드 클론생성 자식노드까지 모두 복사된다.
								irr::scene::ISceneNode *pCloneNode =  pOriNode->clone(m_pSmgr->getRootSceneNode());
								pCloneNode->setVisible(true);

								irr::f32 x = (rand()%100)/2.f;
								irr::f32 y = (rand()%100)/2.f;
								irr::f32 z = (rand()%100)/2.f;


								pCloneNode->setPosition(irr::core::vector3df(x,y,z));					

								char szBuf[256];
								sprintf_s(szBuf,256,"clone_%d",m_CloneCount++);							
								pCloneNode->setName(szBuf);
							}
						}
						break;
					case irr::KEY_MINUS:
						{
							if(m_CloneCount > 0)
							{
								m_CloneCount--;
								char szBuf[256];
								sprintf_s(szBuf,256,"clone_%d",m_CloneCount);
								irr::scene::ISceneNode *pCloneNode = m_pSmgr->getSceneNodeFromName(szBuf);
								pCloneNode->remove();
								//pCloneNode->setName(szBuf);
							}

						}
						break;
					}

				}
				break;
			case irr::EET_MOUSE_INPUT_EVENT:
				{

				}
				break;
			case irr::EET_USER_EVENT:
				break;
		}
		return false;
	}

	void Update()
	{
		
	}

};



void Lession16()
{
	Lsn16_App App;

	while(App.m_pDevice->run())
	{		

		App.Update();
		
		App.m_pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));
		
		App.m_pSmgr->drawAll();
		App.m_pGuiEnv->drawAll();

		App.m_pVideo->endScene();

		//std::list<int> test;
		//adv(test.begin(),3);		
		//
		////std::list<int> test;
		//doadv(test.begin(),3,
		//	std::iterator_traits< std::list<int>::iterator>::iterator_category()
		//	);
		//	

		//
		

	
	}
}

namespace lsn16
{
	//정적씬노드 클로닝
	namespace _00
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

		

			//작업디랙토리 지정
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,30,-100), irr::core::vector3df(0,5,0));		

			{				
				irr::scene::ISceneNode *pNode = pSmgr->addCubeSceneNode();			
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setMaterialTexture(0,pVideo->getTexture("t351sml.jpg"));
				pNode->setPosition(irr::core::vector3df(-3,0,0));

				irr::scene::ISceneNode *pNode2 = pSmgr->addEmptySceneNode();			

				irr::scene::ISceneNode *pNode3 = pSmgr->addSphereSceneNode();			
				pNode3->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode3->setMaterialTexture(0,pVideo->getTexture("wall.jpg"));
				pNode3->setPosition(irr::core::vector3df(15,0,0));
				
				pNode2->setParent(pNode);
				pNode3->setParent(pNode2);			

				irr::scene::ISceneNodeAnimator *pAnim;
				pAnim = pSmgr->createRotationAnimator(irr::core::vector3df(0,1,0));

				pNode2->addAnimator(pAnim);

				pAnim->drop();

				pNode->OnAnimate(0); //계층적 변환 업데이트		
				pNode->setName("usr/scene/node/Original");
				pNode->setVisible(true);				
			}
			

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				
				static int CloneCount=0;
				if((::GetAsyncKeyState('1') & 0x8001) == 0x8001)
				{
					irr::scene::ISceneNode *pOriNode = pSmgr->getSceneNodeFromName("usr/scene/node/Original");
					//pOriNode->setVisible(false);
					//씬노드 클론생성 자식노드까지 모두 복사된다.
					irr::scene::ISceneNode *pCloneNode =  pOriNode->clone(pSmgr->getRootSceneNode());
					pCloneNode->setVisible(true);

					irr::f32 x = (rand()%100) - 50.f;
					irr::f32 y = (rand()%100) - 50.f;
					irr::f32 z = (rand()%100) - 50.f;


					pCloneNode->setPosition(irr::core::vector3df(x,y,z));					

					
					char szBuf[256];
					sprintf_s(szBuf,256,"clone_%d",CloneCount++);
					pCloneNode->setName(szBuf);
				}

				if((::GetAsyncKeyState('2') & 0x8001) == 0x8001)
				{
					if(CloneCount > 0)
					{
						CloneCount--;
						char szBuf[256];
						sprintf_s(szBuf,256,"clone_%d",CloneCount);
						irr::scene::ISceneNode *pCloneNode = pSmgr->getSceneNodeFromName(szBuf);
						//pCloneNode->remove();			
						pSmgr->addToDeletionQueue(pCloneNode);					
					}
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}

	//동적씬노드 클로닝
	//IAnimateMeshSceneNode
	namespace _01
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
		

			//작업디랙토리 지정
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");		


			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,8,-10), irr::core::vector3df(0,5,0));

			{		
				irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(
					pSmgr->getMesh("walkers/ninja/ninja.b3d") 
					);
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);				
				pNode->setFrameLoop(206,250);//대기동작
				pNode->setAnimationSpeed(30);
				pNode->setMaterialTexture(0,pVideo->getTexture("walkers/ninja/nskingr.jpg"));	

				pNode->setName("usr/scene/b3d/ninja/1");	
			}
			

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				
				static int CloneCount=0;
				if((::GetAsyncKeyState('1') & 0x8001) == 0x8001)
				{
					irr::scene::ISceneNode *pOriNode = pSmgr->getSceneNodeFromName("usr/scene/b3d/ninja/1");
					//pOriNode->setVisible(false);
					//씬노드 클론생성 자식노드까지 모두 복사된다.
					irr::scene::ISceneNode *pCloneNode =  pOriNode->clone(pSmgr->getRootSceneNode());
					pCloneNode->setVisible(true);

					irr::f32 x = (rand()%10) - 5.f;
					irr::f32 y = (rand()%10) - 5.f;
					irr::f32 z = (rand()%10) - 5.f;

					pCloneNode->setPosition(irr::core::vector3df(x,y,z));			
					//((irr::scene::IAnimatedMeshSceneNode *)pCloneNode)->setFrameLoop(1,14);

					char szBuf[256];
					sprintf_s(szBuf,256,"clone_%d",CloneCount++);
					pCloneNode->setName(szBuf);
				}

				if((::GetAsyncKeyState('2') & 0x8001) == 0x8001)
				{
					if(CloneCount > 0)
					{
						CloneCount--;
						char szBuf[256];
						sprintf_s(szBuf,256,"clone_%d",CloneCount);
						irr::scene::ISceneNode *pCloneNode = pSmgr->getSceneNodeFromName(szBuf);
						pCloneNode->remove();						
					}
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}
}



