﻿#include <irrlicht.h>
#include <math.h>
//#include <d3dx9.h>

#include <iostream>
#include <vector>



/////////////////////////////////////////////////
//
//사용자 정의 씬노드
//
/////////////////////////////////////////////////

class CSampleSceneNode : public irr::scene::ISceneNode
{
	irr::core::aabbox3d<irr::f32> Box;
	irr::video::S3DVertex Vertices[4];
	irr::video::SMaterial Material;

public:
	CSampleSceneNode(irr::scene::ISceneNode* parent, irr::scene::ISceneManager* mgr, irr::s32 id)
		: irr::scene::ISceneNode(parent, mgr, id)
	{ 

		Material.Wireframe = false;
		Material.Lighting = false;		

		Vertices[0] = irr::video::S3DVertex(-.5,-.5,0, 0,0,-1,irr::video::SColor(0,0,255,255),0,1);
		Vertices[1] = irr::video::S3DVertex(-.5,.5,0, 0,0,-1,irr::video::SColor(0,255,0,255),0,0);
		Vertices[2] = irr::video::S3DVertex(.5,.5,0, 0,0,-1,irr::video::SColor(0,255,255,0),1,0);
		Vertices[3] = irr::video::S3DVertex(.5,-.5,0, 0,0,-1,irr::video::SColor(0,0,255,0),1,1);

		Box.reset(Vertices[0].Pos); 		
		for (irr::s32 i=1; i<4; ++i) 			
			Box.addInternalPoint(Vertices[i].Pos);
	}

	virtual void OnRegisterSceneNode(){  
		if (IsVisible)    
			SceneManager->registerNodeForRendering(this);
		ISceneNode::OnRegisterSceneNode();
	}

	/*virtual void render()
	{  
	irr::u16 indices[] = { 0,2,3, 2,1,3, 1,0,3, 2,0,1 };

	irr::video::IVideoDriver* driver = SceneManager->getVideoDriver();
	driver->setMaterial(Material);
	driver->setTransform(irr::video::ETS_WORLD, AbsoluteTransformation);
	driver->drawIndexedTriangleList(&Vertices[0], 4, &indices[0], 4);
	}*/

	virtual void render()
	{  	
		if (IsVisible == false) return;

		irr::u16 indices[] = { 0,1,2,3,0,2 };
		irr::video::IVideoDriver* driver = SceneManager->getVideoDriver();
		driver->setMaterial(Material);			

		irr::core::matrix4 _TempTrans;
		_TempTrans = driver->getTransform(irr::video::ETS_WORLD);

		driver->setTransform(irr::video::ETS_WORLD, AbsoluteTransformation);			
		driver->drawIndexedTriangleList(&Vertices[0], 4, &indices[0], 2);		

		if(DebugDataVisible)
		{//디버깅정보출력
			irr::video::SMaterial _Material;


			_Material.Lighting = false;
			_Material.Wireframe = false;				

			driver->setMaterial(_Material);			
			irr::core::aabbox3df box1 = getBoundingBox();			
			driver->draw3DBox(box1);				

		}
		driver->setTransform(irr::video::ETS_WORLD,_TempTrans);		//월드변환복귀	
	}

	virtual const irr::core::aabbox3d<irr::f32>& getBoundingBox() const  
	{    
		return Box;  
	}

	virtual irr::u32 getMaterialCount() const
	{
		return 1;
	}

	virtual irr::video::SMaterial& getMaterial(irr::u32 num)
	{
		return Material;

	}

};

void Lession18()
{
	irr::IrrlichtDevice *device = 
		irr::createDevice(irr::video::EDT_DIRECT3D9/*, 

#if(IRR_VERSION == 16)
		irr::core::dimension2d<irr::u32>(640, 480), 
#else
		irr::core::dimension2d<irr::s32>(640, 480), 
#endif


		16, false*/);

	device->setWindowCaption(L"Custom Scene Node - Irrlicht Engine Demo");

	irr::video::IVideoDriver* driver = device->getVideoDriver();
	irr::scene::ISceneManager* smgr = device->getSceneManager();
	irr::scene::ICameraSceneNode *pCam =  smgr->addCameraSceneNodeMaya();
	pCam->setPosition(irr::core::vector3df(0,0,-50));
	pCam->setTarget(irr::core::vector3df(0,0,0));

	//사용자 노드생성
	CSampleSceneNode *myNode = new CSampleSceneNode(smgr->getRootSceneNode(), smgr, 666);
	myNode->drop();
	myNode->setScale(irr::core::vector3df(50,50,1));

	//택스춰 등록
	irr::video::ITexture *pTex = driver->getTexture("../res/irr_exam/irrlicht2_ft.jpg");
	myNode->setMaterialTexture(0,pTex);	

	while(device->run())  {    
		driver->beginScene(true, true, irr::video::SColor(0,100,100,100));
		smgr->drawAll();
		driver->endScene();
	}

	device->drop();
	//return 0;

}


