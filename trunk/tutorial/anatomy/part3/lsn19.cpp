#include "irrlicht.h"

// 방향성 라이트 사용법예제

void Lession19()
{
	irr::IrrlichtDevice *device =
		irr::createDevice(irr::video::EDT_DIRECT3D9/*, 
#if(IRR_VERSION == 16)
		irr::core::dimension2d<irr::u32>(640, 480), 
#else
		irr::core::dimension2d<irr::s32>(640, 480), 
#endif
		32, false, true, true			*/
		);
	device->setWindowCaption(L"Directional Light Tutorial");

	irr::video::IVideoDriver* pVideo = device->getVideoDriver();
	irr::scene::ISceneManager* pSmgr = device->getSceneManager();	

	irr::scene::ICameraSceneNode *pCam =  pSmgr->addCameraSceneNodeFPS();	

	pCam->setPosition(irr::core::vector3df(0,50,-80));
	pCam->setTarget(irr::core::vector3df(0,0,0));	
	pCam->updateAbsolutePosition();

	irr::scene::ISceneManager* m_pSmgr =pSmgr;
	irr::video::IVideoDriver* m_pVideo = pVideo;
	irr::scene::ISceneManager* smgr = pSmgr;


	{
		irr::scene::ISceneNode* pNode = 0;
		irr::scene::IAnimatedMesh* pMesh;			

		pMesh = pSmgr->addHillPlaneMesh("myHill",
			irr::core::dimension2d<irr::f32>(40,40),
			irr::core::dimension2d<irr::u32>(10,10), 
			0,0,
			irr::core::dimension2d<irr::f32>(0,0),
			irr::core::dimension2d<irr::f32>(10,10));

		pNode = pSmgr->addAnimatedMeshSceneNode(pMesh);
		pNode->setPosition(irr::core::vector3df(0,0,0));
		pNode->setMaterialTexture(0, pVideo->getTexture("../res/wall.jpg"));		
	}

	{
		//방향성 라이트추가하기

		irr::scene::ILightSceneNode *pLightNode;
		pLightNode = pSmgr->addLightSceneNode(
			0, 
			irr::core::vector3df(0,0,0),
			irr::video::SColorf(1.0f, 1.0f, 1.f, 1.0f), 
			0.0f);		

		//방향성라이트는 기본적으로 0,0,1방향만 가리키며
		//SLight의 Direction멥버 변수는 랜더링 루푸에서 항상 0,0,1로 재초기화 되므로
		//pLightNode->getLightData().Direction = vector3df(-1,0,0); 이런식으로
		//사용자가 임의방향을 넣어 주는것은 아무런 의미가 없다.
		//방향을 바꾸고싶으면 회전을 수행해야한다.

		pLightNode->getLightData().Type = irr::video::ELT_DIRECTIONAL; 
		pLightNode->getLightData().DiffuseColor.set(1.f, 1.f, 1.f);

		pLightNode->setPosition(irr::core::vector3df(-30,30,0));

		//방향을 바꿈
		pLightNode->setRotation(
			irr::core::vector3df(45.0f,-45,0)
			);

		pLightNode->setName("LightNode");

		{
			irr::scene::IAnimatedMesh *pAniMesh =  pSmgr->addArrowMesh("arrow",
				irr::video::SColor(0,255,0,0),
				irr::video::SColor(0,0,255,0),
				4,4,10,7,5,5			
				);

			//라이트로드를 부모로 붙임
			irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(pAniMesh,pLightNode);
			pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);		
			pNode->setPosition(irr::core::vector3df(0,10,0));

			pNode->setRotation(
				irr::core::vector3df(90.0f,0,0)
				);

		}

		//회전 애니메이터
		{
			irr::scene::ISceneNodeAnimator* anim = 0;
			anim = pSmgr->createRotationAnimator(irr::core::vector3df(0,1.f,0));
			pLightNode->addAnimator(anim);
			anim->drop();
		}

		/*

		결론:
		1.4.1기준으로 아직 방향성광원은 일부만 쓸수있다.
		방향성 광원이지만 그림자를 계산할때는 광원의 위치로계산하기때문에 그림자용 광원으로 쓸수없다.(앞뒤가 안맞음)
		기본 그림자는 그다지 효용성이 없으므로 새로 만들어서 사용해야한다.

		*/
	}


	//요정 생성
	{
		irr::scene::IAnimatedMesh *pAniMesh = pSmgr->getMesh("../res/faerie.md2");
		irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(pAniMesh);
		pNode->setMD2Animation ( irr::scene::EMAT_STAND );				
		pNode->setMaterialTexture(0,pVideo->getTexture("../res/faerie2.bmp"));	
		pNode->setPosition(irr::core::vector3df(0,25,0));
		pNode->setMaterialFlag(irr::video::EMF_NORMALIZE_NORMALS,true);
		pNode->getMaterial(0).Shininess = 8;
		pNode->getMaterial(0).SpecularColor = irr::video::SColor(255,255,255,255);

		// add shadow
		/*pNode->addShadowVolumeSceneNode();
		pSmgr->setShadowColor(irr::video::SColor(150,0,0,0));*/

	}



	//(0,0,1)바라보는 화살표
	{
		irr::scene::IAnimatedMesh *pAniMesh =  pSmgr->addArrowMesh("arrow",
			irr::video::SColor(0,255,0,0),
			irr::video::SColor(0,0,255,0),
			4,4,10,7,5,5			
			);
		irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(pAniMesh);
		pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);		
		pNode->setPosition(irr::core::vector3df(0,10,0));

		pNode->setRotation(
			irr::core::vector3df(90.0f,0,0)
			);

	}	

	while(device->run())  {    
		//pCam->setPosition(irr::core::vector3df(0,50,-80));
		//pCam->setTarget(irr::core::vector3df(0,0,0));	
		pVideo->beginScene(true, true, irr::video::SColor(0,100,100,100));
		pSmgr->drawAll();
		pVideo->endScene();
	}

	device->drop();

}

namespace lsn19
{
	namespace _00
	{
		class CTestMaterialRenderer : public irr::video::IMaterialRenderer
		{
		public:

			//! Constructor
			CTestMaterialRenderer(irr::video::IVideoDriver* driver)
				: Driver(driver)
			{
				pID3DDevice = Driver->getExposedVideoData().D3D9.D3DDev9;
			}

			~CTestMaterialRenderer()
			{
			}

			//! sets a variable in the shader.
			//! \param vertexShader: True if this should be set in the vertex shader, false if
			//! in the pixel shader.
			//! \param name: Name of the variable
			//! \param floats: Pointer to array of floats
			//! \param count: Amount of floats in array.
			virtual bool setVariable(bool vertexShader, const irr::c8* name, const irr::f32* floats, int count)
			{
				//os::Printer::log("Invalid material to set variable in.");
				return false;
			}

			virtual bool isTransparent() const { return true; }


		protected:

			IDirect3DDevice9* pID3DDevice;
			irr::video::IVideoDriver* Driver;
		};



		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9
				//irr::video::EDT_OPENGL
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			irr::video::IMaterialRenderer* renderer = new CTestMaterialRenderer(pVideo);
			irr::s32 newMatrialType = pVideo->addMaterialRenderer(renderer,"CTestMaterialRenderer" );
			renderer->drop();			

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,10,-20), irr::core::vector3df(0,0,0));

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}
}