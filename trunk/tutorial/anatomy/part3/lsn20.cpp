﻿#include <irrlicht.h>
#include <assert.h>

//고급 커스텀 애니메이터(활동자) 예제


class IMyAnimatorEndCallBack : public virtual irr::IReferenceCounted
{
public:
	
	virtual void OnAnimationEnd(irr::scene::ISceneNode *Parent) = 0;
};

class CMySceneNodeAnimatorFlyStraight : public irr::scene::ISceneNodeAnimator {

private:	

	void recalculateImidiateValues()
	{
		Vector = End - Start;
		WayLength = (irr::f32)Vector.getLength();
		Vector.normalize();

		TimeFactor = WayLength / TimeForWay;
	}

		irr::core::vector3df Start;
		irr::core::vector3df End;
		irr::core::vector3df Vector;
		irr::f32 WayLength;
		irr::f32 TimeFactor;
		irr::u32 StartTime;
		irr::u32 TimeForWay;
		IMyAnimatorEndCallBack *m_pEndCallBack;
		bool Loop;
public:
	CMySceneNodeAnimatorFlyStraight(const irr::core::vector3df& startPoint,
		const irr::core::vector3df& endPoint, irr::u32 timeForWay,
		bool loop, irr::u32 now,IMyAnimatorEndCallBack *pEndCallBack)
		: Start(startPoint), End(endPoint), WayLength(0.0f), TimeFactor(0.0f), StartTime(now), TimeForWay(timeForWay), Loop(loop)
	{
#ifdef _DEBUG
		setDebugName("CMySceneNodeAnimatorFlyStraight");
#endif

		m_pEndCallBack = pEndCallBack;
		recalculateImidiateValues();
	}
	//! destructor
	virtual ~CMySceneNodeAnimatorFlyStraight()
	{
	}


	//! animates a scene node
	void animateNode(irr::scene::ISceneNode* node, irr::u32 timeMs)
	{
		if (!node)
			return;

		irr::u32 t = (timeMs-StartTime);

		irr::core::vector3df pos = Start;

		if (!Loop && t >= TimeForWay)
		{			
			if(m_pEndCallBack)
			{
				m_pEndCallBack->OnAnimationEnd(node);
			}
			pos = End;
		}
		else
			pos += Vector * (irr::f32)fmod((irr::f32)t, (irr::f32)TimeForWay) * TimeFactor;
		node->setPosition(pos);
	}

	irr::scene::ISceneNodeAnimator* createClone(irr::scene::ISceneNode* node, irr::scene::ISceneManager* newManager=0)
	{
		return NULL;
	}


	//! Writes attributes of the scene node animator.
	void serializeAttributes(irr::io::IAttributes* out, irr::io::SAttributeReadWriteOptions* options) const
	{
		out->addVector3d("Start", Start);
		out->addVector3d("End", End);
		out->addInt("TimeForWay", TimeForWay);
		out->addBool("Loop", Loop);
	}


	//! Reads attributes of the scene node animator.
	void deserializeAttributes(irr::io::IAttributes* in, irr::io::SAttributeReadWriteOptions* options)
	{
		Start = in->getAttributeAsVector3d("Start");
		End = in->getAttributeAsVector3d("End");
		TimeForWay = in->getAttributeAsInt("TimeForWay");
		Loop = in->getAttributeAsBool("Loop");

		recalculateImidiateValues();
	}

	void setCallBack(IMyAnimatorEndCallBack *pCallBack)
	{
		m_pEndCallBack = pCallBack;
	}
};

//콜백구현
class CMyAnimatorEndCallback : public IMyAnimatorEndCallBack
{
	void OnAnimationEnd(irr::scene::ISceneNode *Parent)
	{
		printf("%s \n",Parent->getName());
		//Parent->removeAnimators();
	} 

};
CMyAnimatorEndCallback testCallBack;

//도우미함수
irr::scene::ISceneNodeAnimator* createMyFlyStraightAnimator(
	irr::scene::ISceneManager *pSmgr, 
	const irr::core::vector3df& startPoint,
	const irr::core::vector3df& endPoint, 
	irr::u32 timeForWay, bool loop,											  
	irr::u32 now,
	IMyAnimatorEndCallBack *pEndCallBack)
{
	irr::scene::ISceneNodeAnimator* anim = 
		new CMySceneNodeAnimatorFlyStraight(startPoint,
		endPoint, 
		timeForWay, 
		loop,now, 
		pEndCallBack);
	return anim;
}


void Lession20()
{
	irr::IrrlichtDevice *pDevice = irr::createDevice(					
			irr::video::EDT_DIRECT3D9/*,
#if(IRR_VERSION == 16)
		irr::core::dimension2d<irr::u32>(640, 480), 
#else
		irr::core::dimension2d<irr::s32>(640, 480), 
#endif
			32,
			false, false, true,
			NULL*/
			);

	pDevice->setWindowCaption(L"lesson 20");

	irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
	irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
	irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

	pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,30,-140), irr::core::vector3df(0,5,0));	

	
	irr::scene::IAnimatedMesh *pAniArrow =  pSmgr->addArrowMesh("arrow",
			irr::video::SColor(255,255,0,0),
			irr::video::SColor(255,0,0,255),
			4,8,6,4,3,3			
			);
	
	irr::core::vector3df StartPoint(100,0,0);
	irr::core::vector3df EndPoint(-100,0,0);
	irr::core::vector3df Point1(-50,50,0);
	
	irr::scene::IAnimatedMeshSceneNode *pArrowNode1= pSmgr->addAnimatedMeshSceneNode(pAniArrow);	
	pArrowNode1->setMaterialFlag(irr::video::EMF_LIGHTING,false);
	pArrowNode1->setPosition(StartPoint);
	pArrowNode1->setName("test node");
	
	{//직선이동
		irr::scene::ISceneNodeAnimator* pAnim =createMyFlyStraightAnimator(pSmgr,
			StartPoint,	//시작위치
			EndPoint, //끝위치
			3000,	//수행시간
			false,	//루핑여부			
			pDevice->getTimer()->getTime(),
			&testCallBack
			);		
		pArrowNode1->addAnimator(pAnim);				
		pAnim->drop();		
	}		

	irr::u32 uLastTick = pDevice->getTimer()->getTime();	

	while(pDevice->run())
	{	
		irr::u32 uTick = pDevice->getTimer()->getTime();
		irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f;		

		pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));	
		{
			irr::core::matrix4 mat;//단위행렬로초기화
			pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화
		}
		
		pSmgr->drawAll();
		pGuiEnv->drawAll();			
		
		pVideo->endScene();	

		uLastTick = pDevice->getTimer()->getTime();
	}

	pDevice->drop();
}
