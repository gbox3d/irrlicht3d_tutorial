﻿
#include <irrlicht.h>
#include <assert.h>


//고급 씬 그래프 관리의 이해
//하위 모든 자식 노드 순회하는 방식
//인자로 촤상위노드(루트)를 넣는다.
void RecursiveUpdateAbsoutePosition_TopDown(irr::scene::ISceneNode* pNode) 
{ 
	if (!pNode ) 
		return; 

	// Do something with the node here. 
	pNode->updateAbsolutePosition();

	// recursive call to children 
	const irr::core::list<irr::scene::ISceneNode*> & children = pNode->getChildren(); 
	irr::core::list<irr::scene::ISceneNode*>::ConstIterator it = children.begin(); 
	for (; it != children.end(); ++it) 
	{ 
		RecursiveUpdateAbsoutePosition_TopDown(*it); 
	} 
} 

//상위 직계 존속만 찾아 올라가는 방식
void RecursiveUpdateAbsoutePosition_BottomUp(irr::scene::ISceneNode* pNode) 
{ 
	if (!pNode ) 
		return; 

	if(pNode->getParent())
	{
		RecursiveUpdateAbsoutePosition_BottomUp(pNode->getParent());
	}

	// Do something with the node here. 
	pNode->updateAbsolutePosition();
} 

//씬노드의 월드 변환행렬구하기.
irr::core::matrix4 GetWorldTransformation(irr::scene::ISceneNode* pNode)
{
	irr::scene::ISceneNode *par = pNode->getParent();

	if(par)
	{
		irr::core::vector3df pos;
		irr::core::matrix4 mat = par->getRelativeTransformation();

		par = par->getParent();
		while(par)
		{
			mat = par->getRelativeTransformation() * mat;
			par = par->getParent();

		}

		return mat;
		//mat.transformVect(pos,pNode->getPosition());   
		//return pos;
	}
	else
		return pNode->getRelativeTransformation();
}

//월드 좌표구하기
irr::core::vector3df GetWorldPosition(irr::scene::ISceneNode* pNode)
{
	irr::core::vector3df pos;
	GetWorldTransformation(pNode).transformVect(pos,pNode->getPosition());  
	return pos;
	/*irr::scene::ISceneNode *par = pNode->getParent();

	if(par)
	{
		irr::core::vector3df pos;
		irr::core::matrix4 mat = par->getRelativeTransformation();

		par = par->getParent();
		while(par)
		{
			mat = par->getRelativeTransformation() * mat;
			par = par->getParent();

		}

		mat.transformVect(pos,pNode->getPosition());   

		return pos;
	}
	else

		return pNode->getPosition();*/
}





void Lession21()
{
	irr::IrrlichtDevice *pDevice = irr::createDevice(					
		irr::video::EDT_NULL/*,
#if(IRR_VERSION == 16)
		irr::core::dimension2d<irr::u32>(640, 480), 
#else
		irr::core::dimension2d<irr::s32>(640, 480), 
#endif
			32,
			false, false, true,
			NULL*/
			);

	//창이름정하기
	pDevice->setWindowCaption(L"hello irrlicht 3d engine");
	//비디오객체얻기
	irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
	//씬매니져 얻기
	irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
	//GUI객체얻기
	irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

	irr::scene::ISceneNode *pNode[6];
	pNode[0] = pSmgr->addEmptySceneNode();
	
	pNode[1] = pSmgr->addEmptySceneNode();
	pNode[1]->setParent(pNode[0]);
	pNode[1]->setPosition(irr::core::vector3df(0,1,0));

	pNode[2] = pSmgr->addEmptySceneNode();
	pNode[2]->setParent(pNode[1]);
	pNode[2]->setPosition(irr::core::vector3df(0,1,0));

	pNode[3] = pSmgr->addEmptySceneNode();
	pNode[3]->setParent(pNode[2]);
	pNode[3]->setPosition(irr::core::vector3df(0,1,0));

	pNode[4] = pSmgr->addEmptySceneNode();
	pNode[4]->setParent(pNode[3]);
	pNode[4]->setPosition(irr::core::vector3df(0,1,0));

	pNode[5] = pSmgr->addEmptySceneNode();
	pNode[5]->setParent(pNode[2]);
	pNode[5]->setPosition(irr::core::vector3df(0,3,0));


	irr::core::vector3df world_pos = GetWorldPosition(pNode[5]);

	printf("%.1f,%.1f,%.1f\n",
		world_pos.X,
		world_pos.Y,
		world_pos.Z
		);

	
	printf("Before \n");
	printf("%.1f,%.1f,%.1f\n",
		pNode[4]->getAbsolutePosition().X,
		pNode[4]->getAbsolutePosition().Y,
		pNode[4]->getAbsolutePosition().Z
		);
	printf("%.1f,%.1f,%.1f\n",
		pNode[5]->getAbsolutePosition().X,
		pNode[5]->getAbsolutePosition().Y,
		pNode[5]->getAbsolutePosition().Z
		);	

	printf("After \n");
	
	RecursiveUpdateAbsoutePosition_TopDown(pNode[0]); //최상위 노드
	RecursiveUpdateAbsoutePosition_BottomUp(pNode[4]); //절대위치를 얻고 싶은 노드

	printf("%.1f,%.1f,%.1f\n",
		pNode[4]->getAbsolutePosition().X,
		pNode[4]->getAbsolutePosition().Y,
		pNode[4]->getAbsolutePosition().Z
		);	

	printf("%.1f,%.1f,%.1f\n",
		pNode[5]->getAbsolutePosition().X,
		pNode[5]->getAbsolutePosition().Y,
		pNode[5]->getAbsolutePosition().Z
		);	
	
	//pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,30,-40), irr::core::vector3df(0,5,0));

	/*while(pDevice->run())
	{
		pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));	
		pSmgr->drawAll();
		pGuiEnv->drawAll();					
		pVideo->endScene();	
	}*/

	pDevice->drop();
}