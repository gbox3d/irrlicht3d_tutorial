﻿
#include <irrlicht.h>
#include <assert.h>




/////////////////////////////////////////////////
//
//법선벡터 이용해서 지면과 각도맞추기 예제
//
/////////////////////////////////////////////////

class Lsn22_App :  public irr::IEventReceiver
{
public:

	irr::IrrlichtDevice *m_pDevice;

	irr::video::IVideoDriver *m_pVideo;
	irr::scene::ISceneManager *m_pSmgr;
	irr::gui::IGUIEnvironment *m_pGuiEnv;

	bool m_bUseQuat;

	Lsn22_App()
	{
		{
			int sel;
			printf("use quaternion? 0:no, 1:yes: ");
			scanf_s("%d",&sel);
			
			if(sel)
				m_bUseQuat = true;
			else
				m_bUseQuat = false;
		}

		m_pDevice = irr::createDevice(					
			irr::video::EDT_DIRECT3D9/*,
#if(IRR_VERSION == 16)
		irr::core::dimension2d<irr::u32>(640, 480), 
#else
		irr::core::dimension2d<irr::s32>(640, 480), 
#endif
			32,
			false, false, true,
			this*/
			);
		m_pVideo = m_pDevice->getVideoDriver();
		m_pSmgr = m_pDevice->getSceneManager();
		m_pGuiEnv = m_pDevice->getGUIEnvironment();	

		irr::scene::ISceneManager *pSmgr = m_pSmgr;
		irr::video::IVideoDriver *pVideo = m_pVideo;

		//창이름정하기
		m_pDevice->setWindowCaption(L"lesson 22. ground normal");


		irr::scene::ICameraSceneNode *pCam =  m_pSmgr->addCameraSceneNode();
		pCam->setPosition(irr::core::vector3df(0,180,-180));
		pCam->setTarget(irr::core::vector3df(0,0,0));	
		//pCam->updateAbsolutePosition();

		{
			irr::scene::ISceneNode *pRoot = m_pSmgr->addEmptySceneNode();
			pRoot->setName("Arrow");		

			irr::scene::IAnimatedMesh *pAniMesh = pSmgr->getMesh("../res/jga/sign.3DS");
			irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(pAniMesh,pRoot);
			pNode->setMaterialTexture(0, pVideo->getTexture("../res/jga/sign.tga"));
			pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
			pNode->setRotation(irr::core::vector3df(90,0,0));
		}


		{
			irr::scene::ISceneNode* pNode = 0;
			irr::scene::IAnimatedMesh* pMesh;			

			pMesh = m_pSmgr->addHillPlaneMesh("myHill",
				irr::core::dimension2d<irr::f32>(32,32),
				irr::core::dimension2d<irr::u32>(16,16), 
				0,50,
				irr::core::dimension2d<irr::f32>(2,2),
				irr::core::dimension2d<irr::f32>(10,10));

			pNode = m_pSmgr->addAnimatedMeshSceneNode(pMesh);
			pNode->setPosition(irr::core::vector3df(0,0,0));
			pNode->setMaterialTexture(0, m_pVideo->getTexture("../res/wall.jpg"));		
			pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
			pNode->setName("ground");

			irr::scene::ITriangleSelector *pSelector;
			pSelector = pSmgr->createOctreeTriangleSelector(pMesh->getMesh(0),pNode);
			pNode->setTriangleSelector(pSelector);
			pSelector->drop();
		}

		
	}

	~Lsn22_App()
	{
		m_pDevice->drop();
	}	
	
	//이벤트 핸들러
	virtual bool OnEvent(const irr::SEvent& event)
	{
		switch(event.EventType)
		{
			case irr::EET_GUI_EVENT:
				{
					
				}
				break;
			case irr::EET_KEY_INPUT_EVENT:
				{
				}
				break;
			case irr::EET_MOUSE_INPUT_EVENT:
				{

				}
				break;
			case irr::EET_USER_EVENT:
				break;
		}
		return false;
	}

	void Update()
	{

		irr::core::position2di mouse_pos = m_pDevice->getCursorControl()->getPosition();

		irr::core::line3df Ray;			
		irr::core::vector3df v3Intersec;
		irr::core::triangle3df tri;
		irr::scene::ITriangleSelector *pSelector;

		//2디 스크린좌표계로부터 피킹레이 얻기
		Ray = m_pSmgr->getSceneCollisionManager()->getRayFromScreenCoordinates(mouse_pos);			

		//샐랙터얻기
		pSelector = m_pSmgr->getSceneNodeFromName("ground")->getTriangleSelector();

		if(!m_bUseQuat)
		{
//#if(IRR_VERSION >= 16)
			const irr::scene::ISceneNode *pCollNode;
			if(m_pSmgr->getSceneCollisionManager()->getCollisionPoint(Ray,pSelector,v3Intersec,tri,pCollNode))
//#else
	//		if(m_pSmgr->getSceneCollisionManager()->getCollisionPoint(Ray,pSelector,v3Intersec,tri))
//#endif
			{
				irr::scene::ISceneNode *pArrow = m_pSmgr->getSceneNodeFromName("Arrow");
				pArrow->setPosition(v3Intersec);

				irr::core::vector3df vNor = tri.getNormal().normalize();

				vNor = irr::core::vector3df(vNor.X,vNor.Z,vNor.Y);

				irr::core::vector3df vAngle = irr::core::vector3df(
					vNor.getHorizontalAngle().X,
					vNor.getHorizontalAngle().Z,
					vNor.getHorizontalAngle().Y
					);
				//결과적으로 회전축이 뒤집히게되므로. 음수로 만들어준다.
				pArrow->setRotation(-vAngle);
			}
		}
		else
		{
			//쿼터니온 응용
//#if(IRR_VERSION >= 16)
			const irr::scene::ISceneNode *pCollNode;
			if(m_pSmgr->getSceneCollisionManager()->getCollisionPoint(Ray,pSelector,v3Intersec,tri,pCollNode))
//#else
	//		if(m_pSmgr->getSceneCollisionManager()->getCollisionPoint(Ray,pSelector,v3Intersec,tri))

//#endif
			{
				irr::scene::ISceneNode *pArrow = m_pSmgr->getSceneNodeFromName("Arrow");
				irr::core::vector3df v0(0,1,0);
				irr::core::vector3df vRot;

				pArrow->setPosition(v3Intersec);

				irr::core::vector3df vNor = tri.getNormal().normalize();		

				irr::core::quaternion quat;
				quat.rotationFromTo(v0,vNor); //두벡터의 사잇각 구하기

				quat.toEuler(vRot);
				vRot *= irr::core::RADTODEG;

				pArrow->setRotation(vRot);			
			}
		}
	}

};


void Lession22()
{
	Lsn22_App App;	

	while(App.m_pDevice->run())
	{
		App.Update();
		
		App.m_pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));
		
		App.m_pSmgr->drawAll();
		App.m_pGuiEnv->drawAll();

		App.m_pVideo->endScene();
	}
}



