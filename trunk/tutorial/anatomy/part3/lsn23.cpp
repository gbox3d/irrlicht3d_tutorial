﻿
#include <irrlicht.h>
#include <assert.h>

//fps 카메라 테스트용
//

namespace lsn23 {
	namespace _00 {

		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(
				irr::video::EDT_DIRECT3D9/*,
#if(IRR_VERSION == 16)
				irr::core::dimension2d<irr::u32>(640, 480), 
#else
				irr::core::dimension2d<irr::s32>(640, 480), 
#endif
				32,
				false, false, true,
				NULL*/
				);

			pDevice->setWindowCaption(L"Adv. fps camera");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res/");



			{
				irr::scene::ISceneNode *pNode =  pSmgr->addCubeSceneNode();		

				pNode->setName("usr/scene/node/cube");
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setMaterialTexture(0,pVideo->getTexture("t351sml.jpg"));	
				pNode->setPosition(irr::core::vector3df(0,0,20));
			}

			{
				irr::scene::ISceneNode *pRoot = pSmgr->addEmptySceneNode();
				pRoot->setName("Arrow");		

				irr::scene::IAnimatedMesh *pAniMesh = pSmgr->getMesh("jga/shot_gun.md2");
				irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(pAniMesh,pRoot);
				pNode->setMaterialTexture(0, pVideo->getTexture("jga/shot_gun.jpg"));
				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				//pNode->setRotation(irr::core::vector3df(90,0,0));		
				pNode->setPosition(irr::core::vector3df(10,-10,20));
				pNode->setFrameLoop(0,0);
				pNode->setLoopMode(false);

				//pRoot->setParent(pCam);
			}

			{
				irr::scene::ICameraSceneNode *pCam = pSmgr->addCameraSceneNodeFPS(0,100,.1f);
				pCam->setName("usr/scene/camera/1");

				pCam->setPosition(irr::core::vector3df(0,20,-50));	
				pCam->setTarget(irr::core::vector3df(10,0,0));	
				pCam->updateAbsolutePosition();
				pCam->addChild(pSmgr->getSceneNodeFromName("Arrow"));
				pCam->addChild(pSmgr->getSceneNodeFromName("usr/scene/node/cube"));

			}

			//irr::core::vector3df tar =  pCam->getTarget();
			//printf("%f,%f,%f \n",tar.X,tar.Y,tar.Z);

			pDevice->getCursorControl()->setVisible(false);

			while(pDevice->run())
			{		
				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));			


				pSmgr->drawAll();
				pGuiEnv->drawAll();	

				{
					irr::scene::ICameraSceneNode *pCam = 
						static_cast<irr::scene::ICameraSceneNode *>(pSmgr->getSceneNodeFromName("usr/scene/camera/1"));
					irr::core::vector3df tar =  pCam->getPosition();
					printf("%f,%f,%f \n",tar.X,tar.Y,tar.Z);
				}



				{
					//	irr::core::matrix4 mat;//단위행렬로초기화
					//	pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

					//	irr::video::SMaterial m;
					//	m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.
					//	m.ZBuffer = false;
					//	
					//	pVideo->setMaterial(m);
					//	
					//	pVideo->draw3DLine(irr::core::vector3df(-100,0,0),irr::core::vector3df(100,0,0),irr::video::SColor(0,0,255,0));
					//	pVideo->draw3DLine(irr::core::vector3df(0,-100,0),irr::core::vector3df(0,100,0),irr::video::SColor(0,0,255,0));
					//	pVideo->draw3DLine(irr::core::vector3df(0,0,-100),irr::core::vector3df(0,0,100),irr::video::SColor(0,0,255,0));

					//	pCam->updateAbsolutePosition();
					//	irr::core::vector3df tar =  pCam->getTarget();
					//	irr::core::vector3df camPos = pCam->getAbsolutePosition();
					//	irr::core::vector3df LookDir = tar - camPos;

					//	irr::scene::ISceneNode *pArrow = pSmgr->getSceneNodeFromName("Arrow");	
					//	
					//	LookDir.normalize();
					//	//pArrow
					//	//pArrow->setRotation(LookDir.getHorizontalAngle());
					//	//pArrow->setPosition(camPos);
					//	//pArrow->OnAnimate(0);

					//	pVideo->draw3DLine(irr::core::vector3df(0,0,0),tar,irr::video::SColor(0,0,255,0));

					//	pVideo->draw3DBox(irr::core::aabbox3df(irr::core::vector3df(0,0,0),irr::core::vector3df(10,10,10)),
					//		irr::video::SColor(255,255,0,0));

					//	pVideo->draw3DTriangle(
					//		irr::core::triangle3df(irr::core::vector3df(0,0,0),irr::core::vector3df(20,0,0),irr::core::vector3df(10,10,0)),
					//		irr::video::SColor(255,0,0,255));
				}



				pVideo->endScene();		
			}

			pDevice->drop();
		}

	}
}