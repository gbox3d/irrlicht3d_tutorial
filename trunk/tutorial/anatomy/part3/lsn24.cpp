﻿#include <irrlicht.h>
#include <assert.h>

#include "comutil.h"

#pragma comment(lib,"comsuppwd.lib") 

/////////////////////////////
//
//스키닝 예제
//
//바이페드를 제어해서 캐릭터가 원하는 방향을 동적으로 바라보게한다.
/////////////////////////////


/*
void loadModel(const c8* filename){	//load a model into the engine	
	if (Model)		Model->remove();	
	Model = 0;	
	scene::IAnimatedMesh* m = Device->getSceneManager()->getMesh(filename);	
	if (!m) 	{		// model could not be loaded		
		if (StartUpModelFile != filename)			
			Device->getGUIEnvironment()->addMessageBox(
			Caption.c_str(), L"The model could not be loaded. Maybe it is not a supported file format."
			);		
		return;	
	}	// set default material properties	
	Model = Device->getSceneManager()->addAnimatedMeshSceneNode(m);	
	Model->setMaterialType(video::EMT_TRANSPARENT_ADD_COLOR);	
	Model->setMaterialFlag(video::EMF_LIGHTING, false);	
	Model->setDebugDataVisible(true);
}
*/


//예제 21의 월드 좌표구하는 함수 사용
//irr::core::vector3df GetWorldPosition(irr::scene::ISceneNode* pNode);


void Lession24()
{
	irr::IrrlichtDevice *pDevice = irr::createDevice(	
		irr::video::EDT_DIRECT3D9/*,
#if(IRR_VERSION == 16)
		irr::core::dimension2d<irr::u32>(640, 480), 
#else
		irr::core::dimension2d<irr::s32>(640, 480), 
#endif			
			32,
			false, false, true,
			NULL*/
			);

	pDevice->setWindowCaption(L"Custom Joint");

	irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
	irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
	irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

	pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,5,10), irr::core::vector3df(0,5,0));	
	

	{//조인트제어 애니관련예제임

		irr::scene::ISkinnedMesh* pSkinMesh = (irr::scene::ISkinnedMesh*)pSmgr->getMesh( "../res/dwarf/dwarf2.b3d" );		

		irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(pSkinMesh);
		pNode->setMaterialFlag(irr::video::EMF_LIGHTING, false);

		/*
		pNode->setMaterialTexture(0,pVideo->getTexture("../res/dwarf/dwarf2.jpg"));				
		pNode->getMaterial(3).setTexture(0,pVideo->getTexture("../res/dwarf/axe.jpg")); //도끼 메트리얼 지정
		*/

		pNode->setAnimationSpeed(15.f);
		pNode->setFrameLoop(2,14);// 걷기
		//pNode->setFrameLoop(292,325); //idle
		pNode->setPosition(irr::core::vector3df(5,0,0));
		pNode->setScale(irr::core::vector3df(.1f,0.1f,0.1f));
	
		pNode->setName("usr/scene/skmesh/testman");
		pNode->setJointMode(irr::scene::EJUOR_CONTROL);
		//pNode->setRenderFromIdentity(true);

		{
			irr::scene::IAnimatedMeshSceneNode *pNode;
			pNode = (irr::scene::IAnimatedMeshSceneNode *)pSmgr->getSceneNodeFromName("usr/scene/skmesh/testman");	
			//조인트 구하기
			irr::core::array<irr::scene::ISkinnedMesh::SJoint *> AllJoint = 				
				((irr::scene::ISkinnedMesh*)pNode->getMesh()->getMesh(0))->getAllJoints();	

			irr::u32 i;
			for(i=0;i<AllJoint.size();i++)
			{
				printf(" %s \n",AllJoint[i]->Name.c_str());
			}
			
		}
	}

	{
		irr::scene::ISceneNode *pNode = pSmgr->addBillboardSceneNode(0,
			irr::core::dimension2df(3,3));
		pNode->setMaterialTexture(0,pVideo->getTexture("../res/particlered.bmp"));
		pNode->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);
		pNode->setMaterialFlag(irr::video::EMF_LIGHTING, false);
		pNode->setName("usr/scene/etc/lookat");
		irr::core::array<irr::core::vector3df> points;
		points.push_back(irr::core::vector3df(3,7,3));
		points.push_back(irr::core::vector3df(3,5,3));
		points.push_back(irr::core::vector3df(-3,7,3));
		points.push_back(irr::core::vector3df(-3,5,3));
		irr::scene::ISceneNodeAnimator *pAnim = pSmgr->createFollowSplineAnimator(pDevice->getTimer()->getTime(),
			points);
		pNode->addAnimator(pAnim);
		pAnim->drop();
	}


	irr::u32 uLastTick = pDevice->getTimer()->getTime();
	while(pDevice->run())
	{	
		irr::u32 uTick = pDevice->getTimer()->getTime();
		irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f;		

		uLastTick = pDevice->getTimer()->getTime();

		pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));
		
		irr::core::vector3df vHead,vTarget;
		{
			irr::scene::IAnimatedMeshSceneNode *pNode,*pNodeLookat;

			pNode = (irr::scene::IAnimatedMeshSceneNode *)pSmgr->getSceneNodeFromName("usr/scene/skmesh/testman");
			pNodeLookat = (irr::scene::IAnimatedMeshSceneNode *)pSmgr->getSceneNodeFromName("usr/scene/etc/lookat");			
			
			pNode->animateJoints();

			irr::scene::IBoneSceneNode *pBone = pNode->getJointNode("head"); //머리 바이페드얻기

			vHead = pBone->getAbsolutePosition();//GetWorldPosition(pBone);
			vTarget = pNodeLookat->getAbsolutePosition();
			
			//바라보는 방향 벡터구하기
			irr::core::vector3df lookat = vTarget - vHead;
			lookat.normalize();

			irr::core::quaternion qt;
			irr::core::vector3df vBaseDir(0,0,1);

			//(0,0,1)기준으로 바라보는 회전 각도구하기
			qt.rotationFromTo(vBaseDir,lookat);
			irr::core::vector3df headrot;
			qt.toEuler(headrot);			
			headrot *= irr::core::RADTODEG;

			//회전적용
			pBone->setRotation(headrot);
			
		}	

		pSmgr->drawAll();
		pGuiEnv->drawAll();

		{

			irr::core::matrix4 mat;//단위행렬로초기화
			pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

			irr::video::SMaterial m;
			m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.
			m.ZBuffer = false;

			pVideo->setMaterial(m);

			pVideo->draw3DLine(vHead,vTarget,irr::video::SColor(0,0,255,0));						
		}
		
		

		pVideo->endScene();	
	}

	pDevice->drop();
}


