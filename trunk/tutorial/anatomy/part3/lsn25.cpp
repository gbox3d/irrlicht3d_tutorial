﻿#include <irrlicht.h>
#include <assert.h>

#include "comutil.h"

#pragma comment(lib,"comsuppwd.lib") 

/////////////////////////////
//
//수학 알고리즘 예
//쿼터니온 응용
/////////////////////////////

void Lession25()
{
	irr::IrrlichtDevice *pDevice = irr::createDevice(					
		irr::video::EDT_DIRECT3D9/*,
#if(IRR_VERSION == 16)
		irr::core::dimension2d<irr::u32>(640, 480), 
#else
		irr::core::dimension2d<irr::s32>(640, 480), 
#endif
			
			32,
			false, false, true,
			NULL*/
			);

	pDevice->setWindowCaption(L"lesson 25");

	irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
	irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
	irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

	pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,20,120), irr::core::vector3df(0,5,0));
	//pSmgr->addCameraSceneNodeMaya();	

	
	{
		irr::scene::ISceneNode *pRoot = pSmgr->addEmptySceneNode();
		pRoot->setName("Axies_node1");				
		irr::scene::IAnimatedMesh *pAniMesh = pSmgr->getMesh("../res/jga/sign.3DS");
		irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(pAniMesh,pRoot);
		pNode->setMaterialTexture(0, pVideo->getTexture("../res/jga/sign.tga"));
		pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
		pNode->setRotation(irr::core::vector3df(90,0,0));
		pRoot->setScale(irr::core::vector3df(.5f,.5f,.5f));
	}

	{
		irr::scene::ISceneNode *pRoot = pSmgr->addEmptySceneNode();
		pRoot->setName("Axies_node2");				
		irr::scene::IAnimatedMesh *pAniMesh = pSmgr->getMesh("../res/jga/sign.3DS");
		irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(pAniMesh,pRoot);
		pNode->setMaterialTexture(0, pVideo->getTexture("../res/jga/sign.tga"));
		pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
		pNode->setRotation(irr::core::vector3df(90,0,0));
		pRoot->setScale(irr::core::vector3df(.5f,.5f,.5f));

		pRoot->setRotation(irr::core::vector3df(-45,0,0));
	}

	{
		irr::scene::ISceneNode *pRoot = pSmgr->addEmptySceneNode();
		pRoot->setName("Axies_node3");				
		irr::scene::IAnimatedMesh *pAniMesh = pSmgr->getMesh("../res/jga/sign.3DS");
		irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(pAniMesh,pRoot);
		pNode->setMaterialTexture(0, pVideo->getTexture("../res/jga/sign.tga"));
		pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
		pNode->setRotation(irr::core::vector3df(90,0,0));
		pRoot->setScale(irr::core::vector3df(.5f,.5f,.5f));
		pRoot->setPosition(irr::core::vector3df(35.f,10,0));
		pRoot->setRotation(irr::core::vector3df(45,0,0));
	}

	{
		irr::scene::ISceneNode *pRoot = pSmgr->addEmptySceneNode();
		pRoot->setName("Axies_node5");				
		irr::scene::IAnimatedMesh *pAniMesh = pSmgr->getMesh("../res/jga/sign.3DS");
		irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(pAniMesh,pRoot);
		pNode->setMaterialTexture(0, pVideo->getTexture("../res/jga/sign.tga"));
		pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
		pNode->setRotation(irr::core::vector3df(90,0,0));
		pRoot->setScale(irr::core::vector3df(.5f,.5f,.5f));
		//pRoot->setPosition(irr::core::vector3df(10.f,0,0));
		//pRoot->setRotation(irr::core::vector3df(45,0,0));
	}

	{
		irr::scene::ISceneNode *pRoot = pSmgr->addEmptySceneNode();
		pRoot->setName("Axies_node4");				
		irr::scene::IAnimatedMesh *pAniMesh = pSmgr->getMesh("../res/jga/sign.3DS");
		irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(pAniMesh,pRoot);
		pNode->setMaterialTexture(0, pVideo->getTexture("../res/jga/sign.tga"));
		pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
		pNode->setRotation(irr::core::vector3df(90,0,0));
		//pRoot->setScale(irr::core::vector3df(.5f,.5f,.5f));
		pRoot->setParent(pSmgr->getSceneNodeFromName("Axies_node3"));
	}




	
	{
		irr::core::vector3df v0,v1,v2;

		v0 = irr::core::vector3df(1,0,0);
		v1 = irr::core::vector3df(1,0,0);
		v2 = irr::core::vector3df(1,0,0);	

		v0.rotateXYBy(64,irr::core::vector3df(0,0,0));
		v1.rotateXZBy(32,irr::core::vector3df(0,0,0));

		//두벡터의 사잇각구하기
		irr::core::quaternion qt;
		qt.rotationFromTo(v0,v1);		
		qt.toEuler(v2);
		v2 *= irr::core::RADTODEG;
		printf("quat: %f,%f,%f\n",v2.X,v2.Y,v2.Z);

		//v0를 v1으로 회전 이동
		v0 = qt * v0;//순서에주의를 거꾸로 곱하면안됨

		if(v0 == v1)
		{
			printf("succes quat v0 %f,%f,%f \n",v0.X,v0.Y,v0.Z);
			printf("succes quat v1 %f,%f,%f \n",v1.X,v1.Y,v1.Z);
		}
	}

	
	////////////////////////////////////////////////////////////

	{
		irr::core::quaternion qt1,qt2,qt3,qt4;		
		irr::core::vector3df vRst;

		//쿼터니온을 곱해주면 두회전값의 합을구할수있다.

		qt1 = irr::core::quaternion(irr::core::vector3df(90,0,0) *irr::core::DEGTORAD);
		qt2 = irr::core::quaternion(irr::core::vector3df(87,0,0) *irr::core::DEGTORAD);

		qt3 = qt1 * qt2;		

		//회전값의 차를 구한다.
		qt2.makeInverse();
		qt4 = qt1 * qt2;

		qt3.toEuler(vRst);
		vRst *= irr::core::RADTODEG;
		printf("%f,%f,%f\n",vRst.X,vRst.Y,vRst.Z);

		qt4.toEuler(vRst);
		vRst *= irr::core::RADTODEG;
		printf("%f,%f,%f\n",vRst.X,vRst.Y,vRst.Z);
		
	}

	irr::core::quaternion trackball_qtX;// = irr::core::quaternion(irr::core::vector3df(0,0,0) *irr::core::DEGTORAD);
	irr::core::quaternion trackball_qtY;
	irr::core::quaternion trackball_qt;

	irr::core::position2di m_pos,m_pos_last;


	irr::u32 uLastTick = pDevice->getTimer()->getTime();
	while(pDevice->run())
	{	
		irr::u32 uTick = pDevice->getTimer()->getTime();
		irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f;		

		uLastTick = pDevice->getTimer()->getTime();

		pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

		m_pos = pDevice->getCursorControl()->getPosition();

		irr::core::position2di m_diff = m_pos - m_pos_last;

		//irr::core::quaternion qt(irr::core::vector3df((float)m_diff.Y,(float)m_diff.X,0) * irr::core::DEGTORAD);
		//각축에 대한 회전 쿼터니온 만들기
		irr::core::quaternion qt_pitch(irr::core::vector3df((float)m_diff.Y,0,0) * irr::core::DEGTORAD);
		irr::core::quaternion qt_yaw(irr::core::vector3df(0,(float)m_diff.X,0) * irr::core::DEGTORAD);

		trackball_qtX *= qt_pitch;
		trackball_qtY *= qt_yaw;
		

		//irr::core::quaternion qt = qt_yaw * qt_pitch;
		//트랙볼 제어하기
		trackball_qt = trackball_qtX * trackball_qtY;				

		{
			//오일러각변환
			irr::core::vector3df vRot,vDiffRot;
			trackball_qt.toEuler(vRot);
			vRot *= irr::core::RADTODEG;
			//트랙볼 제어를 직접받는 화살표
			irr::scene::ISceneNode *pNode = pSmgr->getSceneNodeFromName("Axies_node1");
			pNode->setRotation(vRot);

			//기준표시
			irr::scene::ISceneNode *pNode2 = pSmgr->getSceneNodeFromName("Axies_node2");

			irr::core::quaternion qt1(pNode->getRotation() * irr::core::DEGTORAD);
			irr::core::quaternion qt2(pNode2->getRotation()* irr::core::DEGTORAD);			

			qt2.makeInverse();
			irr::core::quaternion qt3 = qt1 * qt2;	//사잇각구하기		
			
			qt3.toEuler(vDiffRot);
			vDiffRot *= irr::core::RADTODEG;			
			
			pNode = pSmgr->getSceneNodeFromName("Axies_node4");
			pNode->setRotation(vDiffRot);
		}

		
		{
			irr::core::vector3df vDir(0,30.f,0); //거리 30만큼이동
			vDir = trackball_qt * vDir;

			irr::scene::ISceneNode *pNode = pSmgr->getSceneNodeFromName("Axies_node5");
			pNode->setPosition(vDir);

			irr::core::vector3df Rot;
			trackball_qt.toEuler(Rot);

			pNode->setRotation(Rot*irr::core::RADTODEG);
			
		}

		m_pos_last = m_pos;					


		pSmgr->drawAll();
		pGuiEnv->drawAll();

		{
			irr::core::matrix4 mat;//단위행렬로초기화
			pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

			irr::video::SMaterial m;
			m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.
			m.ZBuffer = false;
			
			pVideo->setMaterial(m);
			
			//pVideo->draw3DLine(irr::core::vector3df(0,0,0),irr::core::vector3df(100,0,0),irr::video::SColor(0,0,255,0));			
			//pVideo->draw3DBox(irr::core::aabbox3df(irr::core::vector3df(0,0,0),irr::core::vector3df(10,10,10)),irr::video::SColor(255,255,0,0));
			//pVideo->draw3DTriangle(irr::core::triangle3df(irr::core::vector3df(0,0,0),irr::core::vector3df(20,0,0),irr::core::vector3df(10,10,0)),irr::video::SColor(255,0,0,255));
		}

		pVideo->endScene();	
	}

	pDevice->drop();
}


