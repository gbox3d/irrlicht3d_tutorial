﻿#include <irrlicht.h>
#include <assert.h>

#include "comutil.h"

#pragma comment(lib,"comsuppwd.lib") 


//힐플레인 메쉬응용 평면 앞뒤 표현 예제

inline void DrawDebugBox(irr::video::IVideoDriver *pVideo,irr::core::vector3df pos,irr::f32 scale,irr::video::SColor color)
{
	irr::core::vector3df ptscale(scale,scale,scale);
	pVideo->draw3DBox(irr::core::aabbox3df(pos - ptscale,pos + ptscale),color);							
}

inline void DrawDebugAxies(irr::video::IVideoDriver *pVideo,irr::core::vector3df pos,irr::f32 scale,irr::core::quaternion Rot)
{
	irr::core::vector3df AxiesX(1,0,0);
	irr::core::vector3df AxiesY(0,1,0);
	irr::core::vector3df AxiesZ(0,0,1);

	AxiesX = Rot * AxiesX;
	AxiesY = Rot * AxiesY;
	AxiesZ = Rot * AxiesZ;

	pVideo->draw3DLine(pos,pos+AxiesX*scale,irr::video::SColor(255,255,0,0));
	pVideo->draw3DLine(pos,pos+AxiesY*scale,irr::video::SColor(255,0,255,0));
	pVideo->draw3DLine(pos,pos+AxiesZ*scale,irr::video::SColor(255,0,0,255));	
}

inline void ReverseMeshSurface(irr::scene::IMeshBuffer *pmb)
{
	//irr::scene::IMeshBuffer *pmb = pMesh->getMeshBuffer(0);

	irr::u16 *pIb = pmb->getIndices();
	irr::video::S3DVertex *pVb = (irr::video::S3DVertex *)pmb->getVertices();
	{
		irr::u32 i;
		for(i=0;i<pmb->getIndexCount();i)
		{		
			int nTemp =	pIb[i+1];
			pIb[i+1] =	pIb[i+2];
			pIb[i+2] =	nTemp;

			i+=3;
		}
	}			
}

void Lession26()
{
	irr::IrrlichtDevice *pDevice = irr::createDevice(	
		irr::video::EDT_DIRECT3D9/*,
#if(IRR_VERSION == 16)
		irr::core::dimension2d<irr::u32>(640, 480), 
#else
		irr::core::dimension2d<irr::s32>(640, 480), 
#endif			
			32,
			false, false, true,
			NULL*/
			);

	pDevice->setWindowCaption(L"lesson 26");

	irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
	irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
	irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

	//pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,100,200), irr::core::vector3df(0,5,0));
	irr::scene::ICameraSceneNode *pCam = pSmgr->addCameraSceneNodeFPS();
	pCam->setPosition(irr::core::vector3df(0,0,100));
	pCam->setTarget(irr::core::vector3df(0,0,0));
	pCam->OnAnimate(0);


	{
		irr::scene::ISceneNode* pNode = 0;
		irr::scene::IAnimatedMesh* pMesh;			

		pMesh = pSmgr->addHillPlaneMesh("myHill",
			irr::core::dimension2d<irr::f32>(32,32),
			irr::core::dimension2d<irr::u32>(8,8), 
			0,0	
			);


		pMesh = pSmgr->addHillPlaneMesh("myHill_back",
			irr::core::dimension2d<irr::f32>(32,32),
			irr::core::dimension2d<irr::u32>(8,8), 
			0,0	
			);
		ReverseMeshSurface(pMesh->getMeshBuffer(0));

		//printf("%d \n",pMesh->getMeshBufferCount());	

		/*
		pNode = pSmgr->addAnimatedMeshSceneNode(pMesh);
		pNode->setPosition(irr::core::vector3df(0,0,0));
		pNode->setRotation(irr::core::vector3df(90,0,0));
		pNode->setMaterialTexture(0, pVideo->getTexture("../res/wall.jpg"));		
		pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
		pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);
		pNode->setName("ground");		
		*/
	}


	irr::u32 uLastTick = pDevice->getTimer()->getTime();
	while(pDevice->run())
	{	
		irr::u32 uTick = pDevice->getTimer()->getTime();
		irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f;		

		uLastTick = pDevice->getTimer()->getTime();

		pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

		pSmgr->drawAll();
		pGuiEnv->drawAll();

		{
			irr::core::matrix4 mat;//단위행렬로초기화
			mat.setRotationDegrees(irr::core::vector3df(90,0,0));
			pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

			irr::video::SMaterial m;
			m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.
			//m.ZBuffer = false;
			//m.Wireframe = true;

			m.setTexture(0,pVideo->getTexture("../res/wall.jpg"));			
			pVideo->setMaterial(m);

			irr::scene::IAnimatedMesh* pMesh;
			pMesh = pSmgr->getMesh("myHill");						
			pVideo->drawMeshBuffer(pMesh->getMeshBuffer(0));	

			m.setTexture(0,pVideo->getTexture("../res/rockwall.bmp"));
			pVideo->setMaterial(m);
			pMesh = pSmgr->getMesh("myHill_back");	
			pVideo->drawMeshBuffer(pMesh->getMeshBuffer(0));	
			
			
		}
		pVideo->endScene();	
	}

	pDevice->drop();
}