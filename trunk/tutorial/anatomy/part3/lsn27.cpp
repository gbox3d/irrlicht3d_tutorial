﻿#include <irrlicht.h>
#include <assert.h>

#include "comutil.h"

#pragma comment(lib,"comsuppwd.lib") 

//직교투명과 원근투명 비교예제


void Lession27()
{
	irr::IrrlichtDevice *pDevice = irr::createDevice(					
		irr::video::EDT_DIRECT3D9/*,
#if(IRR_VERSION == 16)
		irr::core::dimension2d<irr::u32>(640, 480), 
#else
		irr::core::dimension2d<irr::s32>(640, 480), 
#endif
			32,
			false, false, true,
			NULL*/
			);

	pDevice->setWindowCaption(L"lesson 27");

	irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
	irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
	irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();	
	
	irr::scene::ICameraSceneNode *pCam = pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,100,200), irr::core::vector3df(0,5,0));	
	pCam->setPosition(irr::core::vector3df(0,0,100));
	pCam->setTarget(irr::core::vector3df(0,0,0));	

	//원근 투명
	{
		irr::scene::IAnimatedMesh *pAniMesh = pSmgr->getMesh("../res/sydney.md2");
		irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(pAniMesh);
		pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);						
		pNode->setMD2Animation(irr::scene::EMAT_STAND);
		pNode->setMaterialTexture(0,pVideo->getTexture("../res/sydney.bmp"));	
		pNode->setPosition(irr::core::vector3df(-20,0,0));

		pNode = pSmgr->addAnimatedMeshSceneNode(pAniMesh);
		pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);						
		pNode->setMD2Animation(irr::scene::EMAT_STAND);
		pNode->setMaterialTexture(0,pVideo->getTexture("../res/sydney.bmp"));	
		pNode->setPosition(irr::core::vector3df(-20,0,20));

		pNode = pSmgr->addAnimatedMeshSceneNode(pAniMesh);
		pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);						
		pNode->setMD2Animation(irr::scene::EMAT_STAND);
		pNode->setMaterialTexture(0,pVideo->getTexture("../res/sydney.bmp"));	
		pNode->setPosition(irr::core::vector3df(-20,0,40));

		pNode = pSmgr->addAnimatedMeshSceneNode(pAniMesh);
		pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);						
		pNode->setMD2Animation(irr::scene::EMAT_STAND);
		pNode->setMaterialTexture(0,pVideo->getTexture("../res/sydney.bmp"));	
		pNode->setPosition(irr::core::vector3df(-20,0,60));
	}

	irr::scene::ISceneManager *pNewSMgr = pSmgr->createNewSceneManager();

	//직교투명 카메라
	{
		irr::scene::ICameraSceneNode *pCam = pNewSMgr->addCameraSceneNode(0, irr::core::vector3df(0,100,200), irr::core::vector3df(0,5,0));	
		pCam->setPosition(irr::core::vector3df(0,0,100));
		pCam->setTarget(irr::core::vector3df(0,0,0));	

		irr::core::matrix4 prjMat;
		prjMat.buildProjectionMatrixOrthoLH(640,480,-1000.f,1000.f);
		pCam->setProjectionMatrix(prjMat);
		

		irr::scene::IAnimatedMesh *pAniMesh = pNewSMgr->getMesh("../res/sydney.md2");
		irr::scene::IAnimatedMeshSceneNode *pNode = pNewSMgr->addAnimatedMeshSceneNode(pAniMesh);
		pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);						
		pNode->setMD2Animation(irr::scene::EMAT_STAND);
		pNode->setMaterialTexture(0,pVideo->getTexture("../res/sydney.bmp"));	
		pNode->setPosition(irr::core::vector3df(20,0,0));

		pNode = pNewSMgr->addAnimatedMeshSceneNode(pAniMesh);
		pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);						
		pNode->setMD2Animation(irr::scene::EMAT_STAND);
		pNode->setMaterialTexture(0,pVideo->getTexture("../res/sydney.bmp"));	
		pNode->setPosition(irr::core::vector3df(20,0,20));

		pNode = pNewSMgr->addAnimatedMeshSceneNode(pAniMesh);
		pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);						
		pNode->setMD2Animation(irr::scene::EMAT_STAND);
		pNode->setMaterialTexture(0,pVideo->getTexture("../res/sydney.bmp"));	
		pNode->setPosition(irr::core::vector3df(20,0,40));

		pNode = pNewSMgr->addAnimatedMeshSceneNode(pAniMesh);
		pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);						
		pNode->setMD2Animation(irr::scene::EMAT_STAND);
		pNode->setMaterialTexture(0,pVideo->getTexture("../res/sydney.bmp"));	
		pNode->setPosition(irr::core::vector3df(20,0,60));

		
	}

	int lastFPS = -1; 
	irr::u32 uLastTick = pDevice->getTimer()->getTime();

	while(pDevice->run())
	{	
		irr::u32 uTick = pDevice->getTimer()->getTime();
		irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f;		

		uLastTick = pDevice->getTimer()->getTime();

		pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

		//두개의 서로 다른 씬매니져 동시에 화면에 적용시키기
		pSmgr->drawAll();
		pNewSMgr->drawAll(); 

		pGuiEnv->drawAll();


		pVideo->endScene();

		{
			if (pVideo->getFPS() != lastFPS) {
				lastFPS = pVideo->getFPS();
				wchar_t tmp[1024];
				swprintf(
					tmp,
					1024,
					L"Irrlicht SplitScreen-Example (FPS: %d)",
					lastFPS);
				pDevice->setWindowCaption(tmp);
			}
		}

	}

	pNewSMgr->drop();

	pDevice->drop();
}