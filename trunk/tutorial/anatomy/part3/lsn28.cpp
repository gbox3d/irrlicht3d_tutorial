﻿#include <irrlicht.h>
#include <assert.h>

//씬파일(.irr)로 로딩된 메쉬 객체 얻어 삼각 샐랙터 등록시키기

void Lession28()
{
	irr::IrrlichtDevice *pDevice = irr::createDevice(
		irr::video::EDT_DIRECT3D9/*,
#if(IRR_VERSION == 16)
		irr::core::dimension2d<irr::u32>(640, 480), 
#else
		irr::core::dimension2d<irr::s32>(640, 480), 
#endif		
		32,
		false, false, false,
		NULL*/
		);

	pDevice->setWindowCaption(L"lesson 28");

	irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
	irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
	irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();	

	irr::scene::ICameraSceneNode *pCam = pSmgr->addCameraSceneNode(0, 
		irr::core::vector3df(-150,100,0), 
		irr::core::vector3df(0,0,0));	
	{
		irr::scene::ITriangleSelector *pSelector;
		irr::io::IFileSystem* ifs;
		ifs = pDevice->getFileSystem();

		irr::core::stringc strwDir = ifs->getWorkingDirectory();	
		printf("%s \n",strwDir.c_str());

		//씬파일이 기준점 되게 작업디랙토리 지정
		ifs->changeWorkingDirectoryTo("../res/stage03");

		//씬로딩...
		pSmgr->loadScene("test.xml");

		irr::io::IAttributes *pAttr = ifs->createEmptyAttributes();		

		irr::scene::ISceneNode *pNode = pSmgr->getSceneNodeFromName("ground");
		//속성개체(xml자료구조)로 노드정보 출력
		pNode->serializeAttributes(pAttr);		
		// xml자료구조에서  Mesh라는 속성얻기
		irr::core::stringc strMeshName = pAttr->getAttributeAsString("Mesh");

		printf("%s\n",strMeshName.c_str());
		irr::scene::IAnimatedMesh *pAniMesh = pSmgr->getMesh(strMeshName.c_str());		

		if(pNode->getType() == irr::scene::ESNT_OCTREE)
		{
			//옥트리일경우	
			pSelector = pSmgr->createOctreeTriangleSelector(
				pAniMesh->getMesh(0),
				pNode
				);
			pNode->setTriangleSelector(pSelector);
		}

		ifs->changeWorkingDirectoryTo(strwDir.c_str());

		pSelector->drop();
	}


	int lastFPS = -1; 
	irr::u32 uLastTick = pDevice->getTimer()->getTime();

	while(pDevice->run())
	{	
		irr::u32 uTick = pDevice->getTimer()->getTime();
		irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f;		

		uLastTick = pDevice->getTimer()->getTime();

		

		pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));
		
		pSmgr->drawAll();	

		pGuiEnv->drawAll();


		//샐랙터 태스트		
		{
			
			irr::core::position2di mouse_pos = pDevice->getCursorControl()->getPosition();

			irr::core::line3df Ray;			
			irr::core::vector3df v3Intersec;
			irr::core::triangle3df tri;
			irr::scene::ITriangleSelector *pSelector;			
			irr::scene::ISceneNode *pPickNode;
			
			//2디 스크린좌표계로부터 피킹레이 얻기
			Ray = pSmgr->getSceneCollisionManager()->getRayFromScreenCoordinates(mouse_pos);						
			//pPickNode = pSmgr->getSceneCollisionManager()->getSceneNodeFromRayBB(Ray);
			pPickNode = pSmgr->getSceneNodeFromName("ground");
			if(pPickNode)
			{
				//샐랙터얻기
				pSelector = pPickNode->getTriangleSelector();
				if(pSelector) //샐랙터가 존재하면...
				{
//#if(IRR_VERSION >= 16)
					const irr::scene::ISceneNode *pColNode;
					if(pSmgr->getSceneCollisionManager()->getCollisionPoint(Ray,pSelector,v3Intersec,tri,pColNode))
//#else
	//				if(pSmgr->getSceneCollisionManager()->getCollisionPoint(Ray,pSelector,v3Intersec,tri))
//#endif
					{
						printf("Intersec pt: %f,%f,%f \n",v3Intersec.X,v3Intersec.Y,v3Intersec.Z);						

						irr::video::SMaterial m;
						m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.
						pVideo->setMaterial(m);				
						pVideo->setTransform(irr::video::ETS_WORLD, pPickNode->getAbsoluteTransformation());						

						//피킹 삼각형보여주기
						pVideo->draw3DTriangle(tri, irr::video::SColor(0,255,0,0));

						//피킹점 보여주기
						pVideo->draw3DLine(pSmgr->getActiveCamera()->getPosition()+irr::core::vector3df(0,1.0f,0),
							v3Intersec,irr::video::SColor(0,255,255,0));

					}
				}
			}
		}

		pVideo->endScene();

		{
			if (pVideo->getFPS() != lastFPS) {
				lastFPS = pVideo->getFPS();
				wchar_t tmp[1024];
				swprintf(
					tmp,
					1024,
					L"Irrlicht SplitScreen-Example (FPS: %d)",
					lastFPS);
				pDevice->setWindowCaption(tmp);
			}
		}

	}

	pDevice->drop();

}