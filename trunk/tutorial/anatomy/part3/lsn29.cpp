﻿#include <irrlicht.h>
#include <assert.h>

#include "comutil.h"

#pragma comment(lib,"comsuppwd.lib") 

/////////////////////////////
//
//수학 알고리즘 예
//쿼터니온 응용2
//해바라기 문제 해법
/////////////////////////////

void Lession29()
{
	irr::IrrlichtDevice *pDevice = irr::createDevice(	
		irr::video::EDT_DIRECT3D9/*,
#if(IRR_VERSION == 16)
		irr::core::dimension2d<irr::u32>(640, 480), 
#else
		irr::core::dimension2d<irr::s32>(640, 480), 
#endif
			32,
			false, false, true,
			NULL*/
			);

	pDevice->setWindowCaption(L"lesson 25");

	irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
	irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
	irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

	pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,20,120), irr::core::vector3df(0,5,0));	
	
	//축노드 생성
	{
		irr::scene::ISceneNode *pRoot = pSmgr->addEmptySceneNode();
		pRoot->setName("Axies_node1");				
		irr::scene::IAnimatedMesh *pAniMesh = pSmgr->getMesh("../res/jga/sign.3DS");
		irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(pAniMesh,pRoot);
		pNode->setMaterialTexture(0, pVideo->getTexture("../res/jga/sign.tga"));
		pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
		pNode->setRotation(irr::core::vector3df(90,0,0));
		pRoot->setScale(irr::core::vector3df(.5f,.5f,.5f));
	}

	//화살표 노드생성
	{
		irr::scene::IAnimatedMesh *pAniMesh =  pSmgr->addArrowMesh("arrow",
			irr::video::SColor(0,255,0,0),
			irr::video::SColor(0,0,255,0),
			4,4,10,7,5,5			
			);

		//라이트로드를 부모로 붙임
		irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(pAniMesh);
		pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);		
		pNode->setName("arrow_node1");	
	}

	//화살표 노드생성
	{
		irr::scene::IAnimatedMesh *pAniMesh =  pSmgr->addArrowMesh("arrow2",
			irr::video::SColor(0,255,255,0),
			irr::video::SColor(0,0,0,255),
			4,4,10,7,5,5			
			);

		//라이트로드를 부모로 붙임
		irr::scene::ISceneNode *pRoot = pSmgr->addEmptySceneNode();
		irr::scene::IAnimatedMeshSceneNode *pNode = pSmgr->addAnimatedMeshSceneNode(pAniMesh);
		pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);	
		pNode->setParent(pRoot);
		pNode->setRotation(irr::core::vector3df(90.f,0,0));
		pRoot->setName("arrow_node2");


		//연기효과
		{
			// add particle system
			irr::scene::IParticleSystemSceneNode* ps =
				pSmgr->addParticleSystemSceneNode(false);

			ps->setParticleSize(irr::core::dimension2d<irr::f32>(6.0f, 6.0f));

			// create and set emitter
			irr::scene::IParticleEmitter* em = ps->createBoxEmitter(
				irr::core::aabbox3d<irr::f32>(-3,0,-3,3,1,3),
				irr::core::vector3df(0.0f,0.0f,0.0f),
				10,40,
				irr::video::SColor(0,255,255,255), irr::video::SColor(0,255,255,255),
				600,2000);
			ps->setEmitter(em);
			em->drop();

			// create and set affector
			irr::scene::IParticleAffector* paf = ps->createFadeOutParticleAffector();
			ps->addAffector(paf);
			paf->drop();

			// adjust some material settings
			ps->setMaterialFlag(irr::video::EMF_LIGHTING, false);
			ps->setMaterialFlag(irr::video::EMF_ZWRITE_ENABLE, false);
			ps->setMaterialTexture(0, pVideo->getTexture("../res/smoke.bmp"));
			ps->setMaterialType(irr::video::EMT_TRANSPARENT_VERTEX_ALPHA);	

			ps->setParent(pRoot);

		}
	}

	irr::core::quaternion trackball_qtX;
	irr::core::quaternion trackball_qtY;
	irr::core::quaternion trackball_qt;
	irr::core::position2di m_pos,m_pos_last;

	irr::core::vector3df vAimDir;

	irr::u32 uLastTick = pDevice->getTimer()->getTime();
	while(pDevice->run())
	{	
		irr::u32 uTick = pDevice->getTimer()->getTime();
		irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f;		

		uLastTick = pDevice->getTimer()->getTime();		

		{
			irr::scene::ISceneNode *pAxies_node1 = pSmgr->getSceneNodeFromName("Axies_node1");
			//irr::scene::ISceneNode *pAxies_node2 = pSmgr->getSceneNodeFromName("Axies_node2");
			irr::scene::ISceneNode *pArrow_node1 = pSmgr->getSceneNodeFromName("arrow_node1");
			//pNode->setRotation(vRot);

			//마우스 입력처리
			m_pos = pDevice->getCursorControl()->getPosition();
			irr::core::position2di m_diff = m_pos - m_pos_last;
			m_pos_last = m_pos;					

			//각축에 대한 회전 쿼터니온 만들기
			irr::core::quaternion qt_pitch(irr::core::vector3df((float)m_diff.Y,0,0) * irr::core::DEGTORAD);
			irr::core::quaternion qt_yaw(irr::core::vector3df(0,(float)m_diff.X,0) * irr::core::DEGTORAD);
			trackball_qtX *= qt_pitch;
			trackball_qtY *= qt_yaw;

			//트랙볼 제어용 쿼터니온 구하기
			trackball_qt = trackball_qtX * trackball_qtY;
			
			irr::core::vector3df vTargetPos(0.f,60.f,0.f);
			vTargetPos = trackball_qt * vTargetPos;
			pArrow_node1->setPosition(vTargetPos);


			irr::core::quaternion qt1(pAxies_node1->getRotation() * irr::core::DEGTORAD);
			
			vAimDir = vTargetPos - pAxies_node1->getPosition();
			vAimDir.normalize();

			irr::core::quaternion qt2(vAimDir.getHorizontalAngle() * irr::core::DEGTORAD);
			
			//구면보간을 해서 천천히 따라가게한다.
			qt1.slerp(qt1,qt2, 1.5f * fDelta);
			
			//오일러 회전각으로 변환해서 회전을 반영한다.
			irr::core::vector3df vRot;
			qt1.toEuler(vRot);
			vRot *= irr::core::RADTODEG;		
			pAxies_node1->setRotation(vRot);

			
		}

		//유도탄 예제
		{
			irr::scene::ISceneNode *pArrow_node1 = pSmgr->getSceneNodeFromName("arrow_node1");
			irr::scene::ISceneNode *pArrow_node2 = pSmgr->getSceneNodeFromName("arrow_node2");

			irr::core::vector3df vDir(0.f,0.f,1.f);
			irr::f32 Speed = 15.0f;
			irr::core::vector3df vTargetDir = 
				pArrow_node1->getPosition() - pArrow_node2->getPosition();

			vTargetDir.normalize();

			irr::core::quaternion qt1(pArrow_node2->getRotation() * irr::core::DEGTORAD);
			irr::core::quaternion qt2(vTargetDir.getHorizontalAngle() * irr::core::DEGTORAD);

			//구면보간을 해서 천천히 따라가게한다.
			qt1.slerp(qt1,qt2, 1.5f * fDelta);
			qt1.normalize();//정규화를 해줘야한다.

			//오일러 회전각으로 변환해서 회전을 반영한다.
			irr::core::vector3df vRot;
			qt1.toEuler(vRot);
			vRot *= irr::core::RADTODEG;		
			pArrow_node2->setRotation(vRot);

			vDir = vDir * (fDelta*Speed);
			vDir = qt1 * vDir;

			pArrow_node2->setPosition(pArrow_node2->getPosition() +	vDir);
		}

		
		//드로잉시작
		pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

		pSmgr->drawAll();
		pGuiEnv->drawAll();
		//사용자 임의 그리기 함수
		{
			irr::core::matrix4 mat;//단위행렬로초기화
			pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

			irr::video::SMaterial m;
			m.Lighting = false;  //라이트를꺼야 색이 제데로나온다.
			m.ZBuffer = false;
			
			pVideo->setMaterial(m);
		}
		pVideo->endScene();	
	}

	pDevice->drop();
}


