﻿#include <stdlib.h>
#include <crtdbg.h>

#include <irrlicht.h>
#include <assert.h>

//메쉬 직접 만들어서 붙이기
irr::scene::IMesh* createTestMesh()
{
	irr::video::S3DVertex v;
	irr::scene::SMeshBuffer* buffer = new irr::scene::SMeshBuffer();

	v.Pos.X = -5.f;
	v.Pos.Y = 5.f;
	v.Pos.Z = 0;
	v.TCoords.X = 0;
	v.TCoords.Y = 0;
	buffer->Vertices.push_back(v);

	v.Pos.X = 5.f;
	v.Pos.Y = 5.f;
	v.Pos.Z = 0;
	v.TCoords.X = 1;
	v.TCoords.Y = 0;
	buffer->Vertices.push_back(v);

	v.Pos.X = 5.f;
	v.Pos.Y = -5.f;
	v.Pos.Z = 0;
	v.TCoords.X = 1;
	v.TCoords.Y = 1;
	buffer->Vertices.push_back(v);

	v.Pos.X = -5.f;
	v.Pos.Y = -5.f;
	v.Pos.Z = 0;
	v.TCoords.X = 0;
	v.TCoords.Y = 1;
	buffer->Vertices.push_back(v);

	//삼각형리스트
	//시계방향으로 감아주기
	buffer->Indices.push_back(0);
	buffer->Indices.push_back(1);
	buffer->Indices.push_back(2);

	buffer->Indices.push_back(3);
	buffer->Indices.push_back(0);
	buffer->Indices.push_back(2);

	buffer->recalculateBoundingBox();
	irr::scene::SMesh* mesh = new irr::scene::SMesh();
	mesh->addMeshBuffer(buffer);
	mesh->recalculateBoundingBox();
	buffer->drop();
	return mesh;
}

void Lession30()
{
	irr::IrrlichtDevice *pDevice = irr::createDevice(
		irr::video::EDT_DIRECT3D9/*,
#if(IRR_VERSION == 16)
		irr::core::dimension2d<irr::u32>(640, 480), 
#else
		irr::core::dimension2d<irr::s32>(640, 480), 
#endif
			32,
			false, false, true,
			NULL*/
			);

	pDevice->setWindowCaption(L"lesson 30");

	irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
	irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
	irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

	pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,10,-20), irr::core::vector3df(0,0,0));	

	pDevice->getFileSystem()->changeWorkingDirectoryTo("../res/");	

	{
		//메쉬 생성
		irr::scene::IMesh* mesh = createTestMesh();
		irr::scene::SAnimatedMesh* animatedMesh = new irr::scene::SAnimatedMesh();
		animatedMesh->addMesh(mesh);
		mesh->drop();
		//씬메니져에 메쉬 등록
		pSmgr->getMeshCache()->addMesh("mesh_test",animatedMesh);
		animatedMesh->drop();
	}
	
	irr::scene::ISceneNode *pNode;	
	pNode = pSmgr->addAnimatedMeshSceneNode(pSmgr->getMesh("mesh_test"));
	pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
	pNode->setMaterialTexture(0,pVideo->getTexture("jga/sign.tga"));		


	irr::u32 uLastTick = pDevice->getTimer()->getTime();
	while(pDevice->run())
	{	
		irr::u32 uTick = pDevice->getTimer()->getTime();
		irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f;		

		uLastTick = pDevice->getTimer()->getTime();

		pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

		pSmgr->drawAll();
		pGuiEnv->drawAll();

		pVideo->endScene();	
	}

	pDevice->drop();
}