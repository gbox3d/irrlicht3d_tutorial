﻿#include <irrlicht.h>
#include <assert.h>

#pragma comment(lib,"irrlicht.lib")

/*

기초 오브잭트 제어 예제
초창기 바이오 하자드식 캐릭터 이동구현

화살표 좌우 키: 회전
화살표 상 키

*/

class Jga_App :  public irr::IEventReceiver
{
public:

	irr::IrrlichtDevice *m_pDevice;

	irr::video::IVideoDriver *m_pVideo;
	irr::scene::ISceneManager *m_pSmgr;
	irr::gui::IGUIEnvironment *m_pGuiEnv;	

	Jga_App ()
	{
		m_pDevice = irr::createDevice(     
			irr::video::EDT_DIRECT3D9/*,
#if(IRR_VERSION == 16)
		irr::core::dimension2d<irr::u32>(640, 480), 
#else
		irr::core::dimension2d<irr::s32>(640, 480), 
#endif
			32,
			false, false, true,
			this*/
			);
		m_pVideo = m_pDevice->getVideoDriver();
		m_pSmgr = m_pDevice->getSceneManager();
		m_pGuiEnv = m_pDevice->getGUIEnvironment(); 


		//scene inittialize here

		irr::scene::ICameraSceneNode *pCam = 
			m_pSmgr->addCameraSceneNode(0,irr::core::vector3df(0,150,10),
			irr::core::vector3df(0,0,0));

		{
			irr::scene::IAnimatedMesh *pAniMesh = m_pSmgr->addArrowMesh("arrow",
				irr::video::SColor(0,255,0,0),
				irr::video::SColor(0,0,255,0),
				5,5,10,7,5,5
				);
			irr::scene::IAnimatedMeshSceneNode *pNode =
				m_pSmgr->addAnimatedMeshSceneNode(pAniMesh);
			assert(pNode);
			pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
			pNode->setPosition(irr::core::vector3df(0,0,0));
			pNode->setName("usr/scene/mesh/arrow1");
			pNode->setRotation(irr::core::vector3df(90,0,0));

		}

		//부모노드 만들기
		{
			irr::scene::ISceneNode *pNode = m_pSmgr->addEmptySceneNode();
			pNode->addChild(m_pSmgr->getSceneNodeFromName("usr/scene/mesh/arrow1"));
			pNode->setName("usr/scene/empty/1");

		}	

	}

	~Jga_App ()
	{
		m_pDevice->drop();
	} 

	//이벤트 핸들러
	virtual bool OnEvent(const irr::SEvent& event)
	{
		switch(event.EventType)
		{
		case irr::EET_GUI_EVENT:
			{

			}
			break;
		case irr::EET_KEY_INPUT_EVENT:
			{

				if(event.KeyInput.Key == irr::KEY_UP)
				{
					irr::scene::ISceneNode *pNode = 
						m_pSmgr->getSceneNodeFromName("usr/scene/empty/1");

					
					irr::core::vector3df vDir(0,0,1);

					/*

					matrix 직접접근하기.

					(0,0) (0,1) (0,2) (0,3)
					(1,0) (1,1) (1,2) (1,3)
					(2,0) (2,1) (2,2) (2,3)
					(3,0) (3,1) (3,2) (3,3)

					(행,렬)

					*/

					irr::core::matrix4 mat = pNode->getAbsoluteTransformation();
					//Y축 회전 정보만 남기기
					mat(3,0) = 0;
					mat(3,1) = 0;
					mat(3,2) = 0; 

					//벡터에 회전 적용
					mat.transformVect(vDir);					

					pNode->setPosition(
						pNode->getPosition() + vDir);
				}
				if(event.KeyInput.Key == irr::KEY_DOWN)
				{
					irr::scene::ISceneNode *pNode = 
						m_pSmgr->getSceneNodeFromName("usr/scene/empty/1");

					
					irr::core::vector3df vDir(0,0,1);

					/*

					matrix 직접접근하기.

					(0,0) (0,1) (0,2) (0,3)
					(1,0) (1,1) (1,2) (1,3)
					(2,0) (2,1) (2,2) (2,3)
					(3,0) (3,1) (3,2) (3,3)

					(행,렬)

					*/

					irr::core::matrix4 mat = pNode->getAbsoluteTransformation();
					//Y축 회전 정보만 남기기
					mat(3,0) = 0;
					mat(3,1) = 0;
					mat(3,2) = 0; 

					//벡터에 회전 적용
					mat.transformVect(vDir);					

					pNode->setPosition(
						pNode->getPosition() - vDir);
				}
				else if(event.KeyInput.Key == irr::KEY_LEFT)
				{	
					irr::scene::ISceneNode *pNode = 
						m_pSmgr->getSceneNodeFromName("usr/scene/empty/1");
					
					pNode->setRotation(pNode->getRotation()+irr::core::vector3df(0,5,0));
				}
				else if(event.KeyInput.Key == irr::KEY_RIGHT)
				{	
					irr::scene::ISceneNode *pNode = 
						m_pSmgr->getSceneNodeFromName("usr/scene/empty/1");
					
					pNode->setRotation(pNode->getRotation()+irr::core::vector3df(0,-5,0));
				}

			}
			break;
		case irr::EET_MOUSE_INPUT_EVENT:
			{

			}
			break;
		case irr::EET_USER_EVENT:
			break;
		}
		return false;
	}

	void Update()
	{

	}

};


//void main()
void Lesson31()
{
	Jga_App App; 



	while(App.m_pDevice->run())
	{  

		App.Update();


		App.m_pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

		App.m_pSmgr->drawAll();
		App.m_pGuiEnv->drawAll();

		App.m_pVideo->endScene();
	}
}



