﻿
#include <irrlicht.h>
#include <assert.h>

/////////////////////////////////////////////////
//
//씬에디터 와 연동 사용하기
//
/////////////////////////////////////////////////

class Lsn32_App :  public irr::IEventReceiver
{
public:

	irr::IrrlichtDevice *m_pDevice;

	irr::video::IVideoDriver *m_pVideo;
	irr::scene::ISceneManager *m_pSmgr;
	irr::gui::IGUIEnvironment *m_pGuiEnv;

	int m_nStatus;
	bool m_bLButton;
	irr::core::vector3df m_v3DestPoint;


	Lsn32_App() :
		m_nStatus(0),
		m_bLButton(false)
	{

		

		m_pDevice = irr::createDevice(					
			irr::video::EDT_DIRECT3D9/*,
#if(IRR_VERSION == 16)
		irr::core::dimension2d<irr::u32>(640, 480), 
#else
		irr::core::dimension2d<irr::s32>(640, 480), 
#endif
			32,
			false, true, true,
			this*/
			);
		m_pVideo = m_pDevice->getVideoDriver();
		m_pSmgr = m_pDevice->getSceneManager();
		m_pGuiEnv = m_pDevice->getGUIEnvironment();	

		irr::scene::ICameraSceneNode *pCam = 
			m_pSmgr->addCameraSceneNode(0,irr::core::vector3df(0,50,-20),
			irr::core::vector3df(0,0,0));
		m_pSmgr->setActiveCamera(pCam);

		{
			irr::scene::IAnimatedMesh *pAniMesh = m_pSmgr->getMesh("../res/ninja/ninja.ms3d");
			irr::scene::IAnimatedMeshSceneNode *pNode = m_pSmgr->addAnimatedMeshSceneNode(pAniMesh);

			pNode->setFrameLoop(206,250);
			pNode->setLoopMode(true);
			pNode->setAnimationSpeed(30);
			pNode->setMaterialTexture(0,
				m_pVideo->getTexture("../res/ninja/nskinrd.jpg")
				);
			pNode->setMaterialFlag(irr::video::EMF_LIGHTING,true);		
			pNode->addShadowVolumeSceneNode();

			pNode->setName("usr/scene/skmesh/ninja1");
		}

		{
			irr::scene::ILightSceneNode *pLightNode = m_pSmgr->addLightSceneNode(
				0,irr::core::vector3df(10,10,0));

			pLightNode->getLightData().AmbientColor = irr::video::SColorf(.2f,.2f,.2f,1);
			pLightNode->getLightData().DiffuseColor = irr::video::SColorf(.3f,.3f,.3f,1);
		}

		{
			irr::scene::ISceneNode* pNode = 0;
			irr::scene::IAnimatedMesh* pMesh;
			pMesh = m_pSmgr->addHillPlaneMesh("myHill",
				irr::core::dimension2d<irr::f32>(20,20),
				irr::core::dimension2d<irr::u32>(40,40),0,0,
				irr::core::dimension2d<irr::f32>(0,0),
				irr::core::dimension2d<irr::f32>(10,10));
			pNode = m_pSmgr->addAnimatedMeshSceneNode(pMesh);
			pNode->setPosition(irr::core::vector3df(0,0,0));
			pNode->setMaterialTexture(0, 
				m_pVideo->getTexture("../res/wall.jpg"));	

			irr::scene::ITriangleSelector *pSelector =
				m_pSmgr->createTriangleSelector(pMesh,pNode);
			pNode->setTriangleSelector(pSelector);
			pNode->setName("usr/scene/ground/1");

			pSelector->drop();

		}

		{
			irr::scene::ISceneNode *pNode = m_pSmgr->addSphereSceneNode();
			pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
			pNode->setMaterialFlag(irr::video::EMF_WIREFRAME,true);
			pNode->setName("usr/scene/cursor/1");

			irr::scene::ISceneNodeAnimator *pAnim =
				m_pSmgr->createRotationAnimator(irr::core::vector3df(0,1.f,0));

			pNode->addAnimator(pAnim);
			pAnim->drop();
		}

	}

	~Lsn32_App()
	{
		m_pDevice->drop();
	}	

	//이벤트 핸들러
	virtual bool OnEvent(const irr::SEvent& event)
	{
		switch(event.EventType)
		{
		case irr::EET_GUI_EVENT:
			{

			}
			break;
		case irr::EET_KEY_INPUT_EVENT:
			{
			}
			break;
		case irr::EET_MOUSE_INPUT_EVENT:
			{
				if(event.MouseInput.Event == irr::EMIE_LMOUSE_LEFT_UP)
				{
					m_bLButton = true;
					irr::core::position2di mouse_pos = irr::core::position2di(
						event.MouseInput.X,
						event.MouseInput.Y);	

					irr::scene::ITriangleSelector *pSelector = 
						m_pSmgr->getSceneNodeFromName("usr/scene/ground/1")->getTriangleSelector();

					irr::core::line3df PickRay;
					irr::core::triangle3df tri;

					PickRay = m_pSmgr->getSceneCollisionManager()->getRayFromScreenCoordinates(
						mouse_pos);				

					//픽킹점 구하기
//#if(IRR_VERSION >= 16)
					const irr::scene::ISceneNode *pColNode;
					m_pSmgr->getSceneCollisionManager()->getCollisionPoint(PickRay,pSelector,m_v3DestPoint,tri,pColNode);
//#else
//					pSmgr->getSceneCollisionManager()->getCollisionPoint(Ray,pSelector,v3Intersec,tri);
//#endif

					//피킹된 위치에 커서올리기						
					m_pSmgr->getSceneNodeFromName("usr/scene/cursor/1")->setPosition(
						m_v3DestPoint
						);
				}

			}
			break;
		case irr::EET_USER_EVENT:
			break;
		}
		return false;
	}

	void Update(irr::f32 fDeltaTick)
	{

		switch(m_nStatus)
		{
		case 0:
			if(m_bLButton)
			{
				m_bLButton = false;
				m_nStatus = 1;

				irr::scene::IAnimatedMeshSceneNode *pNode = 
					(irr::scene::IAnimatedMeshSceneNode *)m_pSmgr->getSceneNodeFromName(
					"usr/scene/skmesh/ninja1");
				pNode->setFrameLoop(0,14); //걷기동작으로
				pNode->setLoopMode(true);
			}
			break;
		case 1:
			{
				irr::scene::ISceneNode *pNode = 
					m_pSmgr->getSceneNodeFromName(
						"usr/scene/skmesh/ninja1"
						);

				//이동 하려는 방향벡터구하기
				irr::core::vector3df vDir = m_v3DestPoint - pNode->getAbsolutePosition();
				vDir.normalize(); //벡터 정규화

				pNode->setPosition(pNode->getAbsolutePosition() + (vDir*fDeltaTick*20) );

				irr::core::vector3df vAngle = vDir.getHorizontalAngle();

				irr::core::vector3df vCurAngle = pNode->getRotation();

				//Yaw회전만 적용 시키기
				vCurAngle.Y = vAngle.Y;
				pNode->setRotation(vCurAngle);				

				irr::core::vector3df vDist = m_v3DestPoint - pNode->getAbsolutePosition();

				if( vDist.getLength() < 1 )
				{
					m_nStatus = 0;
					irr::scene::IAnimatedMeshSceneNode *pNode = 
						(irr::scene::IAnimatedMeshSceneNode *)m_pSmgr->getSceneNodeFromName(
						"usr/scene/skmesh/ninja1");
					pNode->setFrameLoop(206,250); //대기동작으로...
					pNode->setLoopMode(true);
				}
			}
			break;
		}

	}

};


void Lesson32()
{
	Lsn32_App App;	

	irr::u32 uLastTick = App.m_pDevice->getTimer()->getTime();
	while(App.m_pDevice->run())
	{		
		irr::u32 uTick = App.m_pDevice->getTimer()->getTime();
		irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f;		
		uLastTick = App.m_pDevice->getTimer()->getTime();

		App.Update(fDelta);

		App.m_pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

		App.m_pSmgr->drawAll();
		App.m_pGuiEnv->drawAll();

		App.m_pVideo->endScene();
	}
}



