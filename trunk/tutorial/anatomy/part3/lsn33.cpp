﻿#include <irrlicht.h>

#include <iostream>
using namespace std;

//항상성 관련 xml 스크립트 시스템 IAttribute
//펙토리 관련 자동화 예제

namespace lsn33 {


	//펙토리가 생성가능한 씬노드 정보 덤프하기
	namespace _01
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9				
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,10,-20), irr::core::vector3df(0,0,0));

			irr::scene::ISceneNodeFactory *pFactory = pSmgr->getDefaultSceneNodeFactory();
			irr::scene::ISceneNodeAnimatorFactory *pAnimFactory = pSmgr->getDefaultSceneNodeAnimatorFactory();

			cout << "--------------------------scene node ------------------------------" << endl;
			//덤프
			{
				irr::u32 i;
				for(i=0;i< pFactory->getCreatableSceneNodeTypeCount();i++)
				{
					cout << i << "/";
					cout << pFactory->getCreateableSceneNodeTypeName( pFactory->getCreateableSceneNodeType(i) ) << "/";
					cout << pFactory->getCreateableSceneNodeType(i) << "/";
					cout << endl;
				}
			}

			cout << endl;


			cout << "--------------------------animator ------------------------------" << endl;

			//애니 펙토리 덤프
			{
				irr::u32 i;
				for(i=0;i< pAnimFactory->getCreatableSceneNodeAnimatorTypeCount();i++)
				{
					cout << i << "/";
					cout << pAnimFactory->getCreateableSceneNodeAnimatorTypeName( pAnimFactory->getCreateableSceneNodeAnimatorType(i) ) << "/";
					cout << pAnimFactory->getCreateableSceneNodeAnimatorType(i) << "/";
					cout << endl;
				}
			}
			cout << endl;

			//펙토리로 씬노드 만들기
			//이렇게 하는거나 addCubeSceneNode로 만드는 것이나  둘다 같다.			
			irr::scene::ISceneNode *pNode = pFactory->addSceneNode("cube",0); 			

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}

	}

	//속성정보 단순 출력 , serializeAttributes 함수로 속성정보 읽기
	/***********
	irr::io::IAttributes 란 무엇인가?

	사용자 응용 프로그램이 씬노드의 속성정보(entity) 에 일반화 하여 기계적으로 접근할수있도록 만든 자동화 도구
	속성정보 제어용 스크립트 시스템 이다.
	이부분이 일리히트에서는 엔티티시스템으로 볼수있다.

	툴을 만들거나 플러그인방식으로 엔진기능을 확장시킬때 
	실행시간에 사용자 어플리캐이션과 엔진 사이의 정보 교환 수단으로 활용할수있다.

	지원되는 주요 기능은 
	getAttributeCount() 항목 갯수 구하기
	getAttributeType(...) 주어진 항목의 타입정보 얻기
	getAttributeAsXXXX(...) 주어진 항목의 데이터 얻기

	빈속성 정보 생성하기
	irr::io::IAttributes *pAttr = pSmgr->getFileSystem()->createEmptyAttributes(pVideo);	

	내용 체우기
	pNode->serializeAttributes(pAttr);

	clear() 함수로 내용을 모두 클리어할수도 있다.
	*************/
	namespace _02
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9				
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,10,-20), irr::core::vector3df(0,0,0));


			irr::scene::ISceneNodeAnimatorFactory *pAnimFactory = pSmgr->getDefaultSceneNodeAnimatorFactory();
			irr::scene::ISceneNodeFactory *pFactory = pSmgr->getDefaultSceneNodeFactory();			

			//이렇게 하는거나 addCubeSceneNode로 만드는 것이나  둘다 같다.
			irr::scene::ISceneNode *pNode = pFactory->addSceneNode("cube",0); //펙토리로 씬노드 만들기

			{
				irr::scene::ISceneNodeAnimator *pAnim = pAnimFactory->createSceneNodeAnimator("rotation",pNode);
				pAnim->drop();
			}

			cout << endl << "-----------------------scene entity------------ " << endl;
			//기본 속성 덤프
			{
				irr::io::IAttributes *pAttr = pSmgr->getFileSystem()->createEmptyAttributes(pVideo);				

				//속성정보 로딩
				pNode->serializeAttributes(pAttr);

				cout << "type name : " << pFactory->getCreateableSceneNodeTypeName(pNode->getType()) << endl;

				irr::u32 i;
				for(i=0;i < pAttr->getAttributeCount() ;i++)
				{
					wcout << pAttr->getAttributeName(i) << "/" << pAttr->getAttributeTypeString(i) << " : ";
					switch(pAttr->getAttributeType(i))
					{
					case irr::io::EAT_VECTOR3D:
						{
							irr::core::vector3df v3 = pAttr->getAttributeAsVector3d(i);
							cout << v3.X << "," << v3.Y << "," << v3.Z << endl;
						}
						break;
					case irr::io::EAT_STRING:
						cout << pAttr->getAttributeAsString(i).c_str() << endl;
						break;
					case irr::io::EAT_BOOL:
						{
							irr::core::stringc str = pAttr->getAttributeAsBool(i) ? "true" : "false";
							cout << str.c_str() << endl;
						}
						break;
					case irr::io::EAT_INT:
						cout << pAttr->getAttributeAsInt(i) << endl;						
						break;					

					case irr::io::EAT_ENUM:		
						cout << pAttr->getAttributeAsEnumeration(i) << endl;						
						break;											

					case irr::io::EAT_FLOAT:
						cout << pAttr->getAttributeAsFloat(i) << endl;
						break;

					case irr::io::EAT_UNKNOWN:
					default:
						cout << "unknown type" << endl;
						break;
					}

				}
				pAttr->drop();
			}	


			//애니메이터 속성 덤프 해보기
			cout << endl << "-----------------------animator ------------ " << endl;			
			{
				irr::io::IAttributes *pAttr = pSmgr->getFileSystem()->createEmptyAttributes(pVideo);				

				//속성정보 로딩
				irr::core::list<irr::scene::ISceneNodeAnimator*>::ConstIterator it = pNode->getAnimators().begin();

				(*it)->serializeAttributes(pAttr);

				//타입 이름 출력
				cout << "animator type : " <<  pAnimFactory->getCreateableSceneNodeAnimatorTypeName( (*it)->getType()) << endl;

				irr::u32 i;
				for(i=0;i < pAttr->getAttributeCount() ;i++)
				{
					wcout << pAttr->getAttributeName(i) << "/" << pAttr->getAttributeTypeString(i) << " : ";
					switch(pAttr->getAttributeType(i))
					{
					case irr::io::EAT_VECTOR3D:
						{
							irr::core::vector3df v3 = pAttr->getAttributeAsVector3d(i);
							cout << v3.X << "," << v3.Y << "," << v3.Z << endl;
						}
						break;
					case irr::io::EAT_STRING:
						cout << pAttr->getAttributeAsString(i).c_str() << endl;
						break;
					case irr::io::EAT_BOOL:
						{
							irr::core::stringc str = pAttr->getAttributeAsBool(i) ? "true" : "false";
							cout << str.c_str() << endl;
						}
						break;
					case irr::io::EAT_INT:
						cout << pAttr->getAttributeAsInt(i) << endl;						
						break;					

					case irr::io::EAT_ENUM:		
						cout << pAttr->getAttributeAsEnumeration(i) << endl;						
						break;											

					case irr::io::EAT_FLOAT:
						cout << pAttr->getAttributeAsFloat(i) << endl;
						break;

					case irr::io::EAT_UNKNOWN:
					default:
						cout << "unknown type" << endl;
						break;
					}

				}
				pAttr->drop();
			}	

			cout << endl << "------------------------matrial------------ " << endl;

			//메트리얼 속성 덤프
			{
				irr::io::IAttributes *pAttr;// = pSmgr->getFileSystem()->createEmptyAttributes(pVideo);	
				pAttr = pVideo->createAttributesFromMaterial(pNode->getMaterial(0));			

				irr::u32 i;
				for(i=0;i < pAttr->getAttributeCount() ;i++)
				{
					wcout << pAttr->getAttributeName(i) << "/" << pAttr->getAttributeTypeString(i) << " : ";

					switch(pAttr->getAttributeType(i))
					{
					case irr::io::EAT_VECTOR3D:
						{
							irr::core::vector3df v3 = pAttr->getAttributeAsVector3d(i);
							cout << v3.X << "," << v3.Y << "," << v3.Z << endl;
						}
						break;
					case irr::io::EAT_STRING:
						cout << pAttr->getAttributeAsString(i).c_str() << endl;
						break;
					case irr::io::EAT_BOOL:
						{
							irr::core::stringc str = pAttr->getAttributeAsBool(i) ? "true" : "false";
							cout << str.c_str() << endl;
						}
						break;
					case irr::io::EAT_INT:
						cout << pAttr->getAttributeAsInt(i) << endl;						
						break;					

					case irr::io::EAT_ENUM:		
						cout << pAttr->getAttributeAsEnumeration(i) << endl;	
						break;											
					case irr::io::EAT_FLOAT:
						cout << pAttr->getAttributeAsFloat(i) << endl;
						break;
					case irr::io::EAT_UNKNOWN:
					default:
						cout << "unknown type" << endl;
						break;
					}				
				}
				pAttr->drop();
			}

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}

	}

	//속성정보 변경해서 객체에 전달하기(내보내기)
	//deserializeAttributes
	namespace _03
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9				
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,10,-20), irr::core::vector3df(0,0,0));

			//이렇게 하는거나 addCubeSceneNode로 만드는 것이나  둘다 같다.
			irr::scene::ISceneNodeFactory *pFactory = pSmgr->getDefaultSceneNodeFactory();
			irr::scene::ISceneNode *pNode = pFactory->addSceneNode("cube",0); //펙토리로 씬노드 만들기

			{
				//기본 엔티티 속성에 대한 속성객체 적용시키기
				irr::io::IAttributes *pAttr = pSmgr->getFileSystem()->createEmptyAttributes(pVideo);		
				pNode->serializeAttributes(pAttr);

				pAttr->setAttribute("Size",5);
				pNode->deserializeAttributes(pAttr);

				//새로 생성하기전에 해제해줘야한다.
				pAttr->drop();

				//속성 객체를
				//메트리얼에 적용시키기
				pAttr = pVideo->createAttributesFromMaterial(pNode->getMaterial(0));	

				pAttr->setAttribute("Lighting",false);
				pAttr->setAttribute("Wireframe",true);

				pVideo->fillMaterialStructureFromAttributes(	pNode->getMaterial(0), pAttr);				

				pAttr->drop();
			}			

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			while(pDevice->run())
			{	
				static irr::u32 uLastTick=0;
				irr::u32 uTick = pDevice->getTimer()->getTime();						
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = uTick;

				//프레임레이트 갱신, 삼각형수 표시
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				if(pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140)))
				{					

					pSmgr->drawAll();
					pGuiEnv->drawAll();						

					pVideo->endScene();	
				}
			}

			pDevice->drop();
		}

	}

	//속성정보 파일에서 읽기 serialize 예제
	namespace _04
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_NULL				
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");


			//
			{

				//xml파일 로딩
				irr::io::IXMLReader *pXml = pDevice->getFileSystem()->createXMLReader("test_Attr.xml");

				/*

				다음 코드는 정형화된 xml스크립트를 읽어 들이는 기능의 예이다.
				모든 내용은 <attributes> </attributes> 태그안에 들어있어야 하며

				<데이터형 name="이름" value="값" />

				예>
				<string name="Name" value="gbox3d" />


				*/
				irr::io::IAttributes *pAttr = pSmgr->getFileSystem()->createEmptyAttributes(pVideo);


				while(pAttr->read(pXml)  )
				{
					if(pAttr->getAttributeCount() == 0) break;
					irr::u32 i;
					for(i=0;i < pAttr->getAttributeCount() ;i++)
					{
						wcout << pAttr->getAttributeName(i) << "/" << pAttr->getAttributeTypeString(i) << " : ";
						switch(pAttr->getAttributeType(i))
						{
						case irr::io::EAT_VECTOR3D:
							{
								irr::core::vector3df v3 = pAttr->getAttributeAsVector3d(i);
								cout << v3.X << "," << v3.Y << "," << v3.Z << endl;
							}
							break;
						case irr::io::EAT_STRING:
							cout << pAttr->getAttributeAsString(i).c_str() << endl;
							break;
						case irr::io::EAT_BOOL:
							{
								irr::core::stringc str = pAttr->getAttributeAsBool(i) ? "true" : "false";
								cout << str.c_str() << endl;
							}
							break;
						case irr::io::EAT_INT:
							cout << pAttr->getAttributeAsInt(i) << endl;						
							break;					

						case irr::io::EAT_ENUM:		
							cout << pAttr->getAttributeAsEnumeration(i) << endl;						
							break;											

						case irr::io::EAT_FLOAT:
							cout << pAttr->getAttributeAsFloat(i) << endl;
							break;

						case irr::io::EAT_UNKNOWN:
						default:
							cout << "unknown type" << endl;
							break;
						}

					}

					{
						irr::core::vector3df test_pos = pAttr->getAttributeAsVector3d("Position");
						cout << test_pos.X << "," << test_pos.Y << "," << test_pos.Z <<  endl;
					}
					
					irr::core::position2di test_pos = pAttr->getAttributeAsPosition2d("Position");
					cout << test_pos.X << "," << test_pos.Y << endl;
					
				}

				

				pAttr->drop();
				pXml->drop();
			}

			pDevice->drop();

			//디버그모드로 실행시켰을때 콘솔창 자동 닫힘방지^^;
			system("pause");
		}

	}

	//속성정보 와이드 캐릭터 형으로 파일에 쓰기
	namespace _05
	{
		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_NULL				
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");


			//
			{
				irr::io::IAttributes *pAttr = pSmgr->getFileSystem()->createEmptyAttributes(pVideo);	


				irr::core::array<char *> enumtype;
				enumtype.push_back("EDT_NULL");
				enumtype.push_back("EDT_SOFTWARE");
				enumtype.push_back("EDT_BURNINGSVIDEO");
				enumtype.push_back("EDT_DIRECT3D8");
				enumtype.push_back("EDT_DIRECT3D9");
				enumtype.push_back("EDT_OPENGL");
				enumtype.push_back(NULL);

				pAttr->addEnum("m_DriverType","EDT_DIRECT3D9",(char **)&enumtype[0]);

				//어래이 태스트
				irr::core::array<irr::core::stringw> testArray;
				testArray.push_back("test1");
				testArray.push_back("test2");
				testArray.push_back("test3");
				pAttr->addArray("arraytest",testArray);

				pAttr->addPosition2d("test",irr::core::position2di(100,200));

				pAttr->addBinary("test_bin",pDevice,sizeof(irr::IrrlichtDevice));


				//xml파일 쓰기
				irr::io::IXMLWriter *pXml = pDevice->getFileSystem()->createXMLWriter("xml_test/test_Attr_w.xml");

				//주의사항 헤더를 써주지않으면 무슨 코드 포멧의(8,16,32문자형인지 모름) xml파일인지 알수 없으므로 다시 읽을때(pAttr->read(...) ) 에러가 발생할수있다. 
				//처음 파일로 출력할때는 반드시 헤더를 한번 출력해줘야한다.
				//	pXml->writeXMLHeader(); 를 사용할수도있다. 당연한 말이겠지만 저장을 시작할때 한번 만 써주면된다.
				// 세번째 인자는 태그이름이다. 디폴트 인자(NULL)를 쓰면 기본적으로 <Attribute> </Attribute> 가 된다.
				pAttr->write(
					pXml,
					true//헤더 출력 할것인가?
					);				
				pXml->drop();

				pAttr->drop();
			}

			pDevice->drop();

			//디버그모드로 실행시켰을때 콘솔창 자동 닫힘방지^^;
			system("pause");
		}

	}

	//씬노드 xml 파일출력 예제
	//writeSceneNode 함수 직접 만들어 보기 예제
	namespace _06
	{
		void writeSceneNode(
			irr::io::IXMLWriter* writer, 
			irr::scene::ISceneNode* node, 
			irr::scene::ISceneUserDataSerializer* userDataSerializer)
		{
			if (!writer || !node || node->isDebugObject())
				return;

			const wchar_t* name = L"node";						
			{				
				writer->writeElement(
					name,
					false, 					
					L"type",
					irr::core::stringw(node->getSceneManager()->getSceneNodeTypeName(node->getType())).c_str()
					);
			}

			writer->writeLineBreak();
			writer->writeLineBreak();

			// write properties

			irr::io::IAttributes* attr = node->getSceneManager()->getFileSystem()->createEmptyAttributes();
			node->serializeAttributes(attr);

			if (attr->getAttributeCount() != 0)
			{
				attr->write(writer);
				writer->writeLineBreak();
			}

			// write materials

			if ( node->getMaterialCount() )//&& node->getSceneManager()->getVideoDriver())
			{
				const wchar_t* materialElement = L"materials";

				writer->writeElement(materialElement);
				writer->writeLineBreak();

				for (irr::u32 i=0; i < node->getMaterialCount(); ++i)
				{
					irr::io::IAttributes* tmp_attr =
						node->getSceneManager()->getVideoDriver()->createAttributesFromMaterial(node->getMaterial(i));
					tmp_attr->write(writer);
					tmp_attr->drop();
				}

				writer->writeClosingTag(materialElement);
				writer->writeLineBreak();
			}

			// write animators

			if (!node->getAnimators().empty())
			{
				const wchar_t* animatorElement = L"animators";
				writer->writeElement(animatorElement);
				writer->writeLineBreak();

				irr::core::list<irr::scene::ISceneNodeAnimator*>::ConstIterator it = node->getAnimators().begin();
				for (; it != node->getAnimators().end(); ++it)
				{
					attr->clear();
					attr->addString("Type", 
						node->getSceneManager()->getAnimatorTypeName((*it)->getType()));

					(*it)->serializeAttributes(attr);

					attr->write(writer);
				}

				writer->writeClosingTag(animatorElement);
				writer->writeLineBreak();
			}

			// write possible user data

			if ( userDataSerializer )
			{
				irr::io::IAttributes* userData = userDataSerializer->createUserData(node);
				if (userData)
				{
					const wchar_t* userDataElement = L"userData";

					writer->writeLineBreak();
					writer->writeElement(userDataElement);
					writer->writeLineBreak();

					userData->write(writer);

					writer->writeClosingTag(userDataElement);
					writer->writeLineBreak();
					writer->writeLineBreak();

					userData->drop();
				}
			}

			// write children

			irr::core::list<irr::scene::ISceneNode*>::ConstIterator it = node->getChildren().begin();
			for (; it != node->getChildren().end(); ++it)
				writeSceneNode(writer, (*it), userDataSerializer);

			attr->drop();

			writer->writeClosingTag(name);
			writer->writeLineBreak();
			writer->writeLineBreak();
		}



		void main()
		{
			irr::IrrlichtDevice *pDevice = irr::createDevice(					
				irr::video::EDT_DIRECT3D9
				);

			pDevice->setWindowCaption(L"Type-A1");

			irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
			irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
			irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

			//char *str = \\"RES_DIR\\";
			pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");


			pSmgr->addCameraSceneNode(0, irr::core::vector3df(0,10,-20), irr::core::vector3df(0,0,0));

			//태스트 용 씬그라프 생성
			{
				irr::scene::ISceneNode *pNode_root = pSmgr->addDummyTransformationSceneNode();
				pNode_root->setName("usr/scene/dummy/test_root");

				irr::scene::ISceneNode *pNode = pSmgr->addCubeSceneNode();

				pNode->setMaterialFlag(irr::video::EMF_LIGHTING,false);
				pNode->setMaterialTexture(0,pVideo->getTexture("wall.jpg"));

				pNode->setName("usr/scene/cube/1");

				pNode_root->addChild(pNode);
			}

			{//씬노드 직렬화 저장하기

				irr::io::IXMLWriter *pXml = pDevice->getFileSystem()->createXMLWriter("scene/scene_17_1attr.xml");
				pXml->writeXMLHeader();
				irr::scene::ISceneNode *pNode = pSmgr->getSceneNodeFromName("usr/scene/dummy/test_root");
				writeSceneNode(pXml,pNode,NULL);								
				pXml->drop();
			}

			{//씬통체로 저장하기

				pSmgr->saveScene("scene/17_1.xml");	
			}

			//프레임 레이트 표시용 유아이
			irr::gui::IGUIStaticText *pstextFPS = 
				pGuiEnv->addStaticText(L"Frame rate",irr::core::rect<irr::s32>(0,0,100,20),true,true,0,100,true);

			irr::u32 uLastTick = pDevice->getTimer()->getTime();
			while(pDevice->run())
			{	
				irr::u32 uTick = pDevice->getTimer()->getTime();
				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
				uLastTick = pDevice->getTimer()->getTime();

				//프레임레이트 갱신
				{
					wchar_t wszbuf[256];
					swprintf(wszbuf,L"Frame rate : %d\n TriAngle: %d",pVideo->getFPS(),pVideo->getPrimitiveCountDrawn());
					pstextFPS->setText(wszbuf);
				}

				pVideo->beginScene(true, true, irr::video::SColor(255,100,101,140));

				pSmgr->drawAll();
				pGuiEnv->drawAll();		

				pVideo->endScene();	
			}

			pDevice->drop();
		}
	}
}