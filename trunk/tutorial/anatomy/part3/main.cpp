﻿#include <iostream>
#include <map>
#include <string>

typedef void (*fPtr)();
std::map<std::string, fPtr > mapExam;

namespace lsn15 {
	namespace _00{void main();}//skybox
	namespace _01{void main();}//sky dome
	namespace _02{void main();}//water 
	namespace _03{void main();}//billboard
	namespace _04{void main();}//tree billboard
	namespace _05{void main();}//billboard matrix
	namespace _06{void main();} //빌보드텍스트, & 텍스트노드
	namespace _07{void main();}//normal map
	namespace _08{void main();}//애니메이션 메쉬에 노멀멥 적용하기, aniamteMesh를 탄젠트 메쉬로 바꾸기
}

//클로닝관련 예제
namespace lsn16 {
	namespace _00{void main();} //정적씬노드 클로닝
	namespace _01{void main();} //동적씬노드 클로닝
}

//항상성 관련 xml 스크립트 시스템 IAttribute
//펙토리 관련 자동화 예제
namespace lsn33 {
	namespace _01{void main();}
	namespace _02{void main();}
	namespace _03{void main();}
	namespace _04{void main();}
	namespace _05{void main();}
}

//라이트 관리자 예제
namespace lsn34 {
	namespace _01{void main();}
}

namespace sample01 {
	namespace _00 {void main();}
}

void InitModule()
{
	mapExam["15"] = lsn15::_00::main;
	mapExam["15-1"] = lsn15::_01::main;
	mapExam["15-2"] = lsn15::_02::main;
	mapExam["15-3"] = lsn15::_03::main;
	mapExam["15-4"] = lsn15::_04::main;
	mapExam["15-5"] = lsn15::_05::main;
	mapExam["15-6"] = lsn15::_06::main;
	mapExam["15-7"] = lsn15::_07::main;
	mapExam["15-8"] = lsn15::_08::main;

	mapExam["16"] = lsn16::_00::main;
	mapExam["16-1"] = lsn16::_01::main;

	mapExam["33-1"] = lsn33::_01::main;
	mapExam["33-2"] = lsn33::_02::main;
	mapExam["33-3"] = lsn33::_03::main;
	mapExam["33-4"] = lsn33::_04::main;
	mapExam["33-5"] = lsn33::_05::main;
	
	mapExam["34-1"] = lsn34::_01::main;

	mapExam["s1-0"] = sample01::_00::main;


	/*mapExam["1"] = chapter1::main;
	mapExam["2"] = chapter2::main;
	mapExam["3"] = chapter3::main;
	mapExam["4"] = chapter4::main;
	mapExam["5"] = chapter5::main;
	mapExam["6"] = chapter6::main;*/
}

void main()
{
#ifdef _DEBUG
	::_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	//_CrtSetBreakAlloc(1933);
#endif

	std::cout << "input sample id : ";

	std::string input;
	std::cin >> input;

	InitModule(); //태이블 초기화
	

	if(mapExam.find(input) != mapExam.end())
	{
		mapExam[input]();
	}
	else
	{
		std::cout << "not found fuction" << std::endl;
	}
}