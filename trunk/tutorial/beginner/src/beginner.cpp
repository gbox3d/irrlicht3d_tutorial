//============================================================================
// Name        : beginner.cpp
// Author      : gbox3d
// Version     :
// Copyright   : (c)GNA2011
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <map>
#include <irrlicht.h>
using namespace std;

#include <string>

typedef void (*fPtr)();
std::map<std::string, fPtr > mapExam;


//
//namespace lsn01 {
//	namespace _00{void main();}
//}

namespace chapter1 {

namespace lsn01 {void main();}

namespace lsn02 {
	void main();
	namespace _01 {void main();}
	namespace _02 {void main();} //�ؽ��� ���ο���
}

namespace lsn03 {
	void main();
	namespace _01 {void main();}
	namespace _02 {void main();}
}

//namespace test {void main();}

}//chapter1

int main() {

	//mapExam["test"] = chapter1::test::main;

	mapExam["1-1"] = chapter1::lsn01::main;
	mapExam["1-2"] = chapter1::lsn02::main;
	mapExam["1-2-1"] = chapter1::lsn02::_01::main;
	mapExam["1-2-2"] = chapter1::lsn02::_02::main;

	mapExam["1-3"] = chapter1::lsn03::main;
	mapExam["1-3-1"] = chapter1::lsn03::_01::main;
	mapExam["1-3-2"] = chapter1::lsn03::_02::main;
//	mapExam["0-1"] = lsn00::_01::main;
//	mapExam["0-2"] = lsn00::_02::main;
//	mapExam["0-3"] = lsn00::_03::main;


	cout << "beginner guide sample" << endl; // prints beginner guide sample

	cout << "input exam : ";
	std::string input;
	std::cin >> input;

	if (mapExam.find(input) != mapExam.end()) {
		mapExam[input]();
	} else {
		std::cout << "not found fuction" << std::endl;
	}

	return 0;
}
