﻿/*
 * lsn01.cpp
 *
 *  Created on: 2011. 4. 18.
 *      Author: gbox3d
 */

#include <irrlicht.h>

namespace chapter1 {

namespace lsn01 {

class CTestApp :  public irr::IEventReceiver
{
public:

	irr::core::map<irr::EKEY_CODE,bool> m_KeyBuffer;

	bool m_bDirCmd[8];
	bool m_bTrigerCmd[4];

	irr::core::recti m_rectDirBtn[8];
	irr::core::recti m_rectCmdBtn[4];


	CTestApp(irr::core::vector2di pos)
	{

		m_rectDirBtn[0] = irr::core::recti(0,0,32,32) + pos; //up
		m_rectDirBtn[1] = m_rectDirBtn[0] + irr::core::vector2di(32,0);
		m_rectDirBtn[2] = m_rectDirBtn[1] + irr::core::vector2di(0,32);
		m_rectDirBtn[3] = m_rectDirBtn[2] + irr::core::vector2di(0,32);
		m_rectDirBtn[4] = m_rectDirBtn[3] + irr::core::vector2di(-32,0);
		m_rectDirBtn[5] = m_rectDirBtn[4] + irr::core::vector2di(-32,0);
		m_rectDirBtn[6] = m_rectDirBtn[5] + irr::core::vector2di(0,-32);
		m_rectDirBtn[7] = m_rectDirBtn[6] + irr::core::vector2di(0,-32);

		m_rectCmdBtn[0] = m_rectDirBtn[0] + irr::core::vector2di(32*4,0);
		m_rectCmdBtn[1] = m_rectCmdBtn[0] + irr::core::vector2di(32,0);
		m_rectCmdBtn[2] = m_rectCmdBtn[1] + irr::core::vector2di(32,0);
		m_rectCmdBtn[3] = m_rectCmdBtn[2] + irr::core::vector2di(32,0);

		int i;
		for (i = 0; i < 8; i++) {
			m_bDirCmd[i] = false;
		}
		for (i = 0; i < 4; i++) {
			m_bTrigerCmd[i] = false;
		}

	}

	virtual bool OnEvent(const irr::SEvent& event)
	{
		switch(event.EventType)
		{
		case irr::EET_GUI_EVENT:
			{

			}
			break;
		case irr::EET_KEY_INPUT_EVENT: {
			printf("%d\n", event.KeyInput.Key);
			if (event.KeyInput.PressedDown) {
				m_KeyBuffer[event.KeyInput.Key] = true;
			} else {
				m_KeyBuffer[event.KeyInput.Key] = false;

			}
		}
			break;
		case irr::EET_MOUSE_INPUT_EVENT:
			{
				printf("mouse event %d, %d,%d\n", event.MouseInput.ButtonStates,
						event.MouseInput.X, event.MouseInput.Y);
				int i;
				for (i = 0; i < 8; i++) {

					if (m_rectDirBtn[i].isPointInside(
							irr::core::vector2di(event.MouseInput.X,
									event.MouseInput.Y))
							&& event.MouseInput.isLeftPressed() == true) {
						m_bDirCmd[i] = true;
					} else {
						m_bDirCmd[i] = false;

					}
				}

				for(i=0;i<4;i++)
				{
					if (m_rectCmdBtn[i].isPointInside(
							irr::core::vector2di(event.MouseInput.X,
									event.MouseInput.Y))
							&& event.MouseInput.isLeftPressed() == true) {
						m_bTrigerCmd[i] = true;
					} else {
						m_bTrigerCmd[i] = false;

					}
				}

			}
			break;
		case irr::EET_USER_EVENT:
			break;
		default:
			break;
		}
		return false;
	}

	void DrawControlButton(irr::video::IVideoDriver *pVideo)
	{
		int i;
		for(i=0;i<8;i++)
		{
			if(m_bDirCmd[i] == true)
			{
				pVideo->draw2DRectangle(irr::video::SColor(255,255,255,0), m_rectDirBtn[i]);


			}
			else
			{
				pVideo->draw2DRectangleOutline(m_rectDirBtn[i]);
			}
		}

		for (i = 0; i < 4; i++) {
			if(m_bTrigerCmd[i] == true)
			{
				pVideo->draw2DRectangle(irr::video::SColor(255,255,255,0), m_rectCmdBtn[i]);
				//pVideo->draw2DRectangleOutline(m_rectCmdBtn[i]);
			}
			else
			{
				pVideo->draw2DRectangleOutline(m_rectCmdBtn[i]);
			}
		}

	}

} theApp(irr::core::vector2di(100,300));


//기본 A1형
void main() {
	irr::IrrlichtDevice *pDevice = irr::createDevice(
	//irr::video::EDT_DIRECT3D9
			irr::video::EDT_OPENGL);//,irr::core::dimension2du(800,600),true);

	pDevice->setEventReceiver(&theApp);

	pDevice->setWindowCaption(L"Type-A1");

	irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
	irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
	irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

	//pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

	pSmgr->addCameraSceneNode(0, irr::core::vector3df(0, 10, -20),
			irr::core::vector3df(0, 0, 0));

	//큐브 노드 만들기
	{
		irr::scene::ISceneNode *pNode = pSmgr->addCubeSceneNode();
		pNode->setMaterialFlag(irr::video::EMF_LIGHTING, false);
		pNode->setMaterialFlag(irr::video::EMF_WIREFRAME, true);
	}

	//프레임 레이트 표시용 유아이
	irr::gui::IGUIStaticText *pstextFPS = pGuiEnv->addStaticText(L"Frame rate",
			irr::core::rect<irr::s32>(0, 0, 100, 20), true, true, 0, 100, true);

	while (pDevice->run()) {


//		static irr::u32 uLastTick = 0;
//		irr::u32 uTick = pDevice->getTimer()->getTime();
//		irr::f32 fDelta = ((float) (uTick - uLastTick)) / 1000.f; //델타값 구하기
//		uLastTick = uTick;

		//프레임레이트 갱신, 삼각형수 표시
		{
			wchar_t wszbuf[256];
			swprintf(wszbuf,256 ,L"Frame rate : %d\n TriAngle: %d",
					pVideo->getFPS(), pVideo->getPrimitiveCountDrawn());
			//swprintf(wszbuf,256,L"");
			pstextFPS->setText(wszbuf);
		}

		pVideo->beginScene(true, true, irr::video::SColor(255, 100, 101, 140));

		pSmgr->drawAll();
		pGuiEnv->drawAll();

		theApp.DrawControlButton(pVideo);

		pVideo->endScene();
	}

	pDevice->drop();
}


}


}
