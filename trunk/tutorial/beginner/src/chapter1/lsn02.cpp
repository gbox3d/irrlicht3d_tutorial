﻿#include <irrlicht.h>
#include <iostream>
//#include <string>
//#include <vector>

//기본 그리기 예제
namespace chapter1 {
//삼각형 & 박스 그리기
namespace lsn02 {
//기본 A2형
//직접 그리기 프레임웍
void main() {
	irr::IrrlichtDevice *pDevice = irr::createDevice(irr::video::EDT_OPENGL);

	pDevice->setWindowCaption(L"draw 3d somethings");

	irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
	irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
	irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

	pSmgr->addCameraSceneNode(0, irr::core::vector3df(0, 0, -5),
			irr::core::vector3df(0, 0, 0));

	while (pDevice->run()) {
		//				static irr::u32 uLastTick=0;
		//				irr::u32 uTick = pDevice->getTimer()->getTime();
		//				irr::f32 fDelta = ((float)(uTick - uLastTick)) / 1000.f; //델타값 구하기
		//				uLastTick = uTick;

		pVideo->beginScene(true, true, irr::video::SColor(255, 100, 101, 140));

		pSmgr->drawAll();
		pGuiEnv->drawAll();

		//직접 그리기
		{
			irr::core::matrix4 mat;//단위행렬로초기화
			mat.makeIdentity();
			pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

			irr::video::SMaterial m;
			m.Lighting = false; //라이트를꺼야 색이 제데로나온다.
			m.ZBuffer = false;

			pVideo->setMaterial(m);
		}

		//직선그리기
		{
			pVideo->draw3DLine(irr::core::vector3df(0, 0, 0),
					irr::core::vector3df(-3, 3, 0));
		}

		//삼각형그리기
		{
			irr::core::triangle3df tri(irr::core::vector3df(0, 0, 0),
					irr::core::vector3df(1, 1, 0),
					irr::core::vector3df(2, 0, 0));
			pVideo->draw3DTriangle(tri, irr::video::SColor(255, 255, 255, 0));
		}

		//박스 그리기
		{
			irr::core::aabbox3df box(irr::core::vector3df(-1, -1, -1),
					irr::core::vector3df(1, 1, 1));
			pVideo->draw3DBox(box, irr::video::SColor(255, 255, 0, 255));
		}

		pVideo->endScene();
	}
	pDevice->drop();
}


namespace _01 {

//drawVertexPrimitiveList 로 다양한 방법으로 삼각형 단위로 그리기

void main() {
	irr::IrrlichtDevice *pDevice = irr::createDevice(irr::video::EDT_OPENGL);

	pDevice->setWindowCaption(L"draw variable type of triangle");

	irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
	irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
	irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

	pSmgr->addCameraSceneNode(0, irr::core::vector3df(0, 0, -5),
			irr::core::vector3df(0, 0, 0));

	//std::vector<irr::video::S3DVertex> Vertices;

	irr::core::array<irr::video::S3DVertex> Vertices;

	/*

	 1---------2
	 |         |
	 |         |
	 |         |
	 |         |
	 0---------3

	 */

	Vertices.push_back(
			irr::video::S3DVertex(-.5, -.5, 0, 0, 0, -1,
					irr::video::SColor(0, 0, 255, 255), 0, 1));
	Vertices.push_back(
			irr::video::S3DVertex(-.5, .5, 0, 0, 0, -1,
					irr::video::SColor(0, 255, 0, 255), 0, 0));
	Vertices.push_back(
			irr::video::S3DVertex(.5, .5, 0, 0, 0, -1,
					irr::video::SColor(0, 255, 255, 0), 1, 0));
	Vertices.push_back(
			irr::video::S3DVertex(.5, -.5, 0, 0, 0, -1,
					irr::video::SColor(0, 0, 255, 0), 1, 1));

	//tri fan
	//std::vector<irr::u16> Indice_fan;
	irr::core::array<irr::u16> Indice_fan;

	//triangle fan 인덱스 배열만들기
	Indice_fan.push_back(0);
	Indice_fan.push_back(1);
	Indice_fan.push_back(2);
	Indice_fan.push_back(3);

	//tri strip
	irr::core::array<irr::u16> Indice4Strip;

	//인덱스 배열만들기
	//스트립의 특징은 다음에 올 정점이 이전 삼각형의 2,3번째 정점과 인점하게해야한다.
	//첫번째 정점이 새로 추가될삼각형에 포함되면 안된다.
	//2,3,번째 +  새로 추가될 삼각형 이런식이다.
	Indice4Strip.push_back(0);
	Indice4Strip.push_back(1);
	Indice4Strip.push_back(3);
	Indice4Strip.push_back(2);

	//tri list
	irr::core::array<irr::u16> Indice4TriList;

	//인덱스 배열만들기
	Indice4TriList.push_back(0);
	Indice4TriList.push_back(1);
	Indice4TriList.push_back(2);
	Indice4TriList.push_back(2);
	Indice4TriList.push_back(3);
	Indice4TriList.push_back(0);

	while (pDevice->run()) {

//		static irr::u32 uLastTick = 0;
//		//밀리세컨드값얻기
//		irr::u32 uTick = pDevice->getTimer()->getTime();
//		irr::f32 fDelta = ((float) (uTick - uLastTick)) / 1000.f; //델타값 구하기
//		uLastTick = uTick;

		pVideo->beginScene(true, true, irr::video::SColor(255, 100, 101, 140));

		pSmgr->drawAll();
		pGuiEnv->drawAll();

		//EPT_TRIANGLE_FAN 그리기
		{
			irr::core::matrix4 mat;//단위행렬로초기화
			mat.makeIdentity();
			mat.setTranslation(irr::core::vector3df(-2, 0, 0));
			pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

			irr::video::SMaterial m;
			m.Lighting = false; //라이트를꺼야 색이 제데로나온다.
			//m.ZBuffer = false;
			//m.setTexture(TextureLayer[0].Texture =

			pVideo->setMaterial(m);
		}

		pVideo->drawVertexPrimitiveList(
				(irr::video::S3DVertex *) (&Vertices[0]), 4,
				(irr::u16 *) &Indice_fan[0], 2, irr::video::EVT_STANDARD,
				irr::scene::EPT_TRIANGLE_FAN);

		//EPT_TRIANGLE_STRIP 그리기
		{
			irr::core::matrix4 mat;//단위행렬로초기화
			mat.makeIdentity();
			mat.setTranslation(irr::core::vector3df(0, 0, 0));
			pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

			irr::video::SMaterial m;
			m.Lighting = false; //라이트를꺼야 색이 제데로나온다.
			//m.ZBuffer = false;

			pVideo->setMaterial(m);
		}

		pVideo->drawVertexPrimitiveList(
				(irr::video::S3DVertex *) (&Vertices[0]), 4,
				(irr::u16 *) &Indice4Strip[0], 2, irr::video::EVT_STANDARD,
				irr::scene::EPT_TRIANGLE_STRIP);

		//EPT_TRIANGLES 그리기
		{
			irr::core::matrix4 mat;//단위행렬로초기화
			mat.makeIdentity();
			mat.setTranslation(irr::core::vector3df(2, 0, 0));
			pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화

			irr::video::SMaterial m;
			m.Lighting = false; //라이트를꺼야 색이 제데로나온다.
			//m.ZBuffer = false;

			pVideo->setMaterial(m);
		}

		pVideo->drawVertexPrimitiveList(
				(irr::video::S3DVertex *) (&Vertices[0]), 4,
				(irr::u16 *) &Indice4TriList[0], 2, irr::video::EVT_STANDARD,
				irr::scene::EPT_TRIANGLES);

		pVideo->endScene();
	}
	pDevice->drop();
}

} //_01

namespace _02 {
//drawIndexedTriangleList sample

void main() {

	irr::IrrlichtDevice *pDevice = irr::createDevice(irr::video::EDT_OPENGL);
	pDevice->setWindowCaption(L"draw triangle list sample");
	irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
	irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
	irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

	//뷰,투영변환을 씬매니져에 맡긴다.
	pSmgr->addCameraSceneNode(0, irr::core::vector3df(0, 0, -1),
			irr::core::vector3df(0, 0, 0));

	/*
	 1---------2
	 |         |
	 | 	       |
	 0---------3
	 */
	irr::core::array<irr::video::S3DVertex> Vertices;
	Vertices.push_back(
			irr::video::S3DVertex(-.5, -.5, 0, 0, 0, -1,
					irr::video::SColor(0, 0, 255, 255), 0, 1));
	Vertices.push_back(
			irr::video::S3DVertex(-.5, .5, 0, 0, 0, -1,
					irr::video::SColor(0, 255, 0, 255), 0, 0));
	Vertices.push_back(
			irr::video::S3DVertex(.5, .5, 0, 0, 0, -1,
					irr::video::SColor(0, 255, 255, 0), 1, 0));
	Vertices.push_back(
			irr::video::S3DVertex(.5, -.5, 0, 0, 0, -1,
					irr::video::SColor(0, 0, 255, 0), 1, 1));
	irr::core::array<irr::u16> Indice;
	//0,1,2,3,0,2
	Indice.push_back(0);
	Indice.push_back(1);
	Indice.push_back(2);
	Indice.push_back(3);
	Indice.push_back(0);
	Indice.push_back(2);

	while (pDevice->run()) {

//		static irr::u32 uLastTick = 0;
//		//밀리세컨드값얻기
//		irr::u32 uTick = pDevice->getTimer()->getTime();
//		irr::f32 fDelta = ((float) (uTick - uLastTick)) / 1000.f; //델타값 구하기
//		uLastTick = uTick;
//

		pVideo->beginScene(true, true, irr::video::SColor(255, 100, 101, 140));
		pSmgr->drawAll();
		pGuiEnv->drawAll();
		//직접 그리기
		{
			irr::core::matrix4 mat;//단위행렬로초기화
			mat.makeIdentity();
			pVideo->setTransform(irr::video::ETS_WORLD, mat); //변환초기화
			irr::video::SMaterial m;
			m.Lighting = false; //라이트를꺼야 색이 제데로나온다.
			m.setTexture(0,pVideo->getTexture("./res/chelnov.jpeg"));
			//m.ZBuffer = false;
			pVideo->setMaterial(m);
		}
		pVideo->drawIndexedTriangleList(
				(irr::video::S3DVertex *) (&Vertices[0]), 4, //정점 갯수
				(irr::u16 *) &Indice[0], 2 //삼각형갯수
		);
		pVideo->endScene();
	}
	pDevice->drop();
}

} //_02




} //lsn02
} //chapeter1
