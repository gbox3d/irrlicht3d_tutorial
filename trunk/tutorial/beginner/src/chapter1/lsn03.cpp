﻿/*
 * lsn03.cpp
 *
 *  Created on: 2011. 4. 24.
 *      Author: gbox3d
 */

#include <irrlicht.h>

namespace chapter1 {

namespace lsn03 {

    

void main() {
    
    
	irr::IrrlichtDevice *pDevice = irr::createDevice(irr::video::EDT_OPENGL);

    
	pDevice->setWindowCaption(L"sample 3");
	irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
	irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
	irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

	pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

	pSmgr->addCameraSceneNode(0, irr::core::vector3df(0, 30, -40),
			irr::core::vector3df(0, 5, 0));

	{
		irr::scene::ISceneNode *pNode = pSmgr->addCubeSceneNode();

		pNode->setMaterialFlag(irr::video::EMF_LIGHTING, false);
		pNode->setMaterialTexture(0, pVideo->getTexture("t351sml.jpg"));
	}

	{
		irr::scene::IMeshSceneNode *pNode =
				(irr::scene::IMeshSceneNode *) pSmgr->addSphereSceneNode();

		pNode->setMaterialFlag(irr::video::EMF_LIGHTING, false);
		pNode->setMaterialTexture(0, pVideo->getTexture("earth.jpg"));

		pNode->setPosition(irr::core::vector3df(15, 0, 0));
	}

	while (pDevice->run()) {
		pVideo->beginScene(true, true, irr::video::SColor(255, 100, 101, 140));
		pSmgr->drawAll();
		pGuiEnv->drawAll();
		pVideo->endScene();
	}

	pDevice->drop();
}

namespace _01 {
//기본 메쉬 만들기 함수를 이용해서 만들어 씬노드로 만들기
void main() {
	irr::IrrlichtDevice *pDevice = irr::createDevice(irr::video::EDT_OPENGL);

	pDevice->setWindowCaption(L"Type-A1");

	irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
	irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
	irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();

	pSmgr->addCameraSceneNode(0, irr::core::vector3df(0, 5, -10),
			irr::core::vector3df(0, 0, 0));

	//메쉬 생성 등록
	{
		pSmgr->addArrowMesh("usr/mesh/arrow"); //메쉬등록
	}

	{
		irr::scene::IAnimatedMeshSceneNode *pNode =
				pSmgr->addAnimatedMeshSceneNode(
						pSmgr->getMesh("usr/mesh/arrow"));
		pNode->setMaterialFlag(irr::video::EMF_LIGHTING, false);
		pNode->setPosition(irr::core::vector3df(0, 0, 0));
	}

	//이동 회전 변환 적용
	{
		irr::scene::IAnimatedMeshSceneNode *pNode =
				pSmgr->addAnimatedMeshSceneNode(
						pSmgr->getMesh("usr/mesh/arrow"));
		pNode->setMaterialFlag(irr::video::EMF_LIGHTING, false);
		pNode->setPosition(irr::core::vector3df(5, 0, 0));
		pNode->setRotation(irr::core::vector3df(0, 0, -90));
	}

	//프레임 레이트 표시용 유아이
	irr::gui::IGUIStaticText *pstextFPS = pGuiEnv->addStaticText(L"Frame rate",
			irr::core::rect<irr::s32>(0, 0, 100, 20), true, true, 0, 100, true);

	while (pDevice->run()) {
//		static irr::u32 uLastTick = pDevice->getTimer()->getTime();
//		irr::u32 uTick = pDevice->getTimer()->getTime();
//		irr::f32 fDelta = ((float) (uTick - uLastTick)) / 1000.f; //델타값 구하기
//		uLastTick = pDevice->getTimer()->getTime();

		//프레임레이트 갱신, 삼각형수 표시
		{
			wchar_t wszbuf[256];
			swprintf(wszbuf, 256, L"Frame rate : %d\n TriAngle: %d",
					pVideo->getFPS(), pVideo->getPrimitiveCountDrawn());
			pstextFPS->setText(wszbuf);
		}

		pVideo->beginScene(true, true, irr::video::SColor(255, 100, 101, 140));

		pSmgr->drawAll();
		pGuiEnv->drawAll();

		pVideo->endScene();
	}

	pDevice->drop();
}

}//_01

namespace _02 {
//getGeometryCreator 로 메쉬씬노드 만들
void main() {
	irr::IrrlichtDevice *pDevice = irr::createDevice(irr::video::EDT_OPENGL);

	pDevice->setWindowCaption(L"");

	irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
	irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
	irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
	pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

	pSmgr->addCameraSceneNode(0, irr::core::vector3df(0, 5, -40),
			irr::core::vector3df(0, 0, 0));

	//getGeometryCreator 이용해서 큐브 메시생성
	{
		irr::scene::IMesh *pMesh =
				pSmgr->getGeometryCreator()->createCubeMesh();
		//색바꾸기
		pSmgr->getMeshManipulator()->setVertexColors(pMesh,
				irr::video::SColor(255, 255, 0, 0));

		irr::scene::IAnimatedMesh *pAniMesh =
				pSmgr->getMeshManipulator()->createAnimatedMesh(pMesh);
		pMesh->drop();
		pSmgr->getMeshCache()->addMesh("usr/mesh/cube/red", pAniMesh); //씬메니져에 등록
		pAniMesh->drop();
	}

	//getGeometryCreator 이용해서 큐브 메시생성
	{
		irr::scene::IMesh *pMesh =
				pSmgr->getGeometryCreator()->createCubeMesh();
		//색바꾸기
		pSmgr->getMeshManipulator()->setVertexColors(pMesh,
				irr::video::SColor(255, 0, 255, 0));

		irr::scene::IAnimatedMesh *pAniMesh =
				pSmgr->getMeshManipulator()->createAnimatedMesh(pMesh);
		pMesh->drop();
		pSmgr->getMeshCache()->addMesh("usr/mesh/cube/blue", pAniMesh); //씬메니져에 등록
		pAniMesh->drop();
	}

	//getGeometryCreator 이용해서 큐브 메시생성
	{
		irr::scene::IMesh *pMesh =
				pSmgr->getGeometryCreator()->createCubeMesh();
		//색바꾸기
		pSmgr->getMeshManipulator()->setVertexColors(pMesh,
				irr::video::SColor(255, 0, 0, 255));

		irr::scene::IAnimatedMesh *pAniMesh =
				pSmgr->getMeshManipulator()->createAnimatedMesh(pMesh);
		pMesh->drop();
		pSmgr->getMeshCache()->addMesh("usr/mesh/cube/green", pAniMesh); //씬메니져에 등록
		pAniMesh->drop();
	}

	//씬노드 등록
	{
		irr::scene::IAnimatedMeshSceneNode *pNode =
				pSmgr->addAnimatedMeshSceneNode(
						pSmgr->getMesh("usr/mesh/cube/red"));
		pNode->setMaterialFlag(irr::video::EMF_LIGHTING, false);
		pNode->setPosition(irr::core::vector3df(0, 0, 0));
	}

	//씬노드 등록
	{
		irr::scene::IAnimatedMeshSceneNode *pNode =
				pSmgr->addAnimatedMeshSceneNode(
						pSmgr->getMesh("usr/mesh/cube/blue"));
		pNode->setMaterialFlag(irr::video::EMF_LIGHTING, false);
		pNode->setPosition(irr::core::vector3df(-15, 0, 0));

		pSmgr->getMeshManipulator()->setVertexColors(pNode->getMesh(),
						irr::video::SColor(255, 0, 255, 255));

		//pNode->setMaterialTexture(0,pVideo->getTexture("stones.jpg"));
		//pNode->setDebugDataVisible(irr::scene::EDS_FULL);
	}

	//옥트리  등록(씬노드의 버텍스 컬러 바꾸기)
	{
		irr::scene::IMeshSceneNode *pNode = pSmgr->addOctreeSceneNode(
				pSmgr->getMesh("usr/mesh/cube/green"));

		pSmgr->getMeshManipulator()->setVertexColors(pNode->getMesh(),
				irr::video::SColor(255, 255, 255, 0));
		pNode->setMesh(pNode->getMesh());//옥트리 재구성

		pNode->setMaterialFlag(irr::video::EMF_LIGHTING, false);
		pNode->setPosition(irr::core::vector3df(15, 0, 0));

	}

	//프레임 레이트 표시용 유아이
	irr::gui::IGUIStaticText *pstextFPS = pGuiEnv->addStaticText(L"Frame rate",
			irr::core::rect<irr::s32>(0, 0, 100, 20), true, true, 0, 100, true);

	while (pDevice->run()) {
//		static irr::u32 uLastTick = pDevice->getTimer()->getTime();
//		irr::u32 uTick = pDevice->getTimer()->getTime();
//		irr::f32 fDelta = ((float) (uTick - uLastTick)) / 1000.f; //델타값 구하기
//		uLastTick = pDevice->getTimer()->getTime();

		//프레임레이트 갱신, 삼각형수 표시
		{
			wchar_t wszbuf[256];
			swprintf(wszbuf, 256, L"Frame rate : %d\n TriAngle: %d",
					pVideo->getFPS(), pVideo->getPrimitiveCountDrawn());
			pstextFPS->setText(wszbuf);
		}

		pVideo->beginScene(true, true, irr::video::SColor(255, 100, 101, 140));

		pSmgr->drawAll();
		pGuiEnv->drawAll();

		pVideo->endScene();
	}

	pDevice->drop();
}

}//_02

namespace _03 {
//힐플래인메쉬 노드 만들기
void main() {
	irr::IrrlichtDevice *pDevice = irr::createDevice(irr::video::EDT_OPENGL);

	pDevice->setWindowCaption(L"4-4.");

	irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
	irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
	irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
	pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

	pSmgr->addCameraSceneNode(0, irr::core::vector3df(0, 5, -10),
			irr::core::vector3df(0, 0, 0));

	//힐플래인 메쉬 추가
	{
		irr::scene::IAnimatedMesh *pMesh = pSmgr->addHillPlaneMesh(
				"usr/mesh/myhill", irr::core::dimension2d<irr::f32>(8, 8),
				irr::core::dimension2d<irr::u32>(8, 8), 0, 0,
				irr::core::dimension2d<irr::f32>(0, 0),
				irr::core::dimension2d<irr::f32>(8, 8));
	}

	{
		irr::scene::IAnimatedMeshSceneNode *pNode =
				pSmgr->addAnimatedMeshSceneNode(
						pSmgr->getMesh("usr/mesh/myhill"));
		pNode->setMaterialFlag(irr::video::EMF_LIGHTING, false);
		pNode->setMaterialTexture(0, pVideo->getTexture("wall.jpg"));

		pNode->setName("usr/scene/hill1");
	}

	//프레임 레이트 표시용 유아이
	irr::gui::IGUIStaticText *pstextFPS = pGuiEnv->addStaticText(L"Frame rate",
			irr::core::rect<irr::s32>(0, 0, 100, 20), true, true, 0, 100, true);

	while (pDevice->run()) {
//		static irr::u32 uLastTick = pDevice->getTimer()->getTime();
//		irr::u32 uTick = pDevice->getTimer()->getTime();
//		irr::f32 fDelta = ((float) (uTick - uLastTick)) / 1000.f; //델타값 구하기
//		uLastTick = pDevice->getTimer()->getTime();

		//프레임레이트 갱신, 삼각형수 표시
		{
			wchar_t wszbuf[256];
			swprintf(wszbuf, 256, L"Frame rate : %d\n TriAngle: %d",
					pVideo->getFPS(), pVideo->getPrimitiveCountDrawn());
			pstextFPS->setText(wszbuf);
		}

		pVideo->beginScene(true, true, irr::video::SColor(255, 100, 101, 140));

		pSmgr->drawAll();
		pGuiEnv->drawAll();

		pVideo->endScene();
	}

	pDevice->drop();
}



}//_03

namespace _04
{
//메쉬파일읽기
void main() {
	irr::IrrlichtDevice *pDevice = irr::createDevice(irr::video::EDT_OPENGL);

	pDevice->setWindowCaption(L"4-5");

	irr::video::IVideoDriver *pVideo = pDevice->getVideoDriver();
	irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();
	irr::gui::IGUIEnvironment *pGuiEnv = pDevice->getGUIEnvironment();
	pDevice->getFileSystem()->changeWorkingDirectoryTo("../../res");

	pSmgr->addCameraSceneNode(0, irr::core::vector3df(0, 250, -610),
			irr::core::vector3df(0, 200, 0));

	{
		irr::scene::IAnimatedMesh *pAniMesh = pSmgr->getMesh(
				"jga/buildings_by_km_2/building_008.x");
		irr::scene::IMesh *pMesh = pAniMesh->getMesh(0);
		irr::scene::IMeshSceneNode *pNode = pSmgr->addMeshSceneNode(pMesh);

		pNode->setMaterialFlag(irr::video::EMF_LIGHTING, false);
		pNode->setMaterialTexture(0,
				pVideo->getTexture("jga/buildings_by_km_2/building_008tex.jpg"));

	}

	//프레임 레이트 표시용 유아이
	irr::gui::IGUIStaticText *pstextFPS = pGuiEnv->addStaticText(L"Frame rate",
			irr::core::rect<irr::s32>(0, 0, 100, 20), true, true, 0, 100, true);

	while (pDevice->run()) {
//		static irr::u32 uLastTick = pDevice->getTimer()->getTime();
//		irr::u32 uTick = pDevice->getTimer()->getTime();
//		irr::f32 fDelta = ((float) (uTick - uLastTick)) / 1000.f; //델타값 구하기
//		uLastTick = pDevice->getTimer()->getTime();

		//프레임레이트 갱신, 삼각형수 표시
		{
			wchar_t wszbuf[256];
			swprintf(wszbuf, 256, L"Frame rate : %d\n TriAngle: %d",
					pVideo->getFPS(), pVideo->getPrimitiveCountDrawn());
			pstextFPS->setText(wszbuf);
		}

		pVideo->beginScene(true, true, irr::video::SColor(255, 100, 101, 140));

		pSmgr->drawAll();
		pGuiEnv->drawAll();

		pVideo->endScene();
	}

	pDevice->drop();
}

}//_04


}//lsn03

}//chapter1
