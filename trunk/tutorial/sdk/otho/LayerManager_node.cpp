﻿#include "LayerManager_node.h"

namespace irr 
{
	namespace scene
	{
		namespace otho
		{

			const char *CLayerManager_node::TypeName = "LayerManager";

			const c8* CLayerManager_node::CmdListNames[] =			
			{
				"---",
				"add_tx",
				"add_tile",
				"add_seq",
				0
			};	
            
            
            /*
             
             */
			void CLayerManager_node::drawSprite(irr::scene::ISceneManager *pSmgr,irr::core::stringc strTMName, irr::core::position2di pos,irr::u32 index)
			{
				irr::video::IVideoDriver *pVideo = pSmgr->getVideoDriver();
				irr::scene::otho::CLayerManager_node *pNode = (irr::scene::otho::CLayerManager_node *)
                pSmgr->getSceneNodeFromName(strTMName.c_str());
                
				irr2d::layer::S_TileLayer tile = pNode->getTile(index);				
                
				pVideo->draw2DImage( 
                                    pNode->getMaterial(tile.m_uTextureBank).getTexture(0),					
                                    irr::core::position2di(100,100),			
                                    tile.m_CutRect,
                                    NULL,
                                    irr::video::SColor(255,255,255,255),
                                    true
                                    );
			}
            
		}
	}
}