#pragma once

#include <irrlicht.h>
#include "LayerMng.h"

namespace irr {
	namespace scene
	{
		namespace otho
		{			

			class CLayerManager_node :public irr::scene::ISceneNode
			{
				irr::core::stringc m_CurCmd;
				irr::core::stringc m_strParams;

				core::array<video::SMaterial> Materials;				
				irr2d::layer::CLayerMng m_LayerMng;

			public:

				static const char *TypeName;
				static const int TypeID = MAKE_IRR_ID('@','l','y','m');

				//커멘드 관련
				static const c8* CmdListNames[];

				CLayerManager_node(ISceneNode* parent, 
					ISceneManager* mgr) : scene::ISceneNode(parent,mgr)

				{
					m_CurCmd = CmdListNames[0];
				}
				virtual ~CLayerManager_node(void)
				{
				}

				inline irr::u32 getTileConut()
				{
					return m_LayerMng.m_vtTile.size();
				}

				inline irr2d::layer::S_TileLayer &getTile(irr::u32 index)
				{
					return m_LayerMng.m_vtTile[index];
				}

				inline void setTile(irr::u32 index,irr2d::layer::S_TileLayer &tile)
				{
					m_LayerMng.m_vtTile[index] = tile;
				}

				inline void setTileName(irr::u32 index, irr::core::stringw strw)
				{
					m_LayerMng.m_vtTile[index].m_strName = strw;
				}

				inline void addTile(irr2d::layer::S_TileLayer &tile)
				{
					m_LayerMng.m_vtTile.push_back(tile);	
				}

				inline void delTile(irr::u32 index)
				{
					m_LayerMng.m_vtTile.erase(index);
				}

				inline void setMaterial(irr::video::SMaterial& m)
				{
					Materials.push_back(m);
				}

				core::aabbox3d<f32> Box;
				virtual const core::aabbox3d<f32>& getBoundingBox() const {
					return Box;
				}

				//! renders the node.
				virtual void render(){}

				
				//virtual void setReadOnlyMaterials(bool readonly) {}
				////! Returns if the scene node should not copy the materials of the mesh but use them in a read only style
				//virtual bool isReadOnlyMaterials() const { return false; }

				video::SMaterial& getMaterial(u32 i)
				{
					if ( i >= Materials.size())
						return ISceneNode::getMaterial(i);

					return Materials[i];
				}

				
				virtual u32 getMaterialCount() const
				{
					return Materials.size();
				}

				virtual void serializeAttributes(io::IAttributes* out, io::SAttributeReadWriteOptions* options=0) const
				{
					irr::scene::ISceneNode::serializeAttributes(out,options);

					out->addEnum("cmd",m_CurCmd.c_str(),CmdListNames);
					out->addString("param",m_strParams.c_str());

					out->addInt("matr_cnt",Materials.size());					

					m_LayerMng.serializeAttributes(out,options);
				}

				virtual void deserializeAttributes(io::IAttributes* in, io::SAttributeReadWriteOptions* options=0)
				{
					irr::scene::ISceneNode::deserializeAttributes(in,options);

					m_CurCmd = in->getAttributeAsEnumeration("cmd"); //커멘트 읽기
					m_strParams = in->getAttributeAsString("param");

					m_LayerMng.deserializeAttributes(in,options);

					irr::u32 mat_size = in->getAttributeAsInt("matr_cnt");
					if(Materials.size() <  mat_size)
					{
						irr::u32 i;
						for(i=Materials.size();i < mat_size;i++)
						{
							Materials.push_back(irr::video::SMaterial());
						}
					}


					//플러그인만 되도록 함
#ifdef OTHOFACTORY_EXPORTS
					if(m_CurCmd == CmdListNames[1]) //add texture
					{		
						irr::video::SMaterial m;
						Materials.push_back(m);						
					}
					else if(m_CurCmd == CmdListNames[2]) //add tile
					{
						irr2d::layer::S_TileLayer tile;
						tile.m_uTextureBank = 0;
						tile.m_CutRect = irr::core::rect<irr::s32>(0,0,0,0);
						tile.m_colTrans = 0x00000000;
						tile.m_uTileNumber = m_LayerMng.m_vtTile.size();
						addTile(tile);
						//m_LayerMng.m_vtTile.push_back(tile);						
					}
					else 
					{

					}
					m_CurCmd = "---"; 				
#endif

					
				}				


				//! Returns type of the scene node
				virtual ESCENE_NODE_TYPE getType() const { return (ESCENE_NODE_TYPE)TypeID;}//ESNT_CUBE; }	
                
                //! sprite utills
                static void drawSprite(irr::scene::ISceneManager *pSmgr,irr::core::stringc strTMName, irr::core::position2di pos,irr::u32 index);



			};

		}
	}
}
