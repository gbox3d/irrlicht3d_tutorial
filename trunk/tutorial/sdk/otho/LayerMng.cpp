﻿#include <irrlicht.h>
//#include <vector>
//#include <string>
//#include "tinyxml.h"
//#include "ggfsdk.h"
#include "LayerMng.h"


//#include <windows.h>
//#include <stdio.h>
//#include <stdarg.h>

//#include "irr_utill.h"

namespace irr2d
{
	namespace layer 
	{

		///////////////////////////////////////// S_TileLayer //////////////////
		void S_TileLayer::serializeAttributes(irr::io::IAttributes* out, irr::io::SAttributeReadWriteOptions* options) const
		{				
			irr::core::stringc strName = "Tile_";
			strName += m_uTileNumber;

			static irr::c8 szBuf[256];

			sprintf(szBuf,"%d,%d,(%d,%d,%d,%d),%d",m_uTileNumber,m_uTextureBank,
				m_CutRect.UpperLeftCorner.X,m_CutRect.UpperLeftCorner.Y,
				m_CutRect.LowerRightCorner.X,m_CutRect.LowerRightCorner.Y,
                  m_colTrans.color
				);

			out->addString(strName.c_str(),szBuf);
			
			//out->addBinary(strTemp.c_str(),(void *)this,sizeof(S_TileLayer));
		}
		
		void S_TileLayer::deserializeAttributes(irr::io::IAttributes* in, irr::io::SAttributeReadWriteOptions* options)
		{
			//irr::c8 szBuf_TileName[128];
			//sprintf(szBuf_TileName,"Tile_%d",m_uTileNumber);

			irr::c8 szBuf[128];
			
			irr::core::stringc strTemp = "Tile_";// + m_uTileNumber;
			strTemp += m_uTileNumber;			

			S_TileLayer tile;					

			in->getAttributeAsString(
				strTemp.c_str(),
				//strTemp.c_str(),
				szBuf);

			sscanf(szBuf,"%d,%d,(%d,%d,%d,%d),%d",
				&m_uTileNumber,&m_uTextureBank,
				&m_CutRect.UpperLeftCorner.X,&m_CutRect.UpperLeftCorner.Y,
				&m_CutRect.LowerRightCorner.X,&m_CutRect.LowerRightCorner.Y,
				&m_colTrans
				);

			

			/*
			in->getAttributeAsBinaryData(strTemp.c_str(),(void *)&tile,sizeof(S_TileLayer));
			
			m_uTileNumber = tile.m_uTileNumber;
			m_uTextureBank = tile.m_uTextureBank;
			m_CutRect = tile.m_CutRect;
			m_colTrans = tile.m_colTrans;
			*/
		}

		///////////////////////////////////////// CLayerMng //////////////////

		CLayerMng::CLayerMng(void)
		{
		}

		CLayerMng::~CLayerMng(void)
		{
			m_vtTile.clear();
			m_vtTileSeq.clear();
			m_vtTexture.clear();
		}

		/*
		S_Texture *CLayerMng::FindTexture(irr::core::stringc strName)
		{
			
			//irr::core::array<S_Texture>::iterator it;
			irr::u32 i;
			for(i=0;i<m_vtTexture.size();i++)
			//for(it = m_vtTexture.begin();it != m_vtTexture.end();it++)
			{
				if(m_vtTexture[i].m_strName == strName)
				return &m_vtTexture[i];
				//if(it->m_strName == strName)
					//return &(*it);
			}
			return NULL;
		}
		S_TileLayer *CLayerMng::FindTile(irr::core::stringc strName)
		{
			//irr::core::array<S_TileLayer>::iterator it;
			//for(it = m_vtTile.begin();it != m_vtTile.end();it++)
			irr::u32 i;
			for(i=0;i<m_vtTile.size();i++)
			{
				if(m_vtTile[i].m_strName == strName)
					return &m_vtTile[i];
					//return &(*it);
			}

			return NULL;
		}
		
		


		bool CLayerMng::Load(irr::IrrlichtDevice *pdevice,char *szfile)
		{
			irr::io::IXMLReader *pXml = pdevice->getFileSystem()->createXMLReader(szfile);

			irr::io::IAttributes *pAttr = pdevice->getFileSystem()->createEmptyAttributes();

			while(pXml->read())
			{			
				switch(pXml->getNodeType())
				{
				case  irr::io::EXN_ELEMENT:
					{
						irr::core::stringw strwNodeName = pXml->getNodeName();

						if(strwNodeName == "root") //텍스춰 읽기
						{
							m_strwName = pXml->getAttributeValue(L"name");
						}

						else if(strwNodeName == TEXTURE_TAG) //텍스춰 읽기
						{
							while(pXml->read()) 
							{
								if(pXml->getNodeType() == irr::io::EXN_ELEMENT_END)
								{
									if( irr::core::stringw(TEXTURE_TAG) == pXml->getNodeName() )
									{
										pdevice->getLogger()->log("end of texture");
										break;
									}
								}
								else if(pXml->getNodeType() == irr::io::EXN_ELEMENT)
								{
									if(pAttr->read(pXml,true))
									{
										S_Texture TexDesc;
										if(pAttr->getAttributeCount() == 0) break;
										TexDesc.deserializeAttributes(pAttr);
										m_vtTexture.push_back(TexDesc);
										pdevice->getLogger()->log(TexDesc.m_strName.c_str());
									}							
								}
							}
						}
						else if(strwNodeName == TILE_TAG) //타일 읽기
						{
							while(pXml->read())
							{
								if(pXml->getNodeType() == irr::io::EXN_ELEMENT_END)
								{
									if( irr::core::stringw(TILE_TAG) == pXml->getNodeName() )
									{
										pdevice->getLogger()->log("end of tile");
										break;
									}
								}
								else if(pXml->getNodeType() == irr::io::EXN_ELEMENT)
								{
									if(pAttr->read(pXml,true))
									{
										S_TileLayer tile;
										tile.deserializeAttributes(pAttr);
										m_vtTile.push_back(tile);
										pdevice->getLogger()->log(tile.m_strName.c_str());
									}
								}
							}
						}						
						else if(strwNodeName == TILESEQ_TAG) //타일 시퀀스읽기
						{
							while(pXml->read())
							{
								if(pXml->getNodeType() == irr::io::EXN_ELEMENT_END)
								{
									if( irr::core::stringw(TILESEQ_TAG) == pXml->getNodeName() )
									{
										pdevice->getLogger()->log("end of tile seq");
										break;
									}
								}
								else if(pXml->getNodeType() == irr::io::EXN_ELEMENT)
								{
									if(pAttr->read(pXml,true))
									{
										S_TileSqeunce tileseq;
										tileseq.deserializeAttributes(pAttr);
										m_vtTileSeq.push_back(tileseq);
										pdevice->getLogger()->log(tileseq.m_strName.c_str());
									}
								}
							}
						}	
					}
					break;
				case  irr::io::EXN_ELEMENT_END: //루트 노드 종료 여부 판단
					if( irr::core::stringw(L"root") == pXml->getNodeName() )
						pdevice->getLogger()->log("parse success");
					else
						return false;
					break;

				case  irr::io::EXN_TEXT:
					break;
				}			
			}

			pAttr->drop();
			pXml->drop();		

			return true;
			
		}

		bool CLayerMng::Save(irr::IrrlichtDevice *pdevice,char *szfile)
		{
			irr::io::IXMLWriter *pXml = pdevice->getFileSystem()->createXMLWriter(szfile);
			pXml->writeXMLHeader();

			pXml->writeElement(L"root",false,L"name",m_strwName.c_str());
			pXml->writeLineBreak();						

			irr::u32 i;
			
			//텍스춰 출력
			pXml->writeElement(TEXTURE_TAG);
			pXml->writeLineBreak();			
			for(i=0; i < m_vtTexture.size();i++)
			{
				irr::io::IAttributes *pAttr = pdevice->getFileSystem()->createEmptyAttributes();	
				m_vtTexture[i].serializeAttributes(pAttr); //정보 얻어 오기
				pAttr->write(pXml);	
				pAttr->drop();
			}

			pXml->writeClosingTag(TEXTURE_TAG);
			pXml->writeLineBreak();
			
			//타일 출력
			pXml->writeElement(TILE_TAG);
			pXml->writeLineBreak();			
			for(i=0; i < m_vtTile.size();i++)
			{
				irr::io::IAttributes *pAttr = pdevice->getFileSystem()->createEmptyAttributes();	
				m_vtTile[i].serializeAttributes(pAttr); //정보 얻어 오기
				pAttr->write(pXml);	
				pAttr->drop();
			}

			pXml->writeClosingTag(TILE_TAG);
			pXml->writeLineBreak();						

			//타일 시퀀스 출력
			pXml->writeElement(TILESEQ_TAG);
			pXml->writeLineBreak();			
			for(i=0; i < m_vtTileSeq.size();i++)
			{
				irr::io::IAttributes *pAttr = pdevice->getFileSystem()->createEmptyAttributes();	
				m_vtTileSeq[i].serializeAttributes(pAttr); //정보 얻어 오기
				pAttr->write(pXml);	
				pAttr->drop();
			}

			pXml->writeClosingTag(TILESEQ_TAG);
			pXml->writeLineBreak();						


			///////////////////////////

			pXml->writeClosingTag(L"root");
			pXml->writeLineBreak();						

			pXml->drop();			

			return true;
		}
		*/

		void CLayerMng::serializeAttributes(irr::io::IAttributes* out, irr::io::SAttributeReadWriteOptions* options) const {
			
			irr::u32 i;			

			out->addInt("Tile_Cnt",m_vtTile.size());

			for(i=0;i<m_vtTile.size();i++)
			{				
				m_vtTile[i].serializeAttributes(out,options);
			}
		}

		void CLayerMng::deserializeAttributes(irr::io::IAttributes* in, irr::io::SAttributeReadWriteOptions* options) {

			irr::u32 i;		
			irr::u32 tile_cnt = in->getAttributeAsInt("Tile_Cnt");

			if(m_vtTile.size() < tile_cnt)
			{
				for(i=0;i<tile_cnt;i++)
				{
					S_TileLayer tile;
					tile.m_uTileNumber = i;
					m_vtTile.push_back(tile);					
				}
			}

			for(i=0;i<m_vtTile.size();i++)
			{		
				//S_TileLayer tile = m_vtTile[i];

				m_vtTile[i].deserializeAttributes(in,options);
				
				//le.drop();
				//tile.grab();
				//m_vtTile[i] = tile;
			}
		}		
	}
}
