#pragma once

//#include <map>

namespace irr2d
{
namespace layer
{	

	struct S_Texture :public irr::io::IAttributeExchangingObject
	{
		irr::core::stringc m_strName;
		irr::core::stringc m_strFile;

		//! Writes attributes of the object.
		/** Implement this to expose the attributes of your scene node animator for
		scripting languages, editors, debuggers or xml serialization purposes. 
		여기에는 add.... 계열 함수를...
		*/
		inline virtual void serializeAttributes(irr::io::IAttributes* out, irr::io::SAttributeReadWriteOptions* options=0) const 
		{		
			out->clear();
			out->addString("Name",m_strName.c_str());
			out->addString("File",m_strFile.c_str());

		}

		//! Reads attributes of the object.
		/** Implement this to set the attributes of your scene node animator for
		scripting languages, editors, debuggers or xml deserialization purposes. */
		inline virtual void deserializeAttributes(irr::io::IAttributes* in, irr::io::SAttributeReadWriteOptions* options=0) {
			m_strName = in->getAttributeAsString("Name");
			m_strFile = in->getAttributeAsString("File");
		}
		
	};

//!타일 처리 구조체
	struct S_TileLayer :public irr::io::IAttributeExchangingObject
	{
		irr::core::stringc m_strName;
		irr::core::stringc m_strTexName;		
		irr::u32 m_uTileNumber;
		irr::u32 m_uTextureBank;
		
		irr::core::rect<irr::s32> m_CutRect;
		irr::video::SColor m_colTrans;

		virtual void serializeAttributes(irr::io::IAttributes* out, irr::io::SAttributeReadWriteOptions* options=0) const;		
		virtual void deserializeAttributes(irr::io::IAttributes* in, irr::io::SAttributeReadWriteOptions* options=0);


		/*
		{
			irr::core::stringc strTemp = "Tile_";// + m_uTileNumber;
			strTemp += m_uTileNumber;

			irr::u8 buf[sizeof(S_TileLayer)];

			in->getAttributeAsBinaryData(strTemp.c_str(),(void *)buf,sizeof(S_TileLayer));
			
			m_u

		}
		*/
	};

	struct S_AniTileLayer : public irr::io::IAttributeExchangingObject
	{
		irr::core::stringc m_strName;
		irr::core::stringc m_strTexName;		
		irr::core::rect<irr::s32> m_rectCut;
		irr::core::rect<irr::s32> m_rectFrame;
		int m_nTotalFrame;
		irr::video::SColor m_colTrans;

		inline virtual void serializeAttributes(irr::io::IAttributes* out, irr::io::SAttributeReadWriteOptions* options=0) const 
		{	
			out->clear();
			out->addString("Name",m_strName.c_str());
			out->addString("TexName",m_strTexName.c_str());
			out->addRect("cut_rect",m_rectCut);
			out->addRect("frame_rect",m_rectFrame);
			out->addInt("TotalFrame",m_nTotalFrame);
			out->addColor("Trans",m_colTrans);

		}

		inline virtual void deserializeAttributes(irr::io::IAttributes* in, irr::io::SAttributeReadWriteOptions* options=0) 
		{
			m_strName = in->getAttributeAsString("Name");
			m_strTexName = in->getAttributeAsString("TexName");
			m_rectCut = in->getAttributeAsRect("cut_rect");
			m_rectFrame = in->getAttributeAsRect("frame_rect");
			m_nTotalFrame = in->getAttributeAsInt("TotalFrame");
			m_colTrans = in->getAttributeAsColor("Trans");
		}	

	};

	struct S_TileSqeunce : public irr::io::IAttributeExchangingObject
	{
		irr::core::stringc m_strName;		
		irr::f32	m_fFramePerSecond; 

		S_TileSqeunce(): m_fFramePerSecond(1.f) {}

		irr::core::array<irr::core::vector2di> m_vtRefPixel;		
		irr::core::array<irr::s32> m_vtSeqIndex;

		inline void addTileSeq(irr::s32 tile_index,irr::core::vector2di RefPixel )
		{
			m_vtRefPixel.push_back(RefPixel);
			m_vtSeqIndex.push_back(tile_index);
		}

		inline void DelTileSeq(irr::s32 index )
		{
			m_vtRefPixel.erase(index);
			m_vtSeqIndex.erase(index);
		}

		virtual void serializeAttributes(irr::io::IAttributes* out, irr::io::SAttributeReadWriteOptions* options=0) const
		{

			out->clear();
			out->addString("m_strName",m_strName.c_str());
			out->addFloat("m_fFramePerSecond",m_fFramePerSecond);

			{
				irr::u32 i;
				wchar_t wszbuf[256];
				irr::core::array<irr::core::stringw> vtTemp;
				for(i=0;i<m_vtRefPixel.size();i++)
				{
					swprintf(wszbuf,256,L"%d %d",m_vtRefPixel[i].X,m_vtRefPixel[i].Y);
					vtTemp.push_back(wszbuf);
				}
				out->addArray("m_vtRefPixel",vtTemp);
			}
			{
				irr::u32 i;
				wchar_t wszbuf[256];
				irr::core::array<irr::core::stringw> vtTemp;
				for(i=0;i<m_vtSeqIndex.size();i++)
				{
					swprintf(wszbuf,256,L"%d",m_vtSeqIndex[i]);
					vtTemp.push_back(wszbuf);
				}

				out->addArray("m_vtSeqIndex",vtTemp);
			}

		}

		virtual void deserializeAttributes(irr::io::IAttributes* in, irr::io::SAttributeReadWriteOptions* options=0)
		{
			m_strName = in->getAttributeAsString("m_strName");
			m_fFramePerSecond = in->getAttributeAsFloat("m_fFramePerSecond");

			{
				irr::core::array<irr::core::stringw> vtTemp = in->getAttributeAsArray("m_vtRefPixel");
				irr::u32 i;
				for(i=0;i<vtTemp.size();i++)
				{
					irr::core::vector2di pos;
					swscanf(vtTemp[i].c_str(),L"%d %d",&pos.X,&pos.Y);
					m_vtRefPixel.push_back(pos);
				}
				
			}

			{
				irr::core::array<irr::core::stringw> vtTemp = in->getAttributeAsArray("m_vtSeqIndex");
				irr::u32 i;
				for(i=0;i<vtTemp.size();i++)
				{
					irr::s32 nTemp;
					swscanf(vtTemp[i].c_str(),L"%d",&nTemp);
					m_vtSeqIndex.push_back(nTemp);
				}

			}			
		}
		
	};

	struct S_Xcene
	{
		irr::core::stringc m_strName;
		irr::core::array<int> m_vsSpriteLayer;		
	};

//	static const wchar_t *TEXTURE_TAG = L"Texture";
//	static const wchar_t *TILE_TAG = L"Tile";
//	static const wchar_t *TILESEQ_TAG = L"TileSeq";

class CLayerMng : public irr::io::IAttributeExchangingObject
{
public:
	CLayerMng(void);
	virtual ~CLayerMng(void);

	irr::core::array<S_Texture>		m_vtTexture;
	irr::core::array<S_TileLayer>	m_vtTile;	
	irr::core::array<S_TileSqeunce> m_vtTileSeq;
	//irr::core::map<irr::core::stringc, S_AniTileLayer>	m_AniTiles;	
	//irr::core::array<S_SpriteLayer>	m_vtSprite;
	//irr::core::array<S_Xcene>		m_vtXcene;
		
public:

	irr::core::stringw m_strwName;

	virtual void serializeAttributes(irr::io::IAttributes* out, irr::io::SAttributeReadWriteOptions* options=0) const;
	virtual void deserializeAttributes(irr::io::IAttributes* in, irr::io::SAttributeReadWriteOptions* options=0);

	//bool Load(irr::IrrlichtDevice *pdevice,char *szfile){return}
	//bool Save(irr::IrrlichtDevice *pdevice,char *szfile){}
	
	S_Texture		*FindTexture(irr::core::stringc strName);
	S_TileLayer		*FindTile(irr::core::stringc strName);
	//bool CLayerMng::FindAniTile(irr::core::stringc strName,S_AniTileLayer &pAniTile);	
	//S_SpriteLayer	*FindSprite(irr::core::stringc strName);	

	void clearAll()
	{
		m_vtTexture.clear();
		m_vtTile.clear();
		
	}

	bool AddTexture(char *szName,char *szTexFile)
	{
		S_Texture tex;

		tex.m_strName = szName;
		tex.m_strFile = szTexFile;

		m_vtTexture.push_back(tex);

		return true;
	}
	/*
	bool IsExistTile(const char *szName)
	{
		irr::core::stringc strName = szName;
		irr::u32 i;
		//irr::core::array<S_TileLayer>::iterator it;
		//for(it = m_vtTile...begin();it != m_vtTile.end();it++)
		for(i=0;i<m_vtTile.size();i++)
		{
			if(m_vtTile[i].m_strName == strName)
			{				
				return true;
			}
		}

		return false;
	}
	*/

	/*
	bool AddTile(char *szName,char *szTexFile,irr::core::rect<irr::s32> pos)
	{
		S_TileLayer tile;
		
		tile.m_strName = szName;
		tile.m_strTexName = szTexFile;		
		tile.m_CutRect = pos;		
		
		tile.m_colTrans = 0;

		m_vtTile.push_back(tile);

		return true;
	}
	*/

	/*
	bool RemoveTile(irr::core::stringc strName)
	{
		
		//irr::core::vector<S_TileLayer>::iterator it;
		//for(it = m_vtTile.begin();it != m_vtTile.end();it++)
		irr::u32 i;
		
		for(i=0;i<m_vtTile.size();i++)
		{
			if(m_vtTile[i].m_strName == strName)
			{
				m_vtTile.erase(i);
				return true;
			}
		}

		return false;
	}

	bool RemoveTile(const char *szName)
	{
		return RemoveTile(irr::core::stringc(szName));
	}
	*/	

};


}

}
