﻿#include "OthoFactory.h"

//#include "Plan1x1SceneNode.h"
//#include "Plan1X1SeqIndexNode.h"
//#include "SeqFormatedAni.h"

#include "LayerManager_node.h"

//#include "AABBNode.h"




namespace irr{
	namespace scene{
		namespace otho {

			struct S_SCENENODE_DESC
			{
				const int m_nID;
				const char *m_strName;
			};

			static const S_SCENENODE_DESC g_SCN_desc[] =
			{	
				{ CLayerManager_node::TypeID,CLayerManager_node::TypeName},	
				//{ CPlan1x1SceneNode::TypeID,CPlan1x1SceneNode::TypeName},
				//{ CPlan1X1SeqIndexNode::TypeID,CPlan1X1SeqIndexNode::TypeName},				
				//{ CSeqFormatedAni::TypeID,CSeqFormatedAni::TypeName},
				//{ CAABBNode::TypeID,CAABBNode::TypeName}
				
			};


			COthoFactory::COthoFactory(irr::scene::ISceneManager *pSmgr):Manager(pSmgr)
			{
			}


			COthoFactory::~COthoFactory(void)
			{
			}

			ISceneNode* COthoFactory::addSceneNode( ESCENE_NODE_TYPE type, ISceneNode* parent/*=0*/ )
			{
				if (!parent)
					parent = Manager->getRootSceneNode();				

				switch(type)
				{
                        /*
				case CPlan1x1SceneNode::TypeID:
					{
						
						CPlan1x1SceneNode *node = new CPlan1x1SceneNode(parent, Manager, -1);					
						node->drop();
						return node; 
					}
					break;
				case CPlan1X1SeqIndexNode::TypeID:
					{
						CPlan1X1SeqIndexNode *node = new CPlan1X1SeqIndexNode(parent, Manager, -1);	
						node->drop();
						return node;
					}
					break;
*/
				case CLayerManager_node::TypeID:
					{
						CLayerManager_node *node = new CLayerManager_node(parent,Manager);						

						node->drop();
						return node;
					}
					break;	
                        /*
				case CSeqFormatedAni::TypeID:
					{
						CSeqFormatedAni *node = new CSeqFormatedAni(parent,Manager);
						node->drop();
						return node;
					}
					break;
				case CAABBNode::TypeID:
					{
						CAABBNode *node = new CAABBNode(parent,Manager,-1,
							irr::core::aabbox3df(-1,-1,-1,1,1,1) 
							);
						node->drop();
						return node;
					}
                         */
				default:
					break;
				}

				return 0;
			}

			ISceneNode* COthoFactory::addSceneNode( const c8* typeName, ISceneNode* parent/*=0*/ )
			{
				return addSceneNode( getTypeFromName(typeName), parent );

			}

			u32 COthoFactory::getCreatableSceneNodeTypeCount() const
			{
				return sizeof(g_SCN_desc)/sizeof(S_SCENENODE_DESC);

			}

			//! 인덱스->노드 타입 이름
			const c8* COthoFactory::getCreateableSceneNodeTypeName( u32 idx ) const
			{
				if(idx <= getCreatableSceneNodeTypeCount())
					return g_SCN_desc[idx].m_strName;		

				return 0;			
			}

			//!노드 타입아이디-> 노드타입이름
			const c8* COthoFactory::getCreateableSceneNodeTypeName( ESCENE_NODE_TYPE type ) const
			{
				u32 i;
				for(i=0; i< getCreatableSceneNodeTypeCount();i++)
				{
					if(type == g_SCN_desc[i].m_nID)
						return g_SCN_desc[i].m_strName;
				}
				return 0;
			}

			ESCENE_NODE_TYPE COthoFactory::getCreateableSceneNodeType( u32 idx ) const
			{
				if(idx <= getCreatableSceneNodeTypeCount())
					return (ESCENE_NODE_TYPE)g_SCN_desc[idx].m_nID;

				return ESNT_UNKNOWN;
			}

			ESCENE_NODE_TYPE COthoFactory::getTypeFromName( const c8* name )
			{
				u32 i;
				for(i=0; i< getCreatableSceneNodeTypeCount();i++)
				{				
					if(!strcmp(g_SCN_desc[i].m_strName,name))
						return (ESCENE_NODE_TYPE)g_SCN_desc[i].m_nID;
				}
				return ESNT_UNKNOWN;
			}
		}
	}
}