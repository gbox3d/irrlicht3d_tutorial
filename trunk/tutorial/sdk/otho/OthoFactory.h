#pragma once

#include <irrlicht.h>

namespace irr
{
	namespace scene{

		namespace otho {

			class COthoFactory
				:
				public irr::scene::ISceneNodeFactory
			{
			public:
				COthoFactory(irr::scene::ISceneManager *pSmgr);
				virtual ~COthoFactory(void);

				//public:
				//CTestFactory(irr::scene::ISceneManager *pSmgr);
				//virtual ~CTestFactory(void);

				virtual ISceneNode* addSceneNode(ESCENE_NODE_TYPE type, ISceneNode* parent=0);
				virtual ISceneNode* addSceneNode(const c8* typeName, ISceneNode* parent=0);
				virtual u32 getCreatableSceneNodeTypeCount() const;
				virtual const c8* getCreateableSceneNodeTypeName(u32 idx) const;
				virtual ESCENE_NODE_TYPE getCreateableSceneNodeType(u32 idx) const;
				virtual const c8* getCreateableSceneNodeTypeName(ESCENE_NODE_TYPE type) const;

			private:				

				ESCENE_NODE_TYPE getTypeFromName(const c8* name);
				ISceneManager* Manager;
			};
		}
	}
}

