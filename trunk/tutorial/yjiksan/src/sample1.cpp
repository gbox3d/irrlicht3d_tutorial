﻿#include <irrlicht.h>
#pragma comment(lib,"irrlicht.lib")

//첫번째 샘플입니다.
#include "../../sdk/otho/OthoFactory.h"
#include "../../sdk/otho/LayerManager_node.h"
#include "../../sdk/etc/CSampleApp.h"

namespace sample1 {

	void main();
	
    
    

	void main()
	{
		CSampleApp theApp(irr::core::vector2di(100,300));

		irr::IrrlichtDevice *pDevice = 
			irr::createDevice(irr::video::EDT_OPENGL);

		pDevice->setEventReceiver(&theApp);

		irr::video::IVideoDriver *pVideo = 
			pDevice->getVideoDriver();
		
		irr::scene::ISceneManager *pSmgr = pDevice->getSceneManager();

		

        printf("%s \n",pDevice->getFileSystem()->getWorkingDirectory().c_str());
		
		{
			irr::scene::otho::COthoFactory *pFactory = new irr::scene::otho::COthoFactory(pSmgr);
			pSmgr->registerSceneNodeFactory(pFactory);
			pFactory->drop();	
		}

		pSmgr->loadScene("../../res/scene/galaga.xml");

		irr::s32 index = 0;

		while(pDevice->run())
		{
			pVideo->beginScene(true,true,
				irr::video::SColor(255,100,101,140)); 

			if(theApp.PatchKey(irr::KEY_RIGHT))
			{
				index++;				
			}

			irr::scene::otho::CLayerManager_node::drawSprite(pSmgr,"lm_0",irr::core::position2di(100,100),index);
			
			pVideo->endScene();
		}
		pDevice->drop();
	}
}
/*
int main()
{
	sample1::main();
	return 0;
}
 */

