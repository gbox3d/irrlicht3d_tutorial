//
//  beginnerAppDelegate.h
//  beginner
//
//  Created by 석준 이 on 11. 9. 12..
//  Copyright 2011년 우석대게임컨텐츠학과. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface beginnerAppDelegate : NSObject <NSApplicationDelegate> {
    NSWindow *window;
}

@property (assign) IBOutlet NSWindow *window;

@end
